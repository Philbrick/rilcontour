#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 11 14:57:53 2018

@author: m160897
"""
import json
import os
import copy
from PyQt5 import QtCore
from PyQt5.QtCore import QModelIndex
from rilcontourlib.machinelearning.ml_predict import ML_ModelFileIO 
from rilcontourlib.util.rcc_util import   FileInterface
from rilcontourlib.util.FileUtil import   ScanDir

class ML_ModelVersionNode () :
    def __init__ (self, modelVersionDisplayName, modelDefPath, treeModel) :
        self._text = modelVersionDisplayName.strip ()
        self._modelDefPath = modelDefPath
        self._loadedModel = None        
        self._parent = None
        self._treeModel = treeModel
        
            
    def __lt__ (self, version) :
        return self.isVersionLessThan (version) 
    
    def __le__ (self, version) :
        if (self.isVersionEqual (version)) :
            return True
        if (self.isVersionLessThan (version)):
            return True
        return False
    
    def __ne__ (self, version) :
        return not self.isVersionEqual (version) 
    
    def __eq__ (self, version) :
        return self.isVersionEqual (version) 
    
    def __gt__ (self, version) :
        if (self.isVersionEqual (version)) :
            return False
        if (self.isVersionLessThan (version)):
            return False
        return True
    
    def __ge__ (self, version) :
        if (self.isVersionLessThan (version)):
            return False
        return True
    
    def __cmp__ (self, version) :
        if self.isVersionEqual (version) :
            return 0
        elif self.isVersionLessThan (version) :
            return -1
        return 1
    
    def unloadVersionModels (self) :
        self._loadedModel = None
        
    def isVersionNode (self) :
        return True 
    
    def isNodeRemovable (self):
        return True
    
    def removeRows (self, row, count) :
        return False
    
    def isVersion (self, version) :
        return (self._text == version) 
        
    def copy (self, treeModel) :        
        return ML_ModelVersionNode (self._text, self._modelDefPath, treeModel)        
    
    def getModelDefPath (self) :
        return self._modelDefPath
    
    def getNewVersionName (self):
        if (self._parent is None):
            return self._text + "_copy"
        else:
            versionNameLst = []
            for version in self._parent.getModelVersions () :
                versionNameLst.append (version.getText ())
            teststr = self._text + "_copy"
            if teststr not in versionNameLst :
                return teststr
            else:
                count = 2
                while teststr+"_" + str (count) in versionNameLst :
                    count += 1
                return teststr+"_" + str (count)
                
    
    def isMLModel (self) :
        return True
    
    def isVersionLessThan (self, testVersionNode) :
        thisNode = self._text.lower ().split (".")
        testNode = testVersionNode._text.lower ().split (".")
        lastComp = False 
        for index in range (len (thisNode)) :
            lastComp = False 
            if index >= len (testNode):
                return False
            else:
                thistext = thisNode[index].strip ()
                testtext = testNode[index].strip ()
                try :
                    thisInt = int (thistext)    
                    testInt = int (testtext)                
                    
                    if (thisInt > testInt) :
                        return False
                    elif (thisInt < testInt) :
                        lastComp = True
                except:
                    if thistext >  testtext :
                        return False
                    elif (thistext < testtext) :
                        lastComp = True
        if len (thisNode) < len (testNode) :
            return True
        else:
            return lastComp
        
    def isVersionEqual (self, testVersionNode) :
        thisNode = self._text.lower ().split (".")
        testNode = testVersionNode._text.lower ().split (".")
        if len (thisNode) != len (testNode) :
            return False
        for index in range (len (thisNode)) :            
            thistext = thisNode[index].strip ()
            testtext = testNode[index].strip ()
            try :
                thisInt = int (thistext)    
                testInt = int (testtext)                
                if (thisInt != testInt) :
                    return False
            except:
                if testtext != thistext :
                    return False        
        return True        
    
    def getChildern (self) :
        return []
    
    def getModelVersions (self) :
        return []
    
    def getTreePath (self) :
        return self.getParent ().getTreePath ()
    
    def getTreeModel (self) :
        return self._treeModel
    
    def setTreeModel (self, treeModel) :
        self._treeModel = treeModel        
            
    def getText (self) :
        return self._text
    
    def _setParent (self, parent) :
        self._parent = parent 
            
    def getParent (self) :
        return self._parent
    
    def getRow (self) :
        if (self._parent is not None) :
            return self._parent.getModelVersions ().index (self)
        return None
    
    def updateTreePath (self) :
        newTreePath = self.getTreePath ()
        newTreePath.pop ()
        model = self.getMLModel ()
        model.setTreePath (newTreePath)
        ML_ModelFileIO.saveModelDef (model, model.getModelPath ())
            
    def getMLModel (self, ForceAllocation = False) :
        if (ForceAllocation) :
            try :
                return ML_ModelFileIO.loadModelDef (self._modelDefPath, self._treeModel.getProjectDataset())
            except:
                return None
                        
        if (self._loadedModel is not None) :
            return self._loadedModel

        if self._modelDefPath is None :
            return None
        if not os.path.isfile (self._modelDefPath) or not os.path.exists (self._modelDefPath) :
            return None        
        try :
            self._loadedModel = ML_ModelFileIO.loadModelDef (self._modelDefPath, self._treeModel.getProjectDataset())
        except:
            self._loadedModel = None
        return self._loadedModel
    
class ML_ModelTreeNode () :
    
    def __init__ (self, nodeTextName = None, treeModel = None) :
        self._text = nodeTextName.strip ()
        self._childern = []
        self._modelVersions = [] #version node        
        self._parent = None
        self._treeModel = treeModel
        self._mostRecentVersionModelCache = None
    
    def __cmp__ (self, modelTreeNode) :
        if self._text == modelTreeNode._text :
            return 0
        elif self._text < modelTreeNode._text :
            return -1
        return 1
    
    def __lt__ (self, modelTreeNode) :
        return self._text < modelTreeNode._text 
    
    def __le__ (self, modelTreeNode) :
        return self._text <= modelTreeNode._text 
    
    def __ne__ (self, modelTreeNode) :
        return self._text != modelTreeNode._text 
    
    def __eq__ (self, modelTreeNode) :
        return self._text == modelTreeNode._text 
    
    def __gt__ (self, modelTreeNode) :
        return self._text > modelTreeNode._text 
    
    def __ge__ (self, modelTreeNode) :
        return self._text >= modelTreeNode._text 
    
    def isNodeRemovable (self):
        return (len (self._childern) == 0 and len (self._modelVersions) <= 1) or (len (self._childern) <= 1 and len (self._modelVersions) == 0)
    
    def isVersion (self, testVersion) :
        for version in self._modelVersions :
            if version.isVersion (testVersion):
                return True
        return False
    
    def unloadVersionModels (self) : 
         self._mostRecentVersionModelCache = None                   
             
    def getLoadedModels (self) :
        loadedModels = []
        for child in self._childern  :
            loadedModels += child.getLoadedModels ()
        for versions in self._modelVersions  :
            loadedModels.append (versions)
        return loadedModels
            
    def getVersionWithTreeModelPath (self, path):
        for version in self._modelVersions :
            if version.getModelDefPath () == path :
                return version
        return None
            
    def getNewModelName (self):
        siblingList = []
        if self._parent is not None :            
            for siblings in self._parent.getChildern () :
                siblingList.append (siblings.getText ())
        elif self in self._treeModel.getRootMemory ():            
            for node in self._treeModel.getRootMemory () :
                siblingList.append (node.getText ())
        if len (siblingList) == 0 :            
            return self._text + "_copy"
            
        teststr = self._text + "_copy"
        if teststr not in siblingList :
            return teststr
        else:
            count = 2
            while teststr+"_" + str (count) in siblingList :
                count += 1
            return teststr+"_" + str (count)
    
    def updateTreePath (self) :
        newTreePath = self.getTreePath ()
        if (self.isMLModel ()) :
            newTreePath.pop ()        
        
        for child in self._childern :
            child.updateTreePath ()
            
        for version in self.getModelVersions ():
            model = version.getMLModel ()
            model.setTreePath (newTreePath)
            ML_ModelFileIO.saveModelDef (model, model.getModelPath ())
        
    def isVersionNode (self) :
        return False
    
    def isMLModel (self) :
        return len (self._modelVersions) > 0 
    
    def copy (self, parent, treeModel) :
        
        newTreeNode = ML_ModelTreeNode ()
        newTreeNode._text = self._text
        newTreeNode._parent = parent
        newTreeNode._treeModel = treeModel
        for node in self._modelVersions :
            newTreeNode._modelVersions.append (node.copy (treeModel))
    
    def getModelVersions (self) :
        return self._modelVersions
        
    def getMostRecentModelVersion (self) :
        versionCount = len (self._modelVersions)
        if versionCount <= 0 :
            return  None
        if versionCount == 1 :
            return  self._modelVersions[0]        
        foundVersion = self._modelVersions[0]
        for versionIndex in range (1, versionCount) :
            testVersion = self._modelVersions[versionIndex]
            if foundVersion.isVersionLessThan (testVersion) :
                foundVersion = testVersion
        return foundVersion
        
    def  getChildNodeWithName (self, name) : 
        lowerName = name.lower ()
        for child in self._childern :
            childname = child.getText ()
            if childname.lower () == lowerName :
                return child
        return None
    
    def getTreeModel (self) :
        return self._treeModel
    
    def setTreeModel (self, treeModel) :
        self._treeModel = treeModel
        for child in self._childern :
            child.setTreeModel (treeModel)
        for child in self._modelVersions :
            child.setTreeModel (treeModel)
            
    def getText (self) :
        if self._text is None :
            model = self.getMLModel  ()
            self._text =  model.getModelName ()
        return self._text
    
    def getChildern (self) :
        return self._childern
    
    def _setParent (self, parent) :
        self._parent = parent 
        
    def isRoot (self) :
        return (self._parent  is None)
    
    def removeAllChildNodes (self) :
        removelist = copy.copy (self._modelVersions)
        for version in removelist :
            self.removeVersion (version)
        childlist = copy.copy (self._childern)
        for child in childlist :
            child.removeAllChildNodes ()
            self.removeChild (child)
            
    def removeRows (self, row, count) :        
        if len (self._modelVersions) > 0 :     
            removeVersionList = []
            for rowIndex in range (row, row+count) :
                removeVersionList.append (self._modelVersions[rowIndex])
            for item in removeVersionList :
                self.removeVersion (item)
            return True
        elif len (self._childern) > 0 :     
            removeChildList = []            
            for rowIndex in range (row, row+count) :
                self._childern[rowIndex].removeAllChildNodes ()
                removeChildList.append (self._childern[rowIndex])
            for item in removeChildList :
                self.removeChild (item)
            return True
        else:
            return False
        
    def removeChild (self, child) :      
        index = self._childern.index (child)        
        #print (("RemoveChild", self.getText (), child.getText (), index, self._childern, self.getRow ()))
        if index is not None and index >= 0 and index <= len (self._childern) :
            self._treeModel.clearSelection ()            
            nodechanged = self._treeModel.createIndex(self.getRow (), 0, self)                                                                             
            self._treeModel.beginRemoveRows(nodechanged, index, index)
            del self._childern[index]
            self._treeModel.endRemoveRows()
        
        
            
    def removeVersion (self, version) :  
        index = self._modelVersions.index (version)        
        if (index == 0 and len (self._modelVersions) == 1)  :
            del self._modelVersions[0]        
        elif index is not None and index >= 0 and index < len (self._modelVersions) : 
            self._treeModel.clearSelection ()
            nodechanged = self._treeModel.createIndex(self.getRow (), 0, self)                                                                                                                 
            if len (self._modelVersions) == 2 :            
                self._treeModel.beginRemoveRows(nodechanged, 0, 1)                
            else:
                self._treeModel.beginRemoveRows(nodechanged, index, index)
            del self._modelVersions[index]        
            self._treeModel.endRemoveRows()
            
    def hasVersion (self, versionStr) :
        for version in self._modelVersions :
            if version.getText () == versionStr :
                return True
        return False
        
    def getVersion (self, versionStr) :
        for version in self._modelVersions :
            if version.getText () == versionStr :
                return version
        return None
    
    def addVersion (self, version,  HandleDuplicateVersion = "Ignore") :
        if (HandleDuplicateVersion == "Ignore"):
            for existingVersion in self._modelVersions :
                if existingVersion.isVersionEqual (version) :
                    return False
        elif (HandleDuplicateVersion == "Replace"):
            for existingVersion in self._modelVersions :
                if existingVersion.isVersionEqual (version) :
                   self.removeVersion (existingVersion)
            
        version._setParent (self)      
        version.setTreeModel (self.getTreeModel ())        
        tempVersionList = copy.copy (self._modelVersions)
        tempVersionList.append (version)        
        self._mostRecentVersionModelCache = None                   
        tempVersionListLen = len (tempVersionList)
        if (tempVersionListLen <= 1):
            self._modelVersions = tempVersionList              
        else:
            tempVersionList = sorted (tempVersionList)   
            tempVersionList.reverse ()
            if (tempVersionListLen == 2):
                nodechanged = self._treeModel.createIndex(self.getRow (), 0, self)                                                                                                         
                self._treeModel.beginInsertRows (nodechanged, 0,1)
                self._modelVersions = tempVersionList              
                self._treeModel.endInsertRows ()        
            elif  (tempVersionListLen > 2) :
                rowChanged = tempVersionList.index (version)        
                nodechanged = self._treeModel.createIndex(self.getRow (), 0, self)                                                                                                         
                self._treeModel.beginInsertRows (nodechanged, rowChanged,rowChanged)
                self._modelVersions = tempVersionList              
                self._treeModel.endInsertRows ()        
        return True
    
    
           
        
    def getTreePath (self) :
            node = self
            pathLst = []
            while (node is not None) :
                pathLst.append (node.getText ())
                node = node.getParent ()
            pathLst.reverse ()
            return pathLst
    
    def setText (self, text) :        
        self._treeModel.layoutAboutToBeChanged.emit ()            
        rowChanged= self.getRow ()
        nodechanged = self._treeModel.createIndex(rowChanged, 0, self)                                                                             
        self._text = text.strip ()
        self._treeModel.dataChanged.emit (nodechanged, nodechanged)
        self._treeModel.changePersistentIndex(nodechanged, nodechanged)
        self._treeModel.layoutChanged.emit ()                      
        
        if not self.isMLModel () :
            self.getTreeModel()._saveModelTreeLeafDef ()
            self.updateTreePath ()
    
    def addChild (self, child) :                
        child._setParent (self)      
        child.setTreeModel (self.getTreeModel ())        
        tempChildernList = copy.copy (self._childern)
        tempChildernList.append (child)        
        tempChildernList = sorted (tempChildernList)
        rowChanged = tempChildernList.index (child)                        
        nodechanged = self._treeModel.createIndex(self.getRow (), 0, self)       
        #print (("Add Child", self.getText(), rowChanged, rowChanged))                                                                      
        self._treeModel.beginInsertRows (nodechanged, rowChanged,rowChanged)    
        self._childern = tempChildernList        
        self._treeModel.endInsertRows ()

    
    
    def getParent (self) :
        return self._parent
    
    def getRow (self) :
        try:
            if (self._parent is not None) :
                return self._parent.getChildern ().index (self)
            else:
                return self._treeModel.getRootMemory ().index (self)            
        except:
            return None
    
    def getMLModel (self) :
        if (self._mostRecentVersionModelCache is None) :            
            self._mostRecentVersionModelCache = self.getMostRecentModelVersion  ()
        if (self._mostRecentVersionModelCache is not None) :
            return self._mostRecentVersionModelCache.getMLModel ()
        return None
    
    def hasChildNamed (self, childName) :
        for child in self._childern :
            if child.getText () ==  childName :
                return True
        return False
    
    def copyNodeToTreePath (self, treeModel, treepath) :       
       newPath = treepath + [self.getText ()]
       if not self.isMLModel () :           
           treeModel.addNonModelPath (newPath)                                                           
       else:
           for version in self._modelVersions : 
               modelVersion = version.getMLModel (ForceAllocation = True)
               modelVersion.setTreePath (treepath)
               newtreeNode = treeModel.createTreeNode (treeModel, Model = modelVersion, HandleDuplicateVersion="Append", ProjectDataset=treeModel.getProjectDataset ())               
               if (newtreeNode is not None) :
                   newtreeNode.updateTreePath ()
           
       for child in self._childern :
           child.copyNodeToTreePath (treeModel, newPath)
       
                 

    
class ML_ModelTree  (QtCore.QAbstractItemModel):                     
        def __init__(self, ProjectDataset, parent, *args):                    
            QtCore.QAbstractItemModel.__init__(self, parent, *args)                              
            self._ProjectDataset = ProjectDataset
            self._Parent = parent
            self._rootMemory = []            
            self._MLHomeFilePath = ""
            self._clearSelectionCallBack = None
            
        def isTreeModelImportedFromHomeDir (self):
            try :
                return self._MLHomeFilePath == self._ProjectDatase.getMLHomeFilePath () 
            except:
                return False
                
        def getProjectDataset (self) :
            return self._ProjectDataset
        
        def _getMLHomeFilePath (self) :
            return self._MLHomeFilePath
        
        def _setMLHomeFilePath (self, path) :
            self._MLHomeFilePath = path
        
        def addRootNode (self, node) :
            self._rootMemory.append (node)
            
        def getRootMemory (self) :
            return self._rootMemory
        
        def getRootNodeWithName (self, name) :
            for node in self._rootMemory :
                if node.getText () == name :
                    return node
            return None
        
        def setClearSelectionCallback (self, callback) :
            self._clearSelectionCallBack = callback
             
        def clearSelection (self) :
            if (self._clearSelectionCallBack is not None) :
                self._clearSelectionCallBack ()
        
        def removeTreeNode (self, node) :
            if (node.isVersionNode ()) :
                parentNode = node.getParent ()
                #print ("Remove Version 1")
                parentNode.removeVersion (node)
                if (len (parentNode.getModelVersions ()) == 0):
                    self.removeTreeNode (parentNode)
                modelRemoved = node.getMLModel ()
                return (True, modelRemoved)
            else:
                childernCount = len (node.getChildern ())
                versionCount = len (node.getModelVersions ())
                if (childernCount == 0 and versionCount <= 1) :
                    #print ("Remove Version 2")
                    parentNode = node.getParent ()
                    if parentNode is not None :
                        modelRemoved = node.getMLModel ()
                        parentNode.removeChild (node)
                        if modelRemoved is None :
                            self._saveModelTreeLeafDef ()                        
                        return (True, modelRemoved)                    
                elif (childernCount <= 1 and versionCount == 0) :
                    #print ("Remove Child")parent
                    parentNode = node.getParent ()
                    if parentNode is not None :
                        modelRemoved = node.getMLModel ()
                        parentNode.removeChild (node)
                        newChildNode = node.getChildern ()[0]
                        parentNode.addChild (newChildNode)        
                        newChildNode.updateTreePath ()
                        self._saveModelTreeLeafDef ()
                        return (True, modelRemoved)                                        
            return (False, None)

        @staticmethod
        def createTreeNode (newTree, Model = None, ModelPath = None, TreeVersionNode = None, HandleDuplicateVersion="Ignore", ProjectDataset = None) :
            if (Model is not None) :
                model = Model
                log = model.getModelDescriptionLog ()
                versionTxt = log.getVersion ()
                modelPath = model.getModelPath ()
                TreeVersionNode = ML_ModelVersionNode (versionTxt, modelPath, newTree)
            elif ModelPath is not None :
                model = ML_ModelFileIO.loadModelDef  (ModelPath, ProjectDataset)
                log = model.getModelDescriptionLog ()
                versionTxt = log.getVersion ()
                TreeVersionNode = ML_ModelVersionNode (versionTxt, ModelPath, newTree)
            else:
                model = TreeVersionNode.getMLModel (ForceAllocation = True)
            modelsTreePath = model.getTreePath ()                            
            
            node = None
            node = newTree.getRootNodeWithName (modelsTreePath[0])
                                             
            if node is not None :
                for treeNodeIndex in range (1, len (modelsTreePath)) :                
                     childNode = node.getChildNodeWithName (modelsTreePath[treeNodeIndex])
                     if childNode is not None :
                         if len (childNode.getModelVersions ()) > 0 :
                             return None#cannot add to a node which  has versions
                         node = childNode
                     else:
                        newTextChildNode = ML_ModelTreeNode (nodeTextName = modelsTreePath[treeNodeIndex])
                        newTextChildNode.setTreeModel (newTree)
                        node.addChild (newTextChildNode)
                        node = newTextChildNode                               
                childNode = node.getChildNodeWithName (model.getModelName ())
                if childNode is None :
                    mlModelTreeNode = ML_ModelTreeNode (model.getModelName (), newTree)
                    node.addChild (mlModelTreeNode)
                    node = mlModelTreeNode
                else:
                    node = childNode
            if node is not None :
                if len (node.getChildern ()) != 0 : # must be adding to existing Model Node
                      return None          
                if node.addVersion (TreeVersionNode, HandleDuplicateVersion = HandleDuplicateVersion) :
                    return TreeVersionNode
            return None
            
        
        
        
                
                
                
        def _getLoadedModelList (self) :
            loadedModels = []
            for node in self._rootMemory :
                loadedModels += node.getLoadedModels ()                
            return loadedModels
                        
        def getFilteredTreeModel (self, modelType = None, filterROIList = [], filterText=""):                                                            
            if modelType is not None and modelType not in ["classification", "segmentation","classification & segmentation"] :
                print(6 / 0)
                
            newTree = ML_ModelTree (self._ProjectDataset, self._Parent)   

            if modelType == "classification" or modelType == "classification & segmentation" :
                newTree.addRootNode (ML_ModelTreeNode ("Classification",newTree))
            if modelType == "segmentation" or modelType == "classification & segmentation" :
                newTree.addRootNode (ML_ModelTreeNode ("Segmentation",newTree))
            
            
            filterText = filterText.lower().strip ()
            loadedModels = self._getLoadedModelList ()
            for modelNode in loadedModels :                
                model = modelNode.getMLModel ()                
                if (modelType is not None) :
                    if (modelType == "segmentation" and model.isClassificationModel ()) :
                        continue
                    elif (modelType == "classification" and  model.isSegmentationModel ()) :
                        continue
                filterFound = True
                if len (filterText) > 0 :
                    filterFound = False
                    log = model.getModelDescriptionLog ()
                    if filterText in model.getModelName ().lower () or filterText in log.getDescription().lower () or filterText in log.getUser().lower () :
                        filterFound = True
                    if not filterFound :
                        for treePath in model.getTreePath () :
                            if filterText in treePath.lower ()  :
                                filterFound = True
                                break
                if (not filterFound) :
                    continue                                

                if model.isSegmentationModel () and len (filterROIList) > 0:
                     roiPredictionList = model.getROIPredictionList ()
                     if len (roiPredictionList) <= 0:
                         continue
                     matchingROI = False
                     for roiDef in filterROIList :
                         if roiDef is not None :
                             roiName = roiDef.getName ()
                             radlexID = roiDef.getRadlexID ()
                             roiType = roiDef.getType ()
                             for searchROI in roiPredictionList  :
                                 searchRoiType = searchROI.getType ()
                                 if roiType == searchRoiType :
                                     if (radlexID is None or radlexID == "" or radlexID == "unknown") :                                     
                                         if (roiName.lower () == searchROI.getName ().lower ()):
                                             matchingROI = True
                                             break
                                     else:                                                                    
                                         searchRadlexID = searchROI.getRadlexID ()                                
                                         if searchRadlexID == radlexID :
                                             matchingROI = True
                                             break
                         if (matchingROI)   :
                             break
                     if (not matchingROI) :
                         continue
                try:
                    ML_ModelTree.createTreeNode (newTree, TreeVersionNode = modelNode.copy (newTree), ProjectDataset=self.getProjectDataset())    
                except:
                    print ("Could not add filtered tree node to tree model")
            return newTree
                    
        def addNonModelPath (self, modelsTreePath) :
            if len (modelsTreePath) > 0 :                
                rootText = modelsTreePath[0] 
                node = self.getRootNodeWithName (rootText)                
                if (node is not None) :                
                    for treeNodeIndex in range (1, len (modelsTreePath)) :                
                        childNode = node.getChildNodeWithName (modelsTreePath[treeNodeIndex])
                        if childNode is None :                            
                            newTextChildNode = ML_ModelTreeNode (nodeTextName = modelsTreePath[treeNodeIndex])
                            newTextChildNode.setTreeModel (self)
                            node.addChild (newTextChildNode)
                            node = newTextChildNode          
                            self._saveModelTreeLeafDef ()
                        
        def _loadModelTreeLeafDef (self):  
            # ml_homePath = self._ProjectDataset.getMLHomeFilePath ()
            ml_homePath = self._getMLHomeFilePath ()
            if (ml_homePath is not None) :                
                try :
                    fobj = FileInterface ()                      
                    fobj.readDataFile (os.path.join (ml_homePath, "modeldef.dat"))   
                    header = fobj.getFileHeader ()      
                except:
                    header = None
                if (header is not None) :
                    if header.hasParameter ("NonModelPathCount") :
                        pathcount = header.getParameter ("NonModelPathCount")
                        for index in range (pathcount) :
                            path_str = header.getParameter ("M_Path_" + str (index))
                            self.addNonModelPath (path_str)                                    

        def _saveModelTreeLeafDef (self):  
            ml_homePath = self._getMLHomeFilePath ()
            if (ml_homePath is not None) :
                
                treemodel_leaf_path = self._getNonModelLeafPaths ()                
                try :
                    fobj = FileInterface ()             
                    header = fobj.getFileHeader ()      
                    header.setParameter ("NonModelPathCount", len (treemodel_leaf_path))
                    for index in range (len (treemodel_leaf_path)) :
                        header.setParameter ("M_Path_" + str (index), treemodel_leaf_path[index])
                    fobj.writeDataFile (os.path.join (ml_homePath, "modeldef.dat"))                       
                except:
                    pass
                                
        def _getNonModelLeafPaths (self) :
            treePathList = []
            nodeQue = []
            for rootNode in self._rootMemory :
                nodeQue.append (rootNode)
            while len (nodeQue) > 0 :
                nodeItem = nodeQue.pop ()                
                if len (nodeItem.getChildern ()) > 0 :
                    for child in nodeItem.getChildern () :
                        nodeQue.append (child)                
                elif not nodeItem.isMLModel () :                 
                        treepath = nodeItem.getTreePath ()
                        treePathList.append (treepath)
            return treePathList
        
        @staticmethod
        def loadModelTreeFromPath (path, ExistingModelTreeToAddTo = None, LoadModelTreeTextLeafs = True, ProjectDataset=None ):                               
            if ExistingModelTreeToAddTo is  None :
                print (4 / 0)       # error should be defined    
            else:
                newTree = ExistingModelTreeToAddTo            
            newTree._setMLHomeFilePath (path)                       
            try:
                fileIterator = ScanDir.scandir(path)
                for fobj in fileIterator :                    
                    if fobj.name.endswith (".model") :
                        try:                                                 
                            ML_ModelTree.createTreeNode (newTree, ModelPath = fobj.path, ProjectDataset=ProjectDataset)
                        except:
                            pass        
            finally:
                try:
                   fileIterator.close ()
                except:
                    pass
                
            if (LoadModelTreeTextLeafs) :
                newTree._loadModelTreeLeafDef ()
            return newTree
                
        def index (self, row, column, parent):
            if not parent.isValid():         
                if row < len (self._rootMemory) :
                    return self.createIndex(row, column, self._rootMemory[row])            
            else :
                parent = parent.internalPointer()                    
                if len (parent.getChildern ()) == 0 :
                    if row < len (parent.getModelVersions ()) :
                        return self.createIndex(row, column, parent.getModelVersions ()[row])                
                else:
                    #print ((row, column, parent.getChildern ()))
                    if row < len (parent.getChildern ()) :
                        return self.createIndex(row, column, parent.getChildern ()[row])     
            #print (("Error out of bounds", parent.getText (), row, parent.getChildern (), parent.getModelVersions ()))
            return QModelIndex()

        def data(self, index, role):
            if not index.isValid():
                return None
            index = index.internalPointer()            
            if role == QtCore.Qt.DisplayRole :                                              
                return index.getText ()                 
            return None
        
        def hasChildren (self, parent) :
            if not parent.isValid():
               return True
            parent = parent.internalPointer()
            childernLen = len (parent.getChildern ())
            if childernLen == 0 :
                return len (parent.getModelVersions ()) > 1
            return (childernLen > 0)            
        
        def hasIndex (self, row, column, parent) :
            if (row < 0) :
                return False
            if not parent.isValid():
                return row < len (self._rootMemory)            
            parent = parent.internalPointer()
            childernLen = len (parent.getChildern ())
            if childernLen == 0 :
                ModelVersionLen = len (parent.getModelVersions ())
                if ModelVersionLen > 1 :
                    return (row < ModelVersionLen)            
                return False
            return (row < childernLen)            
        
        def rowCount(self, parent):
          if not parent.isValid():
               return len (self._rootMemory)
          else:
              parent = parent.internalPointer()
              childLen = len (parent.getChildern ())              
              if childLen > 0 :
                  #print (("RowCount",parent.getText (), childLen,0))
                  return childLen
              versionLen = len (parent.getModelVersions ())
              if (versionLen > 1) :
                  #print (("RowCount",parent.getText (), 0,versionLen))
                  return versionLen
              return 0
              
        def parent (self, index) :
            if not index.isValid():
                return QModelIndex()
            index = index.internalPointer()
            parent = index.getParent ()            
            if parent is None :
                return QModelIndex() 
            else:
                row = parent.getRow ()
                if row is not None :
                    return self.createIndex(row, 0, parent)                
                else:
                    row = self._rootMemory.index(parent)
                    return self.createIndex(row, 0, parent)                
        
      
        
        def removeRows (self, row, count, parent) :
            if (parent.isValid ()) :
                node = parent.internalPointer()
                removedRows = node.removeRows (row, count)
                if (removedRows) :
                    self._saveModelTreeLeafDef ()
                return removedRows
            return False
        
        def mimeTypes(self) :           
           return ["application/treepath.text.list"]
 
        def mimeData(self, QModelIndexList) :
           mimeData = QtCore.QMimeData ()
           treePathList = []
           for item in QModelIndexList :
               if item.isValid () :
                   node = item.internalPointer ()                   
                   treePathList.append (node.getTreePath ())
           treePathStr = json.dumps (treePathList)
           bArray = QtCore.QByteArray ()
           bArray.append (treePathStr)
           mimeData.setData("application/treepath.text.list", bArray)     
           return mimeData

       
        def supportedDropActions(self) :
            return QtCore.Qt.MoveAction
        
        def flags (self, index) :           
            indexIsValid = (index.isValid())
            index = index.internalPointer()
            if (indexIsValid) :
                if index.isVersionNode () :
                    return  QtCore.Qt.ItemIsEnabled |  QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemNeverHasChildren
                elif (index.getParent () is not None) :
                    if (index.isMLModel ()) :
                        return  QtCore.Qt.ItemIsEnabled |  QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsDragEnabled 
                    else:
                        return  QtCore.Qt.ItemIsEnabled |  QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsDragEnabled | QtCore.Qt.ItemIsDropEnabled
                else:  
                    return  QtCore.Qt.ItemIsEnabled |  QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsDropEnabled
            return  QtCore.Qt.ItemIsEnabled |  QtCore.Qt.ItemIsSelectable                
        
        def _getTreeNode (self, pathList, nodeName)               :
            rootname = pathList[0]
            node = self.getRootNodeWithName (rootname)
            if node is None :
                return None
            for index in range (1, len (pathList)) :
                node = node.getChildNodeWithName (pathList[index])
                if node is None :
                    return None
            return node.getChildNodeWithName (nodeName)
        
        def _isParent (self, isParentNode, testNode) :
            path1 = isParentNode.getTreePath ()
            path2 = testNode.getTreePath ()
            if len (path1) <= len (path2):
                for index in range (len(path1)) :
                    if path1[index] != path2[index] :
                        return False                    
                return True
            else:
                return False
            
        def dropMimeData(self, minedata, action, row, column, parent) :
           if action == QtCore.Qt.IgnoreAction :
               return True
           if not minedata.hasFormat("application/treepath.text.list") :
               return False
           if column > 0:
               return False
           if not parent.isValid () :
               return False
           parent = parent.internalPointer ()
           if (parent.isMLModel ()) :
               return False
           parentTreePath = parent.getTreePath  ()
           parentTreePath.append (parent.getText ())
           minedata = minedata.data("application/treepath.text.list")
           textData = minedata.data ()
           droppedTreePathList = json.loads (textData)
           rootModelType = parentTreePath[0].lower ()
           treeNodeListToMove = []
           for treePath in droppedTreePathList :
               nodeName = treePath.pop ()                
               if treePath[0].lower () != rootModelType :
                   return False
               if parent.hasChildNamed (nodeName)  :
                   return False               
               treeNode = self._getTreeNode (treePath, nodeName)   
               if self._isParent (treeNode, parent) :
                   return False
               if treeNode is None :
                   return False
               treeNodeListToMove.append (treeNode)
           path_list = copy.copy (parent.getTreePath ())           
           for treeNode in treeNodeListToMove :                                           
               treeNode.copyNodeToTreePath (self, path_list)      
           self._saveModelTreeLeafDef ()
           return True
     
        def headerData (self, section, orientation, role):
            return str ("ML Models")
        
        def columnCount(self, parent):
            return 1
        
        def getExistingTreeNodeModelDef (self, modelDefinition) :
            treePath = modelDefinition.getTreePath () 
            nodeName = modelDefinition.getModelName ()
            treeNode = self._getTreeNode (treePath, nodeName) 
            if (treeNode is None):
                return None
            else:
                if not treeNode.isMLModel () :
                    return None
                else:
                    version = modelDefinition.getModelDescriptionLog ().getVersion ()
                    versionStr = version.strip ()
                    version = treeNode.getVersion (versionStr)
                    if (version is not None) :
                        return version.getMLModel ()
                    return None
                
        def doesModelDefinitionOrTreeNodeExist (self, modelDefinition, IgnoreNode = None) :
            treePath = modelDefinition.getTreePath () 
            nodeName = modelDefinition.getModelName ()
            treeNode = self._getTreeNode (treePath, nodeName) 
            if (treeNode is None):
                return False
            else:
                if IgnoreNode is None :
                    return True
                elif not treeNode.isMLModel () :
                    return True
                else:
                    
                    version = modelDefinition.getModelDescriptionLog ().getVersion ()
                    versionStr = version.strip ()
                    if (not treeNode.hasVersion (versionStr)) :
                        return False
                    if IgnoreNode.isVersion (versionStr) :
                        return False
                    return True
                
                    
                
                    
            