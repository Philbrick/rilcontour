#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 11 14:57:53 2018

@author: m160897
"""

from six import *
import os
from PyQt5 import QtCore, QtGui
from PyQt5.QtWidgets import  QDialog, QFileDialog, QLineEdit, QTextEdit
from PyQt5.QtGui import QStandardItemModel
from PyQt5.QtWidgets import  QAbstractItemView, QComboBox, QItemDelegate
from rilcontourlib.dataset.roi.rcc_roi import ROIDef
from rilcontourlib.util.rcc_util import ResizeWidgetHelper, NiftiVolumeData, Coordinate, Tag, MessageBoxUtil
from rilcontourlib.util.DateUtil import TimeUtil, DateUtil
from rilcontourlib.ui.qt_ui_autogen.RCC_ML_ModelDefDlgAutogen import Ui_RCC_ML_ModelDefDlg
import numpy as np
from rilcontourlib.machinelearning.ml_predict import  ML_ModelInputTransformation, ML_ApplyROI2DPrediction, ML_ApplyTagPrediction, ML_PredictionMap, ML_PredictionPreferedViewUtil
from rilcontourlib.util.ErrorListDlg import ErrorListDlg

#-----------------------------------------------------------
#Annotation Prediction Table Model
#-----------------------------------------------------------


class TagDelegate (QItemDelegate) :    
    
    def __init__ (self,  columnType, tableModel, parent) :
         QItemDelegate.__init__ (self, parent)
         #columnType = "Threshold"
         #columnType = "TagName"
         #self._columnType = "TagTrueValue"
         #self._columnType = "TagFalseValue"
         self._columnType = columnType
         self._tableModel = tableModel
         self._tagComboBox = None
        
         self._selectedRow = None
         self._selectedWidget = None
         
    def _tagNameChanged (self) :
        selectedTagIndex = self._tagComboBox.currentIndex() 
        PredictionRowIndex = self._selectedRow
        tagManager = self._tableModel.getAnnotationTagManager ()
        tag = tagManager.getCustomTagByIndex (selectedTagIndex)
        self._tableModel.initTableModelTagData (PredictionRowIndex, tag.getIdentifer())
        
        
    def createEditor(self, parent, options, qModelIndex) :   
        PredictionRowIndex = self._tableModel.data(qModelIndex, QtCore.Qt.UserRole) 
        self._selectedRow = PredictionRowIndex
        rowData = self._tableModel.getPredictionMapIndex (PredictionRowIndex)
        threshold, tagnameID, trueValue, falseValue = rowData
        #tagManager = self._tableModel.getAnnotationTagManager ()
        self._tagComboBox = None
        self._selectedWidget = None
        if (self._columnType == "Threshold") :
            editor = QLineEdit (parent)               
            #predictionIndex = self._tableModel.data(qModelIndex, QtCore.Qt.UserRole)
            editor.setText (str(threshold))
            editor.setAutoFillBackground(True)        
            self._selectedWidget = editor
            self._selectedWidget.textChanged.connect (self._widgetChanged)
            return editor
        
        elif (self._columnType == "TagName") : 
            editor = QComboBox (parent)
            self._tagComboBox = editor
            editor.currentIndexChanged.connect (self._tagNameChanged)
            editor.setAutoFillBackground(True)
            tagManager = self._tableModel.getAnnotationTagManager ()
            tagcount = tagManager.getCustomTagCount()
            selectedTagIndex = -1
            for tagindex in range (tagcount) :
                tag = tagManager.getCustomTagByIndex (tagindex)
                editor.addItem (tag.getName ())    
                if tag.getIdentifer() == tagnameID :
                    selectedTagIndex = tagindex
            if (selectedTagIndex != -1) :
                editor.setCurrentIndex(selectedTagIndex) 
            return editor
        
        elif (self._columnType == "TagTrueValue") or (self._columnType == "TagFalseValue") : 
            tagManager = self._tableModel.getAnnotationTagManager ()
            if (tagnameID is None) :
                tag = tagManager.getCustomTagIndex (0)
            else:
                tag = tagManager.getTagByIdentifer (tagnameID)
                if (tag is None) :
                    tag = tagManager.getCustomTagIndex (0)
            tagType = tag.getType ()
            if (self._columnType == "TagTrueValue") :
                tagValue = trueValue
            elif (self._columnType == "TagFalseValue") :
                tagValue = falseValue
            if tagType == "ItemList" or tagType == "Description" : #["ItemList", "Checked", "Text", "Description"]
                editor = QComboBox (parent)
                editor.setAutoFillBackground(True)
                paramList = tag.getParameterList ()
                selectedParam = -1
                for pIndex, param in enumerate(paramList) :
                     editor.addItem (param)    
                     if param == tagValue :
                         selectedParam = pIndex
                if (selectedParam != -1) :   
                    editor.setCurrentIndex(selectedParam)  
                self._selectedWidget = editor
                self._selectedWidget.currentIndexChanged.connect (self._widgetChanged)
                return editor
            elif tagType == "Text"  : #["ItemList", "Checked", "Text", "Description"]
               editor = QTextEdit (parent)       
               editor.setText (tagValue)
               editor.setAutoFillBackground(True)   
               self._selectedWidget = editor
               self._selectedWidget.textChanged.connect (self._widgetChanged)
               return editor     
            
   
   
        
    def setEditorData(self, qWidgetEditor, qModelIndex) :
        PredictionRowIndex = self._tableModel.data(qModelIndex, QtCore.Qt.UserRole) 
        rowData = self._tableModel.getPredictionMapIndex (PredictionRowIndex)
        threshold, tagnameID, trueValue, falseValue = rowData
       
        if (self._columnType == "Threshold") :
            try :
                self._selectedWidget.textChanged.disconnect (self._widgetChanged)
                reconnect = True
            except:
                reconnect = False
            qWidgetEditor.setText (str(threshold))
            if (reconnect) :
                   self._selectedWidget.textChanged.connect (self._widgetChanged)
            return
        
        elif (self._columnType == "TagName") :
            tagManager = self._tableModel.getAnnotationTagManager ()
            tagcount = tagManager.getCustomTagCount()
            selectedTagIndex = -1
            for tagindex in range (tagcount) :
                tag = tagManager.getCustomTagByIndex (tagindex)
                if tag.getIdentifer() == tagnameID :
                    selectedTagIndex = tagindex
            if (selectedTagIndex != -1) :
                qWidgetEditor.setCurrentIndex(selectedTagIndex) 
            return
        
        elif (self._columnType == "TagTrueValue") or (self._columnType == "TagFalseValue") : 
            tagManager = self._tableModel.getAnnotationTagManager ()
            if (tagnameID is None) :
                tag = tagManager.getCustomTagIndex (0)
            else:
                tag = tagManager.getTagByIdentifer (tagnameID)
                if (tag is None) :
                    tag = tagManager.getCustomTagIndex (0)
            tagType = tag.getType ()
            if (self._columnType == "TagTrueValue") :
                tagValue = trueValue
            elif (self._columnType == "TagFalseValue") :
                tagValue = falseValue
            if tagType == "ItemList" or tagType == "Description" : #["ItemList", "Checked", "Text", "Description"]
                paramList = tag.getParameterList ()
                selectedParam = -1
                for pIndex, param in enumerate(paramList) :
                     if param == tagValue :
                         selectedParam = pIndex
                if (selectedParam != -1 and qWidgetEditor.currentIndex() != selectedParam) :   
                    try :
                         self._selectedWidget.currentIndexChanged.disconnect (self._widgetChanged)
                         reconnect = True
                    except:
                        reconnect = False
                    qWidgetEditor.setCurrentIndex(selectedParam)  
                    if (reconnect) :
                        self._selectedWidget.currentIndexChanged.connect (self._widgetChanged)
                return 
            elif tagType == "Text"  : #["ItemList", "Checked", "Text", "Description"]
               try :
                     self._selectedWidget.textChanged.disconnect (self._widgetChanged)
                     reconnect = True
               except:
                     reconnect = False
               qWidgetEditor.setText (tagValue)
               if (reconnect) :
                   self._selectedWidget.textChanged.connect (self._widgetChanged)
               return     
            """elif tagType == "Checked"  : 
               for pIndex, param in enumerate(["True", "False"]) :
                   if param == tagValue :
                      selectedParam = pIndex
               if (selectedParam != -1) :   
                   qWidgetEditor.setCurrentIndex(selectedParam)  
               return"""
    
    def _widgetChanged (self, index = None) :
       self.setData (self._selectedWidget, self._tableModel, self._selectedRow, UpdateTableModelData = False) 
       
    def setModelData(self,  qWidgetEditor, qAbstractModel, qModelIndex) :                
        PredictionRowIndex = self._tableModel.data(qModelIndex, QtCore.Qt.UserRole) 
        self.setData (qWidgetEditor,qAbstractModel, PredictionRowIndex, UpdateTableModelData = True) 
    
    def setData (self, qWidgetEditor, model, PredictionRowIndex, UpdateTableModelData = True) :  
        rowData = self._tableModel.getPredictionMapIndex (PredictionRowIndex)
        threshold, tagnameID, trueValue, falseValue = rowData
       
        if (self._columnType == "Threshold") :
            threshold = qWidgetEditor.text ()
            self._tableModel.setTableModelData (PredictionRowIndex, threshold, tagnameID, trueValue, falseValue, UpdateTableModelData)
            return
        
        elif (self._columnType == "TagName") :
            tagManager = self._tableModel.getAnnotationTagManager ()
            selectedTagIndex = qWidgetEditor.currentIndex  () 
            tag = tagManager.getCustomTagByIndex (selectedTagIndex)
            self._tableModel.setTableModelData (PredictionRowIndex, threshold, tag.getIdentifer(), trueValue, falseValue, UpdateTableModelData)
            return
        
        elif (self._columnType == "TagTrueValue") or (self._columnType == "TagFalseValue") : 
            tagManager = self._tableModel.getAnnotationTagManager ()
            if (tagnameID is None) :
                tag = tagManager.getCustomTagIndex (0)
            else:
                tag = tagManager.getTagByIdentifer (tagnameID)
                if (tag is None) :
                    tag = tagManager.getCustomTagIndex (0)
            tagType = tag.getType ()
            
            if tagType == "ItemList" or tagType == "Description" : #["ItemList", "Checked", "Text", "Description"]
                paramList = tag.getParameterList ()
                selectedParam = qWidgetEditor.currentIndex()
                tagValue = paramList[selectedParam]
            elif tagType == "Text"  : #["ItemList", "Checked", "Text", "Description"]

                tagValue = qWidgetEditor.toPlainText()
                   
            """elif tagType == "Checked"  : 
               paramList = ["True", "False"]
               selectedParam = qWidgetEditor.currentIndex()
               tagValue = paramList[selectedParam]"""
                
            if (self._columnType == "TagTrueValue") :
               self._tableModel.setTableModelData (PredictionRowIndex, threshold, tagnameID, tagValue, falseValue, UpdateTableModelData)
            elif (self._columnType == "TagFalseValue") :
               self._tableModel.setTableModelData (PredictionRowIndex, threshold, tagnameID, trueValue, tagValue, UpdateTableModelData)
            return
        
    def updateEditorGeometry(self, qWidgetEditor, qStyleOptionViewItem, qModelIndex) :
        qWidgetEditor.setGeometry(qStyleOptionViewItem.rect)



class DatasetAnnotationTableModel(QStandardItemModel) :
    
    def setSinlgePredictionCheckBox (self, singlePredictionCheckBox):
        self._singlePredictionCheckBox = singlePredictionCheckBox
        
    def setDatasetPredictionTags (self, projectDescriptions, projectDescriptionContext, RoiDictionary, modelRows, PredictionInitalization = None) :
        self._RoiDictionary = RoiDictionary
        self._predictionMap = {}
        
                        
        self._datasetTagManager = self._RoiDictionary.getDataFileTagManager ().copy ()
        projectDescriptionTag = Tag  (self._datasetTagManager._getID (),"Description","Description","Unknown", ParamaterList=projectDescriptions, Override_Tactic_Context = projectDescriptionContext)        
        self._datasetTagManager.insertTagAtFront (projectDescriptionTag)        
        self._noneTag = Tag  (self._datasetTagManager._getID (),"None","None","Unknown", Override_Tactic_Context = projectDescriptionContext)   
        self._NoneTagID = self._noneTag.getIdentifer()
        self._datasetTagManager.insertTagAtFront (self._noneTag)          
                     
        for predictionIndex in range (modelRows) :            
            self._predictionMap[predictionIndex] = [0.50, self._NoneTagID, None, None]
            if PredictionInitalization is not None and predictionIndex in PredictionInitalization :
                predictionClass = self._predictionMap[predictionIndex]
                predictionSetting = PredictionInitalization[predictionIndex]
                if (predictionSetting is not None) :
                    if "TagDefTuple" in predictionSetting :
                        tagDef = predictionSetting["TagDefTuple"]
                        tag = Tag.createFromDef (tagDef)
                        existingTag = self._datasetTagManager.getTagByIdentifer (tag.getIdentifer ())
                        if existingTag is None :
                            self._datasetTagManager.replaceTagOrAddByMatchingIdentifier (tag)
                        else:
                            tag = existingTag
                        indentifer = tag.getIdentifer ()
                        """
                        tag = self._datasetTagManager.getTagByIdentifer (indentifer)"""
                        
                        if (tag is not None) :
                            predictionClass[1] = indentifer
                            if "TrueThreshold" in predictionSetting :
                                predictionClass[0] = predictionSetting["TrueThreshold"]
                            if "FalseValue" in predictionSetting :
                                predictionClass[3] = predictionSetting["FalseValue"]
                            if "TrueValue" in predictionSetting :
                                predictionClass[2] = predictionSetting["TrueValue"]
                            if "TagDefTuple" in predictionSetting :
                                tagDef = predictionSetting["TagDefTuple"]
                                tag = Tag.createFromDef (tagDef)
                                predictionClass[1] = tag.getIdentifer ()
        self.updateSingePredictionCheckboxWidget ()            
        
    def initColumnIndexs (self, SetTagPredictionThreshold) :
        self._setTagPredictionThreshold = SetTagPredictionThreshold
        if (not SetTagPredictionThreshold) :
            self._predictionTagThreasholdIndex = -1
            self._predictionTagIndex = 1
            self._predictionTrueTagIndex = 2 
            self._predictionFalseTagIndex = -1
        else:
            self._predictionTagThreasholdIndex = 1
            self._predictionTagIndex = 2
            self._predictionTrueTagIndex = 3
            self._predictionFalseTagIndex = 4
    
    def getPredictionMap (self, tagManager):
        prediction = {}
        for index in range (len (self._predictionMap)) :
            currentData = self._predictionMap[index]
            if (currentData[1] == self._noneTag.getIdentifer ()) :
                prediction[index] = None
            else:
                prediction[index] = {}                          
                tag = tagManager.getTagByIdentifer (currentData [1])                
                prediction[index]["TagDefTuple"] = tag.getTagDefTuple ()
                prediction[index]["TrueValue"] =currentData [2] 
                if (self._setTagPredictionThreshold) :
                    prediction[index]["TrueThreshold"] =currentData [0] 
                    prediction[index]["FalseValue"] =currentData [3]             
        return ML_PredictionMap (prediction)
    
    def getPredictionCount (self) :
        return self.rowCount ()    
    
    def getAnnotationTagManager (self) :
        return self._datasetTagManager

    def getPredictionMapIndex (self, index) :
        return self._predictionMap[index]
    
    def getTableModelData (self, row, column) :
        currentData = self._predictionMap[row]
        if column == self._predictionTagThreasholdIndex : 
             return str (currentData[0])
        elif column == self._predictionTagIndex : 
             tagnameID = currentData[1]
             tag = self._datasetTagManager.getTagByIdentifer (tagnameID)
             return tag.getName ()
        elif column == self._predictionTrueTagIndex : 
             if currentData[2] is None :
                 return ""
             return str (currentData[2])
        elif column == self._predictionFalseTagIndex : 
             if currentData[3] is None :
                 return ""
             return str (currentData[3])
    
    def _setTagValues (self, trueValue, falseValue, PredictionRowIndex, currentData, tagType) :
        trueValueSet = False
        falseValueSet = False
        self._predictionMap[PredictionRowIndex] = currentData
        if (trueValue != currentData[2]):
            currentData[2] = trueValue      
            trueValueSet = True
        if (falseValue != currentData[3]):
            currentData[3] = falseValue
            falseValueSet = True
        if (trueValueSet):
            if (self._predictionTrueTagIndex != -1):
                table_index = self.index(PredictionRowIndex,self._predictionTrueTagIndex)
                if tagType != "Checked" :
                    self.setData (table_index, str(trueValue), QtCore.Qt.EditRole)    
                else:
                    if (trueValue) :
                        self.setData (table_index, "Yes", QtCore.Qt.EditRole)      
                    else:
                        self.setData (table_index, "No", QtCore.Qt.EditRole)      
            
        if (falseValueSet):
            if (self._predictionFalseTagIndex != -1): 
                table_index = self.index(PredictionRowIndex,self._predictionFalseTagIndex)
                if tagType != "Checked" :
                    self.setData (table_index, str(falseValue), QtCore.Qt.EditRole) 
                else:
                    if (falseValue) :
                        self.setData (table_index, "Yes", QtCore.Qt.EditRole)      
                    else:
                        self.setData (table_index, "No", QtCore.Qt.EditRole)   
          
   
        
    def initTableModelTagData (self, PredictionRowIndex, tagnameID) :
        currentData = self._predictionMap[PredictionRowIndex]
        trueValue = ""
        falseValue = ""
        currentData[1] = tagnameID  
        tagManager = self.getAnnotationTagManager ()
        tag = tagManager.getTagByIdentifer (tagnameID)
        tagType = tag.getType ()
        if (tagnameID != self._NoneTagID) :
                if tagType == "Checked"  : 
                    defaultValue = tag.getDefaultValue ()
                    defaultValue =  defaultValue == "Yes"
                    trueValue = not defaultValue
                    falseValue = defaultValue
                elif tagType == "Text"  : 
                    trueValue = ""
                    falseValue = tag.getDefaultValue ()
                elif tagType == "ItemList" or tagType == "Description":
                    trueValue = tag.getDefaultValue ()
                    for value in tag.getParameterList ():
                        if (value != trueValue) :
                            trueValue = value
                            break  
                    falseValue = tag.getDefaultValue ()
        self._setTagValues (trueValue, falseValue, PredictionRowIndex, currentData, tagType)    
        
    def updateSingePredictionCheckboxWidget (self) :
        singlePrediction = True
        currentName = None
        for currentData in self._predictionMap.values ():
            if (currentData is not None) :
                tagID = currentData[1]        
                tag = self._datasetTagManager.getTagByIdentifer (tagID)
                if (tag.getType () != "None") :
                    name = tag.getName ()
                    if currentName is None :
                        currentName = name
                    elif currentName != name :
                        singlePrediction = False
                        break
        if (singlePrediction) :
            self._singlePredictionCheckBox.setChecked (True)
            self._singlePredictionCheckBox.setEnabled (False)
        else:
            self._singlePredictionCheckBox.setEnabled (True)        
        
        
    def setTableModelData (self, PredictionRowIndex, threshold, tagnameID, trueValue, falseValue, UpdateTableModelData = True) :
        currentData = self._predictionMap[PredictionRowIndex]
        if (threshold != currentData[0]):
            table_index = self.index(PredictionRowIndex,self._predictionTagThreasholdIndex)
            currentData[0] = threshold
            self.setData (table_index, threshold, QtCore.Qt.EditRole, SetModelData = UpdateTableModelData)    
            
        
        table_index = self.index(PredictionRowIndex,self._predictionTagIndex)
        tag = self._datasetTagManager.getTagByIdentifer (tagnameID)
        currentTagName = self.data (table_index, QtCore.Qt.EditRole)    
        tagManager = self.getAnnotationTagManager ()
        tag = tagManager.getTagByIdentifer (tagnameID)
        tagType = tag.getType ()
        if (tagnameID != currentData[1] or tag.getName () != currentTagName):
            self.setData (table_index, tag.getName (), QtCore.Qt.EditRole, SetModelData = UpdateTableModelData)    
            currentData[1] = tagnameID
            if (tagnameID == self._NoneTagID) :
                trueValue = ""
                falseValue = ""
            else:
                if tagType == "Checked"  : 
                    defaultValue = tag.getDefaultValue ()
                    defaultValue =  defaultValue == "Yes"
                    trueValue = not defaultValue
                    falseValue = defaultValue
                elif tagType == "Text"  : 
                    trueValue = ""
                    falseValue = tag.getDefaultValue ()
                elif tagType == "ItemList" or tagType == "Description":
                    trueValue = tag.getDefaultValue ()
                    for value in tag.getParameterList ():
                        if (value != trueValue) :
                            trueValue = value
                            break  
                    falseValue = tag.getDefaultValue ()
        
        self._setTagValues (trueValue, falseValue, PredictionRowIndex, currentData, tagType)               
        self.updateSingePredictionCheckboxWidget ()
      
    
    def setData(self, modelIndex, value, role, SetModelData = True):    
        if (role == QtCore.Qt.CheckStateRole):           
            column = modelIndex.column () 
            if (self._predictionTrueTagIndex == column or self._predictionFalseTagIndex == column) :
                PredictionRowIndex = modelIndex.row ()
                currentData = self._predictionMap[PredictionRowIndex]
                tagID = currentData[1]
                tagManager = self.getAnnotationTagManager ()
                tag = tagManager.getTagByIdentifer (tagID)
                if tag.getType () == "Checked"  :      
                    currentData[2] = not currentData[2]
                    currentData[3] = not currentData[3]
                    if (self._predictionTrueTagIndex != -1) :
                        mIndex = self.index(PredictionRowIndex,self._predictionTrueTagIndex)                        
                        if (currentData[2]) :                 
                            QStandardItemModel.setData (self, mIndex,"Yes", QtCore.Qt.EditRole)   
                        else:
                            QStandardItemModel.setData (self, mIndex,"No", QtCore.Qt.EditRole)   
                    
                    if (self._predictionFalseTagIndex != -1) :
                        mIndex = self.index(PredictionRowIndex,self._predictionFalseTagIndex)
                        if (currentData[3]) :                 
                            QStandardItemModel.setData (self, mIndex,"Yes", QtCore.Qt.EditRole)   
                        else:
                            QStandardItemModel.setData (self, mIndex,"No", QtCore.Qt.EditRole)                                                        
        if (not SetModelData):
            return None
        return  QStandardItemModel.setData (self, modelIndex, value, role)     
    
    def data(self, modelIndex, role) :            
        if (role == QtCore.Qt.CheckStateRole) : 
            column = modelIndex.column () 
            if (self._predictionTrueTagIndex == column or self._predictionFalseTagIndex == column) :
                currentData = self._predictionMap[modelIndex.row () ]
                tagManager = self.getAnnotationTagManager ()
                tagID = currentData[1]
                tag = tagManager.getTagByIdentifer (tagID)
                if tag.getType () == "Checked"  : 
                    if (self._predictionTrueTagIndex == column):
                        dataColumn = 2 
                    else:
                        dataColumn = 3
                    value = currentData[dataColumn]
                    if (value) :
                        return QtCore.Qt.Checked                
                    else:
                        return QtCore.Qt.Unchecked
            return None
        return QStandardItemModel.data(self, modelIndex, role)        
    
    def flags (self, qModelIndex) :
        column = qModelIndex.column ()
        if (column == 0) :
            return  QtCore.Qt.ItemIsEnabled     
        else:
            if column == self._predictionTagIndex or column == self._predictionTagThreasholdIndex :
                return  QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsEditable 
            else:
                row = qModelIndex.row ()
                currentData = self._predictionMap[row]
                tagID = currentData[1]
                if tagID == self._NoneTagID :
                    return  QtCore.Qt.ItemIsEnabled
                else:
                    tagManager = self.getAnnotationTagManager ()
                    tag = tagManager.getTagByIdentifer (tagID)
                    if tag.getType () == "Checked"  : 
                        return QtCore.Qt.ItemIsUserCheckable | QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled
                    else:
                        return QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsEditable 
           


#-----------------------------------------------------------
#ROI Mapping Table
#-----------------------------------------------------------
        
class ROIMappingListDelegate (QItemDelegate) :    
    def __init__ (self, PredictionIndexMap , RoiDefs, tableModel, parent) :
        QItemDelegate.__init__ (self, parent)                  
        self._tableModel = tableModel
        self._PredictionIndexMap = PredictionIndexMap
        self._RoiDefs = RoiDefs
        
        self._RoiDefNameList = ["None"]         
        self._ColorList = [QtGui.QColor(0,0,0)]
        self._currentwidget = None
        self._UIDList = ["None"]

        self._ROIDefsNotInProject = tableModel.getROIDefsNotInProject ()                
        for roi in self._ROIDefsNotInProject :     
            name = roi.getName ()
            self._RoiDefNameList.append (roi.getName ())
            self._ColorList.append (roi.getQColor ())
            self._UIDList.append (roi.getNameUID (name))
        
        nameList = RoiDefs.getNameLst ()                                           
        self._RoiDefNameList += nameList                                     
        for name in nameList  :           
            color = RoiDefs.getROIColor (name)
            self._ColorList.append (color)
            self._UIDList.append (RoiDefs.getROINameUID (name))
            
    def createEditor(self, parent, options, qModelIndex) :       
        editor = QComboBox (parent)
        editor.setAutoFillBackground(True)                
        for name in self._RoiDefNameList :
            editor.addItem (str (name))                         
        for index , color in enumerate(self._ColorList) : 
            editor.setItemData(index, color, QtCore.Qt.TextColorRole)            
        PredictionIndex, uid = self._tableModel.data(qModelIndex, QtCore.Qt.UserRole)             
        if uid in self._UIDList :
            index = self._UIDList.index (uid)        
        else:
            index = 0
        editor.setCurrentIndex(index) 
        editor.currentIndexChanged.connect (self._widgetChanged)
        self._currentwidget = editor
        self._PredictionIndex = PredictionIndex
        return editor
    
    def setEditorData(self, qWidgetEditor, qModelIndex) :
        # Get the value via index of the Model
        try :
            qWidgetEditor.currentIndexChanged.disconnect (self._widgetChanged)
            reconnect = True
        except:
            reconnect = False
        _,uid  = qModelIndex.model().data(qModelIndex, QtCore.Qt.UserRole)
        # Put the value into the SpinBox
        if uid in self._UIDList :
            index = self._UIDList.index (uid)  
        else:
            index = 0
        if (index != qWidgetEditor.currentIndex ()) :
            qWidgetEditor.setCurrentIndex(index) 
        if (reconnect) :
            qWidgetEditor.currentIndexChanged.connect (self._widgetChanged)
        #self._defaultV = value
   
    def _widgetChanged (self, param = None) :     
        #self.setModelData (self._currentwidget, self._tableModel, self._tableModel.createIndex (self._currentRow, self._currentColumn, self._internalPtr))
        currentIndex = self._currentwidget.currentIndex()                
        self._PredictionIndexMap[self._PredictionIndex][1] = self._currentwidget.itemText (currentIndex)
        self._PredictionIndexMap[self._PredictionIndex][2] = self._UIDList[currentIndex]
        
        
    def setModelData(self,  qWidgetEditor, qAbstractModel, qModelIndex) :        
        currentIndex = qWidgetEditor.currentIndex()                
        PredictionIndex, _ = self._tableModel.data(qModelIndex, QtCore.Qt.UserRole)
        value = qWidgetEditor.itemText (currentIndex)
        uid = self._UIDList [currentIndex]
        self._PredictionIndexMap[PredictionIndex][1] = value
        self._PredictionIndexMap[PredictionIndex][2] = uid
        qAbstractModel.setData(qModelIndex, value, QtCore.Qt.EditRole)
        qAbstractModel.setData(qModelIndex, (PredictionIndex, uid), QtCore.Qt.UserRole)
                
    def updateEditorGeometry(self, qWidgetEditor, qStyleOptionViewItem, qModelIndex) :
        qWidgetEditor.setGeometry(qStyleOptionViewItem.rect)
        

class ROIThresholdTextDelegate (QItemDelegate) :    
    def __init__ (self, PredictionIndexMap , RoiDefs, tableModel, parent) :         
         QItemDelegate.__init__ (self, parent)         
         self._tableModel = tableModel
         self._PredictionIndexMap = PredictionIndexMap
         self._currentwidget = None
         self._currentRow = None
         self._internalPtr = None
         self._currentColumn = None
         
    def createEditor(self, parent, optiosn, qModelIndex) :       
        editor = QLineEdit (parent)               
        predictionIndex, _ = self._tableModel.data(qModelIndex, QtCore.Qt.UserRole)
        editor.setText (str(self._PredictionIndexMap[predictionIndex][0]))
        editor.setAutoFillBackground(True)        
        self._currentwidget = editor
        self._PredictionIndex = predictionIndex
        editor.textChanged.connect (self._widgetChanged)
        return editor        
    
    def setEditorData(self, qWidgetEditor, qModelIndex) :
        # Get the value via index of the Model
        try :
            qWidgetEditor.textChanged.disconnect (self._widgetChanged)
            reconnect = True
        except:
            reconnect = False
        value = qModelIndex.model().data(qModelIndex, QtCore.Qt.EditRole)        
        value = str (value)
        if (value !=  qWidgetEditor.text ()) :
            qWidgetEditor.setText(value)
        if reconnect : 
            qWidgetEditor.textChanged.connect (self._widgetChanged)
        #self._defaultV = value
     
    def _widgetChanged (self, param = None) :
        value = self._currentwidget.text()
        try :
            value = float (value)
        except:
            return
        self._PredictionIndexMap[self._PredictionIndex][0] = value
       
        
    def setModelData(self,  qWidgetEditor, qAbstractModel, qModelIndex) :        
        value = qWidgetEditor.text()
        try :
            value = float (value)
        except:
            return
        PredictionIndex, _ = self._tableModel.data(qModelIndex, QtCore.Qt.UserRole)
        self._PredictionIndexMap[PredictionIndex][0] = value
        qAbstractModel.setData(qModelIndex, str(value), QtCore.Qt.EditRole)
     
    
    def updateEditorGeometry(self, qWidgetEditor, qStyleOptionViewItem, qModelIndex) :
        qWidgetEditor.setGeometry(qStyleOptionViewItem.rect)
        


    
    
class ROISegmentationTableModel (QStandardItemModel) :           

    def getInternalPredictionMap (self, SegmentationInitalization = None, CurrentROIDefs = None) :
        if SegmentationInitalization is not None :
            segmentationTree = SegmentationInitalization.getTree ()
            for key in segmentationTree.keys () :
                if (segmentationTree[key] is not None) :
                    if (segmentationTree[key]["Threshold"] is None) :
                       threshold = 0.5
                    else:
                       threshold = segmentationTree[key]["Threshold"]
                    roiDef = segmentationTree[key]["ROIDef"]
                    rBlank = ROIDef ()
                    rBlank.setDef (roiDef)                    
                    name = CurrentROIDefs.findBestMatchingROIName (rBlank)                    
                    if name is not None and name != 'None' :
                        uid = CurrentROIDefs.getROINameUID (name)
                        self._predictionMap[key] = [threshold, name, uid]
                    else:          
                        name = rBlank.getName ()
                        uid  = rBlank.getNameUID (name)                        
                        self._predictionMap[key] = [threshold, name, uid]
                        self._ROIDefsNotInProject.append (rBlank)
        return self._predictionMap
    
    def getPredictionCount (self) :
        return self.rowCount ()
    
    def arePredictionThresholdSet (self) :
        return self._setPredictionThreshold
   
    def setPredictionThreshold (self, val) :
        self._setPredictionThreshold = val
    
    def getPredictionMap (self, ROIDefs):
        prediction = {}
        for index in range (len (self._predictionMap)) :
            roiUID = self._predictionMap[index][2]
            if roiUID == "None" :
                prediction[index] = None
            else:
                prediction[index] = {}            
                if (self._setPredictionThreshold) :
                    prediction[index]["Threshold"] = self._predictionMap[index][0]
                else:
                    prediction[index]["Threshold"] = None                            
                                                
                Definition = ROIDefs.getROIDefCopyByUID (roiUID)
                if Definition is None : 
                    if self._SegmentationInitalization is not None :
                        segmentationTree = self._SegmentationInitalization.getTree ()                        
                        for key in segmentationTree.keys () :
                            if (segmentationTree[key] is not None) :                            
                                roiDef = segmentationTree[key]["ROIDef"]  
                                rBlank = ROIDef ()
                                rBlank.setDef (roiDef) 
                                name = rBlank.getName ()
                                uid  = rBlank.getNameUID (name)
                                if uid == roiUID :
                                    Definition = rBlank
                                    break
                if (Definition is None) :
                    prediction[index] = None
                else:
                    prediction[index]["ROIDef"]  = Definition.getDef()
        return ML_PredictionMap (prediction)
    
    def getROIDefsNotInProject (self) :
        return self._ROIDefsNotInProject
    
    def setROIDefs (self, roiDefs, modelRows, SegmentationInitalization = None) :
        self._RoiDefs = roiDefs
        self._predictionMap = {}
        self._SegmentationInitalization = SegmentationInitalization
        self._fileROIDefs = {}
        self._ROIDefsNotInProject = []
        ROIMaskTable = {}
        if SegmentationInitalization is not None :
            segmentationTree = SegmentationInitalization.getTree ()
            for key in segmentationTree.keys () :
                if (segmentationTree[key] is not None) :
                    if (segmentationTree[key]["Threshold"] is None) :
                       threshold = 0.5
                    else:
                       threshold = segmentationTree[key]["Threshold"]
                    roiDef = segmentationTree[key]["ROIDef"]        
                    rBlank = ROIDef ()
                    rBlank.setDef (roiDef) 
                    name = rBlank.getName ()
                    uid  = rBlank.getNameUID (name)
                    #mv   = rBlank.getMask ()
                    ROIMaskTable[key] = (0.5, name, uid)
                    self._fileROIDefs[uid] = rBlank
                    
        ROINameList = roiDefs.getSortedNameLst ()
        if SegmentationInitalization is None:
            for name in ROINameList :
                uid = roiDefs.getROINameUID (name)
                mv = roiDefs.getROIMaskValue (name)
                if mv not in ROIMaskTable :
                    ROIMaskTable[mv] = (0.5, name, uid)
                    
        for predictionIndex in range (modelRows) :
            if predictionIndex not in self._predictionMap :         
                if (predictionIndex in ROIMaskTable) :
                    threshold, name, uid = ROIMaskTable[predictionIndex]
                    self._predictionMap[predictionIndex] = [threshold, name, uid]
                else:
                    self._predictionMap[predictionIndex] = [0.50, "None", "None"]
                                    
    def flags (self, qModelIndex) :  
        if (qModelIndex.column () == 0) :
            return   QtCore.Qt.ItemIsEnabled 
        return  QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsEditable         

    def data(self, index, role):
        if not index.isValid():
            return None
        
        if (self._setPredictionThreshold) :
            colorColumnIndex = 2 
        else:
            colorColumnIndex = 1
        if role == QtCore.Qt.ForegroundRole and index.column () == colorColumnIndex:                        
            _, uid = self.data(index, QtCore.Qt.UserRole)         
            if uid in self._fileROIDefs :
                return self._fileROIDefs[uid].getQColor ()
            else:
                roiName = self.data(index, QtCore.Qt.EditRole)         
                return self._RoiDefs.getROIColor (roiName)            
        return QStandardItemModel.data (self, index, role)

#-----------------------------------------------------------
#ROI Mapping Table
#-----------------------------------------------------------



class RCC_ModelDefDlg (QDialog) :
    def __init__ (self, ProjectDataset, NiftiVolume, selectedSlice , ROIDictionary, contourWindow, parent) :     
        QDialog.__init__ (self, parent)   
        self._showModelOrientation = ""
        self._DoesModelSupportApplicationOnNIfTI = False
        self._lastChannelCount =  None
        self._firstChannelCount = None
        self._roiTableModel  =  None   
        self._classificationTableModel = None
        self._ImageTransformation = np.eye (2, dtype= np.int)
        self._ROIDictionary = ROIDictionary
        self._parent = parent
        self._ProjectDataset = ProjectDataset
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)
        self.ui = Ui_RCC_ML_ModelDefDlg ()        
        self.ui.setupUi (self)        
        self.ui.customClassMethodsTxt.setPlainText (self._getAdpativeDiceExamplePythonCode ())       
        self.ui.FillValueTextBox.setText ("0")
        self._resizeWidgetHelper = ResizeWidgetHelper ()        
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.line)
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.ML_ToolBox)
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.modelNameEdit)
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.modelPathEdit)
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.NIfTI_Slice_Preview)
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.Normalization_NoneBtn)
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.Normalization_fixedMeanSDBtn)
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.Normalization_sliceMeanSDBtn)
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.Normalization_volumeMeanSDBtn)
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.Normalization_customCodeBtn)
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.customCodeTextEdit)
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.classificationModelBtn)
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.segmentationModelBtn)
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.predictionTable)        
        self._resizeWidgetHelper.saveWidgetUIPos   (self, self.ui.modelVersionTxt)            
        self._resizeWidgetHelper.saveWidgetUIPos   (self, self.ui.modelDescriptionTxt)            
        self._resizeWidgetHelper.saveWidgetUIPos   (self, self.ui.modelTreePath)            
        self._resizeWidgetHelper.saveWidgetUIPos   (self, self.ui.modelInputSizeLabel)            
        self._resizeWidgetHelper.saveWidgetUIPos   (self, self.ui.modelOutputSizeLabel)     
        
        self._resizeWidgetHelper.saveWidgetUIPos   (self, self.ui.AxialOrientationCheckBox)     
        self._resizeWidgetHelper.saveWidgetUIPos   (self, self.ui.CoronalOrientationCheckBox)     
        self._resizeWidgetHelper.saveWidgetUIPos   (self, self.ui.SagittalOrientationCheckBox)     
        self._resizeWidgetHelper.saveWidgetUIPos   (self, self.ui.OrientationLabel)     
        
        self._resizeWidgetHelper.saveWidgetUIPos   (self, self.ui.modelTypeGroupBox)     
        self._resizeWidgetHelper.saveWidgetUIPos   (self, self.ui.PredictionTabWidget)     
        self._resizeWidgetHelper.saveWidgetUIPos   (self, self.ui.ImplicitlySelectAllSlicesCheckBox)     
        self._resizeWidgetHelper.saveWidgetUIPos   (self, self.ui.CreateUndefinedROICheckBox)
        self._resizeWidgetHelper.saveWidgetUIPos   (self, self.ui.FilterROIKeepOnlyLargest3dIsland)
        self._resizeWidgetHelper.saveWidgetUIPos   (self, self.ui.ApplyModelToGroupBox)
        self._resizeWidgetHelper.saveWidgetUIPos   (self, self.ui.ApplyToAllSelectedSlices)
        self._resizeWidgetHelper.saveWidgetUIPos   (self, self.ui.ApplyToAllSelectedSlicesWithData)
        self._resizeWidgetHelper.saveWidgetUIPos   (self, self.ui.ApplyToAllSelectedSlicesWithoutData)
        
        self._resizeWidgetHelper.saveWidgetUIPos   (self, self.ui.ExistingDataGroupBox)
        self._resizeWidgetHelper.saveWidgetUIPos   (self, self.ui.DoNotAlterExistingDataBtn)
        self._resizeWidgetHelper.saveWidgetUIPos   (self, self.ui.ReoveExistingDataFromSliceBtn)
        self._resizeWidgetHelper.saveWidgetUIPos   (self, self.ui.RemoveAllExistingDataBtn)
        self._resizeWidgetHelper.saveWidgetUIPos   (self, self.ui.AddToExistingDataBtn)
    
        self._resizeWidgetHelper.saveWidgetUIPos   (self, self.ui.multiSliceVotingComboBox)
        self._resizeWidgetHelper.saveWidgetUIPos   (self, self.ui.SetDescriptionPrediction)
        self._resizeWidgetHelper.saveWidgetUIPos   (self, self.ui.ExistingDataGroupBox_Classification)
        self._resizeWidgetHelper.saveWidgetUIPos   (self, self.ui.DoNotAlterExistingDataBtnClassification)
        self._resizeWidgetHelper.saveWidgetUIPos   (self, self.ui.OverwriteExistingDataBtn)
        self._resizeWidgetHelper.saveWidgetUIPos   (self, self.ui.PredictionTypeOptions)
        self._resizeWidgetHelper.saveWidgetUIPos   (self, self.ui.customClassMethodLabel)
        self._resizeWidgetHelper.saveWidgetUIPos   (self, self.ui.customClassMethodsTxt)
        
        self._resizeWidgetHelper.saveWidgetUIPos   (self, self.ui.isKerasModelOutputChannelLast)            
        self.ui.OkBtn.clicked.connect (self._OkBtn)
        self.ui.CancelBtn.clicked.connect (self._CancelBtn)        
        basepath = self._ProjectDataset.getPythonProjectBasePath ()
        iconPath = os.path.join (basepath , "icons")                
        self.ui.RotateLeftBtn.setIcon(QtGui.QIcon(iconPath + os.sep + "undo.png"))
        self.ui.RotateLeftBtn.setIconSize(QtCore.QSize(20,20))        
        self.ui.rotateRightBtn.setIcon(QtGui.QIcon(iconPath + os.sep + "redo.png"))
        self.ui.rotateRightBtn.setIconSize(QtCore.QSize(20,20))
        self.ui.HFlipBtn.setIcon(QtGui.QIcon(iconPath + os.sep + "HFlip.png"))
        self.ui.HFlipBtn.setIconSize(QtCore.QSize(20,20))
        self.ui.VFlipBtn.setIcon(QtGui.QIcon(iconPath + os.sep + "VFlip.png"))
        self.ui.VFlipBtn.setIconSize(QtCore.QSize(20,20))      
        self.ui.SwapAxisBtn.setIcon(QtGui.QIcon(iconPath + os.sep + "swapAxis.png"))
        self.ui.SwapAxisBtn.setIconSize(QtCore.QSize(20,20))   
        self.ui.ML_ToolBox.widget (0).resizeEvent = self._resizeModelDef
        self.ui.ML_ToolBox.widget (1).resizeEvent = self._resizeModelInputSelection
        self.ui.ML_ToolBox.widget (2).resizeEvent = self._resizeModelNormalization
        self.ui.ML_ToolBox.widget (3).resizeEvent = self._resizeModelPrediction  
        self.ui.ML_ToolBox.widget (4).resizeEvent = self._resizeModelDescription
        
        self.ui.selectKerasModelBtn.clicked.connect (self._selectDLModel)         
        self.ui.predictionTable.horizontalHeader().setStretchLastSection(True)
        self.ui.classificationModelBtn.setEnabled (False)
        self.ui.segmentationModelBtn.setEnabled (False)
        self.ui.multiSliceVotingLabel.setEnabled (False)
        self.ui.multiSliceVotingComboBox.setEnabled (False)
        self.ui.ML_ToolBox.setEnabled (True)
        self.ui.fixedMeanText.setEnabled (False)
        self.ui.fixedmeanlbl.setEnabled (False)
        self.ui.fixedSDLbl.setEnabled (False)
        self.ui.fixedSDText.setEnabled (False)
        self.ui.customCodeTextEdit.setEnabled (False)
        self.ui.isKerasModelOutputChannelLast.setEnabled (False)
        
        self.ui.windowLbl.setEnabled (False)
        self.ui.windowText.setEnabled (False)
        self.ui.minLbl.setEnabled (False)
        self.ui.maxLbl.setEnabled (False)
        self.ui.minText.setEnabled (False)
        self.ui.maxText.setEnabled (False)
        
        self.ui.windowText.editingFinished.connect (self._setMinMaxText)
        self.ui.levelText.editingFinished.connect (self._setMinMaxText)
        self.ui.minText.editingFinished.connect (self._setWindowLevel)
        self.ui.maxText.editingFinished.connect (self._setWindowLevel)
        
        self.ui.levelLbl.setEnabled (False)
        self.ui.levelText.setEnabled (False)
        self.ui.NormalizeChkBox.setEnabled (False)
        self.ui.LowNormLbl.setEnabled (False)
        self.ui.LowNormTxt.setEnabled (False)
        self.ui.HighNormLbl.setEnabled (False)
        self.ui.HighNormTxt.setEnabled (False)
        
        self.ui.settingsGroupBox.setEnabled (False)
        self.ui.SinglePredictBtn.setEnabled (False)
        self.ui.setPredictionProbThresholdBtn.setEnabled (False)
        
        self.ui.Normalization_fixedMeanSDBtn.toggled.connect (self.FixMeanSDNormalizationBtnToggle)
        self.ui.Normalization_customCodeBtn.toggled.connect (self.CustomNormalizationBtnToggle)
        self.ui.WindowLevelNormBtn.toggled.connect (self.WindowLevelNormalizationBtnToggle)
        self.ui.NormalizeChkBox.toggled.connect (self.NormalizationBtnToggle)
        
        self.ui.isKerasModelOutputChannelLast.setEnabled (False) 
        self.ui.isKerasModelOutputChannelLast.toggled.connect (self.outputChannelToggled)
        self._NiftiVolume = NiftiVolume
        self._SelectedSlice = selectedSlice
        
        self._setModelOrientation ()
        
        
        self.ui.ApplyToImagingBtn.toggled.connect (self._applyToModelToImaging)
        self.ui.classificationModelBtn.toggled.connect (self._setClassificationModel)
        self.ui.segmentationModelBtn.toggled.connect (self._setSegmentationModel)
        
        self.ui.NIfTI_Slice_Preview.setHounsfieldVisSettings (contourWindow.getHounsfieldSettingsCopy ())
        self.ui.NIfTI_Slice_Preview.setVisSettings (contourWindow.getVisSettingsCopy ())
        
        self.ui.AxialOrientationCheckBox.toggled.connect (self._setModelOrientation)
        self.ui.CoronalOrientationCheckBox.toggled.connect (self._setModelOrientation)
        self.ui.SagittalOrientationCheckBox.toggled.connect (self._setModelOrientation)
           
        self.ui.RotateLeftBtn.clicked.connect (self._rLeft)
        self.ui.rotateRightBtn.clicked.connect (self._rRight)
        self.ui.HFlipBtn.clicked.connect (self._hFlip)
        self.ui.VFlipBtn.clicked.connect (self._vFlip)
        self.ui.SwapAxisBtn.clicked.connect (self._swapAxis)

        self._resetCustomNormalizationCode ()
        
        self.ui.FillValueTextBox.setText ("0")
        self.ui.fixedMeanText.setText ("0")
        self.ui.fixedSDText.setText ("1")
        self.ui.setPredictionProbThresholdBtn.stateChanged.connect (self._predictionSetThresholdChanged)
        self.ui.resetCustomCodeTemplate.clicked.connect (self._resetCustomNormalizationCode)
        self.ui.resetCustomCodeTemplate.setEnabled (False)
        self._origionalKerasModelPath = ""
        self._currentKerasModelPath  = "" 
        
        date = DateUtil.today ()
        time = TimeUtil.getTimeNow ()
        self.ui.modelVersionTxt.setText (DateUtil.getSortableString (date) + "." + TimeUtil.timeToSortableString (time))
    

   
        
    def _setModelOrientation (self) :
        oldOrientation = self._showModelOrientation
        modelOrientation = []
        checkBoxlst = []
        if self.ui.AxialOrientationCheckBox.isChecked () :
            modelOrientation += ["Axial"]
            checkBoxlst.append (self.ui.AxialOrientationCheckBox)
        if self.ui.CoronalOrientationCheckBox.isChecked () :
            modelOrientation += ["Coronal"]
            checkBoxlst.append (self.ui.CoronalOrientationCheckBox)
        if self.ui.SagittalOrientationCheckBox.isChecked () :
            modelOrientation += ["Sagittal"]
            checkBoxlst.append (self.ui.SagittalOrientationCheckBox)
        if self._showModelOrientation not in modelOrientation :
            self._showModelOrientation = modelOrientation[0]        
        for box in [self.ui.SagittalOrientationCheckBox, self.ui.CoronalOrientationCheckBox, self.ui.AxialOrientationCheckBox] :
            box.setEnabled (True)
        if len (checkBoxlst) == 1 :
            checkBoxlst[0].setEnabled (False)
                    
        if oldOrientation != self._showModelOrientation :
            self._updateSlicePreviewToReflectAffine ()
        
   
    
    def _setMinMaxText (self) :
        try :
            self.ui.windowText.editingFinished.disconnect (self._setMinMaxText)
            connectSetWindowText = True
        except:
            connectSetWindowText = False 
        try:
            self.ui.levelText.editingFinished.disconnect (self._setMinMaxText)
            connectSetLevelText = True
        except:
            connectSetLevelText = False
        try :
            self.ui.minText.editingFinished.disconnect (self._setWindowLevel)
            connectSetMinText = True
        except:
            connectSetMinText = False
        try :
            self.ui.maxText.editingFinished.disconnect (self._setWindowLevel)
            connectSetMaxText = True
        except:
            connectSetMaxText = False
        try :
            window = int (self.ui.windowText.text ())
            if window < 1 :
                self.ui.windowText.setText (1) 
                window = 1
            level = int (self.ui.levelText.text ())
            window = int (self.ui.windowText.text ())
            minV = level - max (int (window/2), 0)
            self.ui.minText.setText (str (minV))
            self.ui.maxText.setText (str (minV + window))   
        except:
            self.ui.windowText.setText ("1000")
            self.ui.levelText.setText ("100")
            self.ui.minText.setText ("-400")
            self.ui.maxText.setText ("600")
        if connectSetWindowText :
            self.ui.windowText.editingFinished.connect (self._setMinMaxText)
        if connectSetLevelText : 
            self.ui.levelText.editingFinished.connect (self._setMinMaxText)
        if connectSetMinText :
            self.ui.minText.editingFinished.connect (self._setWindowLevel)
        if connectSetMaxText :
            self.ui.maxText.editingFinished.connect (self._setWindowLevel)
            
    def _setWindowLevel (self) :
        try :
            self.ui.windowText.editingFinished.disconnect (self._setMinMaxText)
            connectSetWindowText = True
        except:
            connectSetWindowText = False 
        try:
            self.ui.levelText.editingFinished.disconnect (self._setMinMaxText)
            connectSetLevelText = True
        except:
            connectSetLevelText = False
        try :
            self.ui.minText.editingFinished.disconnect (self._setWindowLevel)
            connectSetMinText = True
        except:
            connectSetMinText = False
        try :
            self.ui.maxText.editingFinished.disconnect (self._setWindowLevel)
            connectSetMaxText = True
        except:
            connectSetMaxText = False
        try :
            minText = int (self.ui.minText.text ())
            maxText = int (self.ui.maxText.text())
            if minText > maxText :
                minText, maxText = maxText, minText
                self.ui.minText.setText (str (minText))
                self.ui.maxText.setText (str (maxText))
            window = int (maxText - minText)
            level = int ((minText + maxText) / 2)
            self.ui.windowText.setText (str (window))
            self.ui.levelText.setText (str (level))   
        except:
            self.ui.windowText.setText ("1000")
            self.ui.levelText.setText ("100")
            self.ui.minText.setText ("-400")
            self.ui.maxText.setText ("600")
        if connectSetWindowText :
            self.ui.windowText.editingFinished.connect (self._setMinMaxText)
        if connectSetLevelText : 
            self.ui.levelText.editingFinished.connect (self._setMinMaxText)
        if connectSetMinText :
            self.ui.minText.editingFinished.connect (self._setWindowLevel)
        if connectSetMaxText :
            self.ui.maxText.editingFinished.connect (self._setWindowLevel)
        
    def _applyToModelToImaging    (self) :
         if (self.ui.ApplyToImagingBtn.isChecked ()) :
             self.ui.setPredictionProbThresholdBtn.setChecked (False)
             self.ui.SinglePredictBtn.setEnabled (False)
             self.ui.setPredictionProbThresholdBtn.setEnabled (False)
             self.ui.predictionTable.setEnabled (False)
         else:             
             self.ui.SinglePredictBtn.setEnabled (True)
             self.ui.setPredictionProbThresholdBtn.setEnabled (True)
             self.ui.predictionTable.setEnabled (True)
             
    def _resetCustomNormalizationCode (self) :
        plainTextLst = ["# Custom slice normalization function",
        "# Default implementation does nothing.",
        "# ",
        "# Parameters:",
        "#   SliceData = slice data to normalize",
        "# ",
        "def getNormalizedSlice (SliceData):",
        "    return SliceData"]
        self._default_custom_normalization_code = "\n".join (plainTextLst)
        self.ui.customCodeTextEdit.setPlainText (self._default_custom_normalization_code)
        

    def _setClassificationModel (self, toggle = False, PredictionInitalization = None) :
        self.ui.multiSliceVotingLabel.setEnabled (True)
        self.ui.multiSliceVotingComboBox.setEnabled (True)
        self.ui.ApplyToImagingBtn.setEnabled (False)
        self.ui.ApplyToImagingBtn.setChecked(False)
        self.ui.SinglePredictBtn.setText ("Predict one annotation")
        self.ui.PredictionTypeOptions.setCurrentIndex (0)
        ModelPredictionCount = self.getModelPredictionCount ()        
        if ((PredictionInitalization is not None) and (0 in PredictionInitalization.getTree ())) :
            predictionTree = PredictionInitalization.getTree ()                        
        else:
            predictionTree = None
            
        setPredictionThreshold = self.ui.setPredictionProbThresholdBtn.isChecked ()
        if (ModelPredictionCount is None or ModelPredictionCount <= 0) :
            return                
        
        datasetinitialized = False
        self._roiTableModel = None
        if (self._classificationTableModel is None or self._classificationTableModel.getPredictionCount () != ModelPredictionCount) :
            self._classificationTableModel = DatasetAnnotationTableModel (ModelPredictionCount, 5, self)
            self._classificationTableModel.setSinlgePredictionCheckBox (self.ui.SinglePredictBtn)
            datasetinitialized = True
        
        if (not setPredictionThreshold) :
            self._classificationTableModel.setColumnCount (3)
            self._classificationTableModel.setHorizontalHeaderLabels (["Model Output Index", "Tag", "True value"])        
        else:
            self._classificationTableModel.setColumnCount (5) 
            self._classificationTableModel.setHorizontalHeaderLabels (["Model Output Index", "Prediction Prob" , "Tag", "True value", "False value"])        
        
        if (datasetinitialized) : 
            self._classificationTableModel.setDatasetPredictionTags ( self._ProjectDataset.getScanPhaseList () , self._ProjectDataset.getDescriptionContext (), self._ROIDictionary, ModelPredictionCount, PredictionInitalization =predictionTree)
        elif (PredictionInitalization is not None) :
            self._classificationTableModel.setDatasetPredictionTags ( self._ProjectDataset.getScanPhaseList () , self._ProjectDataset.getDescriptionContext (), self._ROIDictionary, ModelPredictionCount, PredictionInitalization = predictionTree)
        
        self._classificationTableModel.initColumnIndexs (setPredictionThreshold)
        for predictionIndex in range (ModelPredictionCount) :                                        
            for columnIndex in range (self._classificationTableModel.columnCount()) :
                table_index = self._classificationTableModel.index(predictionIndex,columnIndex)
                if (columnIndex == 0) :
                    self._classificationTableModel.setData(table_index, str (predictionIndex), QtCore.Qt.DisplayRole)   
                else:    
                    self._classificationTableModel.setData(table_index, self._classificationTableModel.getTableModelData (predictionIndex, columnIndex), QtCore.Qt.EditRole)   
                self._classificationTableModel.setData(table_index, predictionIndex, QtCore.Qt.UserRole)         
                 
        self.ui.predictionTable.setModel (self._classificationTableModel)           
        self.ui.predictionTable.verticalHeader().hide ()       
        self.ui.predictionTable.setColumnWidth(0, 160)
        self.ui.predictionTable.setColumnWidth(1, 160)
        self.ui.predictionTable.setColumnWidth(2, 160)
        if (setPredictionThreshold) :
            self.ui.predictionTable.setColumnWidth(3, 160)
            self.ui.predictionTable.setColumnWidth(4, 160)
        self.ui.predictionTable.setSelectionBehavior( QAbstractItemView.SelectItems );
        self.ui.predictionTable.setSelectionMode( QAbstractItemView.SingleSelection );                    
        if (not setPredictionThreshold) :
            self.ui.predictionTable.setItemDelegateForColumn (1, TagDelegate ( "TagName",self._classificationTableModel, self.ui.predictionTable)) 
            self.ui.predictionTable.setItemDelegateForColumn (2, TagDelegate ( "TagTrueValue",self._classificationTableModel, self.ui.predictionTable)) 
        else:
            self.ui.predictionTable.setItemDelegateForColumn (1, TagDelegate ( "Threshold",self._classificationTableModel, self.ui.predictionTable)) 
            self.ui.predictionTable.setItemDelegateForColumn (2, TagDelegate ( "TagName",self._classificationTableModel, self.ui.predictionTable)) 
            self.ui.predictionTable.setItemDelegateForColumn (3, TagDelegate ( "TagTrueValue",self._classificationTableModel, self.ui.predictionTable)) 
            self.ui.predictionTable.setItemDelegateForColumn (4, TagDelegate ( "TagFalseValue",self._classificationTableModel, self.ui.predictionTable)) 
   
       
                        
            
    
    def _setSegmentationModel (self, Toggle = False, SegmentationInitalization = None) :   
        self.ui.multiSliceVotingLabel.setEnabled (False)
        self.ui.multiSliceVotingComboBox.setEnabled (False)
        self.ui.SinglePredictBtn.setText ("Predict one ROI per voxel")
        self.ui.PredictionTypeOptions.setCurrentIndex (1)
        ModelPredictionCount = self.getModelPredictionCount ()
                
        
        setPredictionThreshold = self.ui.setPredictionProbThresholdBtn.isChecked ()
        if (ModelPredictionCount is None or ModelPredictionCount <= 0) :
            return                       
        self._classificationTableModel = None
        if (self._roiTableModel is None or self._roiTableModel.getPredictionCount () != ModelPredictionCount) :
            self._roiTableModel = ROISegmentationTableModel (ModelPredictionCount, 3, self)
            self._roiTableModel.setROIDefs (self._ROIDictionary.getROIDefs (), ModelPredictionCount, SegmentationInitalization = SegmentationInitalization)
        self._roiTableModel.setPredictionThreshold (setPredictionThreshold)
        if (not setPredictionThreshold) :
            roitableIndex = 1
            self._roiTableModel.setColumnCount (2)
            self._roiTableModel.setHorizontalHeaderLabels (["Model Output Index", "ROI Name"])        
        else:
            roitableIndex = 2
            self._roiTableModel.setColumnCount (3)
            self._roiTableModel.setHorizontalHeaderLabels (["Model Output Index", "Prediction Prob","ROI Name"]) 
        internalPredictionMap = self._roiTableModel.getInternalPredictionMap (SegmentationInitalization = SegmentationInitalization, CurrentROIDefs = self._ROIDictionary.getROIDefs ())   

        for predictionIndex in range (ModelPredictionCount) :                                        
            table_index = self._roiTableModel.index(predictionIndex,0)
            self._roiTableModel.setData(table_index, str (predictionIndex), QtCore.Qt.DisplayRole    )                        
            
            if (setPredictionThreshold) :            
                table_index = self._roiTableModel.index(predictionIndex,1)
                self._roiTableModel.setData(table_index, str (internalPredictionMap[predictionIndex][0]), QtCore.Qt.EditRole)            
                self._roiTableModel.setData(table_index, (predictionIndex, internalPredictionMap[predictionIndex][2]), QtCore.Qt.UserRole)            
                        
            table_index = self._roiTableModel.index(predictionIndex,roitableIndex)
            self._roiTableModel.setData(table_index, str (internalPredictionMap[predictionIndex][1]), QtCore.Qt.EditRole)            
            self._roiTableModel.setData(table_index, (predictionIndex, internalPredictionMap[predictionIndex][2]), QtCore.Qt.UserRole)            
        
        if (self._DoesModelSupportApplicationOnNIfTI) :            
            self.ui.ApplyToImagingBtn.setEnabled (True)
            self.ui.ApplyToImagingBtn.setChecked (False)
                        
        self.ui.predictionTable.setModel (self._roiTableModel)           
        self.ui.predictionTable.verticalHeader().hide ()       
        self.ui.predictionTable.setColumnWidth(0, 160)
        self.ui.predictionTable.setColumnWidth(1, 160)
        if (setPredictionThreshold) :
            self.ui.predictionTable.setColumnWidth(2, 160)
        self.ui.predictionTable.setSelectionBehavior( QAbstractItemView.SelectItems );
        self.ui.predictionTable.setSelectionMode( QAbstractItemView.SingleSelection );                    
        if (setPredictionThreshold) :
            self.ui.predictionTable.setItemDelegateForColumn (1, ROIThresholdTextDelegate (self._roiTableModel.getInternalPredictionMap (), self._ROIDictionary.getROIDefs (), self._roiTableModel, self.ui.predictionTable))                                    
        self.ui.predictionTable.setItemDelegateForColumn (roitableIndex, ROIMappingListDelegate (self._roiTableModel.getInternalPredictionMap () ,self._ROIDictionary.getROIDefs (), self._roiTableModel, self.ui.predictionTable))              
        return
    
    def _predictionSetThresholdChanged (self) :
        self.outputChannelToggled ()
        
    def outputChannelToggled (self) :
        if self.ui.segmentationModelBtn.isChecked () :
            self._setSegmentationModel ()
        elif self.ui.classificationModelBtn.isChecked () :
            self._setClassificationModel ()
    
    def getModelDefinition (self) :
        return self._okModel 
    
    def _OkBtn (self) :
        self._okModel  = self._getModelDefinition ()
        self.accept ()
    
    def _CancelBtn (self) :    
        self.reject ()

        
    def _updateSlicePreviewToReflectAffine (self) :     
        SelectedSliceView, _ = ML_PredictionPreferedViewUtil.getPreferedSliceViewAndSliceSelection ([self._showModelOrientation], self._parent._parent)
        self._NiftiPreviewSlice = SelectedSliceView.getNIfTIVolumeSliceViewProjection (SliceIndex = SelectedSliceView.getSliceNumber ())
        
        cx, cy = ML_ModelInputTransformation.transformPoint (self._ImageTransformation, 2, 0, None, (5,9))
        SwapXYDim = not (cy != 0 and cy != 8)
        output = ML_ModelInputTransformation.transformImage (self._ImageTransformation ,self._NiftiPreviewSlice, Object_Memory_Cache = self)
        currentSliceVolume = np.zeros ((output.shape[1], output.shape[0], 1), dtype = self._NiftiPreviewSlice.dtype)        
        currentSliceVolume[:,:,0] = output.T
        niftiDataset = NiftiVolumeData.InitFromNumpyArray (currentSliceVolume, self._NiftiVolume, DataAxis = SelectedSliceView.getSliceAxis (), SwapXYDim = SwapXYDim)
        self.ui.NIfTI_Slice_Preview.setNiftiDataSet (niftiDataset, self._ProjectDataset)        
        coordinate = Coordinate ()
        coordinate.setCoordinate ([int (self._NiftiPreviewSlice.shape[0] / 2),int (self._NiftiPreviewSlice.shape[1]/ 2),0])        
        self.ui.NIfTI_Slice_Preview.initNiftiDataSet ( coordinate, "Z")
        self.ui.NIfTI_Slice_Preview.update ()
    
    def _rLeft (self) :                
        tMatrix = np.array ([[0,1],[-1,0]],dtype = np.int)
        self._ImageTransformation = np.matmul (tMatrix,self._ImageTransformation)             
        self._updateSlicePreviewToReflectAffine ()
               
        
    def _rRight (self) :                
        tMatrix = np.array ([[0,-1],[1,0]],dtype = np.int)
        self._ImageTransformation = np.matmul (tMatrix,self._ImageTransformation)
        self._updateSlicePreviewToReflectAffine ()
        
    def _vFlip (self) :        
        tMatrix = np.array ([[1,0],[0,-1]],dtype = np.int)
        self._ImageTransformation = np.matmul (tMatrix, self._ImageTransformation)
        self._updateSlicePreviewToReflectAffine ()
   
    def _swapAxis (self) :
        tMatrix = np.array ([[0,-1],[1,0]],dtype = np.int)
        self._ImageTransformation = np.matmul (tMatrix,self._ImageTransformation)
        tMatrix = np.array ([[-1,0],[0,1]],dtype = np.int)
        self._ImageTransformation = np.matmul (tMatrix, self._ImageTransformation)
        self._updateSlicePreviewToReflectAffine ()
        
    def _hFlip (self) :        
        tMatrix = np.array ([[-1,0],[0,1]],dtype = np.int)
        self._ImageTransformation = np.matmul (tMatrix, self._ImageTransformation)
        self._updateSlicePreviewToReflectAffine ()
                            
            
    def CustomNormalizationBtnToggle (self) :
        self.ui.customCodeTextEdit.setEnabled (self.ui.Normalization_customCodeBtn.isChecked ())
        self.ui.resetCustomCodeTemplate.setEnabled (self.ui.Normalization_customCodeBtn.isChecked ())
    
    def WindowLevelNormalizationBtnToggle (self):
        btnChecked = self.ui.WindowLevelNormBtn.isChecked ()
        self.ui.windowLbl.setEnabled (btnChecked)
        self.ui.windowText.setEnabled (btnChecked)
        self.ui.minLbl.setEnabled (btnChecked)
        self.ui.maxLbl.setEnabled (btnChecked)
        self.ui.minText.setEnabled (btnChecked)
        self.ui.maxText.setEnabled (btnChecked)
        self.ui.levelLbl.setEnabled (btnChecked)
        self.ui.levelText.setEnabled (btnChecked)
        self.ui.NormalizeChkBox.setEnabled (btnChecked)
        
    def NormalizationBtnToggle (self):
        btnChecked = self.ui.NormalizeChkBox.isChecked ()
        self.ui.LowNormLbl.setEnabled (btnChecked)
        self.ui.LowNormTxt.setEnabled (btnChecked)
        self.ui.HighNormLbl.setEnabled (btnChecked)
        self.ui.HighNormTxt.setEnabled (btnChecked)
        
    def FixMeanSDNormalizationBtnToggle (self) :
        btnChecked = self.ui.Normalization_fixedMeanSDBtn.isChecked ()
        self.ui.fixedMeanText.setEnabled (btnChecked)
        self.ui.fixedmeanlbl.setEnabled (btnChecked)
        self.ui.fixedSDLbl.setEnabled (btnChecked)
        self.ui.fixedSDText.setEnabled (btnChecked)
        
    def _selectDLModel (self) :
        dlg= QFileDialog( self )
        dlg.setWindowTitle( 'Select trained Keras model to import' )
        dlg.setViewMode( QFileDialog.Detail )    
        dlg.setFileMode (QFileDialog.ExistingFile)
        dlg.setNameFilters( [self.tr('HDF5 (*.hdf5)'), self.tr('All Files (*)')] )
        dlg.setDefaultSuffix( 'hdf5' )
        dlg.setAcceptMode (QFileDialog.AcceptOpen)
        if dlg.exec_() and (len (dlg.selectedFiles())== 1):
            path = dlg.selectedFiles()[0]                                    
            self.ui.modelPathEdit.setText (path)   
            self._loadModelPredictionCount (path)
            self._origionalKerasModelPath = path
            self._currentKerasModelPath  = path
            ModelPredictionCount = self.getModelPredictionCount ()
            if ModelPredictionCount is not None and ModelPredictionCount > 0 :                
                self.ui.modelPathEdit.setText (path)   
                self.outputChannelToggled ()
                self.ui.SinglePredictBtn.setChecked (True)
                return
        self.ui.classificationModelBtn.setEnabled (False)
        self.ui.segmentationModelBtn.setEnabled (False)
        self.ui.segmentationModelBtn.setChecked (False)
        self.ui.classificationModelBtn.setChecked (False)
        self.ui.settingsGroupBox.setEnabled (False)
        self.ui.SinglePredictBtn.setEnabled (False)
        self.ui.setPredictionProbThresholdBtn.setEnabled (False)
        self.ui.multiSliceVotingLabel.setEnabled (False)
        self.ui.multiSliceVotingComboBox.setEnabled (False)
        self.ui.ApplyToImagingBtn.setEnabled(True)
        self.ui.ApplyToImagingBtn.setChecked(False)
        self.outputChannelToggled ()

    def getModelPredictionCount (self) :
        if self.ui.isKerasModelOutputChannelLast.isChecked () :
            return self._lastChannelCount
        else:
            return self._firstChannelCount
        
    def _loadModelPredictionCount (self, path = None, model = None) :
        try :
            self._lastChannelCount =  None
            self._firstChannelCount = None
            if model is not None :
                ioDescription = model.getModelInputOutputDescription ()
                input_shape = ioDescription.getInputDataSize ()
                output_shape = ioDescription.getOutputDataSize ()
            else:
                if path == None :
                    path = self._currentKerasModelPath
                ModelLoadExceptionList = []
                try :
                    custom_objects = self._ProjectDataset.getKerasModelLoader ().buildCustomKerasObjects (self.ui.customClassMethodsTxt.toPlainText ())
                    
                    if len (custom_objects) > 0 :
                        loadedModel = self._ProjectDataset.getKerasModelLoader().getOrigionalModel (path,  Custom_Objects=custom_objects, ModelLoadExceptionList = ModelLoadExceptionList)
                    else:
                        loadedModel = self._ProjectDataset.getKerasModelLoader().getOrigionalModel (path, ModelLoadExceptionList = ModelLoadExceptionList)
                except Exception as e_message :
                    if len (ModelLoadExceptionList) > 0 :
                        ErrorListDlg.showErrorListDlg (None, "Error loading keras model", "Error loading keras model", ModelLoadExceptionList)                       
                    else:
                        MessageBoxUtil.showMessage (WindowTitle="Error loading keras model",WindowMessage="A error occured load the keras model. Make sure keras and backend (e.g., Tensorflow) packages are up-to-date.\n\nMessage: " +str (e_message))
                        print ("A error occured trying to load the keras model.")
                        print ("")
                    raise 
                input_shape = loadedModel.input_shape
                output_shape = loadedModel.output_shape
            channelOutputLength = len (output_shape) 
            channelInputLength = len (input_shape) 
            self.ui.classificationModelBtn.setEnabled (False)
            self.ui.segmentationModelBtn.setEnabled (False)
            self.ui.segmentationModelBtn.setChecked (False)
            self.ui.classificationModelBtn.setChecked (False)
            self.ui.settingsGroupBox.setEnabled (False)
            self.ui.SinglePredictBtn.setEnabled (False)
            self.ui.setPredictionProbThresholdBtn.setEnabled (False)
            self.ui.ApplyToImagingBtn.setEnabled (False)
            self.ui.ApplyToImagingBtn.setChecked(False)
            self.ui.multiSliceVotingLabel.setEnabled (False)
            self.ui.multiSliceVotingComboBox.setEnabled (False)
            if channelInputLength < 1 :                
                self.ui.modelInputSizeLabel.setText ("Model input size: None")
            else:
                txtLst = ["None"]                
                for index in range (1,channelInputLength) :
                    txtLst.append (str (input_shape[index]))
                self.ui.modelInputSizeLabel.setText ("Model input size: [" +", ".join (txtLst)+ "]")
            if channelOutputLength < 1 :
                self.ui.modelOutputSizeLabel.setText ("Model output size: None")
                return 
            elif channelOutputLength >= 1 and ((channelInputLength == 5 and (input_shape[4] >= 1 or input_shape[1] >= 1)) or (channelInputLength == 4 and (input_shape[3] >= 1 or input_shape[1] >= 1))) :                
                txtLst = ["None"]                
                for index in range (1,channelOutputLength) :
                    txtLst.append (str (output_shape[index]))
                self.ui.modelOutputSizeLabel.setText ("Model output size: [" +", ".join (txtLst)+ "]")
                                
                if (input_shape[1] is None and input_shape[2] is None): 
                    ResizeInputEnabledOpt = False
                    if len (input_shape) == 4 :
                        self.ui.selectPredictionPatch.setChecked (False)
                    else :
                        self.ui.selectPredictionPatch.setChecked (True)
                else:
                    ResizeInputEnabledOpt = True
                self.ui.InputScalingOpperationComboBox.setEnabled (ResizeInputEnabledOpt)
                self.ui.ReScaleOpperationLbl.setEnabled (ResizeInputEnabledOpt)
                self.ui.InputRescalingLbl.setEnabled (ResizeInputEnabledOpt)
                self.ui.selectPredictionPatch.setEnabled (ResizeInputEnabledOpt)
                
                enabled = False    
                if channelOutputLength == 2 :
                    self._DoesModelSupportApplicationOnNIfTI = False
                    self.ui.classificationModelBtn.setEnabled (True)
                    self.ui.classificationModelBtn.setChecked (True)
                    self.ui.multiSliceVotingLabel.setEnabled (True)
                    self.ui.multiSliceVotingComboBox.setEnabled (True)
                    enabled = True 
                elif channelOutputLength == 3 or channelOutputLength == 4 or channelOutputLength == 5 :                         
                    self._DoesModelSupportApplicationOnNIfTI = False
                    self.ui.segmentationModelBtn.setEnabled (True)
                    self.ui.segmentationModelBtn.setChecked (True)
                    self.ui.multiSliceVotingLabel.setEnabled (False)
                    self.ui.multiSliceVotingComboBox.setEnabled (False)                    
                    self._DoesModelSupportApplicationOnNIfTI = True
                    self.ui.ApplyToImagingBtn.setEnabled (True)
                    self.ui.ApplyToImagingBtn.setChecked (False)
                    enabled = True 
                if (enabled) :
                    self.ui.settingsGroupBox.setEnabled (True)
                    self.ui.SinglePredictBtn.setEnabled (True)
                    self.ui.setPredictionProbThresholdBtn.setEnabled (True)         
            else:
                MessageBoxUtil.showMessage (WindowTitle="Keras model error",WindowMessage="Invalid model input/output shape")
            if channelOutputLength < 2 :
                return 
            if (channelOutputLength > 2) :
                self.ui.isKerasModelOutputChannelLast.setEnabled (True)
            else:
                self.ui.isKerasModelOutputChannelLast.setEnabled (False)

            self._lastChannelCount = int (output_shape[channelOutputLength -1])
            self._firstChannelCount = int (output_shape[1])                        
        except:            
            return None

    def _resizeModelDef (self, re) :        
        #parent = self.ui.ML_ToolBox.widget (0)        
        self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.modelNameEdit, self.size ())            
        self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.modelPathEdit, self.size ())            
        self._resizeWidgetHelper.setWidgetXPos  (self.ui.selectKerasModelBtn, self.width () - 163)      
        self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.modelPathEdit, self.size ())            
        self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.modelInputSizeLabel, self.size ())            
        self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.modelOutputSizeLabel, self.size ())            
        self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.isKerasModelOutputChannelLast, self.size ())
        self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.modelOutputSizeLabel, self.size ())  
        self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.customClassMethodLabel, self.size ())  
        self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.customClassMethodsTxt, self.size ())    
        self._resizeWidgetHelper.setAbsoluteHeight  (self.ui.customClassMethodsTxt, self.size ()) 
        
    def _resizeModelInputSelection (self, re) :
        parent = self.ui.ML_ToolBox.widget (1)        
        imageWidth  = parent.width () - 330
        imageHeight = self.height () - 300
        imageWidth = min (imageWidth, imageHeight)
        imageHeight = imageWidth
        self._resizeWidgetHelper.setWidth   (self.ui.NIfTI_Slice_Preview, imageWidth)            
        self._resizeWidgetHelper.setHeight  (self.ui.NIfTI_Slice_Preview, imageHeight)     
        
        self._resizeWidgetHelper.setWidgetXPos  (self.ui.OrientationLabel, parent.width () - 311)            
        self._resizeWidgetHelper.setWidgetXPos  (self.ui.AxialOrientationCheckBox, parent.width () - 311)        
        self._resizeWidgetHelper.setWidgetXPos  (self.ui.CoronalOrientationCheckBox, parent.width () - 311)            
        self._resizeWidgetHelper.setWidgetXPos  (self.ui.SagittalOrientationCheckBox, parent.width () - 311)        
        
        self._resizeWidgetHelper.setWidgetXPos  (self.ui.InputRescalingLbl, parent.width () - 311)            
        self._resizeWidgetHelper.setWidgetXPos  (self.ui.ReScaleOpperationLbl, parent.width () - 311)            
        self._resizeWidgetHelper.setWidgetXPos  (self.ui.MissingValueLbl, parent.width () - 311)            
        self._resizeWidgetHelper.setWidgetXPos  (self.ui.ModeLbl, parent.width () - 311)            
        self._resizeWidgetHelper.setWidgetXPos  (self.ui.FillValueLbl, parent.width () - 311)                    
        self._resizeWidgetHelper.setWidgetXPos  (self.ui.InputScalingOpperationComboBox, parent.width () - 201)            
        self._resizeWidgetHelper.setWidgetXPos  (self.ui.MissingValueModeCombobox, parent.width () - 201)            
        self._resizeWidgetHelper.setWidgetXPos  (self.ui.FillValueTextBox, parent.width () - 171)            
        self._resizeWidgetHelper.setWidgetXPos  (self.ui.RotateLeftBtn, parent.width () - 271)            
        self._resizeWidgetHelper.setWidgetXPos  (self.ui.rotateRightBtn, parent.width () - 221)            
        self._resizeWidgetHelper.setWidgetXPos  (self.ui.HFlipBtn, parent.width () - 171)            
        self._resizeWidgetHelper.setWidgetXPos  (self.ui.VFlipBtn, parent.width () - 121)                         
        self._resizeWidgetHelper.setWidgetXPos  (self.ui.SwapAxisBtn, parent.width () - 71) 
        self._resizeWidgetHelper.setWidgetXPos  (self.ui.selectPredictionPatch, parent.width () - 311)                            
        
    def _resizeModelNormalization (self, re) :
        parent = self.ui.ML_ToolBox.widget (2)
        self._resizeWidgetHelper.setWidth   (self.ui.Normalization_NoneBtn, parent.width () - 70)            
        self._resizeWidgetHelper.setWidth   (self.ui.Normalization_fixedMeanSDBtn, parent.width () - 70)            
        self._resizeWidgetHelper.setWidth   (self.ui.Normalization_sliceMeanSDBtn, parent.width () - 70)            
        self._resizeWidgetHelper.setWidth   (self.ui.Normalization_volumeMeanSDBtn, parent.width () - 70)                    
        self._resizeWidgetHelper.setWidth   (self.ui.Normalization_customCodeBtn, parent.width () - 70)            
        self._resizeWidgetHelper.setAbsoluteWidth   (self.ui.customCodeTextEdit, self.size ())            
        self._resizeWidgetHelper.setHeight   (self.ui.customCodeTextEdit, self.height () - 555)            
        self._resizeWidgetHelper.setWidgetXPos  (self.ui.resetCustomCodeTemplate, parent.width () - 269)   
        self._resizeWidgetHelper.setWidgetYPos  (self.ui.resetCustomCodeTemplate, self.ui.customCodeTextEdit.pos ().y() + self.ui.customCodeTextEdit.height () + 5)   
        
    
    def _resizeModelPrediction (self, re) :
        parent = self.ui.ML_ToolBox.widget (3)
        self._resizeWidgetHelper.setAbsoluteWidth   (self.ui.PredictionTabWidget, self.size ())  
        self._resizeWidgetHelper.setHeight  (self.ui.PredictionTabWidget, parent.height () - 10)  
        
        parentWidthGrow = max (0, parent.width () - 531)
        lhalf = int (parentWidthGrow / 2)
        rhalf = parentWidthGrow - lhalf
        self._resizeWidgetHelper.setWidth   (self.ui.modelTypeGroupBox,261 + lhalf)  
        self._resizeWidgetHelper.setWidth   (self.ui.classificationModelBtn, 251 + lhalf)  
        self._resizeWidgetHelper.setWidth   (self.ui.segmentationModelBtn, 251 + lhalf)  
        
        self._resizeWidgetHelper.setWidgetXPos   (self.ui.settingsGroupBox, 280 + lhalf)  
        self._resizeWidgetHelper.setWidth   (self.ui.settingsGroupBox,231 + rhalf)  
        self._resizeWidgetHelper.setWidth   (self.ui.SinglePredictBtn, 211 + rhalf)  
        self._resizeWidgetHelper.setWidth   (self.ui.setPredictionProbThresholdBtn, 211 + rhalf)  
        self._resizeWidgetHelper.setWidth   (self.ui.ApplyToImagingBtn, 211 + rhalf)  
        
        self._resizeWidgetHelper.setAbsoluteWidth   (self.ui.predictionTable, self.size ())             
        self._resizeWidgetHelper.setHeight  (self.ui.predictionTable, parent.height () - 160)   
        
        self._resizeWidgetHelper.setAbsoluteWidth   (self.ui.ImplicitlySelectAllSlicesCheckBox, self.size ())             
        self._resizeWidgetHelper.setAbsoluteWidth   (self.ui.PredictionTypeOptions, self.size ())             
        self._resizeWidgetHelper.setAbsoluteWidth   (self.ui.multiSliceVotingComboBox, self.size ())   
        self._resizeWidgetHelper.setAbsoluteWidth   (self.ui.SetDescriptionPrediction, self.size ())                 
        self._resizeWidgetHelper.setAbsoluteWidth   (self.ui.ExistingDataGroupBox_Classification, self.size ()) 
        self._resizeWidgetHelper.setAbsoluteWidth   (self.ui.DoNotAlterExistingDataBtnClassification, self.size ())
        self._resizeWidgetHelper.setAbsoluteWidth   (self.ui.OverwriteExistingDataBtn, self.size ())
        
        self._resizeWidgetHelper.setAbsoluteWidth   (self.ui.CreateUndefinedROICheckBox, self.size ())
        self._resizeWidgetHelper.setAbsoluteWidth   (self.ui.FilterROIKeepOnlyLargest3dIsland, self.size ())
        
        self._resizeWidgetHelper.setAbsoluteWidth   (self.ui.ApplyModelToGroupBox, self.size ())
        self._resizeWidgetHelper.setAbsoluteWidth   (self.ui.ApplyToAllSelectedSlices, self.size ())
        self._resizeWidgetHelper.setAbsoluteWidth   (self.ui.ApplyToAllSelectedSlicesWithData, self.size ())
        self._resizeWidgetHelper.setAbsoluteWidth   (self.ui.ApplyToAllSelectedSlicesWithoutData, self.size ())
        
        self._resizeWidgetHelper.setAbsoluteWidth   (self.ui.ExistingDataGroupBox, self.size ())
        self._resizeWidgetHelper.setAbsoluteWidth   (self.ui.DoNotAlterExistingDataBtn, self.size ())
        self._resizeWidgetHelper.setAbsoluteWidth   (self.ui.ReoveExistingDataFromSliceBtn, self.size ())
        self._resizeWidgetHelper.setAbsoluteWidth   (self.ui.RemoveAllExistingDataBtn, self.size ())
        self._resizeWidgetHelper.setAbsoluteWidth   (self.ui.AddToExistingDataBtn, self.size ())
        
        
        
    def _resizeModelDescription (self, re) :
        parent = self.ui.ML_ToolBox.widget (4)        
        self._resizeWidgetHelper.setAbsoluteWidth   (self.ui.modelTreePath, self.size ())            
        self._resizeWidgetHelper.setAbsoluteWidth   (self.ui.modelVersionTxt, self.size ())            
        self._resizeWidgetHelper.setAbsoluteWidth   (self.ui.modelDescriptionTxt, self.size ())            
        self._resizeWidgetHelper.setHeight  (self.ui.modelDescriptionTxt, parent.height () - 100)            
        
    def _internalWindowResize (self, newsize):                            
        if (self._resizeWidgetHelper != None) :                                               
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.ML_ToolBox, newsize)            
            self._resizeWidgetHelper.setAbsoluteHeight  (self.ui.ML_ToolBox, newsize)                                    
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.line, newsize)            
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.line, newsize.height () - 62)                              
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.OkBtn, newsize.height () - 42)                                    
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.CancelBtn, newsize.height () - 42)                    
            self._resizeWidgetHelper.setWidgetXPos  (self.ui.OkBtn, newsize.width () - 261)                                
            self._resizeWidgetHelper.setWidgetXPos  (self.ui.CancelBtn, newsize.width () - 141)                                      
                                       
    def resizeEvent (self, qresizeEvent) :                       
        QDialog.resizeEvent (self, qresizeEvent)                 
        self._internalWindowResize ( qresizeEvent.size () )             
    
    def setTreePath (self, path) :
        self.ui.modelTreePath.setText (":".join (path))        
    
    def setModelName (self, name) :
        self.ui.modelNameEdit.setText (name)        

    def _getAdpativeDiceExamplePythonCode (self) :
        adpativeDiceExample = []
        adpativeDiceExample += ["#@staticmethod"]
        adpativeDiceExample += ["#def adaptive_dice(y_true, y_pred):"]
        adpativeDiceExample += ["#   import tensorflow as tf"]
        adpativeDiceExample += ['#   """Calculates a composite Dice score that is the normalized sum of d_l*e^(-d_l) for l classes where d_l is the Dice score fora particular class.']        
        adpativeDiceExample += ['#       Arguments:']         
        adpativeDiceExample += ['#                  y_true {Tensor} -- Tensor of shape (batch_size, ..., n_classes)  containing ground truth mask.']
        adpativeDiceExample += ['#                  y_pred {Tensor} -- Tensor of shape (batch_size, ..., n_classes)  containing predicted mask.']
        adpativeDiceExample += ['#      Returns:']
        adpativeDiceExample += ['#                  Tensor -- Adaptive Dice score loss."""']
        adpativeDiceExample += ['#   _epsilon = 10**-7']
        adpativeDiceExample += ['#   # Determine axes to pass to tf.reduce_sum']
        adpativeDiceExample += ['#   ndim = len(y_pred.shape)']
        adpativeDiceExample += ['#   reduction_axes = list(range(ndim- 1))']
        adpativeDiceExample += ['#   # Calculate intersections and unions per class']
        adpativeDiceExample += ['#   intersections = tf.reduce_sum(y_true * y_pred, axis=reduction_axes)']
        adpativeDiceExample += ['#   unions = tf.reduce_sum(y_true + y_pred, axis=reduction_axes)']
        adpativeDiceExample += ['#   dice_scores = 2.0 * (intersections + _epsilon) / (unions + _epsilon) # Calculate Dice scores per class']
        adpativeDiceExample += ['#   weights = tf.exp(-1.0 * dice_scores)                   # Calculate weights based on Dice scores']
        adpativeDiceExample += ['#   weighted_dice = tf.reduce_sum(weights * dice_scores)   # Multiply weights by corresponding scores and get sum']        
        adpativeDiceExample += ['#   norm_factor = tf.size(dice_scores, out_type=tf.float32) * tf.exp(-1.0)  # Calculate normalization factor']
        adpativeDiceExample += ['#   return 1.0 - weighted_dice / norm_factor               # Return adaptive Dice score loss']
        return "\n".join (adpativeDiceExample)

        
    def initFromModelDefinition (self, model) :
        preferedAxis = model.getModelAxisList ()                
        
        try :
            self.ui.AxialOrientationCheckBox.toggled.disconnect (self._setModelOrientation)
            self.ui.CoronalOrientationCheckBox.toggled.disconnect (self._setModelOrientation)
            self.ui.SagittalOrientationCheckBox.toggled.disconnect (self._setModelOrientation)
            reconnect = True
        except:
            reconnect = False
        
        self.ui.CoronalOrientationCheckBox.setChecked ("Coronal" in preferedAxis)
        self.ui.SagittalOrientationCheckBox.setChecked ("Sagittal" in preferedAxis)
        self.ui.AxialOrientationCheckBox.setChecked ("Axial" in preferedAxis)

        if reconnect :    
            self.ui.AxialOrientationCheckBox.toggled.connect (self._setModelOrientation)
            self.ui.CoronalOrientationCheckBox.toggled.connect (self._setModelOrientation)
            self.ui.SagittalOrientationCheckBox.toggled.connect (self._setModelOrientation)

        
        self._setModelOrientation ()
            
        self._DoesModelSupportApplicationOnNIfTI = False
        self.ui.multiSliceVotingLabel.setEnabled (True)
        self.ui.multiSliceVotingComboBox.setEnabled (True)
        self.ui.SinglePredictBtn.setChecked (model.getSinglePrediction ())
        self.ui.modelNameEdit.setText (model.getModelName ())
        self._origionalKerasModelPath = model.getOrigionalKerasModelHDF5Path ()
        self._currentKerasModelPath  = model.getCurrentKerasModelHDF5Path ()    
        self.ui.modelTreePath.setText (":".join (model.getTreePath ()))
        self.ui.ApplyToImagingBtn.setEnabled (False)
        self.ui.ApplyToImagingBtn.setChecked (False)
        customPythonCode = model.getKerasCustomObjectPythonCode ()
        if customPythonCode is None or len (customPythonCode.strip ()) == 0 :
            self.ui.customClassMethodsTxt.setPlainText (self._getAdpativeDiceExamplePythonCode ())        
        else:
            self.ui.customClassMethodsTxt.setPlainText (model.getKerasCustomObjectPythonCode ())        
        path = self._origionalKerasModelPath
        if (path is None) :
            path = ""
        self.ui.modelPathEdit.setText (path)   
        path = self._currentKerasModelPath
                
        if (path is not None and os.path.isfile (path) and os.path.exists (path)) :
            try:    
                self._loadModelPredictionCount (path, model)
                ModelPredictionCount = self.getModelPredictionCount ()
                if ModelPredictionCount is not None and ModelPredictionCount > 0 :                                    
                    self.ui.settingsGroupBox.setEnabled (True)
                    self.ui.SinglePredictBtn.setEnabled (True)
                    self.ui.setPredictionProbThresholdBtn.setEnabled (True)
            except:
                self.ui.classificationModelBtn.setEnabled (False)
                self.ui.segmentationModelBtn.setEnabled (False)
                self.ui.segmentationModelBtn.setChecked (False)
                self.ui.classificationModelBtn.setChecked (False)
                self.ui.settingsGroupBox.setEnabled (False)
                self.ui.multiSliceVotingLabel.setEnabled (False)
                self.ui.multiSliceVotingComboBox.setEnabled (False)
                self.ui.SinglePredictBtn.setEnabled (False)
                self.ui.setPredictionProbThresholdBtn.setEnabled (False)
                self._origionalKerasModelPath = ""
                self._currentKerasModelPath = ""
                self.ui.modelPathEdit.setText ("")   
        
        self.ui.setPredictionProbThresholdBtn.setChecked (model.getPredictionProbThreshold ())   
        
        log = model.getModelDescriptionLog ()
        if (log is not None) :
            self.ui.modelVersionTxt.setText (log.getVersion ())
            self.ui.modelDescriptionTxt.setText (log.getDescription ())
        else:
            self.ui.modelVersionTxt.setText ("")
            self.ui.modelDescriptionTxt.setText ("")
          
        if (self.ui.modelVersionTxt.text () == "") :
            date = DateUtil.today ()
            time = TimeUtil.getTimeNow ()
            self.ui.modelVersionTxt.setText (DateUtil.getSortableString (date) + "." + TimeUtil.timeToSortableString (time))
        
        
        transform = model.getModelInputTransformation ()
        self._ImageTransformation = transform.getAffineTransform ()
        self._updateSlicePreviewToReflectAffine ()
        constantFillValue = transform.getConstantFillValue ()
        self.ui.FillValueTextBox.setText (str (constantFillValue))
        
        if transform.getMissingSliceMode () == "Constant" :
            self.ui.MissingValueModeCombobox.setCurrentIndex (0)
        else:
            self.ui.MissingValueModeCombobox.setCurrentIndex (1)
        
        ioDescription = model.getModelInputOutputDescription ()
        input_shape = ioDescription.getInputDataSize ()
        if (input_shape is not None and input_shape[1] is not None and input_shape[2] is not None):                
            self.ui.selectPredictionPatch.setChecked (transform.isPredictOnPatchSet ())
        
        resizeMode = transform.getResizeMode ()
        selectionIndex = self.ui.InputScalingOpperationComboBox.findText (resizeMode)
        if (selectionIndex == -1) :
            selectionIndex = 0
        self.ui.InputScalingOpperationComboBox.setCurrentIndex (selectionIndex)
        
        slicevoting = model.getMultiSliceVoting ()
        selectionIndex = 0
        if (slicevoting is not None) :
           selectionIndex = self.ui.multiSliceVotingComboBox.findText (slicevoting)
           if (selectionIndex == -1) :
               selectionIndex = 0
        self.ui.multiSliceVotingComboBox.setCurrentIndex (selectionIndex)
        
        normalization = model.getModelNormalization ()
        modetxt = normalization.getMode ()
        self.ui.fixedMeanText.setText ("0")
        self.ui.fixedSDText.setText ("1")

        self.ui.windowText.setText ("1000")
        self.ui.levelText.setText ("100")
        self._setMinMaxText ()
        self.ui.NormalizeChkBox.setChecked (True)
        self.ui.LowNormTxt.setText ("0.0")
        self.ui.HighNormTxt.setText ("1.0")        
        self.ui.customCodeTextEdit.setText (self._default_custom_normalization_code)
        self.ui.Normalization_NoneBtn.setChecked (False)
        self.ui.Normalization_fixedMeanSDBtn.setChecked (False)
        self.ui.Normalization_sliceMeanSDBtn.setChecked (False)
        self.ui.Normalization_volumeMeanSDBtn.setChecked (False)
        self.ui.Normalization_customCodeBtn.setChecked (False)
        self.ui.WindowLevelNormBtn.setChecked (False)
        if (modetxt == "None") :
            self.ui.Normalization_NoneBtn.setChecked (True)
        elif (modetxt == "FixedMeanSD") :
            self.ui.Normalization_fixedMeanSDBtn.setChecked (True)
            self.ui.fixedMeanText.setText (str (normalization.getFixedMean ()))
            self.ui.fixedSDText.setText (str (normalization.getFixedSD ()))
        elif (modetxt == "SliceMeanSD") :
             self.ui.Normalization_sliceMeanSDBtn.setChecked (True)
        elif (modetxt == "VolumeMeanSD") :
            self.ui.Normalization_volumeMeanSDBtn.setChecked (True)
        elif (modetxt == "CustomCode") :
            self.ui.Normalization_customCodeBtn.setChecked (True)
            self.ui.customCodeTextEdit.setText (normalization.getCustomCode ())
        elif (modetxt == "WindowLevel") : 
            self.ui.WindowLevelNormBtn.setChecked (True)
            self.ui.NormalizeChkBox.setChecked (normalization.isWindowLevelNormalized ())            
            self.ui.windowText.setText (str (max (normalization.getWindow (), 1)))
            self.ui.levelText.setText (str (normalization.getLevel ()))
            self._setMinMaxText ()
            self.ui.LowNormTxt.setText (str (normalization.getLowNorm ()))
            self.ui.HighNormTxt.setText (str (normalization.getHighNorm ()))
            
        self.ui.DoNotAlterExistingDataBtn.setChecked (False)
        self.ui.ReoveExistingDataFromSliceBtn.setChecked (True)
        self.ui.RemoveAllExistingDataBtn.setChecked (False)
        self.ui.AddToExistingDataBtn.setChecked (False)
        
        self.ui.ApplyToAllSelectedSlices.setChecked (True)
        self.ui.ApplyToAllSelectedSlicesWithData.setChecked (False)
        self.ui.ApplyToAllSelectedSlicesWithoutData.setChecked (False)
        self.ui.CreateUndefinedROICheckBox.setChecked (False)
        self.ui.FilterROIKeepOnlyLargest3dIsland.setChecked (False)
        self.ui.ImplicitlySelectAllSlicesCheckBox.setChecked (False)
        self.ui.SetDescriptionPrediction.setChecked (False)
        self.ui.DoNotAlterExistingDataBtnClassification.setChecked (False)
        self.ui.OverwriteExistingDataBtn.setChecked (True)
        
        howToHandlePreExistingDataDefault = model.getHowToHandlePreExistingDataFlags ()  
    
        self.ui.ImplicitlySelectAllSlicesCheckBox.setChecked ("ImplicitySelectAllSlices" in howToHandlePreExistingDataDefault)
        predictionMap = model.getPredictionMap ()
        
        if model.getType () == "ML_ApplyTagPrediction" : # classification
            self.ui.PredictionTypeOptions.setCurrentIndex (0)
            if "DoNotAlterExistingData" in howToHandlePreExistingDataDefault :
                self.ui.DoNotAlterExistingDataBtnClassification.setChecked (True)
            else:
                self.ui.OverwriteExistingDataBtn.setChecked (True)
            self.ui.SetDescriptionPrediction.setChecked ("SetDescriptionPrediction" in howToHandlePreExistingDataDefault)
            self._setClassificationModel (False, PredictionInitalization = predictionMap)            
        else:                                            # segmentation        
            self.ui.PredictionTypeOptions.setCurrentIndex (1)
            self.ui.CreateUndefinedROICheckBox.setChecked ("CreateUndefinedROI" in howToHandlePreExistingDataDefault)
            self.ui.FilterROIKeepOnlyLargest3dIsland.setChecked ("FilterROIKeepOnlyLargest3dIsland" in howToHandlePreExistingDataDefault)
            
            if "ApplyModelToAllSelectedSlices" in howToHandlePreExistingDataDefault :
                self.ui.ApplyToAllSelectedSlices.setChecked (True)
            elif "ApplyModelToAllSelectedSlicesWithData" in howToHandlePreExistingDataDefault :
                self.ui.ApplyToAllSelectedSlicesWithData.setChecked(True)
            else:
                self.ui.ApplyToAllSelectedSlicesWithoutData.setChecked(True)
                
            if ( "DoNotAlterExistingData" in howToHandlePreExistingDataDefault) :
                self.ui.DoNotAlterExistingDataBtn.setChecked (True)
            elif ("RemoveExistingDataFromSlice" in howToHandlePreExistingDataDefault) :
                self.ui.ReoveExistingDataFromSliceBtn.setChecked (True)
            elif ("RemoveAllExistingData" in howToHandlePreExistingDataDefault) :
                self.ui.RemoveAllExistingDataBtn.setChecked (True)
            else:
                self.ui.AddToExistingDataBtn.setChecked (True)     
            self._setSegmentationModel (False, SegmentationInitalization = predictionMap)               
            self._DoesModelSupportApplicationOnNIfTI = model.doesModelSupportApplicationOnImaging ()
            self.ui.ApplyToImagingBtn.setEnabled (self._DoesModelSupportApplicationOnNIfTI )
            if model.shouldApplyModelOnImaging () :               
                self.ui.ApplyToImagingBtn.setChecked (True)                     
            
    def _getModelDefinition (self) :
        ModelType = "Unknown"
        if self.ui.segmentationModelBtn.isChecked () :
            model = ML_ApplyROI2DPrediction (self._ProjectDataset)            
            ModelType = "Segmentation"
        elif self.ui.classificationModelBtn.isChecked () :
            model = ML_ApplyTagPrediction (self._ProjectDataset) 
            ModelType = "Classification"
        else:
            model = ML_ApplyROI2DPrediction (self._ProjectDataset)   #default to segmentation model   
            ModelType = "Segmentation"
        
        preferedAxis = []
        if self.ui.AxialOrientationCheckBox.isChecked () :
            preferedAxis += ["Axial"]
        if self.ui.CoronalOrientationCheckBox.isChecked () :
            preferedAxis += ["Coronal"]
        if self.ui.SagittalOrientationCheckBox.isChecked () :
            preferedAxis += ["Sagittal"]        
        
        model.setModelAutoImported (None)                    
        model.setPredictionProbThreshold (self.ui.setPredictionProbThresholdBtn.isChecked ())
        model.setSinglePrediction (self.ui.SinglePredictBtn.isChecked ())      
        model.setMultiSliceVoting (self.ui.multiSliceVotingComboBox.currentText ())
        model.setModelName (self.ui.modelNameEdit.text())
        model.setOrigionalKerasModelHDF5Path (self._origionalKerasModelPath)
        model.setKerasCustomObjectPythonCode (self.ui.customClassMethodsTxt.toPlainText ())
        model.setKerasModelHDF5Path  (self._currentKerasModelPath)
        model.setApplyModelOnImaging (self.ui.ApplyToImagingBtn.isChecked ())        
        versionTxt = self.ui.modelVersionTxt.text ().strip ()
        if versionTxt == "" :
            date = DateUtil.today ()
            time = TimeUtil.getTimeNow ()
            versionTxt = DateUtil.getSortableString (date) + "." + TimeUtil.timeToSortableString (time)
            
        model.setModelDescriptionLog (self._ProjectDataset.getUserName (), versionTxt, self.ui.modelDescriptionTxt.toPlainText ())        
        
        newtreePath = self.ui.modelTreePath.text ().split (":")
        removeIndexLst = []
        for index in range (len (newtreePath)) :
            newtreePath[index] = newtreePath[index].strip ()    
            if len (newtreePath[index]) == 0 :
                removeIndexLst.append (index)
        removeIndexLst.reverse ()
        for index in removeIndexLst :
            del newtreePath[index]
        if len (newtreePath) == 0 :
            newtreePath = [ModelType]
        else:
            newPathLower = newtreePath[0].lower ()
            modelTypeLower = ModelType.lower ()
            if (newPathLower != modelTypeLower):
                if (newPathLower == "classification" and modelTypeLower == "segmentation"):
                    newtreePath[0] = ModelType
                elif (newPathLower == "segmentation" and modelTypeLower == "classification"):
                    newtreePath[0] = ModelType
                else:
                    newtreePath = [ModelType] + newtreePath
        model.setTreePath (newtreePath)
               
        kmodel = model.getKerasModel ()
        inputDataSize = kmodel.getModelInputShape ()
        outputDataSize = kmodel.getModelOutputShape ()
        model.setModelInputOutputDescription (preferedAxis, self._NiftiVolume.getDisplaySliceOrientation (),inputDataSize,outputDataSize, self.ui.isKerasModelOutputChannelLast.isChecked ())
        del kmodel
                          
        resizeMode = self.ui.InputScalingOpperationComboBox.currentText()        
        try:
            valueOutsideOfVolume = int (self.ui.FillValueTextBox.text ())        
        except:            
            valueOutsideOfVolume = 0        
        missingSliceMode = self.ui.MissingValueModeCombobox.currentText()  
        if (missingSliceMode == "Constant value") :
            missingSliceMode = "Constant"
        else:
            missingSliceMode = "Reflection"            
        model.setModelInputTransformation (self._ImageTransformation, missingSliceMode, resizeMode, valueOutsideOfVolume, self.ui.selectPredictionPatch.isChecked ())        
        
        
        
        #finish initalization
        FixedMean=0.0
        FixedSD=1.0
        CustomCode=None
        Window = 1000
        Level = 100
        Norm = True
        LowNorm = 0.0
        HighNorm = 1.0        
        if (self.ui.Normalization_NoneBtn.isChecked ()) :
             NormalizationMode="None"
        elif (self.ui.Normalization_fixedMeanSDBtn.isChecked ()) :
             NormalizationMode="FixedMeanSD"
             try:
                 FixedMean= float (self.ui.fixedMeanText.text ())
             except:
                 FixedMean=0.0
             try:
                 FixedSD  = float (self.ui.fixedSDText.text ())
             except:
                 FixedSD = 1.0                 
        elif (self.ui.Normalization_sliceMeanSDBtn.isChecked ()) :
             NormalizationMode="SliceMeanSD"
        elif self.ui.Normalization_volumeMeanSDBtn.isChecked () :
            NormalizationMode="VolumeMeanSD"
        elif (self.ui.WindowLevelNormBtn.isChecked ()) :
            NormalizationMode="WindowLevel"
            try :
                Window = max (int (self.ui.windowText.text ()), 1)
            except:
                Window = 1000
            try :
                Level = int (self.ui.levelText.text ())
            except:
                Level = 100                
            Norm = self.ui.NormalizeChkBox.isChecked ()
            try :
                LowNorm = float (self.ui.LowNormTxt.text ())
            except:
                LowNorm = 0.0               
            try :
                HighNorm = float (self.ui.HighNormTxt.text ())
            except:
                HighNorm = 1.0                           
        elif (self.ui.Normalization_customCodeBtn.isChecked ()) :
             NormalizationMode="CustomCode"
             CustomCode= self.ui.customCodeTextEdit.toPlainText ()
        model.setModelNormalization (NormalizationMode=NormalizationMode,FixedMean=FixedMean,FixedSD=FixedSD,CustomCode=CustomCode,Window=Window,Level=Level,Norm=Norm,LowNorm=LowNorm,HighNorm=HighNorm)
        
        
        if (self.ui.ImplicitlySelectAllSlicesCheckBox.isChecked ()) :
                model.setHowToHandlePreExistingDataFlags ("ImplicitySelectAllSlices")
        if (ModelType == "Segmentation") :
            if (self.ui.CreateUndefinedROICheckBox.isChecked ()) :
                model.setHowToHandlePreExistingDataFlags ("CreateUndefinedROI")
            if (self.ui.FilterROIKeepOnlyLargest3dIsland.isChecked ()) :
                model.setHowToHandlePreExistingDataFlags ("FilterROIKeepOnlyLargest3dIsland")
            if (self.ui.ApplyToAllSelectedSlices.isChecked ()) :
                model.setHowToHandlePreExistingDataFlags ("ApplyModelToAllSelectedSlices")
            elif (self.ui.ApplyToAllSelectedSlicesWithData.isChecked ()) :
                model.setHowToHandlePreExistingDataFlags ("ApplyModelToAllSelectedSlicesWithData")
            else:
                model.setHowToHandlePreExistingDataFlags ("ApplyModelToAllSelectedSlicesWithOutData")
                
            if (self.ui.DoNotAlterExistingDataBtn.isChecked ()) :
                model.setHowToHandlePreExistingDataFlags ("DoNotAlterExistingData")
            elif (self.ui.ReoveExistingDataFromSliceBtn.isChecked ()) :
                model.setHowToHandlePreExistingDataFlags ("RemoveExistingDataFromSlice")
            elif (self.ui.RemoveAllExistingDataBtn.isChecked ()) :
                model.setHowToHandlePreExistingDataFlags ("RemoveAllExistingData")
            else:
                model.setHowToHandlePreExistingDataFlags ("AddToExistingData")
        else:
        
            if (self.ui.DoNotAlterExistingDataBtnClassification.isChecked ()) :
                model.setHowToHandlePreExistingDataFlags ("DoNotAlterExistingData")
            else:
                model.setHowToHandlePreExistingDataFlags ("RemoveExistingDataFromSlice")
            if (self.ui.SetDescriptionPrediction.isChecked ()) :
                model.setHowToHandlePreExistingDataFlags ("SetDescriptionPrediction")
        
        if self.ui.segmentationModelBtn.isChecked () :
            model.setPredictionMap (self._roiTableModel.getPredictionMap (self._ROIDictionary.getROIDefs ().copy ()))            
        elif self.ui.classificationModelBtn.isChecked () :
            projectDescriptions = self._ProjectDataset.getScanPhaseList ()
            projectDescriptionContext = self._ProjectDataset.getDescriptionContext ()
            datasetTagManager = self._ROIDictionary.getDataFileTagManager ().copy ()
            projectDescriptionTag = Tag  (datasetTagManager._getID (),"Description","Description","Unknown", ParamaterList=projectDescriptions, Override_Tactic_Context = projectDescriptionContext)        
            datasetTagManager.insertTagAtFront (projectDescriptionTag)        
            
            model.setPredictionMap (self._classificationTableModel.getPredictionMap (datasetTagManager))      
        return model
        