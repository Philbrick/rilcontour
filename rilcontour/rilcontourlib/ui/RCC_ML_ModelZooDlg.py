#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 11 14:57:53 2018

@author: m160897
"""
from PyQt5 import QtCore
from PyQt5.QtWidgets import  QDialog, QProgressDialog
from rilcontourlib.util.rcc_util import ResizeWidgetHelper, Coordinate, NiftiVolumeData, MessageBoxUtil
from rilcontourlib.ui.qt_ui_autogen.RCC_MLModelZooDlgAutogen import Ui_RCC_MLModelZooDlg
from rilcontourlib.ui.RCC_ML_ModelTree import ML_ModelTree, ML_ModelTreeNode
from rilcontourlib.machinelearning.ML_AutoImportMLModel import ML_AutoImportMLModel
from rilcontourlib.machinelearning.ml_predict import ML_PredictionPreferedViewUtil
from rilcontourlib.util.DateUtil import DateUtil, TimeUtil
import numpy as np

class RCC_ModelZooDlg (QDialog) :
    
    def __init__ (self, parent) :     
        self._RunningGeneratingVisCallBack = False
        self._classificationModelVisualizationSettings = None                
        self._currentVisualizedDatasetSignature = None
        self._parent = parent
        self._RunAutoImportMLModel = True
        self._ProjectDataset = parent._ProjectDataset
        QDialog.__init__ (self, parent)
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, False)
        self.ui = Ui_RCC_MLModelZooDlg ()      
        self.setWindowFlags(self.windowFlags () | QtCore.Qt.Dialog| QtCore.Qt.WindowStaysOnTopHint )         
        self.ui.setupUi (self)                
        self.ui.ApplyToAllSelectedSlices.setChecked (True)
        self._resizeWidgetHelper = ResizeWidgetHelper ()
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.CloseBtn)
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.ApplyBtn)
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.ModelSplitter)
        self.ui.modelFrame.resizeEvent = self._modelFrameResize 
        self.ui.DescriptionOptionFrame.resizeEvent = self._tabFrameResize
        self.updateModelZooDialogModelList ()
        self.ui.modelTypeFilter.addItem ("Classification & Segmentation")
        self.ui.modelTypeFilter.addItem ("Classification")
        self.ui.modelTypeFilter.addItem ("Segmentation")
        self.ui.modelTypeFilter.currentIndexChanged.connect (self._modelTypeFilterIndexChanged)
        self.ui.filterText.editingFinished.connect (self._filterTextChanged)
        self.ui.filterBySelectedROICheckBox.stateChanged.connect (self._filterBySelectedCheckBox)
        self.ui.ApplyBtn.clicked.connect (self._applyBtn)
        self.ui.CloseBtn.clicked.connect (self._closeBtn)
        self.ui.ApplyBtn.setEnabled (False)
        self.ui.DescriptionOptionTabWidget.currentChanged.connect (self._updateClassificationTab)
        self.ui.LimitSegementationToSelectedROI.setEnabled (False)
        self._parent.ROIDefs.addSelectionChangedListener (self._ROISelectionChanged)
        self._treeSelectionChanged ()
        
        self.ui.visualizationClippingSlider.setValue (150)
        self.ui.visualizationClipTxt.setText ("15")
        self.ui.visualizationMethodComboBox.currentIndexChanged.connect (self._visualizationMethodChanged)
        self.ui.IGBaseline.currentIndexChanged.connect (self._visualizationMethodChanged)
        self.ui.IGSteps.editingFinished.connect (self._IGStepsChanged)
        self.ui.ModelOutputComboBox.currentIndexChanged.connect (self._updateClassificationVisualization)
        self.ui.ModelLayerComboBox.currentIndexChanged.connect (self._updateClassificationVisualization)
        self.ui.ColorMagnitudeComboBox.currentIndexChanged.connect (self._updateClassificationVisualization)
        self.ui.ColorRangeComboBox.currentIndexChanged.connect (self._updateClassificationVisualization)        
        self.ui.visualizationClippingSlider.valueChanged.connect (self._clippingSliderMoved)
        self.ui.visualizationClipTxt.editingFinished.connect (self._clippingTextChanged)
        self._lastIGSteps = 5
    
    def updateModelZooDialogModelList (self, UpdateTreeSelection = False) :
        self._coreModel = ML_ModelTree (self._ProjectDataset, self)
        self._coreModel.addRootNode (ML_ModelTreeNode ("Classification",self._coreModel ))
        self._coreModel.addRootNode (ML_ModelTreeNode ("Segmentation",self._coreModel ))
        if (self._RunAutoImportMLModel ) :
            autoImportMLModels = ML_AutoImportMLModel (self._ProjectDataset)
            autoImportMLModels.runNow (self._coreModel, self)
            self._RunAutoImportMLModel = False
            
        ML_ModelTree.loadModelTreeFromPath (self._ProjectDataset.getMLHomeFilePath () , ExistingModelTreeToAddTo = self._coreModel, LoadModelTreeTextLeafs = False, ProjectDataset=self._ProjectDataset)        
        self.ui.ModelTreeView.setModel (self._coreModel)
        selectionModel = self.ui.ModelTreeView.selectionModel ()
        selectionModel.selectionChanged.connect (self._treeSelectionChanged)       
        if (UpdateTreeSelection) :
            self._treeSelectionChanged ()
        
    def _filterTextChanged (self):
        modelTypeComboboxFilterTxt = self.ui.modelTypeFilter.currentText ()
        filterROIList = []
        if (self.ui.filterBySelectedROICheckBox.isChecked ()) :
            selectedROIListNameList = self._parent.ROIDefs.getSelectedROI ()
            for name in selectedROIListNameList :
                filterROIList.append (self._parent.ROIDefs.getROIDefCopyByName (name))
        filteredModel = self._coreModel.getFilteredTreeModel (modelType = modelTypeComboboxFilterTxt.lower (), filterROIList = filterROIList, filterText = self.ui.filterText.text ())  
        self.ui.ModelTreeView.setModel (filteredModel)      
        selectionModel = self.ui.ModelTreeView.selectionModel ()
        selectionModel.selectionChanged.connect (self._treeSelectionChanged)       
        
    def _modelTypeFilterIndexChanged (self,index) :
        self._filterTextChanged ()
            
    def _filterBySelectedCheckBox (self) :
        self._filterTextChanged ()
   
    def getSelectedTreeNode (self)    :
        selection = self.ui.ModelTreeView.selectionModel ()
        item = selection.currentIndex()                 
        treeNode = item.internalPointer ()                               
        return treeNode    
    
    def _ROISelectionChanged (self) :
        modelTypeComboboxFilterTxt = self.ui.modelTypeFilter.currentText ()
        if (self.ui.filterBySelectedROICheckBox.isChecked () and modelTypeComboboxFilterTxt.lower () != "Classification") and self.isVisible ():
            self._filterTextChanged ()
           
    def _treeSelectionChanged (self, selection = None, deselected = None) :
            
        node = self.getSelectedTreeNode ()        
        enabled = node is not None and node.isMLModel ()   
        if (enabled) :
            mlModel = node.getMLModel ()
            self.ui.modelDescriptionTxt.setHtml (mlModel.getHtmlSummary ())
            
            self.ui.ApplyBtn.setEnabled (self._parent.isROIDictonaryFileLoaded ())
            isClassificationModel =  mlModel.isClassificationModel ()  
            if isClassificationModel : 
                self.ui.PredictionTypeOptions.setCurrentIndex (0)
                self.ui.LimitSegementationToSelectedROI.setEnabled (False)
            else:
                self.ui.PredictionTypeOptions.setCurrentIndex (1)
                self.ui.LimitSegementationToSelectedROI.setEnabled (True)
            
            existingDataFlag = mlModel.getHowToHandlePreExistingDataFlags ()
            
            self.ui.ImplicitlySelectAllSlicesCheckBox.setChecked ("ImplicitySelectAllSlices" in existingDataFlag)
            
            if (isClassificationModel) :
                selectionIndex = self.ui.multiSliceVotingComboBox.findText (mlModel.getMultiSliceVoting ())
                if (selectionIndex == -1) :
                    selectionIndex = 0
                    self.ui.multiSliceVotingComboBox.setCurrentIndex (selectionIndex)
                
                existingDataFlag = mlModel.getHowToHandlePreExistingDataFlags ()
                if "DoNotAlterExistingData" in existingDataFlag  :
                    self.ui.DoNotAlterExistingDataBtnClassification.setChecked (True)
                else:
                    self.ui.OverwriteExistingDataBtn.setChecked (True)
                
                self.ui.SetDescriptionPrediction.setEnabled (mlModel.doesSetDescriptionTag ())
                self.ui.SetDescriptionPrediction.setChecked ("SetDescriptionPrediction" in existingDataFlag)
            else:
                
                self.ui.CreateUndefinedROICheckBox.setChecked ("CreateUndefinedROI" in existingDataFlag)
                self.ui.FilterROIKeepOnlyLargest3dIsland.setChecked ("FilterROIKeepOnlyLargest3dIsland" in existingDataFlag)
                if   "ApplyModelToAllSelectedSlices" in existingDataFlag :
                    self.ui.ApplyToAllSelectedSlices.setChecked (True)
                elif "ApplyModelToAllSelectedSlicesWithData" in existingDataFlag :
                    self.ui.ApplyToAllSelectedSlicesWithData.setChecked (True)
                else:
                    self.ui.ApplyToAllSelectedSlicesWithoutData.setChecked (True)
             
                if   "DoNotAlterExistingData" in existingDataFlag :
                    self.ui.DoNotAlterExistingDataBtn.setChecked (True)
                elif "RemoveExistingDataFromSlice" in existingDataFlag :
                    self.ui.ReoveExistingDataFromSliceBtn.setChecked (True)
                elif "RemoveAllExistingData" in existingDataFlag :
                    self.ui.RemoveAllExistingDataBtn.setChecked (True)
                else:
                    self.ui.AddToExistingDataBtn.setChecked (True)
                    
            self.ui.SetDescriptionPrediction.setEnabled (isClassificationModel)
            self.ui.ExistingDataGroupBox_Classification.setEnabled (isClassificationModel)
            self.ui.DoNotAlterExistingDataBtnClassification.setEnabled (isClassificationModel)
            self.ui.OverwriteExistingDataBtn.setEnabled (isClassificationModel)
     
            
            self.ui.CreateUndefinedROICheckBox.setEnabled (not isClassificationModel)
            self.ui.FilterROIKeepOnlyLargest3dIsland.setEnabled (not isClassificationModel)
            self.ui.ImplicitlySelectAllSlicesCheckBox.setEnabled (True)
            self.ui.multiSliceVotingLabel.setEnabled (isClassificationModel)
            self.ui.multiSliceVotingComboBox.setEnabled (isClassificationModel) 
            self.ui.ApplyModelToGroupBox.setEnabled (not isClassificationModel)
            self.ui.ApplyToAllSelectedSlices.setEnabled (not isClassificationModel)
            self.ui.ApplyToAllSelectedSlicesWithData.setEnabled (not isClassificationModel)
            self.ui.ApplyToAllSelectedSlicesWithoutData.setEnabled (not isClassificationModel)
            self.ui.ExistingDataGroupBox.setEnabled (True)
            self.ui.DoNotAlterExistingDataBtn.setEnabled (True)
            self.ui.ReoveExistingDataFromSliceBtn.setEnabled (True)
            self.ui.RemoveAllExistingDataBtn.setEnabled (True)
            self.ui.AddToExistingDataBtn.setEnabled (not isClassificationModel)
        else:
            self.ui.LimitSegementationToSelectedROI.setEnabled (False)
            self.ui.SetDescriptionPrediction.setEnabled (False)
            self.ui.ExistingDataGroupBox_Classification.setEnabled (False)
            self.ui.DoNotAlterExistingDataBtnClassification.setEnabled (False)
            self.ui.OverwriteExistingDataBtn.setEnabled (False)
            
            self.ui.modelDescriptionTxt.setHtml ("")
            self.ui.ApplyBtn.setEnabled (False)
            self.ui.CreateUndefinedROICheckBox.setEnabled (False)
            self.ui.FilterROIKeepOnlyLargest3dIsland.setEnabled (False)
            self.ui.ImplicitlySelectAllSlicesCheckBox.setEnabled (False)
            self.ui.multiSliceVotingLabel.setEnabled (False)
            self.ui.multiSliceVotingComboBox.setEnabled (False) 
            self.ui.ApplyModelToGroupBox.setEnabled (False)
            self.ui.ApplyToAllSelectedSlices.setEnabled (False)
            self.ui.ApplyToAllSelectedSlicesWithData.setEnabled (False)
            self.ui.ApplyToAllSelectedSlicesWithoutData.setEnabled (False)
            self.ui.ExistingDataGroupBox.setEnabled (False)
            self.ui.DoNotAlterExistingDataBtn.setEnabled (False)
            self.ui.ReoveExistingDataFromSliceBtn.setEnabled (False)
            self.ui.RemoveAllExistingDataBtn.setEnabled (False)
            self.ui.AddToExistingDataBtn.setEnabled (False)
        self.updateSliceClassificationVisualization ()
        
    def _applyModelCallBack (self, XPos = None, YPos = None, WholeSlice = False) :
        if (XPos is not None and YPos is not None) or (WholeSlice) :
            if (self._classificationModelVisualizationSettings is None) :
                self._classificationModelVisualizationSettings = {}
            self._classificationModelVisualizationSettings["PatchCenterPos"] = (XPos, YPos)    
            
            if (self._parent.NiftiVolume is not None ) :
                node = self.getSelectedTreeNode ()
                if node is not None and node.isMLModel ()    :
                    mlModel = node.getMLModel ()           
                    
                    createUndefinedROI = self.ui.CreateUndefinedROICheckBox.isChecked ()        
                    applyToAllSlices = self.ui.ImplicitlySelectAllSlicesCheckBox.isChecked ()       
                    
                    isClassificationModel =  mlModel.isClassificationModel ()  
                    if isClassificationModel :
                        applyModelOn = "AllSelectedSlices"
                        if self.ui.DoNotAlterExistingDataBtnClassification.isChecked ():
                            existingData = "DoNotAlter"
                        else:
                            existingData = "Remove"
                            
                        setDescriptionPrediction = self.ui.SetDescriptionPrediction.isChecked ()
                        votingMode = self.ui.multiSliceVotingComboBox.currentText ()
                        LimitSegmentationToSelectedROI = False
                    else:
                        LimitSegmentationToSelectedROI = self.ui.LimitSegementationToSelectedROI.isChecked ()
                        if self.ui.ApplyToAllSelectedSlices.isChecked ():
                            applyModelOn = "AllSelectedSlices"
                        elif self.ui.ApplyToAllSelectedSlicesWithData.isChecked ():
                            applyModelOn = "SelectedSlicesWithData"
                        else :
                            applyModelOn = "SelectedSlicesWithOutData"
                
                        if self.ui.DoNotAlterExistingDataBtn.isChecked ():
                            existingData = "DoNotAlter"
                        elif self.ui.ReoveExistingDataFromSliceBtn.isChecked ():
                            existingData = "Remove"
                        elif self.ui.RemoveAllExistingDataBtn.isChecked ():
                            existingData = "RemoveAll"
                        else:
                            existingData = "AddToData"
                        setDescriptionPrediction = False
                        votingMode = "Average"
                        
                    options = {}
                    options["CreateUndefinedROI"] = createUndefinedROI
                    options["ApplyToAllSlices"] = applyToAllSlices
                    options["ApplyOn"] = applyModelOn
                    options["ExistingData"] = existingData
                    options["VotingMode"] = votingMode
                    options["SetDescriptionPrediction"] = setDescriptionPrediction
                    options["PatchCenterPos"] = (XPos, YPos)                    
                    if self.ui.FilterROIKeepOnlyLargest3dIsland.isEnabled () and self.ui.FilterROIKeepOnlyLargest3dIsland.isChecked () :
                        options["FilterROIKeepOnlyLargest3dIsland"] = True
                    else:
                        options["FilterROIKeepOnlyLargest3dIsland"] = False
                        
                    progdialog = QProgressDialog(parent=self)
                    progdialog.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
                    progdialog.setMinimumDuration (4)
                    progdialog.setWindowFlags(progdialog.windowFlags () | QtCore.Qt.Dialog  | QtCore.Qt.WindowStaysOnTopHint)                
                    progdialog.setWindowTitle ("Applying ML Model")            
                    progdialog.setWindowModality(QtCore.Qt.ApplicationModal)                
                    progdialog.setRange(0,0)                
                    progdialog.setMinimumWidth(300)
                    progdialog.setMinimumHeight(100)
                    try:                    
                        progdialog.forceShow()                       
                        progdialog.setValue (0)
                        self._ProjectDataset.getApp ().processEvents ()
                        
                        if self._parent.ROIDictionary.isROIinPerimeterMode () :
                            loglines, listOfRoiDefsCreated = mlModel.applyMLModel (self._parent.ui.XYSliceSelectorWidget,self._parent.ui.XYSliceView, self._parent.NiftiVolume, self._parent.ROIDictionary, options, App = self._ProjectDataset.getApp (), LimitSegmentationToSelectedROI = LimitSegmentationToSelectedROI, ProgressDialog = progdialog, ProjectDataset = self._ProjectDataset)
                        else:
                            PreferedViewList = mlModel.getModelAxisList ()
                            selectedSliceView, sliceSelector = ML_PredictionPreferedViewUtil.getPreferedSliceViewAndSliceSelection (PreferedViewList, self._parent)                            
                            if (selectedSliceView is not None and sliceSelector is not None ) :
                                loglines, listOfRoiDefsCreated = mlModel.applyMLModel (sliceSelector, selectedSliceView, self._parent.NiftiVolume, self._parent.ROIDictionary, options, App = self._ProjectDataset.getApp (), LimitSegmentationToSelectedROI = LimitSegmentationToSelectedROI, ProgressDialog = progdialog, ProjectDataset = self._ProjectDataset)
                            else:
                                listOfRoiDefsCreated = None
                                loglines = []
                        if listOfRoiDefsCreated is not None and len (listOfRoiDefsCreated) > 0 :
                            self._parent.reloadROIUIList ()
                            self._ProjectDataset.saveROIDefToProject (self._parent.ROIDictionary.getROIDefs (), IgnoreWindow = self._parent) 
                        try :
                            if mlModel.shouldApplyModelOnImaging () :
                                self._parent.ClearAxisProjectionNiftiCache ()
                        except:
                            print ("Error occured clearing hte nifti imaging cache.")
                    finally:                    
                        progdialog.close ()                            
                        self._ProjectDataset.getApp ().processEvents ()
                    if len (loglines) > 0 :
                        loglines = "<br>".join (loglines)
                        currentText = self.ui.predictionLog.toPlainText()
                        if len (currentText) > 0 :
                            currentText = self.ui.predictionLog.toHtml()
                            currentText += "<hr>"
                        else:
                            currentText = self.ui.predictionLog.toHtml()
                        dateStr = DateUtil.dateToString (DateUtil.today ())
                        timeStr = TimeUtil.timeToString(TimeUtil.getTimeNow ())
                        DateTimeStr = dateStr + " at " +timeStr
                        currentText += "<b>Date Time: </b>"+DateTimeStr+"<br>"
                        self.ui.predictionLog.setHtml (currentText + loglines)
        
    def _getSelectionSliceView (self, mlModel) :
        if mlModel is not None and not self._parent.ROIDictionary.isROIinPerimeterMode () :
             PreferedViewList = mlModel.getModelAxisList ()
             selectedSliceView, _ = ML_PredictionPreferedViewUtil.getPreferedSliceViewAndSliceSelection (PreferedViewList, self._parent)    
        else:
             selectedSliceView, _ = ML_PredictionPreferedViewUtil.getPreferedSliceViewAndSliceSelection ([], self._parent) 
        return selectedSliceView 
    
    def _getPredictedSliceCenter (self, mlModel) :
        if mlModel is not None and not self._parent.ROIDictionary.isROIinPerimeterMode () :
            PreferedViewList = mlModel.getModelAxisList ()
            selectedSliceView, _ = ML_PredictionPreferedViewUtil.getPreferedSliceViewAndSliceSelection (PreferedViewList, self._parent)              
            shape = selectedSliceView.getSliceShape ()
            return int (shape[0]/2), int (shape[1]/2)     
        sliceData = self._parent.NiftiVolume.getImageData ()[:,:, 0]
        return int (sliceData.shape[0]/2), int (sliceData.shape[1]/2)     
        
    def _applyBtn (self) :                
        if (self._parent.NiftiVolume is not None ) :
            if not self._parent.isMultiUserMode () and not self._parent.needsToAcquirePLockToSave () :
                self._parent.versionFile (Comment="Auto-version for application of ML Model")
            else:
                while self._parent.needsToAcquirePLockToSave () :
                    if not self._parent.canTryToAcquirePLock () :
                        MessageBoxUtil.showMessage ("File Locked","The file is currently locked by another user. The selected ML model cannot be applied.")
                        return
                    else:
                        unlockDataset = MessageBoxUtil.showWarningMessageBox ("Resource Unlocked", "The file being modifed is currently unlocked. Do you wish to lock the file and save changes to it?")  
                        if unlockDataset :
                            self._parent.checkoutPLock ()
                        else:
                            return 
                    
            node = self.getSelectedTreeNode ()
            if node is not None and node.isMLModel ()    :
                mlModel = node.getMLModel ()            
                
                if not mlModel.isSegmentationModel () or self._parent.showFlattenDeepGrowDlg ("One-click generated segmentations must be flattened before a machine learning segmentation model can be run. Do you wish to flatten the segmentation?") :
                    if mlModel.getModelInputTransformation().isPredictOnPatchSet () :
                        inputDataSize = mlModel.getModelInputOutputDescription ().getInputDataSize()
                        xDim, yDim = inputDataSize[1], inputDataSize[2]
                        if xDim is None and yDim is None :
                            xDim, yDim =  self._getPredictedSliceCenter (mlModel)
                            self._applyModelCallBack (xDim, yDim, WholeSlice = False)
                        else:
                            self._parent.getRangeSelectionBoundingBox (xDim, yDim, self._applyModelCallBack, SliceView = self._getSelectionSliceView(mlModel))
                    else:
                        self._applyModelCallBack (WholeSlice = True)
                    
                
        
    def _closeBtn (self) :
        self._parent.ROIDefs.removeSelectionChangedListener (self._ROISelectionChanged)
        self.close ()
    
    def closeEvent (self, event) :
        self._parent.ROIDefs.removeSelectionChangedListener (self._ROISelectionChanged)        
        
    def _modelFrameResize (self, event) :
        width = self.ui.modelFrame.width ()
        self._resizeWidgetHelper.setWidth (self.ui.filterText, width - 75 )        
        self._resizeWidgetHelper.setWidth (self.ui.filterBySelectedROICheckBox, width - 75 )
        self._resizeWidgetHelper.setWidth (self.ui.modelTypeFilter, width - 75 )
        self._resizeWidgetHelper.setWidth (self.ui.ModelTreeView, width - 18 )
        self._resizeWidgetHelper.setHeight(self.ui.ModelTreeView, self.ui.modelFrame.height () - 100)
    
    def _tabFrameResize (self, event) :
        width = self.ui.DescriptionOptionFrame.width ()
        height = self.ui.DescriptionOptionFrame.height ()
        
        self._resizeWidgetHelper.setWidth (self.ui.DescriptionOptionTabWidget, width  )        
        self._resizeWidgetHelper.setHeight(self.ui.DescriptionOptionTabWidget, height)
        
        self._resizeWidgetHelper.setWidth (self.ui.ModelDescriptionLbl, width - 41 )
        self._resizeWidgetHelper.setWidth (self.ui.modelDescriptionTxt, width - 14 )        
        self._resizeWidgetHelper.setHeight(self.ui.modelDescriptionTxt, height - 74)
        self._resizeWidgetHelper.setWidth (self.ui.multiSliceVotingComboBox, width - 148 )   
        
        self._resizeWidgetHelper.setWidth (self.ui.SetDescriptionPrediction, width - 38 )   
        self._resizeWidgetHelper.setWidth (self.ui.ExistingDataGroupBox_Classification, width - 18 )   
        self._resizeWidgetHelper.setWidth (self.ui.DoNotAlterExistingDataBtnClassification, width - 38 )   
        self._resizeWidgetHelper.setWidth (self.ui.OverwriteExistingDataBtn, width - 38 )           
        self._resizeWidgetHelper.setWidth (self.ui.CreateUndefinedROICheckBox, width - 21 )        
        self._resizeWidgetHelper.setWidth (self.ui.FilterROIKeepOnlyLargest3dIsland, width - 21 )        
        self._resizeWidgetHelper.setWidth (self.ui.ImplicitlySelectAllSlicesCheckBox, width - 21 )        
        self._resizeWidgetHelper.setWidth (self.ui.ApplyModelToGroupBox, width - 21 )        
        self._resizeWidgetHelper.setWidth (self.ui.ExistingDataGroupBox, width - 21 )        
        self._resizeWidgetHelper.setWidth (self.ui.ApplyToAllSelectedSlices, width - 40 )        
        self._resizeWidgetHelper.setWidth (self.ui.ApplyToAllSelectedSlicesWithData, width - 40 )        
        self._resizeWidgetHelper.setWidth (self.ui.ApplyToAllSelectedSlicesWithoutData, width - 40 )                
        self._resizeWidgetHelper.setWidth (self.ui.DoNotAlterExistingDataBtn, width - 40 )        
        self._resizeWidgetHelper.setWidth (self.ui.ReoveExistingDataFromSliceBtn, width - 40 )        
        self._resizeWidgetHelper.setWidth (self.ui.RemoveAllExistingDataBtn, width - 40 )        
        self._resizeWidgetHelper.setWidth (self.ui.AddToExistingDataBtn, width - 40 )
        self._resizeWidgetHelper.setWidth (self.ui.PredictionTypeOptions, width - 10 )
        self._resizeWidgetHelper.setHeight (self.ui.PredictionTypeOptions, height - 30 )        
        self._resizeWidgetHelper.setWidth (self.ui.predictionLog, width - 18 )
        self._resizeWidgetHelper.setHeight (self.ui.predictionLog, height - 40 )        
        self.setClassificationPosition ()
        
    
    def setClassificationPosition (self) :
        if (self._parent.NiftiVolume is  None) :
            MV_Width = 241
            MV_Height = 176                                            
        else:
            SelectedSliceView = None 
            if not self._parent.ROIDictionary.isROIinPerimeterMode () :
                node = self.getSelectedTreeNode ()  
                if node is not None and node.isMLModel ()    :
                    mlModel = node.getMLModel ()           
                    PreferedViewList = mlModel.getModelAxisList ()
                    SelectedSliceView, _ = ML_PredictionPreferedViewUtil.getPreferedSliceViewAndSliceSelection (PreferedViewList, self._parent)    
            if SelectedSliceView is None :
                SelectedSliceView = self._parent.ui.XYSliceView
                            
            width = self.ui.DescriptionOptionFrame.width ()
            height = self.ui.DescriptionOptionFrame.height ()
            #sliceIndex = self._parent.getSelectedSlice ()
            MV_Width = width - 60
            MV_Height = height - 235            
            sliceShape = SelectedSliceView.getSliceShape ()
            xdim, yDim = sliceShape[0], sliceShape[1]        
            MV_Width  = min(MV_Width, int (xdim * MV_Height/ yDim))
            MV_Height = int (yDim * MV_Width / xdim)
            
        self._resizeWidgetHelper.setWidth (self.ui.ClassifierModelVisualization, MV_Width )
        self._resizeWidgetHelper.setHeight (self.ui.ClassifierModelVisualization, MV_Height )
                
        self._resizeWidgetHelper.setWidgetXPos  (self.ui.visualizationClippingSlider, MV_Width + 19)                                        
        self._resizeWidgetHelper.setHeight  (self.ui.visualizationClippingSlider, MV_Height)              
        
        YPos = MV_Height + 19
        self._resizeWidgetHelper.setWidgetYPos  (self.ui.visualizationMethodComboBox, YPos)    
        width = self.ui.DescriptionOptionFrame.width ()
        height = MV_Height + 230
        self._resizeWidgetHelper.setWidth  (self.ui.visualizationMethodComboBox, width - 190)            
        self._resizeWidgetHelper.setWidgetXPos  (self.ui.VisClipLabel, width - 171)                                        
        self._resizeWidgetHelper.setWidgetXPos  (self.ui.visualizationClipTxt, width - 81)     
        self._resizeWidgetHelper.setWidgetYPos  (self.ui.VisClipLabel, YPos)                                       
        self._resizeWidgetHelper.setWidgetYPos  (self.ui.visualizationClipTxt, YPos)    
        
        YPos = height - 181
        self._resizeWidgetHelper.setWidgetYPos  (self.ui.ModelOutputLbl, YPos)                                       
        self._resizeWidgetHelper.setWidgetYPos  (self.ui.ModelOutputComboBox, YPos)    
        self._resizeWidgetHelper.setWidth (self.ui.ModelOutputComboBox, width - 130)
        
        YPos = height - 151
        self._resizeWidgetHelper.setWidgetYPos  (self.ui.ModelLayerLbl, YPos)                                       
        self._resizeWidgetHelper.setWidgetYPos  (self.ui.ModelLayerComboBox, YPos)    
        self._resizeWidgetHelper.setWidth (self.ui.ModelLayerComboBox, width - 130)
        
        self._resizeWidgetHelper.setWidgetYPos  (self.ui.IGBaselineLbl, YPos)                                       
        self._resizeWidgetHelper.setWidgetYPos  (self.ui.IGBaseline, YPos)    
        self._resizeWidgetHelper.setWidgetYPos  (self.ui.IGStepsLbl, YPos)                                       
        self._resizeWidgetHelper.setWidgetYPos  (self.ui.IGSteps, YPos)    
        wth = width - 142
        hwith = int (wth // 2)
        self._resizeWidgetHelper.setWidth (self.ui.IGBaseline, hwith)
        self._resizeWidgetHelper.setWidth (self.ui.IGSteps, hwith)
        self._resizeWidgetHelper.setWidgetXPos  (self.ui.IGStepsLbl, hwith + 80)    
        self._resizeWidgetHelper.setWidgetXPos  (self.ui.IGSteps, hwith + 80 + 51)    
          
        YPos = height - 121
        self._resizeWidgetHelper.setWidgetYPos  (self.ui.colorSettingsGroupBox, YPos)    
        self._resizeWidgetHelper.setWidth (self.ui.colorSettingsGroupBox, width - 20)
        
        self._resizeWidgetHelper.setWidth (self.ui.ColorMagnitudeComboBox, width - 140)
        self._resizeWidgetHelper.setWidth (self.ui.ColorRangeComboBox, width - 140)
        
            
    def setClassificationEnabled (self) :
        if (self._parent.NiftiVolume is  None) :        
            self._setClassificationVisualizationEnabled (False)            
        else:
            node = self.getSelectedTreeNode ()  
            if node is None or not node.isMLModel () :
                self._setClassificationVisualizationEnabled (False)            
            else:
                mlModel = node.getMLModel ()     
                self._setClassificationVisualizationEnabled ((mlModel.isLastLayerSigmoidOrSoftMax() or mlModel.doesModelHaveCustomCodeWithLinearActivation())) 
                """isClassificationModel = mlModel.isClassificationModel ()        
                if (isClassificationModel) :        
                    self._setClassificationVisualizationEnabled (mlModel.isLastLayerSigmoidOrSoftMax()) 
                else:
                    self._setClassificationVisualizationEnabled (False)   """         
                       
    def _getSelectedModelOutput (self,mlModel)  :
        try :
            if (mlModel.isClassificationModel ()) :                                  
                modelOutputTagDescriptionList = mlModel.getPredictionTagNameList ()
            else:
                modelOutputTagDescriptionList = mlModel.getPredictionROINameList ()
            index = self.ui.ModelOutputComboBox.currentIndex ()
            return modelOutputTagDescriptionList[index][0]
        except:
            return None
    
       
    def _generateVisCallBack (self, XPos = None, YPos = None, WholeSlice = False) :
        try :
            if self._RunningGeneratingVisCallBack :
                return
            self._RunningGeneratingVisCallBack = True
            if (self.ui.DescriptionOptionTabWidget.currentIndex () == 3): 
                node = self.getSelectedTreeNode ()  
                if self._parent.NiftiVolume is not None and node is not None and node.isMLModel ()    :
                    mlModel = node.getMLModel ()                
                    #if mlModel.getModelInputTransformation().isPredictOnPatchSet () : 
                    #    return
                    #if mlModel.isClassificationModel () and mlModel.isLastLayerSigmoidOrSoftMax() :  
                    if (mlModel.isLastLayerSigmoidOrSoftMax() or mlModel.doesModelHaveCustomCodeWithLinearActivation()) and not mlModel.shouldApplyModelOnImaging () :                      
                            
                        visualizationmethod = self.ui.visualizationMethodComboBox.currentText ()
                        integratedGradientFunction = None
                        integratedGradientSteps = 0
                        if (visualizationmethod == "Prediction probability") :
                            vismethod = "PredictionProbability"
                        elif (visualizationmethod == "Positive saliency map") :
                            vismethod = "PosGradRangeSaliencyMap"
                        elif (visualizationmethod == "abs (Saliency map)") :
                            vismethod = "AbsSaliencyMap"                            
                        elif (visualizationmethod == "Saliency map") :
                            vismethod = "SaliencyMap"
                        elif visualizationmethod == "Integrated gradient" :
                            vismethod = "IntegratedGradient"
                            integratedGradientFunction = self.ui.IGBaseline.currentText ().lower ()
                            integratedGradientSteps = self._lastIGSteps 
                        elif visualizationmethod == "abs (Integrated gradient)" :
                            vismethod = "abs(IntegratedGradient)"
                            integratedGradientFunction = self.ui.IGBaseline.currentText ().lower ()
                            integratedGradientSteps = self._lastIGSteps 
                        elif (visualizationmethod == "Gradient class activation map (Grad-CAM)"):
                            vismethod = "GradCam"
                        elif (visualizationmethod == "Saliency activation map (SAM)") :
                            vismethod = "SaliencyActivation"
                        elif (visualizationmethod == "Relative saliency activation map (R-SAM)") :
                            vismethod = "RelativeSaliencyActivation"
                        elif (visualizationmethod == "Positive saliency activation map (P-SAM)") :
                            vismethod = "PosSaliencyActivation"
                        elif (visualizationmethod == "Negative saliency activation map (N-SAM)") :
                            vismethod = "NegSaliencyActivation"
                        elif (visualizationmethod == "Class activation map (CAM)") :
                            vismethod = "ClassActivation"
                        else:
                            vismethod = "MethodNotSet"
                        
                        selectedSliceView = None
                        sliceSelector = None
                        if not self._parent.ROIDictionary.isROIinPerimeterMode () :
                            PreferedViewList = mlModel.getModelAxisList ()
                            selectedSliceView, sliceSelector = ML_PredictionPreferedViewUtil.getPreferedSliceViewAndSliceSelection (PreferedViewList, self._parent)    
                        if (selectedSliceView is None or sliceSelector is None) :
                            selectedSliceView = self._parent.ui.XYSliceView
                            sliceSelector = self._parent.ui.XYSliceSelectorWidget
                        sliceIndex = sliceSelector.getSelectdSlice ()
                
                        classificationModelOutputIndex = self._getSelectedModelOutput (mlModel)      
                        if (classificationModelOutputIndex is None) :
                            return
                        modelLayerName = self.ui.ModelLayerComboBox.currentText ()
                        selectedModelLayerIndex = self.ui.ModelLayerComboBox.itemData (self.ui.ModelLayerComboBox.currentIndex ())
                        
                        targetShape =  tuple (list (selectedSliceView.getSliceShape ()) + [1])
                        
                        if (self._classificationModelVisualizationSettings is None or 
                            "SliceIndex" not in self._classificationModelVisualizationSettings or 
                            self._classificationModelVisualizationSettings["SliceIndex"] != sliceIndex or 
                            self._classificationModelVisualizationSettings["SliceAxis"] != selectedSliceView.getSliceAxis () or
                            self._classificationModelVisualizationSettings["VisMethod"] != vismethod  or 
                            self._classificationModelVisualizationSettings["PredictionOutputChannel"] != classificationModelOutputIndex  or 
                            self._classificationModelVisualizationSettings["LayerName"] != modelLayerName or
                            self._classificationModelVisualizationSettings["SelectedModelLayerIndex"] != selectedModelLayerIndex or
                            "NiftiSliceData" not in self._classificationModelVisualizationSettings or 
                            self._classificationModelVisualizationSettings["NiftiSliceData"] is None or 
                            self._classificationModelVisualizationSettings["NiftiSliceData"].shape != targetShape or 
                            self._classificationModelVisualizationSettings["PatchCenterPos"] != (XPos, YPos) or 
                            self._classificationModelVisualizationSettings["IntegratedGradientsFunction"] != integratedGradientFunction or
                            self._classificationModelVisualizationSettings["IntegratedGradientSteps"] != integratedGradientSteps ):  
                              
                            if self._classificationModelVisualizationSettings is None or "SliceAxis" not in self._classificationModelVisualizationSettings or self._classificationModelVisualizationSettings["SliceAxis"] != selectedSliceView.getSliceAxis () :
                                self.setClassificationPosition ()
                            
                            self._currentVisualizedDatasetSignature = self._getParentDatasetSignature ()
                            progdialog = QProgressDialog(parent = None)
                            progdialog.setAttribute(QtCore.Qt.WA_DeleteOnClose, True)
                            progdialog.setMinimumDuration (4)
                            progdialog.setWindowFlags(progdialog.windowFlags () | QtCore.Qt.Dialog  | QtCore.Qt.WindowStaysOnTopHint)                
                            progdialog.setWindowTitle ("Generating ML slice visualization")            
                            progdialog.setWindowModality(QtCore.Qt.ApplicationModal)                
                            progdialog.setRange(0,0)                
                            progdialog.setMinimumWidth(300)
                            progdialog.setMinimumHeight(100)
                            try:                 
                                self.setEnabled (False)
                                progdialog.forceShow()           
                                progdialog.setValue (0)
                                self._ProjectDataset.getApp ().processEvents ()
                                options = {}
                                options["PredictionOutputChannel"] = classificationModelOutputIndex
                                options["Visualization"] = vismethod                            
                                options["LayerName"] = modelLayerName
                                options["SelectedModelLayerIndex"] = selectedModelLayerIndex
                                options["PatchCenterPos"] = (XPos, YPos)
                                options["IntegratedGradientsFunction"] = integratedGradientFunction
                                options["IntegratedGradientsSteps"] = integratedGradientSteps                             
                                
                                if self._classificationModelVisualizationSettings is None : 
                                    self._classificationModelVisualizationSettings = {}
                                self._classificationModelVisualizationSettings["PatchCenterPos"] = (XPos, YPos)    
                                self._classificationModelVisualizationSettings["SliceIndex"] = sliceIndex
                                self._classificationModelVisualizationSettings["SliceAxis"] = selectedSliceView.getSliceAxis ()
                                self._classificationModelVisualizationSettings["VisMethod"] = vismethod
                                self._classificationModelVisualizationSettings["PredictionOutputChannel"] = classificationModelOutputIndex
                                self._classificationModelVisualizationSettings["LayerName"] = modelLayerName   
                                self._classificationModelVisualizationSettings["SelectedModelLayerIndex"] = selectedModelLayerIndex     
                                self._classificationModelVisualizationSettings["IntegratedGradientsFunction"] = integratedGradientFunction
                                self._classificationModelVisualizationSettings["IntegratedGradientSteps"] = integratedGradientSteps
                                try:  
                                    visgradient = mlModel.getVisualization (sliceIndex, selectedSliceView ,  options, App = self._ProjectDataset.getApp (), ProgressDialog = progdialog)
                                    if visgradient is not None :
                                        visgradient = visgradient.astype (np.float)
                                except:
                                    visgradient = None
                                self._classificationModelVisualizationSettings["Gradient"] = visgradient                                            
                                self._classificationModelVisualizationSettings["ClipPrecent"] = None
                                self._classificationModelVisualizationSettings["ColorMagnitude"] = None
                                self._classificationModelVisualizationSettings["ColorRange"] = None
                                self._classificationModelVisualizationSettings["ColorVisualizationMem"] = None        
                                    
                            
                                sliceData = selectedSliceView.getNIfTIVolumeSliceViewProjection (SliceIndex = sliceIndex)
                                if "NiftiSliceData" not in self._classificationModelVisualizationSettings  :
                                    sliceMemory = None
                                else:
                                    sliceMemory = self._classificationModelVisualizationSettings["NiftiSliceData"]                            
                                if (sliceMemory is None or sliceMemory.shape != targetShape) :
                                    sliceMemory = np.zeros (targetShape, dtype = sliceData.dtype)
                                sliceMemory[:,:,0] = sliceData                        
                                self._classificationModelVisualizationSettings["NiftiSliceData"] = sliceMemory
                                if (visgradient is not None) :
                                    if (vismethod in ["RelativeSaliencyActivation", "SaliencyMap", "IntegratedGradient"]) :
                                        thresholdGradientGradient = np.abs(visgradient)
                                    else:
                                        thresholdGradientGradient = visgradient
                                    precent = mlModel.computeOptimalColorizeGradientClipThreshold (thresholdGradientGradient, App = self._ProjectDataset.getApp ())
                                else:
                                    precent = None
                                    
                                if precent is not None :
                                    try :
                                        self.ui.visualizationClippingSlider.valueChanged.disconnect (self._clippingSliderMoved)
                                        reconnectClipSliderMovedReconnect = True
                                    except:
                                        reconnectClipSliderMovedReconnect = False
                                    try:
                                        self.ui.visualizationClipTxt.editingFinished.disconnect (self._clippingTextChanged)
                                        editingFinishedReconnect = True
                                    except:
                                        editingFinishedReconnect = False
                                    self.ui.visualizationClippingSlider.setValue (int (precent) * 10)
                                    self.ui.visualizationClipTxt.setText (str (precent))
                                    if (reconnectClipSliderMovedReconnect) :
                                        self.ui.visualizationClippingSlider.valueChanged.connect (self._clippingSliderMoved)
                                    if (editingFinishedReconnect) :
                                        self.ui.visualizationClipTxt.editingFinished.connect (self._clippingTextChanged)
                                
                            finally:
                                progdialog.close ()
                                self.setEnabled (True)
                                self._ProjectDataset.getApp ().processEvents ()
                            
                                self._classificationModelVisualizationSettings["CreatedNiftiDataset"] = NiftiVolumeData.InitFromNumpyArray (self._classificationModelVisualizationSettings["NiftiSliceData"], self._parent.NiftiVolume, DataAxis = selectedSliceView.getSliceAxis ())                    
                        
                                          
                        niftiDataset = self._classificationModelVisualizationSettings["CreatedNiftiDataset"]
                        clipPrecent = float (self.ui.visualizationClippingSlider.value ()/1000.0)                    
                        colorMagnitudeText = self.ui.ColorMagnitudeComboBox.currentText ()
                        colorRangeText = self.ui.ColorRangeComboBox.currentText ()
                                                                
                        if (self._classificationModelVisualizationSettings["ClipPrecent"] != clipPrecent or
                            self._classificationModelVisualizationSettings["ColorMagnitude"] !=  colorMagnitudeText or
                            self._classificationModelVisualizationSettings["ColorRange"] != colorRangeText or 
                            self._classificationModelVisualizationSettings["ColorVisualizationMem"] is None or 
                            self._classificationModelVisualizationSettings["ColorVisualizationMem"].shape != targetShape) : 
                        
                            modelGradient = self._classificationModelVisualizationSettings["Gradient"]
                            ColorByOrder = colorMagnitudeText == "Output value order"
                            scaleWithinRange = colorRangeText == "Clipped gradient"
                            
                            if modelGradient is None :
                                thresholdGradientGradient = None   
                                ZeroCenter = False
                            elif (self._classificationModelVisualizationSettings["VisMethod"] == "IntegratedGradient"):
                                thresholdGradientGradient = np.abs(modelGradient)
                                ZeroCenter = False
                            elif (self._classificationModelVisualizationSettings["VisMethod"] == "SaliencyMap") :
                                thresholdGradientGradient = np.abs(modelGradient)
                                ZeroCenter = False
                            elif (self._classificationModelVisualizationSettings["VisMethod"] == "RelativeSaliencyActivation") :
                                thresholdGradientGradient = np.abs(modelGradient)
                                ZeroCenter = True
                            else:
                                thresholdGradientGradient = None   
                                ZeroCenter = False
                            
                            
                            self._classificationModelVisualizationSettings["ColorVisualizationMem"] = mlModel.colorizeGradientMap (modelGradient, clipPrecent=clipPrecent, ColorByOrder=ColorByOrder, scaleWithinRange=scaleWithinRange, ClipGradientMask = thresholdGradientGradient, ZeroCenter = ZeroCenter)
                                
                            self._classificationModelVisualizationSettings["ClipPrecent"] = clipPrecent
                            self._classificationModelVisualizationSettings["ColorMagnitude"] =  colorMagnitudeText
                            self._classificationModelVisualizationSettings["ColorRange"] = colorRangeText
                                                             
                        self.ui.ClassifierModelVisualization.setColorOverlay (self._classificationModelVisualizationSettings["ColorVisualizationMem"], 1.0, DisableUpdateSlice = True)
                        self.ui.ClassifierModelVisualization.setHounsfieldVisSettings (self._parent.getHounsfieldSettingsCopy ())
                        self.ui.ClassifierModelVisualization.setVisSettings (self._parent.getVisSettingsCopy ())                    
                        self.ui.ClassifierModelVisualization.setNiftiDataSet (niftiDataset, self._ProjectDataset)
                        coordinate = Coordinate ()
                        sliceDataShape = self._classificationModelVisualizationSettings["NiftiSliceData"].shape
                        coordinate.setCoordinate ([int (sliceDataShape[0] / 2),int (sliceDataShape[1]/ 2),0])
                        self.ui.ClassifierModelVisualization.initNiftiDataSet (coordinate,"Z")
                                           
                        return
                self.ui.ClassifierModelVisualization.setNiftiDataSet (None, None)    
                self._classificationModelVisualizationSettings = None    
                self.ui.ClassifierModelVisualization.update()
        finally:
            self._RunningGeneratingVisCallBack = False
                
    def _generateClassificationVisualization (self) : 
        if (self.ui.DescriptionOptionTabWidget.currentIndex () == 3): 
            node = self.getSelectedTreeNode ()  
            if self._parent.NiftiVolume is not None and node is not None and node.isMLModel ()    :
                mlModel = node.getMLModel ()   
                if (mlModel.isLastLayerSigmoidOrSoftMax() or mlModel.doesModelHaveCustomCodeWithLinearActivation())  and not mlModel.shouldApplyModelOnImaging () :                               
                    if mlModel.getModelInputTransformation().isPredictOnPatchSet () :                                        
                        if self._classificationModelVisualizationSettings is None or "PatchCenterPos" not in self._classificationModelVisualizationSettings :                   
                            inputDataSize = mlModel.getModelInputOutputDescription ().getInputDataSize()
                            xDim, yDim = inputDataSize[1], inputDataSize[2]
                            if xDim is None and yDim is None :
                                xDim, yDim =  self._getPredictedSliceCenter (mlModel) 
                                self._generateVisCallBack (xDim, yDim, WholeSlice = False)
                            else:                                                    
                                xDim, yDim = inputDataSize[1], inputDataSize[2]
                                self._parent.getRangeSelectionBoundingBox (xDim, yDim, self._generateVisCallBack, SliceView = self._getSelectionSliceView(mlModel))
                        else:
                            posx, posy = self._classificationModelVisualizationSettings["PatchCenterPos"]
                            self._generateVisCallBack (posx, posy, WholeSlice = False)
                    else:
                        self._generateVisCallBack (WholeSlice = True)            
                
    def _clippingSliderMoved (self, val) :        
        self._generateClassificationVisualization ()
        text = "%0.1f" % (self.ui.visualizationClippingSlider.value () / 10)
        if text != self.ui.visualizationClipTxt.text () :
            self.ui.visualizationClipTxt.setText (text)
    
    def _clippingTextChanged (self) : 
        txt = self.ui.visualizationClipTxt.text ()
        try :
            val = float (txt)
            sliderVal = val * 10 
            sliderValue = int (sliderVal)
            if sliderValue != self.ui.visualizationClippingSlider.value () :
                self.ui.visualizationClippingSlider.setValue (sliderValue)
                sliderValue = self.ui.visualizationClippingSlider.value ()
                if sliderValue != sliderVal :
                    self.ui.visualizationClipTxt.setText (str (sliderValue/10))
            elif sliderValue/10 != val :
                self.ui.visualizationClipTxt.setText (str (sliderValue/10))
        except:
            text = "%0.1f" % (self.ui.visualizationClippingSlider.value () / 10)
            if text != self.ui.visualizationClipTxt.text () :
                self.ui.visualizationClipTxt.setText (text)
     
    def _initClassificationModelOptions (self) :
            currentVisMethodTxt = self.ui.visualizationMethodComboBox.currentText ()
            modelOutputTxt = self.ui.ModelOutputComboBox.currentText ()
            modelLayerTxt = self.ui.ModelLayerComboBox.currentText ()
            
            try :
                self.ui.visualizationMethodComboBox.currentIndexChanged.disconnect (self._visualizationMethodChanged)
                visualizationMethodComboBoxReconnect = True
            except:
                visualizationMethodComboBoxReconnect = False
            
            try :    
                self.ui.ModelOutputComboBox.currentIndexChanged.disconnect (self._updateClassificationVisualization)
                ModelOutputComboBoxReconnect = True
            except:
                ModelOutputComboBoxReconnect = False
            
            try :     
                self.ui.ModelLayerComboBox.currentIndexChanged.disconnect (self._updateClassificationVisualization)
                ModelLayerComboBoxReconnect = True
            except:
                ModelLayerComboBoxReconnect = False
            try:
                self.ui.visualizationMethodComboBox.clear ()
                self.ui.ModelOutputComboBox.clear ()
                self.ui.ModelLayerComboBox.clear ()
                
                node = self.getSelectedTreeNode ()  
                if self._parent.NiftiVolume is not None and node is not None and node.isMLModel ()    :
                    mlModel = node.getMLModel ()                 
                    if True : # mlModel.isClassificationModel () :
                        methodTxtList = ["Saliency map", "abs (Saliency map)", "Positive saliency map", "Integrated gradient", "abs (Integrated gradient)"]
                        
                        #self.ui.visualizationMethodComboBox.addItem ("Saliency map")
                        MLLayers = mlModel.getModelVisualizationLayers ()
                        if MLLayers is not None and len (MLLayers) > 0 :
                            methodTxtList += ["Gradient class activation map (Grad-CAM)", "Saliency activation map (SAM)", "Positive saliency activation map (P-SAM)", "Negative saliency activation map (N-SAM)"] # , "Relative saliency activation map (R-SAM)"
                            #self.ui.visualizationMethodComboBox.addItem ("Gradient class activation map (Grad-CAM)")
                            #self.ui.visualizationMethodComboBox.addItem ("Saliency activation map (SAM)")
                        if (mlModel.doesModelSupportsCAM ()) :
                            methodTxtList += ["Class activation map (CAM)"]
                            #self.ui.visualizationMethodComboBox.addItem ("Class activation map (CAM)")
                        
                        if not mlModel.isClassificationModel () :
                            methodTxtList += ["Prediction probability"]
                            
                        setMethodIndex = 0
                        for methodIndex, methodTxt in enumerate(methodTxtList) : 
                            self.ui.visualizationMethodComboBox.addItem (methodTxt)
                            if (methodTxt == currentVisMethodTxt) :
                               setMethodIndex =  methodIndex
                        self.ui.visualizationMethodComboBox.setCurrentIndex (setMethodIndex)
                            
                        if MLLayers is not None and len (MLLayers) > 0 :
                            selectedIndex = 0
                            for index, layer_tuple in enumerate (MLLayers) :  
                                layerModelIndex, layer = layer_tuple
                                layerName = mlModel.getModelLayerName (layer)
                                self.ui.ModelLayerComboBox.addItem (layerName)
                                self.ui.ModelLayerComboBox.setItemData (index, layerModelIndex)
                                if modelLayerTxt == layerName :
                                    selectedIndex = index
                            self.ui.ModelLayerComboBox.setCurrentIndex (selectedIndex)
                        
                        if (mlModel.isClassificationModel ()) :                                  
                            modelOutputTagDescriptionList = mlModel.getPredictionTagNameList ()
                            if len (modelOutputTagDescriptionList) > 0 :                                
                                try:
                                    self.ui.ModelOutputComboBox.setEnabled (True)
                                    PreferedViewList = mlModel.getModelAxisList ()
                                    selectedSliceView, sliceSelector = ML_PredictionPreferedViewUtil.getPreferedSliceViewAndSliceSelection (PreferedViewList, self._parent)    
                                    predictionProb = mlModel.getSlicePrediction (self._parent.getSelectedSlice (), selectedSliceView, self._parent.NiftiVolume)
                                    if predictionProb is not None :
                                        selectedIndex = 0                        
                                        for index, outputDescription in enumerate (modelOutputTagDescriptionList) :                       
                                            if outputDescription is not None :
                                                self.ui.ModelOutputComboBox.addItem (outputDescription[1] + " : Prediction Prob %0.3f" % predictionProb[outputDescription[0]])
                                                if (predictionProb[index] > predictionProb[selectedIndex]) :
                                                    selectedIndex = index
                                        self.ui.ModelOutputComboBox.setCurrentIndex (selectedIndex)   
                                except:
                                    self.ui.ModelOutputComboBox.setEnabled (False)
                        else:
                            modelOutputTagDescriptionList = mlModel.getPredictionROINameList ()
                            selectedIndex = 0         
                            for index, outputDescription in enumerate (modelOutputTagDescriptionList) :
                                 self.ui.ModelOutputComboBox.addItem (outputDescription[1])
                                 if modelOutputTxt == outputDescription :
                                      selectedIndex = index
                            self.ui.ModelOutputComboBox.setCurrentIndex (selectedIndex)   
                            
                self._setVisualizationMethodDependentEnabled ()
            finally:
                if (visualizationMethodComboBoxReconnect) :
                    self.ui.visualizationMethodComboBox.currentIndexChanged.connect (self._visualizationMethodChanged)
                if (ModelOutputComboBoxReconnect) :
                    self.ui.ModelOutputComboBox.currentIndexChanged.connect (self._updateClassificationVisualization)
                if (ModelLayerComboBoxReconnect) :
                    self.ui.ModelLayerComboBox.currentIndexChanged.connect (self._updateClassificationVisualization)
        
    def _updateClassificationTab (self, index = 0) :        
        if self.ui.DescriptionOptionTabWidget.currentIndex () == 3 : # 
            self.setClassificationPosition  ()
            self.setClassificationEnabled ()
            self._initClassificationModelOptions ()               
            self._generateClassificationVisualization ()    
    
    def _getParentDatasetSignature (self) :
        try :
            if self._parent.NiftiVolume is None or self._parent.ROIDictionary is None or self._parent.ROIDictionary.getROIFileHandle () is None :
                return (None, None) 
            else:
                return (self._parent.ROIDictionary.getFilename (), self._parent.getSelectedSlice ())
        except:
            return (None, None)
   
    def event (self, event) :
        if (event.type() == QtCore.QEvent.WindowActivate) : 
            if self._getParentDatasetSignature () != self._currentVisualizedDatasetSignature :
                self.updateSliceClassificationVisualization ()    
        return QDialog.event(self, event)
     
        
    def updateSliceClassificationVisualization (self) :
        if not self._RunningGeneratingVisCallBack :
            self._classificationModelVisualizationSettings = None 
            self._updateClassificationTab ()          
        
    def _updateClassificationVisualization (self, index) :
        self._generateClassificationVisualization ()
        
    def _setVisualizationMethodDependentEnabled (self) :
        if (self.ui.visualizationMethodComboBox.currentText () in ["Saliency map", "abs (Saliency map)", "Positive saliency map", "Integrated gradient","abs (Integrated gradient)"]) :
            self.ui.ModelLayerLbl.setEnabled (False)
            self.ui.ModelLayerComboBox.setEnabled (False)            
            self.ui.colorSettingsGroupBox.setEnabled (True)
            self.ui.ColorMagnitudeLabel.setEnabled (True)
            self.ui.ColorMagnitudeComboBox.setEnabled (True)
            self.ui.ColorRangeLabel.setEnabled (True)
            self.ui.ColorRangeComboBox.setEnabled (True)
            self.ui.ColorMagnitudeComboBox.setCurrentIndex (1)
            self.ui.ColorRangeComboBox.setCurrentIndex (1)
        elif (self.ui.visualizationMethodComboBox.currentText () == "Class activation map (CAM)") :
            self.ui.ModelLayerLbl.setEnabled (False)
            self.ui.ModelLayerComboBox.setEnabled (False)            
            self.ui.colorSettingsGroupBox.setEnabled (False)
            self.ui.ColorMagnitudeLabel.setEnabled (False)
            self.ui.ColorMagnitudeComboBox.setEnabled (False)
            self.ui.ColorRangeLabel.setEnabled (False)
            self.ui.ColorRangeComboBox.setEnabled (False)
            self.ui.ColorMagnitudeComboBox.setCurrentIndex (0)
            self.ui.ColorRangeComboBox.setCurrentIndex (0)
        else:
            notPredictionProb = self.ui.visualizationMethodComboBox.currentText () not in ["Prediction probability"]
            self.ui.ModelLayerLbl.setEnabled (notPredictionProb)
            self.ui.ModelLayerComboBox.setEnabled (notPredictionProb)
            
            self.ui.colorSettingsGroupBox.setEnabled (False)
            self.ui.ColorMagnitudeLabel.setEnabled (False)
            self.ui.ColorMagnitudeComboBox.setEnabled (False)
            self.ui.ColorRangeLabel.setEnabled (False)
            self.ui.ColorRangeComboBox.setEnabled (False)
            self.ui.ColorMagnitudeComboBox.setCurrentIndex (0)
            self.ui.ColorRangeComboBox.setCurrentIndex (0)
        if self.ui.visualizationMethodComboBox.currentText () in ["Integrated gradient", "abs (Integrated gradient)"] :
            self.ui.IGBaselineLbl.setVisible (True)
            self.ui.IGBaseline.setVisible (True)
            self.ui.IGStepsLbl.setVisible (True)
            self.ui.IGSteps.setVisible (True)
            self.ui.ModelLayerLbl.setVisible (False)
            self.ui.ModelLayerComboBox.setVisible (False)        
        else:
            self.ui.IGBaselineLbl.setVisible (False)
            self.ui.IGBaseline.setVisible (False)
            self.ui.IGStepsLbl.setVisible (False)
            self.ui.IGSteps.setVisible (False)
            self.ui.ModelLayerLbl.setVisible (True)
            self.ui.ModelLayerComboBox.setVisible (True)        
    
    def _IGStepsChanged (self) :
           try :
               integratedGradientSteps = int (self.ui.IGSteps.text ())
               if integratedGradientSteps < 1 :
                   self.ui.IGSteps.setText ("1")
               elif integratedGradientSteps > 1000 :
                   self.ui.IGSteps.setText ("1000")
               elif integratedGradientSteps != self._lastIGSteps :
                   self._lastIGSteps = integratedGradientSteps
                   self._visualizationMethodChanged ()
           except:
               self.ui.IGSteps.setText (str (int (self._lastIGSteps)))
              
    def _visualizationMethodChanged (self) :
        self._generateClassificationVisualization ()
        self._setVisualizationMethodDependentEnabled ()
            
        
        
    def _setClassificationVisualizationEnabled (self, val) :
        self.ui.visualizationClippingSlider.setEnabled (val)        
        self.ui.DescriptionOptionTabWidget.widget (3).setEnabled (val)                    
        self.ui.visualizationMethodComboBox.setEnabled (val)
        self.ui.VisClipLabel.setEnabled (val)
        self.ui.visualizationClipTxt.setEnabled (val)        
        self.ui.ModelOutputLbl.setEnabled (val)
        self.ui.ModelOutputComboBox.setEnabled (val)        
        self.ui.ModelLayerLbl.setEnabled (val)
        self.ui.ModelLayerComboBox.setEnabled (val)                        
        self.ui.colorSettingsGroupBox.setEnabled (val)
        self.ui.ColorMagnitudeLabel.setEnabled (val)
        self.ui.ColorMagnitudeComboBox.setEnabled (val)
        self.ui.ColorRangeLabel.setEnabled (val)
        self.ui.ColorRangeComboBox.setEnabled (val)                
        if self.ui.ClassifierModelVisualization.isEnabled () and not val :
            self.ui.ClassifierModelVisualization.setNiftiDataSet (None, None) 
            self.ui.ClassifierModelVisualization.initNiftiDataSet (None,"Z")
            self.ui.ClassifierModelVisualization.setColorOverlay (None)
        self.ui.ClassifierModelVisualization.setEnabled (val)        
        self._setVisualizationMethodDependentEnabled ()      
        
    def _internalWindowResize (self, newsize):                            
        if (self._resizeWidgetHelper != None) :                                               
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.ModelSplitter, newsize)            
            self._resizeWidgetHelper.setAbsoluteHeight  (self.ui.ModelSplitter, newsize)                                                
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.ApplyBtn, newsize.height () - 38)                        
            self._resizeWidgetHelper.setWidgetXPos  (self.ui.ApplyBtn, newsize.width () - 335)                                
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.CloseBtn, newsize.height () - 38)                        
            self._resizeWidgetHelper.setWidgetXPos  (self.ui.CloseBtn, newsize.width () - 185)                        
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.LimitSegementationToSelectedROI, newsize.height () - 34)  
            self._resizeWidgetHelper.setWidth (self.ui.LimitSegementationToSelectedROI, self.ui.ApplyBtn.pos().x() - self.ui.LimitSegementationToSelectedROI.pos().x() - 5)                      
                                       
    def resizeEvent (self, qresizeEvent) :                       
        QDialog.resizeEvent (self, qresizeEvent)                 
        self._internalWindowResize ( qresizeEvent.size () )               
    
    
   