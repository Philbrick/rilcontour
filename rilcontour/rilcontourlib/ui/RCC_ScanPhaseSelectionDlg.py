#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan  4 16:59:58 2017

@author: m160897
"""
import numpy as np
import nibabel as nib
from PyQt5 import QtCore, QtGui
from PyQt5.QtWidgets import  QMainWindow, QTreeWidgetItem, QProgressDialog
from rilcontourlib.util.rcc_util import ResizeWidgetHelper, NiftiVolumeData, Coordinate, HounsfieldVisSettings, VisSettings
from rilcontourlib.dataset.roi.rcc_roi import ROIDictionary, ROIDefinitions
from rilcontourlib.ui.rcc_hounsfieldVisOptionsDlg import RCC_HounsfieldVisOptionsDlg
from rilcontourlib.ui.qt_ui_autogen.rcc_scanphaseselectiondlgAutoGen import Ui_rcc_scanphaseselectiondlgautogen
from PyQt5.QtCore import QThread, pyqtSignal

   
    

class RCC_ScanPhaseSelectionDlg (QMainWindow) :
    
    def __init__ (self, parent, projectDataset) :
        QMainWindow.__init__ (self, parent)                
        self._DisableWindowChangeEvent = False
        self._disableDatasetSelectionChanged = False
        self._scanPhaseCache = {}        
        self._hounsfeildDlg = None
        self._origionalScanPhaseCache = {}
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)         
        self.ui = Ui_rcc_scanphaseselectiondlgautogen ()        
        self.ui.setupUi (self)  
        self.menuBar().setNativeMenuBar(False)         
        self._resizeWidgetHelper = ResizeWidgetHelper ()        
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.DatasetTree)        
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.SliceLbl)        
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.SliceNumberTxt)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.SliceSelectionWidget)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.SliceSelectionSlider)
        self._ProjectDataset = projectDataset
        self._selectedTreeWidgetNode = None
        self._initHounsfieldVisSettings = HounsfieldVisSettings ()
        self._initVisSettings = VisSettings ()        
        self._uiSetHounsfieldVisSettings = False
        self._UIInitalized = False
        self.ui.actionHounsfield_visualization_settings.triggered.connect (self.HounsfieldVisualizationDlg)
        self.ui.actionAbout.triggered.connect (self.showAboutDlg)
        self.ui.Quit.triggered.connect (self.closeBtnClk )
        self._clearScanPhaseCache ()
    
    def showAboutDlg (self) :
        if (self._ProjectDataset is not None) :
            self._ProjectDataset.getNiftiDatasetDlg().showAboutDlg (self)
        
    class SavedChangeDictThread (QThread):
                    
        updateProgress = pyqtSignal(['PyQt_PyObject'])        
        threadDone = pyqtSignal(['PyQt_PyObject'])        
        
        def __init__(self, ROIDictionaryList):  
            QThread.__init__(self, None)  
            self._ROIDictionaryLst = ROIDictionaryList
            
        def run (self) :
            for count, roiDictonary in enumerate (self._ROIDictionaryLst) :
                try :                                    
                    print ("saving")
                    print (roiDictonary._filHandle.getPath ())
                    roiDictonary.saveROIDictionaryToFile ()
                    roiDictonary.delete (ForceSilentOverwrite = True)        
                    print ("Done")                    
                    self.updateProgress.emit (count)
                except Exception as e :
                    print (e)    
            self.threadDone.emit (None)
    
    def _updateProgress (self, val) :
        self._progdialog.setValue (val)                
            
    def _updateDone (self, val) :
        self._progdialog.close ()
        del self._progdialog
        del self._scanPhaseCache
        del self._origionalScanPhaseCache
        self._scanPhaseCache = {}
        self._origionalScanPhaseCache = {}
        print ("Done")
        
    def _clearScanPhaseCache (self) :
        if (len (self._scanPhaseCache) == 0) :
            return                
        
        saveDictionaryList = []
        for widget, roiDictonary in self._scanPhaseCache.items () :            
            origionalScanPhase = self._origionalScanPhaseCache[widget]
            if (roiDictonary.getScanPhaseTxt () != origionalScanPhase) :
                saveDictionaryList.append (roiDictonary)
            else:
                handle = roiDictonary.getROIFileHandle ()                 
                roiDictonary.delete (KeepFileHandleOpen = True)   
                handle.closeUnsaved ()                
        self._origionalScanPhaseCache =  {}
        if (len (saveDictionaryList) > 0) :
            # progress UI dialog 
            self._progdialog = QProgressDialog("", "", 0, len (saveDictionaryList), self)
            self._progdialog.setMinimumDuration (0)
            self._progdialog.setWindowFlags(self._progdialog.windowFlags () | QtCore.Qt.Dialog  | QtCore.Qt.WindowStaysOnTopHint)
            self._progdialog.setWindowTitle("Saving Changes")
            self._progdialog.setWindowModality(QtCore.Qt.ApplicationModal)
            self._progdialog.setMaximum (100)
            self._progdialog.forceShow()                   
            self._progdialog.raise_ ()
            
            self._SavedChangeDict = RCC_ScanPhaseSelectionDlg.SavedChangeDictThread (saveDictionaryList)
            self._SavedChangeDict.updateProgress.connect (self._updateProgress)
            self._SavedChangeDict.threadDone.connect (self._updateDone)
            self._SavedChangeDict.start ()
        else:
            del self._scanPhaseCache
            del self._origionalScanPhaseCache
            self._scanPhaseCache = {}
            self._origionalScanPhaseCache = {}
                            
        
    
    def _updateTreeWidgetDescriptions (self, treeWidget) :
        while (treeWidget != None) :
            tpl = treeWidget.data (0, QtCore.Qt.UserRole)        
            widget = tpl[0]      
            description = RCC_ScanPhaseSelectionDlg._getNodeDescription (widget)
            treeWidget.setText (1, description)
            if (RCC_ScanPhaseSelectionDlg.doesNodeContainPredictedScanPhase (widget)):
                treeWidget.setForeground (1, QtGui.QColor("red"))
            else :
                treeWidget.setForeground (1, QtGui.QColor("black"))
            treeWidget = treeWidget.parent()
        
    def _setScanPhasePredictionBtn (self, index) :        
        try:
            treeWidget = self._NiftiDatasetsShown[index-1]
            currentScanPhase, predictedScanPhase = self._getScanPhase (treeWidget)
            if (currentScanPhase != predictedScanPhase and predictedScanPhase != "Unknown") :                                
                                            
                tpl = treeWidget.data (0, QtCore.Qt.UserRole)        
                widget = tpl[0]      
                if (widget in self._scanPhaseCache):
                    tempDictionary = self._scanPhaseCache[widget]                
                    tempDictionary.setScanPhase (predictedScanPhase)  #upates scan phase selection cache
                    nodeindicatorResult = True
                
                    self.setSelectedScanPhase (widget, predictedScanPhase)                                                           
                    widget.setROIDatasetIndicator (nodeindicatorResult)                                               
                    self._ProjectDataset.setSelectedScanPhase (widget, predictedScanPhase)
                    self.setProjectDatasetTreeNodeUpdated (widget)
                    
                    self._updateTreeWidgetDescriptions (treeWidget)                
        except:
            return
        
                
    def _setScanPhasePredictionBtn1 (self) :
        self._setScanPhasePredictionBtn (1)        
         
    def _setScanPhasePredictionBtn2 (self) :
        self._setScanPhasePredictionBtn (2)        
        
    def _setScanPhasePredictionBtn3 (self) :
        self._setScanPhasePredictionBtn (3)        
        
    def _setScanPhasePredictionBtn4 (self) :
        self._setScanPhasePredictionBtn (4)        
        
    def _setScanPhasePredictionBtn5 (self) :
        self._setScanPhasePredictionBtn (5)        
        
    def _setScanPhasePredictionBtn6 (self) :
        self._setScanPhasePredictionBtn (6)        
        
                            
    def getSelectedNIfTIDataset (self) : 
        return self._selectedTreeWidgetNode
    
    def setActiveContourWindow (self, boolVal) :
        return 
    
    #initWindow called on selection of new node in the project dataset brower.  Initalize the window to reflect the selection
    def initWindow (self, treeWidgetNode = None, sliceNumber = None, activeContourWindow = None) :                         
        self._selectedTreeWidgetNode = treeWidgetNode
        self._SliderChangedUpdateWidgetList = [0,1,2,3,4,5]
        self._disableComboBoxChangedEvent = False
        self._disableUIUpdate = False                        
        self._NiftiDatasetsShown = []
        self._clearScanPhaseCache ()
        
        if (self._UIInitalized) :
            try :
                treeSelectionModel = self.ui.DatasetTree.selectionModel ()
                treeSelectionModel.selectionChanged.disconnect (self._DatasetSelectionChanged)             
                self.ui.SliceNumberTxt.editingFinished.disconnect (self.sliceTXTNumberChanged)
                
                self.ui.SliceSelectionSlider.sliderPressed.disconnect (self.sliceSelectionSliderChanged_MouseDown)
                self.ui.SliceSelectionSlider.valueChanged.disconnect (self.sliceSelectionSliderChanged_UpdateWidget1)
                self.ui.SliceSelectionSlider.sliderReleased.disconnect (self.sliceSelectionSliderChanged_UpdateAll)
                
                for index , slider in enumerate (self._sliderSliceAdjustmentUI) :
                    if slider != None :  
                        if (index == 0) :
                            slider.valueChanged.disconnect (self._UIOffsetSliderValueChanged1)
                        elif (index == 1) :
                            slider.valueChanged.disconnect (self._UIOffsetSliderValueChanged2)
                        elif (index == 2) :
                            slider.valueChanged.disconnect (self._UIOffsetSliderValueChanged3)
                        elif (index == 3) :
                            slider.valueChanged.disconnect (self._UIOffsetSliderValueChanged4)
                        elif (index == 4) :
                            slider.valueChanged.disconnect (self._UIOffsetSliderValueChanged5)
                        elif (index == 5) :
                            slider.valueChanged.disconnect (self._UIOffsetSliderValueChanged6)
                        else:
                            print ("Unexpected sider")
                
                self.ui.PhasePrediction1SetBtn.clicked.disconnect (self._setScanPhasePredictionBtn1)
                self.ui.PhasePrediction2SetBtn.clicked.disconnect (self._setScanPhasePredictionBtn2)
                self.ui.PhasePrediction3SetBtn.clicked.disconnect (self._setScanPhasePredictionBtn3)
                self.ui.PhasePrediction4SetBtn.clicked.disconnect (self._setScanPhasePredictionBtn4)
                self.ui.PhasePrediction5SetBtn.clicked.disconnect (self._setScanPhasePredictionBtn5)
                self.ui.PhasePrediction6SetBtn.clicked.disconnect (self._setScanPhasePredictionBtn6)
                
                self.ui.sliderResetBtn.clicked.disconnect (self.resetSliders )
                self.ui.CloseBtn.clicked.disconnect (self.closeBtnClk )                
                for comboBoxIndex, scanPhaseComboBox in enumerate (self._scanPhaseComboBoxList) :
                    scanPhaseComboBox.currentIndexChanged.disconnect (self._scanPhaseComboBoxCurrentIndexChangedCallback[comboBoxIndex])        
            except:
                print ("Error disconnecting from QT-Signal-Slots")
            
        
        
        self.ui.DatasetTree.clear ()
        self.ui.DatasetTree.setHeaderLabels (["Datasets", "Description"])
        
        selectedTreeNode = None
        rootNode = None
        if (treeWidgetNode is not None) :
            if (treeWidgetNode.getParent () is not None or treeWidgetNode.getNodeType () == "PesscaraTreeWidgetNode") :
                selectionNode = treeWidgetNode
                prevWidget = treeWidgetNode
                while (treeWidgetNode.getParent () is not None):
                    prevWidget = treeWidgetNode
                    treeWidgetNode = treeWidgetNode.getParent ()                            
                if (treeWidgetNode.getNodeType () == "PesscaraTreeWidgetNode") :
                    treeWidgetNode.loadAllChildTreeNodes ()
                    rootNode, nodeCount, selectedTreeNode = RCC_ScanPhaseSelectionDlg._constructTreeWidgets (self.ui.DatasetTree, treeWidgetNode, selectionNode)
                else:
                    prevWidget.loadAllChildTreeNodes ()
                    rootNode, nodeCount, selectedTreeNode = RCC_ScanPhaseSelectionDlg._constructTreeWidgets (self.ui.DatasetTree, prevWidget, selectionNode)
                self.ui.DatasetTree.addTopLevelItem (rootNode)
                
        self._NiftiDatasetLeaf = RCC_ScanPhaseSelectionDlg._getTreeWidgetLeafList (rootNode)
        
        self._scanPhasePredictionList = []
        self._scanPhasePredictionList.append ((self.ui.PredictionLbl1, self.ui.PhasePredictionLbl1, self.ui.PhasePrediction1SetBtn))
        self._scanPhasePredictionList.append ((self.ui.PredictionLbl2, self.ui.PhasePredictionLbl2, self.ui.PhasePrediction2SetBtn))
        self._scanPhasePredictionList.append ((self.ui.PredictionLbl3, self.ui.PhasePredictionLbl3, self.ui.PhasePrediction3SetBtn))
        self._scanPhasePredictionList.append ((self.ui.PredictionLbl4, self.ui.PhasePredictionLbl4, self.ui.PhasePrediction4SetBtn))
        self._scanPhasePredictionList.append ((self.ui.PredictionLbl5, self.ui.PhasePredictionLbl5, self.ui.PhasePrediction5SetBtn))
        self._scanPhasePredictionList.append ((self.ui.PredictionLbl6, self.ui.PhasePredictionLbl6, self.ui.PhasePrediction6SetBtn))        
        for item in self._scanPhasePredictionList :
            lbl1, lbl2, btn = item 
            lbl1.setEnabled (False)
            lbl2.setEnabled (False)
            lbl2.setText ("")
            btn.setEnabled (False)
        self.ui.PhasePrediction1SetBtn.clicked.connect (self._setScanPhasePredictionBtn1)
        self.ui.PhasePrediction2SetBtn.clicked.connect (self._setScanPhasePredictionBtn2)
        self.ui.PhasePrediction3SetBtn.clicked.connect (self._setScanPhasePredictionBtn3)
        self.ui.PhasePrediction4SetBtn.clicked.connect (self._setScanPhasePredictionBtn4)
        self.ui.PhasePrediction5SetBtn.clicked.connect (self._setScanPhasePredictionBtn5)
        self.ui.PhasePrediction6SetBtn.clicked.connect (self._setScanPhasePredictionBtn6)
        
        self._scanPhaseComboBoxList = []
        self._scanPhaseComboBoxList.append (self.ui.ScanPhaseSel1)
        self._scanPhaseComboBoxList.append (self.ui.ScanPhaseSel2)
        self._scanPhaseComboBoxList.append (self.ui.ScanPhaseSel3)
        self._scanPhaseComboBoxList.append (self.ui.ScanPhaseSel4)
        self._scanPhaseComboBoxList.append (self.ui.ScanPhaseSel5)
        self._scanPhaseComboBoxList.append (self.ui.ScanPhaseSel6)
            
        self._scanPhaseComboBoxCurrentIndexChangedCallback = []
        self._scanPhaseComboBoxCurrentIndexChangedCallback.append (self._scanPhaseComboBox1Changed)
        self._scanPhaseComboBoxCurrentIndexChangedCallback.append (self._scanPhaseComboBox2Changed)
        self._scanPhaseComboBoxCurrentIndexChangedCallback.append (self._scanPhaseComboBox3Changed)
        self._scanPhaseComboBoxCurrentIndexChangedCallback.append (self._scanPhaseComboBox4Changed)
        self._scanPhaseComboBoxCurrentIndexChangedCallback.append (self._scanPhaseComboBox5Changed)
        self._scanPhaseComboBoxCurrentIndexChangedCallback.append (self._scanPhaseComboBox6Changed)
        
        for comboBoxIndex, scanPhaseComboBox in enumerate (self._scanPhaseComboBoxList) :
            scanPhaseComboBox.setEnabled (False)
            scanPhaseComboBox.currentIndexChanged.connect (self._scanPhaseComboBoxCurrentIndexChangedCallback[comboBoxIndex])        
        
        self._datasetUI = []
        self._datasetUI.append (self.ui.NiftiSliceWidget1)
        self._datasetUI.append (self.ui.NiftiSliceWidget2)
        self._datasetUI.append (self.ui.NiftiSliceWidget3)
        self._datasetUI.append (self.ui.NiftiSliceWidget4)
        self._datasetUI.append (self.ui.NiftiSliceWidget5)
        self._datasetUI.append (self.ui.NiftiSliceWidget6)             
        
        self._NiftiWidgetLblUI = []
        self._NiftiWidgetLblUI.append (self.ui.ScanPhaseLbl1)
        self._NiftiWidgetLblUI.append (self.ui.ScanPhaseLbl2)
        self._NiftiWidgetLblUI.append (self.ui.ScanPhaseLbl3)
        self._NiftiWidgetLblUI.append (self.ui.ScanPhaseLbl4)
        self._NiftiWidgetLblUI.append (self.ui.ScanPhaseLbl5)
        self._NiftiWidgetLblUI.append (self.ui.ScanPhaseLbl6)
                
        for lbl in self._NiftiWidgetLblUI :
            palette = QtGui.QPalette()
            palette.setColor (lbl.foregroundRole(), QtGui.QColor ( 115,194, 88))    
            lbl.setPalette (palette)
            lbl.setText ("")
            lbl.setVisible (False)

        self._NiftiWidgetSliceSelectionDictionary = {} 

        self._SliceWidgetRangeErrorLblLst = []
        self._SliceWidgetRangeErrorLblLst.append (None)
        self._SliceWidgetRangeErrorLblLst.append (self.ui.DatasetRangeErrorLbl2)
        self._SliceWidgetRangeErrorLblLst.append (self.ui.DatasetRangeErrorLbl3)
        self._SliceWidgetRangeErrorLblLst.append (self.ui.DatasetRangeErrorLbl4)
        self._SliceWidgetRangeErrorLblLst.append (self.ui.DatasetRangeErrorLbl5)
        self._SliceWidgetRangeErrorLblLst.append (self.ui.DatasetRangeErrorLbl6)      
        for lbl in self._SliceWidgetRangeErrorLblLst :
            if (lbl != None) :
                palette = QtGui.QPalette()
                palette.setColor (lbl.foregroundRole(), QtGui.QColor (255,205,94))    
                lbl.setPalette (palette)            
                lbl.setVisible (False)
                                      
        self._sliderSliceAdjustmentUI = []
        self._sliderSliceAdjustmentUI.append (None)
        self._sliderSliceAdjustmentUI.append (self.ui.sliceOffsetAdjustment2)
        self._sliderSliceAdjustmentUI.append (self.ui.sliceOffsetAdjustment3)
        self._sliderSliceAdjustmentUI.append (self.ui.sliceOffsetAdjustment4)
        self._sliderSliceAdjustmentUI.append (self.ui.sliceOffsetAdjustment5)
        self._sliderSliceAdjustmentUI.append (self.ui.sliceOffsetAdjustment6)        
        
        for slider in self._sliderSliceAdjustmentUI :
            if slider != None :
                slider.setEnabled (False)
                slider.setMinimum (-1)
                slider.setMaximum (1)
                slider.setValue   (0)                        
        self._SelectedSlice = 0        
        self._SliderBaseOffset = [0, 0, 0, 0, 0, 0]
                        
        self.ui.SliceSelectionSlider.setEnabled (False)
        self.ui.SliceLbl.setEnabled (False)
        self.ui.SliceNumberTxt.setEnabled (False)
        
        ROIDataHandle = None
        if (not self._uiSetHounsfieldVisSettings) : 
            if (rootNode != None) :
                ROIDataHandle = self._getTreeNodeWithData (rootNode)            
            if (ROIDataHandle != None) :            
                tempHounsfieldSettings = HounsfieldVisSettings ()
                ROIDefs = ROIDefinitions(self._ProjectDataset.getDocumentUIDFileLogInterface ())                    
                tempDictionary = ROIDictionary (ROIDefs, tempHounsfieldSettings, None)   
                tempDictionary.loadROIDictionaryFromFile (ROIDataHandle, self._ProjectDataset.getDocumentUIDFileLogInterface ())                  
                #tempDictionary.setProjectDataset (self._ProjectDataset)
                self._initHounsfieldVisSettings = tempHounsfieldSettings.copy ()
                tempDictionary.delete () 
                ROIDefs.delete ()
                ROIDataHandle.close ()                         
            elif (activeContourWindow != None) :
                self._initHounsfieldVisSettings = activeContourWindow.getHounsfieldSettingsCopy ()
                self._initVisSettings = activeContourWindow.getVisSettingsCopy ()            
            
        for dataset in self._datasetUI :
            dataset.setHounsfieldVisSettings (self._initHounsfieldVisSettings)
            dataset.setVisSettings (self._initVisSettings)                
        
        self._NifTIDatacache = {}    
        
        self.ui.SliceSelectionWidget.initWidget ()
        self.ui.SliceSelectionWidget.setSliceCount (0, "Z", callback = False) 
        self.ui.SliceSelectionWidget.setEnabled (False)        
        self.ui.SliceSelectionWidget.setFocusPolicy (QtCore.Qt.ClickFocus)        
        
        treeSelectionModel = self.ui.DatasetTree.selectionModel ()
        treeSelectionModel.selectionChanged.connect (self._DatasetSelectionChanged)             
        self.ui.SliceNumberTxt.editingFinished.connect (self.sliceTXTNumberChanged)
        self.ui.SliceSelectionSlider.sliderPressed.connect (self.sliceSelectionSliderChanged_MouseDown)
        self.ui.SliceSelectionSlider.valueChanged.connect (self.sliceSelectionSliderChanged_UpdateWidget1)
        self.ui.SliceSelectionSlider.sliderReleased.connect (self.sliceSelectionSliderChanged_UpdateAll)
        
        for index , slider in enumerate (self._sliderSliceAdjustmentUI) :
            if slider != None :  
                if (index == 0) :
                    slider.valueChanged.connect (self._UIOffsetSliderValueChanged1)
                elif (index == 1) :
                    slider.valueChanged.connect (self._UIOffsetSliderValueChanged2)
                elif (index == 2) :
                    slider.valueChanged.connect (self._UIOffsetSliderValueChanged3)
                elif (index == 3) :
                    slider.valueChanged.connect (self._UIOffsetSliderValueChanged4)
                elif (index == 4) :
                    slider.valueChanged.connect (self._UIOffsetSliderValueChanged5)
                elif (index == 5) :
                    slider.valueChanged.connect (self._UIOffsetSliderValueChanged6)
                else:
                    print ("Unexpected sider")
                
        self.ui.SliceSelectionWidget.addSliceListener (self.sliceSelectionWidgetChanged)
        self.ui.sliderResetBtn.clicked.connect (self.resetSliders )
        self.ui.CloseBtn.clicked.connect (self.closeBtnClk )        
        self._UIInitalized = True        
        if (selectedTreeNode is not None):
            selectedTreeNode.setSelected (True)
            if (sliceNumber is not None) :
                self.setSliceSelection (sliceNumber)
                return
        self._DatasetSelectionChanged ()
    
    # returns a list of tree leafe nodes falling under the node.
    @staticmethod
    def _getTreeWidgetLeafList (node):          
        if (node == None) :
            return []
        childlst = []
        leaflst = []        
        childlst.append (node)
        while (len (childlst) > 0) :
            node = childlst.pop ()                        
            if ( node.childCount ()== 0) :
               leaflst.append (node) 
            else:
                for index in range (node.childCount ()) :
                    childlst.append (node.child (index))
        return leaflst
        
    @staticmethod
    def _updateTreeWidgetFormating (treeWidget) :
        try:
            while treeWidget != None :
                 tpl = treeWidget.data (0, QtCore.Qt.UserRole)        
                 widget = tpl[0]      
                 hasData = widget.getNodeROIDatasetIndicator ()
                 font = treeWidget.font (0)        
                 font.setBold (hasData)
                 treeWidget.setFont (0, font)
                 treeWidget = treeWidget.parent ()                     
        except :
            print ("Error occured updating tree widget")  
    
    
    def _updateScanPhasePredictionColorIndicator (self, index) :
        treeWidget = self._NiftiDatasetsShown[index-1]
        scanPhase, predictedScanPhase = self._getScanPhase (treeWidget)
        predictlbl, vallbl, setbtn = self._scanPhasePredictionList[index]                        
        palette = QtGui.QPalette()
        if (predictedScanPhase != scanPhase) :
             palette.setColor (vallbl.foregroundRole(), QtGui.QColor("red"))
        else:
             palette.setColor (vallbl.foregroundRole(), QtGui.QColor("black"))
        vallbl.setPalette(palette)                  
        
    #Core internal function alled when scan phase ui combo box selection changes
    def _comboBoxChanged (self, index) :       
        if (not self._disableComboBoxChangedEvent) :
            if (index < len (self._NiftiDatasetsShown)) :
                scanPhase = self._scanPhaseComboBoxList[index].itemText (self._scanPhaseComboBoxList[index].currentIndex ())        
                treeWidget = self._NiftiDatasetsShown[index]
                tpl = treeWidget.data (0, QtCore.Qt.UserRole)        
                widget = tpl[0]           
                if (widget in self._scanPhaseCache) :
                    tempDictionary = self._scanPhaseCache[widget]                
                    
                    tempDictionary.setScanPhase (scanPhase)  #upates scan phase selection cache
                    nodeindicatorResult = scanPhase != "Unknown" or (tempDictionary.ROICountDefined () > 0)
                    widget.setROIDatasetIndicator (nodeindicatorResult)                                               
                    self._ProjectDataset.setSelectedScanPhase (widget, scanPhase)
                    self.setProjectDatasetTreeNodeUpdated (widget)
                    
                    self._updateTreeWidgetDescriptions (treeWidget)
                    self._updateScanPhasePredictionColorIndicator (index)
                    


    # UI call backs for combo box selection                        
    def _scanPhaseComboBox1Changed (self):        
        self._comboBoxChanged (0)
        
    def _scanPhaseComboBox2Changed (self):
        self._comboBoxChanged (1)
                           
    def _scanPhaseComboBox3Changed (self):
        self._comboBoxChanged (2)
                           
    def _scanPhaseComboBox4Changed (self):
        self._comboBoxChanged (3)
                           
    def _scanPhaseComboBox5Changed (self):
        self._comboBoxChanged (4)
                           
    def _scanPhaseComboBox6Changed (self):
        self._comboBoxChanged (5)
    
    #gets the scan phase info for a selected tree node.
    def _getScanPhase (self, treeNode) :        
        tpl = treeNode.data (0, QtCore.Qt.UserRole)
        niftiWidget = tpl[0]
        if niftiWidget in self._scanPhaseCache :  #if scan phase is in the cache, return the cached value
                    
            try:
                tags = self._scanPhaseCache[niftiWidget].getDataFileTagManager ()
                if (tags.hasInternalTag ("ML_DatasetDescription")) :
                    predictedScanPhaseTxt = tags.getInternalTag ("ML_DatasetDescription")                                                        
                else:
                    predictedScanPhaseTxt = "Unknown"
            except:
                predictedScanPhaseTxt = "Unknown"
                
            return (self._scanPhaseCache[niftiWidget].getScanPhaseTxt (), predictedScanPhaseTxt)
        else :
            try :  #otherwise load scanphase value from data file
                ROIDefs = ROIDefinitions(self._ProjectDataset.getDocumentUIDFileLogInterface ())                
                tempDictionary = ROIDictionary (ROIDefs, None, None)           
                ROIDataHandle = self._ProjectDataset.getROIDatasetInterface ().getROIDataForDatasetPath (niftiWidget, UserNameStr = self._ProjectDataset.getUserName ())   
                if (ROIDataHandle is not None) :       
                    tempDictionary.loadROIDictionaryFromFile (ROIDataHandle, self._ProjectDataset.getDocumentUIDFileLogInterface (), ProjectDataset = self._ProjectDataset)                         
                    #tempDictionary.setProjectDataset (self._ProjectDataset)
                    if (niftiWidget in self._NifTIDatacache) :
                        tempDictionary.setNIfTIVolumeOrientationDescription (self._NifTIDatacache[niftiWidget].getNIfTIVolumeOrientationDescription ())
                    tempDictionary.setNiftiDataSourceDescription (niftiWidget.getSaveFileNifitFileSourceFileObject ())                
                    self._scanPhaseCache[niftiWidget] = tempDictionary               
                    self._origionalScanPhaseCache[niftiWidget] = tempDictionary.getScanPhaseTxt ()
                    
                    try:
                        tags = tempDictionary.getDataFileTagManager ()
                        if (tags.hasInternalTag ("ML_DatasetDescription")) :
                            predictedScanPhaseTxt = tags.getInternalTag ("ML_DatasetDescription")                                                        
                        else:
                            predictedScanPhaseTxt = "Unknown"
                    except:
                        predictedScanPhaseTxt = "Unknown"
                    
                    return tempDictionary.getScanPhaseTxt (), predictedScanPhaseTxt
                else:
                    return "Tatic_CannotLoad", "Tatic_CannotLoad"
            except :                
                return "Unknown", "Unknown"
        
    #Project scan phase list is update, clears scan phase cache for items set to now non-existant scan phase values.      
    def scanPhaseListChanged (self, newScanPhaseList)  :        
        setUnknownLst = []
        for widget, roidictionary in self._scanPhaseCache.items () :
            if roidictionary.getScanPhaseTxt () not in newScanPhaseList :
                setUnknownLst.append (widget)
        for widget in setUnknownLst :            
            self.setSelectedScanPhase (widget, "Unknown") #upates local ui _getScanPhase
            self._ProjectDataset.setSelectedScanPhase  (widget,  "Unknown") #propogates removal of scan phase to other dialogs.
        self._DatasetSelectionChanged () # rebuilds the UI to refkect the additon/removal of scan phase options

    def setProjectDatasetTreeNodeUpdated (self, selectedNifti) :
        for treeWidget in self._NiftiDatasetLeaf :
            tpl = treeWidget.data (0, QtCore.Qt.UserRole)
            widget = tpl[0]
            if (widget.equals (selectedNifti)) :                
                 #update tree widget to reflect project tree widget
                 self._updateTreeWidgetFormating (treeWidget)
                            
    def setSelectedScanPhase (self, selectedNifti, scanphase):      
        self._disableComboBoxChangedEvent = True
        for index, treeWidget in enumerate (self._NiftiDatasetsShown) :
            tpl = treeWidget.data (0, QtCore.Qt.UserRole)
            widget = tpl[0]
            if (widget.equals (selectedNifti)) :                
                comboBoxSelectedIndex = self._scanPhaseComboBoxList[index].findText(scanphase)                
                if (comboBoxSelectedIndex != -1 and comboBoxSelectedIndex != self._scanPhaseComboBoxList[index].currentIndex ()) :  
                    self._scanPhaseComboBoxList[index].setCurrentIndex (comboBoxSelectedIndex)
                elif (comboBoxSelectedIndex == -1 and scanphase != "Unknown" and  scanphase != "") :
                    for items in self._scanPhaseComboBoxList :
                        items.addItem (scanphase)
                    comboBoxSelectedIndex = self._scanPhaseComboBoxList[index].findText(scanphase)                
                    self._scanPhaseComboBoxList[index].setCurrentIndex (comboBoxSelectedIndex)
                else:
                    scanphase = "Unknown"
                    comboBoxSelectedIndex = self._scanPhaseComboBoxList[index].findText(scanphase)
                    self._scanPhaseComboBoxList[index].setCurrentIndex (comboBoxSelectedIndex)                                                
                 
                _, predictedScanPhase = self._getScanPhase (treeWidget)
                self._updateScanPhasePredictionColorIndicator (index)
                
                    
        for widgets in list (self._scanPhaseCache.keys ()) :
            if (widgets.equals (selectedNifti)) :
                self._scanPhaseCache[widgets].setScanPhase (scanphase)
        self._disableComboBoxChangedEvent = False
                        

    def _getTreeNodeWithData (self, node) :         
        if (node.childCount() > 0):
            for index in range (node.childCount ()) :
                child = node.child (index)
                found = self._getTreeNodeWithData (child)
                if (found != None) :
                    return found 
        else:                    
            tpl = node.data (0, QtCore.Qt.UserRole)            
            widget = tpl[0]            
            if (widget.getNodeROIDatasetIndicator ()) :
                path = self._ProjectDataset.getROIDatasetInterface ().getROIDataForDatasetPath (widget,  UserNameStr = self._ProjectDataset.getUserName (), IgnoreLock = True)            
                if (path is not None) :
                    if (ROIDictionary.contoursExistForDataSet (path.getPath ())) :                                        
                        return path # returned path is closed by caller            
                    path.closeUnsaved () 
                    del path
        return None    
            
        
    def changeEvent (self, event) :        
        result = QMainWindow.changeEvent (self, event)            
        if (not self._DisableWindowChangeEvent) :
            if (self.isActiveWindow() or self.hasFocus ()) :
                if (self != self._ProjectDataset.getActiveWindow ()) :
                    self.updateWindowVisSettings (self._ProjectDataset.getActiveWindow ())
                self._ProjectDataset.setActiveWindow (self)
                #print ("Window Is Active")
        return result
    

                                   
    def updateWindowVisSettings (self, activeContourWindow):
        if (activeContourWindow != None) :
            hounsfieldVisSettings = activeContourWindow.getHounsfieldSettingsCopy ()
            visSettings = activeContourWindow.getVisSettingsCopy ()            
            for dataset in self._datasetUI :
                dataset.setHounsfieldVisSettings (hounsfieldVisSettings)
                dataset.setVisSettings (visSettings)                
        
    def _UIOffsetSliderValueChanged1 (self):    
        self._updateUI (UpdateNiftWidgetViewList=[0])    
    
    def _UIOffsetSliderValueChanged2 (self):    
        self._updateUI (UpdateNiftWidgetViewList=[1])    
        
    def _UIOffsetSliderValueChanged3 (self):    
        self._updateUI (UpdateNiftWidgetViewList=[2])    
        
    def _UIOffsetSliderValueChanged4 (self):    
        self._updateUI (UpdateNiftWidgetViewList=[3])    
    
    def _UIOffsetSliderValueChanged5 (self):    
        self._updateUI (UpdateNiftWidgetViewList=[4])    
    
    def _UIOffsetSliderValueChanged6 (self):    
        self._updateUI (UpdateNiftWidgetViewList=[5])    
        
    def sliceSelectionSliderChanged_UpdateAll (self) :            
        self._SliderChangedUpdateWidgetList = [0,1,2,3,4,5]
        if (not self.setSliceSelection (self.ui.SliceSelectionSlider.maximum () - self.ui.SliceSelectionSlider.value ())) :
            self._updateUI (UpdateNiftWidgetViewList=[1,2,3,4,5])    
    
    def sliceSelectionSliderChanged_MouseDown (self) :
        self._SliderChangedUpdateWidgetList = [0]
    
    def sliceSelectionSliderChanged_UpdateWidget1 (self) :    
        self.setSliceSelection (self.ui.SliceSelectionSlider.maximum () - self.ui.SliceSelectionSlider.value (), UpdateNiftWidgetViewList =self._SliderChangedUpdateWidgetList)
    
    def sliceTXTNumberChanged (self) :            
        try:
            num = int (self.ui.SliceNumberTxt.text ())
            if (num < 0) :
                num = 0
                self.ui.SliceNumberTxt.setText (num)
            if (num > self.ui.SliceSelectionSlider.maximum ()) :
                num = self.ui.SliceSelectionSlider.maximum ()
                self.ui.SliceNumberTxt.setText (num)            
            if str (num) != self.ui.SliceNumberTxt.text () :
                self.ui.SliceNumberTxt.setText (num)                
            self.setSliceSelection (num)
        except:
            self.ui.SliceNumberTxt.setText (str (self.ui.SliceSelectionSlider.maximum () - self.ui.SliceSelectionSlider.value ()))            
     
    def sliceSelectionWidgetChanged (self, coordinate, Axis) :                  
        self.setSliceSelection (coordinate)
      
    def setSliceSelection (self, index, UpdateNiftWidgetViewList =  [0,1,2,3,4,5]) :
        index = int (index)
        if (index != self._SelectedSlice) :
            self._SelectedSlice = index
            self.ui.SliceNumberTxt.setText (str (index))
            self.ui.SliceSelectionSlider.setValue (self.ui.SliceSelectionSlider.maximum () - index)
            self.ui.SliceSelectionWidget.setSelectedSlice (index) 
            self._updateUI (UpdateNiftWidgetViewList = UpdateNiftWidgetViewList)                
            return True
        return False

    # application mouse wheel event. Mouse wheel adjusts selected slice
    def wheelEvent(self,event) :
        if (not self.ui.SliceSelectionSlider.isEnabled ()):
            event.accept()      
            return 
        
        if (self._SelectedSlice == None) :
            event.accept()
            return
            
        numDegrees = event.angleDelta() / 8
        numSteps = numDegrees / 15
        dz = int (numSteps.y ())
        
        slicenumber = int (self._SelectedSlice)
        slicenumber += dz
        if slicenumber < 0 :
               slicenumber = 0               
        elif slicenumber > self.ui.SliceSelectionSlider.maximum () :
               slicenumber = self.ui.SliceSelectionSlider.maximum ()
               if (slicenumber < 0) :
                    slicenumber = 0
        self.setSliceSelection (slicenumber)
        event.accept()    

            
    def _updateOffsetWidgetSlice (self, sliceWidget, sliceZindex, datasetRangeErrorlbl) :         
        if (sliceZindex >= 0 and sliceZindex < sliceWidget.getMaxSliceCount ()):
            #sliceWidget.updateSliceSelection ( sliceZindex, "Z")
            datasetRangeErrorlbl.setVisible (False)
            coordinate = Coordinate ()                       
            coordinate.setCoordinate (( 0, 0, sliceZindex))            
            sliceWidget.initNiftiDataSet (coordinate, "Z")
            self._NiftiWidgetSliceSelectionDictionary[sliceWidget.getNIfTIVolume ()] = int (sliceZindex)
        else:            
            sliceWidget.initNiftiDataSet (None, "Z")
            datasetRangeErrorlbl.setVisible (True)
            
    def _updateUI (self, UpdateNiftWidgetViewList = [0,1,2,3,4,5]) :         
        if (not self._disableUIUpdate) :
            
           if len (self._datasetUI) > 1 : #compute reference offset to main slice as a distance measure used to draw other slices
               NiftiSliceWidget = self._datasetUI[0]
               if (NiftiSliceWidget.isNiftiDataLoaded ()) :             
                   referenceNiftiSliceDelta = int (self._SelectedSlice - self._SelectedSliceInitalPosition)
                   referenceSliceThickness = float (NiftiSliceWidget.getNIfTIVolume ().getVoxelSize()[2])
                   referenceNiftiDistanceDelta = float (referenceNiftiSliceDelta) * referenceSliceThickness 
            
           for index, NiftiSliceWidget in enumerate (self._datasetUI) :
               if (NiftiSliceWidget.isNiftiDataLoaded ()) :                   
                   if (index in UpdateNiftWidgetViewList)  :
                      if (index == 0) :
                          NiftiSliceWidget.updateSliceSelection ( self._SelectedSlice, "Z")                         
                          self._NiftiWidgetSliceSelectionDictionary[NiftiSliceWidget.getNIfTIVolume ()] = int (self._SelectedSlice)    
                          NiftiSliceWidget.update()
                      else:
                          #nifti widgets are initalized     
                          sliceThickness =  float (NiftiSliceWidget.getNIfTIVolume ().getVoxelSize()[2])
                          if (referenceSliceThickness != sliceThickness and sliceThickness > 0.0) : #if reference slice and target slice have different thickeness then compute offset using slice thickness                              
                              sliceThicknessNormalizedReferenceSliceOffset = int (referenceNiftiDistanceDelta / sliceThickness)                                                            
                          else :
                              sliceThicknessNormalizedReferenceSliceOffset = referenceNiftiSliceDelta # otherwise compute offset using slice count
                              
                          self._updateOffsetWidgetSlice (NiftiSliceWidget, sliceThicknessNormalizedReferenceSliceOffset  + self._SliderBaseOffset[index] - self._sliderSliceAdjustmentUI[index].value (), self._SliceWidgetRangeErrorLblLst[index])                             
                          NiftiSliceWidget.update()
           
         
    def _cleanup (self):        
        self._DisableWindowChangeEvent = True
        if (self._hounsfeildDlg != None) :
            self._hounsfeildDlg.close ()
            self._hounsfeildDlg = None 
        self._clearScanPhaseCache ()    
        for dataset in self._datasetUI :                     
            dataset.delete ()       
        self.ui.SliceSelectionWidget.delete ()
        self._datasetUI = []
        self._sliderSliceAdjustmentUI = []
        self._NiftiWidgetLblUI = []
        self._SliceWidgetRangeErrorLblLst = []
        self._NiftiWidgetSliceSelectionDictionary = {}
        self._NiftiDatasetsShown = []
        self._scanPhaseComboBoxList = []
        self._scanPhaseComboBoxCurrentIndexChangedCallback = []    
        self._NiftiDatasetsShown = []
        self._ProjectDataset.closeScanPhaseWindow (self)        
        
        
            
        
    def closeEvent(self,event):                        
        self._cleanup ()       
        QMainWindow.closeEvent (self, event)   
    
    def closeBtnClk (self) : 
        self.close ()
        
    @staticmethod
    def _getTreeNodeCount (node) :
        return node[1]
    
    def resetSliders (self) :
        self._NiftiWidgetSliceSelectionDictionary = {}  
        self._DatasetSelectionChanged ()
        
    def _DatasetSelectionChanged (self):  
        if (self._disableDatasetSelectionChanged):
            return
        self._disableDatasetSelectionChanged = True
        
        self._disableComboBoxChangedEvent = True
        self._disableUIUpdate = True
        self._SelectedSliceInitalPosition = 0
        self._SliderBaseOffset = [0, 0, 0, 0, 0, 0]
        self.ui.SliceSelectionSlider.setEnabled (False)
        self.ui.SliceLbl.setEnabled (False)
        self.ui.SliceNumberTxt.setEnabled (False)
        items = self.ui.DatasetTree.selectedItems()        
        newDataCache = {}
        dataset = []
                
        for item in items :
            test_for_multipleChildern = item
            while (test_for_multipleChildern.childCount() > 0):
                if (test_for_multipleChildern.childCount() > 1):
                    item.setSelected(False)
                    break
                test_for_multipleChildern = test_for_multipleChildern.child(0)                
                
        items = self.ui.DatasetTree.selectedItems()        
        for item_element in items :            
                        
            item = item_element
            while (item.childCount() > 0) :
                item = item.child (0)            
                
            selectedScanPhaseTxt, _ = self._getScanPhase (item)    
            if (selectedScanPhaseTxt == "Tatic_CannotLoad") :
               item.setSelected (False) 
            else:
                niftiDataFileHandler, nodeCount = item.data (0, QtCore.Qt.UserRole)                                
                if (niftiDataFileHandler not in self._NifTIDatacache) :
                    niftiData = None
                    dataFile = niftiDataFileHandler.getNIfTIDatasetFile ()
                    if (dataFile.hasDataFile ()):                                
                        niftiData = NiftiVolumeData (nib.load (dataFile.getFilePath ()))                    
                else:
                    niftiData = self._NifTIDatacache[niftiDataFileHandler]                        
                newDataCache[niftiDataFileHandler] = niftiData
                                               
                self._scanPhaseCache[niftiDataFileHandler].setNIfTIVolumeOrientationDescription (niftiData.getNIfTIVolumeOrientationDescription ())
                                                                        
                if (niftiData != None) :                                       
                    if (niftiData, nodeCount, item) not in dataset :
                        dataset.append ((niftiData, nodeCount, item))                                                            
                
                
        self._NifTIDatacache = newDataCache
        dataset = sorted (dataset, key=RCC_ScanPhaseSelectionDlg._getTreeNodeCount)
        if (len (dataset) > 6) :
            for index in range (6, len (dataset)) :
                treenode = dataset[index][2]
                treenode.setSelected (False)                
            dataset = dataset[:6]
        
        self._NiftiDatasetsShown = []
        for data in dataset :
            self._NiftiDatasetsShown.append (data[2])
        
        for index, nodes in enumerate (dataset):
            treenode = nodes[2]
            self._NiftiWidgetLblUI[index].setVisible (True)
            self._NiftiWidgetLblUI[index].setText (treenode.text (0))                
            
        row = -1
        coordinate = Coordinate ()  
        coordinate.setCoordinate ((0,0,0))    
        scanPhaseList = self._ProjectDataset.getScanPhaseList ()         
        additionalScanPhaseList = []
        for row, nodeData in enumerate (dataset) :                
            nifti = nodeData[0]                    
            self._scanPhaseComboBoxList[row].setEnabled (True)                                    
            self._scanPhaseComboBoxList[row].currentIndexChanged.disconnect (self._scanPhaseComboBoxCurrentIndexChangedCallback[row])
            self._scanPhaseComboBoxList[row].clear ()
            for scanPhase in scanPhaseList :            
                self._scanPhaseComboBoxList[row].addItem (scanPhase)
            for scanPhase in additionalScanPhaseList :
                self._scanPhaseComboBoxList[row].addItem (scanPhase)
            treeWidget = nodeData[2]
            
            selectedScanPhaseTxt, predictedScanPhase = self._getScanPhase (treeWidget)
                        
            tpl = treeWidget.data (0, QtCore.Qt.UserRole)        
            widget = tpl[0]  
            self._ProjectDataset.setSelectedScanPhase (widget, selectedScanPhaseTxt)
            
            index = self._scanPhaseComboBoxList[row].findText(selectedScanPhaseTxt)
            if (index != -1) :  
                self._scanPhaseComboBoxList[row].setCurrentIndex (index)
            else:
                for item in self._scanPhaseComboBoxList :
                    item.addItem (selectedScanPhaseTxt)
                additionalScanPhaseList.append (selectedScanPhaseTxt)
                index = self._scanPhaseComboBoxList[row].findText(selectedScanPhaseTxt)
                self._scanPhaseComboBoxList[row].setCurrentIndex (index)
            self._scanPhaseComboBoxList[row].currentIndexChanged.connect (self._scanPhaseComboBoxCurrentIndexChangedCallback[row])                                                
            
            nodeHasPredictedScanPhase = (predictedScanPhase != "Unknown") 
            predictlbl, vallbl, setbtn = self._scanPhasePredictionList[row]            
            predictlbl.setEnabled (nodeHasPredictedScanPhase)
            vallbl.setEnabled (nodeHasPredictedScanPhase)            
            setbtn.setEnabled (nodeHasPredictedScanPhase)
            if (not nodeHasPredictedScanPhase) :
                vallbl.setText ("") 
                palette = QtGui.QPalette()
                palette.setColor (vallbl.foregroundRole(), QtGui.QColor("black"))
                vallbl.setPalette(palette)                     
            else:
                vallbl.setText (predictedScanPhase) 
                self._updateScanPhasePredictionColorIndicator (row)
            
            self._datasetUI[row].setNiftiDataSet (nifti, self._ProjectDataset)                        
            self._datasetUI[row].initNiftiDataSet (coordinate, "Z")
            if (row == 0) :
                firstObjCoordinateX = int (nifti.getXDimSliceCount () /2)
                firstObjCoordinateY = int (nifti.getYDimSliceCount () /2)
                
                if nifti in self._NiftiWidgetSliceSelectionDictionary :
                    firstObjCoordinateZ = int (self._NiftiWidgetSliceSelectionDictionary[nifti])
                else:
                    firstObjCoordinateZ = int (nifti.getZDimSliceCount () /2)
                                                                            
                self._initHounsfieldVisSettings.setDataSetMaxRange (nifti.getMinMaxValues())
                
                firstAffine = nifti.getAdjustedAffine ()
                pt = np.array ([[firstObjCoordinateX, firstObjCoordinateY, firstObjCoordinateZ, 1.0]], dtype = float)
                worldCord =  np.matmul (firstAffine, pt.T)   
                
                firstObjSliceCount =nifti.getZDimSliceCount () 
                
                self.ui.SliceSelectionSlider.setMinimum (0)
                self.ui.SliceSelectionSlider.setMaximum (firstObjSliceCount - 1)
                self.ui.SliceSelectionSlider.setValue (firstObjCoordinateZ)
                self.ui.SliceSelectionSlider.setEnabled (True)
                self._SelectedSliceInitalPosition = firstObjCoordinateZ
                
                self.ui.SliceSelectionWidget.setSliceCount (firstObjSliceCount, "Z", callback = False) 
                self.ui.SliceSelectionWidget.setEnabled (True)
                self.ui.SliceSelectionWidget.setSelectedSlice (firstObjCoordinateZ)
                                
                self.ui.SliceLbl.setEnabled (True)
                self.ui.SliceNumberTxt.setEnabled (True)                
                self.ui.SliceNumberTxt.setText (str (firstObjCoordinateZ))                
            else:                                
                if nifti in self._NiftiWidgetSliceSelectionDictionary :
                    sliceIndex = int (self._NiftiWidgetSliceSelectionDictionary[nifti])
                else:
                    destAffine = nifti.getAdjustedAffine ()
                    destInverAffin = np.linalg.inv (destAffine)
                    voxelCord = np.matmul (destInverAffin, worldCord)                                
                    sliceIndex = int(voxelCord[2])                                                        
                
                self._SliderBaseOffset[row] =  sliceIndex
                
                upperlimit =  int (nifti.getZDimSliceCount () -1)
                sliderValue = 0
                
                if sliceIndex < 0 :
                    sliderValue = int (sliceIndex)
                elif sliceIndex > upperlimit :
                    sliderValue = -int (upperlimit - sliceIndex)
                
                minValue = -(firstObjCoordinateZ + 1) - upperlimit - abs (sliderValue)
                maxValue = -minValue
                            
                self._sliderSliceAdjustmentUI[row].setEnabled (True)
                self._sliderSliceAdjustmentUI[row].setMinimum (minValue)
                self._sliderSliceAdjustmentUI[row].setMaximum (maxValue)
                self._sliderSliceAdjustmentUI[row].setValue (sliderValue)                
    
        coordinate = None        
        for index in range (row + 1, 6) :         
            predictlbl, vallbl, setbtn = self._scanPhasePredictionList[index]
            predictlbl.setEnabled (False)
            vallbl.setEnabled (False)
            vallbl.setText ("")
            setbtn.setEnabled (False)
            
            self._scanPhaseComboBoxList[index].setEnabled (False)            
            self._scanPhaseComboBoxList[index].currentIndexChanged.disconnect (self._scanPhaseComboBoxCurrentIndexChangedCallback[index])
            self._scanPhaseComboBoxList[index].clear ()
            self._scanPhaseComboBoxList[index].currentIndexChanged.connect (self._scanPhaseComboBoxCurrentIndexChangedCallback[index])
            
            self._NiftiWidgetLblUI[index].setText ("")
            self._NiftiWidgetLblUI[index].setVisible (False)
            self._datasetUI[index].setNiftiDataSet (None, None)                  
            self._datasetUI[index].initNiftiDataSet (coordinate, "Z")        
            self._datasetUI[index].update ()                    
            if (self._sliderSliceAdjustmentUI[index] != None) :
                self._sliderSliceAdjustmentUI[index].setEnabled (False)
        self._disableUIUpdate = False
        self._disableComboBoxChangedEvent = False
        self._updateUI ()
        self._disableDatasetSelectionChanged = False
        
    @staticmethod
    def doesNodeContainPredictedScanPhase (node) :
        nodelst = [node]
        try :
            while (len (nodelst) > 0) :
                node = nodelst.pop ()
                lst = node.getChildernLst ()
                if (len (lst) > 0) :
                    for child in lst :
                        nodelst.append (child)
                elif (node.isScanPhasePredicted ()) :
                    return True
            return False                
        except:
            return False            
        
    @staticmethod
    def _getNodeDescription (node) :        
        scanphaseList = []
        nodelst = [node]
        while (len (nodelst) > 0) :
           node = nodelst.pop ()
           lst = node.getChildernLst ()
           if (len (lst ) > 0) :
               for item in lst :
                   nodelst.append (item)
           else:
               scanPhaseTxt = node.getScanPhaseTxt ()
               if scanPhaseTxt == "Unknown" :
                   scanPhaseTxt = ""
               if (len (scanphaseList) == 0) :
                   scanphaseList = [scanPhaseTxt]
               elif (scanPhaseTxt not in scanphaseList):
                   return "Multiple"
        if (len (scanphaseList) == 1):
            return scanphaseList[0]     
        return "" 
            
            
    @staticmethod
    def _constructTreeWidgets  ( parent, topwidget, searchNode, nodecounter = 0):            
        foundNode = None
        
        nodecounter += 1
        widgetTxt = topwidget.getText ()        
        scanPhaseTxt = RCC_ScanPhaseSelectionDlg._getNodeDescription (topwidget)
        widget = QTreeWidgetItem (parent, [widgetTxt, scanPhaseTxt])
        widget.setData (0, QtCore.Qt.UserRole, (topwidget, nodecounter))
        
        if (topwidget == searchNode) :
            foundNode = widget
        
        font = widget.font (0)        
        font.setBold (topwidget.getNodeROIDatasetIndicator ())        
        widget.setFont (0, font)
        
        if (RCC_ScanPhaseSelectionDlg.doesNodeContainPredictedScanPhase (topwidget)):
            widget.setForeground (1, QtGui.QColor("red"))
        else :
            widget.setForeground (1, QtGui.QColor("black"))

        childList = topwidget.getChildernLst ()
        for child in childList :            
            childtreewidget, nodecounter, tempFound = RCC_ScanPhaseSelectionDlg._constructTreeWidgets (widget, child, searchNode, nodecounter)
            widget.addChild (childtreewidget)
            if (tempFound != None) :
               foundNode = tempFound
        
        #testwidget for multiple childern
        SetExpanded = False
        testnode = widget
        while (testnode.childCount () > 0):
            if (testnode.childCount () > 1) :
                SetExpanded = True
                break
            testnode = testnode.child(0)
                        
        widget.setExpanded (SetExpanded)
        return widget, nodecounter, foundNode     
        
        
    @staticmethod
    def _internalPaseDlgNiftiViewResize (x, y, width, height, resizeHelper, niftislicewidget, descriptionLbl, scanLbl, scanPhaseSel, sliceOffsetSlider, datasetSliceRangeErrorLbl, predictionLbl, predictionLblTxt, predictionSetBtn) :
        resizeHelper.setWidgetXPos (niftislicewidget, x)
        resizeHelper.setWidgetYPos (niftislicewidget, y)
        resizeHelper.setWidth      (niftislicewidget, width - 17)
        resizeHelper.setHeight     (niftislicewidget, height - 63)
        
        if (datasetSliceRangeErrorLbl != None) :
            resizeHelper.setWidgetXPos (datasetSliceRangeErrorLbl, x)
            resizeHelper.setWidth      (datasetSliceRangeErrorLbl, width - 17)
            midpt = y + (height - 63 - datasetSliceRangeErrorLbl.height ()) / 2 
            resizeHelper.setWidgetYPos (datasetSliceRangeErrorLbl, int (midpt))
                        
        if (sliceOffsetSlider != None) :
            resizeHelper.setWidgetXPos (sliceOffsetSlider, x + width - 16)
            resizeHelper.setWidth      (sliceOffsetSlider, 16)
            resizeHelper.setWidgetYPos (sliceOffsetSlider, y)
            resizeHelper.setHeight     (sliceOffsetSlider, height - 63)
        
        resizeHelper.setWidgetXPos (descriptionLbl, x + 10)
        resizeHelper.setWidgetYPos (descriptionLbl, y + 11)
        resizeHelper.setWidth      (descriptionLbl, width - 10 - 8 - 17)
        
        resizeHelper.setWidgetXPos (scanLbl, x)
        resizeHelper.setWidgetYPos (scanLbl, niftislicewidget.pos().y () + niftislicewidget.height() + 41)
        
        resizeHelper.setWidgetXPos (scanPhaseSel, x + 90)
        resizeHelper.setWidgetYPos (scanPhaseSel, niftislicewidget.pos().y () + niftislicewidget.height() + 41)
        resizeHelper.setWidth      (scanPhaseSel, width - 90)
        
        resizeHelper.setWidgetXPos (predictionLbl, x)
        resizeHelper.setWidgetYPos (predictionLbl, niftislicewidget.pos().y () + niftislicewidget.height() + 11)
        resizeHelper.setWidth      (predictionLbl, 71)
        
        resizeHelper.setWidgetXPos (predictionLblTxt, x + 80)
        resizeHelper.setWidgetYPos (predictionLblTxt, niftislicewidget.pos().y () + niftislicewidget.height() + 11)
        resizeHelper.setWidth      (predictionLblTxt, width -  55 - 80)
        
        resizeHelper.setWidgetXPos (predictionSetBtn, x + width -  52)
        resizeHelper.setWidgetYPos (predictionSetBtn, niftislicewidget.pos().y () + niftislicewidget.height() + 11)
        
        
    
    def _internalWindowResize (self, newsize):                            
        if (self._resizeWidgetHelper != None) :                                                           
            self._resizeWidgetHelper.setProportionalHeight (self.ui.DatasetTree, newsize)        
            self._resizeWidgetHelper.setWidgetYPos     (self.ui.SliceLbl, self.ui.DatasetTree.height () + 24)            
            self._resizeWidgetHelper.setWidgetYPos     (self.ui.SliceNumberTxt, self.ui.DatasetTree.height () + 19)                        
            self._resizeWidgetHelper.setWidgetYPos     (self.ui.SliceSelectionWidget, self.ui.SliceNumberTxt.height () +  self.ui.SliceNumberTxt.pos ().y () + 8)            
            self._resizeWidgetHelper.setWidgetYPos     (self.ui.sliderResetBtn, self.ui.DatasetTree.height () + 19)                        

            self._resizeWidgetHelper.setHeight  (self.ui.SliceSelectionWidget, self.height () - self.ui.SliceSelectionWidget.pos ().y () -82)            
            self._resizeWidgetHelper.setAbsoluteHeight  (self.ui.SliceSelectionSlider, newsize)                    
            self._resizeWidgetHelper.setWidgetYPos     (self.ui.CloseBtn, self.height () - 74)            
        
            width  = self.width () - 436
            width  = int (width / 3)                                             
            height = self.height () - 68
            height = int (height / 2)
            
            RCC_ScanPhaseSelectionDlg._internalPaseDlgNiftiViewResize (410, 9, width, height, self._resizeWidgetHelper, self.ui.NiftiSliceWidget1, self.ui.ScanPhaseLbl1, self.ui.PhaseLbl1, self.ui.ScanPhaseSel1, None, None, self.ui.PredictionLbl1 ,self.ui.PhasePredictionLbl1 , self.ui.PhasePrediction1SetBtn)
            RCC_ScanPhaseSelectionDlg._internalPaseDlgNiftiViewResize (410 + width + 10, 9, width, height, self._resizeWidgetHelper, self.ui.NiftiSliceWidget2, self.ui.ScanPhaseLbl2, self.ui.PhaseLbl2, self.ui.ScanPhaseSel2, self.ui.sliceOffsetAdjustment2, self.ui.DatasetRangeErrorLbl2, self.ui.PredictionLbl2 ,self.ui.PhasePredictionLbl2 , self.ui.PhasePrediction2SetBtn)
            RCC_ScanPhaseSelectionDlg._internalPaseDlgNiftiViewResize (410 + width + width + 20, 9, width, height, self._resizeWidgetHelper, self.ui.NiftiSliceWidget3, self.ui.ScanPhaseLbl3, self.ui.PhaseLbl3, self.ui.ScanPhaseSel3, self.ui.sliceOffsetAdjustment3, self.ui.DatasetRangeErrorLbl3, self.ui.PredictionLbl3 ,self.ui.PhasePredictionLbl3 , self.ui.PhasePrediction3SetBtn)            
            RCC_ScanPhaseSelectionDlg._internalPaseDlgNiftiViewResize (410, 9 + height + 7, width, height, self._resizeWidgetHelper, self.ui.NiftiSliceWidget4, self.ui.ScanPhaseLbl4, self.ui.PhaseLbl4, self.ui.ScanPhaseSel4, self.ui.sliceOffsetAdjustment4, self.ui.DatasetRangeErrorLbl4, self.ui.PredictionLbl4 ,self.ui.PhasePredictionLbl4 , self.ui.PhasePrediction4SetBtn)
            RCC_ScanPhaseSelectionDlg._internalPaseDlgNiftiViewResize (410 + width + 10, 9 + height + 7, width, height, self._resizeWidgetHelper, self.ui.NiftiSliceWidget5, self.ui.ScanPhaseLbl5, self.ui.PhaseLbl5, self.ui.ScanPhaseSel5, self.ui.sliceOffsetAdjustment5, self.ui.DatasetRangeErrorLbl5, self.ui.PredictionLbl5 ,self.ui.PhasePredictionLbl5 , self.ui.PhasePrediction5SetBtn)
            RCC_ScanPhaseSelectionDlg._internalPaseDlgNiftiViewResize (410 + width + width + 20, 9 + height + 7, width, height, self._resizeWidgetHelper, self.ui.NiftiSliceWidget6, self.ui.ScanPhaseLbl6, self.ui.PhaseLbl6, self.ui.ScanPhaseSel6, self.ui.sliceOffsetAdjustment6, self.ui.DatasetRangeErrorLbl6, self.ui.PredictionLbl6 ,self.ui.PhasePredictionLbl6 , self.ui.PhasePrediction6SetBtn)
                        
                        
    def resizeEvent (self, qresizeEvent) :                       
        QMainWindow.resizeEvent (self, qresizeEvent)         
        self._internalWindowResize ( qresizeEvent.size () ) 
        
    def HounsfieldVisualizationDlg (self):         
         self._uiSetHounsfieldVisSettings = True           
         if (self._hounsfeildDlg == None) :
             self._hounsfeildDlg = RCC_HounsfieldVisOptionsDlg (self)
         self._hounsfeildDlg.updateHounsfeildDlgSettings (self._initHounsfieldVisSettings)
         self._hounsfeildDlg.show ()
