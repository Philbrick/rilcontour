#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan  4 16:59:58 2017

@author: m160897
"""
from PyQt5 import QtCore
from PyQt5.QtWidgets import  QDialog, QFileDialog
from rilcontourlib.util.rcc_util import ResizeWidgetHelper
from rilcontourlib.ui.qt_ui_autogen.rcc_pesscaramaskviewerdlgAutoGen import Ui_RCC_PesscaraMaskViewer

class PesscaraMaskViewerDlg (QDialog) :
    
    def __init__ (self, parent) :    
        QDialog.__init__ (self, parent)        
        self.ui = Ui_RCC_PesscaraMaskViewer ()            
        self.ui.setupUi (self)    
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, False)         
        self._parentWindow = None
        self._resizeWidgetHelper = ResizeWidgetHelper ()
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.maskList)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.CloseBtn)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.PythonCodeLbl)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.PythonCodeTxt)
        self.ui.maskList.selectionModel().selectionChanged.connect(self.maskListSelectionChanged)                
        self.ui.CloseBtn.clicked.connect (self.closeDlg)        
        
    def _internalWindowResize (self, newsize):                            
        if (self._resizeWidgetHelper != None) :                                                       
            self._resizeWidgetHelper.setProportionalWidth  (self.ui.maskList, newsize)                           
            self._resizeWidgetHelper.setProportionalWidth  (self.ui.PythonCodeTxt, newsize)            
            self._resizeWidgetHelper.setProportionalXPos (self.ui.PythonCodeLbl, newsize)
            self._resizeWidgetHelper.setProportionalXPos (self.ui.PythonCodeTxt, newsize)
            self._resizeWidgetHelper.setAbsoluteHeight  (self.ui.maskList, newsize)            
            self._resizeWidgetHelper.setAbsoluteHeight  (self.ui.PythonCodeTxt, newsize)            
            self._resizeWidgetHelper.setWidgetYPos     (self.ui.CloseBtn, self.height () - 31)            
            self._resizeWidgetHelper.setWidgetXPos     (self.ui.CloseBtn, self.width () - (546-440))            
          
    def done (self, r) :        
        if (self._parentWindow != None):
            self._parentWindow.updatePesscaraVolumeMask (None)
        self._parentWindow = None
        QDialog.done (self, r)
        
    def closeEvent(self,event):  
        if (self._parentWindow != None):
            self._parentWindow.updatePesscaraVolumeMask (None)
        self._parentWindow = None
        QDialog.closeEvent (self, event)
        
    def resizeEvent (self, qresizeEvent) :                       
        QDialog.resizeEvent (self, qresizeEvent)         
        self._internalWindowResize ( qresizeEvent.size () ) 
        
    def maskListSelectionChanged (self) :         
        self.ui.PythonCodeTxt.setText ("")
        if (self._parentWindow != None) :
            items = self.ui.maskList.selectedItems ()  
            if (len (items) > 0) :
                context = items[0].text ()
                self._parentWindow.updatePesscaraVolumeMask (context)

                if (self._parentWindow.NiftiVolumeTreeSelection != None):
                    if (self._parentWindow.NiftiVolumeTreeSelection.getPesscaraInterace () != None ):
                        series = self._parentWindow.NiftiVolumeTreeSelection.getSeries ()
                        if (series != None) :
                            pInterface = self._parentWindow.NiftiVolumeTreeSelection.getPesscaraInterace ()
                            server = pInterface.getServerName ()           
                            project = pInterface.getProject ()
                            user = pInterface.getUser ()
                            password = pInterface.getPassword ()
                            searchExpression = "@SOBJECT(qin/series['code', '=', '%s'])" %(series.code)                            
                            code = ""
                            code += "tempNIfTIFileName = 'tempNIfTIFileName.nii.gz'         # tempNIfTIFileName = filesystem path to save NIfTI file\n"                 
                            code += "tempNIfTIMaskFileName = 'tempNIfTIMaskFileName.nii.gz' # tempNIfTIMaskFileName = filesystem path to save mask file\n"                 
                            code += "\n"
                            code += "try:\n" 
                            code += "    import wget\n" 
                            code += "    import sys\n" 
                            code += "    sys.path.append ('/tiPY')\n"                            
                            code += "    import qin\n" 
                            code += "    from tactic_client_lib import TacticServerStub\n" 
                            code += "except ImportError:\n" 
                            code += "    print ('Error loading qin or tatic server stub libraries')\n" 
                            code += "\n"
                            code += "#Connect to tatic\n" 
                            code += "#\n" 
                            code += "taticServer = TacticServerStub(setup=False)\n" 
                            code += "taticServer.set_server('%s')\n"  % (server) 
                            code += "taticServer.set_project('%s')\n" % (project)
                            code += "ticket = taticServer.get_ticket ('%s', '%s')\n" % (user, password)
                            code += "taticServer.set_login_ticket(ticket)\n" 
                            code += "\n"
                            code += "#Get Image from server\n"                
                            code += "try :\n"
                            code += "    expression = \"%s\"\n"  % (searchExpression)
                            code += "    SeriesLst = qin.Series.search (expression = expression, server=taticServer)\n"
                            code += "    Series = SeriesLst[0]\n" 
                            code += "    pathsa = taticServer.get_paths (Series.__search_key__, context='image', version=-1, file_type='*', versionless=True)\n" 
                            code += "    file_loca= pathsa['web_paths']\n"
                            code += "    wget.download(file_loca[0], out=tempNIfTIFileName)\n"
                            code += "    print ('Original downloaded.')\n"
                            code += "    pathsa = taticServer.get_paths (Series.__search_key__, context='%s', version=-1, file_type='*', versionless=True)\n" % (context)
                            code += "    file_loca= pathsa['web_paths']\n"
                            code += "    wget.download(file_loca[0], out=tempNIfTIMaskFileName)\n"
                            code += "    print ('Mask downloaded.')\n"
                            code += "except:\n"
                            code += "    print ('Could not download image mask from pesscara')"
                            self.ui.PythonCodeTxt.setText (code)
                return            
            self._parentWindow.updatePesscaraVolumeMask (None)
                
    def closeDlg (self) :        
        if (self._parentWindow != None):
            self._parentWindow.updatePesscaraVolumeMask (None)
        self._parentWindow = None
        self.close ()
        
    def updateMaskList (self, parentWindow) :
        self._parentWindow = parentWindow
        self.ui.maskList.clear ()
        if (self._parentWindow != None) :
            if (self._parentWindow.NiftiVolumeTreeSelection != None):
                if (self._parentWindow.NiftiVolumeTreeSelection.getPesscaraInterace () != None ):
                    series = self._parentWindow.NiftiVolumeTreeSelection.getSeries ()
                    if (series != None) :
                        contextMap = series.getContextMap ()
                        #context = self._parentWindow.NiftiVolumeTreeSelection.getPesscaraContex ()   
                        silentfail = 0
                        for maskContext in list (contextMap.keys ()) :                                                        
                            if (maskContext != "image") :
                                try:
                                    filename = series.getFilenameForContext (maskContext)
                                    if (len (filename) > 0) :
                                        filename = filename[len (filename)-1]
                                        if filename.lower().endswith (".nii.gz") :
                                            self.ui.maskList.addItem (maskContext)
                                        elif filename.lower().endswith (".nii") :
                                            self.ui.maskList.addItem (maskContext)
                                except:
                                    silentfail += 1
