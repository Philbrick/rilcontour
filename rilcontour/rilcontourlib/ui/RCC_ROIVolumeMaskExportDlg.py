#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan  4 16:59:58 2017

@author: m160897
"""
from PyQt5 import QtCore, QtGui
from PyQt5.QtWidgets import  QDialog, QFileDialog, QListWidgetItem
from rilcontourlib.util.rcc_util import ResizeWidgetHelper, PesscaraLoaded
from rilcontourlib.ui.qt_ui_autogen.rcc_roivolumemaskexportdlgAutoGen import Ui_rcc_ROIVolumeMaskExportDlg

class RCC_ROIVolumeMaskExportDlg (QDialog) :
    
    def __init__ (self, parent, ROIDefs, PesscaraContext = "", ROI_FileSystemPath = "", ProjectDataset = None, DisableAppendButton = False, EnableTryLongFileNameExport = False) :        
        self._EnableTryLongFileNameExport = EnableTryLongFileNameExport
        self._ProjectDataset = ProjectDataset
        self._oldDlgROI_FileSystemPath = ROI_FileSystemPath
        self._ExportROIMaskAsBoundingBox = False
        self._OverwriteExistingFiles = False
        self._ExportNiftiFile = True      
        self._pointCrossSliceSize = 1
        self._pointInSliceSize = 9
        self._TryExportLongFileNames = False
        SelectedROINameList = []        
        oldExportDest = ""
        self._appendFileNameCheckState = False        
        if (self._ProjectDataset != None):
            settings = self._ProjectDataset.getExportVolumeMaskSettings ()
            if (len (settings) > 0) :
                try :
                    self._appendFileNameCheckState, self._OverwriteExistingFiles, oldExportDest, ROIFSPath, oldDlgROI_FileSystemPath, SelectedROINameList, self._pointCrossSliceSize, self._pointInSliceSize , self._ExportROIMaskAsBoundingBox, self._ExportNiftiFile, self._TryExportLongFileNames =  settings
                except :
                    print ("A Error occured retriving old dialog export settings")
                if (oldDlgROI_FileSystemPath == ROI_FileSystemPath) :
                    ROI_FileSystemPath = ROIFSPath
        
        self._result = "Cancel"
        QDialog.__init__ (self, None)
        self._roiDef = ROIDefs
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)         
        self.ui = Ui_rcc_ROIVolumeMaskExportDlg ()        
        self.ui.setupUi (self)    
       
        self._resizeWidgetHelper = ResizeWidgetHelper ()
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.ROIVolumeName)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.line_1)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.line_2)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.line_3)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.line_4)
        
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.ExportVolumeMaskToFileSystemOption)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.ExportVolumeMaskToPesscaraOption)
        
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.PathLbl)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.PathTxt)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.PathSelBtn)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.ContextLbl)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.ContextTxt)
                
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.exportNIfTIFileChkBox)        
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.defineAreaROIasBoundingBox)        
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.OverwriteExistingFilesChkBox)
        
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.ExportLongFileNamesChkBox)
        
        
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.AppendFileNameChkBox)
        self.ui.AppendFileNameChkBox.setChecked (self._appendFileNameCheckState)        
        
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.PointROILbl)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.InSliceLbl)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.CrossSliceLbl)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.InSliceTxt)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.CrossSliceTxt)
        
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.InSliceSlider)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.CrossSliceSlider)
        
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.ROIObjectExportLbl)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.ROIObjectList)
        
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.CancelBtn)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.ExportBtn)

        self.ui.ExportBtn.setEnabled (False)
        self.ui.InSliceTxt.setText (str (self._pointInSliceSize))        
        self.ui.CrossSliceTxt.setText (str (self._pointCrossSliceSize))
        self.ui.InSliceSlider.setValue (self._pointInSliceSize)
        self.ui.CrossSliceSlider.setValue (self._pointCrossSliceSize)
        self.ui.exportNIfTIFileChkBox.setChecked (self._ExportNiftiFile)
        self.ui.defineAreaROIasBoundingBox.setChecked (self._ExportROIMaskAsBoundingBox)        
        self.ui.OverwriteExistingFilesChkBox.setChecked (self._OverwriteExistingFiles)
        self.ui.ExportLongFileNamesChkBox.setChecked (self._TryExportLongFileNames)
        self.ui.InSliceTxt.editingFinished.connect    (self.InSliceTxtChange)
        self.ui.CrossSliceTxt.editingFinished.connect (self.CrossSliceTxtChange)     
        self.ui.ROIVolumeName.editingFinished.connect (self.canExport)
        self.ui.InSliceSlider.valueChanged.connect (self.InSliceSliderChange)        
        self.ui.CrossSliceSlider.valueChanged.connect (self.CrossSliceSliderChange)                  
        self._txtChanging = False
        self.setPointSettingsEnabled (False)                
        RCC_ROIVolumeMaskExportDlg._setListUI (self.ui.ROIObjectList, self._roiDef , True) 
                
        self.ui.ROIObjectList.selectionModel().selectionChanged.connect(self.ListSelectionChanged)        
        
        if (PesscaraContext == "" or not PesscaraLoaded.arePesscaraLibrarysLoaded ()):
            self.setPesscaraExportEnabled (False)
        else:
            self.ui.ContextTxt.setText(PesscaraContext)
            #if running from pesscara disable export to file system.  code currently has bugs does not work in that context.
            #nifti as nifit file is not 
            #self.ui.ExportVolumeMaskToFileSystemOption.setEnabled (False)            
            #self.ui.PathSelBtn.setEnabled (False)
        
        if (ROI_FileSystemPath != "") :
            self.ui.PathTxt.setText (ROI_FileSystemPath)
        else:
            self.ui.ExportVolumeMaskToFileSystemOption.setEnabled (False)            
        
        self.ui.PathSelBtn.clicked.connect (self.SelectPath) 
        
        self.ui.ExportVolumeMaskToFileSystemOption.clicked.connect (self.canExport)
        self.ui.ExportVolumeMaskToPesscaraOption.clicked.connect (self.canExport)

        self.ui.CancelBtn.clicked.connect (self.Cancel)
        self.ui.ExportBtn.clicked.connect (self.Export)
        if (not self.ui.ExportVolumeMaskToPesscaraOption.isEnabled () and self.ui.ExportVolumeMaskToFileSystemOption.isEnabled ()) :
            self.ui.ExportVolumeMaskToFileSystemOption.setChecked (True)
        if (not self.ui.ExportVolumeMaskToFileSystemOption.isEnabled () and not self.ui.PathSelBtn.isEnabled () and self.ui.ExportVolumeMaskToPesscaraOption.isEnabled ()) :
            self.ui.ExportVolumeMaskToPesscaraOption.setChecked (True)        
        self.canExport ()  
        self._SelectedROILst = []
        self._ExportDEST = ""
        self._ExportPath = ""
        self._ExportName = ""
        self.ui.PointROILbl.setText ("Point ROI object size (%d voxels): " % (self._pointInSliceSize * self._pointCrossSliceSize))
        
        if (len (SelectedROINameList) > 0) :
            for index in range (self.ui.ROIObjectList.count ()) :
                item = self.ui.ROIObjectList.item(index)
                ROIName = item.data(QtCore.Qt.UserRole)
                if ROIName in SelectedROINameList :                
                    item.setSelected (True)
        
        if (oldExportDest == "FileSystem" and self.ui.ExportVolumeMaskToFileSystemOption.isEnabled ()) :
            self.ui.ExportVolumeMaskToFileSystemOption.setChecked (True)
        elif (oldExportDest == "PESSCARA" and self.ui.ExportVolumeMaskToPesscaraOption.isEnabled ()) :
            self.ui.ExportVolumeMaskToPesscaraOption.setChecked (True)
            
        if (DisableAppendButton) :            
            self.ui.AppendFileNameChkBox.setEnabled (False)
            
        self.canExport ()  
        
    @staticmethod
    def _setListUI (lst, roiDefs, hideundefined):
        lst.clear ()
        if (not hideundefined) :
                item = QListWidgetItem()     
                item.setText ("None")
                item.setData(QtCore.Qt.UserRole, "None")
                item.setForeground (QtGui.QColor (0,0,0))
                lst.addItem (item)    
        namelist =  roiDefs.getNameLst ()        
        for name in namelist: 
                item = QListWidgetItem() 
                item.setText (name + "   [Mask value: " + str (roiDefs.getROIMaskValue (name)) +"]") 
                item.setData(QtCore.Qt.UserRole, name)
                color  = roiDefs.getROIColor (name)
                item.setForeground (color)
                lst.addItem (item)      
                
    def canExport (self):    
        self._ExportDEST = ""
        self._ExportPath = ""
        self._ExportName = ""  
        self._ExportROIMaskAsBoundingBox = self.ui.defineAreaROIasBoundingBox.isChecked ()    
        self._OverwriteExistingFiles = self.ui.OverwriteExistingFilesChkBox.isChecked ()
        self._ExportPesscaraContext = ""
        self._appendFileNameCheckState = self.ui.AppendFileNameChkBox.isChecked ()         
        self._TryExportLongFileNames = self.ui.ExportLongFileNamesChkBox.isChecked ()
        if (self.ui.ExportVolumeMaskToPesscaraOption.isChecked ()) :
            self.ui.exportNIfTIFileChkBox.setEnabled (False)
            self.ui.OverwriteExistingFilesChkBox.setEnabled (False)
            self._ExportNiftiFile = False
            self.ui.ExportVolumeMaskToFileSystemOption.setEnabled (False)
            self._ExportPesscaraContext = self.ui.ContextTxt.text ()
            self.ui.ExportLongFileNamesChkBox.setEnabled (False)
        else:
            self.ui.exportNIfTIFileChkBox.setEnabled (True)
            self.ui.OverwriteExistingFilesChkBox.setEnabled (True)
            self._ExportNiftiFile = self.ui.exportNIfTIFileChkBox.isChecked ()
            self.ui.ExportVolumeMaskToFileSystemOption.setEnabled (True)
            self.ui.ExportLongFileNamesChkBox.setEnabled (not self._ProjectDataset.getROIDatasetInterface().isPesscaraInterface () and self._EnableTryLongFileNameExport)
        
        if self.ui.ExportVolumeMaskToFileSystemOption.isChecked () or self.ui.ExportVolumeMaskToPesscaraOption.isChecked () :            
            txt = self.ui.ROIVolumeName.text()
            txt = txt.strip ()
            if len (txt) > 0 :
                if (len (self.ui.ROIObjectList.selectedItems ()) > 0) :
                    self.ui.ExportBtn.setEnabled (True)
                    
                    self._ExportName = txt                    
                    if (self.ui.ExportVolumeMaskToFileSystemOption.isChecked ()) :
                        self._ExportDEST = "FileSystem"    
                        self._ExportPath = self.ui.PathTxt.text ()                        
                    else:
                        self._ExportDEST = "PESSCARA"                            
                    return
          
        self.ui.ExportBtn.setEnabled (False)
    
    def getAppendFileNameCheckState (self) :    
        return self._appendFileNameCheckState
    
    def SelectPath (self):
        dlg= QFileDialog( self )
        dlg.setWindowTitle( 'Select the directory to save ROI mask data files to:' )
        dlg.setViewMode( QFileDialog.Detail )    
        dlg.setFileMode (QFileDialog.Directory)
        dlg.setNameFilters( [self.tr('All Files (*)')] )
        dlg.setDefaultSuffix( '' )
        dlg.setAcceptMode (QFileDialog.AcceptOpen)
        if dlg.exec_() and (len (dlg.selectedFiles())== 1):
            path = dlg.selectedFiles()[0]                        
            self.ui.PathTxt.setText (path)
            self.ui.ExportVolumeMaskToFileSystemOption.setEnabled (True)
            if (not self.ui.ExportVolumeMaskToPesscaraOption.isEnabled ()) :
                self.ui.ExportVolumeMaskToFileSystemOption.setChecked (True)
        self.canExport ()
            
        
    def InSliceSliderChange (self):
        self._pointInSliceSize = int (self.ui.InSliceSlider.value ())
        self._pointCrossSliceSize = int (self.ui.CrossSliceSlider.value ())
        self.ui.PointROILbl.setText ("Point ROI object size (%d voxels): " % (self._pointInSliceSize * self._pointCrossSliceSize))
        if (not self._txtChanging) :
            self.ui.InSliceTxt.setText ( str (self.ui.InSliceSlider.value ()))
            
    def CrossSliceSliderChange (self):
        self._pointInSliceSize = int (self.ui.InSliceSlider.value ())
        self._pointCrossSliceSize = int (self.ui.CrossSliceSlider.value ())
        self.ui.PointROILbl.setText ("Point ROI object size (%d voxels): " % (self._pointInSliceSize * self._pointCrossSliceSize))
        if (not self._txtChanging) :
            self.ui.CrossSliceTxt.setText ( str (self.ui.CrossSliceSlider.value ()))
        
    def InSliceTxtChange (self) :
        if (self._txtChanging) :
            return
            
        self._txtChanging = True
        try :
            num = int (self.ui.InSliceTxt.text ())
            if num < 1 :     
                num = 1                
            if num > 99 :
                num = 99
            self.ui.InSliceTxt.setText (str (num))
            self.ui.InSliceSlider.setValue (num)
        except:
            val =  self.ui.InSliceSlider.value ()
            self.ui.InSliceTxt.setText (str (val))
        self._txtChanging = False
        
    def CrossSliceTxtChange (self) :        
        if (self._txtChanging) :
            return            
        self._txtChanging = True
        try :
            num = int (self.ui.CrossSliceTxt.text ())
            if num < 1 :     
                num = 1                
            if num > 99 :
                num = 99
            self.ui.CrossSliceTxt.setText (str (num))
            self.ui.CrossSliceSlider.setValue (num)
        except:
            val =  self.ui.CrossSliceSlider.value ()
            self.ui.CrossSliceTxt.setText (str (val))
        self._txtChanging = False
            
    def setPointSettingsEnabled (self, enable):
        self.ui.PointROILbl.setEnabled (enable)
        self.ui.InSliceLbl.setEnabled (enable)
        self.ui.CrossSliceLbl.setEnabled (enable)
        self.ui.InSliceTxt.setEnabled (enable)
        self.ui.CrossSliceTxt.setEnabled (enable)
        self.ui.InSliceSlider.setEnabled (enable)
        self.ui.CrossSliceSlider.setEnabled (enable)
            
    def setPesscaraExportEnabled (self, enable):
        self.ui.ExportVolumeMaskToPesscaraOption.setEnabled (enable)
        self.ui.ContextTxt.setEnabled (enable)
        self.ui.ContextLbl.setEnabled (enable)      
        self.ui.AppendFileNameChkBox.setEnabled (not enable)
        
    def _internalWindowResize (self, newsize):                            
        if (self._resizeWidgetHelper != None) :                                               
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.ROIVolumeName, newsize)            
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.ExportVolumeMaskToFileSystemOption, newsize)            
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.PathTxt, newsize)            
            self._resizeWidgetHelper.setWidgetXPos     (self.ui.PathSelBtn, self.width () - 89)            
            
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.ExportVolumeMaskToPesscaraOption, newsize)            
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.ContextTxt, newsize)            
            
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.AppendFileNameChkBox, newsize)            
            
            
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.line_1, newsize)            
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.line_2, newsize)            
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.line_3, newsize)            
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.line_4, newsize)            
                        
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.defineAreaROIasBoundingBox, newsize) 
            
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.exportNIfTIFileChkBox, newsize)
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.OverwriteExistingFilesChkBox, newsize)            
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.OverwriteExistingFilesChkBox, newsize)                        
            
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.InSliceSlider, newsize)            
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.CrossSliceSlider, newsize)            
            
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.ROIObjectList, newsize)            
            self._resizeWidgetHelper.setAbsoluteHeight  (self.ui.ROIObjectList, newsize)            
            
            self._resizeWidgetHelper.setWidgetYPos     (self.ui.CancelBtn, self.height () - 29)            
            self._resizeWidgetHelper.setWidgetYPos     (self.ui.ExportBtn, self.height () - 29)            
            self._resizeWidgetHelper.setWidgetXPos     (self.ui.CancelBtn, self.width () - 189)            
            self._resizeWidgetHelper.setWidgetXPos     (self.ui.ExportBtn, self.width () - 99)            
                        
    def resizeEvent (self, qresizeEvent) :                       
        QDialog.resizeEvent (self, qresizeEvent)         
        self._internalWindowResize ( qresizeEvent.size () ) 
        
    def ListSelectionChanged (self) : 
        items = self.ui.ROIObjectList.selectedItems ()                
        pointSelected = False
        self._SelectedROILst = []
        for item in items :
            ROIName = item.data(QtCore.Qt.UserRole)
            if (self._roiDef.isROIPoint (ROIName)):
                pointSelected = True            
            self._SelectedROILst.append (ROIName)
        self.setPointSettingsEnabled (pointSelected)                
        ROIName = ""
        if (len (items) == 1) :
            ROIName = items[0].data(QtCore.Qt.UserRole)
        elif (len (items) > 1) :
            ROIName = "Multiple"        
        if ROIName == "" :
            self.ui.ROIVolumeName.setText ("")        
        else:
            self.ui.ROIVolumeName.setText (ROIName + "_ROI_Mask.nii.gz")        
        
        self.canExport ()
        
    def Cancel (self) :
        self.close ()
        
    def Export (self) :
        self._result = "Export"
        self.canExport () # Ensure Export variables         
        if (self._ProjectDataset is not None):
            SelectedROINameList = []
            for index in range(self.ui.ROIObjectList.count ()) :
                item = self.ui.ROIObjectList.item(index)
                if item.isSelected ():
                    ROIName = item.data(QtCore.Qt.UserRole)
                    SelectedROINameList.append (ROIName)
            self._ProjectDataset.setExportVolumeMaskSettings ((self._appendFileNameCheckState, self._OverwriteExistingFiles, self._ExportDEST, self._ExportPath, self._oldDlgROI_FileSystemPath, SelectedROINameList, self._pointCrossSliceSize, self._pointInSliceSize , self._ExportROIMaskAsBoundingBox, self._ExportNiftiFile, self._TryExportLongFileNames ))        
        self.close ()
