#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  7 17:34:18 2019

@author: m160897
"""

from PyQt5 import QtCore
from PyQt5.QtWidgets import   QDialog
from rilcontourlib.ui.qt_ui_autogen.rcc_loadroidatawarrningdlgautogen import Ui_OpenROIWarrningDlg

class LoadROIWarrningDlg (QDialog) :
      def __init__ (self, parent, title, msg, details = None, ReadWrite = False, ReadOnly = False) :        
          QDialog.__init__ (self, parent)        
          self._result = "Cancel"        
          self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)         
          self.ui = Ui_OpenROIWarrningDlg ()        
          self.ui.setupUi (self)          
          self.setWindowTitle (title)          
          self.setWindowFlags(QtCore.Qt.Dialog | QtCore.Qt.WindowStaysOnTopHint)                    
          self.ui.MessageLbl.setPlainText (msg)
          if details is not None :
              self.ui.DetailsText.setPlainText (details)
              self.ui.HideBtn.setEnabled (True)
              self.ui.ShowDetailsBtn.setEnabled (True)
              self.ui.HideBtn.clicked.connect (self.hideDetailsBtn)
              self.ui.ShowDetailsBtn.clicked.connect (self.showDetailsBtn)
          else:
              self.ui.HideBtn.setEnabled (False)
              self.ui.ShowDetailsBtn.setEnabled (False)          
          self.ui.OpenReadOnlyBtn.setEnabled (ReadOnly)
          if ReadOnly :      
              self.ui.OpenReadOnlyBtn.clicked.connect (self.readOnlyBtn)
          self.ui.OpenReadWriteBtn.setEnabled (ReadWrite)
          if ReadWrite :         
              self.ui.OpenReadWriteBtn.clicked.connect (self.readwritebtn)        
          self.ui.CancelOpenBtn.clicked.connect (self.cancelBtn)          
        
      def readOnlyBtn (self)  :
            self._result = "Readonly"        
            self.accept ()
            
      def readwritebtn (self)  :
            self._result = "Readwrite"        
            self.accept ()
            
      def cancelBtn (self)  :
            self._result = "Cancel"        
            self.reject ()
    
      def _setHeight (self, height):
           self.setMinimumHeight (height)
           self.setMaximumHeight (height)
           self.setGeometry (self.geometry().x(),self.geometry().y(),self.width (),height)
            
      def hideDetailsBtn (self):
            self._setHeight (198)            
            
      def showDetailsBtn (self):
          self._setHeight (374)
           
    
      def getResult (self) :
          return self._result
      
      @staticmethod
      def showLoadROIWarrningDlg (title, msg, details = None, ReadWrite = False, ReadOnly = False) :
          dlg = LoadROIWarrningDlg (None, title, msg, details=details, ReadWrite = ReadWrite, ReadOnly = ReadOnly)
          dlg.exec_ ()
          result =dlg.getResult ()
          del dlg
          return result