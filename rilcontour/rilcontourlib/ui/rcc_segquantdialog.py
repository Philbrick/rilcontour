#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep  6 10:58:00 2019

@author: m160897
"""

from PyQt5 import QtCore
from PyQt5.QtWidgets import  QDialog
from rilcontourlib.util.rcc_util import ResizeWidgetHelper
from rilcontourlib.ui.qt_ui_autogen.rcc_segquantresultsAutogen import Ui_ResultsDialog

class RCC_SegQuantDialog (QDialog) :
    
    def __init__ (self, parent) :                
        self._parent = parent        
        self._ProjectDataset = parent._ProjectDataset
        QDialog.__init__ (self, parent)
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, False)
        self.ui = Ui_ResultsDialog ()              
        self.setWindowFlags(self.windowFlags () | QtCore.Qt.Dialog| QtCore.Qt.WindowStaysOnTopHint )         
        self.ui.setupUi (self)                
        self._resizeWidgetHelper = ResizeWidgetHelper ()
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.CloseBtn)
        self._resizeWidgetHelper.saveWidgetUIPos (self,self.ui.ResultsTabWidget)
        self.ui.CloseBtn.clicked.connect (self.close)
    
    def _internalWindowResize (self, newsize):                            
        if (self._resizeWidgetHelper != None) :                                               
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.ResultsTabWidget, newsize)            
            self._resizeWidgetHelper.setAbsoluteHeight  (self.ui.ResultsTabWidget, newsize)                                                
            self._resizeWidgetHelper.setWidgetXPos  (self.ui.CloseBtn, newsize.width () - 111)                                
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.CloseBtn, newsize.height () - 38)                        
                                                             
    def resizeEvent (self, qresizeEvent) :                       
        QDialog.resizeEvent (self, qresizeEvent)                 
        self._internalWindowResize ( qresizeEvent.size () )            