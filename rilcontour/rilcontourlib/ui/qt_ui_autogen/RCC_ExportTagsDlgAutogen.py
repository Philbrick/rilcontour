# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'RCC_ExportTags.ui'
#
# Created by: PyQt5 UI code generator 5.15.0
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_RCC_ExportTagDlg(object):
    def setupUi(self, RCC_ExportTagDlg):
        RCC_ExportTagDlg.setObjectName("RCC_ExportTagDlg")
        RCC_ExportTagDlg.resize(293, 222)
        RCC_ExportTagDlg.setMinimumSize(QtCore.QSize(293, 222))
        RCC_ExportTagDlg.setSizeGripEnabled(True)
        RCC_ExportTagDlg.setModal(True)
        self.tags_tableView = QtWidgets.QTableView(RCC_ExportTagDlg)
        self.tags_tableView.setGeometry(QtCore.QRect(10, 30, 271, 91))
        self.tags_tableView.setObjectName("tags_tableView")
        self.Add_Btn = QtWidgets.QPushButton(RCC_ExportTagDlg)
        self.Add_Btn.setGeometry(QtCore.QRect(10, 160, 40, 40))
        self.Add_Btn.setText("")
        self.Add_Btn.setObjectName("Add_Btn")
        self.label = QtWidgets.QLabel(RCC_ExportTagDlg)
        self.label.setGeometry(QtCore.QRect(10, 10, 271, 16))
        self.label.setObjectName("label")
        self.Remove_Btn = QtWidgets.QPushButton(RCC_ExportTagDlg)
        self.Remove_Btn.setGeometry(QtCore.QRect(60, 160, 40, 40))
        self.Remove_Btn.setText("")
        self.Remove_Btn.setObjectName("Remove_Btn")
        self.Cancel_Btn = QtWidgets.QPushButton(RCC_ExportTagDlg)
        self.Cancel_Btn.setGeometry(QtCore.QRect(200, 170, 81, 31))
        self.Cancel_Btn.setObjectName("Cancel_Btn")
        self.Ok_Btn = QtWidgets.QPushButton(RCC_ExportTagDlg)
        self.Ok_Btn.setGeometry(QtCore.QRect(110, 170, 81, 31))
        self.Ok_Btn.setObjectName("Ok_Btn")
        self.LimitToSerieWithDataChkBox = QtWidgets.QCheckBox(RCC_ExportTagDlg)
        self.LimitToSerieWithDataChkBox.setGeometry(QtCore.QRect(20, 130, 251, 20))
        self.LimitToSerieWithDataChkBox.setChecked(True)
        self.LimitToSerieWithDataChkBox.setObjectName("LimitToSerieWithDataChkBox")

        self.retranslateUi(RCC_ExportTagDlg)
        QtCore.QMetaObject.connectSlotsByName(RCC_ExportTagDlg)

    def retranslateUi(self, RCC_ExportTagDlg):
        _translate = QtCore.QCoreApplication.translate
        RCC_ExportTagDlg.setWindowTitle(_translate("RCC_ExportTagDlg", "Series Annotation CSV Export"))
        self.label.setText(_translate("RCC_ExportTagDlg", "Series Annotations to Export"))
        self.Cancel_Btn.setText(_translate("RCC_ExportTagDlg", "Cancel"))
        self.Ok_Btn.setText(_translate("RCC_ExportTagDlg", "Ok"))
        self.LimitToSerieWithDataChkBox.setText(_translate("RCC_ExportTagDlg", "Limit to series with data."))
