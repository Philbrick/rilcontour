#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 22 14:36:58 2016

@author: m160897
"""
import tempfile
import os
import nibabel as nib
import locale
import numpy as np
import time
import copy
import math
import json
import shutil
import subprocess
import datetime
import functools
import platform
from PyQt5 import QtCore, QtGui
from PyQt5.QtWidgets import  QMainWindow, QDialog, QListWidgetItem, QFileDialog, QProgressDialog
from PyQt5.QtCore import QThread, pyqtSignal, QDate 
from rilcontourlib.ui.qt_ui_autogen.rcc_dataset import Ui_Dataset
from rilcontourlib.util.FileUtil import FileUtil
from rilcontourlib.util.rcc_util import ResizeWidgetHelper, NiftiVolumeData, ExportVolumeStatsCSV, PesscaraLoaded, PlatformMP, MessageBoxUtil, RelativePathUtil, FileInterface, RCC_NoCancelProgressDlg, isDICOM2NIfTIPackageInstalled
from rilcontourlib.dataset.roi.rcc_roi import ROIDefinitions, ROIDef,ROIDictionary
from rilcontourlib.pesscara.rcc_pesscarainterface import RCC_PesscaraInterface
from rilcontourlib.ui.RCC_ROIVolumeMaskExportDlg import RCC_ROIVolumeMaskExportDlg
from rilcontourlib.ui.rcc_contourwindow import RCC_ROIEditorDlg
from rilcontourlib.ui.RCC_ScanPhaseSelectionDlg import RCC_ScanPhaseSelectionDlg
from rilcontourlib.ui.qt_ui_autogen.rcc_projectroieditor import Ui_RCC_ProjectROIEditor
from rilcontourlib.ui.qt_ui_autogen.rcc_newprojectwizard import Ui_RCC_NewProjectWizard
from rilcontourlib.ui.qt_ui_autogen.RCC_UserNameEditorAutoGen import Ui_RCC_UserNameEditor
from rilcontourlib.ui.qt_ui_autogen.RCC_RegistrationUIIndicatorDlgAutoGen import Ui_rcc_RegistrationIndicatorFilter
from rilcontourlib.ui.qt_ui_autogen.rcc_advancedfilefilterAutogen import Ui_AdvancedFileFilters
from rilcontourlib.ui.RCC_DatasetTagEditorUIDlg import RCC_ProjectTagEditorDlg
from rilcontourlib.ui.rcc_datasetcolumnchooser import RCC_DatasetTagingDlg
from rilcontourlib.ui.rcc_export_tagsDlg import RCC_ExportTagsDlg
from rilcontourlib.ui.rcc_roimaskimportdlg import RCC_ROIMaskImportDlg
from rilcontourlib.dataset.rcc_DatasetROIInterface import RCC_ROIDataset_FileSystemObject
from PyQt5.QtWidgets import QMenu, QAction
from rilcontourlib.ui.qt_ui_autogen.RCC_PesscaraTemporaryDataStorageDlgAutogen import Ui_RCC_PesscaraTemporaryDataStorageDlg
from rilcontourlib.ui.qt_ui_autogen.rcc_FilePropertiesDlgAutogen import Ui_FilePropertiesDlg
from rilcontourlib.ui.qt_ui_autogen.RCC_AboutDlgAutogen import Ui_RCC_About
from rilcontourlib.ui.qt_ui_autogen.rcc_renamelongfilenamedlgautogen import Ui_RenameLongfileNameDlg
import pkg_resources
import sys
from rilcontourlib.util.PandasCompatibleTable import PandasCompatibleTable
from rilcontourlib.ui.RCC_FileVersionUI import FileVersionUI
from rilcontourlib.ui.RCC_FilePLockUI import FilePLockUI
from rilcontourlib.dataset.rcc_multiusermanager import MultiUserManager
from rilcontourlib.util.DateUtil import DateUtil, TimeUtil
from rilcontourlib.util.ErrorListDlg import ErrorListDlg
from rilcontourlib.util.LongFileNameAPI import LongFileNameAPI
from rilcontourlib.dataset.RCC_ROIFileLocks import PersistentFSLock
from rilcontourlib.ui.qt_ui_autogen.rcc_importimagingdlg import Ui_ImportImagingDlg
from six import *
from rilcontourlib.ui.qt_ui_autogen.RCC_HelpDlgAutogen import Ui_RCC_Help


class RCC_ImportImagingDlg (QDialog) :
    def __init__ (self,  ProjectDataset, parent = None) :        
        QDialog.__init__ (self, None)      
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)  
        self._ProjectDataset = ProjectDataset
        state = ProjectDataset.getImportImagingDlgState ()
        self.ui = Ui_ImportImagingDlg ()        
        self.ui.setupUi (self)          
        self.setWindowModality(QtCore.Qt.ApplicationModal)        
        self.ui.ImportUsingDicomOrgRadioBtn.toggled.connect (self._importUsingDicomOrgSelected)
        self._resizeWidgetHelper = ResizeWidgetHelper ()
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.ImportPathTxtBox)        
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.dicomOptionsGroupBox)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.PatientIDMappingTxtBox)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.ImportUsingDicomOrgRadioBtn)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.importUsingDirectoryOrganizationRadioBtn)   
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.perredformatComboBox)   
        self.ui.OkBtn.setEnabled (False)
        self.ui.OkBtn.clicked.connect (self._okClicked)
        self.ui.CancelBtn.clicked.connect (self._cancelClicked)
        self.ui.importBtn.clicked.connect (self._selectDir) 
        if state is not None :
            preferredFormatCurrentIndex = 0
            if  "PreferredFormat" in state :
                if state["PreferredFormat"] == ".nii.gz" :
                    preferredFormatCurrentIndex = 0
                else:
                    preferredFormatCurrentIndex = 1
            self.ui.perredformatComboBox.setCurrentIndex (preferredFormatCurrentIndex)
            if "Organization" in state :
                if state["Organization"] == "Dicom" :
                    self.ui.ImportUsingDicomOrgRadioBtn.setChecked (True)
                else:
                    self.ui.importUsingDirectoryOrganizationRadioBtn.setChecked (True)
            if "PatientIDMapping" in state :
                self.ui.PatientIDMappingTxtBox.setText (state["PatientIDMapping"])
        valSet = self.ui.ImportUsingDicomOrgRadioBtn.isChecked ()
        self.ui.PatientIDTranslationLbl.setEnabled (valSet)
        self.ui.PatientIDMappingTxtBox.setEnabled (valSet)
        if state is None :
            self._setState ()
            self._ProjectDataset.setImportImagingDlgState (self._state)
        
    def _importUsingDicomOrgSelected (self, val = None) :
        valSet = self.ui.ImportUsingDicomOrgRadioBtn.isChecked ()
        self.ui.PatientIDTranslationLbl.setEnabled (valSet)
        self.ui.PatientIDMappingTxtBox.setEnabled (valSet)
        
    def _selectDir (self) :
         dlg= QFileDialog( None )
         dlg.setWindowTitle( 'Select Directory to Import Imaging Files From' )
         dlg.setViewMode( QFileDialog.Detail )    
         dlg.setFileMode (QFileDialog.Directory)             
         dlg.setAcceptMode (QFileDialog.AcceptOpen)
         if dlg.exec_() and (len (dlg.selectedFiles())== 1):
             path = dlg.selectedFiles()[0]
             if os.path.isdir (path) :
                 self.ui.ImportPathTxtBox.setText (path)
                 self.ui.OkBtn.setEnabled (True)
                 return
         self.ui.ImportPathTxtBox.setText ("")
         self.ui.OkBtn.setEnabled (False)
   
    def _setState (self):
        if self.ui.ImportUsingDicomOrgRadioBtn.isChecked () :
            organization = "Dicom"
        else:
            organization = "Directory"
        if self.ui.perredformatComboBox.currentIndex () == 0 :
            preferredFormat = ".nii.gz"
        else:
            preferredFormat = ".nii"
        self._state = {"Path" : self.ui.ImportPathTxtBox.text (), "PreferredFormat": preferredFormat,"Organization" : organization, "PatientIDMapping" : self.ui.PatientIDMappingTxtBox.text () }
        
    def _cancelClicked (self) :
        self._setState ()
        self._ProjectDataset.setImportImagingDlgState (self._state)
        self.reject ()
        
    def _okClicked (self) :
        self._setState ()
        self._ProjectDataset.setImportImagingDlgState (self._state)
        self.accept ()
    
    def getState (self):
        return self._state
    
    def _internalWindowResize (self, newsize):                            
        try:
            if (self._resizeWidgetHelper is not None) :                                               
                self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.ImportPathTxtBox, newsize)   
                self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.PatientIDMappingTxtBox, newsize)            
                self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.dicomOptionsGroupBox, newsize)         
                self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.ImportUsingDicomOrgRadioBtn, newsize)    
                self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.importUsingDirectoryOrganizationRadioBtn, newsize) 
                self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.perredformatComboBox, newsize)                               
                width = self.width ()                 
                self._resizeWidgetHelper.setWidgetXPos  (self.ui.importBtn, width - 183)            
                self._resizeWidgetHelper.setWidgetXPos  (self.ui.OkBtn, width - 133)        
                self._resizeWidgetHelper.setWidgetXPos  (self.ui.CancelBtn, width - 253)                        
        except:
            pass
        
    def resizeEvent (self, qresizeEvent) :                       
        QDialog.resizeEvent (self, qresizeEvent)                 
        self._internalWindowResize ( qresizeEvent.size () )   

class RCC_RenameLongFileName (QDialog) :
    def __init__ (self, FileNode, ProjectDataset, parent) :        
        QDialog.__init__ (self, None)        
        self._FileTreeNode = FileNode        
        self._App = ProjectDataset.getApp ()
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)         
        self.ui = Ui_RenameLongfileNameDlg ()        
        self.ui.setupUi (self)          
        self.setWindowModality(QtCore.Qt.ApplicationModal)        
        self._fHandle = None
        self._resizeWidgetHelper = ResizeWidgetHelper ()
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.FSPathTitle)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.FSPathLbl)        
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.LongFileNameTitle)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.LongfileNamePathLbl)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.LongFileNameLbl)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.FSPath)        
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.LongFileNamePath)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.LongFileName)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.OkBtn)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.CancelBtn)

        try :            
            self.ui.FSPath.setText (FileNode.getPath ())            
        except:
            self.ui.FSPath.setText ("Error")
        try :
            longfileNamePath = FileNode.getTreePath (ReturnFileSystemPath = False)
            lfName = FileNode.getText ()
            parent = FileNode.getParent ()                        
            if parent is not None :
                longfileNamePath = parent.getPath ()
            else:
                rootSearchPath = ProjectDataset.getNIfTIDatasetInterface().getRootSearchPath ()
                longfileNamePath = rootSearchPath
            self.ui.LongFileNamePath.setText (longfileNamePath)
            self.ui.LongFileName.setText (lfName)
            self.ui.OkBtn.setEnabled (True)
        except:
            self.ui.LongFileNamePath.setText ("Error")
            self.ui.LongFileName.setText ("Error")
            self.ui.OkBtn.setEnabled (False)

        self.ui.CancelBtn.clicked.connect (self.close)
        self.ui.OkBtn.clicked.connect (self._tryRenameFile)
        self._resizeWidgetHelper.setWidth  (self, 450)      
        self.exec_ ()
        
    def _tryRenameFile (self) :
        preexistingFileName = self._FileTreeNode.getText ()
        fname = self.ui.LongFileName.text ()
        if fname == preexistingFileName :
            self.close ()
        fname = fname.replace ("/","")
        fname = fname.replace ("\\","")
        fname = fname.strip ()        
        
        if fname == preexistingFileName :
            self.close ()
        if len (fname) <= 0 :
            result = "Long filename must contain non-whitespace characters."
        try:            
            try :
                progdialog = RCC_NoCancelProgressDlg(self) 
                progdialog.setRange (0,0)
                progdialog.setValue (0)
                progdialog.setWindowTitle ("Renaming File")
                progdialog.setLabelText ("Renaming long file name to: " + fname)                 
                result = self._FileTreeNode.renameLongfileName (fname, progdialog, self._App)
            finally:
                try :
                    progdialog.close ()                                                
                except:
                    pass
        except:
            result = "Unexpected error occured"        
        if isinstance(result, str) :
            MessageBoxUtil.showMessage ("Invalid Long File Name", result)
        else:
            self.close ()
        
    def _internalWindowResize (self, newsize):                            
        try:
            if (self._resizeWidgetHelper is not None) :                                               
                self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.FSPathTitle, newsize)            
                self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.FSPath, newsize)                            
                self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.LongFileNameTitle, newsize)            
                self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.LongFileNamePath, newsize)            
                self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.LongFileName, newsize)                            
                width = self.width ()                 
                self._resizeWidgetHelper.setWidgetXPos  (self.ui.OkBtn, width - 229)            
                self._resizeWidgetHelper.setWidgetXPos  (self.ui.CancelBtn, width - 119)                        
        except:
            pass
            
        
        
    def resizeEvent (self, qresizeEvent) :                       
        QDialog.resizeEvent (self, qresizeEvent)                 
        self._internalWindowResize ( qresizeEvent.size () )         

class RCC_FileProperties (QDialog) :
    def __init__ (self, FileNode, ProjectDataset, parent) :        
        QDialog.__init__ (self, None)        
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)         
        self.ui = Ui_FilePropertiesDlg ()        
        self.ui.setupUi (self)          
        self.setWindowModality(QtCore.Qt.ApplicationModal)        
        self._fHandle = None
        self._resizeWidgetHelper = ResizeWidgetHelper ()
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.ProjectPathLbl)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.ImagingPathlbl)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.ROIPathLbl)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.ImagingLongNameLbl)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.ImagingShortNameLbl)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.ImagingSizeLbl)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.ROIPathLbl_2)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.ROIOwnerLbl)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.line)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.line_2)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.line_3)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.CloseBtn)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.ownerlbl)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.versionlbl)
        
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.DatasetCountLbl)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.DatasetCount)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.AnnotatedDatasetLbl)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.AnnotatedDatasetCount)
        
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.DirDatasetCountLbl)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.Dir_DatasetCount)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.DirAnnotatedDatasetLbl)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.DirAnnotatedDatasetCount)
        
        
        self._projectdir = None
        self._rootROISearchPath = None
        self._rootNiftiSearchPath = None
        self._FS_ShortNiftiFilePath = None
        self._ROIFilePath = None
        self.ui.ownerlbl.setText ("NA")
        self.ui.versionlbl.setText ("NA")
        self.ui.versionHistoryBtn.setEnabled (False)
        self.ui.OwnerHistoryBtn.setEnabled (False)
                        
        self._projectdir, _ = os.path.split (ProjectDataset.getPath ())
        self.ui.showProjectFileBtn.setEnabled (os.path.isdir (self._projectdir))
        self.ui.ProjectPathLbl.setText ("Project: " + ProjectDataset.getPath ())
        if ProjectDataset.isPesscaraInterface ()  :            
            try :
                self.ui.ImagingPathlbl.setText (ProjectDataset.getNIfTIDatasetInterface().getPesscaraInterface().getServerName ())
            except:
                self.ui.ImagingPathlbl.setText ("Error")
            try :
                self.ui.ROIPathLbl.setText (ProjectDataset.getNIfTIDatasetInterface().getPesscaraContex ())
            except:
                self.ui.ROIPathLbl.setText ("Error")
            self.ui.showProjectNIfTIBtn.setEnabled (False)
            self.ui.ShowProjectROIBtn.setEnabled (False)
        else:
            try :
                self._rootNiftiSearchPath = ProjectDataset.getNIfTIDatasetInterface().getRootSearchPath ()
                if self._rootNiftiSearchPath is None :
                    self._rootNiftiSearchPath = "Path not set"
                    self.ui.showProjectNIfTIBtn.setEnabled (False)
            except:
                self._rootNiftiSearchPath = "Error"
                self.ui.showProjectNIfTIBtn.setEnabled (False)
            
            try :
                self._rootROISearchPath = ProjectDataset.getROIDatasetInterface().getROIDatasetRootPath ()
                if self._rootROISearchPath is None :
                    self._rootROISearchPath = "Path not set"
                    self.ui.ShowProjectROIBtn.setEnabled (False)
            except:
                self._rootROISearchPath = "Error"
                self.ui.ShowProjectROIBtn.setEnabled (False)
            self.ui.ImagingPathlbl.setText (self._rootNiftiSearchPath)
            self.ui.ROIPathLbl.setText (self._rootROISearchPath)
        
        locale.setlocale(locale.LC_ALL, '')
        
        if ProjectDataset.isPesscaraInterface ()  :     
            self.ui.showNiftiImagingBtn.setEnabled (False)
            self.ui.showROIBtn.setEnabled (False)
            self.ui.ImagingLongNameLbl.setText ("NIfTI imaging stored in server")
            self.ui.ImagingShortNameLbl.setText ("Imaging stored in server")
            self.ui.ImagingSizeLbl.setText ("Unknown")
            
            self.ui.ROIPathLbl_2.setText ("ROI data stored in server")
            self.ui.ROISizeLbl.setText ("Unknown")
            self.ui.ROILastChangedLbl.setText ("Unknown")
            self.ui.ROIOwnerLbl.setText ("Unknown")
            self.ui.DatasetCount.setText ("Not implemented")
            self.ui.AnnotatedDatasetCount.setText ("Not implemented")
            self.ui.Dir_DatasetCount.setText ("Not implemented")
            self.ui.DirAnnotatedDatasetCount.setText ("Not implemented")
        else:           
            try :            
                datasetCount, hasDataCount  = ProjectDataset.getNIfTIDatasetInterface().getDatasetStatistics ()
                self.ui.DatasetCount.setText (str('{:n}'.format(datasetCount)))
                precent = 100.0 * float (hasDataCount) / float (datasetCount)
                self.ui.AnnotatedDatasetCount.setText (str('{:n}'.format(hasDataCount)) + " (%0.1f%%)" % (precent))
            except:
               self.ui.DatasetCount.setText ("Error")
               self.ui.AnnotatedDatasetCount.setText ("Error")
            try :            
                if FileNode is not None and not FileNode.isRootNode () :
                    datasetCount, hasDataCount  = ProjectDataset.getNIfTIDatasetInterface().getDatasetStatistics (FileNode)
                self.ui.Dir_DatasetCount.setText (str('{:n}'.format(datasetCount)))
                precent = 100.0 * float (hasDataCount) / float (datasetCount)
                self.ui.DirAnnotatedDatasetCount.setText (str('{:n}'.format(hasDataCount)) + " (%0.1f%%)" % (precent))
            except:
               self.ui.Dir_DatasetCount.setText ("Error")
               self.ui.DirAnnotatedDatasetCount.setText ("Error")   
               
            try :
                self._FS_ShortNiftiFilePath = FileNode.getPath ()        
                self._NiftiLongFileName = LongFileNameAPI.getLongFileNamePath (self._FS_ShortNiftiFilePath)
                self.ui.ImagingLongNameLbl.setText (self._NiftiLongFileName )
                self.ui.ImagingShortNameLbl.setText (self._FS_ShortNiftiFilePath)
            
                try :
                    if os.path.isdir (self._FS_ShortNiftiFilePath):
                        self.ui.ImagingSizeLbl.setText ("NA")
                    elif os.path.isfile (self._FS_ShortNiftiFilePath):
                        try :
                            fileSize = int (os.path.getsize (self._FS_ShortNiftiFilePath))                         
                            self.ui.ImagingSizeLbl.setText (str('{:n}'.format(fileSize)) + " bytes")
                        except :
                            self.ui.ImagingSizeLbl.setText ("Unknown")
                    else:
                        self.ui.ImagingSizeLbl.setText ("Unknown")
                except:
                    self.ui.ImagingSizeLbl.setText ("Unknown")
                    
                if os.path.isdir (self._FS_ShortNiftiFilePath) :
                    self.ui.showNiftiImagingBtn.setEnabled (True)
                elif FileNode.describesNiftiDataFile () :
                    self._FS_ShortNiftiFilePath, _ = os.path.split (self._FS_ShortNiftiFilePath)
                self.ui.showNiftiImagingBtn.setEnabled (os.path.isdir (self._FS_ShortNiftiFilePath))
            except:
                self._FS_ShortNiftiFilePath = "Unknown"
                self.ui.showNiftiImagingBtn.setEnabled (False)            
                self.ui.ImagingLongNameLbl.setText (self._NiftiLongFileName )
                self.ui.ImagingShortNameLbl.setText (self._FS_ShortNiftiFilePath)
            
                        
            try : 
                self._ROIFilePath = ProjectDataset.getROIDatasetInterface().getROIDataFilePathForNiftiDataset (FileNode)
                
                
                if not FileNode.describesNiftiDataFile () :
                    self._ROIFilePath = self._ROIFilePath[:-len ("_ROI.txt")]
                    self.ui.ROIPathLbl_2.setText (self._ROIFilePath)
                    self.ui.ROISizeLbl.setText ("NA")
                    self.ui.ROILastChangedLbl.setText ("NA")
                    try:
                        multiUserManager = ProjectDataset.getProjectFileInterface().getMultiUserFileSharingManager ()
                        if multiUserManager is None :
                            self.ui.ROIOwnerLbl.setText ("multiuser file sharing not enabled")
                        else:
                            self.ui.ROIOwnerLbl.setText ("NA")            
                    except:
                        self.ui.ROIOwnerLbl.setText ("NA")            
                    self.ui.showROIBtn.setEnabled (os.path.isdir (self._ROIFilePath))
                elif not os.path.isfile (self._ROIFilePath) :
                    self.ui.ROIPathLbl_2.setText ("File not found")
                    self.ui.ROISizeLbl.setText ("NA")
                    self.ui.ROILastChangedLbl.setText ("NA")
                    self.ui.ROIOwnerLbl.setText ("NA")
                    self.ui.showROIBtn.setEnabled (False)
                else:
                    try :
                        fHandle = RCC_ROIDataset_FileSystemObject (self._ROIFilePath, NIfTIPath = FileNode.getPath (), ProjectDataset = ProjectDataset, LockFilePath = "HardReadOnly", LockSignature = None, ProjectFileInterface = ProjectDataset.getProjectFileInterface(), OpenFileAsReadOnly = "False", EnableDestructor = True, SuperUserFroceSilentGrabLock = False, SilentIgnoreOnLockFail = True, CreatingCopyForChildProcess = False, Verbose = False, TreePath = FileNode.getTreePath ())
                        pLockFile = PersistentFSLock.getPersistentLockPathForROIFilePath (self._ROIFilePath )
                        pLockData = PersistentFSLock.getCurrentPLockFromPath(pLockFile)
                        FileHandlePLockCount = PersistentFSLock.getPlockCount (pLockFile)
                        FileHandleVersionHistory = fHandle.getFileVersionHistory ()
                        self.ui.ownerlbl.setText (str (FileHandlePLockCount))
                        self.ui.versionlbl.setText (str (len(FileHandleVersionHistory)))
                        self.ui.versionHistoryBtn.setEnabled (len (FileHandleVersionHistory) > 0 and fHandle.canVersionFile () )
                        self.ui.OwnerHistoryBtn.setEnabled (FileHandlePLockCount > 0)
                        self._fHandle = fHandle
                    except:
                        pass
                        
                    if FileHandlePLockCount <= 0 :
                        self.ui.ROIOwnerLbl.setText ("Unlocked")
                    else:
                        val = PersistentFSLock.getLockStatusForCurrentUser (pLockData, ProjectDataset.getProjectFileInterface())
                        if val == "Unlocked" :
                            self.ui.ROIOwnerLbl.setText ("Unlocked")
                        elif val == "UserLocked" :
                            self.ui.ROIOwnerLbl.setText (ProjectDataset.getProjectFileInterface().getUserName ())
                        else:
                            try :
                                multiUserManager = ProjectDataset.getProjectFileInterface().getMultiUserFileSharingManager ()
                                if multiUserManager is None :
                                    self.ui.ROIOwnerLbl.setText ("multiuser file sharing not enabled")
                                else:
                                    pLockFile = PersistentFSLock.getPersistentLockPathForROIFilePath (self._ROIFilePath )
                                    if not os.path.isfile (pLockFile) :
                                        self.ui.ROIOwnerLbl.setText ("NA")
                                    else:
                                        PLockData = PersistentFSLock.getCurrentPLockFromPath (pLockFile)
                                        owner = PersistentFSLock.getCurrentLockOwnerString (PLockData, multiUserManager) 
                                        self.ui.ROIOwnerLbl.setText (owner)
                            except:
                                self.ui.ROIOwnerLbl.setText ("Unknown")
                        
                    self.ui.ROIPathLbl_2.setText (self._ROIFilePath)
                    try :
                        fileSize = int (os.path.getsize (self._ROIFilePath))                         
                        self.ui.ROISizeLbl.setText (str('{:n}'.format(fileSize)) + " bytes")
                    except :
                        self.ui.ROISizeLbl.setText ("Unknown")
                    try :
                        changeTime = os.path.getmtime(self._ROIFilePath)
                        modificationTime = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(changeTime))
                        self.ui.ROILastChangedLbl.setText ("Last changed: " + modificationTime)
                    except:
                        self.ui.ROILastChangedLbl.setText ("Unknown")
                    
                    try :
                        changeTime = os.path.getmtime(self._ROIFilePath)
                        modificationTime = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(changeTime))
                        self.ui.ROILastChangedLbl.setText (modificationTime)
                    except:
                        self.ui.ROILastChangedLbl.setText ("Unknown")
                                        
                    try :
                        self._ROIFilePath,_ = os.path.split (self._ROIFilePath)
                        self.ui.showROIBtn.setEnabled (os.path.isdir (self._ROIFilePath))
                    except:
                        self.ui.showROIBtn.setEnabled (False)
            except:
                self.ui.ROIPathLbl_2.setText ("Unknown")
                self.ui.ROISizeLbl.setText ("Unknown")
                self.ui.ROILastChangedLbl.setText ("Unknown")
                self.ui.ROIOwnerLbl.setText ("Unknown")

        self.ui.CloseBtn.clicked.connect (self.close)
        self.ui.CloseBtn.setDefault (True)
        self.ui.CloseBtn.setAutoDefault (True)
        self.ui.showProjectFileBtn.clicked.connect(self._showProjectFile)
        self.ui.showProjectNIfTIBtn.clicked.connect(self._showProjectImaging)
        self.ui.ShowProjectROIBtn.clicked.connect(self._showProjectROI)
        self.ui.showNiftiImagingBtn.clicked.connect(self._showNifti)
        self.ui.showROIBtn.clicked.connect(self._showROI)
        
        self.ui.OwnerHistoryBtn.clicked.connect (self._showOwnerHistory)
        self.ui.versionHistoryBtn.clicked.connect (self._showVersionHistory)
        
        self._resizeWidgetHelper.setWidth  (self, 600)      
        self.exec_()
    
    def _showOwnerHistory (self) :
        if self._fHandle is not None :
            FilePLockUI.showFilePLockHistoryDlg (self, self._fHandle)
    
    def _showVersionHistory (self, ) :
        if self._fHandle is not None and self._fHandle.canVersionFile () :
            FileVersionUI.showRollBackFileDlg (self, self._fHandle, RollBack=False)
         
            
    @staticmethod 
    def _opendir (path) :
        try:
            if os.path.isdir (path) :                
                if sys.platform == 'darwin':
                        subprocess.check_call(['open', path])
                elif sys.platform == 'linux':             
                    try:
                        subprocess.check_call(['gio', 'open', path])
                    except:
                        subprocess.check_call(['xdg-open', path])
                elif sys.platform == 'win32':
                        subprocess.check_call(['explorer', path])
        except :
            pass
        
    def _showProjectFile (self) :
        RCC_FileProperties._opendir (self._projectdir)
               
    def _showProjectImaging (self) :
        RCC_FileProperties._opendir (self._rootNiftiSearchPath)
        
    def _showProjectROI (self) :
        RCC_FileProperties._opendir (self._rootROISearchPath)
        
    def _showNifti (self) :
        RCC_FileProperties._opendir (self._FS_ShortNiftiFilePath)
        
    def _showROI (self) :
        RCC_FileProperties._opendir (self._ROIFilePath)
   
    def _internalWindowResize (self, newsize):                            
        try:
            if (self._resizeWidgetHelper is not None) :                                               
                self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.ProjectPathLbl, newsize)            
                self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.ImagingPathlbl, newsize)            
                self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.ROIPathLbl, newsize)            
                self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.ImagingLongNameLbl, newsize)            
                self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.ImagingShortNameLbl, newsize)            
                self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.ImagingSizeLbl, newsize)            
                self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.ROIPathLbl_2, newsize)            
                self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.ROIOwnerLbl, newsize)            
                self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.line, newsize)            
                self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.line_2, newsize)            
                self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.line_3, newsize)
                self._resizeWidgetHelper.setProportionalXPos  (self.ui.CloseBtn, newsize)            
                
                width = self.width () 
                
                self._resizeWidgetHelper.setWidgetXPos  (self.ui.showProjectFileBtn, width -389)            
                self._resizeWidgetHelper.setWidgetXPos  (self.ui.showProjectNIfTIBtn, width-258)            
                self._resizeWidgetHelper.setWidgetXPos  (self.ui.ShowProjectROIBtn, width-128)            
                
                self._resizeWidgetHelper.setWidgetXPos  (self.ui.versionHistoryBtn, width -389)            
                self._resizeWidgetHelper.setWidgetXPos  (self.ui.OwnerHistoryBtn, width-258)            
                self._resizeWidgetHelper.setWidgetXPos  (self.ui.showROIBtn, width-128)     
                self._resizeWidgetHelper.setWidgetXPos  (self.ui.showNiftiImagingBtn, width-119)                            
                  
                newWidth = int ((float (width) / 398.0) * 131.0)
                self._resizeWidgetHelper.setWidth  (self.ui.ROISizeLbl, newWidth)            
                lastChangeLabelPos = 50 + newWidth + 9
                self._resizeWidgetHelper.setWidgetXPos  (self.ui.ROILastChangedLbl_2, lastChangeLabelPos)
                self._resizeWidgetHelper.setWidgetXPos  (self.ui.ROILastChangedLbl, lastChangeLabelPos + 101)
                self._resizeWidgetHelper.setWidth  (self.ui.ROILastChangedLbl, width - (lastChangeLabelPos + 101) -4) 
                
                self._resizeWidgetHelper.setWidth  (self.ui.versionlbl, lastChangeLabelPos - 110 + 9)            
                self._resizeWidgetHelper.setWidgetXPos  (self.ui.OwnerCount, lastChangeLabelPos)
                self._resizeWidgetHelper.setWidgetXPos  (self.ui.ownerlbl, lastChangeLabelPos + 101)
                self._resizeWidgetHelper.setWidth  (self.ui.ownerlbl, width - (lastChangeLabelPos + 101) -4)            
                
                newWidth = int ((float (width) / 398.0) * 101.0)
                self._resizeWidgetHelper.setWidth  (self.ui.DatasetCount, newWidth)       
                self._resizeWidgetHelper.setWidth  (self.ui.Dir_DatasetCount, newWidth)       
                lastChangeLabelPos = 80 + newWidth + 9
                self._resizeWidgetHelper.setWidgetXPos  (self.ui.AnnotatedDatasetLbl, lastChangeLabelPos)
                self._resizeWidgetHelper.setWidgetXPos  (self.ui.DirAnnotatedDatasetLbl, lastChangeLabelPos)
                self._resizeWidgetHelper.setWidgetXPos  (self.ui.AnnotatedDatasetCount, lastChangeLabelPos + 81)
                self._resizeWidgetHelper.setWidgetXPos  (self.ui.DirAnnotatedDatasetCount, lastChangeLabelPos + 81)
                self._resizeWidgetHelper.setWidth  (self.ui.AnnotatedDatasetCount, width - (lastChangeLabelPos + 81) -4) 
                self._resizeWidgetHelper.setWidth  (self.ui.DirAnnotatedDatasetCount, width - (lastChangeLabelPos + 81) -4) 

                
        except:
            pass
            
        
        
    def resizeEvent (self, qresizeEvent) :                       
        QDialog.resizeEvent (self, qresizeEvent)                 
        self._internalWindowResize ( qresizeEvent.size () ) 



class RCC_AboutBoxDlg (QDialog) :
    def __init__ (self, ProjectDataset, parent) :        
        QDialog.__init__ (self, None)
        self._wasCanceled = False
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)         
        self.ui = Ui_RCC_About ()        
        self.ui.setupUi (self)          
        self.setWindowFlags(QtCore.Qt.Dialog | QtCore.Qt.WindowStaysOnTopHint)          
        self.setWindowModality(QtCore.Qt.ApplicationModal)        
        if (ProjectDataset is not None) :
            basepath = ProjectDataset.getPythonProjectBasePath ()
            iconPath = os.path.join (basepath , "icons")
            graphic = QtGui.QPixmap(os.path.join (iconPath,"radiology-informatics-main.png"))      
            self.ui.splashGraphic.setPixmap(graphic)
        self.ui.closeBtn.clicked.connect (self.closeWindow)
        self.ui.closeBtn.setDefault (False)
        self.ui.closeBtn.setAutoDefault (False)
        
        try :
            RilcontourVersion = pkg_resources.get_distribution("rilcontour").version
        except :
            RilcontourVersion = "Could not detect Ril-Contour version."
        try:
            PythonVersion = sys.version
        except:
            PythonVersion = "Could not detect Python version."
        try :
            basePath = ProjectDataset.getPythonProjectBasePath ()
        except:
            basePath = "Could not determin Python path."
        try:
            projectDatasetPath = ProjectDataset.getPath ()
        except:
            projectDatasetPath = "Could not determine project path"
        
        txt = ["Ril-Contour: " + RilcontourVersion, "Python: " + PythonVersion,  "Running: " + basePath,  "Project: " + projectDatasetPath]        
        self.ui.versionlbl.setText ("\n".join (txt) )        
        self.exec_()
   
        
    def wasCanceled (self) :
        return self._wasCanceled 
    
    def closeWindow (self):
        self._wasCanceled = True
        self.close ()




class RCC_HelpBoxDlg (QDialog) :
    def __init__ (self, ProjectDataset, parentWindow) :
        QDialog.__init__ (self, parentWindow)  # needs parent, None will pass
        self._parentWindow = parentWindow
        self._wasCanceled = False
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)
        self.ui = Ui_RCC_Help ()        
        self.ui.setupUi (self)          
        #self.setWindowFlags(QtCore.Qt.Dialog | QtCore.Qt.WindowStaysOnTopHint)          
        self.setWindowModality(QtCore.Qt.NonModal)
        
        '''
        self.ui.closeBtn.clicked.connect (self.closeWindow)
        self.ui.closeBtn.setDefault (False)
        self.ui.closeBtn.setAutoDefault (False)
        '''
        self.show()
   
        
    def wasCanceled (self) :
        return self._wasCanceled 
    
    def closeWindow (self):
        self._wasCanceled = True
        self.close ()


        
 
        
class RCC_SetPesscaraTemoraryDataStorageDlg (QDialog) :
    
    def __init__ (self, path, parent) :        
        if path is None :
            path = tempfile.gettempdir()
        self._path = path        
        QDialog.__init__ (self, None)
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)         
        self.ui = Ui_RCC_PesscaraTemporaryDataStorageDlg ()        
        self.ui.setupUi (self)       
        self.ui.DirectoryChosen.setText (self._path)
        self.ui.Ok_Btn.clicked.connect (self._okBtn)
        self.ui.Cancel_Btn.clicked.connect (self._cancelBtn)
        self.ui.SelectDir_Btn.clicked.connect (self._selectDir)
        self._resizeWidgetHelper = ResizeWidgetHelper ()
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.DirectoryChosen)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.SelectDir_Btn)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.Ok_Btn)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.Cancel_Btn)

        
    def getPath (self):
        return self._path
        
    def _okBtn (self) :
        self._path = self.ui.DirectoryChosen.text ()
        self.accept ()
        
    def _cancelBtn (self) :        
        self.reject ()    

    def _selectDir (self) :
        dlg= QFileDialog( self )
        dlg.setWindowTitle( 'Select directory to save temporary data to' )
        dlg.setViewMode( QFileDialog.Detail )    
        dlg.setFileMode (QFileDialog.Directory)
        dlg.setNameFilters( [self.tr('All Files (*)')] )
        dlg.setDefaultSuffix( '' )
        dlg.setAcceptMode (QFileDialog.AcceptOpen)
        if dlg.exec_() and (len (dlg.selectedFiles())== 1):
            path = dlg.selectedFiles()[0]                        
            if os.path.isdir (path) :
                self.ui.DirectoryChosen.setText (path)     
        
    def _internalWindowResize (self, newsize):                            
        try:
            if (self._resizeWidgetHelper is not None) :                                               
                self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.DirectoryChosen, newsize)            
                self._resizeWidgetHelper.setWidgetXPos  (self.ui.SelectDir_Btn, newsize.width () - 150)                                   
                self._resizeWidgetHelper.setWidgetXPos  (self.ui.Ok_Btn, newsize.width () - 190)            
                self._resizeWidgetHelper.setWidgetYPos  (self.ui.Ok_Btn, newsize.height () - 39)                   
                self._resizeWidgetHelper.setWidgetXPos  (self.ui.Cancel_Btn, newsize.width () - 100)                        
                self._resizeWidgetHelper.setWidgetYPos  (self.ui.Cancel_Btn, newsize.height () - 39)    
        except:
            pass
            
    def resizeEvent (self, qresizeEvent) :                       
        QDialog.resizeEvent (self, qresizeEvent)                 
        self._internalWindowResize ( qresizeEvent.size () ) 



class RCC_RegistrationIndicatorFilterDlg (QDialog) :
    
    def __init__ (self, parent, firstDate, currentDate) :        
        self._monthDayYearFilter = (None, None, None)
        QDialog.__init__ (self, None)
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, False)         
        self.ui = Ui_rcc_RegistrationIndicatorFilter ()        
        self.ui.setupUi (self)            
        self.ui.CloseBtn.clicked.connect (self.closeDlg)
        self._firstDate = firstDate
        if (firstDate[0] == None) :
            self.ui.FilterDatesRadioBtn.setEnabled (False)
            self.ui.calendarWidget.setEnabled(False)
            self.ui.ShowAllDatesRadioBtn.setChecked (True)
        else:            
            if (currentDate[0] == None) :
                self.ui.ShowAllDatesRadioBtn.setChecked (True)
                self.ui.calendarWidget.setEnabled(False)
                month, day, year = firstDate
                                
                date = QDate (year, month, day)
                self.ui.calendarWidget.setSelectedDate(date)
               
            else:
                self.ui.calendarWidget.setEnabled(True)
                self.ui.FilterDatesRadioBtn.setChecked (True)            
                month, day, year = currentDate
                date = QDate (year, month, day)
                self.ui.calendarWidget.setSelectedDate(date)                
                
        self.ui.ShowAllDatesRadioBtn.pressed.connect (self._buttonPressed)
        self.ui.FilterDatesRadioBtn.pressed.connect (self._buttonPressed)        
        
    def _buttonPressed (self) :
        if (self.ui.ShowAllDatesRadioBtn.isChecked()):
            self.ui.calendarWidget.setEnabled(True)
        else:
            self.ui.calendarWidget.setEnabled(False)
            month, day, year = self._firstDate
            date = QDate (year, month, day)
            self.ui.calendarWidget.setSelectedDate(date)
            
    def getRegistrationDayFilter (self) :
        if (self.ui.ShowAllDatesRadioBtn.isChecked()):
            return (None, None, None)
        else:
            return (self.ui.calendarWidget.selectedDate().month (), self.ui.calendarWidget.selectedDate().day (), self.ui.calendarWidget.selectedDate().year ())
    
    def closeDlg (self) :        
        self.close ()

class RCC_UserNameEditorDlg (QDialog) :
    
    def __init__ (self, parent, username) :        
        QDialog.__init__ (self, None)
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)         
        self.ui = Ui_RCC_UserNameEditor ()        
        self.ui.setupUi (self)    
        self.ui.lineEdit.setText (username)
        self.ui.lineEdit.setEnabled (parent._ProjectDataset.canChangeUserName ())
        self.ui.OkBtn.clicked.connect (self.closeDlg)
        self._UserName = username
        self.setWindowFlags(self.windowFlags () | QtCore.Qt.Dialog  | QtCore.Qt.WindowStaysOnTopHint)
    
    def closeDlg (self) :
        self._UserName = self.ui.lineEdit.text ().strip ()        
        self.close ()
    
    def getUserName (self) :
        return self._UserName

class RCC_NewProjectWizardDlg (QDialog) :
    def __init__ (self, parent) :
        self._result = "Cancel"
        QDialog.__init__ (self, None)
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)         
        self.ui = Ui_RCC_NewProjectWizard ()        
        self.ui.setupUi (self)    
        self.ui.wizardStack.setCurrentIndex (0)
        self.ui.openpesscaradataset.clicked.connect   (self._openPesscaraDataset)
        self.ui.openfilesystemdataset.clicked.connect (self._openFileSystemDataset)        
        self.ui.ROIPesscara.clicked.connect   (self._roiPesscara)
        self.ui.ROIFileSystemDest.clicked.connect (self._roiFileSystem)
        self.ui.ImportDatasetFromProjectBtn.clicked.connect (self._importdatasetfromProject)
        self.ui.BackBtn.clicked.connect (self._backBtn)
        self.ui.BackBtn.setEnabled (False)
        self.ui.setNameBtn.clicked.connect (self._setProjectNameBtn)
        self.ui._createProjectBtn.clicked.connect (self._createProject)
        self.ui.cancelBtn.clicked.connect (self._cancel)        
        self.ui.openpesscaradataset.setEnabled (PesscaraLoaded.arePesscaraLibrarysLoaded ())        
        # Card 0
        self._pesscaraNiftiInterface = None
        self._pesscaraNiftiSubjectList = None        
        self._fileSystemNIfTISouceDir = ""
        
        # Card 1
        self._filesystemROISave = "" #if "Pescara" save back to pescara server
        
        # Card 2       
        self._projectName = ""
        
        # Card 2.5       
        self._projectPath = "None"
        self.ui.openfilesystemdataset.setDefault (True)
                
    def _backBtn (self) :
        index = self.ui.wizardStack.currentIndex ()
        if (index > 0) :
            self.ui._createProjectBtn.setEnabled (False)        
            self.ui.projectName.setEnabled (True)
            if (index == 1) :
                self.ui.BackBtn.setEnabled (False)
                self.ui.openfilesystemdataset.setDefault (True)
            else:
                if (self._pesscaraNiftiInterface is None) :
                    self.ui.ROIPesscara.setDefault (False)
                    self.ui.ROIFileSystemDest.setDefault (True)
                else:
                    self.ui.ROIFileSystemDest.setDefault (False)
                    self.ui.ROIPesscara.setDefault (True)
            self.ui.wizardStack.setCurrentIndex (index - 1)   
    
    def _openPesscaraDataset (self) :
        self.ui.openpesscaradataset.setEnabled (False)
        self.ui.openfilesystemdataset.setEnabled (False)
        origionalLblTxt = self.ui.label.text ()
        self.ui.label.setText ("Connecting to Pesscara")                
        succeed = False
        self._fileSystemNIfTISouceDir = ""
        self._pesscaraNiftiInterface = RCC_PesscaraInterface ()
        if (self._pesscaraNiftiInterface.login (self, "", "", "", "")):            
            self._pesscaraNiftiSubjectList  = self._pesscaraNiftiInterface.selectSubjectDataset (self)            
            if (len (self._pesscaraNiftiSubjectList ) > 0) :     
                self.ui.ROIPesscara.setEnabled (True)
                self.ui.ROIFileSystemDest.setEnabled (False)
                self.ui.ROIFileSystemDest.setDefault (False)
                self.ui.ROIPesscara.setDefault (True)
                self.ui.wizardStack.setCurrentIndex (1)                                   
                succeed = True
                self.ui.BackBtn.setEnabled (True)       
        self.ui.openpesscaradataset.setEnabled (True)
        self.ui.openfilesystemdataset.setEnabled (True)
        self.ui.label.setText (origionalLblTxt)
        if (not succeed) : 
            self._pesscaraNiftiInterface = None
            self._pesscaraNiftiSubjectList = None
            #   self._project = RCC_DatasetDlg (RCC_PesscaraDataInterface (interface, subjectList), self)                
            #   self._project.addFileSelectionListener (self.FileChanged)
            #   self._project.show ()           

                
    def _openFileSystemDataset (self) :
        self._pesscaraNiftiInterface = None
        self._pesscaraNiftiSubjectList = None
        self._fileSystemNIfTISouceDir = ""
        dlg= QFileDialog( self )
        dlg.setWindowTitle( 'Select directory containing NIfTI files' )
        dlg.setViewMode( QFileDialog.Detail )    
        dlg.setFileMode (QFileDialog.Directory)
        dlg.setNameFilters( [self.tr('All Files (*)')] )
        dlg.setDefaultSuffix( '' )
        dlg.setAcceptMode (QFileDialog.AcceptOpen)
        if dlg.exec_()  and (len (dlg.selectedFiles())== 1):
            self._fileSystemNIfTISouceDir = dlg.selectedFiles()[0]            
            self.ui.ROIPesscara.setEnabled (False)
            self.ui.ROIFileSystemDest.setEnabled (True)
            self.ui.ROIPesscara.setDefault (False)
            self.ui.ROIFileSystemDest.setDefault (True)
            self.ui.wizardStack.setCurrentIndex (1)
            self.ui.BackBtn.setEnabled (True)
        else:
            self._fileSystemNIfTISouceDir = ""
                   
    def _roiPesscara (self) :
        self._filesystemROISave = "PESSCARA"
        self.ui.wizardStack.setCurrentIndex (2)
        self.ui.setNameBtn.setDefault (True)             
        
    def _roiFileSystem (self) :
        self._filesystemROISave = ""
        dlg= QFileDialog( self )
        dlg.setWindowTitle( 'Select directory to save ROI data files to:' )
        dlg.setViewMode( QFileDialog.Detail )    
        dlg.setFileMode (QFileDialog.Directory)
        dlg.setNameFilters( [self.tr('All Files (*)')] )
        dlg.setDefaultSuffix( '' )
        dlg.setAcceptMode (QFileDialog.AcceptOpen)
        if dlg.exec_() and (len (dlg.selectedFiles())== 1):
            self._filesystemROISave = dlg.selectedFiles()[0]                        
            self.ui.wizardStack.setCurrentIndex (2)
            self.ui.setNameBtn.setDefault (True)
            ROIPathList = FileUtil.getPathList (self._filesystemROISave)
            NiftiPathList = FileUtil.getPathList (self._fileSystemNIfTISouceDir)
            if len (ROIPathList) >= len (NiftiPathList) :
                ROIPathisInNiftiPathList = True
                for index in range (len (NiftiPathList)) :
                    if ROIPathList[index] != NiftiPathList[index] :
                        ROIPathisInNiftiPathList = False
                        break
                if ROIPathisInNiftiPathList : 
                    MessageBoxUtil.showMessage ("ROI Path Nested In NIfTI Dataset", "It is highly recommend that ROI datasets are not stored in the location as the NIfTI dataset.")
                if not FileUtil.testCanReadWriteFromDirectory (self._filesystemROISave) :
                    MessageBoxUtil.showMessage ("File Permission Error", "Unable to create directories and write to the specified ROI data directory. You likely do not have file privlages to write to this location. RIL-Contour requires users level write privlages to the ROI data directory.")
        else:
            self._filesystemROISave = ""
            
    def _importdatasetfromProject (self) :
        self._importDatasetProjectPath = ""
        dlg= QFileDialog( self )
        dlg.setWindowTitle( 'Select project to import ROI dataset from:' )
        dlg.setViewMode( QFileDialog.Detail )    
        dlg.setFileMode (QFileDialog.ExistingFile)
        dlg.setNameFilters( [self.tr('Project (*.prj)'), self.tr('All Files (*)')] )
        dlg.setDefaultSuffix( '.prj' )
        dlg.setAcceptMode (QFileDialog.AcceptOpen)
        if dlg.exec_() and (len (dlg.selectedFiles())==1) :
            self._importDatasetProjectPath = dlg.selectedFiles()[0]                        
            self.ui.wizardStack.setCurrentIndex (3)
        else:
            self._importDatasetProjectPath = ""
    

        
    def _setProjectNameBtn (self) :        
        succeed = False
        projectname = self.ui.projectName.text ().strip ()
        if (projectname != "") :            
            if (self._filesystemROISave != "PESSCARA"):        
                succeed = True                
            else:
                if (not self._pesscaraNiftiInterface.doSubjectsHaveProjectDataset (projectname, self._pesscaraNiftiSubjectList)):
                    succeed = True                
                else:
                    succeed = MessageBoxUtil.showQuestionMessageBox (WindowTitle="Over write ROI dataset?",WindowMessage="An existing dataset was detected with the name '%s' in Passcara. Do you wish to open this the dataset?" % (projectname))
                    
        if (succeed) :
            self._projectName = projectname
            self.ui.projectName.setEnabled (False)
            self.ui._createProjectBtn.setEnabled (True)  
            self._createProject ()
            
    def _cancel (self) :
        self._result = "Cancel"
        self.close ()
        
    def _createProject (self) :
        self.ui._createProjectBtn.setDefault (True)
        dlg= QFileDialog( self )
        dlg.setWindowTitle('Save project as:' )
        dlg.setViewMode( QFileDialog.Detail )    
        dlg.setFileMode (QFileDialog.AnyFile)
        dlg.setNameFilters( [self.tr('Project (*.prj)'), self.tr('All Files (*)')] )
        dlg.setDefaultSuffix( '.prj' )
        dlg.setAcceptMode (QFileDialog.AcceptSave)
        if dlg.exec_()  and (len (dlg.selectedFiles())== 1):
            self._projectPath = dlg.selectedFiles()[0]                         
            self._result = "CreateProject"        
            self.close ()
        
                
        
        
        
"""
    RCC_ProjectROIEditorDlg
    
    Modal dialog to enable user to define and modify ROI objects
"""    
class RCC_ProjectROIEditorDlg (QDialog) :
    
    def __init__ (self, parent, roidefs, ProjectDataset) :
        self._parent = parent
        self._ProjectDataset = ProjectDataset
        QDialog.__init__ (self, None)
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)
        self.ReturnValue = roidefs        
        self.ui = Ui_RCC_ProjectROIEditor ()        
        self.ui.setupUi (self)                
        self._resizeWidgetHelper = ResizeWidgetHelper ()
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.ROIList)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.addROIBtn)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.RemoveROIBtn)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.line)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.OkBtn)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.CancelBtn)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.moveDownBtn)        
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.moveUpBtn)        
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.OneROIPerVoxel)        
        
        
        self.ui.ROIList.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.ui.ROIList.customContextMenuRequested.connect(self.showContextMenu)
        
        basepath = ProjectDataset.getPythonProjectBasePath ()
        iconPath = os.path.join (basepath , "icons")
        self.ui.addROIBtn.setIcon(QtGui.QIcon(iconPath + os.sep + "plus.png"))
        self.ui.addROIBtn.setIconSize(QtCore.QSize(25,25))        
        self.ui.RemoveROIBtn.setIcon(QtGui.QIcon(iconPath + os.sep + "minus.png"))
        self.ui.RemoveROIBtn.setIconSize(QtCore.QSize(25,25))
        self.ui.RemoveROIBtn.setEnabled (False)
        
        self.ui.moveUpBtn.setIcon(QtGui.QIcon(iconPath + os.sep + "up.png"))
        self.ui.moveUpBtn.setIconSize(QtCore.QSize(25,25))        
        self.ui.moveUpBtn.setEnabled (False)
        self.ui.moveDownBtn.setIcon(QtGui.QIcon(iconPath + os.sep + "down.png"))
        self.ui.moveDownBtn.setIconSize(QtCore.QSize(25,25))
        self.ui.moveDownBtn.setEnabled (False)
        self.ui.moveUpBtn.clicked.connect (self._moveSelectedROIUp)
        self.ui.moveDownBtn.clicked.connect (self._moveSelectedROIDown)
        
        self.ui.CancelBtn.clicked.connect (self.CancelBtn)
        self.ui.OkBtn.clicked.connect (self.OkBtn)        
        
        self.ui.addROIBtn.clicked.connect (self.addROIBtn)
        self.ui.RemoveROIBtn.clicked.connect (self.RemoveROIBtn)                
        
        self.ui.ROIList.selectionModel().selectionChanged.connect(self.ROIListSelectionChanged)        
        self.ui.ROIList.itemDoubleClicked.connect(self.ROIListDblClk)
        
        self._activeROIDefs = roidefs.copy (CopyFilename = True)
        self._activeROIDefs._changeListener = self.ReturnValue._changeListener
        RCC_ProjectROIEditorDlg._setListUI (self.ui.ROIList, self._activeROIDefs)  
        
        self.ui.OneROIPerVoxel.setChecked (self._activeROIDefs.hasSingleROIPerVoxel ())
        self.setMinimumSize(self.size ())            
    
    
    def _moveSelectedROIUp (self) :
        roidefs = self._getROIDefinitions ()
        items = self.ui.ROIList.selectedItems ()
        if (len (items) > 0):
            selection = items[0]        
            ROIName = selection.data(QtCore.Qt.UserRole)        
            roidefs.moveROIUp (ROIName)
            RCC_ProjectROIEditorDlg._setListUI (self.ui.ROIList, roidefs, selectedItemName = ROIName)                         
    
    def _moveSelectedROIDown (self) :
        roidefs = self._getROIDefinitions ()
        items = self.ui.ROIList.selectedItems ()
        if (len (items) > 0):
            selection = items[0]        
            ROIName = selection.data(QtCore.Qt.UserRole)        
            roidefs.moveROIDown (ROIName)
            RCC_ProjectROIEditorDlg._setListUI (self.ui.ROIList, roidefs, selectedItemName = ROIName)             
            
    def showContextMenu(self, pos) :        
        globalPos = self.ui.ROIList.mapToGlobal(pos)        
        myMenu = QMenu(self)
        addAction = QAction ("Add",self)
        addAction.setEnabled (self.ui.ROIList.isEnabled())
        addAction.triggered.connect (self.addROIBtn)
        myMenu.addAction (addAction)
        
        items = self.ui.ROIList.selectedItems ()
        oneROISelected = (len (items) == 1)
        editAction = QAction ("Edit",self)
        editAction.setEnabled (oneROISelected)
        editAction.triggered.connect (self.ROIListDblClk)
        myMenu.addAction (editAction)
        
        removeAction = QAction ("Remove",self)
        removeAction.setEnabled (oneROISelected)
        removeAction.triggered.connect (self.RemoveROIBtn)        
        myMenu.addAction (removeAction)
        
        myMenu.addSeparator()
        roidefs = self._getROIDefinitions ()
        moveUp = QAction ("Move Up",self)
        
        selectedROIName = ""
        if (oneROISelected) :
            selection = items[0]        
            selectedROIName = selection.data(QtCore.Qt.UserRole)        
        
        firstname= ""
        lastname = ""
        namelist = roidefs.getNameLst ()
        if (len (namelist) > 0) :
            firstname = namelist[0]
            lastname = namelist[len (namelist)-1]
        
        moveUp.setEnabled (oneROISelected and selectedROIName != firstname)
        moveUp.triggered.connect (self._moveSelectedROIUp)
        myMenu.addAction (moveUp)
        moveDown = QAction ("Move Down",self)
        moveDown.setEnabled (oneROISelected and selectedROIName != lastname)
        moveDown.triggered.connect (self._moveSelectedROIDown)
        myMenu.addAction (moveDown)        
        myMenu.exec_(globalPos)
  
    def _internalWindowResize (self, newsize):                            
        try :
            if (self._resizeWidgetHelper is not None) :                                               
                self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.ROIList, newsize)            
                self._resizeWidgetHelper.setAbsoluteHeight  (self.ui.ROIList, newsize)                                                
                self._resizeWidgetHelper.setWidgetYPos  (self.ui.line, newsize.height () - 59)                        
                self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.line, newsize)                                                
                self._resizeWidgetHelper.setWidgetYPos  (self.ui.OkBtn, newsize.height () - 39)                        
                self._resizeWidgetHelper.setWidgetYPos  (self.ui.CancelBtn, newsize.height () - 39)                                    
                self._resizeWidgetHelper.setWidgetYPos  (self.ui.addROIBtn, newsize.height () - 90)                        
                self._resizeWidgetHelper.setWidgetYPos  (self.ui.RemoveROIBtn, newsize.height () - 90)                                
                self._resizeWidgetHelper.setWidgetYPos  (self.ui.moveUpBtn, newsize.height () - 90)                                
                self._resizeWidgetHelper.setWidgetYPos  (self.ui.moveDownBtn, newsize.height () - 90)                                
                self._resizeWidgetHelper.setWidgetXPos  (self.ui.OkBtn, newsize.width () - 201)                        
                self._resizeWidgetHelper.setWidgetXPos  (self.ui.CancelBtn, newsize.width () - 101)                        
                self._resizeWidgetHelper.setWidgetXPos  (self.ui.addROIBtn, newsize.width () - 81)                        
                self._resizeWidgetHelper.setWidgetXPos  (self.ui.RemoveROIBtn, newsize.width () - 41)                                    
                self._resizeWidgetHelper.setWidgetYPos  (self.ui.OneROIPerVoxel, newsize.height () - 84)                                    
                self._resizeWidgetHelper.setWidgetXPos  (self.ui.moveUpBtn, newsize.width () - 161)                                
                self._resizeWidgetHelper.setWidgetXPos  (self.ui.moveDownBtn, newsize.width () - 121)                                
        except:
            pass
            
            
                        
    def resizeEvent (self, qresizeEvent) :                       
        QDialog.resizeEvent (self, qresizeEvent)                 
        self._internalWindowResize ( qresizeEvent.size () )               
    
    def _addROIBtn (self, initROIDef = None) :
        roiNameLst = []
        for ic in range(self.ui.ROIList.count ())  :
            item = self.ui.ROIList.item (ic) 
            ROIName = item.data(QtCore.Qt.UserRole)
            roiNameLst.append (ROIName)
            
        newROIDlg = RCC_ROIEditorDlg (self._parent, roiNameLst, initROIDef = initROIDef, ProjectDataset = self._ProjectDataset)
        newROIDlg.exec_ ()
        if (newROIDlg.ReturnValue is not None) :            
            templist = self._getROIDefinitions ()
            newROI = newROIDlg.ReturnValue             
            #idNum = templist.getNewROIIDNumber ()                    
            if (initROIDef is not None) :
                ROIName = initROIDef.getName ()
                index = templist.getROIIndex (ROIName)                
                PreserveROIUID = newROI.getType () == initROIDef.getType ()
                if PreserveROIUID :
                    ROIUID = initROIDef.getIDNumber ()                    
                else:
                    ROIUID = None
                newROIDef = ROIDef (roiIDNumber = ROIUID, name = newROI.getName (), color = newROI.getColor (), roitype =  newROI.getType (), mask = newROI.getMask (), Hidden = newROI.isHidden (), RadlexID = newROI.getRadlexID (), Locked = newROI.isLocked (), ROIDefUIDObj= newROI.getDefinedUID())
                templist.renameROI (index, ROIName, newROIDef, PreserveROIUID = PreserveROIUID)                
            else:                        
                ROIName = newROI.getName ()
                templist.appendROIDef (ROIDef (roiIDNumber = None, name = ROIName, color = newROI.getColor (), roitype =  newROI.getType (), mask = newROI.getMask (), Hidden = newROI.isHidden (), RadlexID = newROI.getRadlexID (), Locked = newROI.isLocked (), ROIDefUIDObj = newROI.getDefinedUID()))                                                                                                                           
            RCC_ProjectROIEditorDlg._setListUI (self.ui.ROIList, templist, selectedItemName = ROIName)             
            
    def addROIBtn (self) :
        self._addROIBtn ()
        
    def RemoveROIBtn (self) :
        removeNameLst = []
        items = self.ui.ROIList.selectedItems ()
        for item in items :
            ROIName = item.data(QtCore.Qt.UserRole)
            removeNameLst.append (ROIName)
        if (len (removeNameLst) > 0) :
            templist = self._getROIDefinitions ()        
            for name in removeNameLst :
                templist.deleteROI (name)
            RCC_ProjectROIEditorDlg._setListUI (self.ui.ROIList, templist)                    
        
    def ROIListDblClk (self) :
        items = self.ui.ROIList.selectedItems ()
        if (len (items) > 0):
            selection = items[0]        
            ROIName = selection.data(QtCore.Qt.UserRole)
            _roiDefs = self._getROIDefinitions ()
            _color = _roiDefs.getROIColor (ROIName)
            _color = (_color.red (), _color.green (),_color.blue())
            _type = _roiDefs.getROIType (ROIName)
            _mask = _roiDefs.getROIMaskValue (ROIName)
            _hideROI = _roiDefs.isROIHidden (ROIName)
            _RadlexID = _roiDefs.getROINameRadlexID (ROIName)
            _Locked = _roiDefs.isROILocked (ROIName)
            ROIDefUIDObj = _roiDefs.getROIDefUIDObjByName (ROIName)
            idNum = _roiDefs.getROINameIDNumber (ROIName)
            self._addROIBtn (initROIDef = ROIDef (roiIDNumber = idNum, name = ROIName, color = _color, roitype =  _type, mask = _mask, Hidden = _hideROI, RadlexID = _RadlexID, Locked = _Locked, ROIDefUIDObj = ROIDefUIDObj))                        
            
    def ROIListSelectionChanged (self) :
        items = self.ui.ROIList.selectedItems ()
        self.ui.RemoveROIBtn.setEnabled(len (items) > 0)             
        oneROISelected = (len (items) == 1)
        firstname= ""
        lastname = ""
        roidefs = self._getROIDefinitions ()
        namelist = roidefs.getNameLst ()
        if (len (namelist) > 0) :
                firstname = namelist[0]
                lastname = namelist[len (namelist)-1]
        
        selectedROIName = ""
        if (oneROISelected) :
            selection = items[0]        
            selectedROIName = selection.data(QtCore.Qt.UserRole)   
            
        self.ui.moveUpBtn.setEnabled (oneROISelected and selectedROIName != firstname)
        self.ui.moveDownBtn.setEnabled (oneROISelected and selectedROIName != lastname)
        
    @staticmethod
    def _setListUI (lst, roiDefs, selectedItemName = None):
        lst.clear ()        
        namelist =  roiDefs.getNameLst ()        
        selectionRow = -1
        for index, name in enumerate(namelist): 
                item = QListWidgetItem() 
                itemText = name + "   [Type: "+ str (roiDefs.getROIType (name))  +"; Mask value: " + str (roiDefs.getROIMaskValue (name)) 
                if roiDefs.getROINameRadlexID (name) != "unknown" :
                    itemText += "; RadlexID: " + str (roiDefs.getROINameRadlexID (name)) 
                itemText += "]"
                item.setText (itemText) 
                item.setData(QtCore.Qt.UserRole, name)
                color = roiDefs.getROIColor (name)                             
                item.setForeground (color)
                if name == selectedItemName :
                    selectionRow = index
                lst.addItem (item)  
        if (selectionRow != -1) :
            lst.setCurrentRow( selectionRow )
    
    def OkBtn (self):
        #self.setModal (False)                
        self.ReturnValue = self._getROIDefinitions ()     
        self.ReturnValue.setSingleROIPerVoxel (self.ui.OneROIPerVoxel.isChecked())
        self.accept ()
        #self.accept ()
        
    def CancelBtn (self):
        #self.setModal (False)            
        self.reject ()
        
    #fix incoporate old ID Numbers0
    def _getROIDefinitions (self) :
        return self._activeROIDefs
       


    
def _FileSystemMaskExportProcess (tpl) :        
    returnMaskDescriptionList = []        
    dirProcess,  ROIDefs,  dlg_SelectedROILst, dlg_ExportName, dlg_ExportPath,  dlg_pointInSliceSize, dlg_pointCrossSliceSize, dlg_ExportNiftiFile, dlg_ExportROIMaskAsBoundingBox, dlg_OverwriteExistingFiles, appendFileNameCheckState, ProjectFileLockInterface, progressDialog, ProgressBarDeltaValue = tpl
    startProgressValue, endProgressValue = ProgressBarDeltaValue
    deltaValue = float (endProgressValue - startProgressValue)
    dirLen = float (len (dirProcess))
    for paramIndex, param in enumerate(dirProcess) :                      
        maskdescription = None
        ROIDataFile, FilesystemROIDataPath,  fullNiftiFile, writepath, longFileNameWritePath = param
                
        ROIDataFilePath = ROIDataFile.getPath ()
        if not progressDialog.wasCanceled() :
            progressDialog.setLabelText ("Exporting: " + ROIDataFilePath)        
        if progressDialog.wasCanceled() :
            break  
                        
        if fullNiftiFile is None :
            fullNiftiFilePath = ""
        else:
            fullNiftiFilePath = fullNiftiFile.getFilePath ()
               
        ROIDataFile = RCC_ROIDataset_FileSystemObject (ROIDataFilePath, NIfTIPath = fullNiftiFilePath, ProjectDataset = None, ProjectFileInterface = ProjectFileLockInterface, OpenFileAsReadOnly = "HardReadOnly", Verbose= False, TreePath=None) # ProjectData set is None, don't use file save queueing inside of processes.
        ExportBinaryOrROIMask = not ROIDefs.hasSingleROIPerVoxel ()
        
        tempDataDictionary = ROIDictionary (ROIDefs, None, None, LowMemory = True)            
        tempDataDictionary.loadROIDictionaryFromFile (ROIDataFile,  ProjectFileLockInterface)                                            
        
        objLst = []                    
        for roiNames in dlg_SelectedROILst :
            if (tempDataDictionary.isROIDefined (roiNames)) :
                obj = tempDataDictionary.getROIObject (roiNames)
                objLst.append (obj)   
                
        if (len (objLst) > 0) :     
            _, Orig_ROIfilename = os.path.split (FilesystemROIDataPath)
            
            stripDialogName, _ = FileUtil.removeExtension (dlg_ExportName, [".nii", ".nii.gz"])
           
                                   
            if longFileNameWritePath is None :
                CreateWriteDirPathFromShortFilePath = True
            else:            
                writepathdir, _ = os.path.split (longFileNameWritePath)                
                if platform.system() == "Windows" :
                    if len (writepathdir) > 200 :
                        writepathdir, _ = os.path.split (writepath)                                                
                FileUtil.createPath (writepathdir, False)
                CreateWriteDirPathFromShortFilePath = not  os.path.isdir (writepathdir)                         
            if CreateWriteDirPathFromShortFilePath :
                writepathdir, _ = os.path.split (writepath)
                FileUtil.createPath (writepathdir, False)   
            
            writepathdir = FileUtil.correctOSPath (writepathdir)
            
            if (not appendFileNameCheckState) :
                Orig_filename  = stripDialogName + "_" + Orig_ROIfilename          
                ExportDestNiftiFileName = os.path.join (writepathdir, dlg_ExportName)
            else:                
                stripOrig_ROIfilename, Extention = FileUtil.removeExtension (Orig_ROIfilename, [".nii", ".nii.gz"])
                if Extention is None :
                    Extention = ".nii.gz"                
                
                if longFileNameWritePath is not None :
                    try: 
                        stripLongFName = LongFileNameAPI.getLongFileNamePath (fullNiftiFilePath)
                        _, stripLongFName = os.path.split (stripLongFName)
                        stripLongFName, _ = FileUtil.removeExtension (stripLongFName, [".nii", ".nii.gz"])                        
                        stripLongFName.strip ()
                        if stripLongFName != stripOrig_ROIfilename and len (stripLongFName) > 0:
                            test_filename  = stripLongFName + "_" + stripDialogName + Extention
                            try :
                                testFname = os.path.join (writepathdir, test_filename)
                                with open (testFname,"wt") as _ :
                                    stripOrig_ROIfilename = stripLongFName
                                os.remove (testFname)
                            except:
                                pass
                    except:
                        pass
                Orig_filename  = stripOrig_ROIfilename + "_" + stripDialogName + Extention
                ExportDestNiftiFileName = os.path.join (writepathdir, stripOrig_ROIfilename + Extention)
                
            writepath = os.path.join (writepathdir, Orig_filename)
            
        
            validMaskFileName = True
            #windows can have problems with long file names, total paths > 256 bytes or so.  Try a bunch of file names to attempt to find a mask file name that will work
            if (not os.path.isfile (writepath)) :                    
                if (not appendFileNameCheckState) :
                    testNameLst = [writepath, os.path.join (writepathdir, "Mask_"+Orig_ROIfilename), os.path.join (writepathdir, "M_"+Orig_ROIfilename), os.path.join (writepathdir, "M"+Orig_ROIfilename), os.path.join (writepathdir, "Mask.nii.gz"), os.path.join (writepathdir, "M.nii.gz")]
                else:
                    stripOrig_ROIfilename, Extention = FileUtil.removeExtension (Orig_ROIfilename, [".nii", ".nii.gz"])
                    if Extention is None :
                        Extention = ".nii.gz"
                    
                    testNameLst = [writepath, os.path.join (writepathdir, stripOrig_ROIfilename + "_Mask" + Extention), os.path.join (writepathdir, stripOrig_ROIfilename + "_M" + Extention), os.path.join (writepathdir, stripOrig_ROIfilename + "M" + Extention), os.path.join (writepathdir, "Mask" + Extention), os.path.join (writepathdir, "M" + Extention)]
                    
                validMaskFileName = False
                for testName in testNameLst :
                    if (not os.path.isfile (testName)):
                        try:
                            testpath = open (testName, "wb")
                            testpath.close ()
                            os.remove (testName)
                            writepath = testName
                            validMaskFileName = True
                            break
                        except :
                            validMaskFileName = False
            if (not validMaskFileName) :
                # valid filename could not be found reset path to origional so errormessaging will be most accurate
                writepath = testNameLst[0]
                print ("NIfTI: %s" % fullNiftiFilePath)
                print ("Mask: %s" % writepath)
                print ("Error valid mask filename could not be generated. On windows this may occure if file paths exceed opperating system limitations (~256 characters). If using windows try exporting masks to a directory with a shorter path") 
            else:    
                try :  
                    
                    longfilenamePath = LongFileNameAPI.getLongFileNamePath (fullNiftiFilePath)
                    niftiVolumeOrientationDescription = tempDataDictionary.getNIfTIVolumeOrientationDescription ()
                    if (niftiVolumeOrientationDescription == None or not niftiVolumeOrientationDescription.hasParameter ("VolumeDimension") or not niftiVolumeOrientationDescription.hasParameter ("NativeNiftiFileOrientation") or not niftiVolumeOrientationDescription.hasParameter ("FinalROINiftiOrientation") or not niftiVolumeOrientationDescription.hasParameter ("Affine")) :
                        #print ("slow Filesystem mask write")                        
                        NiftiVolumeDataToMask = NiftiVolumeData (nib.load (fullNiftiFilePath))                                                                                             
                        maskExportPath = NiftiVolumeDataToMask.writeMaskVolume (writepath, objLst, MaskValue = None, InSlicePointSize = dlg_pointInSliceSize, CrossSlicePointSize = dlg_pointCrossSliceSize, RoiDefs= ROIDefs, BinaryOrMask = ExportBinaryOrROIMask, ExportNiftiFile = dlg_ExportNiftiFile, ExportNiftiMaskfileDescription = True, roiDictionary = tempDataDictionary, ExportAreaROIUsingBoundingVolumeDefinition = dlg_ExportROIMaskAsBoundingBox, OverwriteFiles = dlg_OverwriteExistingFiles, ExportDestNiftiFileName = ExportDestNiftiFileName)
                    else:
                        #print ("Fast Filesystem write")  
                        try :                                        
                            maskExportPath = NiftiVolumeData._writeMaskVolume (tempDataDictionary.getNIfTIVolumeOrientationDescription (), writepath, objLst, MaskValue = None, InSlicePointSize = dlg_pointInSliceSize, CrossSlicePointSize = dlg_pointCrossSliceSize, RoiDefs= ROIDefs, BinaryOrMask = ExportBinaryOrROIMask, ExportOriginalNiftiFile = dlg_ExportNiftiFile ,ExportOrigionalNiftiFilePath = fullNiftiFilePath,  roiDictionary = tempDataDictionary, ExportNiftiMaskfileDescription = True, ExportAreaROIUsingBoundingVolumeDefinition = dlg_ExportROIMaskAsBoundingBox, OverwriteFiles = dlg_OverwriteExistingFiles, LongNiftiFileName = longfilenamePath, ExportDestNiftiFileName = ExportDestNiftiFileName)
                        except:
                            NiftiVolumeDataToMask = NiftiVolumeData (nib.load (fullNiftiFilePath))                                                                                             
                            maskExportPath = NiftiVolumeDataToMask.writeMaskVolume (writepath, objLst, MaskValue = None, InSlicePointSize = dlg_pointInSliceSize, CrossSlicePointSize = dlg_pointCrossSliceSize, RoiDefs= ROIDefs, BinaryOrMask = ExportBinaryOrROIMask, ExportNiftiFile = dlg_ExportNiftiFile, ExportNiftiMaskfileDescription = True, roiDictionary = tempDataDictionary, ExportAreaROIUsingBoundingVolumeDefinition = dlg_ExportROIMaskAsBoundingBox, OverwriteFiles = dlg_OverwriteExistingFiles, ExportDestNiftiFileName = ExportDestNiftiFileName)
                    if maskExportPath != None :      
                        directory, _ = os.path.split (maskExportPath)                            
                        maskdescription = os.path.join (directory, "NIfTIMaskFileDescription.csv") 
                        basepathlen = len (dlg_ExportPath)                                                          
                        maskdescription = {"Source_NIfTI":longfilenamePath, "Exported_Data":os.path.join (".", maskdescription[basepathlen+1:])}
                except :
                    print (fullNiftiFilePath)
                    print ("Error could not save volume mask to file system")                        
        tempDataDictionary.delete ()
        if maskdescription is not None :
            returnMaskDescriptionList.append (maskdescription)
        if not progressDialog.wasCanceled() :
            progressDialog.setValue (int ((deltaValue * float ((paramIndex) / dirLen)) + startProgressValue))
    return returnMaskDescriptionList
          
def MP_CreateROIFromFileMask (tpl):    
        ROIDefs, ROIDataFile, niftiDataFile, NiftiDataSourceDescription, roiMaskMappings, maskFilePath, method, doNotOverwriteROI,  ExcludeExistingMaskedAreaFromImport, RemoveExistingMaskedAreaFromOverlapping, ProjectFileLockInterface, canAccessUnlockedFiles, ProgressDialog, ProjectDataset, ProgressDialogValueRange = tpl        
        #print ("Create ROI file from mask")
        niftiFilePath = niftiDataFile.getFilePath ()
        CanProcessData = False     
        HasData = False        
        #print ("test is Read Only?")
        if not ROIDataFile.isReadOnly () and ROIDataFile.isFileHandleLocked () :            
            ReleaseFilePLock = False
            CanProcessData = True
            if ROIDataFile.suppportsPersistentFileLocks () :
                status = ROIDataFile.getPersistentFileLockStatus ()
                if status == "UserLocked"  :                          
                    ReleaseFilePLock = False
                    #print ("File is user locked")
                elif status == "UserGroupLocked" or (status == "Unlocked" and canAccessUnlockedFiles) :                                
                    ReleaseFilePLock = True
                    #print ("file is unlocked")
                else:
                    CanProcessData = False
            #print ("can process Data")
            if CanProcessData :
                #print ("Load Data")
                tempDataDictionary = ROIDictionary (ROIDefs, None, None, LowMemory = True)                                                    
                try :                                                                              
                    tempDataDictionary.loadROIDictionaryFromFile (ROIDataFile, ProjectFileLockInterface)
                    #print ("loading dictionary")
                    tempDataDictionary.setNiftiDataSourceDescription (NiftiDataSourceDescription)                                                             
                    gotLock = True
                    if ReleaseFilePLock  :
                        #print ("get persistant lock")
                        gotLock = ROIDataFile.getPersistentFileLock ()     
                    if gotLock :
                        try :                        
                            volumeDescription = tempDataDictionary.getNIfTIVolumeOrientationDescription ()                                                            
                            loadedNiftiOrientation = "Default"
                            if (volumeDescription != None and volumeDescription.hasParameter ("SliceAxis") and tempDataDictionary.ROICountDefined () > 0) : 
                                loadedNiftiOrientation = volumeDescription.getParameter ("SliceAxis")                                                                                                                
                            # Load the new dataset  
                            
                            nifti = nib.load (niftiFilePath)        
                            NiftiData = NiftiVolumeData (nifti, SliceAxis = loadedNiftiOrientation, Verbose = False)
                            
                            if tempDataDictionary.hasDeepGrowAnnotations ()  :
                                roiList = tempDataDictionary.getROIListWithDeepGrowAnnotations ()
                                for roiName in roiList :
                                    tempDataDictionary.clearDeepGrowAnnotations (roiName)
                            tempDataDictionary.verrifyDictionaryIsConsistentWithPerimeterAreaSettings (NiftiData.getSliceDimensions (), ROIDefs, FileSaveThreadManger = None) # filesave manager is allready disabled no need to pass it along
                            
                            tempDataDictionary.setNIfTIVolumeOrientationDescription (NiftiData.getNIfTIVolumeOrientationDescription ())     
                            if (tempDataDictionary.ROICountDefined () == 0) :
                                tempDataDictionary.setROIAreaMode ()                                        
                            tempDataDictionary.versionDocument ("File before importing ROIFileMasks from: " + maskFilePath)  
                            tempDataDictionary.CreateDatasetROIFromMaskForDictionary (roiMaskMappings, maskFilePath, method, doNotOverwriteROI, niftiVolumeToMatchOrientation = NiftiData, ExcludeExistingMaskedAreaFromImport = ExcludeExistingMaskedAreaFromImport, RemoveExistingMaskedAreaFromOverlapping = RemoveExistingMaskedAreaFromOverlapping, FileSaveThreadManger = None, ProgressDialog = ProgressDialog, ProjectDataset = ProjectDataset, ProgressDialogValueRange = ProgressDialogValueRange)  # filesave manager is allready disabled no need to pass it along
                            tempDataDictionary.versionDocument ("File after importing ROIFileMasks from: " + maskFilePath)
                            tempDataDictionary.saveROIDictionaryToFile ()                                        
                        finally:
                            if ReleaseFilePLock :
                                ROIDataFile.releasePersistentFileLock (Comment = "Automatic release for ROI mask import")
                    else:
                        print ("A Error occured import Mask: %s for DS: %s and ROI: %s" % (maskFilePath, niftiFilePath, ROIDataFile.getPath ()))    
                except :
                     print ("A Error occured import Mask: %s for DS: %s and ROI: %s" % (maskFilePath, niftiFilePath, ROIDataFile.getPath ()))
                HasData = (tempDataDictionary.ROICountDefined () > 0 or tempDataDictionary.isScanPhaseDefined ())                                         
                tempDataDictionary.delete ()       
                del tempDataDictionary
        ROIDataPath = ROIDataFile.getPath ()        
        ROIDataFile.close ()           
        print ("ROIDatafile closed in MP Process")
        return CanProcessData, HasData, ROIDataPath


    
def _MP_ExportVolumeCSV (tpl) :               
           ProjectROIDefs, descriptionList, ROIDataFile, SaveFileNifitFileSourceFileObject, niftiDataFile, exportROIList, pointInSlice, pointCrossSlice, CombineNonTouchingROI,  ProjectFileLockInterface = tpl           
           returnData = None
           try :
               niftiDataFilePath = niftiDataFile.getFilePath ()   
               tempDataDictionary = None
               niftiVolume =  None
               DataFilePath = "Unknown"
               try :                                             
                    DataFilePath = ROIDataFile.getPath ()                                                              
                    ROIDefs = ProjectROIDefs.copy ()
                    tempDataDictionary = ROIDictionary (ROIDefs, None, None, LowMemory = True)            
                    tempDataDictionary.loadROIDictionaryFromFile (ROIDataFile, ProjectFileLockInterface )                                    
                                        
                    orientation = tempDataDictionary.getNIfTIVolumeOrientationDescription ()
                    SliceAxis = "Default"
                    if (orientation is not None and orientation.hasParameter ("SliceAxis")) :                        
                        SliceAxis = orientation.getParameter ("SliceAxis")
                            
                    niftiVolume = NiftiVolumeData (nib.load (niftiDataFilePath), SliceAxis = SliceAxis, Verbose = False)   
                    #tempDataDictionary.verrifyDictionaryIsConsistentWithPerimeterAreaSettings (niftiVolume.getSliceDimensions (), ROIDefs, FileSaveThreadManger = None)
                    tempDataDictionary.setNiftiDataSourceDescription (SaveFileNifitFileSourceFileObject)
                    if (tempDataDictionary.getNIfTIVolumeOrientationDescription  () is None) :
                        tempDataDictionary.setNIfTIVolumeOrientationDescription (niftiVolume.getNIfTIVolumeOrientationDescription ())                                                                                                      
                                                                                
                    returnData = ExportVolumeStatsCSV.getDatasetExportData (descriptionList, niftiVolume, tempDataDictionary, exportROIList, pointInSlice, pointCrossSlice, CombineNonTouchingROI= CombineNonTouchingROI, forceROIDefs = ProjectROIDefs)                    
               finally :         
                    if (ROIDataFile is not None) :
                        ROIDataFile.closeUnsaved ()
                    del ROIDataFile
                    if (tempDataDictionary is not None) :
                        tempDataDictionary.clear ()      
                    del tempDataDictionary
                    if (niftiVolume is not None) :
                        del niftiVolume                                                        
           except:
               print ("A Error occured processing: " + DataFilePath)           
               returnData = None               
           return returnData
       
   
def MP_ExportDatasetTags (tpl):                
        ROIDataFile, ProjectROIDefs, exportTagList, NIfTITreeNodeTreePath, LongFileNamePath,  ProjectFileLockInterface  = tpl 
        ROIDefs  = ProjectROIDefs.copy ()                                        
        tempDataDictionary = ROIDictionary (ROIDefs, None, None, LowMemory = True)                     
        tempDataDictionary.loadROIDictionaryFromFile (ROIDataFile,   ProjectFileLockInterface)                
                                        
        tagManager = tempDataDictionary.getDataFileTagManager ()
        outputLst = [NIfTITreeNodeTreePath, LongFileNamePath]                
        for exportTag in exportTagList :
            txt = ""
            if (exportTag.getType () == "Description"):
                txt = tempDataDictionary.getScanPhaseTxt ()                                        
            elif(exportTag.getType () == "Tag"): 
                tagidentifer = exportTag.getIdentifer ()
                tag = tagManager.getTagByIdentifer (tagidentifer)                        
                if (tag is not None) :
                    txt = tag.getTextValue ()                                                
                else:
                    txt = "[Value_Not_Set]"
            else:
                print ("Export Tag Type Not implemented: " + exportTag.getType ())
                txt = ""            
            outputLst.append (txt)
        ROIDataFile.closeUnsaved ()
        del ROIDataFile
        tempDataDictionary.delete ()
        del tempDataDictionary                                        
        return outputLst



class AdvancedFilterDialogOptions (QDialog) :   
    def __init__ (self,  parentWindow) :
        QDialog.__init__ (self, parentWindow)    
        self._parentWindow = parentWindow
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)        
        self.ui = Ui_AdvancedFileFilters ()        
        self.ui.setupUi (self)          
        self._option = parentWindow.getAdvancedFileFilterOptions ()
        
        fileSharing = parentWindow.getProjectDatsetInterface().getProjectFileInterface().getMultiUserFileSharingManager ()                    
        if fileSharing is None :
            self.ui.EditableImageFiles.setEnabled (False)
            if self._option == "EditiableImageFiles" :
                self._option = "AllImageFiles"
                
        if self._option == "AllImageFiles" :
            self.ui.AllImageFiles.setChecked (True)
        elif self._option == "EditiableImageFiles" :
            self.ui.EditableImageFiles.setChecked (True)
        elif self._option == "ImageFilesAssignedToMe" :
            self.ui.ImageFilesAssignedToMe.setChecked (True)
        self.ui.AllImageFiles.clicked.connect (self._allFiles) 
        self.ui.EditableImageFiles.clicked.connect (self._editableFiles) 
        self.ui.ImageFilesAssignedToMe.clicked.connect (self._imageFilesAssignedToMe) 
        self.ui.OkBtn.clicked.connect (self.close)
        
    def _allFiles (self) :
        self._option = "AllImageFiles"
        
    def _editableFiles (self) :
        self._option = "EditiableImageFiles"
        
    def _imageFilesAssignedToMe (self) :
        self._option = "ImageFilesAssignedToMe"
        
    def getFilterOption (self) :
        return self._option    
        
class RCC_DatasetDlg (QMainWindow) :   
         
    def __init__ (self, projectDataset, parent = None) :   
        self._openRecentFileMenuPathList = []
        self._advancedFileFilterOptions = "AllFiles"
        self._exportPath = ""
        self._ProjectDataset = projectDataset        
        self._resizeWidgetHelper = None
        QMainWindow.__init__ (self, parent)
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)         
        self.ui = Ui_Dataset ()               
        self.ui.setupUi (self)               
        self.menuBar().setNativeMenuBar (False)
        self._ProjectDataset.getNIfTIDatasetInterface ().setTreeViewWidget (self.ui.DatasetTreeView)
        self.ui.DatasetTreeView.setModel (self._ProjectDataset.getNIfTIDatasetInterface ())        
        self._resizeWidgetHelper = ResizeWidgetHelper ()     
        if self._ProjectDataset.getNIfTIDatasetInterface ().getInterfaceName () != "RCC_NiftiPesscaraDataInterface" :
            self.ui.DatasetTreeView.collapsed.connect (self._treeViewCollapsed)
            self.ui.DatasetTreeView.expanded.connect (self._treeViewExpanded)            
            self._resizeWidgetHelper.setHeight  (self.ui.DatasetTreeView, self.height () - 100)
            self._resizeWidgetHelper.setWidth  (self.ui.FilterText, self.width () - 52)
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.DatasetTreeView, 37)
            self.ui.FilterText.setVisible (True)
            self.ui.FilterLabel.setVisible (True)
            self.ui.FilterOptionsBtn.setVisible (True)
            self.ui.FilterText.editingFinished.connect (self._filterTextSet)
            self.ui.FilterOptionsBtn.clicked.connect (self._showAdvancedFilterOptions)
        else:
            self._resizeWidgetHelper.setHeight  (self.ui.DatasetTreeView, self.height () - 60)                                                              
            self._resizeWidgetHelper.setWidgetYPos  (self.ui.DatasetTreeView, 0)
            self.ui.FilterText.setVisible (False)
            self.ui.FilterLabel.setVisible (False)
            self.ui.FilterOptionsBtn.setVisible (False)
        
        
        treeSelectionModel = self.ui.DatasetTreeView.selectionModel ()
        treeSelectionModel.selectionChanged.connect (self._DatasetSelectionChanged)             
        self.ui.actionOpen_New_Slice_Viewing_Window.triggered.connect  (self.CreateNewViewingWindow)
        self.ui.actionROI_Editor.triggered.connect  (self.ROIEditorMenubarAction)                       
        self.ui.Quit.triggered.connect (self.closeApp)
        
        self.ui.actionExport_YAML.triggered.connect (self.exportMultiUserFileSharingYAML)
        self.ui.actionRemove_YAML.triggered.connect (self.removeMultiUserFileSharingYAML)
        self.ui.actionAdd_YAML.triggered.connect (self.addMultiUserFileSharingYAML)
    
        self.ui.actionNew_Project.triggered.connect (self.newProjectWizard)
        self.ui.actionOpen_Existing_Project.triggered.connect (self.openProject)
        #self.ui.actionNew_Project.setEnabled (not self._ProjectDataset.isInMultiUserCommandLineMode ())
        #self.ui.actionOpen_Existing_Project.setEnabled (not self._ProjectDataset.isInMultiUserCommandLineMode ())
         
        self.ui.menuImport.aboutToShow.connect (self.canImportROIMask)
        self.ui.actionROI_from_NIfTI_mask.triggered.connect (self.importROIFromMask)
        #disabled
        #self.ui.actionExportProject_info_csv.triggered.connect (self.exportProjectStatusCSV)
        
        self.ui.menuFile.aboutToShow.connect (self._aboutToShowFileMenu)
        
        self.ui.actionShow_Scan_phase_selection_window.triggered.connect (self.ShowScanPhaseSelectionDlg)        
        self.setWindowTitle ("%s (%s)" % (self._ProjectDataset.getFilename (), self._ProjectDataset.getProjectName ()))        
        self.ui.actionExportVolumeMasks.triggered.connect (self.exportVolumeMasks)
        self.ui.actionExportProject_volume_csv.triggered.connect (self.exportVolumeCSV)
        self.ui.actionUser_editor.triggered.connect (self.showSetUserName)
        self.ui.actionTag_editor.triggered.connect (self.showProjectTagEditor)
        self.ui.actionDataset_UI_Column_Chooser.triggered.connect (self.showDatasetColumnChooser)
        self.ui.actionRegistration_indicator_filter.triggered.connect (self.showRegistrationUIFilterDlg)
        self.ui.actionSet_Pesscara_Temporary_Data_Storage_Directory.triggered.connect (self.setPesscaraTemporaryStorageDirDlg)
        
        self.ui.actionExportTactic_dataset_to_FileSystem.triggered.connect (self.exportPesscaraDatasetToFileSystem)
        
        self.ui.actionImportNiftiImaging.triggered.connect (self.importNiftiImagingToProject)
        self.ui.actionImportRilContourProject.triggered.connect (self.importRCProjectImagingToProject)
        
        self.ui.actionLock_file.triggered.connect (self.tryToCheckOutFileSelection)
        self.ui.actionVersion_files.triggered.connect (self.tryToVersionFileSelection)
        self.ui.actionCheckin_file_lock.triggered.connect (self.tryToCommitFileSelection)
        self.ui.actionAssign_file.triggered.connect (self.tryToAssignFileSelection)
        self.ui.actionRevert_checkout.triggered.connect (self.tryToRevertFileSelection)
    
        self.ui.actionProject_tags.triggered.connect (self.exportProjectTagsDlg)
        self.ui.actionAbout.triggered.connect (self.showAboutSelfDlg)

        self.ui.actionHelp.triggered.connect (self.showHelpSelfDlg)


        self.ui.actionRegistration_indicator_filter.setEnabled (self.isLoadingROIFromFileSystem ())        
        self.ui.actionSet_Pesscara_Temporary_Data_Storage_Directory.setEnabled (not self.isLoadingROIFromFileSystem ())
        if projectDataset.isProjectReadOnly () :
            self.ui.actionTag_editor.setEnabled (False)
            self.ui.actionROI_Editor.setEnabled (False)
            
        headerData = self._ProjectDataset.getNIfTIDatasetInterface ().getUIHeader ()
        firstheaderitem = headerData.getVisibleHeaderByIndex (0)
        self.ui.DatasetTreeView.header().setMinimumSectionSize (30)
        self.ui.DatasetTreeView.header().setDefaultSectionSize (30)
        if not firstheaderitem.isHeaderSectionSizeInitlized () :        
            colWidth = int (float (self.width ()) * 0.50)
            self.ui.DatasetTreeView.setColumnWidth (0, colWidth)                        
        else:
            for headerindex in range (headerData.headerVisibleObjectCount ()) :
                headeritem = headerData.getVisibleHeaderByIndex (headerindex)
                sectionwidth = headeritem.getHeaderWidth ()
                self.ui.DatasetTreeView.setColumnWidth (headerindex, sectionwidth)   
        self._saveHeaderWidthsForResize ()   
        self._adjustTreeViewHeaderWidth ()        
       
        self.ui.DatasetTreeView.header().sectionResized.connect (self.SaveTreeViewColumnWidths)          
        if (self._ProjectDataset.getNIfTIDatasetInterface ().getInterfaceName () == "RCC_NiftiFileSystemDataInterface") :
            treeHeader = self.ui.DatasetTreeView.header ()
            treeHeader.setSectionsClickable (True)
            treeHeader.sectionClicked.connect (self._headerClickSort) 
            self._ExpandTreeNodes ()
        
        
        self.ui.DatasetTreeView.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.ui.DatasetTreeView.customContextMenuRequested.connect(self.showDatasetContextMenu)
    
    
    def importRCProjectImagingToProject (self) :                
        def _getRootSearchPath (RILContourProjectPath, header, objectKey, RootKey, RelativeKey) :            
            if header.hasFileObject (objectKey):
                roiFileSystemInterface = header.getFileObject (objectKey)                
                try :
                    ROIDatasetPath = roiFileSystemInterface.getParameter (RootKey)
                    if isinstance (ROIDatasetPath, list)  :
                        ROIDatasetPath = ROIDatasetPath[0]
                    if os.path.isdir (ROIDatasetPath) :
                        return ROIDatasetPath
                except:
                    pass                
                try :
                    ROIRelativePath = roiFileSystemInterface.getParameter (RelativeKey)
                    if ROIRelativePath is None :
                        ROIDatasetPath, _ = os.path.split (RILContourProjectPath)
                    else:
                        ROIDatasetPath = RelativePathUtil.getFullPathFromRelativePathList (ROIRelativePath, RILContourProjectPath)                                
                    if os.path.isdir (ROIDatasetPath) :
                        return ROIDatasetPath
                except:
                    pass
            return None
        
        try:
             dlg= QFileDialog( None )
             dlg.setWindowTitle('Select RILContour Project to Import Imaging & ROI Files From' )
             dlg.setViewMode( QFileDialog.Detail )    
             dlg.setFileMode (QFileDialog.ExistingFile)
             dlg.setAcceptMode (QFileDialog.AcceptOpen)
             dlg.setNameFilters( ['RIL-Contour project file (*.prj)', 'any file (*)'] )                          
             if dlg.exec_() and (len (dlg.selectedFiles())== 1):               
                 path = dlg.selectedFiles()[0]
                 if path != self._ProjectDataset.getPath () :
                     fobj = FileInterface ()   
                     fobj.readDataFile (path)                       
                     header = fobj.getFileHeader ()                         
                         
                     roiSearchPath = _getRootSearchPath (path, header, "RCC_ROIDataset_FileSystemInterface", "ROI_Dataset_Path", "ROI_Relative_Dataset_Path_List")
                     niftiSearchPath = _getRootSearchPath (path, header, "RCC_NiftiFileSystemDataInterface", "RootSearchPath", "RelativeRootSearchPathList")
                     if (roiSearchPath is not None and niftiSearchPath is not None):
                         _, FName = os.path.split (niftiSearchPath)
                         self._ProjectDataset.getNIfTIDatasetInterface ().importNifti (niftiSearchPath, SourceROIDataDir = os.path.join (roiSearchPath, FName))
        except:
             print ("Unexpected Error")
                 
                 
    def importNiftiImagingToProject (self) :         
         #if not isDICOM2NIfTIPackageInstalled () :
         #     MessageBoxUtil.showMessage ("Python Package Required for DICOM to NIfTI Conversion","pip install dicom2nifti to add support for multislice DICOM to NIfTI file conversion. Dicom2nifti is a pypi package and not part of the Conda distribution.")
         #     return 
         dlg = RCC_ImportImagingDlg (self._ProjectDataset, parent = self) 
         if dlg.exec_() == QDialog.Accepted :             
            try:
                 state = self._ProjectDataset.getImportImagingDlgState ()
                 path = state["Path"]
                 organization = state["Organization"]
                 patientMapping = state["PatientIDMapping"]
                 try :
                    PreferredFileFormat = state["PreferredFormat"]
                 except:
                     PreferredFileFormat = ".nii.gz"
                 imagingImported, imagingNotImported = self._ProjectDataset.getNIfTIDatasetInterface ().importNifti (path, TryOrganizeDataBySubjectAccessionExam = organization == "Dicom", PatientIDFilter = patientMapping, PreferredFileFormat = PreferredFileFormat) 
                 if organization == "Dicom" :
                     if len (imagingNotImported) > 0 :
                         fpathList = []
                         pathsNotImported = ""
                         for tpl in imagingNotImported :
                             dirName, fileList = tpl
                             if isinstance (dirName, list) :
                                 dirName = os.sep.join (dirName)
                             dirName = os.path.join (path, dirName)
                             for fname in fileList :
                                fpath = os.path.join (dirName, fname)
                                fpathList.append (fpath)
                             pathsNotImported = "\n".join (fpathList)
                         if len (pathsNotImported) > 0 :
                             MessageBoxUtil.showMultiLineMessage ("Files Import Warrning", "Files not imported due to missing patientID, accession number, or series description dicom tags.", pathsNotImported,self)
            except Exception as exp:
                print ("Unexpected Error" + str (exp))
    
    def getProjectDatsetInterface (self) :
        return self._ProjectDataset
    
    def setAdvancedFileFilterOption (self, option) :
        if self._advancedFileFilterOptions != option :
           self._advancedFileFilterOptions = option
           return True
        return False
    
    def getAdvancedFileFilterOptions (self) :
        return self._advancedFileFilterOptions
    
    def _showAdvancedFilterOptions (self) :
       dlg = AdvancedFilterDialogOptions (self)     
       dlg.exec_ ()
       if self.setAdvancedFileFilterOption (dlg.getFilterOption ()) :
           text = self.ui.FilterText.text ()
           text = text.strip ()
           self._ProjectDataset.getNIfTIDatasetInterface ().setFilterText (text, self.getAdvancedFileFilterOptions ())
    
    def _filterTextSet (self) :        
        try :
            text = self.ui.FilterText.text ()
            text = text.strip ()
            datasetInterface = self._ProjectDataset.getNIfTIDatasetInterface ()
            if datasetInterface is not None :
                datasetInterface.setFilterText (text, self.getAdvancedFileFilterOptions ())
        except:
            print ("Error occured setting filter text.")
        
    def showDatasetContextMenu (self, pos) :        
        if self._ProjectDataset.isRunningInDemoMode () :
            return
        globalPos = self.ui.DatasetTreeView.mapToGlobal(pos)        
        myMenu = QMenu(self)        
        Version_filesEnabled  = False
        Checkout_fileEnabled = False
        Checkin_file_lockEnabled = False
        Assign_fileEnabled = False
        Revert_checkoutEnabled = False        
        IsPesscara =  self._ProjectDataset.getROIDatasetInterface ().isPesscaraInterface ()
        if not IsPesscara :
            selectedTreeNodeStatus = self._getSelctedTreeNodeStatus ()
            selectionContainsUsersLockedFiles = "UserLocked" in selectedTreeNodeStatus
            selectionContainsGroupLockedFiles = "UserGroupLocked" in selectedTreeNodeStatus
            selectionContainsValidUnlockedLockedFiles = ("Unlocked" in selectedTreeNodeStatus and self.canAccessUnlockedFiles ())
        
            AdminMode = self._ProjectDataset.isInSuperUserCommandLineMode ()
            AdminModeSelectionContainsOtherUserLockedFiles = AdminMode and "OtherUserLocked" in selectedTreeNodeStatus
            AdminModeSelectionContainsLockedFiles = AdminMode and (selectionContainsUsersLockedFiles or "OtherUserLocked" in selectedTreeNodeStatus)
            
            Version_filesEnabled = (selectionContainsUsersLockedFiles or selectionContainsGroupLockedFiles or selectionContainsValidUnlockedLockedFiles)
            
            Checkin_file_lockEnabled = selectionContainsUsersLockedFiles
            Revert_checkoutEnabled = selectionContainsUsersLockedFiles or AdminModeSelectionContainsLockedFiles
            Checkout_fileEnabled = (selectionContainsGroupLockedFiles or selectionContainsValidUnlockedLockedFiles or AdminModeSelectionContainsOtherUserLockedFiles)                        
            if (selectionContainsGroupLockedFiles or selectionContainsUsersLockedFiles  or selectionContainsValidUnlockedLockedFiles or AdminModeSelectionContainsOtherUserLockedFiles) :
                fileShareing = self._ProjectDataset.getProjectFileInterface ().getMultuserFileSharingAssignments ()
                if fileShareing is None or len (fileShareing) <= 0 :
                    Assign_fileEnabled = False
                else:
                    Assign_fileEnabled = len (fileShareing) > 0
            else:
                Assign_fileEnabled =False        
        versionFiles = QAction ("Version files",self)
        versionFiles.setEnabled (Version_filesEnabled)
        versionFiles.triggered.connect (self.tryToVersionFileSelection)        
        myMenu.addAction (versionFiles)        
        myMenu.addSeparator()
        
        checkoutFiles = QAction ("Checkout files",self)
        checkoutFiles.setEnabled (Checkout_fileEnabled)
        checkoutFiles.triggered.connect (self.tryToCheckOutFileSelection)    
        myMenu.addAction (checkoutFiles)        
        
        revertCheckoutFiles = QAction ("Revert file checkout",self)
        revertCheckoutFiles.setEnabled (Revert_checkoutEnabled)
        revertCheckoutFiles.triggered.connect (self.tryToRevertFileSelection)    
        myMenu.addAction (revertCheckoutFiles)        
        
        checkinFiles = QAction ("Check-in files",self)
        checkinFiles.setEnabled (Checkin_file_lockEnabled)
        checkinFiles.triggered.connect (self.tryToCommitFileSelection)    
        myMenu.addAction (checkinFiles)        
                
        assignFiles = QAction ("Assign files",self)
        assignFiles.setEnabled (Assign_fileEnabled)
        assignFiles.triggered.connect (self.tryToAssignFileSelection)    
        myMenu.addAction (assignFiles)                
        myMenu.addSeparator()
               
        importmask = QAction ("Import ROI mask",self)
        importmask.setEnabled (self.isLoadingROIFromFileSystem ())
        importmask.triggered.connect (self.importROIFromMask)    
        myMenu.addAction (importmask)                
        myMenu.addSeparator()
        
        exportmask = QAction ("Export volume mask",self)
        exportmask.setEnabled (True)
        exportmask.triggered.connect (self.exportVolumeMasks)    
        myMenu.addAction (exportmask)       
        
        exportvoxelannotation = QAction ("Export voxel csv",self)
        exportvoxelannotation.setEnabled (True)
        exportvoxelannotation.triggered.connect (self.exportVolumeCSV)    
        myMenu.addAction (importmask)       
        
        exportseriesannotation = QAction ("Export series csv",self)
        exportseriesannotation.setEnabled (True)
        exportseriesannotation.triggered.connect (self.exportProjectTagsDlg)    
        myMenu.addAction (exportseriesannotation) 
        
        exportseriesannotation = QAction ("Export series csv",self)
        exportseriesannotation.setEnabled (True)
        exportseriesannotation.triggered.connect (self.exportProjectTagsDlg)    
        myMenu.addAction (exportseriesannotation) 
                           
        if not IsPesscara :
            myMenu.addSeparator()          
            SeedNodeList = self.ui.DatasetTreeView.selectionModel().selectedRows ()         
            showfilepopertiesAction = QAction ("Properties",self)
            showfilepopertiesAction.setEnabled (len (SeedNodeList) == 1)
            showfilepopertiesAction.triggered.connect (self.showSelectedFileProperties)    
            myMenu.addAction (showfilepopertiesAction) 
                        
            myMenu.addSeparator()          

            SeedNodeList = self.ui.DatasetTreeView.selectionModel().selectedRows ()         
            renameLongFileNameAction = QAction ("Rename",self)
            Enabled = False
            if not self._ProjectDataset.getProjectFileInterface().isProjectReadOnly () :
                if len (SeedNodeList) == 1 :
                    rootNode = self._ProjectDataset.getNIfTIDatasetInterface().getRootMemory ()
                    leafsList = self._getSelectedTreeNodeLeafs (AllFiles = True, IgnoreRoot = True)
                    if len (leafsList) == 1 :
                        testNode = leafsList[0]
                    else:
                        testNode = SeedNodeList[0].internalPointer ()
                    Enabled = testNode != rootNode[0]                
            renameLongFileNameAction.setEnabled (Enabled)
            renameLongFileNameAction.triggered.connect (self.showRenameFileDlg)    
            myMenu.addAction (renameLongFileNameAction) 

            if self._ProjectDataset.getNIfTIDatasetInterface ().showShortFileName () :
               
                showShortFileNames = QAction ("Show long file names",self)
                showShortFileNames.setEnabled (True)
                showShortFileNames.triggered.connect (self.showLongFileNames)    
                myMenu.addAction (showShortFileNames) 
            else :
                showLongFileNames = QAction ("Show short file names",self)
                showLongFileNames.setEnabled (True)
                showLongFileNames.triggered.connect (self.showShortFileNames)    
                myMenu.addAction (showLongFileNames) 
            
            
        myMenu.exec_(globalPos)
    
    def showSelectedFileProperties (self)  : 
        SeedNodeList = self.ui.DatasetTreeView.selectionModel().selectedRows () 
        if len (SeedNodeList) == 1 :            
            leafsList = self._getSelectedTreeNodeLeafs (AllFiles = True, IgnoreRoot = True)
            if len (leafsList) == 1 :
                RCC_FileProperties (leafsList[0], self._ProjectDataset, self)
            else:
                RCC_FileProperties (SeedNodeList[0].internalPointer (), self._ProjectDataset, self)
        
    def showRenameFileDlg (self) :
        if self._ProjectDataset.getProjectFileInterface().isProjectReadOnly () :
            return
        SeedNodeList = self.ui.DatasetTreeView.selectionModel().selectedRows () 
        if len (SeedNodeList) == 1 :            
            leafsList = self._getSelectedTreeNodeLeafs (AllFiles = True, IgnoreRoot = True)
            if len (leafsList) == 1 :
                RCC_RenameLongFileName (leafsList[0], self._ProjectDataset, self)
            else:
                RCC_RenameLongFileName (SeedNodeList[0].internalPointer (), self._ProjectDataset, self)
                
        
        
    def showShortFileNames (self) :
        self._ProjectDataset.getNIfTIDatasetInterface ().setShowShortFileName (True)
        
    def showLongFileNames (self) :
        self._ProjectDataset.getNIfTIDatasetInterface ().setShowShortFileName (False)
        
    def _treeViewCollapsed  (self, qModelIndex) :
        try:
            if qModelIndex.isValid() :
                path = qModelIndex.internalPointer().getTreePathList ()
                TreeNodesExpanded = self._ProjectDataset.getExpandedTreeNodes ()
                if (TreeNodesExpanded is None) :
                    return
                root = TreeNodesExpanded
                for nodeindex in range (len(path)-1) :
                    node = path[nodeindex]
                    if node not in root :
                        return
                    root = root[node]
                node = path[-1]
                if node in root :
                    del root[node]
                self._ProjectDataset.setExpandedTreeNodes (TreeNodesExpanded)
        except:
            pass
        
    def _treeViewExpanded (self, qModelIndex) :
         try:
            if qModelIndex.isValid() :
                path = qModelIndex.internalPointer().getTreePathList ()
                TreeNodesExpanded = self._ProjectDataset.getExpandedTreeNodes ()
                if (TreeNodesExpanded is None) :
                    return
                root = TreeNodesExpanded
                for node in path  :
                    if node not in root :  
                         root[node] = {}
                    root = root[node]
                self._ProjectDataset.setExpandedTreeNodes (TreeNodesExpanded)
         except:
            pass
    
        
    def _ExpandTreeNodes (self) :
        if self._ProjectDataset.getNIfTIDatasetInterface ().getInterfaceName () == "RCC_NiftiPesscaraDataInterface" :
            return 
        root = self._ProjectDataset.getExpandedTreeNodes ()
        if root is None or len (root) == 0 :
            rootNodes = self._ProjectDataset.getNIfTIDatasetInterface ().getRootNodeList ()
            root = {}
            for node in rootNodes :
                root[node] = {}
        if len (root) > 0 :
            path = None
            NodeQue = [(root, path)]
            treeModel = self._ProjectDataset.getNIfTIDatasetInterface ()
            while len (NodeQue) > 0:
                node, path = NodeQue.pop () 
                for key, value in node.items () :
                    if path is None :
                        path = [key]
                    else:
                        path += [key]
                    index = treeModel.getPathQModelIndex (path)
                    if index is not None :
                        self.ui.DatasetTreeView.setExpanded (index, True)
                        for childKey, childValue in value.items () :
                            childNode = {}
                            childNode[childKey] = childValue
                            NodeQue.append ((childNode, copy.copy (path)))
                    
        
    def _headerClickSort (self, index) :
        treeHeader = self.ui.DatasetTreeView.header ()
        treeHeader.setSortIndicatorShown (True)
        interface = self._ProjectDataset.getNIfTIDatasetInterface ()
        if interface is None :
            return
        direction = 1
        currentIndex = self.ui.DatasetTreeView.selectionModel().currentIndex()
        selectedNode = currentIndex.internalPointer ()
        if (index == 0) :
           direction = interface.getPrimaryColumnSortDirection  ()
           if direction == None :
               direction = -1
           else:
               direction *= -1
           interface.setPrimaryColumnSortDirection (direction)
        else:
           columnType = interface.getUIHeader().getVisibleHeaderType (index)
           if (columnType is not None) :
               if (columnType == "Tag") :
                   headerObj = interface.getUIHeader().getVisibleHeaderByIndex (index)
                   tagTagIdentifer = headerObj.getIdentifer ()
               else:
                   tagTagIdentifer = None
               direction = interface.getSecondaryColumnSortDirection  (columnType, tagTagIdentifer)
               if direction == None :
                   direction = -1
               else:
                   direction *= -1
               interface.setSecondaryColumnSortDirection (columnType, direction, tagTagIdentifer)
        if selectedNode is not None :
            self.ui.DatasetTreeView.selectionModel().select (interface.createIndex(selectedNode.getRow (), 0, selectedNode), QtCore.QItemSelectionModel.SelectCurrent | QtCore.QItemSelectionModel.Rows| QtCore.QItemSelectionModel.Clear) 
          
        treeHeader = self.ui.DatasetTreeView.header ()
        if direction > 0 : 
            sortOrderIndicator = QtCore.Qt.AscendingOrder
        else:
            sortOrderIndicator = QtCore.Qt.DescendingOrder
        treeHeader.setSortIndicator (index, sortOrderIndicator)
        #self.ui.DatasetTreeView.update ()  
    
    def openProjectFile (self, projectPath) :    
       if self._ProjectDataset.activateExistingOpenProject (projectPath) or self._ProjectDataset.createProjectFromFile (projectPath) :
           self._addToFileRecentMenu (projectPath)     
           
    def _openRecentFile (self, filelist, index):
        if (index < len (filelist) and index >= 0) :
            try :
                projectPath = filelist[index]
                self.openProjectFile (projectPath)
                
            except:
                pass                
        
    def _buildOpenRecentMenu (self) :
        try :
            filemenulist = []
            home = self._ProjectDataset.getHomeFilePath ()
            try :
                filePath = os.path.join (home,"OpenRecentFileList.txt")
                if (os.path.isfile (filePath) and os.path.exists  (filePath)) :
                    filemenu = open (filePath, "rt")
                    try:
                        for line in filemenu :
                            filePath = line.strip ()                            
                            if len (filePath) > 0 :
                                filePath = FileUtil.correctOSPath (filePath)                            
                                if (os.path.isfile (filePath) and os.path.exists  (filePath)) :
                                    filemenulist.append (filePath)
                    finally:
                        filemenu.close ()
            except :
                pass                
            lastPath = self._ProjectDataset.getLastProjectOpened ()
            if lastPath not in filemenulist :
                filemenulist.append (lastPath)
                self._addToFileRecentMenu (lastPath)
            while len (filemenulist) > 10 :
                del filemenulist[0]
            
            if len (filemenulist) > 1 :
                filemenulist.reverse ()
                
            recentFileMenu = QMenu ()
            self._openRecentFileMenuPathList = []
            for count, filePath in enumerate (filemenulist) :
                self._openRecentFileMenuPathList.append (filePath)
                #_, fname = os.path.split (filePath)
                action = recentFileMenu.addAction (filePath) 
                action.setEnabled (os.path.isfile (filePath) and os.path.exists (filePath))
                func = functools.partial (self._openRecentFile, self._openRecentFileMenuPathList, count)
                action.triggered.connect (func)                
                    
            self.ui.actionOpen_RecentProject.setMenu (recentFileMenu)
            
            self.ui.actionOpen_RecentProject.setEnabled (len (filemenulist) > 0)
        except:
            self.ui.actionOpen_RecentProject.setEnabled (False)
       
    def _addToFileRecentMenu (self, path) :
        if (os.path.isfile (path) and os.path.exists  (path)) : 
            try :
                filemenulist = []
                home = self._ProjectDataset.getHomeFilePath ()
                filePath = os.path.join (home,"OpenRecentFileList.txt")
                if os.path.isfile (filePath) and os.path.exists  (filePath) :
                    filemenu = open (filePath, "rt")
                    try :
                        for line in filemenu :                            
                            line = FileUtil.correctOSPath (line.strip ())                            
                            if len (line) > 0 and (os.path.isfile (line) and os.path.exists  (line) and line != path) :
                                filemenulist.append (line)
                    finally :
                        filemenu.close ()
                while len (filemenulist) > 10 :
                    del filemenulist[0]
            except:
                filemenulist = []            
            filemenulist.append (path)
            try :
                filemenu = open (os.path.join (home,"OpenRecentFileList.txt"), "wt")
                try :
                    for line in filemenulist:
                        filemenu.write(line + "\n")
                finally:
                    filemenu.close ()
            except:
                pass                
    
    def _getSelectedTreeNodeLeafs (self, AllFiles = False, IgnoreRoot = False) :
        if self.ui.DatasetTreeView.selectionModel() is None :
            return []
        SeedNodeList = self.ui.DatasetTreeView.selectionModel().selectedRows () 
        nodePtrLst = set ()
        for node in SeedNodeList :
            nodePtrLst.add (node.internalPointer ())               
        if IgnoreRoot :
            removeList = set ()
            for node in nodePtrLst :
                if node.isRootNode () and not node.hasOneChild ():
                    removeList.add (node)
            for node in removeList :
                nodePtrLst.remove (node)
        if len (nodePtrLst) == 0 :
            return []
        nodePtrLst = list (nodePtrLst)
        return self._ProjectDataset.getNIfTIDatasetInterface ().getNiftiTreeNodeList (Memory = nodePtrLst, AllFiles = AllFiles)
    
    def _getSelctedTreeNodeStatus (self):
        self._ProjectDataset.getProjectFileInterface ().aquireFileLock ()
        try :        
            pNodeStatus = set ()
            selectedLeafs = self._getSelectedTreeNodeLeafs (AllFiles = True)            
            for node in selectedLeafs :
                pNodeStatus.add (node.getCurrentUserPLockStatus (AquireFileLock = False, UseDecompressionCache = False))
            return pNodeStatus
        finally:
           self._ProjectDataset.getProjectFileInterface ().releaseFileLock ()
       
    def canAccessUnlockedFiles (self) :
        if self._ProjectDataset.isInSuperUserCommandLineMode () :
            return True 
        projectFileInterface = self._ProjectDataset.getProjectFileInterface ()
        if projectFileInterface.isFileUnlockingCommandLineModeEnabled ()  :
                return True            
        UserName = projectFileInterface.getUserName ()        
        multiUser = projectFileInterface.getMultiUserFileSharingManager ()
        if multiUser is not None and multiUser.isInitalized () :
            return multiUser.canLockUnlockedFiles (UserName)  
        return True
        
    def _aboutToShowFileMenu (self) :
        self._buildOpenRecentMenu ()
        #self.ui.actionOpen_RecentProject.setEnabled (not self._ProjectDataset.isInMultiUserCommandLineMode ())
        isDatasetStoredInTactic = self._ProjectDataset.getNIfTIDatasetInterface ().getInterfaceName () == "RCC_NiftiPesscaraDataInterface"
        self.ui.actionExportTactic_dataset_to_FileSystem.setEnabled (isDatasetStoredInTactic)
        self.ui.actionExportTactic_dataset_to_FileSystem.setVisible (isDatasetStoredInTactic)
        
        self.ui.actionVersion_files.setEnabled (False)
        self.ui.actionLock_file.setEnabled (False)
        self.ui.actionCheckin_file_lock.setEnabled (False)
        self.ui.actionAssign_file.setEnabled (False)
        self.ui.actionRevert_checkout.setEnabled (False)
        self.ui.actionAdd_YAML.setEnabled (False)
        self.ui.actionExport_YAML.setEnabled (False)
        self.ui.actionRemove_YAML.setEnabled (False)
        self.ui.menuAssign_files.setEnabled (False)
        DemoMode = self._ProjectDataset.isRunningInDemoMode () 
                    
        self.ui.actionImportNiftiImaging.setEnabled (False)
        self.ui.actionImportRilContourProject.setEnabled (False)
                        
        IsPesscara =  self._ProjectDataset.getROIDatasetInterface ().isPesscaraInterface ()
        if not IsPesscara and not DemoMode :
            mufs = self._ProjectDataset.getProjectFileInterface().getMultiUserFileSharingManager ()
            isMufsLoaded = mufs is not None and mufs.hasEmbededYAML ()
            self.ui.actionExport_YAML.setEnabled (isMufsLoaded)            
            self.ui.actionRemove_YAML.setEnabled (isMufsLoaded and not self._ProjectDataset.getProjectFileInterface().isProjectReadOnly ())
            self.ui.actionAdd_YAML.setEnabled    (not self._ProjectDataset.getProjectFileInterface().isProjectReadOnly () and mufs is None or (mufs is not None and not mufs.hasEmbededYAML ()))
              
            selectedTreeNodeStatus = self._getSelctedTreeNodeStatus ()
            selectionContainsUsersLockedFiles = "UserLocked" in selectedTreeNodeStatus
            selectionContainsGroupLockedFiles = "UserGroupLocked" in selectedTreeNodeStatus
            selectionContainsValidUnlockedLockedFiles = ("Unlocked" in selectedTreeNodeStatus and self.canAccessUnlockedFiles ())
            
            AdminMode = self._ProjectDataset.isInSuperUserCommandLineMode ()
            AdminModeSelectionContainsOtherUserLockedFiles = AdminMode and "OtherUserLocked" in selectedTreeNodeStatus
            AdminModeSelectionContainsLockedFiles = AdminMode and (selectionContainsUsersLockedFiles or "OtherUserLocked" in selectedTreeNodeStatus)
                    
            self.ui.actionImportNiftiImaging.setEnabled (not self._ProjectDataset.getProjectFileInterface().isProjectReadOnly ())
            self.ui.actionImportRilContourProject.setEnabled (not self._ProjectDataset.getProjectFileInterface().isProjectReadOnly ())
            
            self.ui.actionVersion_files.setEnabled (selectionContainsUsersLockedFiles or selectionContainsGroupLockedFiles or selectionContainsValidUnlockedLockedFiles)
            self.ui.actionCheckin_file_lock.setEnabled (selectionContainsUsersLockedFiles)
            self.ui.actionRevert_checkout.setEnabled (selectionContainsUsersLockedFiles or AdminModeSelectionContainsLockedFiles)
            self.ui.actionLock_file.setEnabled (selectionContainsGroupLockedFiles or selectionContainsValidUnlockedLockedFiles or AdminModeSelectionContainsOtherUserLockedFiles)                        
            if (selectionContainsGroupLockedFiles or selectionContainsUsersLockedFiles  or selectionContainsValidUnlockedLockedFiles or AdminModeSelectionContainsOtherUserLockedFiles) :
                fileShareing = self._ProjectDataset.getProjectFileInterface ().getMultuserFileSharingAssignments ()
                if fileShareing is None or len (fileShareing) <= 0 :
                    self.ui.actionAssign_file.setEnabled (False)                    
                else:
                    self.ui.actionAssign_file.setEnabled (len (fileShareing) > 0)
            else:
                self.ui.actionAssign_file.setEnabled (False)
            
            self.ui.menuAssign_files.setEnabled (self.ui.actionLock_file.isEnabled () or self.ui.actionRevert_checkout.isEnabled () or self.ui.actionCheckin_file_lock.isEnabled () or
                                                 self.ui.actionAssign_file.isEnabled () or self.ui.actionAdd_YAML.isEnabled () or self.ui.actionRemove_YAML.isEnabled () or
                                                 self.ui.actionExport_YAML.isEnabled ())
    
    
    
    def addMultiUserFileSharingYAML (self) :
        if self._ProjectDataset.isRunningInDemoMode () :
            return
            
        if not self._ProjectDataset.isProjectReadOnly () :
            mufs = self._ProjectDataset.getProjectFileInterface().getMultiUserFileSharingManager ()
            if mufs is None :
                mufs = MultiUserManager (None)
            if  mufs is not None and not mufs.hasEmbededYAML() :
                dlg= QFileDialog( None )
                dlg.setWindowTitle('Load multiuser file sharing YAML' )
                dlg.setViewMode( QFileDialog.Detail )    
                dlg.setFileMode (QFileDialog.AnyFile)
                dlg.setNameFilters( ['YAML file (*.yaml)','txt file (*.txt)', 'any file (*)'] )                          
                dlg.setAcceptMode (QFileDialog.AcceptOpen)
                if dlg.exec_() and (len (dlg.selectedFiles())== 1):
                    if not mufs.setInternalYAMLFile (dlg.selectedFiles()[0], self._ProjectDataset.getProjectFileInterface().getProjectPath ()) :
                         MessageBoxUtil.showMessage ("Error Importing YAML", "A error occured importing the YAML file. Multiuser filesharing not initalized.")
                    else:
                        YamlDefWarrnings = mufs.validateYaml ()
                        if len (YamlDefWarrnings) > 0 :
                            print ("")
                            print ("Warrning possible errors in the loaded multi-user YAML definition.")
                            print ("")
                            for line in YamlDefWarrnings :
                                print (line)
                            MessageBoxUtil.showMessage ("Multiuser File-sharing", "Warrning possible errors in the loaded multi-user YAML definition. Click details for more info","\n".join (YamlDefWarrnings)) 
                                            
                        self._ProjectDataset.getProjectFileInterface().setMultiUserFileSharingManager (mufs)
                        self._ProjectDataset.saveProject ()
                        MessageBoxUtil.showMessage ("Multiuser YAML Imported", "To make future changes to the project RIL-Contour will need to be started in administrator mode.  To do this: run rilcontour with the -admin parameter. Example 'rilcontour -admin'")
                
    def exportMultiUserFileSharingYAML (self) :
        if self._ProjectDataset.isRunningInDemoMode () :
            return
        mufs = self._ProjectDataset.getProjectFileInterface().getMultiUserFileSharingManager ()
        if mufs is not None and mufs.hasEmbededYAML () :
            dlg= QFileDialog( None )
            dlg.setWindowTitle('Export multiuser file sharing YAML' )
            dlg.setViewMode( QFileDialog.Detail )    
            dlg.setFileMode (QFileDialog.AnyFile)
            dlg.setNameFilters( ['YAML file (*.yaml)','txt file (*.txt)', 'any file (*)'] )                
            dlg.setAcceptMode (QFileDialog.AcceptSave)
            if dlg.exec_() and (len (dlg.selectedFiles())== 1): 
                try :
                    mufs.saveInternalYAMLToFile (dlg.selectedFiles()[0])
                except:
                    MessageBoxUtil.showMessage ("Error Exporting YAML", "A error occured exporting the YAML file.")
                    
    def removeMultiUserFileSharingYAML (self) :
        if self._ProjectDataset.isRunningInDemoMode () :
            return
        if not self._ProjectDataset.isProjectReadOnly () :
            mufs = self._ProjectDataset.getProjectFileInterface().getMultiUserFileSharingManager ()
            isMufsLoaded = mufs is not None and mufs.hasEmbededYAML ()
            if isMufsLoaded :
                mufs.removeInternalYAMLFile ()
                self._ProjectDataset.saveProject ()
                    
            
    def tryToCheckOutFileSelection (self) :
        if self._ProjectDataset.isRunningInDemoMode () :
            return
        IsPesscara =  self._ProjectDataset.getROIDatasetInterface ().isPesscaraInterface ()
        if IsPesscara :
            return
        leafsList = self._getSelectedTreeNodeLeafs (AllFiles = True, IgnoreRoot = True)        
        if len (leafsList) > 0 :
            FileErrorList = []            
            progdialog = QProgressDialog(" ", "Cancel", int (0), len (leafsList), self)  
            progdialog.setMinimumDuration (0)
            progdialog.setWindowFlags(progdialog.windowFlags () | QtCore.Qt.Dialog | QtCore.Qt.WindowStaysOnTopHint)          
            progdialog.setWindowModality(QtCore.Qt.ApplicationModal)       
            progdialog.setWindowTitle ("Acquiring files")   
            try:
                progdialog.forceShow() 
                progdialog.setValue (0)
                self._ProjectDataset.getApp ().processEvents ()
                SuperOverride = None
                for nodeindex, node in enumerate (leafsList) :
                    progdialog.setValue (nodeindex)
                    self._ProjectDataset.getApp ().processEvents ()
                    try :
                        pLockAcquired = False
                        filePath = "Error occured getting ROI File Handle"
                        ROIDataFile = self._ProjectDataset.getROIDatasetInterface ().getROIDataForDatasetPath (node, UserNameStr = self._ProjectDataset.getUserName (), SilentIgnoreOnLockFail = True, SuperUserFroceSilentGrabLock = True)
                        if ROIDataFile is not None :
                            filePath = ROIDataFile.getPath ()
                            status = ROIDataFile.getPersistentFileLockStatus ()
                            if status == "OtherUserLocked" and self._ProjectDataset.isInSuperUserCommandLineMode () :
                                if SuperOverride is None :
                                    SuperOverride = MessageBoxUtil.showWarningMessageBox  ("Administrator Override","Another user currently holds the file lock. Do you really wish to checkout the file lock?") 
                                if SuperOverride :
                                    ROIDataFile.forceCloseOpenPLocks ()      
                                    status = ROIDataFile.getPersistentFileLockStatus ()
                            if status == "UserGroupLocked" or (status == "Unlocked" and self.canAccessUnlockedFiles ()) :
                                if (ROIDataFile.isFileHandleLocked ()) :       
                                    if ROIDataFile.getPersistentFileLock () :
                                        pLockAcquired = True
                            if not pLockAcquired and status != "UserLocked" :
                                FileErrorList.append (filePath)
                        ROIDataFile.closeUnsaved()
                        del ROIDataFile                        
                    except:
                        pass
                    if progdialog.wasCanceled () :
                        break
            finally:               
                progdialog.close ()                 
                self._ProjectDataset.getApp ().processEvents ()
            if len (FileErrorList) > 0 :
                ErrorListDlg.showErrorListDlg (self, "Acquire File Warrning" ,"The following files were locked by other users and could not be acquired.", FileErrorList)
    
    def tryToVersionFileSelection (self) :
        if self._ProjectDataset.isRunningInDemoMode () :
            return
        IsPesscara =  self._ProjectDataset.getROIDatasetInterface ().isPesscaraInterface ()
        if IsPesscara :
            return        
        succeed, Comment = FileVersionUI.showCreateVersionFileDlg (self)
        if succeed :
            leafsList = self._getSelectedTreeNodeLeafs (AllFiles = True, IgnoreRoot = True)
            if len (leafsList) > 0 :
                FileErrorList = []                
                progdialog = QProgressDialog(" ", "Cancel", int (0), len (leafsList), self)  
                progdialog.setMinimumDuration (0)
                progdialog.setWindowFlags(progdialog.windowFlags () | QtCore.Qt.Dialog | QtCore.Qt.WindowStaysOnTopHint)          
                progdialog.setWindowModality(QtCore.Qt.ApplicationModal)       
                progdialog.setWindowTitle ("Versioning files")   
                try:                    
                    progdialog.forceShow() 
                    progdialog.setValue(0)
                    self._ProjectDataset.getApp ().processEvents ()
                    for nodeindex, node in enumerate (leafsList) :
                        progdialog.setValue (nodeindex)
                        self._ProjectDataset.getApp ().processEvents ()
                        try :
                            fileVersioned = False
                            ROIDataFile = self._ProjectDataset.getROIDatasetInterface ().getROIDataForDatasetPath (node, UserNameStr = self._ProjectDataset.getUserName (), SilentIgnoreOnLockFail = True)                                      
                            if (ROIDataFile is not None) :
                                if  ROIDataFile.isFileHandleLocked () :       
                                    status = ROIDataFile.getPersistentFileLockStatus ()
                                    if status == "UserLocked"  :   
                                            ROIDataFile.versionFile (Comment = Comment, ForceVersion = True)
                                            fileVersioned = True
                                    elif status == "UserGroupLocked" or (status == "Unlocked" and self.canAccessUnlockedFiles ()) :
                                        if ROIDataFile.getPersistentFileLock (VersionComment = Comment, ForceVersion = True)  :                                             
                                             ROIDataFile.releasePersistentFileLock (Comment = Comment)
                                             fileVersioned = True
                                if not fileVersioned :
                                    FileErrorList.append (ROIDataFile.getPath ())
                                ROIDataFile.closeUnsaved ()   
                            del ROIDataFile
                        except:
                            pass
                        if progdialog.wasCanceled () :
                            break
                finally:
                    progdialog.close ()
                    self._ProjectDataset.getApp ().processEvents ()
                if len (FileErrorList) > 0 :
                    ErrorListDlg.showErrorListDlg (self, "Acquire File Warrning" ,"The following files were locked by other users and could not be versioned.", FileErrorList)
    
    def tryToRevertFileSelection (self) :
        if self._ProjectDataset.isRunningInDemoMode () :
            return
        IsPesscara =  self._ProjectDataset.getROIDatasetInterface ().isPesscaraInterface ()
        if IsPesscara :
            return
        leafsList = self._getSelectedTreeNodeLeafs (AllFiles = True, IgnoreRoot = True)        
        if len (leafsList) > 0 :
            if len (leafsList) > 0 :                 
                 if MessageBoxUtil.showWarningMessageBox ("Reverting Changes", "Reverting changes will remove all changes made since the file was checked out. Click 'Yes' to revert the selected files.") :
                     FileErrorList = []
                     progdialog = QProgressDialog(" ", "Cancel", int (0), len (leafsList), self)  
                     progdialog.setMinimumDuration (0)
                     progdialog.setWindowFlags(progdialog.windowFlags () | QtCore.Qt.Dialog | QtCore.Qt.WindowStaysOnTopHint)          
                     progdialog.setWindowModality(QtCore.Qt.ApplicationModal)       
                     progdialog.setWindowTitle ("Reverting Files")   
                     try:                        
                        progdialog.forceShow() 
                        progdialog.setValue(0)
                        self._ProjectDataset.getApp ().processEvents ()
                        SuperUserOverride = None
                        print ("")
                        for nodeindex, node in enumerate (leafsList) :
                            progdialog.setValue (nodeindex)
                            self._ProjectDataset.getApp ().processEvents ()
                            try:
                                ROIDataFile = self._ProjectDataset.getROIDatasetInterface ().getROIDataForDatasetPath (node, UserNameStr = self._ProjectDataset.getUserName (), SilentIgnoreOnLockFail = True, SuperUserFroceSilentGrabLock = True)
                                if (ROIDataFile is not None) :
                                    fileReverted = False
                                    roiDataFilePath = ROIDataFile.getPath ()
                                    PLockStatus = ROIDataFile.getPersistentFileLockStatus (IgnoreNonPersistantFileLock = True)
                                    if (ROIDataFile.isFileHandleLocked ()) :       
                                        if PLockStatus in ["OtherUserLocked","UserLocked"] :
                                            if PLockStatus == "UserLocked" :
                                                ROIDataFile.revertPersistentFileLock (SuperUserOverride = False)
                                                ROIDataFile.closeUnsaved ()   
                                                self._reloadChangedWindow (roiDataFilePath)
                                                fileReverted = True
                                                print ("Reverting " + roiDataFilePath)
                                            elif self._ProjectDataset.isInSuperUserCommandLineMode () and PLockStatus == "OtherUserLocked" :
                                                if SuperUserOverride is None :
                                                    SuperUserOverride = MessageBoxUtil.showWarningMessageBox  ("Administrator Override","Another user currently holds the file lock. Do you really wish to revert the file lock?")        
                                                if SuperUserOverride :
                                                    ROIDataFile.revertPersistentFileLock (SuperUserOverride = SuperUserOverride)
                                                    ROIDataFile.closeUnsaved ()   
                                                    self._reloadChangedWindow (roiDataFilePath)
                                                    fileReverted = True                                    
                                    if not fileReverted :
                                        ROIDataFile.closeUnsaved ()   
                                        if PLockStatus != "Unlocked" :
                                            FileErrorList.append (roiDataFilePath)
                                del ROIDataFile
                            except:
                                pass
                            if progdialog.wasCanceled () :
                                break                        
                     finally:                         
                         progdialog.close ()
                         self._ProjectDataset.getApp ().processEvents ()
                     if len (FileErrorList) > 0 :
                        ErrorListDlg.showErrorListDlg (self, "Revert File Warrning" ,"The following files were locked by other users and could not be reverted.", FileErrorList)    
                         
                             
    def tryToCommitFileSelection (self) :
        if self._ProjectDataset.isRunningInDemoMode () :
            return
        IsPesscara =  self._ProjectDataset.getROIDatasetInterface ().isPesscaraInterface ()
        if IsPesscara :
            return        
        leafsList = self._getSelectedTreeNodeLeafs (AllFiles = True, IgnoreRoot = True)
        if len (leafsList) > 0 :
             succeed, Comment = FilePLockUI.showCheckinPLockDialog (self)
             if succeed :
                 FileErrorList = []                 
                 progdialog = QProgressDialog(" ", "Cancel", int (0), len (leafsList), self)  
                 progdialog.setMinimumDuration (0)
                 progdialog.setWindowFlags(progdialog.windowFlags () | QtCore.Qt.Dialog | QtCore.Qt.WindowStaysOnTopHint)          
                 progdialog.setWindowModality(QtCore.Qt.ApplicationModal)       
                 progdialog.setWindowTitle ("Checking in files")   
                 try:                    
                    progdialog.forceShow() 
                    progdialog.setValue(0)
                    self._ProjectDataset.getApp ().processEvents ()                 
                    for nodeindex, node in enumerate (leafsList) :
                        progdialog.setValue (nodeindex)
                        self._ProjectDataset.getApp ().processEvents ()
                        try:
                            ROIDataFile = self._ProjectDataset.getROIDatasetInterface ().getROIDataForDatasetPath (node, UserNameStr = self._ProjectDataset.getUserName (), SilentIgnoreOnLockFail = True)
                            if (ROIDataFile is not None) :
                                LockReleased = False
                                roiFilePath = ROIDataFile.getPath ()
                                if ROIDataFile.isFileHandleLocked () : 
                                    PLockStatus = ROIDataFile.getPersistentFileLockStatus ()
                                    if PLockStatus == "UserLocked" :
                                        ROIDataFile.releasePersistentFileLock (Comment = Comment)
                                        ROIDataFile.closeUnsaved ()   
                                        LockReleased = True
                                        self._reloadChangedWindow (roiFilePath)
                                if not LockReleased :
                                    ROIDataFile.closeUnsaved ()   
                                    FileErrorList.append (roiFilePath)
                            del ROIDataFile
                        except:
                            pass
                        if progdialog.wasCanceled () :
                            break                    
                 finally:                     
                    progdialog.close ()
                    self._ProjectDataset.getApp ().processEvents ()                 
                 if len (FileErrorList) > 0 :
                     ErrorListDlg.showErrorListDlg (self, "File Check-in Warrning" ,"The following files are checked out by other users.", FileErrorList)
        

    def _reloadChangedWindow (self, filepath) :
        try :           
            if filepath is not None :
                for window in self._ProjectDataset.getActiveContourWindowList () :
                     windowFilePath = window.getLoadedFileHandlePath ()
                     if windowFilePath == filepath :
                         window.closeUnsavedAndReload ()    
        except:
            pass
        return
        
    def tryToAssignFileSelection (self) :
        if self._ProjectDataset.isRunningInDemoMode () :
            return
        IsPesscara =  self._ProjectDataset.getROIDatasetInterface ().isPesscaraInterface ()
        if IsPesscara :
            return                
        leafsList = self._getSelectedTreeNodeLeafs (AllFiles = True, IgnoreRoot = True)
        if len (leafsList) < 1 :
            return
        FileErrorList = []
        if len (leafsList) == 1 :
            node = leafsList[0]            
            SuperOverride = None
            ROIDataFile = self._ProjectDataset.getROIDatasetInterface ().getROIDataForDatasetPath (node, UserNameStr = self._ProjectDataset.getUserName (), SilentIgnoreOnLockFail = True, SuperUserFroceSilentGrabLock = True)
            if (ROIDataFile is not None):
                status = ROIDataFile.getPersistentFileLockStatus (IgnoreNonPersistantFileLock = True)
                if status == "OtherUserLocked" and self._ProjectDataset.isInSuperUserCommandLineMode () :
                    SuperOverride = MessageBoxUtil.showWarningMessageBox  ("Administrator Override","Another user currently holds the file lock. Do you really wish to reassign the file lock?") 
                    if SuperOverride :
                        ROIDataFile = self._ProjectDataset.getROIDatasetInterface ().getROIDataForDatasetPath (node, UserNameStr = self._ProjectDataset.getUserName (), SilentIgnoreOnLockFail = True, SuperUserFroceSilentGrabLock = True)
                        if ROIDataFile is not None :
                            ROIDataFile.forceCloseOpenPLocks ()
                    else:
                        FileErrorList.append (ROIDataFile.getPath ())
                        ROIDataFile.closeUnsaved ()
                        ROIDataFile = None
                elif status != "UserLocked" and status != "UserGroupLocked" and not (status == "Unlocked" and self.canAccessUnlockedFiles ()) :
                    FileErrorList.append (ROIDataFile.getPath ())
                    ROIDataFile.closeUnsaved ()
                    ROIDataFile = None
                    
                if (ROIDataFile is not None) :                                   
                    fileAssigned = False
                    roiFilePath = ROIDataFile.getPath ()
                    if (ROIDataFile.isFileHandleLocked ()) :  
                        succeed, results = FilePLockUI.showAssignPLockDlg (self, ROIDataFile, IncludeSelf = True)
                        if succeed :                            
                            AssignToUserName, Comment = results
                            fileShaingManager = self._ProjectDataset.getProjectFileInterface ().getMultiUserFileSharingManager ()
                            if fileShaingManager.correctNickNameIfNecessary (AssignToUserName) == self._ProjectDataset.getUserName () :
                                ROIDataFile.closeUnsaved ()                                                      
                                fileAssigned = True   
                                self.tryToCheckOutFileSelection ()
                            elif ROIDataFile.transferPersistentFileLock (AssignToUserName, Comment = Comment)  :                                
                                ROIDataFile.closeUnsaved ()                        
                                self._reloadChangedWindow (roiFilePath)
                                fileAssigned = True                        
                    if not fileAssigned :                    
                        FileErrorList.append (roiFilePath)
                        ROIDataFile.closeUnsaved ()                        
        else:
            UserFileSharingAssignments = self._ProjectDataset.getProjectFileInterface ().getMultuserFileSharingAssignments (IncludeSelf = True)
            fileShaingManager = self._ProjectDataset.getProjectFileInterface ().getMultiUserFileSharingManager ()
            succeed, results = FilePLockUI.showMultiFileAssignPLockDlg (self, UserFileSharingAssignments, fileShaingManager)
            if succeed :
                assignToUser,assignToPreviousUser, Comment = results                 
                if fileShaingManager.correctNickNameIfNecessary (assignToUser) ==  self._ProjectDataset.getUserName () :             
                    self.tryToCheckOutFileSelection ()
                else:
                    progdialog = QProgressDialog(" ", "Cancel", int (0), len (leafsList), self)  
                    progdialog.setMinimumDuration (0)
                    progdialog.setWindowFlags(progdialog.windowFlags () | QtCore.Qt.Dialog | QtCore.Qt.WindowStaysOnTopHint)          
                    progdialog.setWindowModality(QtCore.Qt.ApplicationModal)       
                    progdialog.setWindowTitle ("Assigning files to " + assignToUser)   
                    try :                  
                        progdialog.forceShow() 
                        progdialog.setValue (0)
                        self._ProjectDataset.getApp ().processEvents ()
                        SuperOverride = None
                        for nodeindex,  node in enumerate (leafsList) :
                            progdialog.setValue (nodeindex)
                            self._ProjectDataset.getApp ().processEvents ()
                            try :
                                ROIDataFile = self._ProjectDataset.getROIDatasetInterface ().getROIDataForDatasetPath (node, UserNameStr = self._ProjectDataset.getUserName (), SilentIgnoreOnLockFail = True, SuperUserFroceSilentGrabLock = True)
                                if (ROIDataFile is not None) :
                                    status = ROIDataFile.getPersistentFileLockStatus (IgnoreNonPersistantFileLock = True) 
                                    if status == "OtherUserLocked" and self._ProjectDataset.isInSuperUserCommandLineMode () :
                                        if SuperOverride is None :
                                             SuperOverride = MessageBoxUtil.showWarningMessageBox  ("Administrator Override","Another user currently holds the file lock. Do you really wish to reassign the file lock?") 
                                        if SuperOverride :
                                                ROIDataFile.forceCloseOpenPLocks ()
                                        else:
                                            FileErrorList.append (ROIDataFile.getPath ())
                                            ROIDataFile.closeUnsaved ()
                                            ROIDataFile = None
                                    elif status != "UserLocked" and status != "UserGroupLocked" and not (status == "Unlocked" and self.canAccessUnlockedFiles ()) :
                                        FileErrorList.append (ROIDataFile.getPath ())
                                        ROIDataFile.closeUnsaved ()
                                        ROIDataFile = None
                                    if (ROIDataFile is not None) :
                                        fileAssigned = False
                                        roiFilePath = ROIDataFile.getPath ()
                                        if ROIDataFile.isFileHandleLocked () :  
                                            if assignToPreviousUser : 
                                                assignToUser = ROIDataFile.getPreviousPlockOwner ()
                                            if ROIDataFile.transferPersistentFileLock (assignToUser, Comment = Comment)  :
                                                ROIDataFile.closeUnsaved ()
                                                self._reloadChangedWindow (roiFilePath)
                                                fileAssigned = True
                                        if not fileAssigned :                                    
                                            FileErrorList.append (roiFilePath)
                                            ROIDataFile.closeUnsaved ()
                            except:
                                pass
                            if progdialog.wasCanceled () :
                                break
                    finally:                    
                        progdialog.close ()
                        self._ProjectDataset.getApp ().processEvents ()
        if len (FileErrorList) > 0 :
            ErrorListDlg.showErrorListDlg (self, "File Checkout Warrning" ,"The following files are checked out by other users.", FileErrorList)
        
                
        
    def setTreeViewSelectedDataset (self, selectedNode) :
        treeSelectionModel = self.ui.DatasetTreeView.selectionModel ()
        if treeSelectionModel is None :
            return
        
        if (selectedNode is None) :
            if len (treeSelectionModel.selectedIndexes()) > 0 :
                try :
                    treeSelectionModel.selectionChanged.disconnect (self._DatasetSelectionChanged)             
                    Reconnect = True
                except:
                    Reconnect = False        
                #treeSelectionModel.clearSelection()
                self._selectedTreePath = None
                if Reconnect :
                    treeSelectionModel.selectionChanged.connect (self._DatasetSelectionChanged)             
        else:
            currentselectionlist  = set(treeSelectionModel.selectedIndexes())
            currentItemSelected = False        
            for selection in currentselectionlist :
                if selectedNode.getTreePath () == selection.internalPointer ().getTreePath () :
                    currentItemSelected = True
                    break                
            if not currentItemSelected :
                #while len (selectedNode.getChildernLst()) > 0 :
                #    selectedNode = selectedNode.getChildernLst()[0]      
                try :
                    treeSelectionModel.selectionChanged.disconnect (self._DatasetSelectionChanged)             
                    Reconnect = True
                except:
                    Reconnect = False        
                treeModel = self.ui.DatasetTreeView.model ()
                selNode = treeModel.createIndex(selectedNode.getRow (), 0, selectedNode)                
                treeSelectionModel.select (selNode, QtCore.QItemSelectionModel.SelectCurrent | QtCore.QItemSelectionModel.Rows)
                if Reconnect :
                    treeSelectionModel.selectionChanged.connect (self._DatasetSelectionChanged)             
        
    # Import ROI From Mask    
    MP_CreateROIFromFileMask = staticmethod (MP_CreateROIFromFileMask)    
    
    def showAboutDlg (self, Parent) :
        RCC_AboutBoxDlg (self._ProjectDataset, Parent)
    
    def showAboutSelfDlg (self) :
            RCC_AboutBoxDlg (self._ProjectDataset, self)


    def showHelpDlg (self, Parent) :
        RCC_HelpBoxDlg (self._ProjectDataset, Parent)
    
    def showHelpSelfDlg (self) :
            RCC_HelpBoxDlg (self._ProjectDataset, self)

    
         
    def importROIFromMask (self) :   #code works for FS data sources, Tactic DS will take some addtional work.                         
        import psutil
        FileErrorList = []
        ROIDefs = ROIDefinitions(self._ProjectDataset.getDocumentUIDFileLogInterface ())        
        progdialog = None        
        try:            
            ROIDefs.loadROIDefsFromProjectFile ()             
            
            SelectedNodeSubset = None
            if self.areSubsetOfDatasetSelected () :
                SelectedNodeSubset = self.ui.DatasetTreeView.selectionModel().selectedRows ()
            importMaskDialog = RCC_ROIMaskImportDlg (self._ProjectDataset, ROIDefs, self, importSingleMask = False, SelectedNodeSubset = SelectedNodeSubset)
            if (importMaskDialog.exec_()) :
                tup = importMaskDialog.getReturnValue ()   
                if tup is not None :
                    NiftiMappingToMaskFileList, roiMaskMappings, importMethod, doNotOverwriteROI, MaskedDataPerefered, ExistingDataPrefered  = tup                            
                    if (len (NiftiMappingToMaskFileList) > 0) :
                        method = importMethod # "OrMask" # "AndMask"
                        
                        progdialog = QProgressDialog(" ", "Cancel", int (0), len (NiftiMappingToMaskFileList), self)  
                        progdialog.setMinimumDuration (0)
                        progdialog.setWindowFlags(progdialog.windowFlags () | QtCore.Qt.Dialog | QtCore.Qt.WindowStaysOnTopHint)          
                        progdialog.setWindowModality(QtCore.Qt.ApplicationModal)       
                        progdialog.setWindowTitle ("Importing ROI Masks")                           
                        try :
                            progdialog.forceShow()  
                            progdialog.setValue (0)
                            self._ProjectDataset.getApp ().processEvents ()                        
                            if (16000000000 > psutil.virtual_memory()[1]):
                                MaxProc = 1
                            else:
                                MaxProc = 2                            
                            PoolSize = PlatformMP.getAvailableCPU ()
                            PoolSize = min (PoolSize, MaxProc)
                             
                            NiftiDatasetTreeNodeList = []
                            progdialog.setValue (0)                        
                        
                            IsPesscara =  self._ProjectDataset.getROIDatasetInterface ().isPesscaraInterface ()
                            if (IsPesscara) :
                                resultList = []
                            else:
                                procQueue = PlatformMP.processQueue (RCC_DatasetDlg.MP_CreateROIFromFileMask, None, PoolSize = PoolSize, SleepTime = 10, ProjectDataset = self._ProjectDataset, Debug = False, ProgressDialog = progdialog)
                            progdialog.setMaximum (len (NiftiMappingToMaskFileList) * 100)
                            canAccessUnlockedFiles = self.canAccessUnlockedFiles ()
                            for fIndex in range (len (NiftiMappingToMaskFileList)) :
                                    if progdialog.wasCanceled () :
                                        break
                                    NiftiMappingToMaskFileMapping = NiftiMappingToMaskFileList[fIndex]    
                            
                                    niftiDataSetTreeNode, maskFilePath = NiftiMappingToMaskFileMapping
                                    dirname, fname = maskFilePath 
                                    maskFilePath  = os.path.join (dirname,fname)                                
                                    
                                    niftiFilePath = "Unknown"
                                    roiFilePath = "Unknown"                                    
                                    try :                                
                                        niftiDataFile = niftiDataSetTreeNode.getNIfTIDatasetFile ()                                        
                                        ROIDataFile = self._ProjectDataset.getROIDatasetInterface ().getROIDataForDatasetPath (niftiDataSetTreeNode, UserNameStr = self._ProjectDataset.getUserName (), SilentIgnoreOnLockFail = False, SuperUserFroceSilentGrabLock = True)
                                        if (ROIDataFile is not None) :       
                                             if not ROIDataFile.isReadOnly  () and ROIDataFile.isFileHandleLocked () :
                                           
                                                 roiFilePath = ROIDataFile.getPath ()
                                                                                                                                               
                                                 ExcludeExistingMaskedAreaFromImport = ExistingDataPrefered
                                                 RemoveExistingMaskedAreaFromOverlapping = MaskedDataPerefered
                                                 NiftiDataSourceDescription = niftiDataSetTreeNode.getSaveFileNifitFileSourceFileObject ()                                                      
                                                 NiftiDatasetTreeNodeList.append (niftiDataSetTreeNode)               
                                                 BaseProgressIndex = fIndex * 100
                                                 NextProgressIndex = BaseProgressIndex + 100
                                                 if (IsPesscara) :                                                     
                                                     processParameters = (ROIDefs.copy (), ROIDataFile,  niftiDataFile, NiftiDataSourceDescription, roiMaskMappings, maskFilePath, method, doNotOverwriteROI, ExcludeExistingMaskedAreaFromImport, RemoveExistingMaskedAreaFromOverlapping,self._ProjectDataset.getDocumentUIDFileLogInterface (), canAccessUnlockedFiles, progdialog, self._ProjectDataset , (BaseProgressIndex, NextProgressIndex))    
                                                     resultList.append (RCC_DatasetDlg.MP_CreateROIFromFileMask (processParameters))                                                     
                                                 else:     
                                                     if procQueue.isSerial () :
                                                         progressDialog = progdialog 
                                                         prjDataset = self._ProjectDataset 
                                                         mpProgressDialogInterface = None
                                                     else:                                                         
                                                         progressDialog = procQueue.getMPProgressDialogInterface ()
                                                         prjDataset = None
                                                         mpProgressDialogInterface = progressDialog
                                                     
                                                     processParameters = (ROIDefs.copy (), ROIDataFile,  niftiDataFile, NiftiDataSourceDescription, roiMaskMappings, maskFilePath, method, doNotOverwriteROI, ExcludeExistingMaskedAreaFromImport, RemoveExistingMaskedAreaFromOverlapping,self._ProjectDataset.getDocumentUIDFileLogInterface (), canAccessUnlockedFiles, progressDialog, prjDataset, (BaseProgressIndex, NextProgressIndex))                                                     
                                                     cleanupParameters = None
                                                     procQueue.queueJob (self._ProjectDataset.getROISaveThread (), processParameters, cleanupParameters, MPProgressDialogInterface = mpProgressDialogInterface)                                                     
                                                 ROIDataFile.close ()                                                                         
                                                 progdialog.setLabelText (niftiDataFile.getFilePath ())
                                                 if (IsPesscara or procQueue.isSerial ()) :        
                                                     progdialog.setValue (NextProgressIndex)
                                                 self.update ()
                                                 self._ProjectDataset.getApp ().processEvents ()
                                                 if progdialog.wasCanceled() :
                                                     break
                                             else:
                                                 ROIDataFile.close ()                                                                         
                                    except  :
                                            print ("A Error occured spawning import ROI from Mask Process for: %s for DS: %s and ROI: %s" % (maskFilePath, niftiFilePath, roiFilePath))                                    
                            if (not IsPesscara) :
                                resultList = procQueue.join ()
                                del procQueue
                            for resultIndex, resultTpl in enumerate(resultList) :
                                couldProcessData, HasData, Path = resultTpl 
                                if couldProcessData :
                                    niftiDataSetTreeNode = NiftiDatasetTreeNodeList[resultIndex]
                                    niftiDataSetTreeNode.setROIDatasetIndicator (HasData)      
                                else:
                                    FileErrorList.append (Path)
                        finally:                                
                            progdialog.close ()
                            self._ProjectDataset.getApp ().processEvents ()                        
        except  :
            print ("Could not load projects ROI definitions")            
        if (progdialog is not None) :
            try :
                progdialog.close ()
                progdialog = None
            except:                
                progdialog = None
        if len (FileErrorList) > 0 :
            ErrorListDlg.showErrorListDlg (self, "Import File Warrning" ,"The following files were locked by other users. ROI masks were not imported for them.", FileErrorList)
        treeModel = self.ui.DatasetTreeView.model ()
        if treeModel is not None :
            treeModel.rebuildTree ()
        self.ui.DatasetTreeView.update ()
        #Reload DS to update open windows  also  Needed to update UI to show ROIDefs which may be loaded by opening ROIdictionarys
        for window in self._ProjectDataset.getActiveContourWindowList () :
            window.closeUnsavedAndReload ()    
                
    
    def canImportROIFromMask (self) :
        canimportROIFromMaskReturn = False                               
        if (self.isLoadingROIFromFileSystem ()) : 
            canimportROIFromMaskReturn = True                               
        self.ui.actionROI_from_NIfTI_mask.setEnabled (canimportROIFromMaskReturn)
        return canimportROIFromMaskReturn
    
    def canImportROIMask (self) :
        return self.canImportROIFromMask ()



    def SaveTreeViewColumnWidths (self, index, oldsize, newsize):               
        interface = self._ProjectDataset.getNIfTIDatasetInterface ()
        if interface is not None :
            headerData = interface.getUIHeader ()
            count = headerData.headerVisibleObjectCount ()
            for hindex in range (count) :
                headersize = self.ui.DatasetTreeView.header().sectionSize (hindex)
                headerObject = headerData.getVisibleHeaderByIndex (hindex)
                headerObject.setHeaderSectionSize (headersize)
            self._saveHeaderWidthsForResize ()
            self._adjustTreeViewHeaderWidth ()
            self._ProjectDataset.setUIHeaderChanged ()
    
               
    MP_ExportDatasetTags = staticmethod (MP_ExportDatasetTags)       
        
    def exportProjectTagsDlg (self) :
        aw = self._ProjectDataset.getActiveWindow ()
        tagManager = None
        if (aw in self._ProjectDataset.getActiveContourWindowList ()):
            tagManager = aw.getWindowTagManger ()
        if (tagManager == None or len (tagManager.getTags()) == 0) :
            tagManager = self._ProjectDataset.getProjectTagManger ()              
        ShowRegistrationColumnTagIndicator = False # self.isLoadingROIFromFileSystem ()
                 
        exportTagsDlg = RCC_ExportTagsDlg (self, self._ProjectDataset.getNIfTIDatasetInterface ().getUIHeader(), tagManager, self._ProjectDataset, ShowRegistrationTag = ShowRegistrationColumnTagIndicator, LimitToSeriesWithTagsEnabled = True)
        if (QDialog.Accepted == exportTagsDlg.exec_ ()) :            
            exportTagList = exportTagsDlg.getExportTagList ()
            LimitToSeriesWithData = exportTagsDlg.getLimitToSeriesWithData ()
            if (len (exportTagList) > 0) :
                dlg= QFileDialog( None )
                dlg.setWindowTitle( 'Select CSV file to save project tags to' )
                dlg.setViewMode( QFileDialog.Detail )    
                dlg.setFileMode (QFileDialog.AnyFile)
                dlg.setNameFilters( ['csv file (*.csv)', 'excel file (*.xlsx)', 'any file (*)'] )                
                dlg.setAcceptMode (QFileDialog.AcceptSave)
                if dlg.exec_() and (len (dlg.selectedFiles())== 1):
                    exportPath = dlg.selectedFiles()[0]                                                    
                    if ("xlsx" in dlg.selectedNameFilter ()) :
                        if not exportPath.lower ().endswith (".xlsx") :
                            exportPath += ".xlsx"
                    elif not exportPath.lower ().endswith (".csv") :
                        exportPath += ".csv"
                                
                    NIfTITreeNodeList = self._getSelectedNiftiDatasetListForExport (AllFiles = not LimitToSeriesWithData) # returns only niftiTreeNodes defining data, by design setting parameter AllFiles = True to would enable the export of tags from the whole dataset, however this could be very slow and more over result in difficult to interpret output as many nodes would refer to datasets with no tags.
                    progdialog = QProgressDialog("Exporting Tag CSV File", "Cancel", int (0), int (len (NIfTITreeNodeList)), self)   
                    progdialog.setMinimumDuration (0)
                    progdialog.setWindowFlags(progdialog.windowFlags () | QtCore.Qt.Dialog  | QtCore.Qt.WindowStaysOnTopHint)                         
                    progdialog.setWindowTitle ("Exporting Tag CSV File")
                    progdialog.setWindowModality(QtCore.Qt.ApplicationModal)                            
                    try :
                        progdialog.forceShow()    
                        progdialog.setValue(0)
                        self._ProjectDataset.getApp ().processEvents ()
                        cpucount = PlatformMP.getAvailableCPU ()
                        
                        ProjectROIDefs = ROIDefinitions(self._ProjectDataset.getDocumentUIDFileLogInterface ())   
                        try:            
                            ProjectROIDefs.loadROIDefsFromProjectFile ( Verbose = False)             
                        except :
                            ProjectROIDefs = ROIDefinitions(self._ProjectDataset.getDocumentUIDFileLogInterface ())    
                
                        
                        progdialog.setMaximum (len (NIfTITreeNodeList))
                        
                        IsPesscara =  self._ProjectDataset.getROIDatasetInterface ().isPesscaraInterface ()
                        if IsPesscara :
                            DatasetList = []
                        else:
                           procQueue = PlatformMP.processQueue (RCC_DatasetDlg.MP_ExportDatasetTags, None, PoolSize = cpucount, SleepTime = 10, ProjectDataset = self._ProjectDataset)
                           progdialog.setMaximum (len (NIfTITreeNodeList) + 1)
                           
                        for fIndex, NIfTITreeNode in enumerate(NIfTITreeNodeList) :                      
                            ROIDataFile = self._ProjectDataset.getROIDatasetInterface().getROIDataForDatasetPath (NIfTITreeNode, IgnoreLock = True) 
                            if (ROIDataFile is not None) :                        
                                FileSystemPath = NIfTITreeNode.getTreePath()
                                try :
                                    LongFileNamePath = NIfTITreeNode.getTreePath(ReturnFileSystemPath = False)
                                except :
                                    LongFileNamePath = FileSystemPath
                                    
                                processParameters = (ROIDataFile, ProjectROIDefs, exportTagList, FileSystemPath, LongFileNamePath, self._ProjectDataset.getDocumentUIDFileLogInterface ())
                                cleanupParameters = None
                                if (IsPesscara) :
                                    DatasetList.append (RCC_DatasetDlg.MP_ExportDatasetTags (processParameters))
                                else:                                                                        
                                    procQueue.queueJob (self._ProjectDataset.getROISaveThread (), processParameters, cleanupParameters)                                    
                                ROIDataFile.closeUnsaved ()                        
                                self._ProjectDataset.getApp ().processEvents ()
                                progdialog.setLabelText (NIfTITreeNode.getText ())
                                progdialog.setValue (fIndex)
                                if progdialog.wasCanceled() :
                                    break
                        if not IsPesscara :
                            DatasetList = procQueue.join () 
                            progdialog.setValue (len (NIfTITreeNodeList) + 1)
                        self._ProjectDataset.reloadWindowROIDefs () # Needed to update UI to show ROIDefs which may be loaded by opening ROIdictionarys
                        exportTable = PandasCompatibleTable ()
                        exportTable.insertDataVector (0,0, ["Generated", DateUtil.dateToString (DateUtil.today()), TimeUtil.timeToString (TimeUtil.getTimeNow ())  ])                        
                        headerVec = ["File System Source", "Long File Name Source"]                        
                        for tag in exportTagList :
                            try:
                                tagName = tag.getName ()
                            except:
                                tagName = "Unknown"
                            headerVec.append (tagName)                            
                        exportTable.insertDataVector (2,0,headerVec)
                        for row, datavector in enumerate(DatasetList) :
                            exportTable.insertDataVector (row+3, 0, datavector)
                        if (exportTable.getRowCount () > 0) :
                            pd = exportTable.convertTableToPandasDataFrame()
                            if (exportPath.lower().endswith (".xlsx")):
                                pd.to_excel (exportPath,index=False,header=False)
                            else:
                                pd.to_csv (exportPath,index=False,header=False)                            
                    finally:
                        progdialog.close ()
                        self._ProjectDataset.getApp ().processEvents ()
                      
                
            
    def setPesscaraTemporaryStorageDirDlg (self):
        if (not self.isLoadingROIFromFileSystem ()) :            
            dlg = RCC_SetPesscaraTemoraryDataStorageDlg ( self._ProjectDataset.getProjectTempStorageDir(), self)
            if (QDialog.Accepted == dlg.exec_ ()) :
                self._ProjectDataset.setProjectTempStorageDir(dlg.getPath ())                
                
        
    def isLoadingROIFromFileSystem (self) :
        return (self._ProjectDataset is not None and self._ProjectDataset.getROIDatasetInterface() is not None and not self._ProjectDataset.getROIDatasetInterface().isPesscaraInterface ())
    
    def showDatasetColumnChooser (self) :
        
        aw = self._ProjectDataset.getActiveWindow ()
        tagManager = None
        if (aw in self._ProjectDataset.getActiveContourWindowList ()):
            tagManager = aw.getWindowTagManger ()
        if (tagManager == None or len (tagManager.getTags()) == 0) :
            tagManager = self._ProjectDataset.getProjectTagManger ()
              
        ShowRegistrationColumnTagIndicator = self.isLoadingROIFromFileSystem ()
            
        columnChooser = RCC_DatasetTagingDlg (self._ProjectDataset.getNIfTIDatasetInterface ().getUIHeader(), tagManager,self._ProjectDataset, self, ShowRegistrationTag = ShowRegistrationColumnTagIndicator)
        if (QDialog.Accepted == columnChooser.exec_ ()) :
             newDataUISetHeaderDefinitions = columnChooser.getDatasetUIHeader ()                        
             self._ProjectDataset.getNIfTIDatasetInterface ().setUIHeader (newDataUISetHeaderDefinitions)                          

             colWidth = int (float (self.width ()) * 0.50)
             self.ui.DatasetTreeView.setColumnWidth (0, colWidth)                
             objectCount = newDataUISetHeaderDefinitions.headerVisibleObjectCount () -1
             if (objectCount > 0) :
                 colWidth= math.floor (float (colWidth) / float (objectCount))
                 for index in range (1, objectCount) :
                     self.ui.DatasetTreeView.setColumnWidth (index, colWidth)                
             
             #Save Section Sizes and save changes to the project
             self.SaveTreeViewColumnWidths (-1,-1,-1) #pramaters not used

            
            
    
    def showProjectTagEditor (self) :
         projectEditorDlg = RCC_ProjectTagEditorDlg (self._ProjectDataset, self._ProjectDataset.getProjectTagManger ().copy (), self, self._ProjectDataset.getScanPhaseList (), self._ProjectDataset.getDescriptionContext ())                        
         if (QDialog.Accepted == projectEditorDlg.exec_ ()) :
             newProjectTagDefinitions, projectDescriptions, projectDescriptionContext = projectEditorDlg.getTagDataSetManager ()                                
             if (self._ProjectDataset.setProjectDescriptionContext (projectDescriptionContext)) :
                 MessageBoxUtil.showMessage ("Information", "Please restart rilcontour to reflect the change in the project's description context.")
             self._ProjectDataset.setScanPhaseList (projectDescriptions)
             self._ProjectDataset.setProjectTagManger (newProjectTagDefinitions)
                          
             
    def showRegistrationUIFilterDlg (self):                        
        if (self.isLoadingROIFromFileSystem ()) :
            try:
                dlg = RCC_RegistrationIndicatorFilterDlg (self,  self._ProjectDataset.getNIfTIDatasetInterface ().getFirstRegistrationDate (), self._ProjectDataset.getNIfTIDatasetInterface ().getRegistrationDayFilter ())
                dlg.exec_ ()
                monthDayYear = dlg.getRegistrationDayFilter ()
                self._ProjectDataset.getNIfTIDatasetInterface ().setRegistrationDayFilter (monthDayYear)
            except :
                self._ProjectDataset.getNIfTIDatasetInterface ().setRegistrationDayFilter ((None, None, None))
            self.update ()
        
    def showSetUserName (self) :
        dlg = RCC_UserNameEditorDlg (self, self._ProjectDataset.getUserName (ShowWindow = False))
        dlg.exec_ ()
        userName = dlg.getUserName ()
        dlg = 0
        self._ProjectDataset.setUserName (userName)
        
    def openProject (self) :
        dlg= QFileDialog( None )
        dlg.setWindowTitle('Open project:' )
        dlg.setViewMode( QFileDialog.Detail )    
        dlg.setFileMode (QFileDialog.AnyFile)
        dlg.setNameFilters( [self.tr('Project (*.prj)'), self.tr('All Files (*)')] )
        dlg.setDefaultSuffix( '.prj' )
        dlg.setAcceptMode (QFileDialog.AcceptOpen)
        if dlg.exec_()  and (len (dlg.selectedFiles())== 1):
            projectPath = dlg.selectedFiles()[0]                  
            self.openProjectFile (projectPath)            
        
        
    def newProjectWizard (self) :
        wizard = RCC_NewProjectWizardDlg (self)
        wizard.exec_ ()        
        if (wizard._result == "CreateProject") :        
            self._newproject = self._ProjectDataset.createProjectFromWizard (wizard)                    
        
            
    def CreateNewViewingWindow (self) :
        if (self._ProjectDataset.getScanPhaseSelectionDlg () != None) :
            MessageBoxUtil.showMessage ("Can not open contouring window", "The scan phase selectin window must be closed top open contouring windows.")
            return
        
        print ("Create New Window")
        item = self.ui.DatasetTreeView.selectionModel().currentIndex() 
        niftidataset = item.internalPointer ()                       
        for window in self._ProjectDataset.getActiveContourWindowList () :
            if (window.getSelectedNIfTIDataset () is not None and window.getSelectedNIfTIDataset () == niftidataset):
                niftidataset = None
                break                
        self._ProjectDataset.createContouringWindow ()       
        if (niftidataset is not None) :
            activeContourWindow = self._ProjectDataset.getActiveWindow()
            activeContourWindow.DatasetTreeSelectionChanged (niftidataset)
    
    def _saveHeaderWidthsForResize (self) :
        headerData = self.ui.DatasetTreeView.header()
        sectionWidthList = []
        for headerindex in range (headerData.count ()) :
            sectionWidthList.append (headerData.sectionSize (headerindex)) 
        self._PreferedHeaderWidth = sectionWidthList
    
    def _adjustTreeViewHeaderWidth (self) :
        try :
            self.ui.DatasetTreeView.header().sectionResized.disconnect (self.SaveTreeViewColumnWidths)  
            reconnectSectionResizeListener = True
        except:
            reconnectSectionResizeListener = False
        
        width = self.width () - 20
        headerData = self.ui.DatasetTreeView.header()
        totalSizeAccumulator = 0
        for headerindex in range (headerData.count ()) :
            totalSizeAccumulator += headerData.sectionSize (headerindex)
        if totalSizeAccumulator < width :
             index = len (self._PreferedHeaderWidth) - 1
             oldWidth = self.ui.DatasetTreeView.columnWidth (index)
             self.ui.DatasetTreeView.setColumnWidth (index,oldWidth + width - totalSizeAccumulator) 
            
        elif (totalSizeAccumulator > width):
            totalSizeAccumulator = np.sum (self._PreferedHeaderWidth)
            if totalSizeAccumulator <= width :
               for index in range (len (self._PreferedHeaderWidth)-1) :
                   self.ui.DatasetTreeView.setColumnWidth (index, self._PreferedHeaderWidth[index])
               index = len (self._PreferedHeaderWidth) - 1
               self.ui.DatasetTreeView.setColumnWidth (index, self._PreferedHeaderWidth[index] + width - totalSizeAccumulator)
            else: 
                sectionWidthList = copy.copy (self._PreferedHeaderWidth)
                headerindex = headerData.count () - 1
                while (headerindex >= 0 and totalSizeAccumulator > width) :
                   sectionWidth = sectionWidthList[headerindex] 
                   smallestDim = min (30, sectionWidth)
                   if totalSizeAccumulator - (sectionWidth - smallestDim) > width :
                       sectionWidthList[headerindex] = smallestDim
                       totalSizeAccumulator -= (sectionWidth - smallestDim)
                   else:
                       sectionWidthList[headerindex] = sectionWidth - (totalSizeAccumulator - width )
                       totalSizeAccumulator = width
                       break
                   headerindex -= 1
                for index in range (headerindex,  headerData.count ()) :
                    self.ui.DatasetTreeView.setColumnWidth (index, sectionWidthList[index])
        if (reconnectSectionResizeListener) :
            self.ui.DatasetTreeView.header().sectionResized.connect (self.SaveTreeViewColumnWidths)     
        
    def _internalWindowResize (self):                            
        try :
            if (self._resizeWidgetHelper is not None) :    
                try :
                    self.ui.DatasetTreeView.header().sectionResized.disconnect (self.SaveTreeViewColumnWidths)  
                    reconnectSectionResizeListener = True
                except:
                    reconnectSectionResizeListener = False
                self._resizeWidgetHelper.setWidth  (self.ui.DatasetTreeView, self.width ())   
                if self._ProjectDataset.getNIfTIDatasetInterface ().getInterfaceName () != "RCC_NiftiPesscaraDataInterface" :
                    self._resizeWidgetHelper.setHeight  (self.ui.DatasetTreeView, self.height () - 100)
                    self._resizeWidgetHelper.setWidth  (self.ui.FilterText, self.width () - 152)
                    self._resizeWidgetHelper.setWidgetYPos  (self.ui.FilterText, 7)                    
                    self._resizeWidgetHelper.setWidgetYPos  (self.ui.DatasetTreeView, 37)      
                    self._resizeWidgetHelper.setWidgetXPos  (self.ui.FilterOptionsBtn, self.width () - 93)                    
                    self._resizeWidgetHelper.setWidgetYPos  (self.ui.FilterOptionsBtn, 7)                    
                else:
                    self._resizeWidgetHelper.setHeight  (self.ui.DatasetTreeView, self.height () - 60)                                                              
                    self._resizeWidgetHelper.setWidgetYPos  (self.ui.DatasetTreeView, 0)
                self._adjustTreeViewHeaderWidth ()
                if (reconnectSectionResizeListener) :
                    self.ui.DatasetTreeView.header().sectionResized.connect (self.SaveTreeViewColumnWidths)     
        except:
            pass
               
            
    def resizeEvent (self, qresizeEvent) :                       
        QMainWindow.resizeEvent (self, qresizeEvent)         
        self._internalWindowResize (  )              
        
    def closeApp (self) :
        self.close ()
    
    def changeEvent (self, event) :        
        if (self._ProjectDataset.isApplicationActive ()) :
            #print (("Event Type", event.type ()))
            self._ProjectDataset.setProjectWindowFocus (self.isActiveWindow() or self.hasFocus ())
        return QMainWindow.changeEvent (self, event)            
    
                
    def areSubsetOfDatasetSelected (self) :
        return (len (self.ui.DatasetTreeView.selectionModel().selectedRows ()) > 1)
    
        
    
    def _getSelectedNiftiDatasetListForExport (self, AllFiles = False) :
        if not self.areSubsetOfDatasetSelected () :
            return self._ProjectDataset.getNIfTIDatasetInterface ().getNiftiTreeNodeList (AllFiles = AllFiles)
        else:
            SeedNodeList = self.ui.DatasetTreeView.selectionModel().selectedRows () 
            nodePtrLst = []
            for node in SeedNodeList :
                nodePtrLst.append (node.internalPointer ())
            return self._ProjectDataset.getNIfTIDatasetInterface ().getNiftiTreeNodeList (Memory = nodePtrLst)
        
           
    def _DatasetSelectionChanged (self):           
        selectionLst = self.ui.DatasetTreeView.selectionModel().selectedRows ()  
        selectionCount = len (selectionLst)
        if len (selectionLst) <= 1 :
            if selectionCount == 0 :
                niftidataset = None
            else:
                niftidataset = selectionLst[0].internalPointer ()
                test = niftidataset
                while (test.getChildCount () == 1) :
                    test = test.getChild (0)
                if (test.getChildCount () == 0) :
                    niftidataset = test
        
            activeWindow = self._ProjectDataset.getActiveWindow()        
            if (activeWindow is not None) :            
                if (activeWindow ==  self._ProjectDataset.getScanPhaseSelectionDlg ()) :
                     self._ProjectDataset.getScanPhaseSelectionDlg ().initWindow (niftidataset)
                else:    
                    foundWindow = None
                    """if niftidataset is not None :
                        selectedTreePath = niftidataset.getTreePath ()
                        for window in self._ProjectDataset.getActiveContourWindowList () :                    
                            if (window.getSelectedNIfTIDataset () is not None):
                                loadedWindowTreePath = window.getSelectedNIfTIDataset ().getTreePath ()
                                if loadedWindowTreePath.startswith (selectedTreePath):                            
                                    foundWindow = window
                                    break                        """
                    if foundWindow is None : 
                        activeWindow.DatasetTreeSelectionChanged (niftidataset)
                    """else:
                        foundWindow.raise_ ()
                        foundWindow.activateWindow ()
                        #print ("Dataset allready shown in another window show dataset twice")"""
            self._ProjectDataset.updateActiveWindowDatasetSelection ()

       
    # application close event, called when application quits.  detaches event handlers from the App.
    def closeEvent(self,event):             
        self._ProjectDataset.closeProject ()
        QMainWindow.closeEvent (self, event)         
     
    def ROIEditorMenubarAction (self) :       
             ROIDefs = ROIDefinitions(self._ProjectDataset.getDocumentUIDFileLogInterface ())        
             try:            
                 ROIDefs.loadROIDefsFromProjectFile ()             
             except :
                 ROIDefs = ROIDefinitions(self._ProjectDataset.getDocumentUIDFileLogInterface ())       
             dlg = RCC_ProjectROIEditorDlg (self, ROIDefs, self._ProjectDataset)                         
             if (QDialog.Accepted == dlg.exec_ ()):          
                 ROIDefs = dlg.ReturnValue
                 self._ProjectDataset.saveROIDefToProject (ROIDefs)                 
             dlg = 0     
    
    
              
    def exportPesscaraDatasetToFileSystem (self) :                
        dlg= QFileDialog( self )
        dlg.setWindowTitle( 'Select directory to export tactic dataset to' )
        dlg.setViewMode( QFileDialog.Detail )    
        dlg.setFileMode (QFileDialog.Directory)
        dlg.setNameFilters( [self.tr('All Files (*)')] )
        dlg.setDefaultSuffix( '' )
        dlg.setAcceptMode (QFileDialog.AcceptOpen)
        if dlg.exec_() and (len (dlg.selectedFiles())== 1):
            exportPath = dlg.selectedFiles()[0]
            
            niftibasePath = os.path.join (exportPath,"NIfTI")
            rootRoibasePath = os.path.join (exportPath,"ROI")
            if os.path.exists (niftibasePath) or os.path.exists (rootRoibasePath) :
               if not MessageBoxUtil.showWarningMessageBox ("Export folder not empty", "The dataset export folder contains NIfTI and/or ROI data folders. Data within these folders may be overwitten do you wish to continue?") :
                   print ("Dataset export aborted by user.")
                   return
            try :
                if not os.path.exists (niftibasePath) :
                    os.mkdir (niftibasePath)
            except:
                print ("Error could not create directory: " + niftibasePath)
                print ("Check file system privlages")
                return
            try :
                if not os.path.exists (rootRoibasePath) :
                    os.mkdir (rootRoibasePath)
            except:
                print ("Error could not create directory: " + rootRoibasePath)
                print ("Check file system privlages")
                return
            if not os.path.isdir (niftibasePath) :
                print ("Error could find directory: " + niftibasePath)
                return
            if not os.path.isdir (rootRoibasePath) :
                print ("Error could find directory: " + rootRoibasePath)
                return
            
            try:
                roibasePath = os.path.join (rootRoibasePath,"NIfTI")
                if not os.path.exists (roibasePath) :
                    os.mkdir (roibasePath)
            except:
                print ("Error could not create directory: " + roibasePath)
                print ("Check file system privlages")
                return
            if not os.path.isdir (roibasePath) :
                print ("Error could find directory: " + roibasePath)
                return
            
            _, projName = os.path.split (self._ProjectDataset.getPath ())
            saveAsPath = os.path.join (exportPath, projName)
            if saveAsPath == self._ProjectDataset.getPath () :
                saveAsPath = os.path.join (exportPath, "Exported_" + projName)
            self._ProjectDataset.saveProject (ForceSave = True, SaveAsPath = saveAsPath, ROIFSPath = rootRoibasePath, NIfTIFSPath = niftibasePath) 
            
            progdialog = QProgressDialog("Exporting Tactic Dataset to File System", "Cancel", int (0), int (1), self)
            progdialog.setMinimumDuration (0)
            progdialog.setWindowFlags(progdialog.windowFlags () | QtCore.Qt.Dialog  | QtCore.Qt.WindowStaysOnTopHint)                
            progdialog.setWindowTitle ("Exporting Tactic Dataset to File System")
            progdialog.setLabelText("")
            progdialog.setWindowModality(QtCore.Qt.ApplicationModal)        
            progdialog.forceShow()                   
            progdialog.setValue (0)            
            app = self._ProjectDataset.getApp ()
            app.processEvents ()
            print ("Start: Getting dataset list.")
            progdialog.setMaximum (0)
            NIfTITreeNodeList = self._getSelectedNiftiDatasetListForExport ()
            progdialog.setMaximum (int (len (NIfTITreeNodeList)))
            print ("Done: Getting dataset list.")
            for nodeIndex, node in enumerate (NIfTITreeNodeList) :
                 progdialog.setValue (nodeIndex)
                 try :
                     ROIDataFile = self._ProjectDataset.getROIDatasetInterface().getROIDataForDatasetPath (node, IgnoreLock = True)
                     if not ROIDataFile.getPath ().endswith ("_ROI.txt") :
                         print ("Unrecongized ROI data file type: " + ROIDataFile.getPath ())
                         continue
                     app.processEvents ()
                     niftiDataFile = node.getNIfTIDatasetFile ()   
                     
                     app.processEvents ()
                     treepath = node.getTreePath ()
                     print ("Exporting TreePath:" + treepath)
                     #print ("NiftiBasePath " + niftibasePath)
                     #print ("ROIBasePath " + roibasePath)
                     #print ("Exporting: TreePath " + treepath)
                     progdialog.setLabelText("Exporting: " + treepath)
                     exportNiftiPath = os.path.join (niftibasePath, treepath)
                     exportROIPath = os.path.join (roibasePath, treepath)
                     
                     _ ,extention = FileUtil.removeExtension (niftiDataFile.getFilePath (),[".nii.gz", ".nii"])
                     if extention is None :
                         print ("Unrecongized NIfTI data file type: " + niftiDataFile.getFilePath ())
                         continue
                     else:
                         if not exportNiftiPath.lower().endswith (extention) : 
                             exportNiftiPath += extention
                         elif not exportNiftiPath.endswith (extention) : 
                             exportNiftiPath = exportNiftiPath[:-len (extention)] + extention
                     if not exportROIPath.endswith ("_ROI.txt") :
                         exportROIPath += "_ROI.txt"
                     FileUtil.createPath (exportNiftiPath, hasfilename = True)
                     FileUtil.createPath (exportROIPath, hasfilename = True)
                     app.processEvents ()
                     print ("          ROI:" + exportROIPath)
                     shutil.copyfile (ROIDataFile.getPath (), exportROIPath)
                     app.processEvents ()
                     print ("          NIfTI:" + exportNiftiPath)
                     shutil.copyfile (niftiDataFile.getFilePath (), exportNiftiPath)
                     app.processEvents ()
                     if progdialog.wasCanceled () :
                         break
                 except:
                     print ("")
                     print ("A error occured exporting dataset") 
                     print ("    ROI datafile: " + ROIDataFile.getPath ())
                     print ("    NIfTI datafile: " + niftiDataFile.getFilePath ())
                     print ("")
            FileUtil.setPermissionForFilesUnderDirectory (exportPath)
            progdialog.close ()
    
    class PesscaraMaskExport (QThread):
                    
        updateProgress = pyqtSignal(['PyQt_PyObject','PyQt_PyObject'])        
        threadDone = pyqtSignal(['PyQt_PyObject'])        
        
        def __init__(self, projectDataset, NIfTITreeNodeList, dlg):            
            QThread.__init__(self, None)                                                
            self._ProjectDataset = projectDataset
            self._maskExportCanceled = False            
            self._NiftiTreeNodeList = NIfTITreeNodeList
            self._PesscaraContext = dlg._ExportPesscaraContext
            self._SelectedROILst = dlg._SelectedROILst
            self._pointInSliceSize = dlg._pointInSliceSize
            self._pointCrossSliceSize = dlg._pointCrossSliceSize
            self._ExportROIMaskAsBoundingBox = dlg._ExportROIMaskAsBoundingBox
            self._ExportName = dlg._ExportName
            
        def cancelMaskExport (self) :
            self._maskExportCanceled = True
            
        def run (self) :  
           print ("Starting Pesscara ROI export")                        
           for index, NIfTITreeNode in enumerate (self._NiftiTreeNodeList) :            
                
                ROIDataFile = self._ProjectDataset.getROIDatasetInterface().getROIDataForDatasetPath (NIfTITreeNode, IgnoreLock = True)
                
                self.updateProgress.emit (index, "Processing: " + ROIDataFile.getPath ())
                
                try:                            
                    if  (self._maskExportCanceled) :
                        print ("Pesscara export canceled.")
                        break
                except:
                    break   
                                    
                ROIDefs = ROIDefinitions(self._ProjectDataset.getDocumentUIDFileLogInterface ())        
                try:            
                    ROIDefs.loadROIDefsFromProjectFile (Verbose = False)             
                except :
                    ROIDefs = ROIDefinitions(self._ProjectDataset.getDocumentUIDFileLogInterface ())                
                ExportBinaryOrROIMask = not ROIDefs.hasSingleROIPerVoxel ()
                
                tempDataDictionary = ROIDictionary (ROIDefs, None, None, LowMemory = True)            
                tempDataDictionary.loadROIDictionaryFromFile (ROIDataFile,  self._ProjectDataset.getDocumentUIDFileLogInterface ())                
                
                objLst = []                    
                for roiNames in self._SelectedROILst :
                    if (tempDataDictionary.isROIDefined (roiNames)) :
                        obj = tempDataDictionary.getROIObject (roiNames)
                        objLst.append (obj)   
                        
                if (len (objLst) > 0) :                                                                                 
                        try:                    
                            temppath = tempfile.mkdtemp ()
                            try :                                
                                tempNiftiFileName = os.path.join (temppath, self._ExportName)                                
                                try:
                                    niftiVolumeOrientationDescription = tempDataDictionary.getNIfTIVolumeOrientationDescription ()
                                    if (niftiVolumeOrientationDescription == None or not niftiVolumeOrientationDescription.hasParameter ("NativeNiftiFileOrientation") or not niftiVolumeOrientationDescription.hasParameter ("FinalROINiftiOrientation")) :
                                        #print ("Slow Pesscara Volume Write")
                                        niftiDataFile = NIfTITreeNode.getNIfTIDatasetFile ()                                
                                        NiftiVolumeDataToMask = NiftiVolumeData (nib.load (niftiDataFile.getFilePath ()))                                                                                             
                                        NiftiVolumeDataToMask.writeMaskVolume (tempNiftiFileName, objLst, MaskValue = None, InSlicePointSize = self._pointInSliceSize, CrossSlicePointSize = self._pointCrossSliceSize, RoiDefs= ROIDefs, BinaryOrMask = ExportBinaryOrROIMask, ExportAreaROIUsingBoundingVolumeDefinition = self._ExportROIMaskAsBoundingBox)
                                    else:
                                        #print ("Fast Pesscara Volume Write")
                                        NiftiVolumeData._writeMaskVolume (tempDataDictionary.getNIfTIVolumeOrientationDescription (), tempNiftiFileName, objLst, MaskValue = None, InSlicePointSize = self._pointInSliceSize, CrossSlicePointSize = self._pointCrossSliceSize, RoiDefs= ROIDefs, BinaryOrMask = ExportBinaryOrROIMask, ExportAreaROIUsingBoundingVolumeDefinition = self._ExportROIMaskAsBoundingBox)                                        
                                        
                                    Series = NIfTITreeNode.getSeries ()                    
                                    NIfTITreeNode.getPesscaraInterace ().setSeriesROIMask (tempNiftiFileName, Series, self._PesscaraContext)
                                    os.remove (tempNiftiFileName)
                                    print ("Cleaning up.")            
                                except:
                                    try:
                                        os.remove (tempNiftiFileName)
                                    except:
                                        print ("Warrning could not delete temporary file: %s" % (tempNiftiFileName))
                            finally:
                                try:
                                    os.rmdir (temppath)
                                except:
                                    print ("Warrning could not remove temporary directory: %s" % (temppath))
                        except:
                          print ("Error could not save volume mask to pesscara")                                      
                tempDataDictionary.delete ()                                        
           self.threadDone.emit (None)                   


    def _ProgressDlgCancel (self) :
        self._progressCanceled.value = 1

    def _PesscaraMaskExportProgressDlgCancel (self) :
        self._pessscaraMaskExportThread.cancelMaskExport ()
        
    def _ProgressDlgUpdate (self, val) :
        self._exportMaskProgressDlg.setValue (val)
    
    def _PesscaraMaskExportProgressDlgUpdate (self, val, label) :
        self._exportMaskProgressDlg.setValue (val)
        self._exportMaskProgressDlg.setLabelText (label)
        
    def _ProgressDlgDone (self, val) :
        self._exportMaskProgressDlg.close ()
        self._exportMaskProgressDlg = 0
    
    _FileSystemMaskExportProcess = staticmethod (_FileSystemMaskExportProcess)    
    
    def exportVolumeMasks (self) :    
            
        def _correctForDuplicatePath (writepath, DuplicateFileNameDictionary) :
            if (writepath not in DuplicateFileNameDictionary) :
                DuplicateFileNameDictionary[writepath] = 1
            else:                        
                count = DuplicateFileNameDictionary[writepath]
                DuplicateFileNameDictionary[writepath] = count + 1
                writepath, extention = FileUtil.removeExtension (writepath,[".nii.gz", ".nii"])
                writepath += "_%d%s" % (count,extention)
            return writepath
                        
        PesscaraContext  = self._ProjectDataset.getNIfTIDatasetInterface ().getPesscaraContex ()
        ROIDefs = ROIDefinitions(self._ProjectDataset.getDocumentUIDFileLogInterface ())        
        try:            
           ROIDefs.loadROIDefsFromProjectFile ()             
        except :
           ROIDefs = ROIDefinitions(self._ProjectDataset.getDocumentUIDFileLogInterface ())       
                 
        dlg = RCC_ROIVolumeMaskExportDlg (self, ROIDefs, PesscaraContext = PesscaraContext, ROI_FileSystemPath = "", ProjectDataset = self._ProjectDataset, EnableTryLongFileNameExport = True)
        dlg.exec_ ()
        if (dlg._result != "Export"):
            return
        progdialog = QProgressDialog("Exporting ROI Volume Mask", "Cancel", int (0), int (1),self)        
        progdialog.setMinimumDuration (0)
        progdialog.setWindowFlags(progdialog.windowFlags () | QtCore.Qt.Dialog  | QtCore.Qt.WindowStaysOnTopHint)                
        progdialog.setWindowTitle ("Exporting ROI Volume Mask")
        progdialog.setWindowModality(QtCore.Qt.ApplicationModal)        
        progdialog.forceShow()                   
        progdialog.setValue(0)
        self._ProjectDataset.getApp ().processEvents ()
        print ("Start: Getting dataset list.")
        NIfTITreeNodeList = self._getSelectedNiftiDatasetListForExport ()
        progdialog.setMaximum (int (len (NIfTITreeNodeList)))
        print ("Done: Getting dataset list.")        
        DuplicateFileNameDictionary = {}
    
        if (dlg._ExportDEST == "FileSystem") :                
            try:
                appendFileNameCheckState = dlg.getAppendFileNameCheckState ()
                progdialog.setLabelText("Export ROI Masks")

                IsExportingFromPesscara = self._ProjectDataset.getROIDatasetInterface().isPesscaraInterface ()    
            
                DirToProcess =  {}
                MaskExportFileList = []                
                ProjectROIDefs = ROIDefinitions(self._ProjectDataset.getDocumentUIDFileLogInterface ())   
                try:            
                    ProjectROIDefs.loadROIDefsFromProjectFile (Verbose = False)             
                except :
                    ProjectROIDefs = ROIDefinitions(self._ProjectDataset.getDocumentUIDFileLogInterface ())    
                
                if (IsExportingFromPesscara) :        
                    progdialog.setMaximum (int (len (NIfTITreeNodeList) * 100))                
                    TryToExportLongFileNames = False
                else:
                    TryToExportLongFileNames = dlg._TryExportLongFileNames
                    
                for nodeIndex, NIfTITreeNode in enumerate (NIfTITreeNodeList) :   
                    ROIDataFile = self._ProjectDataset.getROIDatasetInterface().getROIDataForDatasetPath (NIfTITreeNode, IgnoreLock = True, Verbose = False)
                    try :
                        niftiDataFile = NIfTITreeNode.getNIfTIDatasetFile ()                           
                    except:
                        niftiDataFile = None
                    
                    writepath = os.path.join (dlg._ExportPath,NIfTITreeNode.getFilesystemROIDataPath (ReturnFileSystemPath = True))                        
                    writepath = _correctForDuplicatePath (writepath, DuplicateFileNameDictionary)                    
                        
                    if TryToExportLongFileNames :
                        longFileNameWritePath = os.path.join (dlg._ExportPath,NIfTITreeNode.getFilesystemROIDataPath (ReturnFileSystemPath = False))                        
                        longFileNameWritePath = _correctForDuplicatePath (longFileNameWritePath, DuplicateFileNameDictionary)                        
                    else:
                        longFileNameWritePath = None
                    
                            
                    ParamTuple = ((ROIDataFile, NIfTITreeNode.getFilesystemROIDataPath (),  niftiDataFile, writepath, longFileNameWritePath))                    
                    if (IsExportingFromPesscara) :                                                
                        BaseProgressIndex = nodeIndex*100
                        NextProgressIndex = BaseProgressIndex  + 100                        
                        self._ProjectDataset.getApp ().processEvents ()
                        MaskExportFileList += RCC_DatasetDlg._FileSystemMaskExportProcess (([ParamTuple], ProjectROIDefs.copy (), dlg._SelectedROILst, dlg._ExportName, dlg._ExportPath,  dlg._pointInSliceSize, dlg._pointCrossSliceSize, dlg._ExportNiftiFile, dlg._ExportROIMaskAsBoundingBox, dlg._OverwriteExistingFiles, appendFileNameCheckState, self._ProjectDataset.getDocumentUIDFileLogInterface (), progdialog , (BaseProgressIndex, NextProgressIndex)))                        
                        ROIDataFile.closeUnsaved ()
                        del ROIDataFile
                        if progdialog.wasCanceled() :
                            break
                    else:
                        writedir, _ = os.path.split (writepath)
                        if (writedir not in DirToProcess) :
                            DirToProcess[writedir] = []                        
                        DirToProcess[writedir].append (ParamTuple)                                                
                        
                #process dont split files contained within one directory across multiple process to avoid file system threading error.
                if (not IsExportingFromPesscara) : 
                    processCount = max (5, PlatformMP.getAvailableCPU ())
                    procQueue = PlatformMP.processQueue (RCC_DatasetDlg._FileSystemMaskExportProcess, None, PoolSize = processCount, SleepTime = 5, ProjectDataset = self._ProjectDataset, Debug = False, ProgressDialog = progdialog)
                    progdialog.setMaximum (len (DirToProcess)*100)
                    exportCount = 0
                    if procQueue.isSerial () :
                        processDialogInterface = None
                        progressDialog = progdialog 
                    else:
                        processDialogInterface = procQueue.getMPProgressDialogInterface ()
                        progressDialog = processDialogInterface
                        
                    for dirPath, dirProcess in DirToProcess.items ():                                            
                        BaseProgressIndex = exportCount*100
                        NextProgressIndex = BaseProgressIndex + 100
                        processParameters = (dirProcess, ProjectROIDefs.copy (), dlg._SelectedROILst, dlg._ExportName, dlg._ExportPath,  dlg._pointInSliceSize, dlg._pointCrossSliceSize, dlg._ExportNiftiFile, dlg._ExportROIMaskAsBoundingBox, dlg._OverwriteExistingFiles, appendFileNameCheckState, self._ProjectDataset.getDocumentUIDFileLogInterface (), progressDialog , (BaseProgressIndex, NextProgressIndex))
                        cleanupParameters = None
                        procQueue.queueJob (self._ProjectDataset.getROISaveThread (), processParameters, cleanupParameters, MPProgressDialogInterface = processDialogInterface)                                                                                            
                        exportCount += 1                        
                        self._ProjectDataset.getApp ().processEvents ()
                        if progdialog.wasCanceled() :
                            break  
                    MaskExportFileList = procQueue.join ()
                    self._ProjectDataset.reloadWindowROIDefs () # Needed to update UI to show ROIDefs which may be loaded by opening ROIdictionarys                    
                    
                if (len (MaskExportFileList) > 0) and (not progdialog.wasCanceled()) :
                    dt = datetime.datetime.now ()
                    localtime = (dt.hour, dt.minute, dt.second, dt.microsecond)
                    date = (dt.month, dt.day, dt.year)
                
                    filename = os.path.join (dlg._ExportPath,"MaskDescriptionFileLst.txt")
                    file = open (filename,"wt")
                    file.write (json.dumps (("Date file created (month, day, year)", date, "Time file created (hour, minute, second, microsecond)", localtime))+ "\n")                
                    file.write (json.dumps (["Source_NIfTI", "Exported_Data"])+"\n")                
                    for maskfileset in MaskExportFileList :                
                        if maskfileset is not None :
                            for maskfilename in maskfileset :
                                if (maskfilename is not None) :
                                    file.write (json.dumps (maskfilename) + "\n")                
                    file.close ()
            finally:
                progdialog.close ()
                self._ProjectDataset.getApp ().processEvents ()
                
            
        elif (dlg._ExportDEST == "PESSCARA") :      
            #refactor to follow above inline paradigm
            self._exportMaskProgressDlg = progdialog            
            self._pessscaraMaskExportThread = RCC_DatasetDlg.PesscaraMaskExport (self._ProjectDataset, NIfTITreeNodeList, dlg)
            self._exportMaskProgressDlg.canceled.connect (self._PesscaraMaskExportProgressDlgCancel)
            self._pessscaraMaskExportThread.updateProgress.connect (self._PesscaraMaskExportProgressDlgUpdate)
            self._pessscaraMaskExportThread.threadDone.connect (self._ProgressDlgDone)             
            self._pessscaraMaskExportThread.start ()                   
       
    
    
    
        
    # Import ROI From Mask    
    _MP_ExportVolumeCSV = staticmethod (_MP_ExportVolumeCSV)    
    
    def exportVolumeCSV (self) :                   
        ProjectROIDefs = ROIDefinitions(self._ProjectDataset.getDocumentUIDFileLogInterface ())        
        try:            
           ProjectROIDefs.loadROIDefsFromProjectFile ()             
        except :
           ProjectROIDefs = ROIDefinitions(self._ProjectDataset.getDocumentUIDFileLogInterface ())               
        exportPath, pointInSlice, pointCrossSlice, exportROIList, CombineNonTouchingROI = ExportVolumeStatsCSV.exportVolumeCSVUI (self, ProjectROIDefs)         
        if (exportPath != "") :
            NIfTITreeNodeList = self._getSelectedNiftiDatasetListForExport ()
                    
            progdialog = QProgressDialog("Generating Table Data", "Cancel", int (0), int (len (NIfTITreeNodeList)),self)
            progdialog.setMinimumDuration (0)
            progdialog.setWindowFlags(progdialog.windowFlags () | QtCore.Qt.Dialog  | QtCore.Qt.WindowStaysOnTopHint)                
            progdialog.setWindowTitle ("Generating Table Data")
            progdialog.setWindowModality(QtCore.Qt.ApplicationModal)   
            try:
                progdialog.forceShow()    
                progdialog.setValue(0)
                self._ProjectDataset.getApp ().processEvents ()               
                descriptionList = self._ProjectDataset.getScanPhaseList () 
                ProjectROIDefs = ROIDefinitions(self._ProjectDataset.getDocumentUIDFileLogInterface ())   
                try:            
                    ProjectROIDefs.loadROIDefsFromProjectFile (Verbose = False)             
                except :
                    ProjectROIDefs = ROIDefinitions(self._ProjectDataset.getDocumentUIDFileLogInterface ())    
                
                cpuCount = PlatformMP.getAvailableCPU ()
                IsPesscara =  self._ProjectDataset.getROIDatasetInterface ().isPesscaraInterface ()
                if (IsPesscara) :
                    DatasetList = []
                    progdialog.setMaximum (len (NIfTITreeNodeList))
                else:
                    procQueue = PlatformMP.processQueue (RCC_DatasetDlg._MP_ExportVolumeCSV, None, PoolSize = cpuCount, SleepTime = 10, ProjectDataset = self._ProjectDataset, Debug=False)
                    progdialog.setMaximum (len (NIfTITreeNodeList) + 1)
                            
                for fIndex, NIfTITreeNode in enumerate (NIfTITreeNodeList) :     
                        ROIDataFile = self._ProjectDataset.getROIDatasetInterface().getROIDataForDatasetPath (NIfTITreeNode, IgnoreLock = True, Verbose = False)  
                        if (ROIDataFile is not None) : 
                            SaveFileNifitFileSourceFileObject = NIfTITreeNode.getSaveFileNifitFileSourceFileObject ()                                                        
                            niftiDataFile = NIfTITreeNode.getNIfTIDatasetFile ()    
                            processParameters = (ProjectROIDefs, descriptionList, ROIDataFile, SaveFileNifitFileSourceFileObject, niftiDataFile, exportROIList, pointInSlice, pointCrossSlice, CombineNonTouchingROI, self._ProjectDataset.getDocumentUIDFileLogInterface ())                      
                            cleanupParameters = None
                            if (IsPesscara) :
                                DatasetList.append (RCC_DatasetDlg._MP_ExportVolumeCSV (processParameters))                            
                            else:
                                procQueue.queueJob (self._ProjectDataset.getROISaveThread (), processParameters, cleanupParameters)                                                    
                            ROIDataFile.closeUnsaved ()
                            niftiDataFile.closeDataFile ()
                            progdialog.setLabelText (NIfTITreeNode.getText ())
                            progdialog.setValue (fIndex)
                            self._ProjectDataset.getApp ().processEvents ()
                            if progdialog.wasCanceled() :
                                break
                if (not IsPesscara) :
                    DatasetList = procQueue.join ()
                    progdialog.setValue (len (NIfTITreeNodeList) + 1)
            finally:
                progdialog.close()
                self._ProjectDataset.getApp ().processEvents ()
            self._ProjectDataset.reloadWindowROIDefs () # Needed to update UI to show ROIDefs which may be loaded by opening ROIdictionarys
            if (len (DatasetList) > 0) : 
                progdialog = QProgressDialog("Generating Table", "Cancel", int (0), int (0),self)
                progdialog.setMinimumDuration (0)
                progdialog.setWindowFlags(progdialog.windowFlags () | QtCore.Qt.Dialog  | QtCore.Qt.WindowStaysOnTopHint)                
                progdialog.setWindowTitle ("Exporting Tables")
                progdialog.setWindowModality(QtCore.Qt.ApplicationModal)        
                try:
                    progdialog.forceShow()    
                    progdialog.setValue(0)
                    self._ProjectDataset.getApp ().processEvents ()
                    exportTable = ExportVolumeStatsCSV.getDatasetTable (DatasetList)
                    if (exportTable is not None) :
                        pd = exportTable.convertTableToPandasDataFrame()
                        if (exportPath.lower().endswith (".xlsx")):
                            pd.to_excel (exportPath,index=False,header=False)
                        else:
                            pd.to_csv (exportPath,index=False,header=False)
                finally:
                    progdialog.close ()
                    self._ProjectDataset.getApp ().processEvents ()
            
                                                  

                    
    @staticmethod
    def _exportProjectStatusCSV (depth, nodeList, treemem) :
        childNodeCount = 0
        for nodetupl in nodeList :        
            node, childern = nodetupl
            if (depth not in treemem) :
                treemem[depth] = []
            treemem[depth].append (node)
            if (len (childern) > 0)    :
                childNodeCount += RCC_DatasetDlg._exportProjectStatusCSV (depth + 1, childern, treemem)
        return childNodeCount + len (nodeList)
         
    def exportProjectStatusCSV (self) :      
        dlg= QFileDialog( None )
        dlg.setWindowTitle('Save project status csv:' )
        dlg.setViewMode( QFileDialog.Detail )    
        dlg.setFileMode (QFileDialog.AnyFile)
        dlg.setNameFilters( [self.tr('Project (*.csv)'), self.tr('All Files (*)')] )
        dlg.setDefaultSuffix( '.csv' )
        dlg.setAcceptMode (QFileDialog.AcceptSave)
        if dlg.exec_()  and (len (dlg.selectedFiles())== 1):
            projectPath = dlg.selectedFiles()[0]                                     
            
            progdialog = QProgressDialog("Generating project status CSV file", "Cancel", int (0), int (100), self)
            progdialog.setMinimumDuration (0)
            progdialog.setWindowFlags(progdialog.windowFlags () | QtCore.Qt.Dialog  | QtCore.Qt.WindowStaysOnTopHint)                
            progdialog.setWindowTitle ("Generating project status CSV file")
            progdialog.setWindowModality(QtCore.Qt.ApplicationModal)                    
            try:
                progdialog.forceShow()    
                progdialog.setValue (0)
                self._ProjectDataset.getApp ().processEvents ()
                treeDepthMemory = {}
                allnodes = self._ProjectDataset.getNIfTIDatasetInterface ().getAllTreeNodes()
                totalNodeCount = RCC_DatasetDlg._exportProjectStatusCSV (0, allnodes, treeDepthMemory)
                progdialog.setMaximum (totalNodeCount)
            
                treeDepths = len (treeDepthMemory)
                statusfile = open(projectPath, "wt")                
                
                statusfile.write ("Depth, Name, FullDataPath, DefinesNIfTIData, HasAtLeastOneROIDefined, ChildernCount\n")          
                nodeCounter = 0
                progdialog.setValue (nodeCounter)   
                for depth in range (0, treeDepths) :
                    nodeList = treeDepthMemory[depth]
                    if  (progdialog.wasCanceled ()) :
                            break
                    for node in nodeList : 
                        progdialog.setValue (nodeCounter)   
                        if  (progdialog.wasCanceled ()) :
                            break
                    
                        nodeStringList =  []
                        nodeStringList.append (str (depth))
                        nodeStringList.append (node.getFilename ())
                        nodeStringList.append (node.getPath ())
                        if (node.describesNiftiDataFile ()) :
                            nodeStringList.append ("True")
                        else:
                            nodeStringList.append ("False")
                        if (node.getNodeROIDatasetIndicator ()) :
                            nodeStringList.append ("True")
                        else:
                            nodeStringList.append ("False")
                        nodeStringList.append (str (node.getChildCount ()))
                        statusfile.write (", ".join (nodeStringList) + "\n")                      
                        nodeCounter += 1            
                statusfile.close ()        
                if  (progdialog.wasCanceled ()) :
                    os.path.remove ("ProjectStatus.CSV")
                    print ("CSV File Export Cancled")
                else:
                    print ("CSV File Exported")
            except:
                print ("A Error occured exporting the data.")
                progdialog.setValue (totalNodeCount)
            finally:
                progdialog.close ()
                self._ProjectDataset.getApp ().processEvents ()

    def ShowScanPhaseSelectionDlg (self) :          
        if (len (self._ProjectDataset.getActiveContourWindowList ()) > 0) :
            MessageBoxUtil.showMessage ("Can not open scan phase selection window", "All contouring windows must be closed to open the scan phase selection window.")
            return
        
        activeWindow = self._ProjectDataset.getActiveWindow ()      
        if (self._ProjectDataset.getScanPhaseSelectionDlg () == None) :
            scanPhaseSelectionDlg = RCC_ScanPhaseSelectionDlg (self, self._ProjectDataset)
            self._ProjectDataset.setScanPhaseSelectionDlg (scanPhaseSelectionDlg)
            
        scanPhaseSelectionDlg = self._ProjectDataset.getScanPhaseSelectionDlg ()
                    
        if (activeWindow is not None and activeWindow != scanPhaseSelectionDlg and activeWindow.getSelectedNIfTIDataset () != None) :                        
            scanPhaseSelectionDlg.initWindow (activeWindow.getSelectedNIfTIDataset (), sliceNumber = activeWindow.getSelectedSlice (), activeContourWindow = activeWindow)
        else:
            item = self.ui.DatasetTreeView.selectionModel().currentIndex() 
            loaded = False
            if (item is not None) :
                niftidataset = item.internalPointer ()    
                if (niftidataset is not None and niftidataset.getParent () is not None)  :
                    scanPhaseSelectionDlg.initWindow (niftidataset)
                    loaded = True
                    
            if (not loaded) :
                scanPhaseSelectionDlg.initWindow (None)
                                       
        scanPhaseSelectionDlg.setGeometry (self.x ()+self.width (), self.y ()+21, scanPhaseSelectionDlg.width (), self.height ())                
        scanPhaseSelectionDlg.show ()    
        self._ProjectDataset.setActiveWindow (scanPhaseSelectionDlg)
       
            
    def DatasetOpenMenubarAction (self) :
        return
        """
        path = QFileDialog.getExistingDirectory(self, 'Open ROI Dataset')            
        path = QtCore.QDir.toNativeSeparators (path)
        if (path != "") :
            self.RCCDataset.load (path)        
            for childindex in range (self.ui.TreeDataset.topLevelItemCount()) :
                boldParent = False
                ch = self.ui.TreeDataset.topLevelItem  (childindex)
                for scannumber in range (ch.childCount ()) :
                    scandata = ch.child (scannumber)
                    nifti = scandata._NiftiDataFile
                    roiPath = self.RCCDataset.getROIDataForDatasetPath (nifti)
                    bold = False
                    if ROIDictionary.contoursExistForDataSet (roiPath) :
                        boldParent = True
                        bold = True
                        
                    font = scandata.font (0)                            
                    font.setBold (bold)
                    scandata.setFont (0, font)     
                font = ch.font (0)                            
                font.setBold (boldParent)
                ch.setFont (0, font)             
            if (self.NiftiVolumePath != None) : 
                self.LoadROIMenubarAction ()        
            self.setWindowTitle ("ROI:" + self.RCCDataset.getROIDataSetRootPath () + " NifTi: " + self.RCCDataset.getNiftiDataSetRootPath ())
            """
    def DatasetSaveAsMenubarAction (self) :
                return
                """
                path = QFileDialog.getSaveFileName(self, 'Save ROI Dataset As')        
                path = QtCore.QDir.toNativeSeparators (path[0])
                if (path != "") :
                    self.RCCDataset.save (path, self._app, self, True)
                    self.setWindowTitle ("ROI:" + self.RCCDataset.getROIDataSetRootPath () + " NifTi: " + self.RCCDataset.getNiftiDataSetRootPath ())
                    """  
                
        
    def DatasetSaveCopyMenubarAction (self) :
                return
                """
                path = QFileDialog.getSaveFileName(self, 'Save ROI Dataset Copy')
                path = QtCore.QDir.toNativeSeparators (path[0])
                if (path != ""):
                    self.RCCDataset.saveCopy (path, self._app, self, True)
                    self.setWindowTitle ("ROI:" + self.RCCDataset.getROIDataSetRootPath () + " NifTi: " + self.RCCDataset.getNiftiDataSetRootPath ())
                    """ 
        
    def LoadFromPesscara (self) :          
                return
                
