# -*- coding: utf-8 -*-
"""
Created on Tue Sep 13 15:39:04 2016

@author: Philbrick
"""

import numpy as np
import math
from six import *
from PyQt5 import  QtGui, QtWidgets, QtCore
from scipy import ndimage
from rilcontourlib.util.rcc_util import  HounsfieldVisSettings, VisSettings, PythonVersionTest
import copy
try :
    import weakref
    UseWeakRef = True
except:
    UseWeakRef = False
#from numba import jit
import matplotlib.pyplot as plt

def debug (img) :
    plt.imshow (img); plt.show ()

class NpQtImageViewer:
    """Creates Images which can easily be drawn on a Qt Surface in Qt """                          
    def __init__ (self) :
        self._maskRGBA = None
        self._colorMemCache = None 
   

    #@jit (cache=True)
    def SetSliceNPDataBW (self, data, mask = None, colorOverlay = None, colorOverlayTranspValue = 0.5, ColorTable = None, ColorData = None) :   
        #data = data / 4
       
        if ColorData is not None :
            datashape = ColorData.shape[0:2]
            ColorData = np.clip (ColorData,0, 255)   
            ColorData = ColorData.astype (np.uint8)    
            width, height = datashape
            if self._colorMemCache is not None and self._colorMemCache.shape != (4, width, height) :
                self._colorMemCache = None
            if self._colorMemCache is None :
                self._colorMemCache = np.zeros ((4, width, height), dtype = np.uint8)   
                self._colorMemCache[3,:,:] = 255
            colorRGB = self._colorMemCache
            colorRGB[2,:,:] = ColorData[:,:,0]
            colorRGB[1,:,:] = ColorData[:,:,1]
            colorRGB[0,:,:] = ColorData[:,:,2] 
            image =  QtGui.QImage (colorRGB.tobytes('F'), width, height, width*4, QtGui.QImage.Format_ARGB32)
            self.pixmap = QtGui.QPixmap.fromImage (image)
        else:
            datashape = data.shape[0:2]
            data = np.clip (data,0, 255)        
            data = data.astype (np.uint8)        
            width, height = datashape
            image =  QtGui.QImage (data.tobytes('F'), width, height, width, QtGui.QImage.Format_Grayscale8)
            self.pixmap = QtGui.QPixmap.fromImage (image)
           
           
            
        #MaskSet = True
        #mask = data > 200
        if (mask is not None and mask.shape == datashape) : 
            if (self._maskRGBA is None or self._maskRGBA.shape != (4, width, height)) :
                self._maskRGBA = np.zeros ((4, width, height), dtype = np.uint8)                    
            maskRGBA = self._maskRGBA    
                            
            if ColorTable is None :
                mask = np.clip (mask,0, 1)        
                
                maskRGBA[0,...] = 75 * mask   #B
                maskRGBA[1,...] = 167 * mask  #G
                maskRGBA[2,...] = 255 * mask  #R
                maskRGBA[3,...] = int (255/2) * mask    # Alpha  255 == on
            else:
                maskRGBA[...] = np.moveaxis(ColorTable[mask], -1, 0)
                maskRGBA[3,...] = (colorOverlayTranspValue * maskRGBA[3,...]).astype(np.uint8)
           
            maskImage =  QtGui.QImage (maskRGBA.tobytes('F'), width, height, width * 4, QtGui.QImage.Format_ARGB32)                
            qp = QtGui.QPainter (self.pixmap)            
            qp.drawImage (0,0,maskImage)
            qp.end ()            
        elif (colorOverlay is not None and (colorOverlay.shape[0],colorOverlay.shape[1]) == datashape) :
            if (self._maskRGBA is None or self._maskRGBA.shape != (4, width, height)) :
                self._maskRGBA = np.zeros ((4, width, height), dtype = np.uint8)                    
            maskRGBA = self._maskRGBA                                
            colorOverlayMmask = (colorOverlay[:,:,0] >= 0).astype (np.uint8)
            maskRGBA[0,:,:] = colorOverlay[:,:,0]  #B
            maskRGBA[1,:,:] = colorOverlay[:,:,1]  #G
            maskRGBA[2,:,:] = colorOverlay[:,:,2]  #R
            maskRGBA[3,:,:] = (255.0 * colorOverlayTranspValue) * colorOverlayMmask
                
            maskImage =  QtGui.QImage (maskRGBA.tobytes('F'), width, height, width * 4, QtGui.QImage.Format_ARGB32)                
            qp = QtGui.QPainter (self.pixmap)            
            qp.drawImage (0,0,maskImage)
            qp.end ()            
        return        
        
    def SetSliceNPDataBlack (self, y, x) :           
        data = np.zeros ((x,y),dtype=np.uint8)        
        width, height = x,y
        image =  QtGui.QImage (data.tobytes('F'), width, height, width, QtGui.QImage.Format_Grayscale8)
        self.pixmap = QtGui.QPixmap.fromImage (image)
        
class _Zoom :
    def __init__ (self) :
        self.clear ()
    
    def isInitalized (self) :
        return self._Initialized
    
    def setInitalized (self, initalized) :
        self._Initialized = initalized
    
    def scrollY (self, dy) :
        height = self._maxY - self._minY
        self._minY = dy
        self._maxY = dy + height
        
    def scrollX (self, dx) :
        width = self._maxX - self._minX
        self._minX = dx
        self._maxX = dx + width
        
    def setZoom (self, minX, minY, maxX, maxY):  #zoom coordinates set in image coordinates not in screen coordiante
        self._Initialized = True
        self._minX = minX
        self._maxX = maxX
        self._minY = minY
        self._maxY = maxY     
        self._orig_minX = minX
        self._orig_maxX = maxX
        self._orig_minY = minY
        self._orig_maxY = maxY 
        
    
    def copy (self) :
        newZ = _Zoom ()
        newZ._Initialized = self._Initialized
        newZ._minX = self._minX
        newZ._minY = self._minY
        newZ._maxY = self._maxY
        newZ._maxX = self._maxX
        newZ._orig_minX = self._orig_minX
        newZ._orig_minY = self._orig_minY
        newZ._orig_maxX = self._orig_maxX
        newZ._orig_maxY = self._orig_maxY        
        newZ._centerX = self._centerX
        newZ._centerY = self._centerY
        return newZ
            
    def getCenterTarget (self) :
        return (self._centerX, self._centerY)
    
    def setCenterTarget (self, xp, yp) :
        try :
            if xp is None :
                self._centerX = None
            else:
                self._centerX = int (xp)
            if yp is None :
                self._centerY = None
            else:
                self._centerY = int (yp)
        except:
            self._centerX = None
            self._centerY = None
    
    def hasCenterTarget (self) :
        return self._centerX is not None
    
    def clear (self):
        self._Initialized = False
        self._oldZoom = -1
        self._currenetZoom = -1
        self._minX = None
        self._maxX = None
        self._minY = None
        self._maxY = None
        self._orig_minX = None
        self._orig_maxX = None
        self._orig_minY = None
        self._orig_maxY = None
        self._centerX = None
        self._centerY = None
    
    def reset (self):
        self._minX = self._orig_minX 
        self._maxX = self._orig_maxX
        self._minY = self._orig_minY
        self._maxY = self._orig_maxY
    
    def getOrigMinX (self) :
        return self._orig_minX
    
    def getOrigMaxX (self) :
        return self._orig_maxX
    
    def getOrigMinY (self) :
        return self._orig_minY
    
    def getOrigMaxY (self) :
        return self._orig_maxY
    
    def getZoomCenterCord (self)    :
        return (int ((self._minX + self._maxX) /2),  int ((self._minY + self._maxY) /2))

    def setMaxY (self, my) :
        self._maxY = my
    
    def setMaxX (self, mx) :
        self._maxX = mx

    def setMinY (self, my) :
        self._minY = my
    
    def setMinX (self, mx) :
        self._minX = mx
              
    def getMaxY (self) :
        return self._maxY
    
    def getMaxX (self) :
        return self._maxX    
    
    def width (self):
        return self._maxX - self._minX + 1
    
    def height (self):
        return self._maxY - self._minY + 1
        
    def getMinX (self) :
        if (self._Initialized == False or self._minX < 0):
            return 0
        else:
            return self._minX
    
    def getMinY (self) :
        if (self._Initialized == False or self._minY < 0):
            return 0
        else:
            return self._minY

    def convertFromScreenToImage (self, x,y):
        if (not  self._Initialized) :
            return (x,y)
        else:                              
            return (x + self._minX, y + self._minY)
            
    def convertFromImageToScreen (self, x,y):
        if (not  self._Initialized) :
            return (x,y)
        else:            
            return (x - self._minX, y - self._minY)       
        
class ViewPortTransformation :
    def __init__ (self) :
        self._RotateDeg = 0
        self._Flip = ""    

    def getFlipHorizontal (self) :
        return (self._Flip == "H" or self._Flip == "HV")
        
    def setFlipHorizontal (self, value):
        if (value) :
            if (self._Flip == "V"):
                self._Flip = "HV"
            elif (self._Flip == ""):
                self._Flip = "H"
        else :
            if (self._Flip == "HV"):
                self._Flip = "V"
            elif (self._Flip == "H"):
                self._Flip = ""
        
    def getFlipVertical (self) : 
        return (self._Flip == "V" or self._Flip == "HV")
        
    def setFipVertical (self, value) :
        if (value) :
            if (self._Flip == "H"):
                self._Flip = "HV"
            elif (self._Flip == ""):
                self._Flip = "V"
        else :
            if (self._Flip == "HV"):
                self._Flip = "H"
            elif (self._Flip == "V"):
                self._Flip = ""
                
    def getRotation (self) :
        return self._RotateDeg
        
    def setRotation (self, deg) :
        testangle = math.fabs (deg)
        if (testangle == 0 or testangle == 180 or testangle == 90):
            self._RotateDeg = deg
        
    def getTransformedVoxelScale (self, voxelsize) :
        if (math.fabs (self._RotateDeg) == 90) :
            return (voxelsize[1], voxelsize[0])
        return voxelsize
        
    def getQTransformation (self) :                
        transform = QtGui.QTransform()       
        transform.rotate( self._RotateDeg)
        if (self._Flip == "H") :
            transform.scale(-1, 1)
        elif (self._Flip == "V") :
            transform.scale(1, -1)
        elif (self._Flip == "HV") :
            transform.scale(-1, -1)
        return transform
    
    @staticmethod
    def _transformSlice (axis, RotateDeg, Flip, Volume, sliceIndex):
        if axis == 'X' :
            sliceData = Volume[sliceIndex, : , :]
        elif axis == 'Y' :
            sliceData = Volume[:, sliceIndex , :]
        elif axis == 'Z' :
            sliceData = Volume[:, :, sliceIndex]
        sliceData = np.copy (sliceData)
        if RotateDeg == 90 :
            sliceData = np.rot90 (sliceData)
        elif RotateDeg == -90 :
            sliceData = np.rot90 (sliceData, -1)
        elif abs(RotateDeg) == 180 :
            sliceData = np.rot180 (sliceData)
        if ("H" in Flip) :
            sliceData = np.fliplr (sliceData)
        if ("V" in Flip) :
            sliceData = np.flipud (sliceData)        
        return sliceData
    
    def transformNPSlice (self,Volume, sliceIndex, axis='Z'):
        if axis == "Z" and self._RotateDeg == 0 and self._Flip =="" :
            sliceIndex = max (0,min (Volume.shape[2]-1,sliceIndex))
            return Volume[:,:,sliceIndex]
        if axis == 'X' :
            zDim = 0            
        elif axis == 'Y' :
            zDim = 1
        elif axis == 'Z' :
            zDim = 2
        sliceIndex = max (0,min (Volume.shape[zDim]-1,sliceIndex))
        return ViewPortTransformation._transformSlice (axis, self._RotateDeg, self._Flip, Volume, sliceIndex)
    
    def transformNPVolume (self,Volume, axis='Z'):
        if axis == "Z" and self._RotateDeg == 0 and self._Flip =="" :
            return Volume            
        if axis == 'X' :
            xDim,yDim,zDim = 1, 2, 0            
        elif axis == 'Y' :
            xDim,yDim,zDim = 0, 2, 1
        elif axis == 'Z' :
            xDim,yDim,zDim = 0, 1, 2
        if abs (self._RotateDeg) == '90' :
            outputshape = (Volume.shape[yDim],Volume.shape[xDim],Volume.shape[zDim])
        else:
            outputshape = (Volume.shape[xDim],Volume.shape[yDim],Volume.shape[zDim])                
        transformedVolume= np.zeros (outputshape, dtype=Volume.dtype)        
        for sliceIndex in range (Volume.shape[zDim]) : 
            transformedVolume[:,:,sliceIndex] = ViewPortTransformation._transformSlice (axis, self._RotateDeg, self._Flip, Volume, sliceIndex)
        return transformedVolume
    
    def transformImageScreenViewToSliceMap (self, sliceData, axis="Z") :            
        if self._RotateDeg == 0 and self._Flip =="" :
            return sliceData
        sliceData = np.copy (sliceData)
        Rot = -self._RotateDeg
        if Rot == 90 :
            sliceData = np.rot90 (sliceData)
        elif Rot == -90 :
            sliceData = np.rot90 (sliceData, -1)
        elif abs(Rot) == 180 :
            sliceData = np.rot180 (sliceData)
            
        if ("H" in self._Flip) :
            sliceData = np.fliplr (sliceData)
        if ("V" in self._Flip) :
            sliceData = np.flipud (sliceData)            
        return sliceData
    
    def transformScreenToImage (self, x , y, pixmapwidth, pixmapheight):
        #print ("Pre Transform Screen To Image %d, %d" % (x,y))
        #print ("Post Transform Screen To Image %d, %d" % (x,y))
        if   (self._RotateDeg == 90 or self._RotateDeg == -90) :
                pixmapwidth, pixmapheight =  pixmapheight, pixmapwidth
                
        if   (self._RotateDeg == 90) :
                x, y = y,  pixmapwidth-x        
                pixmapwidth, pixmapheight =  pixmapheight, pixmapwidth
                
        elif (self._RotateDeg == 180) :
                x, y = pixmapwidth - x, pixmapheight - y
        elif (self._RotateDeg == -90) :
                x, y = pixmapheight - y, x
                pixmapwidth, pixmapheight =  pixmapheight, pixmapwidth
        
        if (self._Flip == "H" or self._Flip == "HV") :
               x = pixmapwidth - x                           
        if (self._Flip == "V" or self._Flip == "HV") :
               y = pixmapheight - y    
       
        return (x,y)
    
    def transformControlPointIndexToImage (self, CP):
        if   (self._RotateDeg == 90) :
               cpMap = [2,0,3,1,7,6,4,5]   
               CP = cpMap[CP]
        elif (self._RotateDeg == 180) :
               cpMap = [3,2,1,0,5,4,7,6]
               CP = cpMap[CP]
        elif (self._RotateDeg == -90) :
               cpMap = [1,3,0,2,6,7,5,4]
               CP = cpMap[CP]
        
        if (self._Flip == "H" or self._Flip == "HV") :
               cpMap = [1,0,3,2,5,4,6,7]
               CP = cpMap[CP]                          
        if (self._Flip == "V" or self._Flip == "HV") :
               cpMap = [2,3,0,1,4,5,7,6]
               CP = cpMap[CP]   
        return CP    
    
    def transformShapeImageToScreen (self, s1, s2) :
        if   (self._RotateDeg == 90 or self._RotateDeg == -90) :
            return s2, s1
        return s1, s2
        
    def transformImageToScreen (self, x , y, pixmapwidth, pixmapheight):
        #print ("pixmapheight %d pixmapwidth %d" % ( pixmapwidth, pixmapheight))
        if (self._Flip == "H" or self._Flip == "HV") :
               x = pixmapwidth - x                           
        if (self._Flip == "V" or self._Flip == "HV") :
               y = pixmapheight - y
               
        if   (self._RotateDeg == 90) :
                x, y = pixmapheight - y, x
                pixmapwidth, pixmapheight =  pixmapheight, pixmapwidth
        elif (self._RotateDeg == 180) :
                x, y = pixmapwidth  - x, pixmapheight  - y
        elif (self._RotateDeg == -90) :
                x, y = y,  pixmapwidth-x        
                pixmapwidth, pixmapheight =  pixmapheight, pixmapwidth
                        
        return (x,y)
                
class NiftiSliceWidget (QtWidgets.QWidget):
    def __init__ (self, parent=None):
        QtWidgets.QWidget.__init__ (self, parent)
        #self._lastPenDraw = None
        self._coordinateProjectionSliceNumberCacheProjectionState = None
        self._coordinateProjectionSliceNumberCache = []
        self._trasformPTCache  = None
        self._SelectionChangedDisabled = False
        self._ZoomInitlizedDim = None
        self._isSelected = False
        self._selectionListener = []
        self._lastPenDraw = None
        self._DrawSelectedROIOnTop = False
        self._windowUpdateEnabled = True
        self._pendingWindowUpdate = False
        self.enableUpdateSlice (UpdateSlice = False)
        self._TempDrawPixmap = None 
        self._AccumulatorPixmap = None
        self._colorOverlayTranspValue = 0.5
        self._colorOverlay = None
        self._PenSettings = None
        self._MousePressedButton = 0
        self._mX = None # old Mouse X cordinate used for pen drawing, in Image Coordinates
        self._lastMouseSystemCoordinate = None 
        self._ROIDefs = None
        self._disableZoomSlider  = False
        self._cancelMouseMove = False
        w = self.width  ()
        h = self.height ()
        self._visSettings  = None
        self._hounsfieldVisSettings = None
        self._OffScreenBuffer = QtGui.QPixmap (w, h)        
        self._ROIDrawBuffer = QtGui.QPixmap (w, h)        
        self._nifti_volume = None
        self._projectDataset = None
        self._Coordinate = None
        self._sliceListener = []
        self.SliceViewer = None        
        self._UISlider = None 
        self._UpdateSlice ()  
        self._ROIAxisObjectList = []
        self._MouseMoveSliceListener = []
        self._MousePressedListners = []
        self._MouseReleasedListners = []
        self._MouseDblClickListners = []
        self._MouseMovedEventList = []
        self._SliceAxis = None
        self._zoom = _Zoom ()
        self._zoomListener = []
        self._ROIBBVisilbe = True
        self._ROIBBResize = True
        self._hounsfieldVisSettings = HounsfieldVisSettings ()  
        self._ViewPortTransformation = ViewPortTransformation ()
        self._UIZoomScrollX = None
        self._UIZoomScrollY = None
        self._drawCoordinateIndicator = False
        self._MousePressedInitalized = False 
        self._MouseMoved = False
        self._overlayNiftiVolumeMask = None
        self._overlayNiftiVolumeMaskColorTable = None
        self._SelectedObjectLst = []
        self.setColorOverlay (overlay = None, transpValue = 0.5) 
        
        self._selectionDimWidth, self._selectionDimHeight = 256, 256
        self._RangeSelectionEnabled = False
        
        self._filenameLableTxt = ""
        self._HiddenLabelTxt = ""
        self._ZoomLabelTxt = ""
        self._LockedLabelTxt = ""
        self._SharedROIMask = None
        self._AxisProjectionControls = None
        #self._oldzoomstate = None
    #def allocateCoordinate (self) :
         #self._Coordinate  = Coordinate ()
         #self._Coordinate.setCoordinate ((0, 0, 0))    
    
    def setAxisProjectionControls (self, controls) :
        self._AxisProjectionControls = controls
        
    def getAxisProjectionControls (self) :
        return self._AxisProjectionControls
    
    def areAxisProjectionControlsEnabledForAxis (self) :
        return self._AxisProjectionControls is not None and self._AxisProjectionControls.isAxisTransformationEnabled () and self._AxisProjectionControls.isAxisRotated () 
    
    def getNIfTIVolumeSliceViewProjection (self, SliceIndex = None, ReturnColorNiftiVolumeData = False) :
        if self._nifti_volume is None or self._SliceAxis is None :
            return None      
        if ReturnColorNiftiVolumeData and self._nifti_volume.hasColorImageData () :
            RGBColorNiFti = self._nifti_volume.getColorImageData()
            red   = RGBColorNiFti[...,0]
            green = RGBColorNiFti[...,1]
            blue  = RGBColorNiFti[...,2]
            channelData = []
            for colorChannel in [red, green, blue] :
                if SliceIndex is not None :
                    if self.areAxisProjectionControlsEnabledForAxis () and self._SliceAxis == "Z":
                        c_data , clipMask = self.getAxisProjectionControls().getNIfTIProjection (self.getCoordinate (), None, Mask = None, UseCache = False, SliceIndex = SliceIndex, ArrayMem = colorChannel) 
                    else:
                        c_data = self._ViewPortTransformation.transformNPSlice (colorChannel, SliceIndex, axis=self._SliceAxis)
                else:
                    c_data = self._ViewPortTransformation.transformNPVolume (colorChannel, self._SliceAxis)
                channelData.append (c_data)
            return np.concatenate (channelData, axis = -1)
        else:            
            if SliceIndex is not None :
                if self.areAxisProjectionControlsEnabledForAxis () and self._SliceAxis == "Z":
                    rawImageData, clipMask = self.getAxisProjectionControls().getNIfTIProjection (self.getCoordinate (), self._nifti_volume, UseCache = True, SliceIndex = SliceIndex)
                    return rawImageData
                else:
                    return self._ViewPortTransformation.transformNPSlice (self._nifti_volume.getImageData (), SliceIndex, axis=self._SliceAxis)
            return self._ViewPortTransformation.transformNPVolume (self._nifti_volume.getImageData (), self._SliceAxis)
    
    def transformShapeImageToScreen (self, s1, s2) :
        if s1 is None or s2 is None :
            return None, None
        return self._ViewPortTransformation.transformShapeImageToScreen (s1, s2)
    
    def transformSliceMapPointToScreen (self, cX, cY) :
        if cX is None or cY is None :
            return None, None
        return self._ViewPortTransformation.transformImageToScreen (cX, cY, self.SliceViewer.pixmap.width (), self.SliceViewer.pixmap.height ())
    
    def transformImageScreenViewToSliceMap (self, npdata) :
        if self._SliceAxis is None :
            return None
        return self._ViewPortTransformation.transformImageScreenViewToSliceMap (npdata, self._SliceAxis)
    
    def setWindowUpdateEnabled (self, stat) :
        if self._windowUpdateEnabled != stat :
            self._windowUpdateEnabled = stat 
            if (stat and self._pendingWindowUpdate) :
                self.update ()                
    
    def update (self,x1 = None, x2 = None, x3 = None, x4 = None) :
        if self._windowUpdateEnabled :
            if (x2 is not None) :
                super(NiftiSliceWidget, self).update (x1, x2, x3, x4)
            elif (x1 is not None) :
                super(NiftiSliceWidget, self).update (x1)
            else:
                super(NiftiSliceWidget, self).update ()
            self._pendingWindowUpdate = False
        else:
            self._pendingWindowUpdate = True
                
        
        
    def getFileNameLabel (self) :
        return self._filenameLableTxt
    
    def setFileNameLabel (self, txt) :
        if self._filenameLableTxt != txt : 
            self._filenameLableTxt = txt
            self.update ()
    
    def getROIHiddenLabel (self) :
        return self._HiddenLabelTxt
    
    def setROIHiddenLabel (self, txt) :
        if self._HiddenLabelTxt != txt : 
            self._HiddenLabelTxt = txt
            self.update ()
    
    def setROILockedLabel (self, txt):
        if self._LockedLabelTxt != txt : 
            self._LockedLabelTxt = txt
            self.update ()
            
    def getZoomLabel (self):
        return self._ZoomLabelTxt
        
    def setZoomLabel (self, txt) :
        if self._ZoomLabelTxt != txt : 
            self._ZoomLabelTxt = txt
            self.update ()
            
    def setRangeSelectionDim (self, width, height) :
            self._selectionDimWidth, self._selectionDimHeight = width, height
            
    def getRangeSelectionDim (self) :            
        return (self._selectionDimWidth, self._selectionDimHeight)
    
    def isRangeSelectionEnabled (self) :            
        return (self._RangeSelectionEnabled)
    
    def setRangeSelectionEnabled (self, enabled) :            
        if (self._RangeSelectionEnabled != enabled) :
            self._RangeSelectionEnabled = enabled
            self.update()
        
        
    def  delete (self):
        self._coordinateProjectionSliceNumberCacheProjectionState = None
        self.setColorOverlay (overlay = None, transpValue = 0.5) 
        self._isSelected = False
        self._selectionListener = []
        self._lastMouseSystemCoordinate = None
        self.removeVisSettings ()
        self.removeHounsfieldVisSettings ()
        self._visSettings  = None
        self._hounsfieldVisSettings = None
        self._OffScreenBuffer = None
        self._ROIDrawBuffer = None
        self._nifti_volume = None
        self._projectDataset = None
        self._Coordinate = None
        self._sliceListener = []
        self.SliceViewer = None        
        self._UISlider = None 
        self._UpdateSlice ()  
        self._ROIAxisObjectList = []
        self._MouseMoveSliceListener = []
        self._MousePressedListners = []
        self._MouseReleasedListners = []
        self._MouseDblClickListners = []
        self._SliceAxis = None
        self._zoom = None
        self._zoomListener = []
        self._ROIBBVisilbe = True
        self._ROIBBResize = True
        self._hounsfieldVisSettings = None
        self._ViewPortTransformation = None
        self._UIZoomScrollX = None
        self._UIZoomScrollY = None
        self._drawCoordinateIndicator = False
        self._MousePressedInitalized = False 
        self._MouseMoved = False
        self._overlayNiftiVolumeMask = None
        self._overlayNiftiVolumeMaskColorTable = None
        self._SelectedObjectLst = []
        self._SharedROIMask = None
        self._TempDrawPixmap = None 
        self._AccumulatorPixmap = None
        self._AxisProjectionControls = None
        self._trasformPTCache  = None
        self._coordinateProjectionSliceNumberCache = []
        #self._lastPenDraw = None
        
    def closeEvent (self, event) :
        print ("_Nifti Sice widget close event")
        self.delete ()
        QtWidgets.QWidget.closeEvent (self, event)
        
    def getAxisDisplayOrientation (self) :        
        # Return X min, xmax, y min, y max orientation indicators
        
        if (self._nifti_volume is None or self._SliceAxis is None) :
            return (('', '', '', ''))        
        
        displayAxisIndex = -1
        if self._SliceAxis == 'X' : 
            displayAxisIndex = 0
        elif self._SliceAxis == 'Y' : 
            displayAxisIndex = 1
        elif self._SliceAxis == 'Z' : 
            displayAxisIndex = 2
        else:
            return (('Invalid Axis Index', 'Invalid Axis Index', 'Invalid Axis Index', 'Invalid Axis Index'))        
        
        try :
            contourorientation = self._nifti_volume.getContourOrientation ()
            orientation = self._nifti_volume.getOrientationString (contourorientation[displayAxisIndex])
        except:
            return (('', '', '', ''))
        #xmin, xmax, ymin, ymax
        
        if (orientation == "Axial") :
            return (('R', 'L', 'A', 'P'))
        if (orientation == "Coronal") :
            return (('R', 'L', 'S', 'I'))
        if (orientation == "Sagittal") :
            return (('A', 'P', 'S', 'I'))
        return (('?', '?', '?', '?'))
    
    def isNiftiDataLoaded (self) :
        return self._nifti_volume  is not None
   
    def _setCoordinate (self, coord) :
        self._Coordinate = coord.copy ()
        if (self._nifti_volume is None) :
            self._Coordinate.clipToShape ([0,0,0])
        else:
            self._Coordinate.clipToShape (self._nifti_volume.getSliceDimensions ())        
        self._scrollZoomedViewToCoordinateSelection (self._Coordinate)
        
              
    def setCoordinate (self, Coordinate, callChangeListener = True) :
        if (self._Coordinate is None or self._Coordinate.getCoordinate() != Coordinate.getCoordinate ()) :
            self._setCoordinate (Coordinate)                                       
            if (callChangeListener) : 
                self._callSliceListener (self._Coordinate.getXCoordinate (), self._Coordinate.getYCoordinate (), self._Coordinate.getZCoordinate (), False)  
    
        
 
          
    def getCoordinate (self)  :
        return self._Coordinate
        
    def setDrawCoordinateIndicator (self, val) :
         self._drawCoordinateIndicator = val
         
    def getDrawCoordinateIndicator (self) :
        return self._drawCoordinateIndicator 
    
    def _getPixmap (self) :       
        pixmap = self.SliceViewer.pixmap.transformed(self._ViewPortTransformation.getQTransformation ())        
        return pixmap
    
    def _getPixmapWidthHeight (self) :
        pixmap = self._getPixmap()
        return pixmap.width (), pixmap.height ()
        
    def _getPixmapHeight (self) :
        return self._getPixmap().height ()
        
    def _getPixmapWidth (self) :
        return self._getPixmap().width ()
        
    def disableUpdateSlice (self):
        self._UpdateSliceEnabled = False
    
    def isUpdateSliceEnabled (self) :
        return self._UpdateSliceEnabled
    
    def enableUpdateSlice (self, UpdateSlice = False):
        self._UpdateSliceEnabled = True
        if (UpdateSlice) :
             self._UpdateSlice ()        
        
    def _hounsFieldVisSettingsChanged (self):
        self._UpdateSlice ()        
                
    def setHounsfieldVisSettings (self, settings) :
        if (self._hounsfieldVisSettings != None) :
            self.removeHounsfieldVisSettings () 
        self._hounsfieldVisSettings = settings
        self._hounsfieldVisSettings.addChangeListener (self._hounsFieldVisSettingsChanged)

    def UpdateSlice (self) :
        self._UpdateSlice()
        self.update ()                
        
    def _visSettingsChanged (self):
        if (self._overlayNiftiVolumeMask is not None and self._overlayNiftiVolumeMaskColorTable is not None) :
            self._UpdateSlice()
        self.update ()                
        
    def setVisSettings (self, settings) :
        if (self._visSettings != None) :
            self.removeVisSettings () 
        self._visSettings = settings
        self._visSettings.addListener (self._visSettingsChanged)
    
    def removeVisSettings (self) :                
        if (self._visSettings != None) :
            self._visSettings.removeListener (self._visSettingsChanged)    
            self._visSettings = None

    def removeHounsfieldVisSettings (self) :        
        if (self._hounsfieldVisSettings != None) :
            self._hounsfieldVisSettings.removeChangeListener (self._hounsFieldVisSettingsChanged)
            self._hounsfieldVisSettings = None
        
    def setROIBoundingBoxResizeable (self, setting):
        if (setting != self._ROIBBResize):
            self._ROIBBResize = setting
            #print ("setROIBoundingBoxVisible.update")
            self.update ()
            
    def setROIBoundingBoxVisible (self, setting):
        if (setting != self._ROIBBVisilbe):
            self._ROIBBVisilbe = setting
            #print ("setROIBoundingBoxVisible.update")
            self.update ()
            
    def addZoomListener (self, callback):
        if (callback not in self._zoomListener) :
            self._zoomListener.append (callback)
    
    def getPrecentShown (self):        
        if (not self._zoom.isInitalized ()) :            
            return 1.0
        else:
            zArea = self._zoom.width () * self._zoom.height ()
            width, height = self._getPixmapWidthHeight ()
            tArea =  width * height 
            return float (zArea) / float (tArea)        
    
    
    def _callZoomListener (self) :                
        if (self._UIZoomScrollX is not None) :            
            try:
                self._UIZoomScrollX.valueChanged.disconnect  (self._zoomSliderMovedX)                
                Connected=True
            except:
                Connected=False
            if not self._zoom.isInitalized () :
                self._UIZoomScrollX.setVisible (False)
            else:
                maximum = self._getPixmapWidth () - self._zoom.width ()
                self._UIZoomScrollX.setVisible (maximum > 0)
                self._UIZoomScrollX.setMinimum (0)
                self._UIZoomScrollX.setMaximum (maximum)
                self._UIZoomScrollX.setValue (self._zoom.getMinX())
                self._UIZoomScrollX.setEnabled (maximum > 0)
            if Connected :
                self._UIZoomScrollX.valueChanged.connect  (self._zoomSliderMovedX)                
        if (self._UIZoomScrollY is not None) :
            try:
                self._UIZoomScrollY.valueChanged.disconnect  (self._zoomSliderMovedY)
                Connected=True
            except:
                Connected=False
            if not self._zoom.isInitalized () :
                self._UIZoomScrollY.setVisible (False)
            else:
                maximum = self._getPixmapHeight () - self._zoom.height ()
                self._UIZoomScrollY.setVisible (maximum > 0)                                
                self._UIZoomScrollY.setMinimum (0)
                self._UIZoomScrollY.setMaximum (maximum)
                self._UIZoomScrollY.setValue (self._zoom.getMinY())            
                self._UIZoomScrollY.setEnabled (maximum > 0)
            if Connected :
                self._UIZoomScrollY.valueChanged.connect  (self._zoomSliderMovedY)
        for i in self._zoomListener :
            i (self)         
    
    def setMouseTracker (self, boolval) :
        """if (boolval) :
            print ("Set Mouse Tracking = TRUE")
        else:
             print ("Set Mouse Tracking = False")"""
        return self.setMouseTracking (boolval)
        
                            
    def _zoomSliderMovedX (self, num) :    
        if (self._disableZoomSlider) :
            return
        if (not self.isNiftiDataLoaded ()) :
            return
        if (self._UIZoomScrollX is not None and self._UIZoomScrollX.isVisible ()):
            if (self._zoom.isInitalized ()) :                             
                self._zoom.scrollX(num)            
        
        self.update ()   
        
    def _zoomSliderMovedY (self, num) :    
        if (self._disableZoomSlider) :
            return
        if (not self.isNiftiDataLoaded ()) :
            return
        if (self._UIZoomScrollY != None and self._UIZoomScrollY.isVisible ()):            
            if (self._zoom.isInitalized ()) :                             
                self._zoom.scrollY (num)
        #print ("_zoomSliderMoved.update")                
        self.update ()   
        
     
    def mouseWheelZoom (self, dstep) :        
        if (not self.isNiftiDataLoaded ()) :            
            return                
        self.setSelected (True)
        pmWidth, pmHeight = self._getPixmapWidthHeight ()
        pmWidth -= 1
        pmHeight -= 1
        if (self._zoom.isInitalized ()):
            left = self._zoom.getOrigMinX ()
            top = self._zoom.getOrigMinY()
            right = self._zoom.getOrigMaxX()
            bottom = self._zoom.getOrigMaxY()
        else :
            left = 0
            top = 0
            right = pmWidth 
            bottom = pmHeight             
        Changed = False
        width = right - left
        height = bottom - top        
        two_step = 8 * dstep
        if height > width :
            ratio = float (height) / float (width)            
            newwidth = min (max (width - two_step, 5), pmWidth)
            if newwidth != width :
                width = newwidth
                height = ratio * width
                height = int (min (max (height, 5), pmHeight))
                Changed = True
        else:
            ratio = float (width) / float (height)
            newheight = min (max (height - two_step, 5), pmHeight)
            if newheight != height :
                height = newheight
                width = ratio * height
                width = int (min (max (width, 5), pmWidth))
                Changed = True
                          
        if Changed :            
            if width >= pmWidth and height >= pmHeight :
                self._zoom.clear ()            
                self._callZoomListener ()
                self.update () 
            else:
                if self._zoom.hasCenterTarget () :
                    cX, cY = self._zoom.getCenterTarget ()
                else:
                    cX = int ((left + right) / 2)
                    cY = int ((top + bottom) / 2)
                    self._zoom.setCenterTarget (cX, cY)
                left, right = self._getCenterZoomWindow (cX, width, pmWidth)
                top, bottom = self._getCenterZoomWindow (cY, height, pmHeight)                
                self.zoomIn (left, top, right, bottom, calledFromClick = False, calledFromWheel = True)
                                                            
        
    def zoomIn (self, x1, y1, x2,y2, calledFromClick = False, calledFromWheel = False) : # screen Coord to Zoom on
        #print (((x1,y1),(x2,y2)))   
        #x1, y1  = self.transformPointFromScreenToImage (x1,y1)
        #x2, y2 = self.transformPointFromScreenToImage (x2,y2)   
        #self._oldzoomstate = None
        if (not self.isNiftiDataLoaded ()) :            
            return
        
        if (self._Coordinate is None) :
             return 
                
        if (x1 < x2 - 2 and y1 < y2 - 2) :            
            self._zoom.setZoom (x1,y1, x2, y2) 
            if not calledFromClick and not calledFromWheel : 
                self._zoom.setCenterTarget (int ((x1 + x2) / 2), int ((y1 + y2) / 2))
            if (not self._isZoomed() and not calledFromClick): # Assume viewing on XY Axis
                x1, y1  = self.transformPointFromScreenToImage (x1,y1)
                x2, y2 = self.transformPointFromScreenToImage (x2,y2)                   
                Z = self._Coordinate.getZCoordinate ()
                X = int ((x1 + x2) / 2)
                Y = int ((y1 + y2) / 2)
                self.zoomInClick ( X, Y, Z)                
                                
            width, height = self._reInitalizeZoom ()            
            self._ZoomInitlizedDim = ((self.width (), self.height ()), (width, height))
            if (not self._zoom.isInitalized ()) : # if zooming and does not fill view                                                                        
                _hVoxelScale, _vVoxelScale = self._ViewPortTransformation.getTransformedVoxelScale ((float (self.getHVoxelScale ()), float (self.getVVoxelScale ())))
                PixmapApectRatio = (_hVoxelScale * self._getPixmapWidth ()) / (_vVoxelScale *  self._getPixmapHeight ())
                drawWidth = int (PixmapApectRatio * self.height())                                        
                if drawWidth <= self.width() :                                                  
                    if (drawWidth < 1) :
                        drawWidth = 1
                    self._DrawOffSetX = int ((self.width() - drawWidth) / 2)
                    self._DrawOffSetY = 0                                    
                    self._DrawWidthScale = float (drawWidth) / float (self._getPixmapWidth ())            
                    self._DrawHeightScale = float (self.height()) /  float (self._getPixmapHeight ())
                else:                   
                    PixmapApectRatio =  (_vVoxelScale *  self._getPixmapHeight ()) / (_hVoxelScale  * self._getPixmapWidth ())
                    drawHeight = int (PixmapApectRatio * self.width())
                    if (drawHeight < 1) :
                        drawHeight = 1
                    self._DrawOffSetY = int ((self.height() - drawHeight) / 2)
                    self._DrawOffSetX = 0
                    self._DrawWidthScale = float (self.width ()) / float (self._getPixmapWidth ())
                    self._DrawHeightScale = float (drawHeight) /  float (self._getPixmapHeight ())                     
            self._callZoomListener ()   
            self.update ()
            return

    
      
    def _getCenterZoomWindow  (self, cX, width, right, clearZoom = None) :
        edgeLeft  = int (width / 2)
        edgeRight = int (width - 1 - edgeLeft)        
        nL = cX - edgeLeft
        nR = cX + edgeRight
        if (nL < 0):
            nR += -nL
            nL = 0
        if (nR > right):
            nL -= nR - right
            nR = right
            if nL < 0 :
                nL = 0 
                if clearZoom is not None : 
                    clearZoom += 1
        if clearZoom is not None :
            return nL, nR, clearZoom
        return nL, nR
        
    def zoomInClick (self, X, Y, Z)  :               
        if (not self.isNiftiDataLoaded ()) :
                return
        #self._oldzoomstate = None
        if self._SliceAxis == 'X' : 
            cX = Y
            cY = Z
        elif self._SliceAxis == 'Y' :    
            cX = X
            cY = Z
        elif self._SliceAxis == 'Z' :
            cX = X
            cY = Y        
        else:
            print ("Error")
            
        if (self._zoom.isInitalized ()):
            left = self._zoom.getOrigMinX()
            top = self._zoom.getOrigMinY()
            bottom = self._zoom.getOrigMaxY()
            right = self._zoom.getOrigMaxX()
        else :
            left = 0
            top = 0
            right,bottom = self._getPixmapWidthHeight ()
            right -= 1
            bottom -= 1
        width  = right - left + 1
        height = bottom - top + 1 
        if (width >= 5 or height >= 5) :
            edge = int (2.0*width / 5.0)
            width -= edge    
            left, right = self._getCenterZoomWindow (cX, width, right)
                    
            edge = int (2.0*height / 5.0)
            height -= edge
            top, bottom = self._getCenterZoomWindow (cY, height, bottom)                      
        self._zoom.setCenterTarget (cX, cY)
        self.zoomIn (left, top, right, bottom, calledFromClick = True)

    def zoomOut (self) :        
        if (not self.isNiftiDataLoaded ()) :
                return
        if (self._zoom.isInitalized ()) :            
            left = self._zoom.getOrigMinX()
            top = self._zoom.getOrigMinY()
            bottom = self._zoom.getOrigMaxY()
            right = self._zoom.getOrigMaxX()      
            width  = int (right - left + 1)
            height = int (bottom - top + 1)
            
            clearZoom = 0
            edgeGrowth = int (max ((5.0 * width / 3.0), 1))
            left, right, clearZoom = self._getCenterZoomWindow (int ((left+right)/2), edgeGrowth, self._getPixmapWidth () -1, clearZoom)
            
            edgeGrowth = int (max ((5.0 * height / 3.0), 1))
            top, bottom, clearZoom = self._getCenterZoomWindow (int ((top+bottom)/2), edgeGrowth, self._getPixmapHeight () -1, clearZoom)
                        
            if clearZoom == 2 :
                self._zoom.clear ()            
                self._callZoomListener ()
                #print ("zoomOut.update")
                self.update ()
            else:
                self.zoomIn (left, top, right, bottom, calledFromClick = True)
        
    def getSliceAxis (self) :
        return self._SliceAxis
      
    def transformPointFromScreenToImage (self, x, y):         
        x,y = ((x - self._DrawOffSetX) / self._DrawWidthScale, (y - self._DrawOffSetY) / self._DrawHeightScale)        
        x,y = self._zoom.convertFromScreenToImage (x,y)  
        x,y = self._ViewPortTransformation.transformScreenToImage (x,y, self.SliceViewer.pixmap.width (), self.SliceViewer.pixmap.height ())
        return (int (x), int (y)) 
         
    def transformPointFromScreenToImageOrientation (self, x, y, width, height):        
        x,y = self._ViewPortTransformation.transformScreenToImage (x,y, width, height)
        return (int (x), int (y)) 
    
    def transformPointFromImageOrientationToImageScreen (self, x, y, width, height):        
        x,y = self._ViewPortTransformation.transformImageToScreen (x,y, width, height)
        return (int (x), int (y)) 
    
    def transformPointFromImageToScreen (self, x, y):        
        x,y = self._ViewPortTransformation.transformImageToScreen (x,y, self.SliceViewer.pixmap.width (), self.SliceViewer.pixmap.height ())
        x,y = self._zoom.convertFromImageToScreen (x,y) 
        x,y = (x * self._DrawWidthScale + self._DrawOffSetX, y * self._DrawHeightScale + self._DrawOffSetY)         
        return (int (x), int (y))                     
        
    def getSliceShape (self):
        return self._getSelectedSliceDataShape (Mask = self.getVolumeMask ())
      
     
    def getNIfTIVolume (self) :
        return self._nifti_volume
      
    def _getSliceData (self, index, Mask = None):        
        if Mask is None :
            volume = self._nifti_volume
        else:
            volume = Mask
            
        if (index < 0) :
            print ("Error Slice coordinate is less than zero (index = %d).  Setting slice Coordiante to 0." % (index))
            index = 0
        if self._SliceAxis == 'X' :
            if (index >= volume.getXDimSliceCount ()) :
                print ("Error Slice X coordiante too large adjusting. (Invalid Coordinate = %d adjusting to %d)" % (index,  volume.getXDimSliceCount () - 1))
                index = volume.getXDimSliceCount () - 1                   
            data =  volume.getImageData (X=index)
        elif self._SliceAxis == 'Y' :
            if (index >= volume.getYDimSliceCount ()) :
                print ("Error Slice Y coordiante too large adjusting. (Invalid Coordinate = %d adjusting to %d)" % (index,  volume.getYDimSliceCount () - 1))
                index = volume.getYDimSliceCount () - 1                    
            data =  volume.getImageData (Y=index)
        elif self._SliceAxis == 'Z' :
            if (index >= volume.getZDimSliceCount ()) :
                print ("Error Slice Z coordiante too large adjusting. (Invalid Coordinate = %d adjusting to %d)" % (index,  volume.getZDimSliceCount () - 1))
                index = volume.getZDimSliceCount () - 1            
            data =  volume.getImageData (Z=index)
        return data
    
    def _getSelectedSliceDataShape (self, Mask = None) :
        if (self._Coordinate is None) :
            print ("Error: Coordinate is set to None returning nothing")
            return None
        if Mask is None :
             dx, dy, dz = self._nifti_volume.getSliceDimensions ()
        else:
             dx, dy, dz = Mask.getSliceDimensions ()
        if self._SliceAxis == 'X' :
           return (dy, dz)
        elif self._SliceAxis == 'Y' :
            return (dx, dz)     
        elif self._SliceAxis == 'Z' :
            if not  self.areAxisProjectionControlsEnabledForAxis () :               
                 return (dx, dy)     
            else:
                if Mask is not None :
                    niftiMem = Mask
                else:
                    niftiMem = self._nifti_volume
                SliceData, _ = self._AxisProjectionControls.getAxialViewProjection (niftiMem)
                if SliceData is None or len (SliceData["NIfTI"]) == 0 :
                    return (dx, dy)  
                else:
                    SliceData = SliceData["NIfTI"]
                    projectionMask = SliceData["Mask"] 
                    return projectionMask.shape
                
    def _getSelectedSliceData (self, Mask = None) :
        if (self._Coordinate is None) :
            print ("Error: Coordinate is set to None returning nothing")
            return None, None
        if self._SliceAxis == 'X' :
            return self._getSliceData (self._Coordinate.getXCoordinate (), Mask = Mask), None
        elif self._SliceAxis == 'Y' :
            return self._getSliceData (self._Coordinate.getYCoordinate (), Mask = Mask), None            
        elif self._SliceAxis == 'Z' :
            if not self.areAxisProjectionControlsEnabledForAxis ():   
                return self._getSliceData (self._Coordinate.getZCoordinate (), Mask = Mask) , None   
            else:                
                ReturnMem, ClipMask = self._AxisProjectionControls.getNIfTIProjection (self._Coordinate, self._nifti_volume, Mask = Mask)
                return ReturnMem, ClipMask
                    
    
    def _doesVolumeHaveColorData (self) :
       try :
           if self._nifti_volume is None :
               return False
           return self._nifti_volume.hasColorImageData ()
       except:
           return False
        
    def _getSelectedSliceColorData (self) :
        if (self._Coordinate is None) :
            print ("Error: Coordinate is set to None returning nothing")
            return None     
        try :
            if self._SliceAxis == 'X' :
                xCord = self._Coordinate.getXCoordinate ()
                return self._nifti_volume.getColorImageData()[xCord,:,:]
            elif self._SliceAxis == 'Y' :
                yCord = self._Coordinate.getYCoordinate ()    
                return self._nifti_volume.getColorImageData()[:,yCord,:]
            elif self._SliceAxis == 'Z' :
                zCord = self._Coordinate.getZCoordinate ()
                if not self.areAxisProjectionControlsEnabledForAxis ():   
                    return self._nifti_volume.getColorImageData()[:,:,zCord]
                else:
                    # possible bug here may need to use ROI Pojection.
                    
                    colorMem = self._nifti_volume.getColorImageData()
                    Red, clipMask = self._AxisProjectionControls.getNIfTIProjection (self._Coordinate, self._nifti_volume, Mask = None, UseCache = False, ArrayMem = colorMem[...,0])
                    Green, _ = self._AxisProjectionControls.getNIfTIProjection (self._Coordinate, self._nifti_volume, Mask = None, UseCache = False, ArrayMem = colorMem[...,1])
                    Blue, _ = self._AxisProjectionControls.getNIfTIProjection (self._Coordinate, self._nifti_volume, Mask = None, UseCache = False, ArrayMem = colorMem[...,2])
                    mask = np.logical_not (clipMask)
                    Red[mask] = 0
                    Green[mask] = 0
                    Blue[mask] = 0
                    #Red = np.expand_dims(Red, -1)
                    #Green = np.expand_dims(Green, -1)
                    #Blue = np.expand_dims(Blue, -1)
                    ReturnMem = np.stack ([Red, Green, Blue], axis = 2)
                    return ReturnMem
        except:
            return (np.zeros (self.getSliceShape (), dtype= np.uint8))
    
    def _getWindowedSliceData (self, bwindex = None):
        data, clipMask = self._getSelectedSliceData ()
        if data is None and clipMask is not None:
            return np.zeros (clipMask.shape, dtype=np.int)
        data = np.copy (data)     
        if clipMask is not None :
            if bwindex is not None :
                bwindex = np.logical_and (clipMask, bwindex)
            else:
                bwindex = clipMask
        if (self._hounsfieldVisSettings is None or self._hounsfieldVisSettings.maximizeContrastAcross5To95PrecentInVolume ()) :
            minD, maxD = self._nifti_volume.getVolumeMinMax ()                        
            if minD is not None and maxD is not None :
                VRange = maxD - minD            
                maxD = (0.95 * float (VRange)) + minD
                minD = (0.05 * float (VRange)) + minD  
        elif (self._hounsfieldVisSettings is None or self._hounsfieldVisSettings.maximizeContrastAcross5To95PrecentInSlice ()) :
            if (bwindex is not None) :
                minD = np.min (data[bwindex])            
                maxD = np.max (data[bwindex])
            else:
                minD = np.min (data)            
                maxD = np.max (data)
            VRange = maxD - minD
            maxD = (0.95 * float (VRange)) + minD
            minD = (0.05 * float (VRange)) + minD
        elif (self._hounsfieldVisSettings is None or self._hounsfieldVisSettings.maximizeWithinSliceContrast ()) :
            if (bwindex is not None) :
                minD = np.min (data[bwindex])            
                maxD = np.max (data[bwindex])
            else:
                minD = np.min (data)            
                maxD = np.max (data)
        else:
            minD, maxD  = self._hounsfieldVisSettings.getCustomHounsfieldRange (ClipToDataset = False)                
            np.clip (data, minD, maxD, out = data)
        
        if clipMask is not None :
            data[np.logical_not (clipMask)] = minD
        
        if maxD is not None and maxD is not None :
            maxD = int (maxD)
            minD = int (minD)
            m = maxD - minD
            if data.dtype != np.int :
                data = data.astype (np.int)
            if m == 0 : 
               data[...] = 0
            else :
               data = (((data - minD).astype (float) / float(m)) * float (255.0)).astype(int)
               np.clip(data, 0,255, out = data)
        return data
        
    def _UpdateSlice (self, callback = True) : 
        if (not self.isUpdateSliceEnabled()) :
            return
        self._updateSliderValue () 
        if self._Coordinate is None :
             self.SliceViewer = NpQtImageViewer ()                
             self.SliceViewer.SetSliceNPDataBlack ( self.height (), self.width  ())   
             self._ROIAxisObjectLit = []
        else:    
            windowBWComponentOfColorData = self._hounsfieldVisSettings.windowBWComponentOfColorData ()            
            if self.SliceViewer is None:
                self.SliceViewer = NpQtImageViewer ()  
                
            if self._doesVolumeHaveColorData () :
               data = None
               colorData = np.copy (self._getSelectedSliceColorData ())
               if windowBWComponentOfColorData :
                    bwindex = colorData[:,:,0] == colorData[:,:,1]
                    bwindex[colorData[:,:,0] != colorData[:,:,2]] = False
                    if np.any (bwindex) :
                        data = self._getWindowedSliceData (bwindex = bwindex)                        
                        data = data.astype (colorData.dtype)
                        for channel in range (3) :
                            colorData[:,:,channel][bwindex] = data[bwindex]
            else:
                colorData = None
                data = self._getWindowedSliceData ()
                
            if (self.getVolumeMask () is not None) :
                mask, clipMask  = self._getSelectedSliceData(Mask = self.getVolumeMask ())
                if mask is None :
                    self.SliceViewer.SetSliceNPDataBlack ( self.height (), self.width  ())   
                    self._ROIAxisObjectLit = []
                else:
                    if clipMask is not None :
                        mask[np.logical_not (clipMask)] = 0
                    self.SliceViewer.SetSliceNPDataBW (data, mask = mask , colorOverlay = None, colorOverlayTranspValue = (self._visSettings.getUITransparency()/255.0), ColorTable = self._overlayNiftiVolumeMaskColorTable, ColorData = colorData)   
            elif (self.getColorOverlay () is not None) :
                self.SliceViewer.SetSliceNPDataBW (data, mask = None, colorOverlay = self.getColorOverlay(), colorOverlayTranspValue = self.getColorOverlayTranspValue (), ColorData = colorData)                 
            else:
                self.SliceViewer.SetSliceNPDataBW (data, mask = None, colorOverlay = None, ColorData = colorData)                   
        if (callback) :
            #print ("_updateSlice.update")
            self.update ()       
    
    def getColorOverlay (self):
        return self._colorOverlay
        
    def getColorOverlayTranspValue (self) :
        return self._colorOverlayTranspValue
    
    def setColorOverlay (self, overlay, transpValue = 0.5, DisableUpdateSlice = False) :
        transpValue = min (max (0.0,transpValue), 1.0)    
        if (transpValue != self._colorOverlayTranspValue) or (self._colorOverlay is None and overlay is not None) or (self._colorOverlay is not None and overlay is None) or (overlay is not None and self._colorOverlay is not None and self._colorOverlay.shape != overlay.shape) or (overlay is not None and self._colorOverlay is not None and np.any (self._colorOverlay - overlay)) :
            if overlay is not None :
                self._colorOverlay = np.copy (overlay)
            else:
                self._colorOverlay = None
            self._colorOverlayTranspValue = transpValue
            if not DisableUpdateSlice :
                self._UpdateSlice()
        
    def getROIObjectList (self) :
        return self._ROIAxisObjectList
    
    def getSharedROIMask (self) :
        return self._SharedROIMask
    
    #( self.SelectedCoordinate, self.NiftiVolume, "Z")    
    def setROIObjectList (self, ROIDictionary, SetCoordinate = None):
        if (SetCoordinate is not None) :
            self._setCoordinate (SetCoordinate)
        if (ROIDictionary is None) :
            self._ROIAxisObjectList = []           
            self._SelectedObjectLst = []
            self._ROIDefs  =  None
            self._SharedROIMask = None
            return
        elif ROIDictionary.getROIFileHandle () is None :
           self._ROIAxisObjectList = []           
        elif (self._Coordinate is not None) :                        
            self._ROIAxisObjectList = ROIDictionary.getROIAxisList (self._Coordinate, self._SliceAxis, niftivolume = self.getNIfTIVolume(), AxisProjectionControls = self._AxisProjectionControls)
            if self._SliceAxis == "Z" :
                ROINameList = ROIDictionary.getROIListWithDeepGrowAnnotations (self._Coordinate.getZCoordinate ())
                for ROIName in ROINameList :
                    obj = ROIDictionary.getROIObject (ROIName)
                    if obj not in ROINameList :
                         self._ROIAxisObjectList.append (obj)
        else:
            self._ROIAxisObjectList = []
            print ("Warring ROI Axis Object not set self._Coordinate is None")
        self._SelectedObjectLst = ROIDictionary.getSelectedROIObjectLst ()
        self._ROIDefs = ROIDictionary.getROIDefs ()                
        self._SharedROIMask = ROIDictionary.getSharedVolumeCacheMem ()                
        
    def setTempROIObjectList (self, objLst):
        self._SelectedObjectLst = copy.copy (objLst)
         
    def _updateSliderValue (self) :
        if (not self.isNiftiDataLoaded ()) :
               return
        if (self._UISlider != None):            
            val = None
            if (self._Coordinate is not None) :
                if self._SliceAxis == 'X' :                
                    val = self._Coordinate.getXCoordinate ()
                elif self._SliceAxis == 'Y' :
                    val = self._Coordinate.getYCoordinate ()
                elif self._SliceAxis == 'Z' :
                    val = self._Coordinate.getZCoordinate ()            
            if (val is not None) :
                self._UISlider.setEnabled (True)
                self._UISlider.setValue (self._UISlider.maximum () - val)                
            else:
                self._UISlider.setEnabled (False)
    
    def _UISliderValueChanged (self) :           
        if (not self.isNiftiDataLoaded ()) :
                return     
        if (self._Coordinate is None) :
            return        
        axisCoord = self._UISlider.maximum () - self._UISlider.value ()   
        newXCoordinate, newYCoordinate, newZCoordinate = self._Coordinate.getCoordinate ()
        newXCoordinate = int (newXCoordinate)
        newYCoordinate = int (newYCoordinate)
        newZCoordinate = int (newZCoordinate)
        #newXCoordinate = self._Coordinate.getXCoordinate ()
        #newYCoordinate = self._Coordinate.getYCoordinate ()
        #newZCoordinate = self._Coordinate.getZCoordinate ()        
        oldCoord = (newXCoordinate, newYCoordinate, newZCoordinate)
        if self._SliceAxis == 'X' :             
            newXCoordinate = axisCoord
        elif self._SliceAxis == 'Y' :                
            newYCoordinate = axisCoord
        elif self._SliceAxis == 'Z' :
            newZCoordinate = axisCoord
        newCoord = (newXCoordinate, newYCoordinate, newZCoordinate)        
        if (newCoord != oldCoord) :       
            self.setSelected (True)
            self._callSliceListener (newXCoordinate, newYCoordinate, newZCoordinate, False)        

    def getMaxSliceCount (self) :
        if self._nifti_volume is None :
            return 0
        elif self._SliceAxis == 'X' :    
            maxV = self._nifti_volume.getXDimSliceCount () 
        elif self._SliceAxis == 'Y' :    
            maxV = self._nifti_volume.getYDimSliceCount () 
        elif self._SliceAxis == 'Z' :    
            maxV = self._nifti_volume.getZDimSliceCount () 
        else:
            print ("Error")
        return maxV
    
    def getSliceCoordinate (self, sliceNumber) :
        if self._Coordinate is None :            
            return (0,0,0)        
        newXCoordinate, newYCoordinate, newZCoordinate = self._Coordinate.getCoordinate ()
        newXCoordinate = int (newXCoordinate)
        newYCoordinate = int (newYCoordinate)
        newZCoordinate = int (newZCoordinate)
        if self._SliceAxis == 'X' :
            return (sliceNumber, newYCoordinate, newZCoordinate)
        elif self._SliceAxis == 'Y' :    
            return (newXCoordinate, sliceNumber, newZCoordinate)
        elif self._SliceAxis == 'Z' :
            return (newXCoordinate, newYCoordinate, sliceNumber)
            
    def getSliceNumber (self) :
        if self._Coordinate is None :
            print ("Coordiante not defined")
            return 0
        elif self._SliceAxis == 'X' : 
            return self._Coordinate.getXCoordinate ()
        elif self._SliceAxis == 'Y' :    
            return self._Coordinate.getYCoordinate ()
        elif self._SliceAxis == 'Z' :            
            return self._Coordinate.getZCoordinate ()    
        
    def getAxisProjectionSliceNumber (self, ReturnIsCached = False, SliceNumber = None, Coordinate = None) :  
        if Coordinate is None :
            Coordinate = self._Coordinate
        testCord = tuple (Coordinate.getCoordinate ())
        if SliceNumber is None :
            SliceNumberOffset = 0
        else:
            SliceNumberOffset = SliceNumber - testCord[2]
        axisProjectionState = self._AxisProjectionControls.getProjectionState ()
        if self._coordinateProjectionSliceNumberCacheProjectionState == axisProjectionState :
            first = True
            for cachedCoord in self._coordinateProjectionSliceNumberCache :
                if (testCord == cachedCoord) :  
                    if not first :
                        self._coordinateProjectionSliceNumberCache = [testCord]
                    if ReturnIsCached :
                       return self._coordinateProjectionSliceNumberCachedValue + SliceNumberOffset, True
                    return self._coordinateProjectionSliceNumberCachedValue + SliceNumberOffset
                first = False
        self._coordinateProjectionSliceNumberCacheProjectionState = axisProjectionState 
        self._coordinateProjectionSliceNumberCache = [testCord]
        self._coordinateProjectionSliceNumberCachedValue = self._AxisProjectionControls.getProjectedSliceForWorldCoordinate (Coordinate, self._nifti_volume)  
        if ReturnIsCached :
            return self._coordinateProjectionSliceNumberCachedValue + SliceNumberOffset, False
        return self._coordinateProjectionSliceNumberCachedValue + SliceNumberOffset

        
    def isAxisProjectionRotationEnabled (self) :
        return  self._AxisProjectionControls is not None and self.getSliceAxis () == "Z" and self._AxisProjectionControls.isAxisRotated () 
        
    def transformPlaneCoordinateToVolumeCoordinates (self, mX, mY):
        if not self.isAxisProjectionRotationEnabled () :
            return mX, mY, self.getSliceNumber ()
        projectedSliceNumber, IsCached = self.getAxisProjectionSliceNumber (ReturnIsCached = True)
        testInPlanePoint = (mX, mY)
        if IsCached :
            if self._trasformPTCache == testInPlanePoint :
                return self._trasformPTCacheValue
        self._trasformPTCache = testInPlanePoint 
        points = self._AxisProjectionControls.getWorldCoordinateForPlaneCoordinate (mX, mY, projectedSliceNumber, self._nifti_volume)     
        #print (projectedSliceNumber)
        dx, dy, dz = self._nifti_volume.getSliceDimensions ()
        dx, dy, dz  = int (min (max(points[0],0), dx -1)), int (min(max (points[1],0), dy - 1)), int(min(max (points[2],0), dz - 1))
        NewPoint  = (dx, dy, dz)
        if len (self._coordinateProjectionSliceNumberCache) < 2 :
            self._coordinateProjectionSliceNumberCache.append (NewPoint)
        else:
            self._coordinateProjectionSliceNumberCache[1] = (NewPoint)
        self._coordinateProjectionSliceNumberCachedValue = projectedSliceNumber 
        self._trasformPTCacheValue = NewPoint
        return NewPoint
    
    
    def getProjectDataset (self) :
        return self._projectDataset
    
    def setNiftiDataSet (self, niftiVolume, ProjectDataset):
        if (self._nifti_volume != niftiVolume) :
            if self._nifti_volume is not None :
                self._nifti_volume.removeChangeListener (self._UpdateSlice)        
            self._nifti_volume = niftiVolume   
            self._projectDataset = ProjectDataset 
            if (self._nifti_volume is None) :
                self._Coordinate = None        
            else:
                self._nifti_volume.addChangeListener (self._UpdateSlice)        
        
    def initNiftiDataSet (self, coordinate, sliceaxis, callback = True, slider = None, zoomScrollX = None, zoomScrollY = None):                        
        self._MouseMoveLineList = []
        self._DrawHeightScale = 1
        self._DrawWidthScale = 1
        self._DrawOffSetX = 0
        self._DrawOffSetY = 0       
        if coordinate != None :
            self._setCoordinate (coordinate)
        else:
            self._Coordinate = None
        self._SliceAxis = sliceaxis
        self.clearTemporaryDrawLine ()
        self._TemporaryLineColor = QtGui.QColor (0,0,0)
        
        if (self._UIZoomScrollX != zoomScrollX) :
            if (self._UIZoomScrollX is not None) :
                try:
                    self._UIZoomScrollX.valueChanged.disconnect  (self._zoomSliderMovedX)
                except:
                    pass
            self._UIZoomScrollX = zoomScrollX
            if (self._UIZoomScrollX is not None) :
                try : 
                    self._UIZoomScrollX.valueChanged.connect  (self._zoomSliderMovedX)
                except:
                    pass
       
        if (self._UIZoomScrollY != zoomScrollY) :
            if (self._UIZoomScrollY is not None):
                try :
                    self._UIZoomScrollY.valueChanged.disconnect  (self._zoomSliderMovedY) 
                except:
                    pass
            self._UIZoomScrollY = zoomScrollY 
            if (self._UIZoomScrollY is not None) :
                try:
                    self._UIZoomScrollY.valueChanged.connect  (self._zoomSliderMovedY)
                except:
                    pass
        if (slider != self._UISlider):
            if (self._UISlider is not None):
                try:
                    self._UISlider.valueChanged.disconnect (self._UISliderValueChanged)
                except: 
                    pass
            self._UISlider = slider            
            if (self._UISlider is not None) :
                try :
                    self._UISlider.valueChanged.connect (self._UISliderValueChanged)
                except:
                    pass
        if (self._UISlider is not None) :
            try :
                self._UISlider.valueChanged.disconnect (self._UISliderValueChanged)
            except:
                pass
            self._UISlider.setMinimum (0)            
            maxV = self.getMaxSliceCount () - 1            
            self._UISlider.setMaximum (maxV)
            self._UISlider.setEnabled (maxV > 0)       
            try :
                self._UISlider.valueChanged.connect (self._UISliderValueChanged)
            except: pass
        
        if (not self.isNiftiDataLoaded ()) :
            self.SliceViewer = None
            self._hVoxelScale = 1
            self._vVoxelScale = 1
        else :
           
            if self._SliceAxis == 'X' :                            
                self._hVoxelScale = self._nifti_volume.getNormalizedVoxelSize()[1]
                self._vVoxelScale = self._nifti_volume.getNormalizedVoxelSize()[2]
            elif self._SliceAxis == 'Y' :                
                self._hVoxelScale = self._nifti_volume.getNormalizedVoxelSize()[0]
                self._vVoxelScale = self._nifti_volume.getNormalizedVoxelSize()[2]
            elif self._SliceAxis == 'Z' :                                
                self._hVoxelScale = self._nifti_volume.getNormalizedVoxelSize()[0]
                self._vVoxelScale = self._nifti_volume.getNormalizedVoxelSize()[1]                    
            
        if self._zoom.isInitalized () :
            self.zoomOut ()
        if (self._UISlider is not None) :
            self._UISlider.valueChanged.disconnect (self._UISliderValueChanged)
        self._UpdateSlice (callback)
        if (self._UISlider is not None) :
            self._UISlider.valueChanged.connect (self._UISliderValueChanged)
   
    def getHVoxelScale (self) :
        if self._SliceAxis == 'Z' :
            if self.isNiftiDataLoaded () and self.areAxisProjectionControlsEnabledForAxis () :   
                _, voxelDim = self._AxisProjectionControls.getAxialViewProjection (self._nifti_volume)
                if voxelDim is not None :
                    voxelDim = np.copy (voxelDim)
                    voxelDim /= np.min (voxelDim)
                    return voxelDim[0]
                
        return self._hVoxelScale
    
    def getVVoxelScale (self) :
        if self._SliceAxis == 'Z' :
            if self.isNiftiDataLoaded () and self.areAxisProjectionControlsEnabledForAxis () :   
              _, voxelDim = self._AxisProjectionControls.getAxialViewProjection (self._nifti_volume)
              if voxelDim is not None :
                  voxelDim = np.copy (voxelDim)
                  voxelDim /= np.min (voxelDim)
                  return voxelDim[1]
              
        return self._vVoxelScale
    
    def addSliceListener (self, listener) :    
        if (listener not in self._sliceListener) :
            self._sliceListener.append (listener)

    def _hasSliceListener (self) :
        return (len (self._sliceListener) > 0)
    
    def _callSliceListener (self, cx, cy, cz, mouseEvent) :    
        handled = False
        for i in self._sliceListener :        
            handled = i ( cx, cy, cz, self._SliceAxis, mouseEvent) or handled
        return handled
    
    def _scrollZoomedViewToCoordinateSelection (self, coordinate) :
        if (self._zoom.isInitalized ()):
             minX = self._zoom.getMinX ()
             maxX = self._zoom.getMaxX ()
             minY = self._zoom.getMinY ()
             maxY = self._zoom.getMaxY ()
             newXCoordinate, newYCoordinate, newZCoordinate = self._Coordinate.getCoordinate ()
             newXCoordinate = int (newXCoordinate)
             newYCoordinate = int (newYCoordinate)
             newZCoordinate = int (newZCoordinate)
        
             if self._SliceAxis == 'Z' : 
                 xc, yc = newXCoordinate, newYCoordinate
             elif self._SliceAxis == 'Y'  :
                 xc, yc = newXCoordinate, newZCoordinate
             elif self._SliceAxis == 'X' :        
                 xc, yc = newYCoordinate, newZCoordinate
             xc, yc = self.transformPointFromImageOrientationToImageScreen (xc, yc, self.SliceViewer.pixmap.width (), self.SliceViewer.pixmap.height ())
                      
             dx = 0 
             dy = 0 
             if xc < minX :                  
                 dx = xc - minX -10
             elif xc > maxX :                  
                dx = xc - maxX + 10
                
             if yc < minY :                  
                 dy = yc - minY - 10
             elif yc > maxY :                  
                dy = yc - maxY + 10 
             if dx != 0 or dy != 0 :     
                self.scroll (dx, dy)                           
                self.update ()     
    
    def updateSliceSelection  (self, i, axis, DisableScrollToZoomAndUpdateSlice = False) :                
        if (self._Coordinate is None) :
            return False
        i = max (0,i)
        if axis == "X":
            if (self._nifti_volume is None) :
                i = 0
            else:                    
                i = min (i,self._nifti_volume.getSliceDimensions ()[0] -1)
          
            if not self._Coordinate.setXCoordinate (i) :          
                return False    
        elif axis == "Y":
            if (self._nifti_volume is None) :
                i = 0
            else: 
                i = min (i, self._nifti_volume.getSliceDimensions ()[1] -1)
        
            if not self._Coordinate.setYCoordinate (i) :          
                return False  
        elif axis == "Z":
             if (self._nifti_volume is None) :
                i = 0
             else:
                i = min(i, self._nifti_volume.getSliceDimensions ()[2] -1)
             if not self._Coordinate.setZCoordinate (i) :          
                return False
        
        if not DisableScrollToZoomAndUpdateSlice :
           self.CoordianteChangedUpdateSliceAxis (self._SliceAxis == axis)
        return True
    
    def CoordianteChangedUpdateSliceAxis (self, UpdateSliceAxis):
        self._scrollZoomedViewToCoordinateSelection (self._Coordinate)                 
        if UpdateSliceAxis :
            self._UpdateSlice ()
        else:
            #print ("updateSliceSelection.update")
            self.update ()
            
    def resizeEvent (self, event):
        w = event.size().width()
        h = event.size().height()
        self._OffScreenBuffer = QtGui.QPixmap (w, h)
        self._ROIDrawBuffer = QtGui.QPixmap (w, h)
        QtWidgets.QWidget.resizeEvent (self, event)
        
    def getVisibleImageWidth (self) :
         if not self._zoom.isInitalized () :
              return self._getPixmapWidth ()
         else:
             maxW = self._getPixmapWidth ()
             zoomW = self._zoom.width ()
             zoomW = max (0, zoomW)
             return min (maxW, zoomW)
             
    def getVisibleImageHeight (self):
         if not self._zoom.isInitalized () :
              return self._getPixmapHeight ()
         else:
             maxH = self._getPixmapHeight ()
             zoomH = self._zoom.height ()
             zoomH = max (0, zoomH)
             return min (maxH, zoomH)
    
    def drawPixMap (self, qp, x1, y1, w1, h1, pixmap, x2, y2, w2, h2):
        self._overlay_x1 = x1
        self._overlay_y1 = y1
        self._overlay_w1 = w1
        self._overlay_h1 = h1
        self._overlay_x2 = x2
        self._overlay_y2 = y2
        self._overlay_w2 = w2
        self._overlay_h2 = h2
        qp.drawPixmap (x1, y1, w1, h1, pixmap, x2, y2, w2, h2)  
    
    def getClippingView (self) :
        x1 = self._overlay_x1
        y1 = self._overlay_y1 
        w1 = self._overlay_w1 
        h1 = self._overlay_h1 
        return (x1, y1, w1, h1)
    
    def drawOverlayQTPixmap (self, qp, pixmap):
        
                 
        x1 = self._overlay_x1
        y1 = self._overlay_y1 
        w1 = self._overlay_w1 
        h1 = self._overlay_h1 
        x2 = self._overlay_x2 
        y2 = self._overlay_y2 
        w2 = self._overlay_w2 
        h2 = self._overlay_h2 
        transformedpixmap = pixmap.transformed(self._ViewPortTransformation.getQTransformation ())        
        """if ScreenClipping is not None :
           clipXStart, clipYStart, clipWidth, clipHeight =  ScreenClipping
           if clipXStart >= x1 and clipXStart <= x1 + w1 and clipYStart >= y1 and  clipYStart <= y1 + h1  :
                clipWidth =  min (clipWidth + clipXStart, x1 + w1) - clipXStart
                clipHeight = min (clipHeight + clipYStart, y1 + h1) - clipYStart
                
                scaleWidth = float (w2)/float(w1)
                scaleHeight  = float (h2)/float(h1)
                x2 += int (scaleWidth * float (clipXStart - x1)) 
                y2 += int (scaleHeight * float (clipYStart - y1)) 
                x1 = int (clipXStart)
                y1 = int (clipYStart)
                w2 = int (scaleWidth * float (clipWidth))
                h2 = int (scaleHeight * float (clipHeight))
                w1 = int (clipWidth)
                h1 = int (clipHeight)"""
        
        qp.drawPixmap (x1, y1, w1, h1, transformedpixmap, x2, y2, w2, h2)  
    
    def _isZoomed (self) :                  
        #_hVoxelScale, _vVoxelScale = self._ViewPortTransformation.getTransformedVoxelScale ((float (self._hVoxelScale), float (self._vVoxelScale)))                     
        #heightZoom = float (self.height()) / float (_vVoxelScale * float (self.getVisibleImageHeight ()))
        #widthZoom  = float (self.width ()) / float (_hVoxelScale * float (self.getVisibleImageWidth ()))
        return self._getPixmapWidth () > self.getVisibleImageWidth () or self._getPixmapHeight () > self.getVisibleImageHeight () 
        #return max (widthZoom, heightZoom) > 1
            
    
    def isZoomed (self) :
        try:
            if (self._zoom is not None) :
                return self._zoom.isInitalized ()
            return False
        except :
            return False
    
    def scroll (self, dx, dy) :
         Change = False
         #self._disableZoomSlider = True                 
         if (self._UIZoomScrollX is not None and dx != 0)  :
             pos = self._UIZoomScrollX.value () + dx
             self._UIZoomScrollX.setValue (pos)
             #self._setMoveZoomX (self._UIZoomScrollX.value ())    
             Change = True             
         if (self._UIZoomScrollY is not None and dy != 0)  :             
             pos = self._UIZoomScrollY.value () + dy
             self._UIZoomScrollY.setValue (pos)         
             #self._setMoveZoomY (self._UIZoomScrollY.value ())                           
             Change = True                      
         #self._disableZoomSlider = False         
         if (Change) :
             self.update ()
         
        
    def _clearOldMouseCursor (self) :
        if self._mX is not None : 
           self.clearOldPen (None, None, None)
           self._mX = None 
           
    def setPaintBrush (self, pen) :
        if self._PenSettings is None and pen is None :
            return
        elif self._PenSettings is None  :
            pass
        elif self._PenSettings.arePenSettingsChanged (pen) :
            pass
        else:
            return        
        self._PenSettings = pen 
        self.update ()
    
    def _reInitalizeZoom (self) : 
                
        if (self._zoom.isInitalized ()) : # if zooming and does not fill view                        
            self._zoom.reset ()   
            window_height = self.height()
            window_width = self.width ()
            _hVoxelScale, _vVoxelScale = self._ViewPortTransformation.getTransformedVoxelScale ((float (self.getHVoxelScale()), float (self.getVVoxelScale ())))        
            heightZoom = float (float(window_height) / (_vVoxelScale * float (self.getVisibleImageHeight ())))
            widthZoom  = float (float (window_width) / (_hVoxelScale * float (self.getVisibleImageWidth ())))        
            zoom = min (widthZoom, heightZoom)           
            cX, cY = self._zoom.getZoomCenterCord ()                      
            xDim = math.ceil (float (window_width)/ float(_hVoxelScale * float(zoom)))
            
            yDim = math.ceil (float(window_height)/ float(_vVoxelScale * float (zoom)))
            
            zoom_minX = int (cX - xDim / 2)
            zoom_maxX = zoom_minX + xDim - 1
            zoom_minY = int (cY - yDim / 2)
            zoom_maxY = zoom_minY + yDim - 1
            
            pm_width, pm_height = self._getPixmapWidthHeight ()                        
            
            # Clip edges
            if (zoom_minX < 0) :
                zoom_maxX += -zoom_minX
                zoom_minX = 0
            if (zoom_minY < 0) :
                zoom_maxY += -zoom_minY
                zoom_minY = 0            
            if (zoom_maxX >= pm_width) :
                zoom_minX = max (0, zoom_minX - (zoom_maxX - pm_width))
                zoom_maxX = pm_width                
            if (zoom_maxY >= pm_height) :
                zoom_minY = max (0, zoom_minY - (zoom_maxY - pm_height))
                zoom_maxY = pm_height
            
            self._zoom.setMinX (zoom_minX)                 
            self._zoom.setMaxX (zoom_maxX)                 
            self._zoom.setMinY (zoom_minY)                 
            self._zoom.setMaxY (zoom_maxY)                             
            zoom_width = self._zoom.width ()
            zoom_height = self._zoom.height ()
            if (self._UIZoomScrollX is not None) :
                maximum = pm_width - zoom_width
                if zoom_minX != self._UIZoomScrollX.value () or maximum !=  self._UIZoomScrollX.maximum () :
                    try:
                        self._UIZoomScrollX.valueChanged.disconnect  (self._zoomSliderMovedX) 
                        ReConnect = True
                    except:
                        ReConnect = False
                    self._UIZoomScrollX.setMinimum (0)                                    
                    self._UIZoomScrollX.setMaximum (maximum)              
                    self._UIZoomScrollX.setValue (zoom_minX)       
                    self._UIZoomScrollX.setEnabled (maximum > 0)
                    self._UIZoomScrollX.setVisible (maximum > 0)
                    if ReConnect :
                        self._UIZoomScrollX.valueChanged.connect  (self._zoomSliderMovedX) 

                
            if (self._UIZoomScrollY is not None) :
                maximum = pm_height - zoom_height
                if zoom_minY != self._UIZoomScrollY.value () or maximum !=  self._UIZoomScrollY.maximum () :
                    try :    
                        self._UIZoomScrollY.valueChanged.disconnect  (self._zoomSliderMovedY) 
                        ReConnect = True
                    except:
                        ReConnect = False
                    self._UIZoomScrollY.setMinimum (0)                
                    self._UIZoomScrollY.setMaximum (maximum)
                    self._UIZoomScrollY.setValue (zoom_minY)       
                    self._UIZoomScrollY.setEnabled (maximum > 0)
                    self._UIZoomScrollY.setVisible (maximum > 0)
                    if ReConnect :
                        self._UIZoomScrollY.valueChanged.connect  (self._zoomSliderMovedY) 
                        
            #if (self._oldzoomstate == None or self._oldzoomstate._minX != self._zoom._minX or self._oldzoomstate._maxX != self._zoom._maxX or self._oldzoomstate._minY != self._zoom._minY or self._oldzoomstate._maxY != self._zoom._maxY) :
            #    self._oldzoomstate = self._zoom.copy ()                
            #    CallZoomListener = True
                
            width  = math.ceil (float(zoom_width)  * zoom * _hVoxelScale)
            height = math.ceil (float(zoom_height) * zoom * _vVoxelScale)             
            if (width < window_width) :           
                self._DrawOffSetX = int ((window_width - width) / 2)
            elif (self._zoom.getMinX() == 0):                
                self._DrawOffSetX = 0
            else:
                self._DrawOffSetX = window_width - width
            
            if height < window_height :            
                self._DrawOffSetY = int ((window_height - height) / 2)
            elif (self._zoom.getMinY() == 0):                
                self._DrawOffSetY = 0
            else:
                self._DrawOffSetY = window_height - height            
            self._DrawWidthScale  = float (zoom) * float (_hVoxelScale)
            self._DrawHeightScale = float (zoom) * float (_vVoxelScale)        
            return width, height
        return 0, 0

    
    
    def setDrawSelectedROIOnTop (self, val) :
        self._DrawSelectedROIOnTop = val
    
    def isSelected (self) :   
        return self._isSelected 
    
    def setSelectionChangeDisabled (self):
        self._SelectionChangedDisabled = True
        
    def setSelectionChangeEnabled (self):
        self._SelectionChangedDisabled = False
        
    def setSelected (self, selected) :
        if self._SelectionChangedDisabled :
            return 
        if selected != self._isSelected :
            self._isSelected = selected
            self.update ()
            if selected : 
                self._callSelectionListener ()
                
    def _callSelectionListener (self) :        
        for ref in self._selectionListener :
            try :
                if not UseWeakRef or PythonVersionTest.IsPython2 () :
                    ref (self)
                else:
                    ref()(self)
            except:
                pass
    
    def selectionChangeCallback (self, selectedSliceWidget):
        if selectedSliceWidget != self :
           self.setSelected (False) 
        
    
    def addSelectionListener (self, callback) :        
        for ref in self._selectionListener :
            if not UseWeakRef or PythonVersionTest.IsPython2 () :
                if ref == callback :
                    return
            else:
                if ref() == callback :
                    return
        if not UseWeakRef or PythonVersionTest.IsPython2 () :
            self._selectionListener.append (callback)
        else:
            try :       
                ref = weakref.WeakMethod(callback)    
                self._selectionListener.append (ref)
            except:
                pass
    
    def removeSelectionListener (self, callback) :
        for index, ref in enumerate (self._selectionListener) :
            if not UseWeakRef or PythonVersionTest.IsPython2 () :
                found = ref == callback 
            else:
                found = ref() == callback 
            if found :            
                del self._selectionListener[index]
                return
    
    
    
                
    def _drawSubLineSegment (self, qp, pt, center, Colors, mC) :
        dp = pt - center
        mag = np.sqrt (np.sum (dp*dp))
        if mC is None or mag == 0 :
            qp.setPen (Colors[0])      
        else:
             dp /= mag
             cos = np.sum (dp * mC)
             if cos > 0.9 :
                 qp.setPen (Colors[1])      
             else:
                 qp.setPen (Colors[0]) 
    
        p1, p2 = pt[0], pt[1] #self.transformPointFromImScreen (pt[0], pt[1])
        p3, p4 = center[0], center[1] #self.transformPointFromImageToScreen (center[0], center[1])
        qp.drawLine ( p1,p2,p3,p4)     
          
                         
    def _drawLine (self, qp, ptsX, center, slp, leftE, rightE, topE,bottomE, Colors = None, mC = None):
        
        def _clipPoint ( pt, top, bottom, slp, center) :
                    ty = pt[1]
                    if ty < 0 or ty > self.height () :
                        ypt1 = top * slp + center
                        ypt2 = bottom * slp + center
                        if  ty < 0 :
                            if ypt1[1] < ypt2[1] :
                                pt[:] = ypt1
                            else:
                                pt[:] = ypt2
                        else:
                            if ypt1[1] > ypt2[1] :
                                pt[:] = ypt1
                            else:
                                pt[:] = ypt2
                    pt[1] = max (min (pt[1], self.height ()), 0)
                    pt[0] = max (min (pt[0], self.width ()), 0)
                    return pt
                
        dx, dy = slp[0], slp[1]
        left = leftE / dx
        right = rightE / dx
        ptsX[0,:] = left * slp + center
        ptsX[1,:] = right * slp + center
        ptsX = ptsX.astype (np.int) 
        top = topE / dy
        bottom = bottomE / dy
        ptsX[0,:] = _clipPoint (ptsX[0,:], top, bottom, slp, center)
        ptsX[1,:] = _clipPoint (ptsX[1,:], top, bottom, slp, center)
        if Colors is None:
            qp.drawLine ( ptsX[0,0], ptsX[0,1], ptsX[1,0], ptsX[1,1])
        else:
            self._drawSubLineSegment (qp, ptsX[0,:], center, Colors, mC)
            self._drawSubLineSegment (qp, ptsX[1,:], center, Colors, mC)
            qp.setPen (Colors[1]) 
                        
    def _getCenter (self) :
            return int (self.width () / 2), int (self.height() / 2)
                    
    def _drawSelectedCoordinateIndicator (self, qp, markerList = None) :      
        if  self._Coordinate is not None : 
            qp.setPen (QtGui.QColor (43,92, 155))                        
            xc, yc, zc = self._Coordinate.getCoordinate ()
            if self._SliceAxis == 'Z' : 
                if self._AxisProjectionControls is  not None and self._AxisProjectionControls.isAxisRotated () :
                    xc, yc = self._AxisProjectionControls.getProjectedPlaneXYCoordinateFromWC (self._Coordinate, self.getNIfTIVolume())                
                xc, yc = self.transformPointFromImageToScreen (xc, yc)
            elif self._SliceAxis == 'Y'  :
                xc, yc = self.transformPointFromImageToScreen (xc, zc)
            elif self._SliceAxis == 'X' :        
                xc, yc = self.transformPointFromImageToScreen (yc, zc)
            
            if self._AxisProjectionControls is  None or not self._AxisProjectionControls.isEnabledForAxis (self) :
                radians = 0.0
            else:
                radians = self._AxisProjectionControls.getAxisSlope (self)
                if not np.isnan (radians) and radians != 0.0 :
                    dx = math.cos (radians)
                    dy = math.sin (radians)
                    
            if np.isnan (radians) or radians == 0.0 or dx == 0 or dy == 0 :
                if self._AxisProjectionControls is not None and self._AxisProjectionControls.isMarkerSelected (self) and self._AxisProjectionControls.isEnabledForAxis (self) :
                    mouseCord = self._AxisProjectionControls.getLastMouseSelection (self)
                    mC = None
                    if self._nifti_volume is not None :
                        centerX, centerY = self._getCenter ()                        
                                                
                        if centerX is not None and centerY is not None :     
                            center = np.array ([xc, yc],dtype = np.float)
                            if mouseCord is not None :   
                                mC = np.array ([mouseCord[0],mouseCord[1]], dtype = np.float)
                                mC -= center
                                mag = np.sqrt (np.sum (mC*mC))
                                if mag > 0 :
                                    mC /= mag
                                else:
                                    mC = None
                            pt = np.array ([ [0,center[1]], [ self.width (), center[1]]], dtype = np.float)
                            for index in range (2) :
                                self._drawSubLineSegment (qp, pt[index,:], center, [QtGui.QColor (247,207,94), QtGui.QColor (247,207,94)], mC) 
                            qp.setPen (QtGui.QColor (247,207,94))       
                            qp.drawText(center[0], center[1], "0.0 deg")
            else:
                centerX, centerY = self._getCenter ()
                if centerX is not None and centerY is not None :          
                    slope = np.array ([dx, dy], dtype=np.float)
                                        
                    center = np.array ([xc, yc],dtype = np.float)
                    ptsX = np.zeros ((2,2),dtype=np.float)
                    leftE = float (-center[0])
                    rightE = float (self.width () -center[0])
                    leftE, rightE = min (leftE, rightE), max (leftE, rightE)
                    topE = float (-center[1])
                    bottomE = float (self.height () - center[1])
                    topE, bottomE = min (topE, bottomE), max (topE, bottomE)
                    
                    if self._AxisProjectionControls.isMarkerSelected (self) :
                        mouseCord = self._AxisProjectionControls.getLastMouseSelection (self)
                        mC = None
                        if mouseCord is not None :
                            mC = np.array ([mouseCord[0],mouseCord[1]], dtype = np.float)
                            mC -= center
                            mag = np.sqrt (np.sum (mC*mC))
                            if mag > 0 :
                                mC /= mag
                            else:
                                mC = None
                        self._drawLine (qp, ptsX, center, slope, leftE, rightE, topE, bottomE, [QtGui.QColor (247,207,94), QtGui.QColor (247,207,94)], mC = mC)
                        #self._drawLine (qp, ptsX, center, pSlope,  leftE, rightE, topE, bottomE, [QtGui.QColor (247,207,94), QtGui.QColor (209,94, 64)], mC = mC)
                        qp.setPen (QtGui.QColor (247,207,94))        
                        deg = radians * 180.0/math.pi
                        qp.drawText(center[0], center[1], "%0.1f deg" % deg )
                    else:
                        qp.setPen (QtGui.QColor (247,207,94)) 
                        self._drawLine (qp, ptsX, center, slope, leftE, rightE, topE, bottomE)
                        #self._drawLine (qp, ptsX, center, pSlope, leftE, rightE, topE, bottomE)
            qp.setPen (QtGui.QColor (43,92, 155))  
            qp.drawLine ( xc, 0, xc, self.height ())
            qp.drawLine ( 0, yc, self.width (), yc)
                    
    def  paintEvent (self, event):                               
        #print (("Paint Update rect", event.rect ()))
        event.accept ()
        try:
            self._lastPenDraw = None
            if (not self.isNiftiDataLoaded ()) :            
                 qp = QtGui.QPainter (self)
                 qp.setPen (QtGui.QColor (0,0, 0))
                 qp.setBrush (QtGui.QColor (0,0,0))
                 qp.drawRect (0,0, self.width (), self.height ())
                 qp.end ()
                 return
                
            if self._windowUpdateEnabled :
                
                self._OffScreenBuffer.fill(QtGui.QColor(0,0,0,255))
                qp = QtGui.QPainter (self._OffScreenBuffer)
                
                qp.setPen (QtGui.QColor (255,205,94))
                qp.setBrush (QtGui.QColor (0,0,0,0))
                    
                #self._zoom.reset ()        
                #qp.drawRect (0,0, self.width (), self.height ())
                qp.setRenderHint (qp.SmoothPixmapTransform, self._visSettings.getSmoothVisualization ())
                
                initDim = (self.width (), self.height ())
                if self._ZoomInitlizedDim is None or initDim != self._ZoomInitlizedDim[0] :                
                    width, height = self._reInitalizeZoom ()
                    self._ZoomInitlizedDim = (initDim, (width, height))
                else:
                    width, height = self._ZoomInitlizedDim[1]
                if (self._zoom.isInitalized ()) : # if zooming and does not fill view                                            
                    self.drawPixMap (qp, self._DrawOffSetX, self._DrawOffSetY, width, height, self._getPixmap(), self._zoom.getMinX (), self._zoom.getMinY (), self._zoom.getMaxX() - self._zoom.getMinX() + 1, self._zoom.getMaxY() - self._zoom.getMinY() + 1)  
                    qp.drawRect (self._DrawOffSetX-1, self._DrawOffSetY-1, width+1, height+1)      
                else:
                    _hVoxelScale, _vVoxelScale = self._ViewPortTransformation.getTransformedVoxelScale ((float (self.getHVoxelScale()), float (self.getVVoxelScale ())))
                    PixmapApectRatio = (_hVoxelScale * self._getPixmapWidth ()) / (_vVoxelScale *  self._getPixmapHeight ())
                    drawWidth = int (PixmapApectRatio * self.height())                                        
                    if drawWidth <= self.width() :                                                  
                        if (drawWidth < 1) :
                            drawWidth = 1
                        self._DrawOffSetX = int ((self.width() - drawWidth) / 2)
                        self._DrawOffSetY = 0            
                        self.drawPixMap (qp, self._DrawOffSetX,0, drawWidth, self.height(), self._getPixmap(), 0, 0, self._getPixmapWidth (),  self._getPixmapHeight ())  
                        qp.drawRect (self._DrawOffSetX-1,-1, drawWidth+1, self.height()+1)
                        self._DrawWidthScale = float (drawWidth) / float (self._getPixmapWidth ())            
                        self._DrawHeightScale = float (self.height()) /  float (self._getPixmapHeight ())
                    else:                   
                        PixmapApectRatio =  (_vVoxelScale *  self._getPixmapHeight ()) / (_hVoxelScale  * self._getPixmapWidth ())
                        drawHeight = int (PixmapApectRatio * self.width())
                        if (drawHeight < 1) :
                            drawHeight = 1
                        self._DrawOffSetY = int ((self.height() - drawHeight) / 2)
                        self._DrawOffSetX = 0
                        self.drawPixMap (qp, 0, self._DrawOffSetY, self.width (), drawHeight, self._getPixmap(), 0, 0, self._getPixmapWidth (),  self._getPixmapHeight ())  
                        qp.drawRect (-1,self._DrawOffSetY-1, self.width ()+1, drawHeight+1)  
                        self._DrawWidthScale = float (self.width ()) / float (self._getPixmapWidth ())
                        self._DrawHeightScale = float (drawHeight) /  float (self._getPixmapHeight ())      
                #Draw Axis
                qp.setRenderHint (qp.SmoothPixmapTransform, False)
                qp.setPen (QtGui.QColor (43,92, 155))
               
                if self.getDrawCoordinateIndicator () :
                    self._drawSelectedCoordinateIndicator (qp)    
                _visSettings = self._visSettings
                if (self.getVolumeMask () is not None or _visSettings == None) :
                    _visSettings = VisSettings (TransparencyValue = 0)
                
                if (_visSettings.getDrawROI ()) :
        
                    self._ROIDrawBuffer.fill(QtGui.QColor(0,0,0,0))
                    RoiDrawBuffer = QtGui.QPainter (self._ROIDrawBuffer)
                    
                    if self._ROIDefs is not  None :
                        drawObjectList = self._ROIAxisObjectList
                        if len (self._SelectedObjectLst) == 1 and self._SelectedObjectLst[0] not in self._ROIAxisObjectList :
                            drawObjectList = self._ROIAxisObjectList + [self._SelectedObjectLst[0]]
                        else: 
                            drawObjectList = self._ROIAxisObjectList
                    
                    
                        drawObjectList = self._ROIDefs.getObjectLstInROIOrder (drawObjectList, UseDrawTopOverride = self._DrawSelectedROIOnTop)
                        drawObjectList.reverse()
                        
                        WinWidth = self.width ()
                        WinHeight = self.height()
                        if (self._TempDrawPixmap is None) or (self._TempDrawPixmap.width () != WinWidth or self._TempDrawPixmap.height () != WinHeight) :
                            self._TempDrawPixmap = QtGui.QPixmap (WinWidth, WinHeight)   
                        TempDrawPixmap = self._TempDrawPixmap
                        clearVisSettings = _visSettings.copy ()
                        clearVisSettings.setUITranparency (255)
                        if not self._ROIDefs.hasSingleROIPerVoxel() :         
                            if (self._AccumulatorPixmap is None) or (self._AccumulatorPixmap.width () != WinWidth or self._AccumulatorPixmap.height () != WinHeight) :
                                self._AccumulatorPixmap = QtGui.QPixmap (WinWidth, WinHeight)   
                            AccumulatorPixmap = self._AccumulatorPixmap
                            AccumulatorPixmap.fill(QtGui.QColor(0,0,0,0))
                        
                        bufferOrigionalOpacity = RoiDrawBuffer.opacity ()
                        TranspOpacity = _visSettings.getUITransparency ()/255.0
                        
                        
                        OptimizedSingleROIPerVoxelDrawUsed = False
                        if self._ROIDefs.hasSingleROIPerVoxel() and len (drawObjectList) > 1 and (self._PenSettings is None or not self._PenSettings.isErasePen ()) :
                            All_Area_ObjInSlice = True
                            for obj in drawObjectList  : 
                                if not obj.isROIArea () :
                                    All_Area_ObjInSlice = False
                                    break
                                elif obj.hasDeepGrowAnnotations () and (obj not in self._SelectedObjectLst or len (self._SelectedObjectLst) > 1)  :
                                    All_Area_ObjInSlice = False # don't draw using if deep grow annotaitons are present
                                    break
                            if All_Area_ObjInSlice :
                                obj = drawObjectList[0]
                                if len (self._SelectedObjectLst) == 1 :
                                    SelectedObjects = self._SelectedObjectLst
                                else:
                                    SelectedObjects = []
                                
                                niftiMem = self.getNIfTIVolume()
                                if self.getSliceAxis () == "Z" and niftiMem is not None and self._AxisProjectionControls is not None and self._AxisProjectionControls.isAxisRotated () :
                                    
                                    def  GetVolumeFunc (FirstSlice, LastSlice) :
                                         sliceVolumeData, colorTable, objectIDToCacheID = obj.getOptomizedDrawSingleROIColorizedSlice (self,self._ROIDefs, [], ReturnVolume = True)
                                         return sliceVolumeData, colorTable, objectIDToCacheID                                                                
                                    
                                    HideObjects = set (SelectedObjects)
                                    for obj in drawObjectList :
                                        if self._ROIDefs.isROIHidden (obj.getName (self._ROIDefs)) :
                                           HideObjects.add (obj)                                 
                                    mask, ColorTable = self._AxisProjectionControls.transformROIDataFromVolumeToProjection (niftiMem, self.getAxisProjectionSliceNumber (), GetVolumeFunc = GetVolumeFunc, HideObjects = HideObjects, ClipROIObjectList = drawObjectList) 
                                else:
                                    SliceNumber = self.getSliceNumber () 
                                    mask, ColorTable, objectIDToCacheID  = obj.getOptomizedDrawSingleROIColorizedSlice (self,self._ROIDefs, SelectedObjects, SliceNumber)
                                
                                
                                if mask is not None and ColorTable is not None :
                                    width, height = mask.shape                                                                                                 
                                    mask = np.moveaxis(ColorTable[mask], -1, 0)                                                                            
                                    mask = mask.astype (np.uint8)
                                    mask[3,...] = (TranspOpacity * mask[3,...]).astype(np.uint8)                            
                                    maskImage =  QtGui.QImage (mask.tobytes('F'), width, height, width * 4, QtGui.QImage.Format_ARGB32)                
                                    maskPixmap = QtGui.QPixmap.fromImage (maskImage)                                                             
                                    RoiDrawBuffer.setCompositionMode (QtGui.QPainter.CompositionMode_SourceOver)                                     
                                    self.drawOverlayQTPixmap (RoiDrawBuffer, maskPixmap)
                                    if len (SelectedObjects) == 1  :
                                        obj = SelectedObjects[0]
                                        roiBBVisible = False
                                        isROIHidden = self._ROIDefs.isROIHidden (obj.getName (self._ROIDefs))
                                        if self._ROIDefs.hasSingleROIPerVoxel() :
                                            TempDrawPixmap.fill(QtGui.QColor(0,0,0,0))
                                            TempDrawBufferPainter = QtGui.QPainter (TempDrawPixmap)                    
                                            TempDrawBufferPainter.setCompositionMode (QtGui.QPainter.CompositionMode_SourceOver)
                                            obj.draw (TempDrawBufferPainter, self, self._ROIDefs, self._Coordinate.getZCoordinate (), self._ROIBBResize, roiBBVisible, clearVisSettings, overrideHideROI = True)                    
                                            TempDrawBufferPainter.end ()  
                                            
                                            RoiDrawBuffer.setCompositionMode (QtGui.QPainter.CompositionMode_DestinationOut) 
                                            RoiDrawBuffer.drawPixmap (0,0, TempDrawPixmap)
                                            if not isROIHidden :
                                                RoiDrawBuffer.setCompositionMode (QtGui.QPainter.CompositionMode_SourceOver)
                                                RoiDrawBuffer.setOpacity (TranspOpacity)
                                                RoiDrawBuffer.drawPixmap (0,0, TempDrawPixmap)
                                                RoiDrawBuffer.setOpacity (bufferOrigionalOpacity)                                
                                    OptimizedSingleROIPerVoxelDrawUsed = True               
                        
                        
                        if not OptimizedSingleROIPerVoxelDrawUsed :
                            pointObjList = []                        
                            objList = []
                            for obj in drawObjectList  :
                                if obj.isROIArea () :
                                    objList.append (obj)
                                else:
                                    pointObjList.append (obj)                            
                            drawObjectList = objList + pointObjList
                            for obj in drawObjectList  :                            
                                # print(obj.name)
                                #roiBBVisible = self._ROIBBVisilbe
                                #if (len (self._TemporaryLine) > 0) or ((obj.isROIArea() and obj.isTemporaryObjectSliceMaskInitalized())) :
                                roiBBVisible = False
                                isROIHidden = self._ROIDefs.isROIHidden (obj.getName (self._ROIDefs))
                                if self._ROIDefs.hasSingleROIPerVoxel() :
                                    TempDrawPixmap.fill(QtGui.QColor(0,0,0,0))
                                    TempDrawBufferPainter = QtGui.QPainter (TempDrawPixmap)                    
                                    TempDrawBufferPainter.setCompositionMode (QtGui.QPainter.CompositionMode_SourceOver)
                                    obj.draw (TempDrawBufferPainter, self, self._ROIDefs, self._Coordinate.getZCoordinate (), self._ROIBBResize, roiBBVisible, clearVisSettings, overrideHideROI = True)                    
                                    TempDrawBufferPainter.end ()  
                                    
                                    RoiDrawBuffer.setCompositionMode (QtGui.QPainter.CompositionMode_DestinationOut) 
                                    RoiDrawBuffer.drawPixmap (0,0, TempDrawPixmap)
                                    if not isROIHidden :
                                        RoiDrawBuffer.setCompositionMode (QtGui.QPainter.CompositionMode_SourceOver)
                                        RoiDrawBuffer.setOpacity (TranspOpacity)
                                        RoiDrawBuffer.drawPixmap (0,0, TempDrawPixmap)
                                        RoiDrawBuffer.setOpacity (bufferOrigionalOpacity)
                                    """RoiDrawBuffer.setCompositionMode (QtGui.QPainter.CompositionMode_DestinationOut) 
                                    obj.draw (RoiDrawBuffer, self, self._ROIDefs, self._Coordinate.getZCoordinate (), self._ROIBBResize, roiBBVisible, clearVisSettings, overrideHideROI = True)  # Erase overlapping
                                    RoiDrawBuffer.setCompositionMode (QtGui.QPainter.CompositionMode_SourceOver)
                                    obj.draw (RoiDrawBuffer, self, self._ROIDefs, self._Coordinate.getZCoordinate (), self._ROIBBResize, roiBBVisible, _visSettings)  #draw over top"""
                                else:     
                                    if not isROIHidden :
                                        TempDrawPixmap.fill(QtGui.QColor(0,0,0,0))
                                        TempDrawBufferPainter = QtGui.QPainter (TempDrawPixmap)                    
                                        TempDrawBufferPainter.setCompositionMode (QtGui.QPainter.CompositionMode_SourceOver)
                                        obj.draw (TempDrawBufferPainter, self, self._ROIDefs, self._Coordinate.getZCoordinate (), self._ROIBBResize, roiBBVisible, clearVisSettings)                    
                                        TempDrawBufferPainter.end ()                    
                                    
                                        TempDrawPixmap2 = QtGui.QPixmap (TempDrawPixmap)
                                                     
                                        TempDrawBufferPainter = QtGui.QPainter (TempDrawPixmap)                    
                                        TempDrawBufferPainter.setCompositionMode (QtGui.QPainter.CompositionMode_DestinationOut)                    
                                        TempDrawBufferPainter.drawPixmap (0,0, AccumulatorPixmap)
                                        TempDrawBufferPainter.end ()      
                                                            
                                        TempDrawPixmap2Painter = QtGui.QPainter (TempDrawPixmap2)                    
                                        TempDrawPixmap2Painter.setCompositionMode (QtGui.QPainter.CompositionMode_DestinationOut)                    
                                        TempDrawPixmap2Painter.drawPixmap (0,0, TempDrawPixmap)
                                        TempDrawPixmap2Painter.end ()      
                                        
                                        AccumulatorPainter = QtGui.QPainter (AccumulatorPixmap)                    
                                        AccumulatorPainter.setCompositionMode (QtGui.QPainter.CompositionMode_SourceOver)                    
                                        AccumulatorPainter.drawPixmap (0,0, TempDrawPixmap)
                                        AccumulatorPainter.setOpacity (0.5)
                                        AccumulatorPainter.drawPixmap (0,0, TempDrawPixmap2)                    
                                        AccumulatorPainter.setOpacity (1.0)                    
                                        AccumulatorPainter.end ()
                                        del TempDrawPixmap2                          
                        
                        RoiDrawBuffer.setCompositionMode (QtGui.QPainter.CompositionMode_SourceOver)
                        if not self._ROIDefs.hasSingleROIPerVoxel() :                                                
                            RoiDrawBuffer.setOpacity (TranspOpacity)
                            RoiDrawBuffer.drawPixmap (0,0, AccumulatorPixmap)                                  
                            RoiDrawBuffer.setOpacity (bufferOrigionalOpacity)
                        """    del TempDrawPixmap 
                            del AccumulatorPixmap                
                        else:
                             del TempDrawPixmap """
                        if len (self._SelectedObjectLst) == 1 :
                            obj = self._SelectedObjectLst[0]            
                            if obj != None  : #and obj not in self._ROIAxisObjectList
                                roiBBVisible = self._ROIBBVisilbe
                                if (len (self._TemporaryLine) > 0) or ((obj.isROIArea() and obj.isTemporaryObjectSliceMaskInitalized())) :
                                    roiBBVisible = False
                                RoiDrawBuffer.setCompositionMode (QtGui.QPainter.CompositionMode_SourceOver)
                                obj.draw (RoiDrawBuffer, self, self._ROIDefs, self._Coordinate.getZCoordinate (), self._ROIBBResize, roiBBVisible, _visSettings, drawObj = False)
                    RoiDrawBuffer.end ()
                    
                    
                    qp.setCompositionMode (QtGui.QPainter.CompositionMode_SourceOver)
                    qp.drawPixmap (0,0, self._ROIDrawBuffer)
                    
                if (len(self._TemporaryLine) >= 2):
                    qp.setPen (self._TemporaryLineColor)
                    qp.setBrush (QtGui.QColor (0,0,0,0))      
                    x,y = self._TemporaryLine[0]
                    x, y = self.transformPointFromImageToScreen (x, y)
                   
                    path = QtGui.QPainterPath()
                    path.moveTo(x, y);                    
                    for tup in self._TemporaryLine :
                      x, y = tup
                      x, y = self.transformPointFromImageToScreen (x, y)
                      path.lineTo (x,y)
                    qp.drawPath(path)
                    #qp.drawEllipse (x-5,y-5,10,10)
                
                font=qp.font() 
                font.setPointSize (12)
                qp.setFont(font)
                if (self._visSettings is not None and self._visSettings.showSliceOrientation ()) :            
                    #//font.setWeight(QFont::DemiBold);
                    if self.getSliceAxis () != 'Z' or self._AxisProjectionControls is None or not self._AxisProjectionControls.isAxisRotated () :
                        LeftMsg, RightMsg, TopMsg, BottomMsg = self.getAxisDisplayOrientation ()
                        width = int (self.width ())
                        height = int (self.height ())
                        qp.setPen (QtGui.QColor (0, 172, 212))
                        middleX = int (width / 2)
                        qp.drawText(middleX, 15, TopMsg)
                        qp.drawText(middleX, height-3, BottomMsg)
                        middleY = int (height / 2)
                        qp.drawText(3, middleY , LeftMsg)           
                        middleY = int (height / 2)
                        qp.drawText(width - 15, middleY , RightMsg)
        
                
                
                if self._visSettings is None or self._visSettings.showFileNameTitle() and len (self._filenameLableTxt) > 0 :
                    textYOffset  = 20
                    qp.setPen (QtGui.QColor (115,194, 88,255))
                    qp.drawText(10, textYOffset  , self._filenameLableTxt)               
                else:
                    textYOffset  = 0
                                                     
                if (len (self._HiddenLabelTxt) > 0) :
                    qp.setPen (QtGui.QColor (255,0, 0,255))            
                    qp.drawText(10, 20 + textYOffset , self._HiddenLabelTxt)               
                    textYOffset += 20
                    
                if (len (self._LockedLabelTxt) > 0) :
                    qp.setPen (QtGui.QColor (255,0, 0,255))                            
                    qp.drawText(10, textYOffset  + 20 , self._LockedLabelTxt)               
                    
                
                if (len (self._ZoomLabelTxt) > 0) :
                    qp.setPen (QtGui.QColor (0, 172, 212, 255))
                    height = int (self.height ())
                    qp.drawText(10, height - 15 , self._ZoomLabelTxt)               
               
                if self.isNiftiDataLoaded () :                
                    if self._nifti_volume.getImagingShown () != "Origional" :
                        qp.setPen (QtGui.QColor (0, 172, 212, 255))
                        height = int (self.height ())
                        qp.drawText(int(self.width ())-20, height - 15 , "T")               
            
                if (self.isSelected ()) :
                    oldpen = qp.pen ()            
                    qp.setPen (QtGui.QColor (0, 172, 212, 255))                        
                    newPen = qp.pen ()
                    newPen.setWidth (5)
                    qp.setPen (newPen)
                    qp.drawRect (0, 0,self.width ()-1,self.height ()-1)                       
                    qp.setPen (oldpen)                        
                
                if self.isRangeSelectionEnabled () : 
                           qp.setPen (QtGui.QColor (110,210,220,255))
                           qp.setBrush (QtGui.QColor (110,210,220,100))      
                                             
                           width,height  = self.getRangeSelectionDim ()
                           
                           #use last mouse coordinate, convert back and forth from screen to image to correct for voxel scaling
                           try:  
                               _px, _py = self._lastMouseSystemCoordinate #self.transformPointFromImageToScreen (px,py)
                           except:
                               _px, _py = 0,0 # Mouse may not have been initalized yet.  if so defualt to 0,0,
                               
                           _px, _py = self.transformPointFromScreenToImage (_px, _py)
                           halfPatchX = int (width/2)
                           halfPatchY = int (height/2)
                           _px = max (halfPatchX, _px)
                           _py = max (halfPatchY, _py)
                           _px = max (min ( self._getPixmapWidth()  - int (width - halfPatchX), _px), 0)
                           _py = max (min ( self._getPixmapHeight() - int (height - halfPatchY), _py), 0)                  
                           lpx = _px - int (width/2)
                           lpy = _py - int (height/2)                   
                           lpx = max (lpx, 0)
                           lpy = max (lpy, 0)
                           
                           rpx = min (lpx+width, self._getPixmapWidth() )
                           rpy = min (lpy + height,  self._getPixmapHeight())
                           
                           ulb, urb = self.transformPointFromImageToScreen (lpx, lpy)
                           llb, lrb = self.transformPointFromImageToScreen (rpx, rpy)
            
                           qp.drawRect (ulb, urb,llb - ulb, lrb - urb)                   
                           if (len (self._HiddenLabelTxt) > 0) :
                               YPos = 60
                           else:
                               YPos = 40
                           if (len (self._LockedLabelTxt) > 0) :
                               YPos += 20 
                           qp.drawText(10, YPos , "Select patch or press 'esc' to cancel.")                                      
                           #print ((ul, ur,ll-ul, lr-ur))        
                           
                elif (self._PenSettings is not None) :                            
                   if self._PenSettings.getPenTool () == "Draw" : 
                       if (self._mX  is not None) :               
                           qp.setPen (QtGui.QColor (200,200,200,200))
                           qp.setBrush (QtGui.QColor (0,0,0,0))      
                           
                           settings = self._PenSettings 
                           radius = settings.getPenSize ()
                           #width = radius + radius
                           #height = width
                           #print ((px-radius,py-radius,px+radius,py+radius))
                           ul, ur = self.transformPointFromImageToScreen (0,0)
                           oneX, oneY = self.transformPointFromImageToScreen (1,1)
                           oneX = oneX - ul
                           oneY = oneY - ur
                           oneX = max (0, oneX)
                           oneY = max (0, oneY)
                      
                           radiusX = radius 
                           radiusY = radius
                           if self._PenSettings.getNormalizePenDimensionsToVoxelDimensions () and self._nifti_volume is not None :                                                                              
                               axis = self.getSliceAxis ()
                               xDim, yDim, zDim = self._nifti_volume.getNormalizedVoxelSize ()                                           
                               if axis == "Z" :
                                   pX = xDim
                                   pY = yDim
                               elif axis == "Y" :
                                    pX = xDim
                                    pY = zDim
                               elif axis == "X" :
                                    pX = yDim
                                    pY = zDim                    
                               if pX != pY : 
                                    size  = float(radius)
                                    if pX < pY :                            
                                        penXDim = max (int (size),1)                                                        
                                        penYDim = max (1, int (size / float (pY)))
                                        
                                    else:                            
                                        penYDim = max (1, int (size))                            
                                        penXDim = max (1, int (size / float (pX)))                        
                                    radiusX= penXDim 
                                    radiusY= penYDim 
                                    
                               
                           
                           ll, lr = self.transformPointFromImageToScreen (radiusX,radiusY)
                               
                           ll = ll - ul
                           lr = lr - ur
                           
                           #use last mouse coordinate, convert back and forth from screen to image to correct for voxel scaling
                           _px, _py = self._lastMouseSystemCoordinate #self.transformPointFromImageToScreen (px,py)
                           _px, _py = self.transformPointFromScreenToImage (_px, _py)
                           _px, _py = self.transformPointFromImageToScreen (_px, _py)
                           
                           ul = _px - ll
                           ur = _py - lr
                           
                           ll += _px
                           lr += _py
                           #height *=  _vVoxelScale 
                           #width  *=  _hVoxelScale 
                           x1, y1, width, height = ul, ur,ll-ul + oneX + 1, lr-ur + oneY + 1
                           try:
                               pen_pixmap = settings.getPenPixmap (self.getNIfTIVolume (), self, width, height)
                           except:
                               print ("Creating mask from pen failed.")
                               pen_pixmap = None
                           if pen_pixmap is not None :
                               oldMode = qp.compositionMode ()                           
                               qp.setCompositionMode(QtGui.QPainter.CompositionMode_SourceOver)
                               qp.drawPixmap(x1 - 1, y1 - 1, pen_pixmap)
                               #qp.setCompositionMode(QtGui.QPainter.CompositionMode_Overlay)
                               #qp.drawPixmap(dpx, dpy, pen_pixmap)
                               #qp.setCompositionMode(QtGui.QPainter.CompositionMode_ColorDodge)
                               #qp.drawPixmap(dpx, dpy, pen_pixmap)
                               qp.setCompositionMode (oldMode)
                           elif (settings.isCircle ()) :
                               x1, y1, width, height = ul+oneX, ur+oneY,ll-ul + 1-oneX, lr-ur + 1 - oneY
                               qp.drawEllipse (x1, y1, width, height)
                           else:
                               #x1, y1, width, height = ul, ur,ll-ul + oneX + 1, lr-ur + oneY + 1
                               qp.drawRect (x1, y1, width, height)
                           self._lastPenDraw = (x1-1,y1-1, x1 +width+2, y1 + height+2)
                       elif not self.isSelected () and self._visSettings is not None and self._visSettings.moveToCoordinatDuringPaint() and not self.getDrawCoordinateIndicator () and self._projectDataset.getApp ().mouseButtons() != QtCore.Qt.NoButton :
                           self._drawSelectedCoordinateIndicator (qp)    
                            
                qp.end ()  
                    
            
            qp = QtGui.QPainter (self)
            qp.drawPixmap (0,0, self._OffScreenBuffer)
            qp.end ()
        finally:
            if self.isRangeSelectionEnabled () or (self._PenSettings is not None and self._PenSettings.getPenTool () == "Draw") :
                self.setCursor(QtCore.Qt.BlankCursor)
            else:
                self.setCursor(QtCore.Qt.ArrowCursor)
        #QtWidgets.QWidget.paintEvent (self, event)
        #if (CallZoomListener) :
        #    self._callZoomListener ()        
    
    
        

    def _callMousePressedListner (self, x, y):        
        for listner in self._MousePressedListners :
            if (listner (x, y, self)):
                return True
        return False
   
    def _callMouseReleasedListner (self, x, y):
        for listner in self._MouseReleasedListners :
            if (listner (x, y, self.getSliceAxis ())):
                return True
        return False
     
    def getDrawOffset (self) :
        return self._DrawOffSetX, self._DrawOffSetY
    
    def addMousePressedListener (self, listner):
       if  (listner not in self._MousePressedListners) :
           self._MousePressedListners.append (listner)
       
    def addMouseReleasedListner (self, listner):
        if (listner not in self._MouseReleasedListners) :
            self._MouseReleasedListners.append (listner)    
            
    def addMouseDblClickListener (self, listener) :
        if (listener not in self._MouseDblClickListners) :
            self._MouseDblClickListners.append (listener)    
        
    def mouseDoubleClickEvent( self, event ):
         if (not self.isNiftiDataLoaded ()) :
                return
         self._MousePressedInitalized = False       
         x = event.pos().x()
         y = event.pos().y()
         self._lastMouseSystemCoordinate = (x,y)
         x, y = self.convertMousePointToImageCoordinates (self._lastMouseSystemCoordinate) 
         self._MouseMoved = False         
         self._MouseMoveLineList = [(x,y)]    
         self.clearTemporaryDrawLine ()         
         for callback in self._MouseDblClickListners :
             callback (x,y)
    
    def getMouseButtonPressed (self) :
       return self._MousePressedButton
   
    def mousePressEvent (self, event) :
         #print ("Mouse Down")          
         if (not self.isNiftiDataLoaded ()) :
                return
         self.setSelected (True)
         self._MousePressedButton = event.buttons()            
         self._MousePressedInitalized = False       
         x = event.pos().x()
         y = event.pos().y()
         #print (("Raw Mouse Pressed", x, y))
         self._lastMouseSystemCoordinate = (x,y)
         x, y = self.convertMousePointToImageCoordinates (self._lastMouseSystemCoordinate) 
         #print (("Transform Mouse Pressed", x, y))
         self._MouseMoved = False
         self._MouseMovedEventList = []
         self._MouseMoveLineList = [(x,y)]    
         self.clearTemporaryDrawLine ()
         if (self._callMousePressedListner (x, y)):
             return
         self._MousePressedInitalized = True
         self._MouseDownPos = event.pos ()         
        
    def mouseReleaseEvent (self, event) :
         if (self._cancelMouseMove)  :
            self._cancelMouseMove = False
            return         
         if (not self.isNiftiDataLoaded ()) :
                return         
         #print ("Mouse Released")  
         x = event.pos().x()
         y = event.pos().y()
         self._lastMouseSystemCoordinate = (x,y)
         x, y = self.convertMousePointToImageCoordinates  (self._lastMouseSystemCoordinate)  
         if (self._callMouseReleasedListner (x, y)):
             return
        
         if (not self._MousePressedInitalized) :
             if  self._MouseMoved  :
                 self.endMouseMove (event)   
             return
             
         self._MousePressedInitalized = False
         if  not self._MouseMoved :         
            clickEventHandled = self.mouseClickEvent (event)                
            self.endMouseMove (event, ClickEventHandled = clickEventHandled) 
         else:
            self.endMouseMove (event)     
   
    def convertMousePointToImageCoordinates (self,pos):
        #print ("Convert In %d, %d" % (pos[0], pos[1]) )
        newXCoordinate, newYCoordinate = self.transformPointFromScreenToImage (pos[0], pos[1])
        #
        #(pos[0] - self._DrawOffSetX) / self._DrawWidthScale        
        #newYCordinate = (pos[1] - self._DrawOffSetY) / self._DrawHeightScale    
        newYCoordinate = max (newYCoordinate, 0)
        newXCoordinate = max (newXCoordinate, 0)                
        if self._SliceAxis == 'Z' :             
            xDim, yDim = self.getSliceShape ()
            newXCoordinate = min (newXCoordinate, xDim -1)
            newYCoordinate = min (newYCoordinate, yDim -1)
            return (newXCoordinate, newYCoordinate)
        elif self._SliceAxis == 'X' : 
            xDim, yDim, zDim = self._nifti_volume.getSliceDimensions  ()
            newZCoordinate = newYCoordinate    
            newYCoordinate = newXCoordinate                   
            newYCoordinate = min (newYCoordinate, yDim -1)
            newZCoordinate = min (newZCoordinate, zDim -1)        
            #print ("Mouse x")
            #print (newYCoordinate, newZCoordinate)
            return (newYCoordinate, newZCoordinate)
        elif self._SliceAxis == 'Y' :   
            xDim, yDim, zDim = self._nifti_volume.getSliceDimensions  ()
            newZCoordinate = newYCoordinate    
            newXCoordinate = min (newXCoordinate, xDim -1)
            newZCoordinate = min (newZCoordinate, zDim -1)
            #print ("Mouse Y")
            #print (newXCoordinate, newZCoordinate)
            return (newXCoordinate, newZCoordinate)   
   
    def clearOldPen (self, px, py, radius):        
        if px is None :
            if self._lastPenDraw is not None :
                x1,y1, x2,y2 = self._lastPenDraw
                self._lastPenDraw = None
                self.update (x1-1, y1-1, x2- x1 + 3, y2-y1 + 3)             
        else:
            radius += 1
            ul, ur = self.transformPointFromImageToScreen (px-radius,py-radius)
            ll, lr = self.transformPointFromImageToScreen (px+radius,py+radius)
            ul, ll = min (ul,ll), max (ul,ll)
            ur, lr = min (ur,lr), max (ur,lr)
            ul -= 1
            ur -= 1
            ll += 1
            lr += 1
            if self._lastPenDraw is not None :
                x1,y1, x2,y2 = self._lastPenDraw
                ul = min (x1, ul)
                ur = min (y1, ur)
                ll = max (x2, ll)
                lr = max (y2, lr)
                self._lastPenDraw = None            
            self.update (ul-1, ur-1, ll- ul + 3, lr-ur + 3)


        
    
    def triggerSliceChangeMouseMoveUpdate (self, index):
        #print ("Triggered Slice Change")
        length = len (self._MouseMovedEventList)
        if length >= 1 :            
            mostRecentEvent, currentCoordinate = self._MouseMovedEventList.pop()
            newEventList = [mostRecentEvent]
        
            found = False
            while len (self._MouseMovedEventList) > 0 :
                lastvalidEvent, lastCoordiante = self._MouseMovedEventList.pop()
                self._Coordinate.setCoordinate(lastCoordiante, DisableCallback = True)
                if (self.getSliceNumber () == index) :
                    found = True
                    break
            if (found) : 
                self.endMouseMove    (lastvalidEvent)
                
            self._MouseMovedEventList = newEventList
            self._Coordinate.setCoordinate(currentCoordinate, DisableCallback = True)
            self.mousePressEvent (newEventList[0])
            self.mouseMoveEvent  (newEventList[0])
            
        
    def mouseMoveEvent (self, event) :        
        if (not self.isNiftiDataLoaded () or self._cancelMouseMove) :
                return
        #print ("MouseMove")            
        self._MouseMovedEventList.append ((event, self._Coordinate.getCoordinate ()))        
        pos = event.pos ()
        self._lastMouseSystemCoordinate = (pos.x(),  pos.y ())
        currentPos = self.convertMousePointToImageCoordinates (self._lastMouseSystemCoordinate)
        settings = self._PenSettings 
        if self.isRangeSelectionEnabled():                                    
            self.update ()
                            
        if (settings is not None) :
            self._mX, self._mY  = currentPos
            self.clearOldPen (self._mX, self._mY, settings.getPenSize ())
        else:
            self._clearOldMouseCursor () 
        
        if (event.buttons () == QtCore.Qt.NoButton) :                        
            self._callMouseMoveListener ([currentPos], "MouseMoveButtonUp", False)
        else:                        
            endoflist = len (self._MouseMoveLineList) - 1
            if (endoflist >= 0) :
                lastPos = self._MouseMoveLineList[endoflist]
                firstPos = self._MouseMoveLineList[0]    
                currentPos = self.convertMousePointToImageCoordinates ((pos.x(),  pos.y ()))
                if ((firstPos[0] - currentPos[0])**2 +  (firstPos[1] - currentPos[1])**2 >= 25) :
                    self._MouseMoved = True                     
                if (currentPos != lastPos):           
                   self.setSelected (True)
                   self._MouseMoveLineList.append (currentPos)
                   if (len (self._MouseMoveLineList) >= 2) :
                       self._callMouseMoveListener (self._MouseMoveLineList, "MouseMove", False)            
     
    def leaveEvent(self, event) :
        self._clearOldMouseCursor ()
        
    def endMouseMove (self, event, ClickEventHandled = False) :
        if (not self.isNiftiDataLoaded ()) :
            return
        #print ("endMouseMove")
        pos = event.pos ()
        self._lastMouseSystemCoordinate = (pos.x(),  pos.y ())
        endoflist = len (self._MouseMoveLineList) - 1
        lastPos = self._MouseMoveLineList[endoflist]        
        currentPos = self.convertMousePointToImageCoordinates (self._lastMouseSystemCoordinate)
        firstPos = self._MouseMoveLineList[0]    
        if ((firstPos[0] - currentPos[0])**2 +  (firstPos[1] - currentPos[1])**2 >= 25) :            
            self._MouseMoved = True                     
        if (currentPos != lastPos):        
           self._MouseMoveLineList.append (currentPos)
        #if (len (self._MouseMoveLineList) >= 2) :        
        self._callMouseMoveListener (self._MouseMoveLineList, "EndMouseMove", ClickEventHandled)
        self._MouseMovedEventList = []
    
    def cancelMouseMove (self) :
        self._MouseMoveLineList = []  
        self.clearTemporaryDrawLine ()
        if (self._MousePressedInitalized) :
            self._MousePressedInitalized = False 
            self._MouseMoved = False
            self._cancelMouseMove = True            
        self._MouseMovedEventList = []
        self.update ()
        
    def clearTemporaryDrawLine (self):
        self._TemporaryLine = []
        
    def setTemporaryDrawLine (self, tLine, tLineColor):
        self._TemporaryLine = tLine
        self._TemporaryLineColor = tLineColor
        
    def addMouseMoveListener (self, callback) :
        if (callback not in self._MouseMoveSliceListener) :
            self._MouseMoveSliceListener.append (callback)

    def _callMouseMoveListener (self, line, eventtype, ClickEventHandled) :
        for i in self._MouseMoveSliceListener :
            i (line, eventtype, self, self._MousePressedButton, ClickEventHandled)
    
        
    def convertMouseMoveImageCoordiantesToXYZImageCoordinates (self, mouseMoveX, mouseMoveY):
        if (self._Coordinate is None) :    
            print ("Error undefined coordinate")
            return (0,0,0)            
        if self.isAxisProjectionRotationEnabled () :
            newXCoordinate, newYCoordinate, newZCoordinate = self.transformPlaneCoordinateToVolumeCoordinates (mouseMoveX, mouseMoveY)
        else:
            newXCoordinate, newYCoordinate, newZCoordinate = self._Coordinate.getCoordinate ()            
            if self._SliceAxis == 'X' : 
                newYCoordinate = mouseMoveX
                newZCoordinate = mouseMoveY
                newXCoordinate = int (newXCoordinate)
            elif self._SliceAxis == 'Y' : 
                newXCoordinate = mouseMoveX
                newZCoordinate = mouseMoveY
                newYCoordinate = int (newYCoordinate)
            elif self._SliceAxis == 'Z' : 
                newXCoordinate = mouseMoveX
                newYCoordinate = mouseMoveY            
                newZCoordinate = int (newZCoordinate)        
        return (newXCoordinate, newYCoordinate, newZCoordinate)
        
    def mouseClickEvent (self, event) :
            if (not self._hasSliceListener ()) :
                return False
            
            if (not self.isNiftiDataLoaded ()) :
                return False
            #print ("click Event")
            #if event.buttons () == QtCore.Qt.LeftButton :     
            if (self._Coordinate is None) :    
                #print ("Error undefined coordinate")
                return False            
            pos = event.pos ()                            
            self._lastMouseSystemCoordinate = (pos.x(),  pos.y ())
            
            newXCoordinate, newYCoordinate, newZCoordinate = self._Coordinate.getCoordinate ()            
            if self._SliceAxis == 'Z' :
                newXCoordinate, newYCoordinate = self.convertMousePointToImageCoordinates (self._lastMouseSystemCoordinate) 
                if self.isAxisProjectionRotationEnabled () :
                    newXCoordinate, newYCoordinate, newZCoordinate = self.transformPlaneCoordinateToVolumeCoordinates (newXCoordinate, newYCoordinate)
                    #print ("New Z Cord " + str (newZCoordinate))
                else:
                    newZCoordinate = int (newZCoordinate)    
            elif self._SliceAxis == 'X' : 
                newYCoordinate, newZCoordinate = self.convertMousePointToImageCoordinates (self._lastMouseSystemCoordinate)                 
                newXCoordinate = int (newXCoordinate)                
            elif self._SliceAxis == 'Y' : 
                newXCoordinate, newZCoordinate = self.convertMousePointToImageCoordinates (self._lastMouseSystemCoordinate)                 
                newYCoordinate = int (newYCoordinate)                
            #print (newXCoordinate, newYCoordinate, newZCoordinate)
            return self._callSliceListener (newXCoordinate, newYCoordinate, newZCoordinate, True)
    
    def getLastSystemMouseCoordinate (self) :
       return self._lastMouseSystemCoordinate
   
    def transformControlPointIndexToImage (self, index) :
        return self._ViewPortTransformation.transformControlPointIndexToImage (index)
    
    def getVolumeMask (self) :
        return self._overlayNiftiVolumeMask
        
    def setVolumeMask (self, niftiVolumeMask, colorTable = None) :
        if  not (self._overlayNiftiVolumeMask is niftiVolumeMask) :
            self._overlayNiftiVolumeMask = niftiVolumeMask
            self._overlayNiftiVolumeMaskColorTable = colorTable
            self._UpdateSlice()
        
                
         
           

