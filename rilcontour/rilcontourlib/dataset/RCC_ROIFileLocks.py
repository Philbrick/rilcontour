import os
from six import *
from rilcontourlib.util.FileUtil import FileUtil
from rilcontourlib.util.DateUtil import TimeUtil, DateUtil
import pickle
from uuid import uuid4
import json
import time
import multiprocessing

class PLockDataWrapper :
    def __init__ (self, logEntry, lockFilePath) :
        self._log = logEntry
        self._LockFilePath = lockFilePath
        
    def getPLockUID (self) :
        return self._log["PLockUID"]
    
    def getUserName (self) :
        return self._log["UserName"]
    
    def getUserUID (self) :
        return self._log["UserUID"]
    
    def getGroupName (self) :
        return self._log["GroupName"]
    
    def getCreatedTime (self) :
        return self._log["Created"]["Time"]
    
    def getCreatedDate (self) :
        return self._log["Created"]["Date"]
    
    def isClosed (self) :
        return self._log["Closed"] is not None 

    def getClosedTime (self) :
        return self._log["Closed"]["Time"]
    
    def getClosedDate (self) :
        return self._log["Closed"]["Date"]
    
    def getClosedVersionUID (self) :
        return self._log["ClosedVersion"]
    
    def getOpenedVersionUID (self) :
        return self._log["OpenedVersion"]
    
    def getDocumentUID (self) :
        return self._log["DocumentUID"]
    
    def getDocumentPath (self) :
       return FileUtil.changeExtention (self._LockFilePath, ".plock", "_ROI.txt")
    
    def getPLockFilePath (self) :
       return self._LockFilePath
   
    def getNonPLockFilePath (self): 
        return FileUtil.changeExtention (self._LockFilePath,".plock",".lock" ) 
   
    def getCloseComment (self) :
        if "CloseComment" not in self._log or self._log["CloseComment"] is None:
            return ""
        return self._log["CloseComment"]
    
    def getOpenComment (self) :
        if "OpenComment" not in self._log or self._log["OpenComment"] is None:
            return ""
        return self._log["OpenComment"]
  
class PersistentFSLock :    
    
    @staticmethod
    def getPersistentLockPathForROIFilePath (roiDatasetPath) :
        return FileUtil.changeExtention (roiDatasetPath, "_ROI.txt",".plock")
    
    @staticmethod
    def isFilePathAPLockFile (Path) :
        return Path.endswith (".plock") 
    
    @staticmethod 
    def _lowerStr (text) :
        if text is None :
            return text
        return text.lower ()
    
    @staticmethod 
    def _createLockLog (UserName, UserUID, FileHandle, OpenedFileVersionUID, GroupName = None):
        logEntry = {}        
        logEntry["PLockUID"] = "PLockUID_"+ str (uuid4()) + "_" + str (time.time ())
        logEntry["UserName"] = PersistentFSLock._lowerStr (UserName)
        logEntry["UserUID"] =  UserUID
        logEntry["GroupName"] =  PersistentFSLock._lowerStr (GroupName)
        logEntry["Created"] = {"Time" : TimeUtil.getTimeNow (), "Date" : DateUtil.today ()}  
        logEntry["Closed"]  =  None 
        logEntry["ClosedVersion"] = None
        logEntry["OpenedVersion"] = OpenedFileVersionUID
        logEntry["DocumentUID"] = FileHandle.getDocumentUID ()
        logEntry["CloseComment"] = None
        logEntry["OpenComment"] = None
        return logEntry
    
      
    
    @staticmethod 
    def _UIDNotEqual (uid1, cdata) :
        try :
            if cdata is None or "UserUID" not in cdata :
                return False
            uid2 =  cdata["UserUID"]
            if uid1 is None or uid2 is None:
                return False
            return uid1 != uid2
        except:
            return False
    
        
    @staticmethod 
    def _NameEqual (UserName, UserUID, mostRecentEntry) :
        try :
            #(mostRecentEntry["UserName"] == UserName and (mostRecentEntry["UserUID"] is None or UserUID is None))            
            if mostRecentEntry is None  :                
                return False                                     
            #if UserUID is not None and "UserUID" in mostRecentEntry and mostRecentEntry["UserUID"] is not None :
            #    return UserUID == mostRecentEntry["UserUID"]
            if "UserName" not in mostRecentEntry :
                return False
            name =  mostRecentEntry["UserName"]
            if name is None or UserName is None :
                return False
            if name != UserName :
                return False            
            return True            
        except:
            return False
        
    @staticmethod
    def getPreviousPlockHolder (data, username, userUID) :
        if data is None :
            return None
        username = PersistentFSLock._lowerStr (username)
        for index in range (len (data) -1, -1, -1) :
            cdata = data[index]
            if cdata is not None and "UserName" in cdata :
                if PersistentFSLock._UIDNotEqual (userUID, cdata) :
                    return cdata["UserName"]
                if username is not None and  cdata["UserName"] is not None and cdata["UserName"] != username :
                    return cdata["UserName"]
        return None 
    
    
    
    @staticmethod 
    def _createFileHeader (data, FileHandle):
        header = {}
        header["ROIPath"] = FileHandle.getPath ()
        header["NIfTIPath"] =  FileHandle.getNIfTIPath ()        
        return header

    @staticmethod 
    def saveFileData (path, data, FileHandle):
        try:
            writetofile = (PersistentFSLock._createFileHeader (data, FileHandle), data)
            with open (path, "wb") as outfile :     
                pickle.dump (writetofile, outfile, protocol = 2)            
            FileUtil.setFilePermissions (path, FileHandle.getFilePermissions ())
            try :
                dataset = FileHandle.getProjectDataset()
                if dataset is not None :
                    niftiInterface = dataset.getNIfTIDatasetInterface ()
                    if niftiInterface is not None :
                        eventHandler = niftiInterface.getFileSysetmEventHandler ()
                        if eventHandler is not None :
                            eventHandler.processEvent (path)                    
            except:
                pass
            return True
        except:
            return False

    @staticmethod 
    def loadFileData (path):
        try: 
            with open (path, "rb") as infile :
                data = pickle.load (infile)
            return data[1]
        except:
            return None
    
    @staticmethod 
    def getCurrentPLockFromPath (path) :
       try :
           PLockData = PersistentFSLock.loadFileData  (path)
           return PersistentFSLock.getCurrentPlock (PLockData)
       except:
           return None
    
    @staticmethod 
    def getPlockCount (path) :
        try:
            return len (PersistentFSLock.loadFileData  (path))
        except:
            return 0
        
    @staticmethod 
    def getCurrentPlock (PLockData = None) :
        try :
            if PLockData is None or len (PLockData) == 0 :
                return None
            return PLockData[-1]
        except:
            return None
    
    @staticmethod
    def _getLockFilePaths (nonPersistentFilePath) :
        if nonPersistentFilePath.endswith (".plock"):
                persistentLockfilePath = nonPersistentFilePath
                nonPersistentFilePath  = FileUtil.changeExtention (persistentLockfilePath,".plock",".lock" )
        else:
                persistentLockfilePath = FileUtil.changeExtention (nonPersistentFilePath,".lock",".plock" )
        return nonPersistentFilePath, persistentLockfilePath
    
    @staticmethod 
    def getPersistentLockFileName (pfile) :
        if pfile.endswith (".plock") :
            return pfile
        if pfile.endswith (".lock") :
           return FileUtil.changeExtention (pfile,".lock",".plock" )  
        return None
    
    @staticmethod
    def getPLockFileHistory (persistentLockfilePath, PLockData = None) :
        persistentLockfilePath = PersistentFSLock.getPersistentLockFileName (persistentLockfilePath)
        if persistentLockfilePath is None :
            return []
        if PLockData is None :
             PLockData = PersistentFSLock.loadFileData (persistentLockfilePath)      
        resultlist = []
        if PLockData is not None :
            for data in PLockData :
                resultlist.append (PLockDataWrapper (data, persistentLockfilePath))
        return resultlist    
    
    @staticmethod 
    def isFileLockedByUser (persistentLockfilePath, UserName, UserUID, PLockData = None):
        persistentLockfilePath = PersistentFSLock.getPersistentLockFileName (persistentLockfilePath)
        if persistentLockfilePath is None :
            return False
        if PLockData is None :
             PLockData = PersistentFSLock.loadFileData (persistentLockfilePath)      
        if PLockData is None or len (PLockData) <= 0:
            return False
        mostRecentEntry = PLockData[-1] 
        if mostRecentEntry["Closed"] is not None :
            return False
        return PersistentFSLock._NameEqual (PersistentFSLock._lowerStr (UserName), UserUID, mostRecentEntry) 
       
    
    @staticmethod 
    def _getLockStatus (mostRecentEntry, UserName, UserUID, UserGroupList, acessUnlockedFiles):
        if mostRecentEntry is None or mostRecentEntry["Closed"] is not None :
            if acessUnlockedFiles :
                return "Unlocked"
            return "OtherUserLocked"        
        if PersistentFSLock._NameEqual (PersistentFSLock._lowerStr (UserName), UserUID, mostRecentEntry) :            
            return "UserLocked"     
        if (mostRecentEntry["GroupName"] in UserGroupList and mostRecentEntry["UserUID"] is None and mostRecentEntry["UserName"] is None) : 
            return "UserGroupLocked"     
        if (mostRecentEntry["UserName"] in UserGroupList and mostRecentEntry["UserUID"] is None) : 
            return "UserGroupLocked"
        return "OtherUserLocked"
    
    @staticmethod 
    def getCurrentUserGroupList (projectFileInterface) :
        UserGroupList = []
        UserName = PersistentFSLock._lowerStr (projectFileInterface.getUserName ())        
        multiUser = projectFileInterface.getMultiUserFileSharingManager ()        
        if multiUser is not None and multiUser.isInitalized (): 
            UserGroupList = multiUser.getUserGroups (UserName)
        return UserGroupList
            
    @staticmethod 
    def getLockStatusForCurrentUser (PfileLock, projectFileInterface, UserGroupList = None) :   
        if UserGroupList is None : 
            initalizeUserGroupIfNecessary = True
            UserGroupList = []
        else:
            initalizeUserGroupIfNecessary = False
        acessUnlockedFiles = False
        UserName = PersistentFSLock._lowerStr (projectFileInterface.getUserName ())        
        multiUser = projectFileInterface.getMultiUserFileSharingManager ()
        multiUserInitalized = False
        if multiUser is not None and multiUser.isInitalized (): 
            multiUserInitalized = True            
            if projectFileInterface.isFileUnlockingCommandLineModeEnabled ()  :
                acessUnlockedFiles = True
            else:
                acessUnlockedFiles = multiUser.canLockUnlockedFiles (UserName)            
            if PfileLock is None :
                if acessUnlockedFiles :
                    return "Unlocked"
                else:
                    return "OtherUserLocked"
        elif PfileLock is None :
            return "Unlocked"
        if multiUserInitalized and initalizeUserGroupIfNecessary :
            UserGroupList = multiUser.getUserGroups (UserName)
        elif not multiUserInitalized :
            acessUnlockedFiles = True
        UserUID = projectFileInterface.getUserUID ()       
        lockStatus = PersistentFSLock._getLockStatus (PfileLock, UserName, UserUID, UserGroupList, acessUnlockedFiles)        
        return lockStatus
        
        
    @staticmethod 
    def getCurrentLockOwnerString (PLockData, multiUserManager) :
        try :
            if (PLockData is None or len (PLockData) == 0) :
                return "Unlocked"
            logEntry = PLockData[-1] 
            if logEntry["UserName"] is not None :
                userName = multiUserManager.getUserNickname (logEntry["UserName"])
                try:
                    if logEntry["Closed"] is not None :
                        return "Lock closed by: %s on %s at %s" % (userName, DateUtil.dateToString (logEntry["Closed"]["Date"]) , TimeUtil.timeToString (logEntry["Closed"]["Time"])) 
                    else:
                        return "Lock opened by: %s on %s at %s" % (userName, DateUtil.dateToString (logEntry["Created"]["Date"]) , TimeUtil.timeToString (logEntry["Created"]["Time"]))
                except :
                    if logEntry["Closed"] is not None :
                        return "Lock closed by: %s" % (userName)
                    else:
                        return "Lock opened by: %s" % (userName)
            elif logEntry["GroupName"] is not None :
                try:
                    if logEntry["Closed"] is not None :
                        return "Lock closed by: group %s on %s at %s" % (logEntry["GroupName"], DateUtil.dateToString (logEntry["Closed"]["Date"]) , TimeUtil.timeToString (logEntry["Closed"]["Time"])) 
                    else:
                        return "Lock opened by: group %s on %s at %s" % (logEntry["GroupName"], DateUtil.dateToString (logEntry["Created"]["Date"]) , TimeUtil.timeToString (logEntry["Created"]["Time"]))
                except :
                    if logEntry["Closed"] is not None :
                        return "Lock closed by: group %s" % (logEntry["GroupName"])
                    else:
                        return "Lock opened by: group %s" % (logEntry["GroupName"])
        except :
            pass
        return "A exception occured trying to determine the resource lock state"
    
    @staticmethod 
    def _getstr (text, defaultxt) :
        if text is None :
            return defaultxt
        return str (text)
    
           
    @staticmethod     
    def revertPersistentFileLock (nonPersistentFilePath, UserName, FSUID, UserUID, ROIFileHandle, SuperUserOverride = False ):
        UserName = PersistentFSLock._lowerStr (UserName)
        try :
            nonPersistentFilePath, persistentLockfilePath =  PersistentFSLock._getLockFilePaths (nonPersistentFilePath)            
                 
            if not NonPersistentFSLock.isFileLocked (nonPersistentFilePath, FSUID) :
                print ("Non persistent file lock not must be held")
                return False            
            dataLog = PersistentFSLock.loadFileData (persistentLockfilePath)
            if dataLog is None or len (dataLog) <= 0:
                return True
            mostRecentEntry = dataLog[-1]  
            if mostRecentEntry["Closed"] is not None :
                return True
            if  PersistentFSLock._NameEqual (UserName, UserUID, mostRecentEntry) :                        
                ROIFileHandle.removeFileVersionsAfter (mostRecentEntry["OpenedVersion"])
                del dataLog[-1]
                if len (dataLog) > 0 :
                    mostRecentEntry = dataLog[-1]  
                    mostRecentEntry["Closed"] = None
                    mostRecentEntry["ClosedVersion"]  = None
                    mostRecentEntry["CloseComment"] = None
                return PersistentFSLock.saveFileData (persistentLockfilePath, dataLog, ROIFileHandle)
            elif SuperUserOverride: 
                ROIFileHandle.removeFileVersionsAfter (mostRecentEntry["OpenedVersion"])
                del dataLog[-1]
                if len (dataLog) > 0 :
                    mostRecentEntry = dataLog[-1]  
                    mostRecentEntry["Closed"] = None
                    mostRecentEntry["ClosedVersion"]  = None
                    mostRecentEntry["CloseComment"] = None
                return PersistentFSLock.saveFileData (persistentLockfilePath, dataLog, ROIFileHandle)
            return False
        except:
           return False

    @staticmethod
    def forceClosePersistentFSLock (nonPersistentFilePath, UserName, FSUID, UserUID, ROIFileHandle, Comment):
       try :
            nonPersistentFilePath, persistentLockfilePath =  PersistentFSLock._getLockFilePaths (nonPersistentFilePath)             
            
            if not NonPersistentFSLock.isFileLocked (nonPersistentFilePath, FSUID) :
                print ("Non persistent file lock  must be held")
                return False               
            dataLog = PersistentFSLock.loadFileData (persistentLockfilePath)
            if dataLog is None or len (dataLog) <= 0:
                return True
            mostRecentEntry = dataLog[-1]  
            if mostRecentEntry["Closed"] is not None :
                return True
            VersionUID = None
            if ROIFileHandle.canVersionFile () :
                version = ROIFileHandle.versionFile (Comment = "")
                if version is not None :
                    VersionUID = version.getVersionUID ()
            mostRecentEntry["Closed"] = {"Time" : TimeUtil.getTimeNow (), "Date" : DateUtil.today ()}  
            mostRecentEntry["ClosedVersion"] = VersionUID
            mostRecentEntry["CloseComment"] = Comment
            ROIFileHandle.removeUncessaryFileVersionsBetween (mostRecentEntry["OpenedVersion"], mostRecentEntry["ClosedVersion"])
            return PersistentFSLock.saveFileData (persistentLockfilePath, dataLog, ROIFileHandle)                       
       except:
            return False
       
        
    @staticmethod 
    def createPersistentFSLock (nonPersistentFilePath, UserName, FSUID, UserUID, UserGroupList, ROIFileHandle, VersionComment = None, ForceVersion = False):
        if VersionComment is None :
            VersionComment = ""
        UserName = PersistentFSLock._lowerStr (UserName)
        try :
            nonPersistentFilePath, persistentLockfilePath =  PersistentFSLock._getLockFilePaths (nonPersistentFilePath)       
            
            if not NonPersistentFSLock.isFileLocked (nonPersistentFilePath, FSUID) :
                print ("Non persistent file lock  must be held")
                return False
            if os.path.exists (persistentLockfilePath) :
                dataLog = PersistentFSLock.loadFileData (persistentLockfilePath)
                if dataLog is None :
                   dataLog = [] 
                if PersistentFSLock.isFileLockedByUser (persistentLockfilePath, UserName, UserUID, PLockData = dataLog) :
                    return True
            else  :
                try :
                    lockfile = os.open (persistentLockfilePath, os.O_CREAT | os.O_EXCL )
                    os.close (lockfile)
                    FileUtil.setFilePermissions (persistentLockfilePath, ROIFileHandle.getFilePermissions ())
                    dataLog = [] 
                except:
                     print ("A error occured creating the persistent lock file: %s" % persistentLockfilePath) 
                     interface = ROIFileHandle.getProjectFileInterface ()
                     if not interface.weakLocksCommandLineParameterEnabled () :
                         return False
                     else:
                           try :
                               with open (persistentLockfilePath,"wt") as lockfile :
                                   pass      
                               FileUtil.setFilePermissions (persistentLockfilePath, ROIFileHandle.getFilePermissions ())
                           except:                               
                               print ("A error occured creating the weak lock file: %s" % persistentLockfilePath) 
                               return False
                  
            groupName = None
            VersionUID = None
            if len (dataLog) > 0:
               mostRecentEntry = dataLog[-1]  
               if mostRecentEntry["Closed"] is None :
                   if PersistentFSLock._NameEqual (UserName, UserUID, mostRecentEntry) :
                       mostRecentEntry["UserUID"] = UserUID   # update with UID
                       return PersistentFSLock.saveFileData (persistentLockfilePath, dataLog, ROIFileHandle)
                   elif (mostRecentEntry["UserUID"] is None and mostRecentEntry["UserName"] is None and mostRecentEntry["GroupName"] in UserGroupList) or (mostRecentEntry["UserUID"] is None and mostRecentEntry["UserName"] in UserGroupList and mostRecentEntry["GroupName"] is None) :
                       if VersionUID is None and ROIFileHandle.canVersionFile () :                            
                            version = ROIFileHandle.versionFile (Comment = VersionComment, ForceVersion = ForceVersion)
                            if version is not None :
                                VersionUID = version.getVersionUID ()
                    
                       mostRecentEntry["Closed"] = {"Time" : TimeUtil.getTimeNow (), "Date" : DateUtil.today ()}  
                       mostRecentEntry["ClosedVersion"] = VersionUID
                       mostRecentEntry["CloseComment"] = ""
                       ROIFileHandle.removeUncessaryFileVersionsBetween (mostRecentEntry["OpenedVersion"], mostRecentEntry["ClosedVersion"])
                       groupName = mostRecentEntry["GroupName"]
                   else:
                       return False        
            if VersionUID is None and ROIFileHandle.canVersionFile () :
                version = ROIFileHandle.versionFile (Comment = VersionComment, ForceVersion = ForceVersion)
                if version is not None :
                    VersionUID = version.getVersionUID ()
            dataLog.append (PersistentFSLock._createLockLog (UserName, UserUID, ROIFileHandle, VersionUID, GroupName = PersistentFSLock._lowerStr (groupName)))    
            return PersistentFSLock.saveFileData (persistentLockfilePath, dataLog, ROIFileHandle)
        except:
           return False
    
    @staticmethod    
    def _GenVersionTransferComment (UserName, AssignToUserName, Comment, multiUserMannager):
        UserName = PersistentFSLock._getstr(UserName,"Unknown")
        UserName = multiUserMannager.getUserNickname (UserName)
        Comment = PersistentFSLock._getstr (Comment, "")                
        AssignToUserName = PersistentFSLock._getstr (AssignToUserName, "Unknown")    
        AssignToUserName = multiUserMannager.getUserNickname (AssignToUserName)
        header = UserName + " assigned file to " + AssignToUserName + "<br>"
        if len (Comment) <= 0 :
            return header
        return "<br>".join([header,"<hr><b>Comment</b>", Comment.replace ("\n","<br>")])        
    
    
    @staticmethod 
    def transferPersistentLockTo (nonPersistentFilePath, UserName, FSUID, UserUID, UserGroupList, ROIFileHandle, AssignToUserName, AssignToUserUID, multiUserMannager, Comment):
        UserName = PersistentFSLock._lowerStr (UserName)
        AssignToUserName = PersistentFSLock._lowerStr (AssignToUserName)
        try :
            nonPersistentFilePath, persistentLockfilePath =  PersistentFSLock._getLockFilePaths (nonPersistentFilePath)             
                 
            if not NonPersistentFSLock.isFileLocked (nonPersistentFilePath, FSUID) :
                print ("Non persistent file lock not must be held")
                return False
            VersionUID = None
            oldGroupName = None
            dataLog = PersistentFSLock.loadFileData (persistentLockfilePath)            
            if dataLog is None or len (dataLog) <= 0 : # first time plock being created.
                dataLog = []
                if AssignToUserName == "none" : #nothing is being changed
                    return True
              
                if VersionUID is None and ROIFileHandle.canVersionFile () :
                    version = ROIFileHandle.versionFile (Comment = PersistentFSLock._GenVersionTransferComment (UserName, AssignToUserName, Comment, multiUserMannager))
                    if version is not None :
                        VersionUID = version.getVersionUID ()
                dataLog.append (PersistentFSLock._createLockLog (UserName, UserUID, ROIFileHandle, VersionUID, GroupName = None)) #if a user transfers a file that was never acquired.  Log acquisition.
                mostRecentEntry = dataLog[-1]  
                mostRecentEntry["Closed"] = {"Time" : TimeUtil.getTimeNow (), "Date" : DateUtil.today ()}  
                mostRecentEntry["ClosedVersion"] = VersionUID                
            else:        
                mostRecentEntry = dataLog[-1]  
                if mostRecentEntry["Closed"] is not None and AssignToUserName == "none"  :
                    return True
                if mostRecentEntry["Closed"] is not None and mostRecentEntry["UserName"] == AssignToUserName  :
                    return True
                if mostRecentEntry["Closed"] is not None and mostRecentEntry["UserName"] is None and mostRecentEntry["UserUID"] is None and mostRecentEntry["GroupName"] == AssignToUserName and AssignToUserUID is None :
                    return True
                
                if VersionUID is None and ROIFileHandle.canVersionFile () :
                    version = ROIFileHandle.versionFile (Comment = PersistentFSLock._GenVersionTransferComment (UserName, AssignToUserName, Comment, multiUserMannager))
                    if version is not None :
                        VersionUID = version.getVersionUID ()
                if mostRecentEntry["Closed"] is None : # plock is currently open                     
                    mostRecentEntry["Closed"] = {"Time" : TimeUtil.getTimeNow (), "Date" : DateUtil.today ()}  
                    mostRecentEntry["ClosedVersion"] = VersionUID
                    mostRecentEntry["CloseComment"] = PersistentFSLock._GenVersionTransferComment (UserName, AssignToUserName, Comment, multiUserMannager)
                    ROIFileHandle.removeUncessaryFileVersionsBetween (mostRecentEntry["OpenedVersion"], mostRecentEntry["ClosedVersion"])
                    oldGroupName = mostRecentEntry["GroupName"]
                    if mostRecentEntry["UserName"] != UserName :                          
                        dataLog.append (PersistentFSLock._createLockLog (UserName, UserUID, ROIFileHandle, VersionUID, GroupName = oldGroupName)) #if a user transfers a file that they did not own from the group to another have the user take ownship to record they moved the file
                        mostRecentEntry = dataLog[-1]  
                        mostRecentEntry["Closed"] = {"Time" : TimeUtil.getTimeNow (), "Date" : DateUtil.today ()}  
                        mostRecentEntry["ClosedVersion"] = VersionUID                
                else:
                    dataLog.append (PersistentFSLock._createLockLog (UserName, UserUID, ROIFileHandle, VersionUID, GroupName = None)) #if a user transfers a file that is closed, acquire file to create a log of the file transfer
                    mostRecentEntry = dataLog[-1]  
                    mostRecentEntry["Closed"] = {"Time" : TimeUtil.getTimeNow (), "Date" : DateUtil.today ()}  
                    mostRecentEntry["ClosedVersion"] = VersionUID                
            if AssignToUserName != "none" :
                if VersionUID is None and ROIFileHandle.canVersionFile () :
                    version = ROIFileHandle.versionFile (Comment = PersistentFSLock._GenVersionTransferComment (UserName, AssignToUserName, Comment, multiUserMannager))
                    if version is not None :
                        VersionUID = version.getVersionUID ()
                if multiUserMannager.isGroupDefined (AssignToUserName) :
                    dataLog.append (PersistentFSLock._createLockLog (None, None, ROIFileHandle, VersionUID, GroupName = AssignToUserName))
                else:
                    if oldGroupName is not None :
                        if multiUserMannager.isUserInGroup (AssignToUserName, oldGroupName) :
                            oldGroupName = None
                    dataLog.append (PersistentFSLock._createLockLog (AssignToUserName, AssignToUserUID, ROIFileHandle, VersionUID, GroupName = oldGroupName))  
                    mostRecentEntry = dataLog[-1]  
                    mostRecentEntry["OpenComment"] = Comment
            return PersistentFSLock.saveFileData (persistentLockfilePath, dataLog, ROIFileHandle)
        except:
           return False
       
    @staticmethod    
    def _genVersionCloseComment (Comment):
        Comment = PersistentFSLock._getstr (Comment, "")        
        return Comment.replace ("\n","<br>")        
        
    @staticmethod 
    def closePersistentFSLock (nonPersistentFilePath, UserName, FSUID, UserUID, ROIFileHandle, Comment, UserGroups = None):
        UserName = PersistentFSLock._lowerStr (UserName)
        try :
            nonPersistentFilePath, persistentLockfilePath =  PersistentFSLock._getLockFilePaths (nonPersistentFilePath)             
                 
            if not NonPersistentFSLock.isFileLocked (nonPersistentFilePath, FSUID) :
                print ("Non persistent file lock not must be held")
                return False
            
            dataLog = PersistentFSLock.loadFileData (persistentLockfilePath)
            if dataLog is None or len (dataLog) <= 0:
                return True
            mostRecentEntry = dataLog[-1]  
            if mostRecentEntry["Closed"] is not None :
                return True
            if PersistentFSLock._NameEqual (UserName, UserUID, mostRecentEntry) :
                VersionUID = None
                if ROIFileHandle.canVersionFile () :
                    version = ROIFileHandle.versionFile (Comment = PersistentFSLock._genVersionCloseComment (Comment))
                    if version is not None :
                        VersionUID = version.getVersionUID ()   
                groupName = mostRecentEntry["GroupName"]
                mostRecentEntry["Closed"] = {"Time" : TimeUtil.getTimeNow (), "Date" : DateUtil.today ()}  
                mostRecentEntry["ClosedVersion"] = VersionUID
                mostRecentEntry["CloseComment"] = PersistentFSLock._genVersionCloseComment (Comment)
                ROIFileHandle.removeUncessaryFileVersionsBetween (mostRecentEntry["OpenedVersion"], mostRecentEntry["ClosedVersion"])
                if UserGroups is not None :
                    if groupName not in UserGroups :
                        groupName = None
                if groupName is not None :
                    dataLog.append (PersistentFSLock._createLockLog (None, None, ROIFileHandle, VersionUID, GroupName = groupName))
                else:
                    userName, userID, groupName  = ROIFileHandle.getDefaultUserFileCheckin ()
                    if userName is not None :
                        dataLog.append (PersistentFSLock._createLockLog (userName, userID, ROIFileHandle, VersionUID, GroupName = None))
                    elif groupName is not None : 
                        dataLog.append (PersistentFSLock._createLockLog (None, None, ROIFileHandle, VersionUID, GroupName = groupName))
                return PersistentFSLock.saveFileData (persistentLockfilePath, dataLog, ROIFileHandle)
            return False
        except:
           return False
       


      
class NonPersistentFSLock :
    accessLock = multiprocessing.RLock() 
    
    @staticmethod
    def getFileLockPathForROIFilePath (roiDatasetPath) :
        return FileUtil.changeExtention (roiDatasetPath, "_ROI.txt",".lock")
    
    @staticmethod 
    def isFileLocked (lockfilePath, signature, AccessLock = None) :
        if AccessLock is None :
            AccessLock = NonPersistentFSLock.accessLock
        try :
            AccessLock.acquire()
            if lockfilePath is None or signature is None :
                return False
            if not os.path.exists (lockfilePath) :
               return False
            try :
                lockSignature = NonPersistentFSLock.getLockFile (lockfilePath, AccessLock = AccessLock)
                if lockSignature is None or "LockSignature" not in lockSignature :
                    return False
                return (lockSignature["LockSignature"] == signature)
            except:
                print ("A error occured reading the lock file") 
            return False
        finally:
           AccessLock.release()
           
    
    @staticmethod 
    def isFilePathALockFile (roiDatasetPath) :
        return roiDatasetPath.endswith (".lock")
    
    @staticmethod
    def doesLockFileExistForROIFilePath (roiDatasetPath) :
         return os.path.exists (FileUtil.changeExtention (roiDatasetPath, "_ROI.txt",".lock"))
    
    @staticmethod
    def createLockFile (lockfilePath, UserName, FSUID, UserUID, WeakLocksEnabled = False, ProjectDataset = None, FileHandleUID = None) :
       try :
           NonPersistentFSLock.accessLock.acquire()
           if not os.path.exists (lockfilePath) :
               FileUtil.createPath (lockfilePath, hasfilename = True)           
               try :
                   lockfile = os.open (lockfilePath, os.O_CREAT | os.O_EXCL )
                   os.close (lockfile)
                   fileCreationSucceeded = True
               except:
                   print ("A error occured creating the lock file: %s" % lockfilePath) 
                   fileCreationSucceeded = False
                   if WeakLocksEnabled :
                       try :
                           with open (lockfilePath,"wt") as lockfile :
                               pass                            
                           fileCreationSucceeded = True
                       except:
                           fileCreationSucceeded = False
                           print ("A error occured creating the weak lock file: %s" % lockfilePath) 
               if fileCreationSucceeded :
                   with open (lockfilePath, "wt") as lockfile :
                       lockfile.write (FSUID + "\n")
                       lockfile.write (UserUID + "\n")
                       timestr = TimeUtil.timeToString (TimeUtil.getTimeNow ())
                       datestr = DateUtil.dateToString (DateUtil.today())
                       lockfile.write ("File locked by %s at %s\n" % (UserName, timestr + " on " + datestr))
                       lockfile.write (json.dumps([FileHandleUID]))
                       
                   """dirname, _ = os.path.split (lockfilePath)
                   savedTraceBackPath = os.path.join (dirname, FileHandleUID.replace("/","")+".stack")
                   print("Saved TraceBack: "+ savedTraceBackPath)
                   with open (savedTraceBackPath, "wt") as debugStack :
                       import traceback
                       traceback.print_stack (file = debugStack)"""
           else:
               try:
                   lockSignature = NonPersistentFSLock.getLockFile (lockfilePath)
                   if lockSignature is not None and lockSignature["LockSignature"] == FSUID :
                       if "OpenFileHandles" in lockSignature :
                           if FileHandleUID not in lockSignature[ "OpenFileHandles"] :
                               lockSignature["OpenFileHandles"].append (FileHandleUID)
                               """dirname, _ = os.path.split (lockfilePath)
                               with open (os.path.join (dirname, FileHandleUID.replace("/","")+".stack"), "wt") as debugStack :
                                   import traceback
                                   traceback.print_stack (file = debugStack)"""
                               
                       FilePermissions = FileUtil.getFilePermissions (lockfilePath)                       
                       with open (lockfilePath, "wt") as lockfile :
                           lockfile.write (FSUID + "\n")
                           lockfile.write (lockSignature["UserUIDCreatedLock"] + "\n")
                           lockfile.write (lockSignature["LockDescription"] +"\n")
                           lockfile.write (json.dumps(lockSignature["OpenFileHandles"]))
                       FileUtil.setFilePermissions (lockfilePath, FilePermissions)
               except:
                    pass
                
           lockSignature = None        
           if os.path.exists (lockfilePath) :
               try :
                   lockSignature = NonPersistentFSLock.getLockFile (lockfilePath)
                   if ProjectDataset is not None :
                       try :
                            niftiInterface = ProjectDataset.getNIfTIDatasetInterface ()
                            if niftiInterface is not None :
                                eventHandler = niftiInterface.getFileSysetmEventHandler ()
                                if eventHandler is not None :
                                    eventHandler.processEvent (lockfilePath)                    
                       except:
                           print ("Error occured in createLockFile calling lockfile path process event")
                       
               except:
                   print ("A error occured reading the lock file") 
                   lockSignature = None   
           return lockSignature
       finally:
           NonPersistentFSLock.accessLock.release()
           
   
    @staticmethod 
    def getLockFile (LockFilePath, ReturnLines = False, AccessLock = None) :
        try:
            if AccessLock is None :
                AccessLock = NonPersistentFSLock.accessLock
            try :
                AccessLock.acquire()
                with open (LockFilePath, "rt") as file :
                    data = file.readlines ()
                for index in range (len (data)) :
                    data[index] = data[index].strip ()
                if ReturnLines :
                    return data
                return NonPersistentFSLock.decodeLockFileContents (data)
            finally:
                AccessLock.release()
        except:
            return None
            
    
    @staticmethod
    def decodeLockFileContents (filecontents) :
        result = {}
        if len (filecontents) == 4 or len (filecontents) == 5:
            result["LockSignature"] =  filecontents[0]
            result["UserUIDCreatedLock"] =  filecontents[1]
            result["LockDescription"] =  filecontents[2]
            result["OpenFileHandles"] =  json.loads (filecontents[-1])
            if  not isinstance (result["OpenFileHandles"], list) :
                result["OpenFileHandles"] = []
        elif len (filecontents) == 3:
            result["LockSignature"] =  filecontents[0]
            result["UserUIDCreatedLock"] =  filecontents[1]
            result["LockDescription"] =  filecontents[2]
            result["OpenFileHandles"] = []
        elif len (filecontents) == 2:
            result["LockSignature"] =  filecontents[0]
            result["LockDescription"] =  filecontents[1]
            result["OpenFileHandles"] = []
        elif len (filecontents) == 1 :
            result["LockSignature"] =  filecontents[0]
            result["OpenFileHandles"] = []
        return result
    
    @staticmethod
    def areLockSignaturesEqual (signature1, signature2) :
        if len (signature1) == len (signature2) :
            for key, value in signature1.items () :
                if key not in signature2 or value != signature2[key] :
                    return False
            return True
        return False
    
    @staticmethod 
    def unlock (path, ProjectDataset = None, AccessLock = None) :
        if AccessLock is None :
            AccessLock = NonPersistentFSLock.accessLock
        try :
            AccessLock.acquire()
            if os.path.exists (path) :
                os.remove (path)   
                print ("File .lock removed: " + path)
                if ProjectDataset is not None :
                    try :
                            niftiInterface = ProjectDataset.getNIfTIDatasetInterface ()
                            if niftiInterface is not None :
                                eventHandler = niftiInterface.getFileSysetmEventHandler ()
                                if eventHandler is not None :
                                    eventHandler.processEvent (path)          
                    except:
                        print ("Error occured in unlock calling lockfile path process event")
        finally:
            AccessLock.release()
       
                
    @staticmethod 
    def getRawText (data) :
        if not isinstance (data,dict) :
            return data
        txt = []
        if "LockSignature" in data :
            txt.append (data["LockSignature"])
        if "UserUIDCreatedLock" in data :
            txt.append (data["UserUIDCreatedLock"])
        if "LockDescription" in data :
            txt.append (data["LockDescription"])
        return "\n".join (txt)

    @staticmethod
    def tryToUnlockFile (LockFilePath, LockSignature, ProjectDataset = None, AccessLock = None, FileHandleUID = None) :
        #print ("\n--------------------------")
        #print ("tryToUnlockFile: " + LockFilePath)
        #print ("--------------------------")
        if AccessLock is None :
            AccessLock = NonPersistentFSLock.accessLock
        try :
            AccessLock.acquire()
            try :
                #print ("")
                if os.path.exists (LockFilePath)  :
                    if LockSignature is None :
                        return False
                    fileContents = NonPersistentFSLock.getLockFile (LockFilePath, AccessLock = AccessLock)
                    if fileContents is not None :
                        if fileContents["LockSignature"] == LockSignature :                            
                            print ("Removeing open file handle lock: " + FileHandleUID)
                            if FileHandleUID in fileContents["OpenFileHandles"] :
                                del fileContents["OpenFileHandles"][fileContents["OpenFileHandles"].index (FileHandleUID)]                                
                                """dirname, _ = os.path.split (LockFilePath)
                                os.remove(os.path.join (dirname, FileHandleUID.replace("/","")+".stack"))"""
                                
                            if len (fileContents["OpenFileHandles"]) < 1 :
                                NonPersistentFSLock.unlock (LockFilePath, ProjectDataset = ProjectDataset, AccessLock = AccessLock)
                                print ("File %s was unlocked (removed)." % LockFilePath)
                                #print ("")
                                #import traceback
                                #traceback.print_stack ()
                            else:
                               print ("Open file handles: " + ", ".join (fileContents["OpenFileHandles"]))
                               with open (LockFilePath, "wt") as lockfile :
                                   lockfile.write (fileContents["LockSignature"] + "\n")
                                   lockfile.write (fileContents["UserUIDCreatedLock"] + "\n")
                                   lockfile.write (fileContents["LockDescription"] +"\n")
                                   lockfile.write (json.dumps (fileContents["OpenFileHandles"]))
                                   lockfile.flush ()
                        else:
                            printWarrningMSG = False
                            try:
                                if fileContents["LockSignature"] != LockSignature["LockSignature"] :
                                     printWarrningMSG = True
                            except :
                                printWarrningMSG = True
                            if printWarrningMSG : 
                                print ("File has changed, opened by another user, and was not removed.")
                                print ("Expected:")
                                print (LockSignature)
                                print ("Found:")
                                print (NonPersistentFSLock.getRawText (fileContents))
                #print ("")
                return True            
            except:
                print ("A error occured unlocking %s" % LockFilePath)
            return False
        finally: 
            AccessLock.release()            