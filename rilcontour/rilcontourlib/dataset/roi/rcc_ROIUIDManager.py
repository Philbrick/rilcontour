#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan  6 13:59:10 2019

@author: KennethPhilbrick
"""
from six import *
from uuid import uuid4 
import time

class ROIUIDManager:
    def __init__ (self):
        self._UID_Keys = {}        
       
    def equals (self, test) :
        try:
            if test is None :
                return False
            if len(self._UID_Keys) != len (test._UID_Keys) :
                return False
            for key, value in self._UID_Keys.items ():
                if key not in test._UID_Keys:
                    return False
                if value != test._UID_Keys[key] :
                    return False
            return True
        except:
            return False
        
    def hasUIDKey (self, key) :
        return key in self._UID_Keys
    
    def defineUIDKey (self, key, project, name, origionalName = None):        
        if origionalName is None :
            if key in self._UID_Keys :
                tpl = self._UID_Keys[key]
                origionalName = tpl[2]            
        self._UID_Keys[key] = (project, name, origionalName)
    
    def getUIDKeyOrigionalName (self, key) :
        return self._UID_Keys[key][2]
        
    def setOrigionalNameForUID (self, key, origionalName)    :
        self._UID_Keys[key][2] = origionalName
    
    def tryToGetUIDKeyForOrigionalName (self, origionalName) :
        if origionalName is None :
            return None
        for key, values in self._UID_Keys.items ():
            if values[2] == origionalName :
                return key
        return None
    
    def defineUIDtoUIDKey (self, key, value, origionalValue = None) :
        self._UID_Keys[key] = (None, value, origionalValue)
    
    def getUIDfromUID (self, searchUID) :
       if searchUID in self._UID_Keys :   
           _, foundUID, _ = self._UID_Keys[searchUID]
           return foundUID
       return None
    
    def copy (self) :
        newUIDObj = ROIUIDManager ()
        for key, value in self._UID_Keys.items () :
            project, name, origionalValue = value
            newUIDObj.defineUIDKey (key, project, name, origionalName = origionalValue)
        return newUIDObj
    
    def count (self) :
        return len (self._UID_Keys)
    
    def getNameUID (self, name) :
        for key, value in self._UID_Keys.items () :
           project, keyName, _ = value
           if keyName == name :
               return key
        return None
    
    def getUIDName (self, uid) :
        if uid in self._UID_Keys :
            project, keyName, _ = self._UID_Keys[uid]
            return keyName
        return None
    
    def getUIDProject (self, uid) :
        if uid in self._UID_Keys :
            project, keyName, _ = self._UID_Keys[uid]
            return project
        return None
    
    def getTuple (self):
        retlst =  []
        for key, value in self._UID_Keys.items () : 
            project, keyName, origionalName = value
            returnobj = (key, (project, keyName, origionalName))
            retlst.append (returnobj)
        return tuple (retlst)
    
    def initFromTuple (self, tuplist) :
        self._UID_Keys = {}
        for obj in tuplist :            
            key, pair = obj
            if len (pair) == 3 :
                project, keyName, origionalName = pair                 
            elif len (pair) == 2 :
                project, keyName = pair 
                origionalName = None
            self.defineUIDKey (key, project, keyName, origionalName)

    def getUIDList (self) :
        retlst =  []
        for key in self._UID_Keys.keys () : 
            retlst.append (key)
        return retlst
    
    def removeProjectUID (self, UID) :
        if UID in self._UID_Keys :
            del self._UID_Keys[UID]
            
    def hasUIDDefined (self) :
       return len (self._UID_Keys) > 0
    
    def getFirstDefinedUID (self) :
      uidList = list (self._UID_Keys.keys ()) 
      if len (uidList) <= 0 : 
          return None
      return uidList [0]
   
    @staticmethod
    def generateUID (project, user, ROIName) :
       uid =  str (uuid4())
       uid = "ROIDefUID_" + project + "_" + user + "_" + ROIName+"_"+ uid + "_" + str (time.time ())
       return uid