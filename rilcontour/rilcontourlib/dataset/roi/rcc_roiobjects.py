# -*- coding: utf-8 -*-
"""
Created on Fri Oct 14 11:07:45 2016

@author: Philbrick
"""

#import json
import sys
import math
import copy
import os
import tempfile
import time
import uuid
from six import *
import nibabel as nib
import numpy as np
from scipy import ndimage
from skimage.draw import polygon
from PyQt5 import QtGui, QtCore
from nipype.interfaces.ants import ApplyTransforms, ApplyTransformsToPoints
from rilcontourlib.util.rcc_util import Coordinate, AreaMerge, FileObject, NiftiVolumeData, FindContours,ANTSRegistrationLog, PythonVersionTest
from rilcontourlib.dataset.roi.rcc_ROIUIDManager import ROIUIDManager
import json
import weakref
from rilcontourlib.machinelearning.ml_DatasetInterface import ML_KerasModelLoaderInterface
#from numba import jit

try :
    from skimage.transform import resize 
except :
    try:
        from scipy.misc import imresize
    except:
        print ("Error: could not import either:  skimage.transform.resize or  scipy.misc.imresize")


class DeepGrowPoint :
    
    def __init__ (self, pointID, coordinate, radius, sigma, positive, parent) :
        self._pointID = pointID
        self._coordinate = coordinate.copy ()  
        self._radius = radius
        self._positive = positive
        self._sigma = sigma
        self._Parent = weakref.ref(parent)        
        self._sliceMaskCache = {}        
       
        
    def draw (self, qp, SliceDrawn, color, DGrowAnnotation, niftislicewidget):
        origionalOpacity = qp.opacity()
        
        xCord = self._coordinate.getXCoordinate ()
        yCord = self._coordinate.getYCoordinate ()
       
        selectedAnnotation = DGrowAnnotation.getSelectedAnnotation ()
        if selectedAnnotation is None :
             selectedUID, selectedObject = None, None
             isPointSelected = False
        else:
             selectedUID, selectedObject = selectedAnnotation
             isPointSelected = selectedUID.getUID () == self._pointID
             #print (("SelectedPoint", selectedUID , self._pointID))
        xCord, yCord = niftislicewidget.transformPointFromImageToScreen (xCord, yCord) 
        qp.setOpacity(1.0)
        if isPointSelected  :
            qp.setBrush (QtGui.QColor (0,0,0,0))  
            if selectedObject == "Radius" :
                solidLinePen = QtGui.QPen(QtGui.QColor (35,238,203,255/2), 4, QtCore.Qt.SolidLine)  
            else:
                solidLinePen = QtGui.QPen(QtGui.QColor (35,148,237,255/4), 4, QtCore.Qt.SolidLine)  
            qp.setPen (solidLinePen)
            radius = int (self.getRadius ())
            
            xR1, yR1 = niftislicewidget.transformPointFromImageToScreen (0, 0)
            xR2, yR2 = niftislicewidget.transformPointFromImageToScreen (radius, radius)
            circleXR, CircleYR = abs (xR2-xR1), abs (yR2-yR1)
            
            qp.drawEllipse (xCord-circleXR,yCord-CircleYR,circleXR+circleXR,CircleYR+CircleYR)
            solidLinePen = QtGui.QPen(QtGui.QColor (0,0,0,255/4), 1, QtCore.Qt.SolidLine)  
            qp.setPen (solidLinePen)
            qp.drawEllipse (xCord-circleXR-2,yCord-CircleYR-2,circleXR+circleXR+4,CircleYR+CircleYR+4)
            qp.drawEllipse (xCord-circleXR+2,yCord-CircleYR+2,circleXR+circleXR+-4,CircleYR+CircleYR-4)
       
        solidLinePen = QtGui.QPen(QtGui.QColor (0,0,0,255), 0, QtCore.Qt.SolidLine)     
        qp.setPen (solidLinePen)
        qp.setBrush (QtGui.QColor (0,0,0,255))             
        
        qp.drawRect (xCord-7, yCord - 7,14,14)        
        solidLinePen = QtGui.QPen(QtGui.QColor (150,150,150,255), 0, QtCore.Qt.SolidLine)             
        qp.setPen (solidLinePen)
        if isPointSelected :
            qp.setBrush (QtGui.QColor (35,238,203,255))             
        else:
             qp.setBrush (QtGui.QColor (35,148,237,255))        
        qp.drawRect (xCord-6, yCord - 6,12,12)                
        solidLinePen = QtGui.QPen(QtGui.QColor (0,0,0,255), 0, QtCore.Qt.SolidLine)     
        qp.setPen (solidLinePen)
                
        qp.drawLine (xCord-3, yCord,xCord+3,yCord)
        if self._positive :
           qp.drawLine (xCord, yCord-3,xCord,yCord+3)
    
        #if isPointSelected and selectedObject == "Radius" :
        
        qp.setOpacity(origionalOpacity)
        
    def getSign (self) :
        return self._positive
    
    def getSliceRange (self) :
        return [self._coordinate.getZCoordinate ()]
     
    def getTuple (self) :
        return (self._pointID, self._coordinate.getCoordinate (), self._radius, self._sigma,  self._positive)
    
    def copy (self, parent) :
        dgPoint = DeepGrowPoint (self._pointID, self._coordinate, self._radius, self._sigma, self._positive, parent)
        dgPoint._sliceMaskCache = copy.deepcopy (self._sliceMaskCache)
        return dgPoint
    
    def clearCache (self) :
        self._sliceMaskCache = {}
        
    def _getParent (self) :
        return self._Parent ()
    
    def _setNotifyParentPointChanged (self) :
        parent = self._getParent ()
        if parent is not None :
            parent._pointChanged  (self)    
            parent.callDeepGrowSliceChangeListener ()                
            
    
    def getUID (self) :
        return self._pointID
    
    def getSigma (self) :
        return self.getRadius () / 2.0 
    
    def setRadius (self, radius) :
        radius = min (max (radius, 20), 300)
        if radius != self._radius :
            self._radius = radius  
            self._sliceMaskCache = {}  
            self._setNotifyParentPointChanged ()
              
    
    def getRadius (self) :
        return max (self._radius, 20)
    
    
    def getAxisSlice (self, Axis) :
        if Axis == "Z" :
            return  self._coordinate.getZCoordinate ()
        elif Axis == "X" :
            return  self._coordinate.getXCoordinate ()
        elif Axis == "Y" :
            return  self._coordinate.getYCoordinate ()
        
    def getCenterCoordinateInSliceView (self, Axis) :
        if Axis == "Z" :
            return  (self._coordinate.getXCoordinate (), self._coordinate.getYCoordinate ())
        elif Axis == "X" :
            return  (self._coordinate.getYCoordinate (), self._coordinate.getZCoordinate ())
        elif Axis == "Y" :
            return  (self._coordinate.getXCoordinate (), self._coordinate.getZCoordinate ())
    
    def setCenterPosInSlice (self, Axis, imageX, imageY) :
        if Axis == "Z" :
            self._coordinate.setCoordinate ([imageX, imageY, self._coordinate.getZCoordinate ()])
        elif Axis == "X" :
            self._coordinate.setCoordinate ([self._coordinate.getXCoordinate (), imageX, imageY])
        elif Axis == "Y" :
            self._coordinate.setCoordinate ([imageX, self._coordinate.getYCoordinate (), imageY])
        #self._sliceMaskCache = {}  
        self._setNotifyParentPointChanged ()
        
    def getVoxelDimInSliceView (self, Axis) : 
        parent = self._getParent ()
        if parent is None :
            return (0.0, 0.0)
        vDim = parent.getVoxelDim ()
        if Axis == "Z" :
            return  (float (vDim[0]), float (vDim[1]))
        elif Axis == "X" :
            return  (float (vDim[1]), float(vDim[2]))
        elif Axis == "Y" :
            return  (float (vDim[0]), float (vDim[2]))
        return (0.0, 0.0)
        
    def getCoordinate (self) :
        return self._coordinate
    
    def doesPointTouchSlice (self, sliceNumber,Axis) :
        if Axis == "Z" :
            dist = sliceNumber - self._coordinate.getZCoordinate ()
        if Axis == "X" :
            dist = sliceNumber - self._coordinate.getXCoordinate ()
        if Axis == "Y" :
            dist = sliceNumber - self._coordinate.getYCoordinate ()
        return dist == 0
             
                 
    def setCoordinate (self, coordinate) :
        if self._coordinate.getZCoordinate () != coordinate.getZCoordinate () :
            self._sliceMaskCache = {}
        self._coordinate =  coordinate.copy ()
        self._setNotifyParentPointChanged ()
    
    def doesPointCrossSlice (self, sliceNumber , Axis) :
        radius = self.getRadius ()          
        if radius <= 0 :
            return False
        cord = self.getCoordinate ()   
        if Axis == "X" :
            centerCord = cord.getXCoordinate ()
        elif Axis == "Y" :
            centerCord = cord.getYCoordinate ()
        elif Axis == "Z" :
            centerCord = cord.getZCoordinate ()
        return  centerCord == sliceNumber 
             
    def isInSlice (self, sliceNumber) :   
        return self.doesPointCrossSlice (sliceNumber, "Z")
    
    def isEdgeAt (self, coordinate):
        radius = self.getRadius ()
        if radius <= 0 :
            return False        
        zCord = coordinate.getZCoordinate ()
        center = self.getCoordinate ()      
        cZ = center.getZCoordinate ()
        if cZ != zCord  :
            return False
        xCord = coordinate.getXCoordinate ()
        yCord = coordinate.getYCoordinate ()
        cX = center.getXCoordinate ()
        cY = center.getYCoordinate ()
        radius -= 1
        radius = max (0, radius)
        dist = math.sqrt ((float (cX - xCord))**2 + (float(cY - yCord ))**2)         
        if dist >= radius - 1 and dist <= radius + 1 :
            return True
        return False
            
    def isPointInside (self, coordinate):        
        radius = self.getRadius ()
        if radius <= 0 :
            return False
        zCord = coordinate.getZCoordinate ()
        center = self.getCoordinate ()     
        cZ = center.getZCoordinate ()
        if cZ != zCord :
            return False
        xCord = coordinate.getXCoordinate ()
        yCord = coordinate.getYCoordinate ()
        cX = center.getXCoordinate ()
        cY = center.getYCoordinate ()
        radius -= 1
        radius = max (0, radius)
        return (float (cX - xCord))**2 + (float(cY - yCord ))**2 <= radius **2
    
       
    
    def addToSliceMask (self, sliceMask, PointIDMask, SliceNumber):
        radius = self.getRadius ()
        if radius <= 0 :
            self._sliceMaskCache = {}
            return False
        coordinate = self.getCoordinate ()
        drawZCoord = SliceNumber
        
        AxisCord = coordinate.getZCoordinate ()  
        if AxisCord != drawZCoord :
            return False
       
        if  "gausian" not in self._sliceMaskCache :
          
            coordinate = self.getCoordinate ()                                                        
            dist = abs(AxisCord - drawZCoord)
            
            if dist <= 0 :     
                maxDim = math.ceil (5.0 * self.getSigma ())
                middle = int (maxDim / 2)
                if "gausian" in  self._sliceMaskCache and self._sliceMaskCache["gausian"].shape == (maxDim, maxDim) :
                    self._sliceMaskCache["gausian"][...] =  0
                else:
                    self._sliceMaskCache["gausian"] = np.zeros ((maxDim, maxDim), dtype=np.float)
                mem = self._sliceMaskCache["gausian"]
                mem[middle, middle] = 1.0
                self._sliceMaskCache["gausian"] = ndimage.filters.gaussian_filter(mem, self.getSigma ())
                self._sliceMaskCache["gausian"] *= (0.0015917113029941467 / np.max (self._sliceMaskCache["gausian"]))  # normalize to radius 1
                self._sliceMaskCache["gmiddle"] = middle
                
        if "gausian" in self._sliceMaskCache :
            gausian = self._sliceMaskCache["gausian"]
            gausian_middle = self._sliceMaskCache["gmiddle"]
            """center = self.getCoordinate ()        
            AxisCord = (center.getXCoordinate (), center.getYCoordinate ())
           
            if "Cord" not in self._sliceMaskCache or self._sliceMaskCache["Cord"] != AxisCord :
                self._sliceMaskCache["Cord"] = AxisCord"""
            
            cX = coordinate.getXCoordinate ()   
            cY = coordinate.getYCoordinate ()   
            xDim, yDim= gausian.shape 
            
            gXmin = 0
            gYmin = 0
            gXmax = xDim
            gYmax = yDim
            
            sxMin =  cX - gausian_middle + 1
            sYMin =  cY - gausian_middle + 1
            sxMax =  sxMin + xDim 
            syMax =  sYMin + yDim
            if sxMin  < 0 :
                gXmin = -sxMin
                sxMin = 0
            if sYMin  < 0 : 
                gYmin = -sYMin
                sYMin = 0
            if sxMax > sliceMask.shape[0]:
               gXmax -= sxMax - sliceMask.shape[0]
               sxMax =  sliceMask.shape[0]
            if syMax > sliceMask.shape[1]:
               gYmax -= syMax - sliceMask.shape[1]
               syMax =  sliceMask.shape[1]
            try :
                sliceDest = sliceMask[sxMin:sxMax,sYMin:syMax]
                gausianSource = gausian[gXmin:gXmax,gYmin:gYmax]
                changedMask = np.abs (sliceDest) < gausianSource
                if not np.any (changedMask):
                    return False
                if self._positive :       
                    sliceDest[changedMask] = gausianSource[changedMask]
                else:
                    sliceDest[changedMask] = -gausianSource[changedMask]
                sliceMask[sxMin:sxMax,sYMin:syMax] = sliceDest
                if PointIDMask is not None :
                    sliceDest = PointIDMask[sxMin:sxMax,sYMin:syMax]
                    sliceDest[changedMask] = self.getUID ()
                    PointIDMask[sxMin:sxMax,sYMin:syMax] = sliceDest
                return True
            except:
                return False
        return False
            
class DeepGrowAnnotation :
    def __init__ (self, parent, Points3d = False):
        self._ParentObject = weakref.ref (parent)
        self._changedSlices = set ()
        self._PointIDCounter = int (0)
        self._maskCache = {}
        self._predictionCache = {}
        self._deepGrowPointDict = {}        
        self._sliceChangeListener = []
        self._selectedDeepGrowAnnotation = None
        self._QTDrawMask = {}
        self._getQTMaskMem = {}
        self._DeepGrowSlicePredictionClipMasks = {}
        self._sliceAxisCache = None
        self._XAxisIndex = AxisIndex (0, None)
        self._YAxisIndex = AxisIndex (1, None)
        self._predictedSliceCacheModelVersion = None
        self._predictedSliceCache = {}        
        self._perpendicularAxisChanged=["X","Y"]
    
    def getParentObj (self) :
        return self._ParentObject()
    
    def isDeepGrowSlicePredictionClipSliceSet (self, sliceIndex) :
        return sliceIndex in self._DeepGrowSlicePredictionClipMasks 
    
    def getDeepGrowSlicePredictionClipSliceSet (self, sliceIndex) :
        if sliceIndex in self._DeepGrowSlicePredictionClipMasks :
            return self._DeepGrowSlicePredictionClipMasks[sliceIndex]
        return None
    
    def setDeepGrowSlicePredictionClipSlice (self, sliceIndex, mask) :
        self._DeepGrowSlicePredictionClipMasks[sliceIndex] = mask
        self.callDeepGrowSliceChangeListener ()
        
    def hasAnnotations (self) :
        return len (self._deepGrowPointDict) > 0
    
    
    def _updateAxisXYCache (self, NIfTIVolume, ProjectDataset, ROIUID) :
        if self._sliceAxisCache is None :
            cord = Coordinate ()              
            for index in list (self._changedSlices) :                
                cord.setCoordinate ([0,0,index])
                self.getDeepGrowMaskPrediction (index, "Z", NIfTIVolume, ProjectDataset, ROIUID) 
            self._sliceAxisCache =  (self._XAxisIndex, self._YAxisIndex)
            
    def hasAnnotationOnYAxis (self, NIfTIVolume, ProjectDataset, ROIUID, cY) :
        self._updateAxisXYCache (NIfTIVolume, ProjectDataset, ROIUID)
        mask = self._YAxisIndex.getAxisMask (NoCopy = True) 
        if mask is None :
            return False
        return mask[cY]
    
    def hasAnnotationOnXAxis (self, NIfTIVolume, ProjectDataset, ROIUID, cX) :
        self._updateAxisXYCache (NIfTIVolume, ProjectDataset, ROIUID)
        mask = self._XAxisIndex.getAxisMask (NoCopy = True) 
        if mask is None :
            return False
        return mask[cX]
        
    def hasAnnotationsForSlice (self, sliceNumber) :
        for pt in self._deepGrowPointDict.values () :
            if pt.getCoordinate().getZCoordinate () == sliceNumber:
                return True
        return False
    
    def getSliceList (self) :
        ptList = set () 
        for pt in self._deepGrowPointDict.values () :
             ptList.add (pt.getCoordinate().getZCoordinate ())
        return sorted (list (ptList))
    
    def getSliceCount (self) :
        ptList = set () 
        for pt in self._deepGrowPointDict.values () :
             ptList.add (pt.getCoordinate().getZCoordinate ())
        return len (ptList)
        
    def addDeepGrowSliceChangeListener (self, callbackMethod) :
        removeLst = []
        found = False
        for index, callback in enumerate(self._sliceChangeListener) :             
            if callback () is None :
                removeLst.append (index)
            elif callback () == callbackMethod :
                found = True
                break
        if not found :                
            if PythonVersionTest.IsPython2 () :
                self._sliceChangeListener.append (callbackMethod)
            else:
                self._sliceChangeListener.append (weakref.WeakMethod(callbackMethod))
        if len(removeLst) > 0 :
            removeLst.reverse ()
            for index in removeLst :
                del self._sliceChangeListener[index]
                
    
    def removeDeepGrowSliceChangeListener (self, callbackMethod) :
        removeLst = []        
        for index, callback in enumerate(self._sliceChangeListener) :             
            if PythonVersionTest.IsPython2 () :
                if callback is None or callback == callbackMethod :
                    removeLst.append (index)
                    break     
            elif callback () is None :
                removeLst.append (index)
            elif callback () == callbackMethod :
                removeLst.append (index)
                break        
        if len(removeLst) > 0 :
            removeLst.reverse ()
            for index in removeLst :
                del self._sliceChangeListener[index]

    def callDeepGrowSliceChangeListener (self) :
        removeLst = []        
        for index, callback in enumerate(self._sliceChangeListener) :    
            if PythonVersionTest.IsPython2 () :
                if callback is None :
                     removeLst.append (index)
                else:
                    callback (self)
            elif callback () is None :
                removeLst.append (index)
            else:
                callback()(self)                
        if len(removeLst) > 0 :
            removeLst.reverse ()
            for index in removeLst :
                del self._sliceChangeListener[index]
            
    def initFromFileObj (self, fobj) :       
       self._PointIDCounter = fobj.getParameter ("PointIDCounter")
       PointCount = fobj.getParameter ("PointCount")       
       self._deepGrowPointDict = {}
       self._maskCache = {}
       self._DeepGrowSlicePredictionClipMasks = {}
       for pc in range (PointCount) :
           tup = fobj.getParameter ("PT_%d" % pc)
           pointID, coordinate, radius, sigma, positive = tup
           cord = Coordinate ()
           cord.setCoordinate (coordinate)
           self._deepGrowPointDict[pointID] =  DeepGrowPoint (pointID, cord, radius, sigma, positive, self)
   
    def getFileObject (self) :
       fobj = FileObject ("DeepGrowAnnotations")              
       fobj.setParameter ("PointIDCounter", self._PointIDCounter)
       fobj.setParameter ("PointCount", len (self._deepGrowPointDict))
       count = 0
       for key in sorted (list (self._deepGrowPointDict.keys ())) :
           fobj.setParameter ("PT_%d" % count, self._deepGrowPointDict[key].getTuple ())
           count += 1
       return fobj
       
    def getSelectedAnnotation (self) :
        if self._selectedDeepGrowAnnotation is None :
            return None 
        annotationUID, Selection = self._selectedDeepGrowAnnotation
        if annotationUID not in self._deepGrowPointDict :
            self._selectedDeepGrowAnnotation = None
            return None
        return self._deepGrowPointDict[annotationUID], Selection 
    
    def setSelectedDeepGrowControlPoint  (self, SelectedDeepGrowControlPoint, Selection) :
        oldSelection = self._selectedDeepGrowAnnotation
        self._selectedDeepGrowAnnotation = (SelectedDeepGrowControlPoint, Selection)  
        if oldSelection is None and SelectedDeepGrowControlPoint is not None or oldSelection[0] != SelectedDeepGrowControlPoint :
            self.callDeepGrowSliceChangeListener ()
        
    def clearSelectedDeepGrowControlPoint (self) :
       self._selectedDeepGrowAnnotation = None 
       self.callDeepGrowSliceChangeListener ()
    
    def copy (self) : 
        annotation = DeepGrowAnnotation (self)
        annotation._selectedDeepGrowAnnotation = self._selectedDeepGrowAnnotation
        annotation._PointIDCounter = self._PointIDCounter
        annotation._maskCache = copy.deepcopy (self._maskCache)
        annotation._deepGrowPointDict = {}
        for key, value in self._deepGrowPointDict.items () :
            annotation._deepGrowPointDict[key] = value.copy (annotation)        
        annotation._predictedSliceCacheModelVersion = self._predictedSliceCacheModelVersion
        annotation._predictedSliceCache = copy.deepcopy (self._predictedSliceCache)
        annotation._XAxisIndex = self._XAxisIndex.copy ()
        annotation._YAxisIndex = self._YAxisIndex.copy ()
        annotation._changedSlices = copy.deepcopy (self._changedSlices)
        annotation._perpendicularAxisChanged = copy.deepcopy (self._perpendicularAxisChanged)
        return annotation
        
    @staticmethod
    def getVolumeShape (NIfTIVolume, Axis = None) :
        if NIfTIVolume is None :
            return None
        VolumeShape = NIfTIVolume.getSliceDimensions ()
        if Axis == "Z" :
            return (VolumeShape[0], VolumeShape[1])
        elif Axis is None :
            return VolumeShape
        elif Axis == "X" :
            return (VolumeShape[1], VolumeShape)
        elif Axis == "Y" :
            return (VolumeShape[0], VolumeShape)        
        return None
        
            
    def getPointsInSlice (self, sliceNumber):
        ptList = []
        for pt in self._deepGrowPointDict.values () :
            if pt.isInSlice (sliceNumber) :
                ptList.append (pt) 
        return ptList
 
    def getPointUID (self) :
        self._PointIDCounter += 1
        return self._PointIDCounter
    
    def _pointChanged (self, point) :            
        for axis in self._maskCache.keys () :
            cord = self._maskCache[axis]["cord"]
            if point.doesPointCrossSlice (cord, axis) :
                self._maskCache[axis]["mem"] = None             
                    
        for axis in self._QTDrawMask.keys () :
            if self._QTDrawMask[axis] is not None :
                cord = self._QTDrawMask[axis][0]
                if point.doesPointCrossSlice (cord, axis) :
                    self._QTDrawMask[axis] = None 

        self._sliceAxisCache = None
        zc = point.getCoordinate().getZCoordinate ()
        self._setSliceChanged (zc)        
            
    def createDeepGrowPoint (self, coordinate, Radius = 3.0, Sigma = 1.0, Positive = True):
        uID = self.getPointUID ()
        dgPoint =  DeepGrowPoint (uID,coordinate, Radius, Sigma, Positive, self)
        self._deepGrowPointDict[uID] = dgPoint
        self._pointChanged (dgPoint)
        self._selectedDeepGrowAnnotation = (uID, "Center")
        self.callDeepGrowSliceChangeListener ()
        return dgPoint
    
    def clearAllSlices (self) :
        if len (self._deepGrowPointDict) > 0 :
            self._PointIDCounter = int (0)
            self._maskCache = {}
            self._deepGrowPointDict = {}        
            self._selectedDeepGrowAnnotation = None
            self._QTDrawMask = {}
            self._getQTMaskMem = {}
            self._DeepGrowSlicePredictionClipMasks = {}
            self.callDeepGrowSliceChangeListener ()            
            self._sliceAxisCache = None        
            self._clearSliceChanged ()            
            self._XAxisIndex = AxisIndex (0, None)
            self._YAxisIndex = AxisIndex (1, None)
            self._predictedSliceCacheModelVersion = None
            self._predictedSliceCache = {}
            self._perpendicularAxisChanged=["X","Y"]
            
            
            
    def removeSlice (self, sliceNumber) :        
        pointObjList = self.getPointObjectInSlice ('Z', sliceNumber) 
        if len (pointObjList) > 0 :
            for pObj in pointObjList :
                self._pointChanged (pObj)
                uid = pObj.getUID ()
                del self._deepGrowPointDict[uid]
                if self._selectedDeepGrowAnnotation is not None and self._selectedDeepGrowAnnotation[0] == uid :
                    self._selectedDeepGrowAnnotation = None
            self.callDeepGrowSliceChangeListener ()
            return True
        return False
    
    def removeDeepGrowPoint (self, uid):
        if uid in self._deepGrowPointDict :
            point = self._deepGrowPointDict[uid]
            self._pointChanged (point)
            del self._deepGrowPointDict[uid]      
            self.callDeepGrowSliceChangeListener ()
            
    def getPointObjectInSlice (self, Axis, sliceNumber) :        
        PointObjects = []  
        for point in self._deepGrowPointDict.values () :
            if point.doesPointCrossSlice (sliceNumber,  Axis) :
               PointObjects.append (point)
        return PointObjects
    
    
    def getPointObjectSelection (self, cord, niftslicewidget) :
        cX, cY = cord
        Axis = niftslicewidget.getSliceAxis ()
        #cord = niftslicewidget.getLastSystemMouseCoordinate ()
        SliceNumber  = niftslicewidget.getSliceNumber ()                
        if self._selectedDeepGrowAnnotation is not None :
            pid, selection = self._selectedDeepGrowAnnotation
            if pid in self._deepGrowPointDict :
                pointObj = self._deepGrowPointDict[pid] 
                pc = pointObj.getCoordinate ()
                if Axis == 'Z' : 
                    xcord = pc.getXCoordinate ()
                    ycord = pc.getYCoordinate ()
                    SNumber = pc.getZCoordinate ()
                elif Axis == 'X' :
                    xcord = pc.getYCoordinate ()
                    ycord = pc.getZCoordinate ()
                    SNumber = pc.getXCoordinate ()
                elif Axis == 'Y' :
                    xcord = pc.getXCoordinate ()
                    ycord = pc.getZCoordinate ()
                    SNumber = pc.getYCoordinate ()
                if SNumber == SliceNumber :
                    radius = pointObj.getRadius ()
                    rx1, ry1 = niftslicewidget.transformPointFromImageToScreen (0, 0)
                    rx2, ry2 = niftslicewidget.transformPointFromImageToScreen (radius, radius)
                    rx = abs (rx2 - rx1) + 4
                    ry = abs (ry2 - ry1) + 4
                    xcord, ycord = niftslicewidget.transformPointFromImageToScreen (xcord, ycord)
                    dX = abs(xcord - cX)
                    dY = abs(ycord - cY)
                    if (rx >= dX and ry >= dY) :
                        precent = float(dX)/float(rx)
                        radius = math.ceil(float(rx) * precent + (1.0 - precent) * float(ry))
                        angle = math.acos (float(dX)/float (radius)) 
                        outsideEdgeDy = math.ceil (float (radius) * math.sin (angle))
                        if outsideEdgeDy >= dY :
                            rx -= 8
                            if dX >= rx :
                                return (pointObj.getUID (), "Radius")
                            ry -= 8
                            precent = float(dX)/float(rx)
                            radius = rx * precent + (1.0 - precent) * ry
                            angle = math.acos (float(dX)/float (radius)) 
                            insideEdgeDy = math.floor (float (radius) * math.sin (angle))
                            if insideEdgeDy <=  dY :
                                return (pointObj.getUID (), "Radius")
                  
                    
                    if cX >= xcord - 7 and cY >= ycord - 7 and cX <= xcord + 7 and cY <= ycord + 7 :
                        return (pointObj.getUID (), "Center")
            
        PointObjects = self.getPointObjectInSlice (Axis, SliceNumber)
        for pointObj in PointObjects :
            pc = pointObj.getCoordinate ()
            if Axis == 'Z' : 
                xcord = pc.getXCoordinate ()
                ycord = pc.getYCoordinate ()
            elif Axis == 'X' :
                xcord = pc.getYCoordinate ()
                ycord = pc.getZCoordinate ()
            elif Axis == 'Y' :
                xcord = pc.getXCoordinate ()
                ycord = pc.getZCoordinate ()
            xcord, ycord = niftslicewidget.transformPointFromImageToScreen (xcord, ycord)
            #print ((cX,cY, xcord, ycord))
            if cX >= xcord - 7 and cY >= ycord - 7 and cX <= xcord + 7 and cY <= ycord + 7 :
                return (pointObj.getUID (), "Center")
        return None
        
    
    def _removeInvalidPoints (self, NIfTIVolume) :
        if NIfTIVolume is not None :
            VolumeShape = NIfTIVolume.getSliceDimensions ()
            removePTLst = []
            for value in self._deepGrowPointDict.values ():                
                coord =  value.getCoordinate ()
                cX, cY, cZ = coord.getCoordinate ()
                if cX < 0 or cX >= VolumeShape[0] or cY < 0 or cY >= VolumeShape[1] or cZ < 0 or cZ >= VolumeShape[2] :
                   removePTLst.append (value.getUID ())
            if len (removePTLst) > 0 :
                for uid in removePTLst :
                    del self._deepGrowPointDict[uid]
                self._maskCache = {}
                self.callDeepGrowSliceChangeListener ()
            
    def getDeepGrowDrawMask (self, cord, Axis, NIfTIVolume):           
        SliceShape = DeepGrowAnnotation.getVolumeShape (NIfTIVolume, Axis)
        if Axis in self._maskCache :
            axisData = self._maskCache[Axis]
            if axisData["cord"] == cord and axisData["mem"] is not None :
                if axisData["mem"].shape == SliceShape :
                    return axisData["mem"]
                
        if Axis not in self._maskCache :
            self._maskCache[Axis] = {}             
            self._maskCache[Axis]["mem"] = np.zeros (SliceShape, dtype = np.float)        
        elif self._maskCache[Axis]["mem"] is None or self._maskCache[Axis]["mem"].shape != SliceShape :            
            self._maskCache[Axis]["mem"] = np.zeros (SliceShape, dtype = np.float)        
        else:
            self._maskCache[Axis]["mem"][:] = 0.0
        self._maskCache[Axis]["cord"] = cord                    
        mask = self._maskCache[Axis]["mem"]
        self._removeInvalidPoints (NIfTIVolume)
        for point in self._deepGrowPointDict.values () :                  
            if point.doesPointCrossSlice (cord, Axis) :    
                point.addToSliceMask (mask, None, SliceNumber = cord)             
        return mask
        
    def getDeepGrowMaskPrediction (self, cord, Axis, NIfTIVolume, ProjectDataset, ROIUID, Mask = None) : 
         if not ML_KerasModelLoaderInterface.isMLSupportEnabled ()  :
             return None
         else:             
             if ProjectDataset is None :
                 return None
             
             hasCordChanged = cord in self._getSliceChanged ()
             if not hasCordChanged :
                 if Axis in self._QTDrawMask :
                    axisData = self._QTDrawMask[Axis]
                    if axisData is not None :
                        if axisData[0] == cord  :
                            return  axisData[2]  
                    
             from rilcontourlib.machinelearning.ml_DeepGrow import DeepGrowMLInterface             
             modelVersion = DeepGrowMLInterface.getModelVersion (DeepGrowROIModels = ProjectDataset.getDeepGrowROIModelInterface(), ROIUID = ROIUID)
             if modelVersion is None :
                 return None
             if modelVersion != self.getDeepGrowPredictionCacheModelVersion ():
                 self._clearPredictedCache (modelVersion)                 
             elif not hasCordChanged :
                 if Axis in self._predictedSliceCache and cord in self._predictedSliceCache[Axis] and "mask" in self._predictedSliceCache[Axis][cord] and self._predictedSliceCache[Axis][cord]["mask"] is not None :
                     return self._predictedSliceCache[Axis][cord]["mask"]   
                                      
             PythonProjectBasePath = ProjectDataset.getPythonProjectBasePath ()
             MLInterface = ProjectDataset.getKerasModelLoader ()
             if Mask is None or MLInterface is None or PythonProjectBasePath is None  :
                 Mask = self.getDeepGrowDrawMask (cord, Axis, NIfTIVolume)
                 
             predictedSegmentation = None
             if Mask is not None :
                 SliceData = None
                 if Axis == "Z" : 
                    SliceData =  NIfTIVolume.getImageData()[:,:,cord]
                    
                 elif Axis == "X" : 
                    SliceData =  NIfTIVolume.getImageData()[cord,:,:]
                 elif Axis == "Y" : 
                    SliceData =  NIfTIVolume.getImageData()[:,cord,:]
                 if SliceData is not None :
                     predictedSegmentation = DeepGrowMLInterface.predictDeepGrowSegementation  (SliceData, Mask, MLInterface, PythonProjectBasePath, DeepGrowROIModels = ProjectDataset.getDeepGrowROIModelInterface(), ROIUID = ROIUID)
                     clipMask = self.getDeepGrowSlicePredictionClipSliceSet (cord)
                     if clipMask is not None :
                         predictedSegmentation[clipMask > 0] = 0                     
                     if not np.any (predictedSegmentation) :
                         predictedSegmentation = None
             
             self._setPredictedSegmentation (Axis, cord, predictedSegmentation)                                          
             return predictedSegmentation

    def getDeepGrowPredictionCacheModelVersion (self):
        return self._predictedSliceCacheModelVersion
    
    def _clearPredictedCache (self, modelVersion) :
        self._predictedSliceCacheModelVersion = modelVersion
        self._predictedSliceCache = {}
        self._XAxisIndex = AxisIndex (0, None)
        self._YAxisIndex = AxisIndex (1, None)
        
    def _setPredictedSegmentation (self, Axis, cord, predictedSegmentation) :
        if cord in self._changedSlices :
           self._changedSlices.remove (cord) 
        if predictedSegmentation is None :
            if Axis in self._predictedSliceCache :
                if cord in self._predictedSliceCache[Axis] :
                    if "mask" in self._predictedSliceCache[Axis][cord] and self._predictedSliceCache[Axis][cord]["mask"] is not None :
                        xAxisCache = self._predictedSliceCache[Axis][cord]["xaxis_cache"]
                        yAxisCache = self._predictedSliceCache[Axis][cord]["yaxis_cache"]
                        xNonZeroAxis = xAxisCache.getAxisMask (NoCopy = True).nonzero ()
                        yNonZeroAxis = yAxisCache.getAxisMask (NoCopy = True).nonzero ()
                        xAxisCache.clear ()
                        yAxisCache.clear ()
                        self._rebuildPerpendicularAxisCache (cord, xAxisCache, xNonZeroAxis, yAxisCache, yNonZeroAxis)                        
                        self._predictedSliceCache[Axis][cord]["mask"] = None                        
                        self._perpendicularAxisChanged = ["X","Y"]
        else:
            if Axis not in self._predictedSliceCache :
                self._predictedSliceCache[Axis] = {}
            if cord not in self._predictedSliceCache[Axis] :
                self._predictedSliceCache[Axis][cord] = {}          
            if "mask" not in self._predictedSliceCache[Axis][cord] or self._predictedSliceCache[Axis][cord]["mask"] is None :                
                predictionDict = self._predictedSliceCache[Axis][cord]
                predictionDict["mask"] = predictedSegmentation                                 
                xAxisCache = AxisIndex (0, predictedSegmentation.shape)
                yAxisCache = AxisIndex (1, predictedSegmentation.shape)
                xAxisCache.setMask (predictedSegmentation, OptimizedForInit = True)
                yAxisCache.setMask (predictedSegmentation, OptimizedForInit = True)
                predictionDict["xaxis_cache"] = xAxisCache 
                predictionDict["yaxis_cache"] = yAxisCache
                self._XAxisIndex.updateMaskFromChildMask (xAxisCache, None, None)
                self._YAxisIndex.updateMaskFromChildMask (yAxisCache, None, None)
                self._perpendicularAxisChanged = ["X","Y"]
            else:
                predictionDict = self._predictedSliceCache[Axis][cord]
                predictionDict["mask"] = predictedSegmentation                                 
                xAxisCache = predictionDict["xaxis_cache"]
                yAxisCache = predictionDict["yaxis_cache"]
                xIndexZero = xAxisCache.setMask (predictedSegmentation, OptimizedForInit = False)
                yIndexZero = yAxisCache.setMask (predictedSegmentation, OptimizedForInit = False)                                   
                self._rebuildPerpendicularAxisCache (cord, xAxisCache, xIndexZero, yAxisCache, yIndexZero)                                                
                self._perpendicularAxisChanged = ["X","Y"]
    
    def _setSliceChanged (self, sliceIndex) :
        if sliceIndex not in self._changedSlices :
            self._changedSlices.add (sliceIndex)
            self._perpendicularAxisChanged = ["X","Y"]
    
    def _getSliceChanged (self) :
        return self._changedSlices
    
    def _clearSliceChanged (self) :
        if len (self._changedSlices) > 0 :
            self._changedSlices = set ()
            self._perpendicularAxisChanged = ["X","Y"]
        
    def _rebuildPerpendicularAxisCache (self, cord, xAxisCache, xIndexZero, yAxisCache, yIndexZero) :
        if "Z" in self._predictedSliceCache :
            rebuildXAxis = xIndexZero is not None and len (xIndexZero) > 0
            rebuildYAxis = yIndexZero is not None and len (yIndexZero) > 0 
            xAxisMasks = []
            yAxisMasks = []
            for index, predictionDict in self._predictedSliceCache["Z"].items () :
                if index != cord and "mask" in predictionDict and predictionDict["mask"] is not None :                    
                    if rebuildXAxis :
                        xAxisMasks.append (predictionDict["xaxis_cache"].getMaskValuesAtIndexs (xIndexZero))
                    if rebuildYAxis :
                        yAxisMasks.append (predictionDict["yaxis_cache"].getMaskValuesAtIndexs (yIndexZero))            
            self._XAxisIndex.updateMaskFromChildMask (xAxisCache, xIndexZero, xAxisMasks)            
            self._YAxisIndex.updateMaskFromChildMask (yAxisCache, yIndexZero, yAxisMasks)
        
    def getPerpendicularAxisMask (self, sliceNumber, Axis, NIfTIVolume, ProjectDataset, ROIUID, shape) :        
        for index in list (self._changedSlices) :                            
            self.getDeepGrowMaskPrediction (index, "Z", NIfTIVolume, ProjectDataset, ROIUID) 
        mem = np.zeros (shape, dtype = np.uint8)
        if "Z" in self._predictedSliceCache :
            for sliceIndex, sliceData in self._predictedSliceCache["Z"].items () :
                if "mask" in sliceData : 
                    maskData = sliceData["mask"]
                    if maskData is not None :        
                        if Axis == "X" : 
                            mem[:,sliceIndex] = maskData[sliceNumber,:]
                        else:
                            mem[:,sliceIndex] = maskData[:,sliceNumber]
        return mem
        
    def resetPerpendicularAxisChanged (self,axis) :
        if axis in self._perpendicularAxisChanged :
            del self._perpendicularAxisChanged[self._perpendicularAxisChanged.index (axis)]
            
    def hasPerpendicularAxisChanged (self,axis) :
        if len (self._changedSlices) > 0 :
            self._perpendicularAxisChanged = ["X","Y"]
            return True
        return axis in self._perpendicularAxisChanged
            
    
    
    def getDeepGrowSliceXYAxisSliceNumberList (self, NIfTIVolume, ProjectDataset, ROIUID, XSliceMem, YSliceMem) :
        self._updateAxisXYCache (NIfTIVolume, ProjectDataset, ROIUID)
        XAxisIndex, YAxisIndex = self._sliceAxisCache
        XAxisIndex = XAxisIndex.getAxisMask (NoCopy = True)
        YAxisIndex = YAxisIndex.getAxisMask (NoCopy = True)
        if XAxisIndex is not  None :
            XSliceMem[XAxisIndex] = True 
        if YAxisIndex is not  None :
            YSliceMem[YAxisIndex] = True
      
                
    def invalidatePredictionCache (self, cord, Axis) :
        if Axis in self._QTDrawMask :
            data = self._QTDrawMask[Axis]
            if data is not None and data[0] == cord :
                del self._QTDrawMask[Axis]
    
                
    def getDeepGrowQTDrawMask (self, cord, Axis, color, NIfTIVolume, ProjectDataset, ROIUID):   
         if Axis in self._QTDrawMask :
             data = self._QTDrawMask[Axis]
             if data is not None and data[0] == cord :
                 return data[1]
         mask = self.getDeepGrowDrawMask (cord, Axis, NIfTIVolume)
         VolumeShape = NIfTIVolume.getSliceDimensions ()
         if Axis == "Z" : 
            width, height = VolumeShape[0], VolumeShape[1]
         elif Axis == "X" : 
            width, height = VolumeShape[1], VolumeShape[2]
         elif Axis == "Y" : 
            width, height = VolumeShape[0], VolumeShape[2]
             
         if (Axis not in self._getQTMaskMem or self._getQTMaskMem[Axis] is None or self._getQTMaskMem[Axis].shape != (4, width, height)) :           
             self._getQTMaskMem[Axis] = np.zeros ((4, width, height),  dtype = np.uint8)
         else:
             self._getQTMaskMem[Axis][...] = 0
         maskRGBA = self._getQTMaskMem[Axis]  
         maskRGBA[...] = 0   
            
         rawPrediction = self.getDeepGrowMaskPrediction (cord, Axis, NIfTIVolume, ProjectDataset, ROIUID, Mask = mask)
         if rawPrediction is not None :
             
             NativeSetMask._setSolidColorMask (maskRGBA, color.blue (), color.green (), color.red (), rawPrediction)
             predictionImage =  QtGui.QImage (maskRGBA.tobytes(order="F"), width, height, width * 4, QtGui.QImage.Format_ARGB32)
             image = QtGui.QPixmap.fromImage (predictionImage)     
             #qp = QtGui.QPainter (image)
             #qp.drawPixmap (0,0, maskPixMap)
             #qp.end ()
         else:              
             predictionImage =  QtGui.QImage (maskRGBA.tobytes(order="F"), width, height, width * 4, QtGui.QImage.Format_ARGB32)
             image = QtGui.QPixmap.fromImage (predictionImage)                       
         self._QTDrawMask[Axis] = (cord, image, rawPrediction)
         return image
    
    
     
class ContourIDManger :
    
    def __init__ (self, parentObj) :
        self._parentObj = parentObj
        self._idNumber = 0
        self._recycleList = []
        self._allocatedIDList = []
        self._names = {}
        self._contourType = {}

    def copy (self, newParent) :
        cm = ContourIDManger (newParent)        
        cm._idNumber = self._idNumber
        cm._recycleList = copy.copy (self._recycleList)
        cm._allocatedIDList = copy.copy (self._allocatedIDList)
        cm._names = copy.copy (self._names)
        cm._contourType = copy.copy (self._contourType)
        return cm
        
    def allocateID (self) :
        self._parentObj.setObjectChanged ()
        if (len (self._recycleList) > 0) :        
            newID = self._recycleList.pop ()
        else :
            self._idNumber += 1
            newID = self._idNumber
        self._allocatedIDList.append (newID)
        self._names[newID] = "Untitled"
        self._contourType[newID] = "Auto"
        return newID
    
    def _getKey (self, item) :
        return item[1]

    def getNameLst (self):
        lst = [] 
        for key, value in self._names.items () :
            lst.append ((key, value))
        return sorted (lst, key=self._getKey)        
    
    def getContourType (self, contourID):
        return self._contourType[contourID]
                
    def setContourType (self, contourID, contourType):
        if (contourID not in self._contourType or self._contourType[contourID] != contourType) :
            self._parentObj.setObjectChanged ()
            self._contourType[contourID] = contourType
            #print (("Set contour Type" , contourID, contourType, self))
            return True
        return False            

    def getContourName (self, contourID):
        return self._names[contourID]

    def hasContourName (self, contourID):
        return contourID in self._names

    def setContourName (self, contourID, Name):
        self._parentObj.setObjectChanged ()
        self._names[contourID] = Name
    
  
            
    def deleteID (self, contourID) :                        
        changed = False
        if (contourID in self._allocatedIDList ):
            self._allocatedIDList.remove (contourID)
            changed = True
        if (contourID in self._names) :
            del self._names[contourID]
            changed = True
        if (changed) :
            self._parentObj.setObjectChanged ()
        if (contourID not in self._recycleList) :
            self._recycleList.append (contourID)
        
    def load (self, finterface) : 
        self._idNumber, self._recycleList, self._allocatedIDList = finterface.getParameter ("ContourIDManager")
        count = finterface.getParameter ("ContourIDManagerNameCount")
        for i in range (count):
            key, name, contourType = finterface.getParameter ("ContourIDManager" + str(i + 1))
            self._names[key] = name
            self._contourType[key] = contourType
        
    def save (self, finterface)    :
        finterface.setParameter ("ContourIDManager", (self._idNumber, self._recycleList, self._allocatedIDList))
        finterface.setParameter ("ContourIDManagerNameCount", len (self._names))
        i = 1
        for key, value in self._names.items () :
            finterface.setParameter ("ContourIDManager" + str(i), (key, value, self._contourType[key]))
            i += 1
    
    def hasAllocatedContourID (self) :
        return len (self._allocatedIDList) > 0
    
    def getAllocatedIDList (self):
        return self._allocatedIDList


    

"""
    To support fast undo/redo Contour objects are copied only on write (modification ) other wise they are copied by reference to support this paradgim if a contour is modified a new instance will n
    need to be re-created to support this Contour objects do not have set accessor methods (there are a few exceptions for code falling outside of the undo/redo. 
    Contour parameters should only be set through higher level slice and multi-slice accessor methods.
"""
class Contour :
   def __init__ (self, contourID = None, area = None,  shape = None, contourType = None, LastDrawDirection = None, SnappedHumanEditedSliceCount  = None, parentCount = None, ContourToCopy = None, finterface = None, contourAreaMask = None):        
       self._setChanged ()
       self.clearContourInitalizedForCrossSliceChange ()
       self._contouredAreaCompressedAxisDim = None
       self._RunningInLowMemoryMode = False
       if (ContourToCopy is not None) :
           self._RunningInLowMemoryMode = ContourToCopy._RunningInLowMemoryMode
           self._HumanEditedContour = ContourToCopy._HumanEditedContour
           if (contourType is not None) :
               if (contourType == "Computer") :
                   self._HumanEditedContour = False 
               else:
                   self._HumanEditedContour = True          

           self._ContourID = ContourToCopy._ContourID           
           if (contourID is not None) :               
               self._ContourID = contourID

           self._shape = ContourToCopy._shape
           if (area is not None) :
               self._setContourPerimeter (area)   
           elif (contourAreaMask is not None) :
               self._setContourDefinedByMask (contourAreaMask)
           else:
               if (ContourToCopy.isContourDefinedByPerimeter ()) :
                   self._coordinates = np.copy (ContourToCopy._coordinates) #copy set Area
                   self._initContourArea()      
               else:
                   self._setContourDefinedByMask (ContourToCopy._getCArea())
            
           if (shape is not None) :
              xdim, ydim = shape
              self._shape = (xdim, ydim)
              if (self.isContourDefinedByPerimeter()) :
                 self._coordinates[0, :] = np.clip (self._coordinates[0, :], 0, xdim - 1)
                 self._coordinates[1, :] = np.clip (self._coordinates[1, :], 0, ydim - 1)       
                 self._initContourArea()    
              elif (self._getCArea().shape != self._shape) :
                  newMem = np.zeros (self._shape, dtype=np.uint8)
                  newMem[:min (self._getCArea().shape[0], self._shape[0]), :min (self._getCArea().shape[1], self._shape[1])]  = self._getCArea()[:min (self._getCArea().shape[0], self._shape[0]), :min (self._getCArea().shape[1], self._shape[1])]    
                  self._setCArea (newMem)
          
           self._LastDrawDirection = ContourToCopy._LastDrawDirection           
           if (LastDrawDirection is not None) :
               self._LastDrawDirection = LastDrawDirection
          
           self._SnappedHumanEditedSliceCount = ContourToCopy._SnappedHumanEditedSliceCount
           if (SnappedHumanEditedSliceCount is not None) :
               self._SnappedHumanEditedSliceCount = SnappedHumanEditedSliceCount
            
               
       elif (contourID is not None) :
         if (contourType == "Computer") :
             self._HumanEditedContour = False 
         else:
             self._HumanEditedContour = True          
         self._ContourID = contourID
         self._shape = shape
         if (contourAreaMask is not None) :
             self._setContourDefinedByMask (contourAreaMask)
             
         else:
             self._setContourPerimeter (area)   
         if (LastDrawDirection is None) :
             LastDrawDirection = -1
         self._LastDrawDirection = LastDrawDirection
         if (SnappedHumanEditedSliceCount is None) :
             SnappedHumanEditedSliceCount  = 0
         self._SnappedHumanEditedSliceCount = SnappedHumanEditedSliceCount 
         
       elif (finterface is not None) :               
         self._ContourID  = finterface.getParameter ("ContourID")         
         self._HumanEditedContour  = finterface.getParameter ("HumanEditedContour")                  
         self._LastDrawDirection  = finterface.getParameter ("LastDrawDirection")                  
         self._SnappedHumanEditedSliceCount  = finterface.getParameter ("SnappedHumanEditedSliceCount")         
         self._shape = finterface.getParameter ("Shape")         
         if (finterface.hasFileObject ("ContourMask")) :
             contourMaskObj = finterface.getFileObject ("ContourMask") 
             self._setContourDefinedByMask (Contour._decodeContourMaskObj (self._shape, contourMaskObj))
         else: 
             if (finterface.hasParameter ("ContourCount")) :
                 ContourCount  = finterface.getParameter ("ContourCount")                                            
                 self._coordinates = np.zeros ((2, ContourCount), dtype=int)       
                 for i in range (ContourCount) :
                   self._coordinates[0, i], self._coordinates[1, i] = finterface.getParameter ("XY" + str (i))
             else:
                 contours = finterface.getParameter ("XY")
                 self._coordinates = np.array (contours, dtype=int)
             self._initContourArea()      
   
   def setRunningInLowMemoryMode (self) :
       self._RunningInLowMemoryMode = True
       
   def compressContourMemory (self) :
       if (self._contouredArea is not None and self._contouredAreaCompressedAxisDim is None):
           self._contouredAreaCompressedAxisDim = self._contouredArea.shape[0]
           self._contouredArea = np.packbits (self._contouredArea, axis = 0)
   
   def _setCArea (self, mem) :
       self._contouredArea = mem
       self._contouredAreaCompressedAxisDim = None
       if self._RunningInLowMemoryMode :
          self.compressContourMemory  ()
       
   def _getCArea (self) :
       if (self._contouredAreaCompressedAxisDim is not None):       
           ca = np.unpackbits (self._contouredArea, axis = 0)
           ca = ca[0:self._contouredAreaCompressedAxisDim, :]
           if self._RunningInLowMemoryMode :           
               return ca
           self._contouredArea = ca
           self._contouredAreaCompressedAxisDim = None
       return self._contouredArea
    
   @staticmethod
   def getXYMinMax (sliceData) :
       xMin = None
       xMax = None
       yMin = None
       yMax = None
       for x in range (sliceData.shape[0]) :
           if np.any (sliceData[x,:]) :
               xMin = x
               break
       if (xMin is None):
           return (xMin, yMin, xMax, yMax)
       for x in range (sliceData.shape[0]-1,-1,-1) :
           if np.any (sliceData[x,:]) :
               xMax = x
               break
       for y in range (sliceData.shape[1]-1,-1,-1) :
           if np.any (sliceData[:,y]) :
               yMax = y
               break
       for y in range (sliceData.shape[1]) :
           if np.any (sliceData[:,y]) :
               yMin = y
               break
       return (xMin, yMin, xMax, yMax)
    
   @staticmethod
   def _getSourceDestShape (dest, sourceSize) :
       if sourceSize > dest :
           half = math.floor ((sourceSize - dest) / 2)
           return (0, dest, half, half + dest)
       elif sourceSize == dest :
           return (0, dest, 0, sourceSize)
       elif (dest > sourceSize) :
           return (0, sourceSize, 0, sourceSize)
       
   def clipContourToDimensions (self, xdim,ydim, xScale, yScale, HumanEditied):      
       self._HumanEditedContour = HumanEditied     
       if (self._shape[0] != xdim or self._shape[1] != ydim or xScale != 1.0 or yScale != 1.0) :                     
           if (self.isContourDefinedByPerimeter ()) :                    
               coordinates = np.copy (self._coordinates.T)                                    
               if xScale != 1.0  :                   
                   rescaledXCord = coordinates[:,0].astype (np.float) * xScale
                   coordinates[:,0] =  rescaledXCord.astype (np.int)
                   del rescaledXCord
                   coordinates[:,0] = np.clip (coordinates[:,0], 0, xdim -1)                              
               if yScale != 1.0  :                                      
                   rescaleYCord = coordinates[:,1].astype (np.float) * yScale
                   coordinates[:,1] = rescaleYCord.astype (np.int)
                   del rescaleYCord
                   coordinates[:,1] = np.clip (coordinates[:,1], 0, ydim -1)               
               self._shape = (xdim, ydim)               
               self._setContourPerimeter (coordinates)                  
               self._initContourArea()      
           else:                              
               self._shape = (xdim, ydim)               
               resizeShape = (int (float(self._getCArea().shape[0]) * xScale), int (float(self._getCArea().shape[1]) * yScale))
               try :
                   contouredArea = resize(self._getCArea(), resizeShape , order = 0, preserve_range = True)                          
               except:
                   contouredArea = imresize(self._getCArea(), resizeShape , interp = "nearest", mode = None)                          
               
               new_contouredArea = np.zeros ((xdim, ydim), dtype = self._getCArea().dtype)
               dX1, dX2, sX1, sX2 = Contour._getSourceDestShape (new_contouredArea.shape[0], contouredArea.shape[0])
               dY1, dY2, sY1, sY2 = Contour._getSourceDestShape (new_contouredArea.shape[1], contouredArea.shape[1])
               new_contouredArea[dX1:dX2,dY1:dY2] = (contouredArea[sX1:sX2,sY1:sY2]).astype (new_contouredArea.dtype)
               self._setContourDefinedByMask (new_contouredArea)
                              
           
       
   @staticmethod #   @jit (cache=True)
   def _decodeLine (mem, y, formatType, data) :
       if y >= 0 and y < mem.shape[0] :           
           if (formatType == 0) :   #Run Length
               for tup in data :
                   firstIndex, lastIndex = tup[0], tup[1]
                   firstIndex = max (0, firstIndex)
                   lastIndex = min (lastIndex, mem.shape[1])                   
                   if (firstIndex <= lastIndex):
                       mem[y, firstIndex:lastIndex+1] = 1
                   #else:                        
                   #     print  ("Unexpected")
                   #     print ((y, firstIndex, lastIndex))
           elif (formatType == 1) :               
               firstIndex, Raw = data[0],data[1] #("RAW")
               if (len (Raw) > 0) :
                   Raw = np.array (Raw,dtype=np.uint8)
                   firstIndex = max (0, firstIndex)
                   lastIndex = min (firstIndex+Raw.shape[0], mem.shape[1])
                   mem[y, firstIndex:firstIndex+Raw.shape[0]] = Raw[:]
           #else:
           #    print ("Error UnRecognized Line Format Type")
   
   @staticmethod 
   def _decodeLineObj (mem, lineobj) :
       if lineobj.hasParameter ("I") and lineobj.hasParameter ("F") :
           y = lineobj.getParameter ("I")
           formatType = lineobj.getParameter ("F")           
           if (formatType == 0) :
               if lineobj.hasParameter ("RunLen") :
                   data =  lineobj.getParameter ("RunLen")
                   Contour._decodeLine (mem, y, formatType, data)                                     
           elif (formatType == 1) :
               if lineobj.hasParameter ("RAW") :
                   data = lineobj.getParameter ("RAW")
                   Contour._decodeLine (mem, y, formatType, data)                                                        
           else:
               print ("Error UnRecognized Line Format Type")
   
   @staticmethod 
   def _decodeImageLineObj (mem, line) :
       if (len (line) == 3) :
           y = line[0]       
           formatType = line[1]                  
           data = line[2]                  
           Contour._decodeLine (mem, y, formatType, data)
                  
               
   @staticmethod 
   def _decodeContourMaskObj (shape, contourMaskObj) :       
       if contourMaskObj.hasParameter ("Img") :
           mem = np.zeros (shape, dtype=np.uint8)
           linelist = contourMaskObj.getParameter ("Img")
           for line in linelist :               
               Contour._decodeImageLineObj (mem, line)
       elif contourMaskObj.hasParameter ("LineList") :
           mem = np.zeros (shape, dtype=np.uint8)
           linelist = contourMaskObj.getParameter ("LineList")
           for linenumber in linelist :
               lineobj = contourMaskObj.getFileObject ("L_"+str (linenumber))
               Contour._decodeLineObj (mem, lineobj)
       elif contourMaskObj.hasParameter ("Pickle_Data") :
           import pickle
           import zlib
           import binascii
           data = contourMaskObj.getParameter ("Pickle_Data")
           shape = contourMaskObj.getParameter ("Data_Shape")           
           data =  binascii.unhexlify(data)
           data = zlib.decompress (data)
           data = pickle.loads (data)
           mem = np.unpackbits (data, axis = 0)
           del data
           mem = mem[0:shape[0],0:shape[1]]           
       return mem
   
   @staticmethod 
   def setPickleMaskFileObj (img, fobj) :     
       import pickle
       import zlib
       import binascii
       compressedMem = np.packbits (img, axis = 0)
       compressedMem = pickle.dumps (compressedMem, protocol=2)
       compressedMem = zlib.compress (compressedMem)
       compressedMem = binascii.hexlify (compressedMem)
       compressedMem = compressedMem.decode('utf-8')                       
       fobj.setParameter ("Pickle_Data", compressedMem)
       del compressedMem
       fobj.setParameter ("Data_Shape", tuple (img.shape))
       
   
   @staticmethod     #@jit (cache=True)
   def _getLineExportData (lineData) :
       #return a list of tuples marking first and last ranges of the mask       
       length = lineData.shape[0]
       if (length == 0) :
           return []
       lineData = np.clip (lineData, 0,1)
       if (length == 1 and lineData[0] == 1) :
           return [(0,0)]
       
       lineReturnList = []
       transitions = lineData[:-1] - lineData[1:]
       nonZerotransitions = transitions.nonzero ()[0]
       if (lineData[0] != 0):
           previousIndex = 0
       else:
           previousIndex = -1
           
       #nonZerotransitions = nonZerotransitions.tolist ()       
       for index in nonZerotransitions:
           if (previousIndex == -1) :                              
               previousIndex = index + 1
           else:                      
               nextIndex = index      
               #if (previousIndex > nextIndex) :
               #    print ("Index Error")
               lineReturnList.append ((previousIndex,nextIndex))
               previousIndex = -1
              
       if previousIndex != -1 :          
           lastIndex = len (lineData) -1
           lineReturnList.append ((previousIndex,lastIndex))           
              
       return lineReturnList 
   
   @staticmethod #   @jit(cache=True)
   def _EncodeLineObj (linenumber, contourMask) :
       lineData = contourMask[linenumber, :]
       if (not np.any (lineData)) :
           return None
       returnLst = [linenumber]       
   
       lineList = Contour._getLineExportData (lineData)   
       
       if (len (lineList) == 0) :
           returnLst.append (1)           
           returnLst.append ((0, []))                      
       else:   
           firstIndex, lastIndex = lineList[0][0], lineList[len(lineList)-1][1]
           flatlen = lastIndex - firstIndex + 2
                      
           if flatlen < len (lineList) * 3 :
               returnLst.append (1)           
               returnLst.append ((firstIndex, lineData[firstIndex:lastIndex + 1].tolist ()))                                     
           else:
               returnLst.append (0)                          
               returnLst.append (lineList)                                                    
       return returnLst
       
                
   @staticmethod 
   def _EncodeContourMaskObj (contourMask) :
       fobj = FileObject ("ContourMask")       
       LineList = []
       for linenumber in range (0, contourMask.shape[0]) :
           lineObj = Contour._EncodeLineObj (linenumber, contourMask)
           if (lineObj is not None):               
               LineList.append (lineObj)
       fobj.setParameter ("Img", LineList)
       return fobj
 
   def _getEncodeContourMaskFileObj (self) :
       if (self._cachedContourMaskFileObj is None) :
           self._cachedContourMaskFileObj = Contour._EncodeContourMaskObj (self._getCArea())
       return self._cachedContourMaskFileObj
       
   def _setEmptyContourPerimeter (self):
       self._coordinates = np.array ([], dtype=np.int) 
       
   def isContourDefinedByPerimeter (self) :
       #returns True if contour object is defined by a perimeter, 
       #if false contour is defined directly by the mask defined in 
       return len (self._coordinates) > 0
   
   def _setContourDefinedByMask (self, mask) :
       self._setEmptyContourPerimeter ()       
       if tuple (mask.shape) != tuple (self._shape) :
            self._setCArea (np.zeros(self._shape, dtype=np.uint8))
       else: 
           if (mask.dtype != np.uint8) :
               self._setCArea (np.copy (np.clip (mask,0,1).astype (np.uint8)))
           else:
               self._setCArea (np.copy (np.clip (mask,0,1)))
       self._area =  None
       
   def setContourInitalizedForCrossSliceChange (self)  :
       self._initalizedForCrossSliceChange = True
      
   def clearContourInitalizedForCrossSliceChange (self)  :
       self._initalizedForCrossSliceChange = False
       
   def isContourInitalizedForCrossSliceChange (self)  :
       return self._initalizedForCrossSliceChange
       
   def clearMaskContourCrossSlice (self, Axis, SliceRange, ContourType = "Computer") :
       if self._initalizedForCrossSliceChange :                        
            mem = self._getCArea ()
            firstSlice, lastSlice = SliceRange
            if Axis == "X":
                mem[firstSlice:lastSlice,:] = 0
            else:
                mem[:,firstSlice:lastSlice] = 0
            self._setCArea (mem)
            self._area =  None
            self._HumanEditedContour =  ContourType == "Human"            
            self._setChanged ()
       
        
            
   #used by ML_Predict to change adjust contours on perpendicular axis fast.
   def setMaskContourCrossSlice (self,Data, X = None, Y = None, ContourType = "Computer") :
        if self._initalizedForCrossSliceChange :            
            mem = self._getCArea ()
            if X is not None :
                mem[X,:] = Data
            elif Y is not None :
                mem[:,Y] = Data
            else:
                mem[...] = Data[...]
            self._setCArea (mem)
            self._area =  None
            self._HumanEditedContour =  ContourType == "Human"            
            self._setChanged ()

   def getMaskContourCrossSlice (self, X = None, Y = None) :        
            mem = self._getCArea ()
            if X is not None :
                return mem[X,:]
            else:
                return mem[:,Y]            
     
   def getFileObjectCache (self) :
       return self._fileObjectCache
       
   def setFileObjectCache (self, cache) :
       self._fileObjectCache = cache
       
   def _setChanged (self):              
       self._fileObjectCache = None 
       self._cachedContourMaskFileObj = None    
       
   def isChanged (self) :
       return  self._fileObjectCache is None
   
   def checkSliceShape (self, xdim, ydim) :
       if self._shape[0] == xdim and self._shape[1] == ydim :
           return False
       return True
         
   """def getMinMaxXYBounding (self) :   
       if (self.isContourDefinedByPerimeter ()): 
           maxX = np.max (self._coordinates[0,:])
           maxY = np.max (self._coordinates[1,:])
           minX = np.min (self._coordinates[0,:])
           minY = np.min (self._coordinates[1,:])
           return (minX, minY, maxX, maxY)
       else:
          inside = self._getCArea() #ndimage.morphology.binary_erosion (self._contouredArea)
          minX, minY, maxX, maxY = Contour.getXYMinMax (inside)          
          if (minX is None) :
              return (0,0, self._shape[0] - 1, self._shape[1] - 1)
          return (minX, minY, maxX, maxY)"""
          
   def addToFileObj (self, f) :              
       f.setParameter ("ContourID", self._ContourID)         
       f.setParameter ("HumanEditedContour", self._HumanEditedContour)         
       f.setParameter ("Shape", self._shape)         
       f.setParameter ("LastDrawDirection", self._LastDrawDirection)                  
       f.setParameter ("SnappedHumanEditedSliceCount", self._SnappedHumanEditedSliceCount)         
            
       if (self.isContourDefinedByPerimeter ()) : 
           f.setParameter ("XY", self._coordinates.tolist ())           
       else:
           f.addInnerFileObject (self._getEncodeContourMaskFileObj ())
          
   def isHumanEditedContour (self) :
       return self._HumanEditedContour
          
   
   def getHumanEditedContourSnapCount (self) :
       return self._SnappedHumanEditedSliceCount        
       
   def getContourID (self):
       return (self._ContourID)
       
   def getArea (self) : 
       if self._area is None :
           self._area =  np.sum (self._getCArea ())  
       return self._area
              
   def _initContourArea (self) :
       if (self.isContourDefinedByPerimeter ()) :
           rr, cc = polygon (self._coordinates[1, :], self._coordinates[0, :])
           cc = np.clip (cc, 0, self._shape[0] - 1)
           rr = np.clip (rr, 0, self._shape[1] - 1)       
           mem = np.zeros(self._shape, dtype=np.uint8)       
           mem[cc, rr] = 1
           self._setCArea (mem)
           self._area =  None

   def isPointInsideContour (self, x, y) :
       return (self._getCArea()[x, y] == 1)

   def getCoordinates (self) :
      if (self.isContourDefinedByPerimeter ()):   
          return self._coordinates
      else:
           print ("Error Cannot return a perimeter object of a mask based contour.")
           return np.array ([], dtype=np.int)
        
   def copy(self) :
      newContour = Contour ()
      newContour._shape = self._shape
      newContour._HumanEditedContour = self._HumanEditedContour
      newContour._SnappedHumanEditedSliceCount = self._SnappedHumanEditedSliceCount
      newContour._ContourID = self._ContourID
      if (self.isContourDefinedByPerimeter ()):
          newContour._coordinates = np.copy (self._coordinates) #copy set Area
          newContour._initContourArea()  
      else:
          newContour._setContourDefinedByMask (self._getCArea())
          newContour._cachedContourMaskFileObj = self._cachedContourMaskFileObj
          
      newContour._LastDrawDirection = self._LastDrawDirection
      newContour.setFileObjectCache (self.getFileObjectCache ())
      return newContour
    
   def _setContourPerimeter (self, area) :           
       first = area[0]
       last = area[len(area) - 1]
       if (int (first[0]) != int (last[0]) or int (first[1]) != int (last[1])) :       
           firstrow = np.array ([[first[0], first[1]]])
           area = np.concatenate ((area, firstrow), axis = 0)                             
       
       area = np.array (area,dtype=np.int)
       if area.shape[0] > 2 :
           #remove points from contour which fall on the contour line. consecutive poiints with the same slope are on the same line.
           slope = area[:-1] - area[1:]           
           dx = slope[:-1,0] - slope[1:,0]
           dy = slope[:-1,1] - slope[1:,1]
           del slope
           dx[dx != 0] = 1
           dx[dy != 0] = 1  
           del dy           
           dx[dx.shape[0] -1] =  1
           cord_lst = dx.nonzero ()[0]           
           del dx
           cord_lst += 1
           firstindex = np.zeros ((1,), dtype = np.int)
           cord_lst = np.concatenate ([firstindex, cord_lst, firstindex])
           area = area[cord_lst]               
           del cord_lst 
       self._coordinates = area.T                 
       self._setChanged ()
       self._initContourArea()
   
   def addContourArea (self, area, setVal):       
       if (setVal == 1) :          
           return np.bitwise_or (area, self._getCArea())
       else:
           return np.bitwise_and (area, 1 - self._getCArea())

   def getContourArea (self) :                 
      return np.copy (self._getCArea())
   
   def contourDefinesMask (self) :
       return np.any (self._getCArea())
   
   def center (self) :              
           yDim = range (self._shape[1]) 
           xDim = range (self._shape[0])           
           xVec = np.matmul (self._getCArea().T, xDim)
           yVec = np.matmul (self._getCArea(), yDim)
           cx = np.sum (xVec)
           cy = np.sum (yVec)           
           return (cx, cy, self.getArea())
              
   def getLastDrawDirection (self) :
        return self._LastDrawDirection
                   
class ContourNester :
    def __init__ (self) :
        self._contourTracker = {}

    def _getContour (self, contour) :
        if contour not in self._contourTracker :
            contourDictionary = {}
            contourDictionary["ParentList"] = []
            contourDictionary["ChildernList"] = []
            contourDictionary["SiblingList"] = []
            contourDictionary["ParentLastDrawDirection"] = -1
            contourDictionary["ParentCount"] = 0
            contourDictionary["OtherContourAreaTest"] = 2 * contour._getCArea()       
            contourDictionary["SelfContourAreaTest"] = -1 * contour._getCArea()                   
            self._contourTracker[contour] = contourDictionary             
        return self._contourTracker[contour]
    
    def _getContourList (self, contour, listName) :
        contourDictionary = self._getContour(contour)        
        if listName not in contourDictionary :
            contourDictionary[listName] = []
        return contourDictionary[listName]    
            
    def setParentLastDrawDirection (self, contour, direction): #Function used soley in def _InitLineCross (self):       
         self._getContour(contour)["ParentLastDrawDirection"] = direction         
        
    def getParentLastDrawDirection (self, contour):            #Function used soley in def _InitLineCross (self):       
       return self._getContour(contour)["ParentLastDrawDirection"] 
   
    @staticmethod    
    def _buildStr (lst) :
       first = True
       parentstring = ""
       for parent in lst :
           if (first) :
               parentstring = str (parent._ContourID)
               first = False
           else:
               parentstring = parentstring + ", " + str (parent._ContourID)
       return parentstring
   
    
    def printDebug (self, contour) :               
        parentstring = ContourNester._buildStr (self._getContourList (contour, "ParentList") )
        childstring = ContourNester._buildStr (self._getContourList (contour, "ChildernList"))
        siblingstring = ContourNester._buildStr (self._getContourList (contour, "SiblingList"))
      
        debugmsg = str(self._ContourID) + ":"
        debugmsg = debugmsg + " Parent Count: " + str (self.getParentCount (contour))
        if (parentstring != "") :
            debugmsg = debugmsg + " Parents (" + parentstring +  ")"
        if (childstring != "") :
            debugmsg = debugmsg + " Childern (" + childstring +  ")"
        if (siblingstring != "") :
            debugmsg = debugmsg + " Siblings (" + siblingstring +  ")"
        print (debugmsg)
               
    def addParent(self, contour, parent) :
        cList = self._getContourList(contour, "ParentList")
        if (parent not in cList) :
            cList.append (parent)                       
        cList = self._getContourList(parent, "ChildernList")
        if (self not in cList) :
            cList.append (contour)
               
    def addSibling (self, contour, sibling) :
       cList = self._getContourList (contour, "SiblingList") 
       if (sibling not in cList) :
            cList.append (sibling)
       cList = self._getContourList (sibling, "SiblingList") 
       if (contour not in cList) :
            cList.append (contour)        
            
    
        
    def _hasParent (self, contour, parent) :
         return parent in self._getContour(contour)["ParentList"]
    
    def _hasChild (self, contour, child):
        return child in self._getContourList(contour, "ChildernList")
            
    def _hasSibling (self, contour, sibling):
         return sibling in self._getContourList(contour, "SiblingList")

    def resetParentCount (self, contour):
        self._getContour(contour)["ParentCount"] = len (self._getContour(contour)["ParentList"])        
        
    def decementParentCount (self, contour):
        try :
            self._getContour(contour)["ParentCount"] -= 1
        except :
            self._getContour(contour)["ParentCount"] = 0        
            
    def getParentCount (self, contour):
        try :
            return self._getContour(contour)["ParentCount"]
        except:
            return 0
        
    def getChildList (self, contour):
        return self._getContourList(contour, "ChildernList")         
       
    def testIfParentorSibling (self, contour, other): #returns true if other is parent
        if (self._hasChild (contour, other) or self._hasSibling (contour, other) or self._hasParent (contour, other)):
            return False, False       
        otherOnly = False 
        selfOnly = False
        both = False                    
        selfContourAreaTest = self._getContour(contour)["SelfContourAreaTest"] 
        test =  self._getContour(other)["OtherContourAreaTest"] + selfContourAreaTest
        if  2 in test  : 
            otherOnly = True
        if -1 in test  :
            selfOnly = True
        if  1 in test  :
            both  = True                              
                               
        if (otherOnly and not selfOnly and not both) :
            print ("Test if PArent or Sibling Error")
            if (1 in selfContourAreaTest) :
                print ("One found")
            else:
                print ("All No area")
            
        testedSliceIsParent  = (otherOnly and both and not selfOnly)
        testedSliceIsSibling = (otherOnly and not both and selfOnly)
        return testedSliceIsParent, testedSliceIsSibling 


class AxisIndex :
    def __init__ (self, axisIndex, AxisShape):
        self._axisIndex = axisIndex
        #self._projectionAxisMask = None
        #self._projectionState = None
        if AxisShape is None :
            self._axisMask = None
        else:
            self._axisMask = np.zeros ((AxisShape[axisIndex],), dtype = np.bool)

    def clear (self, SetDimToNone = False, ClearSelectSliceList = None) :
        if SetDimToNone :
            self._axisMask = None    
        elif self._axisMask is not None :
            self._axisMask[...] = False
        
    def copy (self) :
        newData = AxisIndex (self._axisIndex, None)
        if self._axisMask is not None :
            newData._axisMask = np.copy (self._axisMask)
        else:
            newData._axisMask = None
        return newData
    
    def getAxis (self) :
        return self._axisIndex

    def setMask (self, axisData, OptimizedForInit = False) :        
        if axisData is None :            
            if OptimizedForInit :
                if self._axisMask is not None :
                    self._axisMask[...] = False
                    return
                return
            if self._axisMask is not None :
               indexSetToZero = (self._axisMask).nonzero ()[0]
               self._axisMask[...]  = False
               return indexSetToZero
            return None
        data = np.any (axisData, axis = int (1-self._axisIndex))
        if OptimizedForInit :
            self._axisMask = data
            return        
        if self._axisMask is not None :
            indexSetToZero = (data.astype (np.int8) - self._axisMask.astype (np.int8) < 0).nonzero ()[0]
        else:
            indexSetToZero = None
        self._axisMask = data
        return indexSetToZero        
        
    def getAxisMask (self, NoCopy = False) :
        if self._axisMask is None : 
            return None
        if NoCopy :
            return self._axisMask
        else:
            return np.copy (self._axisMask)
        
    def getMaskValuesAtIndexs(self, indexs) :
        if self._axisMask is not None :
            return self._axisMask[indexs]
        return None
    
    """def setAxisProjection (self, projectionControl=None, VolumeMemory = None) :
        if projectionControl is None :  # Axis index 0 = X   Axis Index = 1 = Y
            self._projectionAxisMask = None
        AxisNormal = projectionControl.getAxisNormal (self.getAxis ())
        if AxisNormal is None or AxisNormal[1] == 0 :
            self._projectionAxisMask = None
        if VolumeMemory is not None :
            self._projectionAxisMask = np.any (VolumeMemory, axis = self.getAxis ())
        else:
            self._projectionAxisMask = None
      
        
    def getAxisProjection (self, NoCopy = False) :
        if self._projectionAxisMask is not None :
            if NoCopy :
                return self._projectionAxisMask
            return np.copy (self._projectionAxisMask)
        return self.getAxisMask (NoCopy = NoCopy)"""
        
    """
    getROIVolume (self, SharedMemCache = None):
    roiVolume 
    def updateAxisProjection ()
        

    def getAxisProjection (self, projectionControl, roivolume, NoCopy = False) :
        if projectionControl is None :  # Axis index 0 = X   Axis Index = 1 = Y
            return self.getAxisMask (NoCopy)
        normal = projectionControl.getAxisNormal ()
        
        axis = self.getAxis ()
        
        if normal[0] == 0.0 :
            return self.getAxisMask (NoCopy)
        if self._projectedAxisNormal is not None and not np.any (self._projectedAxisNormal - normal != 0) :
            pass
            #rebuild cached Axis
        else:
            sliceIndex = projectionControl.getAxisSliceCount (axis, roivolume)
            for index in sliceIndex :
                projectedSlice = projectionControl.getProjectedSlice (sliceIndex, axis, roivolume)
                data = np.any (axisData, axis = int (1-self._axisIndex))"""
   
    
    def updateMaskFromChildMask (self, newMask, indexsChanged, listofindexmasks) :        
        if self._axisMask is None :
            value = newMask.getAxisMask (NoCopy = True)
            if value is not None :
                self._axisMask = np.copy (value)
        else:
            value = newMask.getAxisMask (NoCopy = True)
            if value is not None :
                np.logical_or (self._axisMask, value, out = self._axisMask)
        if indexsChanged is not None and len (indexsChanged) > 0 :
            if len (listofindexmasks) > 0 :
                stackedMasks = np.stack (listofindexmasks)
                HasMaskAtIndex = np.any (stackedMasks, axis = 0)
                self._axisMask[indexsChanged] = HasMaskAtIndex
            else:
                self._axisMask[indexsChanged] = False
                
        """if self._projectionState is not None :
            if "ChangedIndex" not in self._projectionState :
                self._projectionState["ChangedIndex"] = np.zeros (self._axisMask.shape, dtype=np.bool)
            self._projectionState["ChangedIndex"][indexsChanged]   """
            

class SinglROIAreaObjDef :    
   def __init__ (self, sliceNumber = None, contour = None, shape = None, finterface = None, MultiROIAreaObjectParent = None, DelayInitLineCross = False):       
       if (MultiROIAreaObjectParent is not None) :
           self._MultiROIAreaObjectParent = weakref.ref (MultiROIAreaObjectParent)() 
       else:
           self._MultiROIAreaObjectParent = None 
       self._ContouredArea = None
       #self._contouredAreaCompressedAxisDim = 0
       self.setInitLineCross ()    
       self._contourDictionary = {} 
       self._objectContainsPerimeterContours = False      
       self._shape = shape       
       self._setFileObjectCache (None)
       self._hasCopied = True
       self._XAxisMask = AxisIndex (0, shape)
       self._YAxisMask = AxisIndex (1, shape)
       
       if (sliceNumber is not None) :
           self._sliceNumber = sliceNumber  
           if (contour is not None) :
               self._setContourDictionary(contour.getContourID (), contour)                      
           
       elif (finterface is not None) :
           self._sliceNumber, self._shape = finterface.getParameter ("Header")             
           contourFileObjects = finterface.getFileObjects ()
           for contourFile in contourFileObjects :
               contourID = contourFile.getParameter ("ContourID")
               self._setContourDictionary (contourID, Contour (finterface = contourFile))       
           self._setFileObjectCache (finterface, SetWriteCache = False)
       self._clearParentClippingCache ()
       
   def _clearParentClippingCache (self) :
       try :
           if  self._MultiROIAreaObjectParent is not None and self._sliceNumber is not None :                      
               self._MultiROIAreaObjectParent._clearObjClipMask (Axis = "Z", SliceNumber = self._sliceNumber)
       except:
           pass
       
    
   def hasDataInAxisMaskRange (self, firstSliceNumber,LastIndex, Axis) :
       if self._setInitLineCross :
           self.getSingleSliceContouredAreaFromContourData ()
       if Axis == "X" :
           mask = self._XAxisMask.getAxisMask (NoCopy = True)
       if Axis == "Y" :
           mask = self._YAxisMask.getAxisMask (NoCopy = True)
       if mask is None :
           return False
       return np.any (mask[firstSliceNumber:LastIndex+1])
       
   def getXAxisMask (self) :
       if self._setInitLineCross :
           self.getSingleSliceContouredAreaFromContourData ()
       return self._XAxisMask
       
   def getYAxisMask (self) :
       if self._setInitLineCross :
           self.getSingleSliceContouredAreaFromContourData ()
       return self._YAxisMask
    
   def isROIRunningInLowMemoryMode (self) :
       try :
           return self._MultiROIAreaObjectParent.isRunningInLowMemoryMode ()
       except:
           return False
              
   def sliceContainsPerimeterContours (self) : 
       return self._objectContainsPerimeterContours
       
   def isContourAreaNotNone (self) :
       return self._ContouredArea is not None
   
   def isContourAreaNone (self) :
       return self._ContouredArea is None
   
   def _setContourDictionary (self, index, contourData) :
       self._contourDictionary[index] = contourData
       self._objectContainsPerimeterContours = self._objectContainsPerimeterContours or contourData.isContourDefinedByPerimeter ()
       if (self.isROIRunningInLowMemoryMode ()) :
           contourData.setRunningInLowMemoryMode ()
   
   def setContourAreaMemToNone (self) :
       self._ContouredArea = None
        
   def _setContourAreaMem (self, mem): #used to track setting variable to facilitate memory management
       if self.isROIRunningInLowMemoryMode () :
           self._ContouredArea = None
       else:
           self._ContouredArea = mem
       if (mem is not None and self._MultiROIAreaObjectParent is not None) :
           try :
               self._MultiROIAreaObjectParent.notifySliceSetContourAreaParameter ()
           except:
               pass 
       
   def ifPossibleCompressROISliceMemory (self) :
       if (self.isContourAreaNotNone ()) :
           self._ContouredArea = None
           for value in self._contourDictionary.values () :
               value.compressContourMemory ()
       return True
       
   
   def setSliceNotCopied (self) :
       self._hasCopied = False
       
   def hasCopied (self) :
       return self._hasCopied
   
   def clear (self) :       
       #self._setCopyRequired () Done not required
       self._XAxisMask.clear ()
       self._YAxisMask.clear ()
       self.setObjectChanged ()
       self.setInitLineCross ()
       self._MultiROIAreaObjectParent = None       
       self._contourDictionary = {}  
       self._objectContainsPerimeterContours = False            
       self._shape = (0,0)       
       self._sliceNumber = 0
       self._setFileObjectCache (None)
       self._clearParentClippingCache ()
    
   def setMultiROIAreaObjectParent (self, parent) :
       self._MultiROIAreaObjectParent  = parent
       
   def setInitLineCross (self) :       
       self._ContouredArea = None
       self._setInitLineCross = True              
       if (self._MultiROIAreaObjectParent is not None) :
           self._MultiROIAreaObjectParent._setSingleSliceInitLineCross ()
           self._clearParentClippingCache ()

   def isInitLineCrossSet (self) :
       return self._setInitLineCross   
   
   def getSingleSliceContouredAreaFromContourDataForMultiSliceParent (self) :
       if (self._setInitLineCross or self.isContourAreaNone()) :
           self.setInitLineCross ()   
           ca, xAxisMaskIndexs, yAxisMaskIndexs = self._InitLineCross ()
           self._setContourAreaMem (ca)
           self._setInitLineCross = False 
           return ca, xAxisMaskIndexs, yAxisMaskIndexs
       return self._ContouredArea, None, None
       
   def getSingleSliceContouredAreaFromContourDataForMultiSliceParent_OptimizedForInit (self) :
       if (self._setInitLineCross or self.isContourAreaNone()) :    
           self._clearParentClippingCache ()
           ca = self._InitLineCross (OptimizedForInit = True)  
           if self.isROIRunningInLowMemoryMode () :
               self._ContouredArea = None
           else:
               self._ContouredArea = ca
           self._setInitLineCross = False  
           return ca
       return self._ContouredArea
   
   def getSingleSliceContouredAreaFromContourData (self, ReturnAxisIndexs = False) :
       SetParent = False
       if (not self._setInitLineCross) :
           self.setInitLineCross ()
       ca, xAxisMaskIndexs, yAxisMaskIndexs = self._InitLineCross ()
       self._setContourAreaMem (ca)
       self._setInitLineCross = False       
       try:
           if (self._MultiROIAreaObjectParent is not None and  self._sliceNumber is not None) :               
               SetParent = self._MultiROIAreaObjectParent.setVolumeSliceMask_Data_SliceCallback (self._sliceNumber, ca)
               if not ReturnAxisIndexs :
                   self._MultiROIAreaObjectParent.setVolumeAxisMask (self._sliceNumber, self._XAxisMask, xAxisMaskIndexs, self._YAxisMask, yAxisMaskIndexs)
       except: 
           SetParent = False
       if  ReturnAxisIndexs :
          return ca, SetParent,  xAxisMaskIndexs, yAxisMaskIndexs
       return ca, SetParent
   
   def _getSingleSliceContouredArea (self) :
       if (self._setInitLineCross) :
           _ContouredArea, setParent = self.getSingleSliceContouredAreaFromContourData ()           
           if (setParent) :
                self._setContourAreaMem (None)                 
           return _ContouredArea
       else:           
           if self.isContourAreaNotNone () :
               return self._ContouredArea         
           else: 
               try :
                   if (self._MultiROIAreaObjectParent is not None and self._MultiROIAreaObjectParent.isSliceMaskVolumeCacheSet () and self._MultiROIAreaObjectParent.isSliceValidInVolumeCache(self._sliceNumber) ):
                       return self._MultiROIAreaObjectParent.getVolumeSliceMask_Data (self._sliceNumber)                                          
               except:
                   pass               
               _ContouredArea, _ = self.getSingleSliceContouredAreaFromContourData ()               
               return _ContouredArea               
       
   def _getFileObjectCache (self, Name):
       if (Name is not None) :
           if self._fobjCache.getFileObjectName () != Name :
               self._fobjCache.setFileObjectName (Name)
       return self._fobjCache 
   
   def _setFileObjectCache (self, fobj, SetWriteCache = True) :       
       #self._setCopyRequired () Dont think this is needed
       if (fobj is not None and SetWriteCache) :
           fulltext = fobj.getFileObjectTxt ()
           fobj.setWriteCache (fulltext)
       self._fobjCache = fobj
       
   def setObjectChanged (self) :       
       self._setFileObjectCache (None)
       if (self._MultiROIAreaObjectParent is not None) :
           self._MultiROIAreaObjectParent.setObjectChanged ()
       
   def isChanged (self) :       
       return self._getFileObjectCache (Name = None) is None
       
       
   def clipSliceToDimensions (self, xdim,ydim, xScale, yScale, sliceNumber, HumanEdited = True) :              
       #self._setCopyRequired ()    Done         
       if (self._sliceNumber != sliceNumber) :           
           self.setObjectChanged ()           
           self._sliceNumber = sliceNumber   
       if (self._shape != (xdim, ydim) or xScale != 1.0 or yScale != 1.0) :                      
           self.setObjectChanged ()
           self._shape = (xdim, ydim)          
           for contour in self._contourDictionary.values () :
               contour.clipContourToDimensions (xdim, ydim, xScale, yScale, HumanEdited)                      
           self.setInitLineCross ()           
           
       
       
   def checkSliceShape (self, xdim, ydim) :
       #self._setCopyRequired ()   Done          
       dimchanged = False
       self._shape = list (self._shape)
       if (self._shape[0] != xdim):           
           dimchanged = True
           self._shape[0] = xdim
       if (self._shape[1] != ydim):           
           dimchanged = True
           self._shape[1] = ydim
       self._shape = tuple (self._shape)  
       updateList = []
       for key ,contour in self._contourDictionary.items () :
           if (contour.checkSliceShape (xdim, ydim)):
               updateList.append ((key, contour))
       if (len (updateList) > 0) :           
           for pair in updateList :
               key, contour = pair                             
               self._setContourDictionary (key, Contour (shape = (xdim, ydim), ContourToCopy = contour))
           dimchanged = True
       if (dimchanged) :           
           self.setInitLineCross ()           
           self.setObjectChanged ()
           return True
       return False
       
   def getContourIDList (self) :
       return list (self._contourDictionary.keys ())
       
   def getShape (self) :
       return self._shape
  
   def setSliceNumber (self, slicenumber):
       #self._setCopyRequired () Done
       self._sliceNumber = slicenumber
       #self.setObjectChanged ()  Avoid Unecessary Save if just slice number is modified
   
       
   def copy ( self, MultiROIAreaObjectParent, DeepCopyContours = False) :
        newobj = SinglROIAreaObjDef (sliceNumber = self._sliceNumber, shape = self._shape,  MultiROIAreaObjectParent = MultiROIAreaObjectParent)        
        newobj._setFileObjectCache (self._getFileObjectCache (Name = None), SetWriteCache = False)
        newobj._XAxisMask = self._XAxisMask.copy ()
        newobj._YAxisMask = self._YAxisMask.copy ()
        if (not DeepCopyContours) :
            for number, contour in self._contourDictionary.items () :
                newobj._contourDictionary[number] = contour
        else:
            for number, contour in self._contourDictionary.items () :
                newobj._contourDictionary[number] = contour.copy ()                
        newobj._objectContainsPerimeterContours = self._objectContainsPerimeterContours
        if (not self._setInitLineCross) :
            newobj._setInitLineCross = False
            if (self.isContourAreaNone()) :    
                newobj._ContouredArea = None                
            else:
                newobj._ContouredArea = np.copy (self._ContouredArea)
            #newobj._contouredAreaCompressedAxisDim = self._contouredAreaCompressedAxisDim                
        #self._resetCopyRequired ()
        return newobj  

   def initalizeContourAreaFromContourIfNone (self) :
       if self.isContourAreaNone() :
           try :
               self.getSingleSliceContouredAreaFromContourData ()
           except:
               self._ContouredArea = None          
           
       
   def getMaxHumanEditedContourSnapCount (self) :
       snapcount = 0
       for contour in self._contourDictionary.values () :
           snapcount = max (snapcount, contour.getHumanEditedContourSnapCount ())
       return snapcount
           
   def setHumanEditedContour (self, contour, humanedited) :
           #self._setCopyRequired () Done        
           if (contour.getContourID () in self._contourDictionary and self._contourDictionary[contour.getContourID ()].isHumanEditedContour () != humanedited) :                              
               self.setObjectChanged ()               
               if (humanedited):
                   self._setContourDictionary(contour.getContourID (), Contour (contourType = "Human", ContourToCopy = contour))
               else:
                   self._setContourDictionary(contour.getContourID (), Contour (contourType = "Computer", ContourToCopy = contour))
               
               
   
   def setHumanEditedContourSnapCount (self, contour, snappedSliceCount) :
           #self._setCopyRequired ()      Done        
           if (contour.getContourID () in self._contourDictionary and self._contourDictionary[contour.getContourID ()].getHumanEditedContourSnapCount () != snappedSliceCount) :                              
               self.setObjectChanged ()               
               self._setContourDictionary (contour.getContourID (), Contour (SnappedHumanEditedSliceCount = snappedSliceCount , ContourToCopy = contour))
               
       
   def addContour (self, contour, DelayInitLineCross = False) :           
       #self._setCopyRequired () Done               
       self.setObjectChanged ()
       if (contour.isHumanEditedContour () or not self.hasContour (contour.getContourID ())) :
           self._setContourDictionary (contour.getContourID (), contour)           
       else:           
            humanedited = self._contourDictionary[contour.getContourID ()].isHumanEditedContour ()
            if (humanedited):
                 self._setContourDictionary (contour.getContourID (), Contour (contourType = "Human", ContourToCopy = contour))
            else:
                 self._setContourDictionary (contour.getContourID (), Contour (contourType = "Computer", ContourToCopy = contour))
       self.setInitLineCross ()  
         
   def getMaskContourCrossSlice (self,contourID, X = None, Y = None) :        
       if self.hasContour (contourID) :
           contourData = self._contourDictionary[contourID] 
           return contourData.getMaskContourCrossSlice (X = X, Y = Y)
       return None
   
   def isSliceInitalizedForCrossSliceChange (self,contourID):
       if self.hasContour (contourID) :
           contourData = self._contourDictionary[contourID] 
           return contourData.isContourInitalizedForCrossSliceChange ()
       return None
   
   def clearContourAreaMapForCrossSliceChange (self, contourID, Axis, SliceRange, ContourType = "Computer") :                                   
       if self.hasContour (contourID) :
           self.setObjectChanged ()
           contourData = self._contourDictionary[contourID] 
           if ContourType != "Human" :               
                if contourData.isHumanEditedContour () :
                    ContourType = "Human"                    
           if not contourData.isContourInitalizedForCrossSliceChange () :               
               contourData = Contour (ContourToCopy = contourData)                                                    
               contourData.setContourInitalizedForCrossSliceChange ()               
           contourData.clearMaskContourCrossSlice (Axis, SliceRange, ContourType = ContourType)           
           self._setContourDictionary (contourID, contourData)                             
           self.setInitLineCross ()     
       
         
    
   def setContourAreaMapForCrossSliceChange (self, contourID, Data, X = None, Y = None, ContourType = "Computer") :
       #self._setCopyRequired () Done               
       self.setObjectChanged ()
       if self.hasContour (contourID) :
           contourData = self._contourDictionary[contourID] 
           if ContourType != "Human" :               
                if contourData.isHumanEditedContour () :
                    ContourType = "Human"                    
           if not contourData.isContourInitalizedForCrossSliceChange () :               
               contourData = Contour (ContourToCopy = contourData)                                                    
               contourData.setContourInitalizedForCrossSliceChange ()               
           contourData.setMaskContourCrossSlice (Data, X = X, Y = Y, ContourType = ContourType)           
           self._setContourDictionary (contourID, contourData)                  
       else:
           shape = self.getShape ()
           mem = np.zeros (shape,dtype=np.uint8)
           if X is not None :
               mem[X,:] = Data
           elif Y is not None :
               mem[:,Y] = Data
           else:
               mem[...] = Data[...]
           contour = Contour (contourID= contourID, shape= shape, contourType = ContourType, SnappedHumanEditedSliceCount= 0,contourAreaMask = mem)
           contour.setContourInitalizedForCrossSliceChange ()
           self._setContourDictionary (contourID, contour)                  
       self.setInitLineCross ()  
       
   def endCrossSliceChange (self, contourID) :       
       ContourDictionaryLen = len (self._contourDictionary)
       if ContourDictionaryLen == 0 :
          return  False
       if ContourDictionaryLen > 1 : 
          return None
       if self.hasContour (contourID) :
           contourData = self._contourDictionary[contourID] 
           if contourData.isContourInitalizedForCrossSliceChange () :                               
                contourData.clearContourInitalizedForCrossSliceChange ()
                return contourData.contourDefinesMask ()                    
       return None
                
   
   def _doesContourDefineNewArea (self, testContour) :
       for contour in self._contourDictionary.values () :
           if (contour.isContourDefinedByPerimeter ()) :
               if not np.any (contour.getContourArea () - testContour.getContourArea ())  :
                   return False
       return True
       
   def mergeSliceROI (self, slicetoMerge, sliceContourID, newContourIDToUse)  : # return update newContorIDLST
        #self._setCopyRequired () Done             
        self.setObjectChanged () 
        if (self._MultiROIAreaObjectParent is None) :
            return
        if newContourIDToUse is None :
            mergedData = None
            for ContourID, ContourData in self._contourDictionary.items () :  
                if (mergedData is None) :
                    mergedData = ContourData.getContourArea ()
                else:
                    mergedData = np.bitwise_or (mergedData, ContourData.getContourArea ())
                    
            for ContourID, ContourData in slicetoMerge._contourDictionary.items () :   
                if (mergedData is None) :
                    mergedData = ContourData.getContourArea ()
                else:
                    mergedData = np.bitwise_or (mergedData, ContourData.getContourArea ())
                    
            lst = self._MultiROIAreaObjectParent.getContourIDManger ().getAllocatedIDList ()   
            if len (lst) > 0 :
                contourID = lst[0]
            else:
                contourID = self._MultiROIAreaObjectParent.addROIAreaContour ("Batch Untitled")    
            self._setContourDictionary (contourID, Contour (contourID = contourID, ContourToCopy = ContourData, contourAreaMask = mergedData))
            self.setInitLineCross ()                     
            return None
        else:
            for ContourID, ContourData in slicetoMerge._contourDictionary.items () :           
               testContour = Contour (contourID = -1, ContourToCopy = ContourData)
               if (self._doesContourDefineNewArea (testContour)) :
                   
                   pos = sliceContourID.index (ContourID)
                   if (newContourIDToUse[pos] == -1) :
                       newContourIDToUse[pos] = self._MultiROIAreaObjectParent.addROIAreaContour ("Batch Untitled")                     
                   cID = newContourIDToUse[pos]           
                   
                   self._setContourDictionary(cID, Contour (contourID = cID, ContourToCopy = ContourData))
                   self.setInitLineCross ()        
            return newContourIDToUse
   
   def getClippingArea (self, clipx, clipy) :
        mem = np.zeros (self._shape, dtype=np.uint8)
        x1, x2 = clipx
        y1, y2 = clipy
        mem[x1:x2, y1:y2] = 1
        return mem
   
   def getSelectionMask (self, clipX, clipY):            
       return  np.bitwise_and (self._getSingleSliceContouredArea (), self.getClippingArea (clipX, clipY))                         
       
   def getVoxels (self, NiftiVolumeData, clipX, clipY, MaskedVolume = None, SliceClipMask = None) :
       zindex = self._sliceNumber     
       try :
           slicedata = NiftiVolumeData [:, :, zindex]       
       except :
           print ("Error Slice Index out of bounds!")
           slicedata = np.zeros ((NiftiVolumeData.shape[0],NiftiVolumeData.shape[1]),dtype=NiftiVolumeData.dtype)
           
       mask = self.getSelectionMask (clipX, clipY)    
       if MaskedVolume is not None :
           np.bitwise_and (mask, MaskedVolume[:,:,zindex], out=mask)
       if (SliceClipMask is not None) :
           mask[SliceClipMask > 0] = 0
       
       voxels = slicedata[mask>0]       
       return (voxels, len (self._contourDictionary))
                       
   def getContoursAtPoint (self, x,y):       
       if len (self._contourDictionary) <= 0 : 
           return []
       elif len (self._contourDictionary) == 1 :
           dictKeys = list (self._contourDictionary.keys ())[0]
           if not self._contourDictionary[dictKeys].isContourDefinedByPerimeter () :               
               mem = self._getSingleSliceContouredArea ()
               if mem[x,y] > 0 :                   
                   return [dictKeys]
               return []
       contourList = []                                
       for contourID, contour in self._contourDictionary.items () :
           if (contour.isPointInsideContour (x,y)) :
               contourList.append (contourID)
       return contourList
             
   def hasContour (self, contourID) :    
       return contourID in self._contourDictionary
   
   def isContourDefinedByPerimeterConID (self, contourID) :
       return self._contourDictionary[contourID].isContourDefinedByPerimeter ()
           
       
   def getContour (self, contourID)  :
       return self._contourDictionary[contourID]
       
   def getSliceArea (self, NonPerimeterMasksOnly = False, DoNotCopy = False) :
       if (not NonPerimeterMasksOnly) :
           if (DoNotCopy) :
               return self._getSingleSliceContouredArea ()
           else:    
               return np.copy (self._getSingleSliceContouredArea ())
       else:
          mem = np.zeros (self.getShape (), dtype = np.uint8)
          for contour in self._contourDictionary.values () :
              if not contour.isContourDefinedByPerimeter () :
                 mem[contour.getContourArea () > 0] = 1
          return mem

   def isSliceAreaDefined (self, NonPerimeterMasksOnly = False) :
       if (not NonPerimeterMasksOnly) :
          return np.any (self._getSingleSliceContouredArea ())
       else:
          for contour in self._contourDictionary.values () :
              if not contour.isContourDefinedByPerimeter () :
                 if (np.any (contour.getContourArea ())) :
                     return True
          return False
      
   def removeNonPerimeterContours (self):          
          #self._setCopyRequired ()         Done     
          contourIDLst = []
          for contourID, contour in self._contourDictionary.items () :
              if not contour.isContourDefinedByPerimeter () :
                 contourIDLst.append (contourID)
          if len (contourIDLst) > 0 :              
              for contourID in contourIDLst :
                  del self._contourDictionary[contourID]
              self._objectContainsPerimeterContours = False      
              self.setObjectChanged ()
              self.setInitLineCross ()               
          return contourIDLst
       
   def deleteContourID (self, idnumber) :              
       #self._setCopyRequired () Done
       if (idnumber in self._contourDictionary) :           
           self.setObjectChanged ()
           #foundContour = self._contourDictionary[idnumber]
           del self._contourDictionary[idnumber]           
           if len (self._contourDictionary) <= 1 :
               self._objectContainsPerimeterContours = False   
               for contour in self._contourDictionary.values () :
                   if contour.isContourDefinedByPerimeter () :
                       self._objectContainsPerimeterContours = True
           self.setInitLineCross ()                        
       return len (self._contourDictionary)
         
   def deleteAllContourID (self) :              
       #self._setCopyRequired () Done
       if (len (self._contourDictionary) == 0) :
           return []               
       self.setObjectChanged ()
       ContourIDDeleted = list (self._contourDictionary.keys ())
       self._contourDictionary = {}
       self._objectContainsPerimeterContours = False      
       self.setInitLineCross ()          
       return ContourIDDeleted

   
   @staticmethod
   def _getSinglROIAreaObjFileObj (contour) :
       if (contour.isChanged ()) :
           contourFileobj = FileObject ("Contour" + str (contour.getContourID ()))
           contour.addToFileObj (contourFileobj)       
           contour.setFileObjectCache (contourFileobj)
           return contourFileobj
       else:               
           return contour.getFileObjectCache ()
       
   def getSinglROIAreaObjFileObj (self, i, key)    :
       if (self.isChanged ()) :
           f = FileObject ("Slice" + str (i))
           f.setParameter ("Key", key)                    
           f.setParameter ("Header", (self._sliceNumber, self._shape))             
           for obj in map ( SinglROIAreaObjDef._getSinglROIAreaObjFileObj, list (self._contourDictionary.values ())) :
               f.addInnerFileObject (obj)
           self._setFileObjectCache (f)
       return self._getFileObjectCache (Name = "Slice" + str (i))
               
   """def getMinMaxXYBounding (self) :       
       first = True
       for contour in self._contourDictionary.values () :
           if (first) :
               minX, minY, maxX, maxY = contour.getMinMaxXYBounding ()
               first = False
           else:       
               tminX, tminY, tmaxX, tmaxY = contour.getMinMaxXYBounding ()
               maxX = max(tmaxX, maxX)
               maxY = max(tmaxY, maxY)
               minX = min(tminX, minX)
               minY = min(tminY, minY)       
       return (minX, minY, maxX, maxY)"""
         
         
   def doesSliceContainHumanEditedContour (self) :
       for contour in self._contourDictionary.values () :           
           if (contour.isHumanEditedContour ()):
               return True
       return False
            
       
   def getContourCoordinates (self, contourid):
       return self._contourDictionary [contourid].getCoordinates ()       
       
   def setContourPerimeter (self, contourid, area) :        
       #self._setCopyRequired () Done
       self._setContourDictionary (contourid, Contour (area = area , ContourToCopy = self._contourDictionary [contourid]))
       self.setInitLineCross ()
       self.setObjectChanged ()       

   def getSliceNumber (self) :
       return self._sliceNumber
   
   def getSliceNumberList (self):
       return [self._sliceNumber]
   
   def getCoordinate(self):
       middleX, middleY, middleCount = 0,0, 0        
       for contour in self._contourDictionary.values () :            
            cx, cy, count = contour.center ()
            #if (contour.getLastDrawDirection () == 1) :
            middleX += cx
            middleY += cy            
            middleCount += count
            #else:
            #    middleX -= cx
            #   middleY -= cy            
            #    middleCount -= count       
       c = Coordinate ()
       if (middleCount == 0) :
           c.setCoordinate ( (0, 0,self._sliceNumber))                  
       else:
           c.setCoordinate ( (int (middleX/middleCount), int (middleY/middleCount) ,self._sliceNumber))                   
       return c
                                             
   """def isContourCrossY (self, testPointY):       
       return np.any (self._getSingleSliceContouredArea ()[:, testPointY])
       
   def isContourCrossX (self, testPointX):       
       return np.any (self._getSingleSliceContouredArea ()[testPointX, :])   """
                                   
   def getContourIDManager (self):
       if (self._MultiROIAreaObjectParent is not None) :
           return self._MultiROIAreaObjectParent.getContourIDManger ()
       return None
       
   def _InitLineCross (self, OptimizedForInit = False):             
       #self._setCopyRequired () Not Required to update, method called when needed to composit contours
       _ContouredArea = None
       if (len(self._contourDictionary) == 1) :
           firstContour = list (self._contourDictionary.values())[0]
           if not firstContour.isContourDefinedByPerimeter () :
               _ContouredArea = firstContour.getContourArea () # contour in contour mask editing modes only one contour should exist an in for any slice              
       if (_ContouredArea is None) :           
           NestManager = ContourNester ()
           _ContouredArea = np.zeros(self._shape, dtype=np.uint8)          
           contourlst = []
           contourMaskList = []
           for contour in self._contourDictionary.values () :
               if (contour.getArea () > 0 and contour.isContourDefinedByPerimeter ()) :
                   contourlst.append (contour)
               else:
                   contourMaskList.append (contour)                             
               
           for contour in contourlst :
               for innerContour in contourlst :
                   if (innerContour != contour) :                   
                       isParent, isSibling = NestManager.testIfParentorSibling (contour, innerContour)
                       if (isParent) :
                           NestManager.addParent (contour, innerContour)                       
                       elif (isSibling) :
                           NestManager.addSibling (contour, innerContour)       
           tempcontourlist = []
           for contour in contourlst :
               NestManager.resetParentCount (contour)                                    
               tempcontourlist.append (contour)               
               #NestManager.printDebug (contour)
           #print ("starting topology")
           idManager = self.getContourIDManager ()      
           while (len (tempcontourlist) > 0):           
               removeLst = []
               #processed = False
               for contour in tempcontourlist :               
                   if NestManager.getParentCount (contour) == 0 :                           
                       #processed = True
                       
                       contourDir = idManager.getContourType (contour.getContourID ())
                       #print (("Init Contour", contour.getContourID (), contourDir, idManager))
                       if (contourDir == "Add"):
                           LastDrawDirection = 1
                       elif (contourDir== "Sub"):
                           LastDrawDirection = -1
                       else:
                           LastDrawDirection = NestManager.getParentLastDrawDirection (contour) * -1
                           #NestManager.printDebug (contour)
                       #print (("last Draw", LastDrawDirection))
                           
                       _ContouredArea = contour.addContourArea (_ContouredArea, LastDrawDirection)
                       if (contour.getLastDrawDirection () !=  LastDrawDirection):                           
                           self._setContourDictionary (contour.getContourID (), Contour (LastDrawDirection = LastDrawDirection, ContourToCopy = contour))                          
                       #print ("Handeling Parent")
                       #NestManager.printDebug (contour)
                       #print ("Handeling Childern")
                       for childern in NestManager.getChildList (contour) :
                           NestManager.decementParentCount (childern)
                           NestManager.setParentLastDrawDirection (childern, LastDrawDirection)
                           #NestManager.printDebug (childern)
                       removeLst.append (contour)
               #if (not processed) :
               #    print("Error detected no childern removed in list")
               #    print ("List contains")
               #    for debug in tempcontourlist :               
               #        NestManager.printDebug (debug)
               for contour in removeLst :               
                   tempcontourlist.remove (contour)
           
           # add contours maps to the volume 
           for contour in contourMaskList :
               _ContouredArea = contour.addContourArea (_ContouredArea, 1)
           #print ("done init line cross")       
              
       if OptimizedForInit :           
           self._XAxisMask.setMask (_ContouredArea, OptimizedForInit = True)
           self._YAxisMask.setMask (_ContouredArea, OptimizedForInit = True)
           return _ContouredArea       
       xIndexs = self._XAxisMask.setMask (_ContouredArea, OptimizedForInit = False)
       yIndexs = self._YAxisMask.setMask (_ContouredArea, OptimizedForInit = False)
       return _ContouredArea, xIndexs, yIndexs
   
   def isInAxisList (self, cord, axis, SharedROIMask = None):              
     try : 
          if (axis == 'Z') :
              result = self._sliceNumber == cord.getZCoordinate ()     
          elif (axis == 'Y') :    
              result = self._YAxisMask.getAxisMask (NoCopy = True)[cord.getYCoordinate ()] > 0      
          else:
              result = self._XAxisMask.getAxisMask (NoCopy = True)[cord.getXCoordinate ()] > 0
          return result
     except:
         return False
   
       
   @staticmethod 
   def _drawContourMarks (qp, contourdist, gapdistance, x1, y1, x2, y2) :
       gapdistance = gapdistance 
       dx = x2 - x1
       dy = y2 - y1
       segmentlength = int (math.sqrt (dx * dx + dy * dy))       
       i = gapdistance - contourdist
       if (i > segmentlength) :
           return segmentlength + contourdist
       while (i <= segmentlength) :
           x = int ((dx * i / segmentlength) + x1)
           y = int ((dy * i / segmentlength) + y1)
           qp.drawRect (x - 1,y -1, 3, 3)         
           i += gapdistance
       return segmentlength - (i - gapdistance)
                                     
   
   
   def getYAxisPixMapROILineCross (self, coordinate) :                
       return self._getSingleSliceContouredArea ()[:, coordinate]         

   def getXAxisPixMapROILineCross (self, coordinate) :                
       return self._getSingleSliceContouredArea ()[coordinate, :]         
   
   def getXYAxisPixMapROILineCross (self, coordinate, Axis) :                
       if (Axis == 'Y') :
           return self.getYAxisPixMapROILineCross (coordinate)  
       else:
           return self.getXAxisPixMapROILineCross (coordinate)           
         
   """@staticmethod
   def drawPerpendicularOverlay (qp, pixmap, niftislicewidget, SliceDrawn, clipx, clipy, clipz, visSettings, drawObj = True, PerpendicularTempMask= None):
       if (not drawObj) :
           return
       sliceAxis = niftislicewidget.getSliceAxis ()
       if sliceAxis == 'Z' :                         
           qp.setOpacity(float (visSettings.getUITransparency ())/float(255))              
           x1, y1 = niftislicewidget.transformPointFromImageToScreen (clipx[0], clipy[0])   
           x2, y2 = niftislicewidget.transformPointFromImageToScreen (clipx[1], clipy[1])    
           xoffset1, yoffset1 = niftislicewidget.transformPointFromImageToScreen (0, 0)
           xoffset2, yoffset2 = niftislicewidget.transformPointFromImageToScreen (1, 1)
           xoffset = xoffset2 - xoffset1
           yoffset = yoffset2 - yoffset1
           xoffset = max (0, xoffset - 1)
           yoffset = max (0, yoffset - 1)
           x2 += xoffset
           y2 += yoffset                 
           qp.setClipping (True)
           qp.setClipRect (x1, y1, x2 - x1+1, y2 - y1+1)           
           if (SliceDrawn >= clipz[0] and SliceDrawn <= clipz[1]):  
              niftislicewidget.drawOverlayQTPixmap (qp, pixmap, perpendicularpixmap = PerpendicularTempMask)                 
           qp.setClipping (False)
           qp.setOpacity(1)"""               
   
   def hasPerimeterContours (self) :
       contourCount = len (self._contourDictionary)               
       if contourCount == 1 :
           firstContour = list (self._contourDictionary.values())[0]
           return firstContour.isContourDefinedByPerimeter () 
       elif contourCount > 2 :
           return True                      
       return False       
   
   @staticmethod
   def drawZSlice (AreaQTPixmapMask, qp, niftislicewidget, clipx, clipy, clipz, visSettings) :
       if visSettings is not None :
           qp.setOpacity(float (visSettings.getUITransparency ())/float(255))
       if clipx is not None and clipy is not None :           
           x1, y1 = niftislicewidget.transformPointFromImageToScreen (clipx[0], clipy[0])   
           x2, y2 = niftislicewidget.transformPointFromImageToScreen (clipx[1], clipy[1])
           xoffset1, yoffset1 = niftislicewidget.transformPointFromImageToScreen (0, 0)
           xoffset2, yoffset2 = niftislicewidget.transformPointFromImageToScreen (1, 1)
           xoffset = xoffset2 - xoffset1
           yoffset = yoffset2 - yoffset1
           xoffset = max (0, xoffset - 1)
           yoffset = max (0, yoffset - 1)
           x2 += xoffset
           y2 += yoffset
           qp.setClipping (True)
           qp.setClipRect (x1, y1, x2 - x1+1, y2 - y1+1)
       niftislicewidget.drawOverlayQTPixmap (qp, AreaQTPixmapMask)   
       qp.setClipping (False)
       qp.setOpacity(1)    
               
    
   def draw(self, AreaQTPixmapMask, tempPixMapMaskSlice, name, qp, niftislicewidget, ROIDefs, SliceDrawn, clipx, clipy, clipz, selectedContours = [], previousSliceIndex = None, visSettings = None, drawObj = True):               
       if (not drawObj) :
           return
       sliceAxis = niftislicewidget.getSliceAxis ()
       if sliceAxis == 'Z' :          
        
           if (SliceDrawn == self._sliceNumber or tempPixMapMaskSlice is not None) : #if current slice is selected or temp mask draw buffer has data in it
               if (tempPixMapMaskSlice is not None) :           
                  SinglROIAreaObjDef.drawZSlice (tempPixMapMaskSlice, qp, niftislicewidget, clipx, clipy, clipz, visSettings)    
               elif (SliceDrawn >= clipz[0] and SliceDrawn <= clipz[1]):           
                  SinglROIAreaObjDef.drawZSlice (AreaQTPixmapMask, qp, niftislicewidget, clipx, clipy, clipz, visSettings)    
                                                                             
               if self.hasPerimeterContours () :
                   color = ROIDefs.getROIColor (name)
                   solidLinePen = QtGui.QPen(color, 0, QtCore.Qt.SolidLine)                  
                   dashedLinePen = QtGui.QPen(color, 0, QtCore.Qt.DashLine)                                 
                   solidLinePenDarker = QtGui.QPen(color.darker (200), 0, QtCore.Qt.SolidLine)                  
                   dashedLinePenDarker = QtGui.QPen(color.darker (200), 0, QtCore.Qt.DashLine)                  
    
                   activePen = solidLinePen
                   activePenDarker = solidLinePenDarker
                   qp.setPen (activePen)                        
                   qp.setBrush (QtGui.QColor (0,0,0,0))                        
                   for contour in self._contourDictionary.values() :               
                       if (contour.isContourDefinedByPerimeter ()) :
                           cord = contour.getCoordinates ()                      
                           
                           if (visSettings.getUIDashIndicator ()):
                               if (contour.getContourID () in selectedContours ):                           
                                   activePen = dashedLinePen
                                   activePenDarker = dashedLinePenDarker
                               else:
                                   activePen = solidLinePen                       
                                   activePenDarker = solidLinePenDarker
                               
                           if (contour.getLastDrawDirection () == 1) :                      
                               strokePen = activePen
                           else:
                               strokePen = activePenDarker                 
                                   
                           if (cord.shape[1] >= 2) :                   
                               x,y = (cord[0,0], cord[1,0])                       
                               x, y = niftislicewidget.transformPointFromImageToScreen (x, y)                       
                               i = 1
                               path = QtGui.QPainterPath()
                               path.moveTo(x, y);                    
                               while (i < cord.shape[1]):
                                   #oldX = x
                                   #oldY = y
                                   x, y = (cord[0,i], cord[1,i])
                                   x, y = niftislicewidget.transformPointFromImageToScreen (x, y)                       
                                   #qp.drawLine (oldX, oldY, x, y)
                                   path.lineTo (x,y)
                                   i += 1                       
                               qp.strokePath(path, strokePen)

                   qp.setPen (solidLinePen)                                                      
                   if (visSettings.getUIBoxIndicator ()) :                        
                       for contour in self._contourDictionary.values() :
                           if (contour.isContourDefinedByPerimeter ()) :                                       
                               if (contour.getLastDrawDirection () == 1) :                      
                                   setColor = color                       
                               else:
                                   setColor = color.darker (200)                       
                               qp.setPen (setColor)           
                               qp.setBrush (setColor)        
                               if (contour.getContourID () in selectedContours ):
                                   cord = contour.getCoordinates ()                              
                                   x,y = (cord[0,0], cord[1,0])
                                   x, y = niftislicewidget.transformPointFromImageToScreen (x, y)                       
                                   i = 1
                                   contourdist = 0
                                   while (i < cord.shape[1]):
                                       oldX = x
                                       oldY = y
                                       x, y = (cord[0,i], cord[1,i])
                                       x, y = niftislicewidget.transformPointFromImageToScreen (x, y)                       
                                       
                                       contourdist = SinglROIAreaObjDef._drawContourMarks (qp, contourdist, 15, oldX, oldY, x, y)                           
                                       i += 1  
               

                   

class BoundingBoxControlPoint :
    def __init__ (self, xi, yi, xbox, ybox) :
        self._ControlPointSize = 10        
        self._xi = xi
        self._yi = yi        
        if (xi == 0 or xi == 1) :        
            self._x = xbox[xi]
        else:
            self._x = int (xbox[0] + xbox[1]) / 2
        
        if (yi == 0 or yi == 1) :        
            self._y = ybox[yi]
        else:
            self._y = int (ybox[0] + ybox[1]) / 2       
           
    
    def getPointSize (self) :
        return self._ControlPointSize
        
    def draw (self, qp) : 
       controlPointSize = self._ControlPointSize
       halfSize = int (controlPointSize / 2)
       qp.setPen ((QtGui.QColor (255,255,255)))
       qp.setBrush (QtGui.QColor (178,178,178))
       qp.drawRect (self._x - halfSize, self._y - halfSize, controlPointSize, controlPointSize)
         
    def drawSelectd (self, qp) : 
       controlPointSize = self._ControlPointSize
       halfSize = int (controlPointSize / 2)
       qp.setPen ((QtGui.QColor (255,255,255)))
       qp.setBrush (QtGui.QColor (116,170,215))
       qp.drawRect (self._x - halfSize, self._y - halfSize, controlPointSize, controlPointSize)  

    def isSelected (self, niftslicewidget):
                
        controlPointSize = int (self._ControlPointSize)
        halfSize = int (controlPointSize / 2)
        testX, testY = self._x, self._y
        testX -= halfSize
        testY -= halfSize        
        mX, mY = niftslicewidget.getLastSystemMouseCoordinate ()
        return (testX <= mX and testX + controlPointSize >= mX and testY <= mY and testY + controlPointSize >= mY)
            
    def getPosition (self):
        return [self._x , self._y]
        
    def getBoundingBoxIndex (self) :
        return (self._xi, self._yi)


class NativeSetMask :
    @staticmethod  #@jit(cache=True) 
    def _setSolidColorMask (maskRGBA, Blue, Green, Red, mask) :
        maskRGBA[0,:,:] = Blue  #B
        maskRGBA[1,:,:] = Green   #G
        maskRGBA[2,:,:] = Red  #R
        maskRGBA[3,:,:] = 255 * mask    # Alpha  255 == on
        
    @staticmethod   #@jit(cache=True) 
    def _setMask (maskRGBA, Blue, Green, Red, mask) :
        maskRGBA[0,:,:] = Blue * mask   #B
        maskRGBA[1,:,:] = Green * mask  #G
        maskRGBA[2,:,:] = Red * mask  #R
        maskRGBA[3,:,:] = 255 * mask    # Alpha  255 == on
                
class SingleSliceDefQtSliceMemCache :
    def __init__ (self) :
        self._QTMaskMemCache = None                
        self._sliceNumber = None
        self._sliceAxis = None
        self._AreaQTPixmap = None               
        self._QTClipMaskDrawSettings = ()  #testsettings = (color.rgba())     
        self._temporarySliceAxis = None
        self._temporarySliceNumber = None
        self._temporaryQTClipMaskDrawSettings = ()
        self._temporaryObjectPixMapMask = None        
        self._temporaryObjectMask = None        
        self._AxisProjectionState = None
        self._tempAxisProjectionState  = None
    
    def __getstate__(self):
        # Copy the object's state from self.__dict__ which contains
        # all our instance attributes. Always use the dict.copy()
        # method to avoid modifying the original state.
        state = self.__dict__.copy()
        # Remove the unpicklable entries.
        state['_QTMaskMemCache'] = None
        state['_sliceNumber'] = None        
        state['_sliceAxis'] = None        
        state['_AreaQTPixmap'] = None
        state['_QTClipMaskDrawSettings'] = ()        
        state['_AxisProjectionState'] = None
        state['_temporarySliceAxis'] = None
        state['_temporarySliceNumber'] = None
        state['_temporaryQTClipMaskDrawSettings'] = ()        
        state['_temporaryObjectPixMapMask'] = None
        state['_temporaryObjectMask'] = None  
        state['_tempAxisProjectionState'] = None
        return state
    
    def copy (self) :
        return SingleSliceDefQtSliceMemCache ()
         
    def clearSliceCache (self) :
         self._sliceNumber = None       
         self._sliceAxis = None
         self._AreaQTPixmap = None
         self._QTClipMaskDrawSettings = ()
    
    def ifSelectedClearSliceCache (self, sliceNumber) :
        if (self._sliceNumber == sliceNumber or self._temporarySliceAxis in ['X','Y']) :            
            self.clearSliceCache ()
    
    
    def _getQPixmap (self, mask, color) :
      width, height = mask.shape
      if (self._QTMaskMemCache is None or self._QTMaskMemCache.shape != (4, width, height)) :      
         self._QTMaskMemCache = np.zeros ((4, width, height), dtype = np.uint8)                                           
      maskRGBA = self._QTMaskMemCache
      NativeSetMask._setMask (maskRGBA, color.blue (), color.green (), color.red (), mask)
      """maskRGBA[0,:,:] = color.blue () * mask   #B
      maskRGBA[1,:,:] = color.green () * mask  #G
      maskRGBA[2,:,:] = color.red () * mask  #R
      maskRGBA[3,:,:] = 255 * mask    # Alpha  255 == on"""
      image =  QtGui.QImage (maskRGBA.tobytes(order = 'F'), width, height, width * 4, QtGui.QImage.Format_ARGB32)                        
      return QtGui.QPixmap.fromImage (image)       
  
    def setTempQtMask (self, sliceView, mask, NoCopy = False, ClipBox = None):                
        self._temporarySliceNumber = sliceView.getSliceNumber()
        self._temporarySliceAxis = sliceView.getSliceAxis()
        if (NoCopy) :
            self._temporaryObjectMask = mask
            if ClipBox is not None and self._temporaryObjectPixMapMask is not None and self._temporaryQTClipMaskDrawSettings is not None and len (self._temporaryQTClipMaskDrawSettings) > 0:
                xmin, xmax, ymin, ymax = ClipBox
                if xmin is not None :
                    xmin = max( 0, min (xmin, mask.shape[0] - 1))
                    xmax = max( 0, min (xmax + 1, mask.shape[0] - 1))
                    ymin = max( 0, min (ymin, mask.shape[1] - 1))
                    ymax = max( 0, min (ymax + 1, mask.shape[1] - 1))
                    clippedMask = mask[xmin:xmax,ymin:ymax] 
                    color = self._temporaryQTClipMaskDrawSettings[0]
                    colorB = 0xFF & color
                    colorG = 0xFF & (color >> 8)
                    colorR = 0xFF & (color >> 16)
                    mem = np.zeros ((4, clippedMask.shape[0], clippedMask.shape[1]), dtype = np.uint8)
                    NativeSetMask._setMask (mem, colorB, colorG, colorR, clippedMask)
                    """mem[0,:,:] = colorB * clippedMask   #B
                    mem[1,:,:] = colorG * clippedMask  #G
                    mem[2,:,:] = colorR * clippedMask  #R
                    mem[3,:,:] = 255 * clippedMask    # Alpha  255 == on"""
                    image =  QtGui.QImage (mem.tobytes(order = 'F'), clippedMask.shape[0], clippedMask.shape[1], clippedMask.shape[0] * 4, QtGui.QImage.Format_ARGB32)
                    qp = QtGui.QPainter (self._temporaryObjectPixMapMask)            
                    qp.setCompositionMode (QtGui.QPainter.CompositionMode_Source)                                     
                    qp.drawImage (xmin,ymin,image)
                    qp.end ()
                    return
                    
        else:
            self._temporaryObjectMask = np.copy (mask)
        self._temporaryQTClipMaskDrawSettings = ()
        self._temporaryObjectPixMapMask = None
    
    def hasTempMask (self, sliceView) :
        if not sliceView.areAxisProjectionControlsEnabledForAxis () :
            tempProjection= None
        else:
            tempProjection = sliceView.getAxisProjectionControls ().getProjectionState ()
        return (self._temporaryObjectMask is not None and sliceView.getSliceNumber() == self._temporarySliceNumber and sliceView.getSliceAxis() == self._temporarySliceAxis and self._tempAxisProjectionState == tempProjection and self._temporaryObjectMask.shape == tuple (sliceView.getSliceShape()))
        
    def getTempMask (self, sliceView, alternativeMask = None, NoCopy = False) :        
        sliceNumber = sliceView.getSliceNumber()
        sliceAxis = sliceView.getSliceAxis()
        if not sliceView.areAxisProjectionControlsEnabledForAxis () :
            tempProjection= None
        else:
            tempProjection = sliceView.getAxisProjectionControls ().getProjectionState ()
        if (self._temporaryObjectMask is not None and sliceNumber == self._temporarySliceNumber and sliceAxis == self._temporarySliceAxis and self._temporaryObjectMask.shape == tuple (sliceView.getSliceShape()) and self._tempAxisProjectionState == tempProjection) :
            if NoCopy : 
                return self._temporaryObjectMask
            else:
                return np.copy(self._temporaryObjectMask)
        self._temporarySliceNumber = sliceNumber
        self._temporarySliceAxis = sliceAxis
        if alternativeMask is not None :
            self._temporaryObjectMask = np.copy (alternativeMask)
        if self._temporaryObjectMask is None :
            self._temporaryObjectMask = np.zeros (sliceView.getSliceShape(),dtype=np.uint8)    
        self._tempAxisProjectionState = tempProjection
        return self._temporaryObjectMask
        
    
    def clearTempMask (self) :
        self._temporarySliceNumber = None
        self._temporarySliceAxis = None
        self._temporaryObjectMask = None
        self._temporaryQTClipMaskDrawSettings = ()
        self._temporaryObjectPixMapMask = None
        self._tempAxisProjectionState = None
        
    def getTempQtMaskPixMap (self, sliceView, color) :
        """if  self._temporarySliceNumber != sliceNumber :
            return None"""
        if (self._temporaryObjectMask is None or self._temporarySliceAxis != sliceView.getSliceAxis()) :
            return None                
        isAxisProjection = sliceView.areAxisProjectionControlsEnabledForAxis () 
        if not isAxisProjection :
           AxisProjectionState = None
        else:
           axisProjectionControls = sliceView.getAxisProjectionControls()
           AxisProjectionState = axisProjectionControls.getProjectionState ()
        testsettings = (color.rgba(), AxisProjectionState)     
        if  self._temporaryObjectPixMapMask is None or self._temporaryQTClipMaskDrawSettings != testsettings :
            self._temporaryQTClipMaskDrawSettings = testsettings
            self._temporaryObjectPixMapMask = self._getQPixmap (self._temporaryObjectMask, color)
        return self._temporaryObjectPixMapMask
    
    """def getPerpendicularTempMask (self, sliceView) :            
        if (self._temporaryObjectMask is None) :
            return (None, None, None)
        if  self._temporarySliceAxis == sliceView.getSliceAxis() :
            return (None, None, None)
        return (self._temporarySliceAxis, self._temporarySliceNumber, self._temporaryObjectPixMapMask)
    
    def hasPerpendicularTempMask (self, sliceView) :
        axis, sliceNumber, pixmap = self.getPerpendicularTempMask (sliceView)
        return axis is not None and pixmap is not None and sliceNumber is not None"""
    
  
        
        
        
    def getQtMaskPixMap (self, sliceView, color, singleSliceDef, SliceDictionary = None,   ROIObj = None ) :       
       testsettings = (color.rgba())            
       sliceNumber = sliceView.getSliceNumber()
       sliceAxis = sliceView.getSliceAxis()
       
       isAxisProjection = sliceView.areAxisProjectionControlsEnabledForAxis () 
       if not isAxisProjection :
           AxisProjectionState = None
       else:
           axisProjectionControls = sliceView.getAxisProjectionControls()
           selectedCoord = sliceView.getCoordinate ()
           AxisProjectionState = [axisProjectionControls.getProjectionState ()]
           if axisProjectionControls.isYAxisRotated () :
               AxisProjectionState += ["Y", selectedCoord.getXCoordinate ()]
           if axisProjectionControls.isXAxisRotated () :
               AxisProjectionState += ["X", selectedCoord.getYCoordinate ()]
           if ROIObj is not None :
               AxisProjectionState = tuple (AxisProjectionState + ROIObj._getBoundingBoxLst ())
           else:
               AxisProjectionState = tuple (AxisProjectionState )
       
       if (self._AreaQTPixmap == None or self._sliceNumber != sliceNumber or sliceAxis != self._sliceAxis or testsettings != self._QTClipMaskDrawSettings or (singleSliceDef is not None and singleSliceDef.isInitLineCrossSet ()) or AxisProjectionState != self._AxisProjectionState):                                         
           self._QTClipMaskDrawSettings = testsettings
           self._sliceNumber = sliceNumber                  
           self._sliceAxis = sliceAxis
           self._AxisProjectionState = AxisProjectionState
           
           if SliceDictionary is not None and isAxisProjection and sliceView.isNiftiDataLoaded ():
               
               def GetVolumeFunc (firstSlice,LastSlice) :
                   volumeCache = ROIObj._getSliceMaskVolumeCache ()
                   return volumeCache.getROISubVolume (firstSlice,LastSlice, volumeCache.getSharedMemCache ())
               
               imageData, _  = axisProjectionControls.transformROIDataFromVolumeToProjection (sliceView.getNIfTIVolume (), sliceView.getAxisProjectionSliceNumber (), GetVolumeFunc = GetVolumeFunc, ROIIDNumber = ROIObj.getROIDefIDNumber (), ClipROIObjectList = [ROIObj])
        
           elif singleSliceDef is None :
                imageData = np.zeros (sliceView.getSliceShape (), dtype=np.uint8)
           else:
                imageData = singleSliceDef._getSingleSliceContouredArea ()
           self._AreaQTPixmap = self._getQPixmap   (imageData, color)                                                                             
       return self._AreaQTPixmap
   

   
class VolumeMaskCache : 
    def __init__ (self, ROIDefIDNumber) :
        self.clearVolume ()
        self.shape = None      
        self._ObjUID = ROIDefIDNumber        
        self._RunningInLowMemoryMode = False
        self._Parent = None
        
    def changeROIOID (self, oldIDNum, newIdNum):
        if (self._ObjUID == oldIDNum):
            self._ObjUID = newIdNum
        if self._sharedMemCache is not None :
            self._sharedMemCache.changeROIOID (oldIDNum, newIdNum)
    
    def getSharedMemCache (self) :
        return self._sharedMemCache
       
    def clearVolume (self) :
        self._RunningInLowMemoryMode = False
        self._Parent = None        
        self._ObjUID = None
        self._volume = None
        self.shape = None
        self._ZSliceCacheChanged = False
        self._ZSliceCache = None
        self._ZSliceCacheIndex = None  
        self._XSliceCache = None
        self._XSliceCacheIndex = None
        self._YSliceCache = None
        self._YSliceCacheIndex = None
        self._TempXUnPackBuffer = None 
        self._TempYUnPackBuffer = None 
        self._TempZUnPackBuffer = None 
        self._sharedMemCache = None    
        #self._sliceInitalizationSet = set ()
    
    def setLowMemoryMode (self) :
        self._RunningInLowMemoryMode = True
        if self._volume is not None :
            self._volume = None
            self._ZSliceCacheChanged = False
            self._ZSliceCache = None
            self._ZSliceCacheIndex = None  
            self._XSliceCache = None
            self._XSliceCacheIndex = None
            self._YSliceCache = None
            self._YSliceCacheIndex = None
            self._TempXUnPackBuffer = None 
            self._TempYUnPackBuffer = None 
            self._TempZUnPackBuffer = None 
            self._sharedMemCache = None    
            #self._sliceInitalizationSet = set ()
    
    def getROISubVolume  (self, FirstSlice, LastSlice, SharedMemCache = None): 
        try:
            if (SharedMemCache is not None) :
                vol = SharedMemCache.getROISubVolume (self._ObjUID, FirstSlice, LastSlice)
                if vol is not None :
                    return vol  
            subVolShape = list (self.shape)
            subVolShape[-1] = LastSlice + 1 - FirstSlice
            mem = np.zeros (subVolShape, dtype=np.uint8)    
            sliceSet = set (self._Parent.getSortedKeyList ())
            for index, sliceI in enumerate (range (FirstSlice, LastSlice + 1)) :
                if sliceI in sliceSet :
                    mem[:,:,index] = self.getSlice (sliceI) 
            return mem
        except:
            return None 
         
    def getROIVolume (self, SharedMemCache = None):
        if (SharedMemCache is not None) :
            vol = SharedMemCache.getROIVolume (self._ObjUID)
            if vol is not None :
                return vol        
        if self._RunningInLowMemoryMode :         
            mem = np.zeros (self.shape, dtype=np.uint8)
            for sliceIndex in self._Parent.getSortedKeyList () :
                mem[:,:,sliceIndex ] = self.getSlice (sliceIndex)
            return mem        
        else:
            mem = np.unpackbits ( self._volume, axis = 2)
            mem = mem[:,:,0:self.shape[2]]
            return mem 
        """mem = np.zeros (self.shape, dtype=np.uint8)
        sliceList = None 
        if self._Parent is not None :
            try :
                sliceList = self._Parent.getSortedKeyList ()
            except:
                sliceList = None 
        if sliceList is None :
            sliceList = range (vol.shape[2])
        for index in sliceList :
            try :
                mem[:,:,index] = self.getSlice (index)
            except:
                pass
        return mem"""
        
    @staticmethod
    def initalizeVolume (shape, SharedMemCache = None, obj = None, RunningInLowMemoryMode = False) :
        vol = VolumeMaskCache (obj.getROIDefIDNumber ())
        vol.shape = (shape[0],shape[1], shape[2])    
        vol._RunningInLowMemoryMode = RunningInLowMemoryMode        
        vol._Parent = obj
        if not vol._RunningInLowMemoryMode :
            #vol._sliceInitalizationSet = set(list (range (shape[2])))
            if (SharedMemCache is not None) :
                SharedMemCache.initalizeVolume (vol._ObjUID, shape)
                vol._sharedMemCache  = SharedMemCache
            else:
                vol._volume = np.zeros ((shape[0],shape[1], math.ceil(shape[2]/8.0)), dtype=np.uint8)        
                vol._TempXUnPackBuffer = np.zeros ((1,shape[1], vol._volume.shape[2]), dtype=np.uint8)
                vol._TempYUnPackBuffer = np.zeros ((shape[0],1, vol._volume.shape[2]), dtype=np.uint8)
                vol._TempZUnPackBuffer = np.zeros ((shape[0],shape[1], 1), dtype=np.uint8)
        return vol
    
    def zeroVolume (self) :
        self._ZSliceCache = None
        self._ZSliceCacheIndex = None  
        self._XSliceCache = None
        self._XSliceCacheIndex = None
        self._YSliceCache = None
        self._YSliceCacheIndex = None
        if self._RunningInLowMemoryMode : 
            self._volume = None
            self._ZSliceCacheChanged = False
            #self._sliceInitalizationSet = set()
        else:
            #self._sliceInitalizationSet = set(list (range (self.shape[2])))
            if (self._sharedMemCache is not None and self._sharedMemCache.isInitalized ()) :
                self._sharedMemCache.zeroVolume (self._ObjUID)
            else:
                self._volume[:,:,:] = 0                        
                self._ZSliceCacheChanged = False
                
    def isInitalized (self) :
        return (self._sharedMemCache is not None and self._sharedMemCache.isInitalized ()) or self._volume is not None or self._RunningInLowMemoryMode

    def _updateZSliceCache (self) :
        if (self._ZSliceCacheChanged) :
            self._volume[:,:,  self._ZSliceCacheIndex] = np.packbits (self._ZSliceCache, axis = 2)[:,:,0]
            self._ZSliceCacheChanged = False
            self._XSliceCache = None
            self._XSliceCacheIndex = None 
            self._YSliceCache = None
            self._YSliceCacheIndex = None
            return True
        return False
    
    def _getZSliceCache (self, hindex) :        
         if hindex != self._ZSliceCacheIndex :
            self._updateZSliceCache ()
            self._TempZUnPackBuffer[:,:,0] = self._volume[:,:, hindex]
            self._ZSliceCache = np.unpackbits ( self._TempZUnPackBuffer, axis = 2)
            self._ZSliceCacheIndex = hindex
            self._ZSliceCacheChanged = False            
    
    def getSlice (self, height) :
        if self._RunningInLowMemoryMode : 
            sliceshape = (self.shape [0], self.shape [1])
            return self._Parent.getSliceMap  (height, sliceshape, NonPerimeterMasksOnly = False, ClipOverlayedObjects = False)           
        
        """if height in self._sliceInitalizationSet :
            slicedata = self._Parent.getSliceMap  (height, sliceshape, NonPerimeterMasksOnly = False, ClipOverlayedObjects = False)
            self.setSlice (height, slicedata)"""
           
        if (self._sharedMemCache is not None and self._sharedMemCache.isInitalized ()) :
            if self._ZSliceCacheIndex != height :
                self._ZSliceCacheIndex = height
                self._ZSliceCache = self._sharedMemCache.getSlice (self._ObjUID, height)               
            return  self._ZSliceCache
        hindex = int (height / 8)
        self._getZSliceCache (hindex)
        cacheIndex = height % 8
        return np.copy (self._ZSliceCache[:,:,cacheIndex])
    
    def zeroSlice (self, height) :
        if not self._RunningInLowMemoryMode : 
            #self._sliceInitalizationSet.add (height)
            if (self._sharedMemCache is not None and self._sharedMemCache.isInitalized ()) :                        
                if self._ZSliceCacheIndex == height :
                    self._ZSliceCacheIndex = None                
                self._XSliceCacheIndex = None 
                self._YSliceCacheIndex = None
                return self._sharedMemCache.zeroSlice (self._ObjUID, height)            
            hindex = int (height / 8)
            self._getZSliceCache (hindex)
            cacheIndex = height % 8
            self._ZSliceCache[:,:,cacheIndex] = 0
            self._ZSliceCacheChanged = True
        
    def setSlice (self, height, data) :
        if not self._RunningInLowMemoryMode : 
            """if height in self._sliceInitalizationSet :
                 self._sliceInitalizationSet.remove (height)"""
            if (self._sharedMemCache is not None and self._sharedMemCache.isInitalized ()) :            
                if self._ZSliceCacheIndex == height :
                    self._ZSliceCacheIndex = None
                self._XSliceCacheIndex = None 
                self._YSliceCacheIndex = None
                """if len (self._sliceInitalizationSet) == 0 :
                    self._sharedMemCache.removeObjectsWithMissingData (self._ObjUID)"""
                return self._sharedMemCache.setSlice (self._ObjUID, height, data)
            hindex = int (height / 8)
            self._getZSliceCache (hindex)
            cacheIndex = height % 8
            self._ZSliceCache[:,:,cacheIndex]  = data
            self._ZSliceCacheChanged = True
    
    def _setSliceFast (self, height, data) :
        if not self._RunningInLowMemoryMode : 
            if (self._sharedMemCache is not None and self._sharedMemCache.isInitalized ()) :            
                return self._sharedMemCache.setSlice (self._ObjUID, height, data)
            hindex = int (height / 8)
            self._getZSliceCache (hindex)
            cacheIndex = height % 8
            self._ZSliceCache[:,:,cacheIndex]  = data
            self._ZSliceCacheChanged = True

    
    def initializeSlice (self, height, data) :
        if not self._RunningInLowMemoryMode : 
            """if height in self._sliceInitalizationSet :
                 self._sliceInitalizationSet.remove (height)
                 if len (self._sliceInitalizationSet) == 0 :
                    self._sharedMemCache.removeObjectsWithMissingData (self._ObjUID)"""
            if (self._sharedMemCache is not None and self._sharedMemCache.isInitalized ()) :
                self._ZSliceCacheIndex = None            
                self._XSliceCacheIndex = None 
                self._YSliceCacheIndex = None
                return self._sharedMemCache.initializeSlice (self._ObjUID, height, data)
            self.setSlice (height, data)
    
    """def initalizeSlicesWithMissingData (self) :
        if not self._RunningInLowMemoryMode and len (self._sliceInitalizationSet) > 0 : 
                sliceshape = (self.shape [0], self.shape [1])
                listcopy = list (self._sliceInitalizationSet)
                for height in listcopy :
                   slicedata = self._Parent.getSliceMap  (height, sliceshape, NonPerimeterMasksOnly = False, ClipOverlayedObjects = False)
                   self._setSliceFast (height, slicedata)
                if (self._sharedMemCache is not None and self._sharedMemCache.isInitalized ()) :            
                    self._ZSliceCacheIndex = None
                    self._XSliceCacheIndex = None 
                    self._YSliceCacheIndex = None
                    self._sharedMemCache.removeObjectsWithMissingData (self._ObjUID)
                self._sliceInitalizationSet = set ()"""
            
    
    def isSliceValidInVolumeCache (self, height) :
        if self._RunningInLowMemoryMode : #or height in self._sliceInitalizationSet :
            return False
        return height >= 0 and height < self.shape[2]
                
    def getYAxisSlice (self, yIndex) :
       if self._RunningInLowMemoryMode : 
           return None
       else: 
           #self.initalizeSlicesWithMissingData ()
           if (self._sharedMemCache is not None and self._sharedMemCache.isInitalized ()) :
               if self._YSliceCacheIndex != yIndex :                
                   self._YSliceCacheIndex = yIndex
                   self._YSliceCache = self._sharedMemCache.getYAxisSlice (self._ObjUID, yIndex)                          
               return self._YSliceCache
           
           if (self._updateZSliceCache () or self._YSliceCacheIndex !=  yIndex) :
               self._YSliceCacheIndex = yIndex
               self._TempYUnPackBuffer[:,0,:] = self._volume[:,yIndex, :]
               self._YSliceCache = np.unpackbits (self._TempYUnPackBuffer, axis = 2)
               self._YSliceCache = self._YSliceCache[:,0,:self.shape[2]]
           return np.copy (self._YSliceCache)
        
    def getXAxisSlice (self, xIndex) :
        if self._RunningInLowMemoryMode : 
            return None
        else:
            #self.initalizeSlicesWithMissingData ()
            if (self._sharedMemCache is not None and self._sharedMemCache.isInitalized ()) :            
                if self._XSliceCacheIndex != xIndex :                
                    self._XSliceCacheIndex = xIndex
                    self._XSliceCache = self._sharedMemCache.getXAxisSlice (self._ObjUID, xIndex)            
                return self._XSliceCache
            
            if (self._updateZSliceCache () or self._XSliceCacheIndex !=  xIndex) :
               self._XSliceCacheIndex = xIndex
               self._TempXUnPackBuffer[0,:,:] = self._volume[xIndex,:, :]
               self._XSliceCache = np.unpackbits (self._TempXUnPackBuffer, axis = 2)
               self._XSliceCache = self._XSliceCache[0,:,:self.shape[2]]
            return np.copy (self._XSliceCache)
        
    





class MultiROIAreaObject :
   def __init__ (self, sliceNumber = None, XYArea = None, shape = None, ContourType = None, finterface = None, VolumeShape = None, QApp = None, SliceMaskVolumeCache = None):           
       self._crossSliceChangeSliceNumberList = []
       self._DeepGrowAnnotation = DeepGrowAnnotation (self)
       self._DeepGrowAnnotation.addDeepGrowSliceChangeListener (self._DeepGrowObjectChanged)
       
       
       self._ROIUIDManager = ROIUIDManager ()
       self._singleSliceDefQtSliceMemCache = SingleSliceDefQtSliceMemCache ()
       self._objectHasSlicesWithAllocatedContourMemoryThatCanOptionallyCleared = False
       self.resetCompressionWaitIndicator ()
       self.resetObjectUndoChanged ()
       self._singeSliceInitLineCross = False
       
       #self._sliceMaskVolumeCache = None
       if SliceMaskVolumeCache is None :
           SliceMaskVolumeCache = VolumeMaskCache (None)
       self._sliceMaskVolumeCache = SliceMaskVolumeCache
       
       self._volumeShape = VolumeShape
       self._XAxisMask = AxisIndex (0, None)
       self._YAxisMask = AxisIndex (1, None)   
        
       self.setObjectChanged ()       
       self._objClipDictionary = None 
       self._SaveCurrentBoundingBox  = None
       self._getQTMaskMem = None
       self._cachedAxisMask = None
       self._RegistrationLog = ANTSRegistrationLog (self)                     
       self._userChangeLog = ()
       self._contourIDManger = ContourIDManger (self)
       self._isObjMoving = False
       self._sliceNumber = sliceNumber
       self._selectedContourIDList = []
       self._ROIAreaObjectSliceDictionary = {}
       self._clearSortedKeyList ()
       self._selected = False
       self._ZBoundingBox = np.array ([0, 0])   
       self._XBoundingBox = np.array ([-1, 0])   
       self._YBoundingBox = np.array ([-1, 0])   
       self._SelectedBoundingBoxControlPoint = None              
       self._YAxisROIDrawSettings = ()
       self._XAxisROIDrawSettings = ()
       self._YAxisROIPixMap = None
       self._XAxisROIPixMap = None       
       self._ROIDefIDNumber = None
       if (sliceNumber is not None) :       
           contourID = self._contourIDManger.allocateID ()
           self._selectedContourIDList = [contourID] 
           contour = Contour ( contourID, XYArea, shape, ContourType)                                          
           self._ROIAreaObjectSliceDictionary = {sliceNumber : SinglROIAreaObjDef (sliceNumber, contour , shape, MultiROIAreaObjectParent = self)}   
           self._clearSortedKeyList ()
           #Bounding box will subsquently be adjustd through call to adjustBoundBoxToFitSlice
           self._ZBoundingBox = np.array ([sliceNumber - 5, sliceNumber + 5])           
           minx = XYArea[0][0] 
           miny = XYArea[0][1]
           maxx = minx
           maxy = miny
           self._XBoundingBox = np.array([minx-1, maxx+1])
           self._YBoundingBox = np.array([miny-1, maxy+1])
    
       elif (finterface is not None) :
            self._RegistrationLog.loadFromFile (FileHeader = finterface)
            roilist = {}
            self._contourIDManger.load (finterface)
            self._selectedContourIDList = []            
            if (finterface.hasFileObject ("DeepGrowAnnotations")):
                self._DeepGrowAnnotation.initFromFileObj (finterface.getFileObject ("DeepGrowAnnotations"))
            
            _XBoundingBox1, _XBoundingBox2, _YBoundingBox1, _YBoundingBox2, _ZBoundingBox1, _ZBoundingBox2 =  finterface.getParameter ("BoundingBox")            
            self._sliceNumber = finterface.getParameter ("SliceNumber") 
            count = finterface.getParameter ("SliceCount")         
            if (finterface.hasParameter ("UserChangeLog")):
                self._userChangeLog = finterface.getParameter ("UserChangeLog")  
            if (finterface.hasParameter ("ROIDefUIDObj")):
                self._ROIUIDManager.initFromTuple (finterface.getParameter ("ROIDefUIDObj"))   
            index = None
            try : #Code to make slice reading more robust.  Allows for errors in slice data.  Only slices with errors will not be read. 
                startTimeObj = time.time ()                                
                for sliceNum in range (count) : 
                    contourFileinterface = finterface.getFileObject ("Slice"+ str (sliceNum + 1))         
                    index = contourFileinterface.getParameter ("Key")  
                    singleSliceDef = None
                    try :                        
                        singleSliceDef = SinglROIAreaObjDef (finterface = contourFileinterface,  MultiROIAreaObjectParent = self)                                                                             
                    except:
                        singleSliceDef = None
                        print ("A error occured reading slice: %d" % index)       
                    if (singleSliceDef is not None) :
                        roilist[index] = singleSliceDef
                    if (QApp is not None) :
                        QApp.processEvents ()
                print ("Object Load Time: " + str (time.time () - startTimeObj))
            except:
                roilist = {}
                for keyName in finterface.getFileObjectKeyList () : 
                    contourFileinterface = finterface.getFileObject (keyName)         
                    index = contourFileinterface.getParameter ("Key")      
                    singleSliceDef = None
                    try :
                        singleSliceDef = SinglROIAreaObjDef (finterface = contourFileinterface,  MultiROIAreaObjectParent = self) 
                    except :
                        singleSliceDef = None
                        print ("A error occured reading slice: %d" % index)
                    if (singleSliceDef is not None) :
                        roilist[index] = singleSliceDef
                
            if (self._sliceNumber not in  roilist) :
                self._sliceNumber = index
            self._ROIAreaObjectSliceDictionary = roilist
            self._clearSortedKeyList ()                                    
            self._ZBoundingBox[0] = _ZBoundingBox1
            self._ZBoundingBox[1] = _ZBoundingBox2
            self._XBoundingBox[0] = _XBoundingBox1
            self._XBoundingBox[1] = _XBoundingBox2
            self._YBoundingBox[0] = _YBoundingBox1
            self._YBoundingBox[1] = _YBoundingBox2
            self._setFileObjectCache (finterface, SetWriteCache = False)       
   

   """def _updateProjectedSlice (self, axialProjectionControl, SharedROIMask) :
       if self._XAxisMask.hasProjectedSliceChanged () or self._YAxisMask.hasProjectedSliceChanged () :
           vc = self._getSliceMaskVolumeCache (SharedROIMask = SharedROIMask)
           ROI3DVolume = vc.getROIVolume (SharedROIMask)         
           projectedVolume = np.zeros (axialProjectionControl.getProjectionVolume (ROI3DVolume), dtype = np.bool)
           axialProjectionControl.TransformROIVolume (ROI3DVolume, projectedVolume)
           
           
           self._XAxisMask.setProjectionAxis ()
           self._YAxisMask.setProjectionAxis ()"""
         
            
   def setVolumeAxisMask (self, sliceNumber, XAxisMask, xAxisMaskIndexs, YAxisMask, yAxisMaskIndexs) :
       #self._XAxisMask.setProjectionSliceChange (sliceNumber)
       #self._YAxisMask.setProjectionSliceChange (sliceNumber)
       Xlistofindexmasks = []
       Ylistofindexmasks = []
       if yAxisMaskIndexs is None :
           yAxisMaskIndexs = np.array ([],dtype=np.int )         
       if xAxisMaskIndexs is None :
           xAxisMaskIndexs = np.array ([],dtype=np.int )     
       if len (yAxisMaskIndexs) > 0 or len (xAxisMaskIndexs) > 0 :
           sliceNumberSet = set ()
           sliceNumberSet.add (sliceNumber)           
           for Number, sliceData in self._ROIAreaObjectSliceDictionary.items ():
               if sliceNumber != Number and sliceData.isInitLineCrossSet () :
                   sliceNumberSet.add (Number)
                   _, _, additionalXIndex, additionalYIndex = sliceData.getSingleSliceContouredAreaFromContourData (ReturnAxisIndexs = True)                                                         
                   self._XAxisMask.updateMaskFromChildMask (sliceData.getXAxisMask(), None, None)       
                   self._YAxisMask.updateMaskFromChildMask (sliceData.getYAxisMask(), None, None)
                   xAxisMaskIndexs = np.concatenate ((xAxisMaskIndexs, additionalXIndex))
                   yAxisMaskIndexs = np.concatenate ((yAxisMaskIndexs, additionalYIndex))                  
           for Number, sliceData in self._ROIAreaObjectSliceDictionary.items ():
               if Number not in sliceNumberSet :
                   if len (yAxisMaskIndexs) > 0 :
                       masks = sliceData.getYAxisMask().getMaskValuesAtIndexs (yAxisMaskIndexs)
                       Ylistofindexmasks.append (masks)                
                   if len (xAxisMaskIndexs) > 0 :
                       masks = sliceData.getXAxisMask().getMaskValuesAtIndexs (xAxisMaskIndexs)
                       Xlistofindexmasks.append (masks)          
       if len (xAxisMaskIndexs) <= 0 :
           xAxisMaskIndexs = None
       if len (yAxisMaskIndexs) <= 0 :
           yAxisMaskIndexs = None       
       self._XAxisMask.updateMaskFromChildMask (XAxisMask, xAxisMaskIndexs, Xlistofindexmasks)              
       self._YAxisMask.updateMaskFromChildMask (YAxisMask, yAxisMaskIndexs, Ylistofindexmasks)

    
   def isSliceValidInVolumeCache (self, height) :
       if not self._sliceMaskVolumeCache.isInitalized () :
           return False
       return self._sliceMaskVolumeCache.isSliceValidInVolumeCache (height)
   
   """def initalizeVolumeCacheSlicesWithMissingData (self) :
        if self._sliceMaskVolumeCache.isInitalized () :
            self._sliceMaskVolumeCache.initalizeSlicesWithMissingData ()"""
            
   def isRunningInLowMemoryMode (self) :
       try :
           if self._objClipDictionary is not None :
               if self._objClipDictionary.isRunningInLowMemoryMode () :
                   if self._sliceMaskVolumeCache.isInitalized () :
                       self._sliceMaskVolumeCache.setLowMemoryMode ()
                   return True
           return False
       except:
           return False
       
   def getSortedKeyList (self) :
       if self._sortedKeyList is None :
           self._sortedKeyList = sorted (list (self._ROIAreaObjectSliceDictionary.keys ()))
       return self._sortedKeyList
 
   def _clearSortedKeyList (self) :
       self._sortedKeyList = None
      
   def getChangedSlices (self, olderObject) :
       ChangedSlices = []
       slicelist = set (list (self._ROIAreaObjectSliceDictionary.keys ()) + list (olderObject._ROIAreaObjectSliceDictionary.keys ()))
       for key in slicelist :
           if key not in self._ROIAreaObjectSliceDictionary :
               ChangedSlices.append (key)
           elif key not in olderObject._ROIAreaObjectSliceDictionary :
               ChangedSlices.append (key)
           elif olderObject._ROIAreaObjectSliceDictionary[key] is not self._ROIAreaObjectSliceDictionary[key] :
               ChangedSlices.append (key)
       return ChangedSlices



   def setROIDefUIDManager (self, ROIUIDManager) :
       self._ROIUIDManager = ROIUIDManager

   def notifySliceSetContourAreaParameter (self) :
       self._objectHasSlicesWithAllocatedContourMemoryThatCanOptionallyCleared = True
   
   def resetCompressionWaitIndicator (self):
       self._compressionWaitCount = 5
       
   def ifPossibleCompressROIMemory (self) :       
       if (self._objectHasSlicesWithAllocatedContourMemoryThatCanOptionallyCleared) :                      
           self._compressionWaitCount -= 1
           if (self._compressionWaitCount <= 0) :
               for sliceData in self._ROIAreaObjectSliceDictionary.values ():
                   sliceData.ifPossibleCompressROISliceMemory ()                   
               self._compressionWaitCount = 5
               self._objectHasSlicesWithAllocatedContourMemoryThatCanOptionallyCleared = False
               return True
       return False
   
   def getQTSliceCache (self) :
       return self._singleSliceDefQtSliceMemCache
   
   def _setSingleSliceInitLineCross (self) :
       self._singeSliceInitLineCross = True
   
   def isSliceMaskVolumeCacheSet (self):
       return self._sliceMaskVolumeCache.isInitalized ()
       #return self._sliceMaskVolumeCache is not None
   
   def _clearVolumeMaskSliceIfInitalized (self, index) :                              
       if (self.isSliceMaskVolumeCacheSet ()) :
           try :
               if (index > 0 and index < self._sliceMaskVolumeCache.shape[2]) :
                   #self._sliceMaskVolumeCache [:,:,index] = 0
                   self._sliceMaskVolumeCache.zeroSlice (index)                   
           except:
               pass 
           
   def _getSliceMaskVolumeCache (self, SharedROIMask = None) :     
       if (not self.isSliceMaskVolumeCacheSet () and self._volumeShape is not None) : 
           self._initalizeVolumeSliceMask (self._volumeShape, SharedROIMask = SharedROIMask)           
       if (self._singeSliceInitLineCross) :
           VolumeMaskUpdateLst = []           
           for index in self.getSortedKeyList () :
               sliceData = self._ROIAreaObjectSliceDictionary[index]
               if (sliceData.isInitLineCrossSet ()) :                   
                   sliceArea, xAxisMaskIndexs, yAxisMaskIndexs = sliceData.getSingleSliceContouredAreaFromContourDataForMultiSliceParent ()                                      
                   if (self.isSingleROIPerVoxelMode ()) :
                       clipMask = self._getObjClipMask ("Z", index)
                       if (clipMask is not None) :
                           sliceArea[clipMask > 0] = 0      
                   VolumeMaskUpdateLst.append ((index, sliceData, xAxisMaskIndexs, yAxisMaskIndexs) )
                   #self.setVolumeAxisMask (index, sliceData.getXAxisMask (), xAxisMaskIndexs, sliceData.getYAxisMask (), yAxisMaskIndexs)                           
                   self.setVolumeSliceMask_Data_SliceCallback (index, sliceArea)                          
           self._singeSliceInitLineCross = False
           for tpl in VolumeMaskUpdateLst :
               index, sliceData, xAxisMaskIndexs, yAxisMaskIndexs = tpl  
               self.setVolumeAxisMask (index, sliceData.getXAxisMask (), xAxisMaskIndexs, sliceData.getYAxisMask (), yAxisMaskIndexs)   
       return self._sliceMaskVolumeCache
       
       
   def clear (self):
       self._crossSliceChangeSliceNumberList = []
       self._XAxisMask.clear (SetDimToNone=True)
       self._YAxisMask.clear (SetDimToNone=True)
       self._DeepGrowAnnotation = DeepGrowAnnotation (self)
       self._DeepGrowAnnotation.addDeepGrowSliceChangeListener (self._DeepGrowObjectChanged)
       for sliceIndex in self.getSortedKeyList () :
           self._ROIAreaObjectSliceDictionary[sliceIndex].clear ()           
       self._ROIAreaObjectSliceDictionary = {}      
       self._clearSortedKeyList ()       
       self._clearVolumeSliceCache (ClipSliceMask = False)
       self._ROIUIDManager = ROIUIDManager ()       
       self._volumeShape = None
       self._singeSliceInitLineCross = False
       self.setObjectChanged ()       
       self._objClipDictionary = None 
       self._SaveCurrentBoundingBox  = None
       self._getQTMaskMem = None
       self._cachedAxisMask = None
       self._RegistrationLog = ANTSRegistrationLog (self)                     
       self._userChangeLog = ()
       self._contourIDManger = ContourIDManger (self)
       self._isObjMoving = False
       self._sliceNumber = 0
       self._selectedContourIDList = []            
       self._selected = False
       self._ZBoundingBox = np.array ([0, 0])   
       self._XBoundingBox = np.array ([-1, 0])   
       self._YBoundingBox = np.array ([-1, 0])   
       self._SelectedBoundingBoxControlPoint = None              
       self._YAxisROIDrawSettings = ()
       self._XAxisROIDrawSettings = ()
       self._YAxisROIPixMap = None
       self._XAxisROIPixMap = None       
       self._ROIDefIDNumber = None
       
   def __getstate__(self):
        # Copy the object's state from self.__dict__ which contains
        # all our instance attributes. Always use the dict.copy()
        # method to avoid modifying the original state.
        state = self.__dict__.copy()
        # Remove the unpicklable entries.
        state['_YAxisROIPixMap'] = None
        state['_XAxisROIPixMap'] = None        
        state['_YAxisROIDrawSettings'] = ()        
        state['_XAxisROIDrawSettings'] = ()        
        return state

   def hasSliceInSliceView (self, cX, sliceView, ROIDictionary) :
       axis = sliceView.getSliceAxis ()
       if axis == "Z" :
           if sliceView.areAxisProjectionControlsEnabledForAxis () :
                axisProjection = self._getAxisProjection (ROIDictionary, SliceView = sliceView) 
                return self._hasProjectedSliceAtIndex (axisProjection, cX)             
           else:
               return self.hasSlice (cX)
       elif axis == "X" :
           return self.hasAnnotationOnXAxis (cX, SharedROIMask = ROIDictionary.getSharedVolumeCacheMem())
       elif axis == "Y" :
           return self.hasAnnotationOnYAxis (cX, SharedROIMask = ROIDictionary.getSharedVolumeCacheMem())
           
   def hasAnnotationOnYAxis (self, cY, SharedROIMask = None) :
        self._getSliceMaskVolumeCache (SharedROIMask = SharedROIMask)
        axis = self._YAxisMask.getAxisMask (NoCopy = True)
        if axis is None :
            return False
        return axis[cY]
    
   def hasAnnotationOnXAxis (self, cX, SharedROIMask = None) :
        self._getSliceMaskVolumeCache (SharedROIMask = SharedROIMask)
        axis = self._XAxisMask.getAxisMask (NoCopy = True)
        if axis is None :
            return False
        return axis[cX]
    
   def getXAxisSliceNumberList (self, SliceMem = None, SharedROIMask = None):
       self._getSliceMaskVolumeCache (SharedROIMask = SharedROIMask)
       axis = self._XAxisMask.getAxisMask (NoCopy = True)
       if SliceMem is None :           
           if axis is None :
               return np.array ([],dtype=np.uint8)
           return axis.nonzero ()[0]
       elif axis is not None :
           SliceMem[axis] = True
   
   def getYAxisSliceNumberList (self, SliceMem = None, SharedROIMask = None):
       self._getSliceMaskVolumeCache (SharedROIMask = SharedROIMask)
       axis = self._YAxisMask.getAxisMask (NoCopy = True)
       if SliceMem is None :           
           if axis is None :
               return np.array ([],dtype=np.uint8)
           return axis.nonzero ()[0]
       elif axis is not None :
           SliceMem[axis] = True
       
   def getVolumeSliceMask_Data (self, sliceIndex, SharedROIMask = None) :             
       memCache = self._getSliceMaskVolumeCache (SharedROIMask = SharedROIMask)
       if (sliceIndex >= 0 and sliceIndex < memCache.shape[2]) :
           #return memCache[:,:,sliceIndex] 
           return memCache.getSlice (sliceIndex)
       return None
       
   def setVolumeSliceMask_Data_SliceCallback (self, sliceIndex, sliceData) :
       if (self.isSliceMaskVolumeCacheSet () and sliceIndex >= 0 and sliceIndex < self._sliceMaskVolumeCache.shape[2]) :            
           try :
               #self._sliceMaskVolumeCache[:,:,sliceIndex] = sliceData
               self._sliceMaskVolumeCache.setSlice (sliceIndex, sliceData)
               self.getQTSliceCache ().ifSelectedClearSliceCache (sliceIndex)
               return True
           except:
               #self._sliceMaskVolumeCache = None
               self._sliceMaskVolumeCache.clearVolume ()
       return False
   
   def _initalizeVolumeSliceMask_Data (self, sliceIndex, sliceData) :
       if (self.isSliceMaskVolumeCacheSet () and sliceIndex >= 0 and sliceIndex < self._sliceMaskVolumeCache.shape[2]) :            
           try :
               #self._sliceMaskVolumeCache[:,:,sliceIndex] = sliceData
               self._sliceMaskVolumeCache.initializeSlice (sliceIndex, sliceData)               
               return True
           except:
               #self._sliceMaskVolumeCache = None
               self._sliceMaskVolumeCache.clearVolume ()
       return False
                 
   def getSliceVolumeShape (self) :   
       if (self._volumeShape is None):
           return None
       return tuple (self._volumeShape)       

   def isSingleROIPerVoxelMode (self) :
       try :
           return self._sliceMaskVolumeCache.getSharedMemCache () is not None
       except:
           return False
   
   def _clearVolumeSliceCache (self, ClearSelectSliceList = None, ClipSliceMask = True) :       
       if (ClearSelectSliceList is None) :
           if (self.isSliceMaskVolumeCacheSet ()) :
               #del self._sliceMaskVolumeCache
               #self._sliceMaskVolumeCache = None
               self._sliceMaskVolumeCache.zeroVolume ()           
           self._XAxisMask.clear (SetDimToNone=True)
           self._YAxisMask.clear (SetDimToNone=True)
           for sliceIndex in self.getSortedKeyList () :
               value = self._ROIAreaObjectSliceDictionary[sliceIndex]                             
               value.setSliceNumber (sliceIndex)
               value.setMultiROIAreaObjectParent (self)           
               value.setContourAreaMemToNone ()
               #optmized to use zero mem in whole volume
               
               sliceArea = value.getSingleSliceContouredAreaFromContourDataForMultiSliceParent_OptimizedForInit ()
               if (ClipSliceMask and self.isSingleROIPerVoxelMode ()) :
                   clipMask = self._getObjClipMask ("Z", sliceIndex)
                   if (clipMask is not None) :
                       sliceArea[clipMask > 0] = 0
               self.setVolumeAxisMask (sliceIndex, value.getXAxisMask (), None, value.getYAxisMask (), None)                           
               self._initalizeVolumeSliceMask_Data (sliceIndex, sliceArea)                                    
       else:          
           for sliceIndex in ClearSelectSliceList :
               self._clearVolumeMaskSliceIfInitalized (sliceIndex)                   
               if sliceIndex in self._ROIAreaObjectSliceDictionary :
                   value = self._ROIAreaObjectSliceDictionary[sliceIndex]
                   value.setSliceNumber (sliceIndex)
                   value.setMultiROIAreaObjectParent (self)           
                   value.setContourAreaMemToNone ()                               
                   # optmize to use zero slice in call back.                   
                   sliceArea, _, _ = value.getSingleSliceContouredAreaFromContourDataForMultiSliceParent ()                   
                   
                   if (ClipSliceMask and self.isSingleROIPerVoxelMode ()) :
                       clipMask = self._getObjClipMask ("Z", sliceIndex)
                       if (clipMask is not None) :
                           sliceArea[clipMask > 0] = 0
                   self.setVolumeSliceMask_Data_SliceCallback (sliceIndex, sliceArea)     
           self._XAxisMask.clear (SetDimToNone=True)
           self._YAxisMask.clear (SetDimToNone=True)
           for sliceIndex, value in self._ROIAreaObjectSliceDictionary.items () :               
                self.setVolumeAxisMask (sliceIndex, value.getXAxisMask (), None, value.getYAxisMask (), None)                           
       self._singeSliceInitLineCross = False
               
   def setVolumeShape (self, NiftiVolumeDataShape, sharedVolumeCache, ClearSelectSliceList = None, QApp=None, ClipSliceMask = True) :
       self._volumeShape = NiftiVolumeDataShape
       if (sharedVolumeCache != self._sliceMaskVolumeCache.getSharedMemCache ()) :
           self._initalizeVolumeSliceMask (self._volumeShape, SharedROIMask = sharedVolumeCache, QApp = QApp, ClipSliceMask = ClipSliceMask)                      
       else : 
           self._clearVolumeSliceCache (ClearSelectSliceList = ClearSelectSliceList, ClipSliceMask = ClipSliceMask)          
       
   def _initalizeVolumeSliceMask (self, NiftiVolumeDataShape = None, SharedROIMask = None, QApp = None, ClipSliceMask = True) :
       self._XAxisMask.clear (SetDimToNone=True)
       self._YAxisMask.clear (SetDimToNone=True)
       self._volumeShape = NiftiVolumeDataShape
       if NiftiVolumeDataShape is None :
           if (self.isSliceMaskVolumeCacheSet ()) :
               self._sliceMaskVolumeCache.clearVolume ()
               #del self._sliceMaskVolumeCache
           #self._sliceMaskVolumeCache = None
           self.getQTSliceCache ().clearSliceCache ()
           self._singeSliceInitLineCross = False
           return 
       
       if (not self.isSliceMaskVolumeCacheSet () or self._sliceMaskVolumeCache.shape != NiftiVolumeDataShape) :
           #if (self.isSliceMaskVolumeCacheSet ()) :
           #    del self._sliceMaskVolumeCache
           #self._sliceMaskVolumeCache = np.zeros (NiftiVolumeDataShape, dtype = np.uint8)           
           self._sliceMaskVolumeCache = VolumeMaskCache.initalizeVolume (NiftiVolumeDataShape, SharedROIMask, obj = self, RunningInLowMemoryMode = self.isRunningInLowMemoryMode ()) 
       else:
           self._sliceMaskVolumeCache.zeroVolume ()                   
       for sliceIndex in self.getSortedKeyList () :
           sliceData = self._ROIAreaObjectSliceDictionary[sliceIndex]
           sliceData.setSliceNumber (sliceIndex)
           sliceData.setMultiROIAreaObjectParent (self)           
           
           sliceArea = sliceData.getSingleSliceContouredAreaFromContourDataForMultiSliceParent_OptimizedForInit ()
           if (ClipSliceMask and self.isSingleROIPerVoxelMode ()) :
               clipMask = self._getObjClipMask ("Z", sliceIndex)
               if (clipMask is not None) :
                  sliceArea[clipMask > 0] = 0           
           self.setVolumeAxisMask (sliceIndex, sliceData.getXAxisMask (), None, sliceData.getYAxisMask (), None)                           
           self._initalizeVolumeSliceMask_Data (sliceIndex, sliceArea)   
           if QApp is not None :
                QApp.processEvents ()                 
       self.getQTSliceCache ().clearSliceCache ()
       self._objectHasSlicesWithAllocatedContourMemoryThatCanOptionallyCleared = True
       
           
   def _correctSliceNumber (self) :
       if (self._sliceNumber not in self._ROIAreaObjectSliceDictionary) :
           self.setObjectChanged ()
           if (len (self._ROIAreaObjectSliceDictionary) > 0) :
               self._sliceNumber = self.getSortedKeyList ()[0]               
           else:
               self._sliceNumber = None
           
           
   
       
   def clipROIToDimensions (self, origionalVoxelDim, DestinationVolumeSize, DestinationVolumeVoxelSize) :
       self.setObjectChanged ()     
       xdim, ydim, zdim   = DestinationVolumeSize
       
       xScale = float (origionalVoxelDim[0]) / float (DestinationVolumeVoxelSize[0]) 
       yScale = float (origionalVoxelDim[1]) / float (DestinationVolumeVoxelSize[1]) 
       zScale = float (origionalVoxelDim[2]) / float (DestinationVolumeVoxelSize[2]) 
       
       zDelta = int (math.floor ( zScale / 2.0))
              
       newSliceDictionary = {}                           
       
       prevIndex = None
       lastSliceCountoured = -1
       for sliceIndex in self.getSortedKeyList () :
           newZIndex = int (zScale * sliceIndex)       
           if (newZIndex >= 0 and newZIndex <= zdim) :                                             
               if prevIndex is not None and prevIndex == sliceIndex -1 :
                   firstSlice = lastSliceCountoured + 1
               else:
                   firstSlice = newZIndex - zDelta
               for newSliceIndex in range (firstSlice, newZIndex + zDelta + 1, 1) :
                   if (newSliceIndex >= 0 and newSliceIndex < zdim) :
                       newSliceDictionary[newSliceIndex] = self._ROIAreaObjectSliceDictionary[sliceIndex].copy (self, DeepCopyContours = True)
                       HumanEdited = newSliceIndex == newZIndex
                       if (HumanEdited) : #if slice is being copied across set its edited status based on its prior
                           HumanEdited = newSliceDictionary[newSliceIndex].doesSliceContainHumanEditedContour ()                       
                       self.doesSliceContainHumanEditedContour
                       newSliceDictionary[newSliceIndex].clipSliceToDimensions (xdim, ydim, xScale, yScale, newSliceIndex, HumanEdited = HumanEdited)                                            
                       lastSliceCountoured = newSliceIndex
               prevIndex =  newZIndex
              
       del self._ROIAreaObjectSliceDictionary
       self._ROIAreaObjectSliceDictionary =  newSliceDictionary
       self._clearSortedKeyList ()
       self._clearVolumeSliceCache (ClipSliceMask = False)               
       
       self._correctSliceNumber ()
    
       if (xScale != 1.0) :
           self._XBoundingBox[0] = int (float (self._XBoundingBox[0]) * xScale)
           self._XBoundingBox[1] = int (float (self._XBoundingBox[1]) * xScale)
           self._XBoundingBox = np.clip  (self._XBoundingBox, 0, xdim -1)
       if (yScale != 1.0) :
           self._YBoundingBox[0] = int (float (self._YBoundingBox[0]) * yScale)
           self._YBoundingBox[1] = int (float (self._YBoundingBox[1]) * yScale)
           self._YBoundingBox = np.clip  (self._YBoundingBox, 0, ydim -1)           
       if (zScale != 1.0) :
           self._ZBoundingBox[0] = int (float (self._ZBoundingBox[0]) * zScale - zDelta)
           self._ZBoundingBox[1] = int (float (self._ZBoundingBox[1]) * zScale + zDelta)
           self._ZBoundingBox = np.clip  (self._ZBoundingBox, 0, zdim -1)
       
           
   # object only supports 1 slice temporary mask at a time
   def setTemporaryObjectPixMapMask (self, sliceView,  mask, NoCopy = False, ClipBox = None):       
       #axis = sliceView.getSliceAxis ()
       #if axis in ["X", "Y"] or (axis == "Z" and sliceView.getSliceNumber () in self._ROIAreaObjectSliceDictionary) :
       #   self.getQTSliceCache ().setTempQtMask (sliceView, mask, NoCopy = NoCopy, ClipBox = ClipBox) 
       self.getQTSliceCache ().setTempQtMask (sliceView, mask, NoCopy = NoCopy, ClipBox = ClipBox) 
   
    
   def getSliceViewObjectMask (self, sliceView, SliceNumber = None, ClipOverlayingROI = False, DisableAxisProjection = False) :
       if SliceNumber is None :
           SliceNumber = sliceView.getSliceNumber()               
       sliceAxis = sliceView.getSliceAxis ()
       if sliceAxis == "Z" :
           if sliceView.areAxisProjectionControlsEnabledForAxis ()  and sliceView.isNiftiDataLoaded () and not DisableAxisProjection :
               
               def GetVolumeFunc (firstSlice,LastSlice) :
                   if not ClipOverlayingROI :
                       volumeCache = self._getSliceMaskVolumeCache ()
                       return volumeCache.getROISubVolume (firstSlice,LastSlice, volumeCache.getSharedMemCache ())
                   else:
                       sliceDim = sliceView.getNIfTIVolume().getSliceDimensions()
                       sliceShape = (sliceDim[0], sliceDim[1])     
                       returnVolumeMem = np.zeros ((sliceDim[0], sliceDim[1], LastSlice - firstSlice +1),dtype=np.uint8)
                       for sliceIndex, SliceNumber in enumerate(range (firstSlice,LastSlice + 1)) :
                           returnVolumeMem[:,:,sliceIndex] = self.getSliceMap  (SliceNumber, sliceShape, NonPerimeterMasksOnly = False, ClipOverlayedObjects = True)
                       return returnVolumeMem               
               imageData, _  = sliceView.getAxisProjectionControls().transformROIDataFromVolumeToProjection (sliceView.getNIfTIVolume (), sliceView.getAxisProjectionSliceNumber (SliceNumber = SliceNumber), GetVolumeFunc = GetVolumeFunc, ROIIDNumber = self.getROIDefIDNumber (), ClipROIObjectList = [self], DisableProjectionSliceCache = ClipOverlayingROI)
               return imageData 
           
           elif SliceNumber not in self._ROIAreaObjectSliceDictionary:
               sliceDim = sliceView.getNIfTIVolume().getSliceDimensions()
               sliceShape = (sliceDim[0], sliceDim[1])
               return np.zeros (sliceShape, dtype=np.uint8)
           elif (ClipOverlayingROI) :
               sliceDim = sliceView.getNIfTIVolume().getSliceDimensions()
               sliceShape = (sliceDim[0], sliceDim[1])     
               return  self.getSliceMap  (SliceNumber, sliceShape, NonPerimeterMasksOnly = False, ClipOverlayedObjects = True)
           else:
               return self._ROIAreaObjectSliceDictionary[SliceNumber]._getSingleSliceContouredArea ()                      
       elif sliceAxis in ["X", "Y"] :
           width, height  = sliceView.getSliceShape()                  
           return np.copy (self._getSliceAxisMask ( width, height, sliceAxis, SliceNumber, SharedROIMask = sliceView.getSharedROIMask ()))


   def setSliceViewObjectMask (self, sliceView,  contourID, crossSliceMap, SliceIndex, ChangeMap, SliceViewObjectsChanged, ContourType = "Human", Coordinate = None):
        sliceAxis = sliceView.getSliceAxis ()        
        if sliceAxis == 'Z' :
            if sliceView.areAxisProjectionControlsEnabledForAxis ()  and sliceView.isNiftiDataLoaded ():
                sliceView.getAxisProjectionControls().setROISliceChangeMasks (sliceView, ChangeMap, crossSliceMap, self, contourID, ContourType, SliceIndex = SliceIndex, Coordinate = Coordinate)
                SliceViewObjectsChanged.add (self)  
            else:
                sliceDim = sliceView.getNIfTIVolume().getSliceDimensions()
                ZSliceShape = (sliceDim[0], sliceDim[1])
                self.setSliceROIAreaMask (contourID, SliceIndex , crossSliceMap, ZSliceShape, ContourType)  
        else:
            hasSlice = np.any (ChangeMap, axis = 0)
            slicesChanged = hasSlice.nonzero()[0]
            if len (slicesChanged) > 0 :                
                sliceDim = sliceView.getNIfTIVolume().getSliceDimensions()
                ZSliceShape = (sliceDim[0], sliceDim[1])
                for zSliceIndex in slicesChanged :                
                    rowdata = crossSliceMap[: ,zSliceIndex]
                    if sliceAxis == 'X' :
                        self.setSliceDataForCrossSliceChange (ZSliceShape, contourID, zSliceIndex, rowdata, X = SliceIndex, Y=None, ContourType = ContourType)
                    else: #elif sliceAxis == 'Y' :
                        self.setSliceDataForCrossSliceChange (ZSliceShape, contourID, zSliceIndex, rowdata, X = None, Y=SliceIndex, ContourType = ContourType)                                             
                self.adjustBoundingBoxToIncludeSliceList (slicesChanged, sliceDim)
                SliceViewObjectsChanged.add (self)       
                                
    
    
    
   # object only supports 1 slice temporary mask at a time  
   def getTemporaryObjectSliceMask (self, sliceView, ClearBoundingBox = True, ClipOverlayingROI = False, NoCopy = False, NiftiVolumeDim = None) :       
       QTCache =  self.getQTSliceCache ()
       if QTCache.hasTempMask (sliceView) :
           maskData = QTCache.getTempMask (sliceView, None, NoCopy = NoCopy)  
       else:           
           currentSliceData = self.getSliceViewObjectMask (sliceView, ClipOverlayingROI = ClipOverlayingROI)
           maskData = QTCache.getTempMask (sliceView, currentSliceData) 
       if (self._SaveCurrentBoundingBox is None) :           
           self._SaveCurrentBoundingBox = (self._XBoundingBox[0], self._XBoundingBox[1], self._YBoundingBox[0], self._YBoundingBox[1], self._ZBoundingBox[0], self._ZBoundingBox[1])
           if (ClearBoundingBox) :               
               maxWidth, maxHeight,maxSliceCount = NiftiVolumeDim
               self._XBoundingBox[0], self._XBoundingBox[1], self._YBoundingBox[0], self._YBoundingBox[1], self._ZBoundingBox[0], self._ZBoundingBox[1] = 0, maxWidth,0, maxHeight, 0, maxSliceCount
       return maskData
       
   
   def isTemporaryObjectSliceMaskInitalized (self) :
        return (self._SaveCurrentBoundingBox is not None) 
 
   
       
   def _getSliceCopy (self, sliceNumber) :
       sliceData = self._ROIAreaObjectSliceDictionary[sliceNumber]
       if (not sliceData.hasCopied ()):           
           sliceData = sliceData.copy (self)
           self._ROIAreaObjectSliceDictionary[sliceNumber] = sliceData
           self._clearSortedKeyList ()
       return sliceData
       
   # object only supports 1 slice temporary mask at a time
   def clearTemporaryObjectSliceMask (self) :
       self.getQTSliceCache ().clearTempMask ()
       if self._SaveCurrentBoundingBox is not None :
           self._XBoundingBox[0], self._XBoundingBox[1], self._YBoundingBox[0], self._YBoundingBox[1], self._ZBoundingBox[0], self._ZBoundingBox[1] = self._SaveCurrentBoundingBox
           self._SaveCurrentBoundingBox = None
                  
      
   @staticmethod
   def _initAreaObjectLineCrossForSlicesList (obj):
       return obj.setInitLineCross ()
   
   #performance method
   def initAreaObjectLineCrossForSlicesList (self, sliceList) :   
       sliceObjListToInit = []
       for sliceNumber in sliceList :
           if (sliceNumber in self._ROIAreaObjectSliceDictionary) :
               sliceObjListToInit.append (self._ROIAreaObjectSliceDictionary[sliceNumber])
       if len (sliceObjListToInit) > 0 :
           return list (map(MultiROIAreaObject._initAreaObjectLineCrossForSlicesList, sliceObjListToInit))
               
               
           
   def setObjectChangeLog (self, val) :
       self._userChangeLog = val
       self.setObjectChanged ()
       
   def getObjectChangeLog (self) :
       return self._userChangeLog
          
   def getANTSRegistrationLog (self) :
       return self._RegistrationLog
   
   def setBoundingBoxCoordinates (self, bbox):                   
       changed = False
       xbox, ybox, zbox = bbox
       for rindex in range (2) :
           if (self._ZBoundingBox[rindex] != zbox[rindex]) :
               self._ZBoundingBox[rindex] = zbox[rindex]              
               changed = True
           if (self._XBoundingBox[rindex] != xbox[rindex]) :
               self._XBoundingBox[rindex] = xbox[rindex]
               changed = True
           if (self._YBoundingBox[rindex] != ybox[rindex]) :
               self._YBoundingBox[rindex] = ybox[rindex]
               changed = True
       if (changed) :
           self.setObjectChanged ()
       
   def getZBoundingBoxCoordinate (self) :
       return self._ZBoundingBox[0], self._ZBoundingBox[1]
   
   def _getBoundingBoxLst (self):
       return [self._XBoundingBox[0], self._YBoundingBox[0], self._ZBoundingBox[0], self._XBoundingBox[1], self._YBoundingBox[1], self._ZBoundingBox[1]]
   
   def getBoundingBoxCoordinates (self) :
       c1 = Coordinate ()
       c2 = Coordinate ()
       c1.setCoordinate ((self._XBoundingBox[0], self._YBoundingBox[0], self._ZBoundingBox[0]))
       c2.setCoordinate ((self._XBoundingBox[1], self._YBoundingBox[1], self._ZBoundingBox[1]))
       return (c1, c2)

   def isSliceRangeContainClippedSlices (self, niftiVolumeDim, firstSlice, lastSlice = None, SharedROIMask = None, AxisMem = None) :

       def hasContoursInRange (first, last) :
           for sliceNumber in range (first, last+1) :
               if sliceNumber in self._ROIAreaObjectSliceDictionary :
                   return True
           return False
       
       if not self.isBoundingBoxDefined () :
           return False
       XDim, YDim, ZDim = niftiVolumeDim
      
       if lastSlice is not None and firstSlice != lastSlice :
           
           if firstSlice < 0 :
               if lastSlice < 0 :
                   return False
               else:
                   firstSlice = 0
           if lastSlice >= ZDim :
               if firstSlice >= ZDim :
                   return False
               else:
                   lastSlice = ZDim - 1
           if firstSlice <= self._ZBoundingBox[0] and hasContoursInRange (int (firstSlice), int (min (self._ZBoundingBox[0], lastSlice))) : 
               return True
           if lastSlice >= self._ZBoundingBox[1] and hasContoursInRange (int (max(self._ZBoundingBox[1],firstSlice)), int (lastSlice)) :
               return True
       else: 
           if firstSlice < 0 or firstSlice >= ZDim :
               return False
           if (firstSlice <= self._ZBoundingBox[0]  or firstSlice >= self._ZBoundingBox[1]) and firstSlice in self._ROIAreaObjectSliceDictionary  : 
               return True
       if AxisMem is None :
           AxisMem = np.zeros ((int (max (XDim,YDim)),),dtype=np.bool)
       else:
           AxisMem[:] = False
       try :
           self.getXAxisSliceNumberList (SliceMem = AxisMem, SharedROIMask = SharedROIMask)
       except:
           return True
       if np.any (AxisMem[:self._XBoundingBox[0]]) or np.any (AxisMem[self._XBoundingBox[1]:]) :
           return True
       AxisMem[:XDim] = False
       try :
           self.getYAxisSliceNumberList (SliceMem = AxisMem, SharedROIMask = SharedROIMask)
       except:
           return True
       if np.any (AxisMem[:self._YBoundingBox[0]]) or np.any (AxisMem[self._YBoundingBox[1]:]) :
           return True
       return False
       
   def isBoundingBoxDefined (self):
       return self._XBoundingBox[0] != -1 and self._YBoundingBox[0] != -1

   def getBoundingBox (self) :
       return ([self._XBoundingBox[0], self._XBoundingBox[1]],[self._YBoundingBox[0], self._YBoundingBox[1]],[self._ZBoundingBox[0],self._ZBoundingBox[1]])
    
   def exportROIContourMask (self, contourIDLst, volumeDescription, FileName = None, MaskDialation = 0, tempfiledir = None):
        shape = volumeDescription.getParameter ("VolumeDimension")
        data = np.zeros (shape, dtype=np.uint16)                              
                
        objXRange, objYRange = self._getAxisBoundingBox (None, sliceAxis = 'Z')            
        _, objZRange = self._getAxisBoundingBox (None, sliceAxis = 'Y')
        DataValueSet = False       
        for contourID in contourIDLst :
            for z in range (int (objZRange[0]), int (objZRange[1]) + 1, 1) :                                
                if (self.hasSlice(z)) :             
                    sel_slice = self.getSlice (z)
                    if (sel_slice.hasContour (contourID)) :
                       contour = sel_slice.getContour (contourID)
                       slicemap = contour.getContourArea ()                       
                       minV = np.min (slicemap)
                       maxV = np.max (slicemap)                       
                       if (minV != 0 and minV != 1) :
                           print ("Warrning: Slicemap not zeroed as expected, correcting.")
                           slicemap -= minV
                           maxV -= minV
                       if (maxV != 1 and maxV != 0) :
                           print ("Warrning: slicemap data max not 1 as expected, correcting.")
                           slicemap = (slicemap / maxV).astype (data.dtype)
                           maxV = 1            
                       if (len (contourIDLst) == 1) :
                           data[:,:,z] = slicemap
                       else:
                           data[:,:,z] = np.bitwise_or (data[:,:,z], slicemap)         
                       DataValueSet = True                     
        if DataValueSet : # contours were written out now 
            if (MaskDialation > 0):
                data = ndimage.binary_dilation(data, iterations = MaskDialation).astype(data.dtype)
    
            nativeOrientation = volumeDescription.getParameter ("NativeNiftiFileOrientation")
            finalContouredOrientation = volumeDescription.getParameter ("FinalROINiftiOrientation")
            data = NiftiVolumeData.transformData (finalContouredOrientation, nativeOrientation, data)    
            """if (volumeDescription.getParameter ("FlipYAxis")) :            
                data = data [:,range(shape[1] -1, -1,-1),:]  
            data = data [:,:,range(shape[2] -1, -1,-1)]                                  """
            img = nib.Nifti1Image(data.astype (float), volumeDescription.getParameter ("Affine"))                #, header = volumeDescription.getParameter ("NIfTIDataFileHeader")
            
            if (FileName == None) :
                contourfileinputFile = tempfile.mkstemp (suffix= "_"+str (uuid.uuid1 ())+".nii.gz", dir = tempfiledir)                
                contourfileinputFileName = contourfileinputFile[1]
                os.close (contourfileinputFile[0])
                os.remove (contourfileinputFileName)
                FileName = contourfileinputFileName
            try : 
                nib.save (img, FileName)
                return True, FileName
            except:
                os.remove (FileName)        
        return False, ""
    
   def applyAntsTransformToContourROI (self, roiDictionary, contourID, contouredVolumeDescription, uncontouredVolumeDescription, ANTS_Transformations, fixedReferenceImageNiftiFilePath, tempfiledir = None, NumThreads = 10) :                               
        self.setObjectChanged ()
        #for each contour ID write out the volume defined by the contour then map the volume then read the map back in        
        shape = uncontouredVolumeDescription.getParameter ("VolumeDimension")        
        sliceshape = (shape[0], shape[1])
                
        DataValueSet, contourfileinputFileName = self.exportROIContourMask ([contourID], contouredVolumeDescription, tempfiledir = tempfiledir)
        
        if DataValueSet : # contours were written out now 
           
            contourfileoutputFile = tempfile.mkstemp (suffix= "_"+str (uuid.uuid1 ())+".nii.gz", dir = tempfiledir)                
            contourfileOutputFileName = contourfileoutputFile[1]
            os.close (contourfileoutputFile[0])
            os.remove (contourfileOutputFileName)
                            
            at = ApplyTransforms ()
            at.inputs.dimension = 3
            at.inputs.input_image = contourfileinputFileName
            at.inputs.reference_image = fixedReferenceImageNiftiFilePath
            at.inputs.output_image = contourfileOutputFileName
            at.inputs.interpolation = 'Linear'
            at.inputs.default_value = 0
            at.inputs.transforms = ANTS_Transformations #['trans.mat', 'ants_Warp.nii.gz']
            at.inputs.invert_transform_flags = [False] * len (ANTS_Transformations)
            at.inputs.num_threads = NumThreads
            #at.cmdline 
            at.run ()                
            os.remove (contourfileinputFileName)                  
            
            sliceIndexLst = copy.copy (self.getSortedKeyList ())
            for sliceIndex in sliceIndexLst :         
                self.removeSliceContour (contourID, sliceIndex)
                            
            newcontourmapping = NiftiVolumeData (nib.load (contourfileOutputFileName))
            imageData = newcontourmapping.getImageData()
            imageData[imageData != 0] = 1
            imageData = imageData.astype (np.uint8)
            newcontourmapping.setImageData(imageData)
            os.remove (contourfileOutputFileName)  
            sliceIndexLst = self.getSortedKeyList ()
            if ( len (sliceIndexLst) > 1 ) :
                for sliceIndex in range (newcontourmapping.getZDimSliceCount ()) :
                    transforedContourSliceData = newcontourmapping.getImageData (Z = sliceIndex) 
                    if (np.max (transforedContourSliceData) > 0) : # data set for the slice            
                       if (roiDictionary.isROIinAreaMode ()) :
                           self.setSliceROIAreaMask (contourID, sliceIndex, transforedContourSliceData > 0, sliceshape, "Computer", SnappedHumanEditedSliceCount = 0)
                       else:
                           contourlst = FindContours.findContours (transforedContourSliceData.astype (np.float))                       
                           if (len (contourlst) > 0) :  # contours found                                        
                              self.addLargestAreaAtSlicePos(contourID, sliceshape, contourlst,  sliceIndex, "Computer")                                       
            elif ( len (sliceIndexLst) == 1 ) :                
                print ("Special Case, setting slice area for single slice area definition")
                tempSliceMap = np.zeros ((newcontourmapping.getXDimSliceCount (), newcontourmapping.getYDimSliceCount ()), dtype = newcontourmapping.get_data_dtype())
                maskSet = False
                ZIndexWeighted = 0
                ZIndexCount = 0
                for sliceIndex in range (newcontourmapping.getZDimSliceCount ()) :
                    transforedContourSliceData = newcontourmapping.getImageData (Z = sliceIndex)                                        
                    makedPointCount = int (np.sum (transforedContourSliceData))
                    if (makedPointCount > 0) : # data set for the slice                                                  
                       tempSliceMap = np.bitwise_or (tempSliceMap, transforedContourSliceData)
                       maskSet = True
                       ZIndexWeighted += int (makedPointCount * sliceIndex)
                       ZIndexCount    += makedPointCount
                       
                if (maskSet) :
                    sliceIndex = int (ZIndexWeighted / ZIndexCount)
                    if (roiDictionary.isROIinAreaMode ()) :
                         self.setSliceROIAreaMask (contourID, sliceIndex, tempSliceMap > 0, sliceshape, "Computer", SnappedHumanEditedSliceCount = 0)
                    else:
                        contourlst = FindContours.findContours (tempSliceMap.astype (np.float))                                           
                        if (len (contourlst) > 0) :  # contours found                                        
                           self.addLargestAreaAtSlicePos(contourID, sliceshape, contourlst,  sliceIndex, "Computer")                                                       
                
            return True
        return False
   
   """@staticmethod
   def getMinMax (tpl) :
       index, slicedata = tpl
       xmin = -1
       for x in range (slicedata.shape[0]) :
           if np.any (slicedata[x,:]) :
               xmin = x
               break
       if xmin == -1 :
           return (False, index, 0,0,0,0)
       for y in range (slicedata.shape[1]) :
           if np.any (slicedata[:,y]) :
               ymin = y
               break
       for y in range (slicedata.shape[1]-1,-1,-1) :
           if np.any (slicedata[:,y]) :
               ymax = y
               break
       for x in range (slicedata.shape[0]-1,-1,-1) :
           if np.any (slicedata[x,:]) :
               xmax = x
               break
       return (True, index, xmin, ymin, xmax, ymax)
    
   def getContourBoundingBox (self, ContourID):
       ZBoundingBox = np.array ([-1, 0])   
       XBoundingBox = np.array ([0, 0])   
       YBoundingBox = np.array ([0, 0])                
       sliceDataList = []
       
       for sliceIndex in self.getSortedKeyList () :
          ROIslice = self._ROIAreaObjectSliceDictionary[sliceIndex]
          if (ROIslice.hasContour (ContourID)) :
              sliceShape = ROIslice.getShape ()
              slicedata = self.getContourMap (ContourID, sliceIndex, sliceShape)    
              sliceDataList.append ((sliceIndex, slicedata))
       sliceMinMaxList = list (map (MultiROIAreaObject.getMinMax,sliceDataList))  
       for slicedata in sliceMinMaxList  :     
           hasData, sliceIndex, xMin, yMin, xMax, yMax = slicedata
           if (hasData) : 
              if (ZBoundingBox[0] == -1) :                  
                  ZBoundingBox = np.array ([sliceIndex, sliceIndex])                     
                  XBoundingBox = np.array ([xMin, xMax])   
                  YBoundingBox = np.array ([yMin, yMax])   
              else:
                  ZBoundingBox[0] = min (sliceIndex, ZBoundingBox[0])
                  ZBoundingBox[1] = max (sliceIndex, ZBoundingBox[1])            
                  XBoundingBox[0] = min (xMin, XBoundingBox[0])
                  XBoundingBox[1] = max (xMax, XBoundingBox[1])                                
                  YBoundingBox[0] = min (yMin, YBoundingBox[0])
                  YBoundingBox[1] = max (yMax, YBoundingBox[1])         
       if (ZBoundingBox[0] == -1) :
           ZBoundingBox[0] = 0
       return (XBoundingBox, YBoundingBox, ZBoundingBox)"""
   
   @staticmethod 
   def _combineBoundingBox (box1, box2):
       d1 = min (box1[0], box2[0])
       d2 = max (box1[1], box2[1])
       changed = False
       if (d1 != box1[0]) or (d2 != box1[1]) :
           changed = True
       return np.array ([d1, d2], dtype = box1.dtype), changed
       
   def mergeObjectBoundingBoxes (self, obj) :       
       self._ZBoundingBox, zC = MultiROIAreaObject._combineBoundingBox (self._ZBoundingBox, obj._ZBoundingBox)
       self._XBoundingBox, xC = MultiROIAreaObject._combineBoundingBox (self._XBoundingBox, obj._XBoundingBox)
       self._YBoundingBox, yC = MultiROIAreaObject._combineBoundingBox (self._YBoundingBox, obj._YBoundingBox)       
       if (zC or xC or yC) :
           self.setObjectChanged ()
   
   def clipBoundBoxToVolume (self, volumeShape) :       
       if volumeShape is not None :            
            minX = min (max (0, self._XBoundingBox[0]), volumeShape[0] - 1)
            minY = min (max (0, self._YBoundingBox[0]), volumeShape[1] - 1)
            minZ = min (max (0, self._ZBoundingBox[0]), volumeShape[2] - 1)
            maxX = max (min (volumeShape[0] - 1, self._XBoundingBox[1]), 0)
            maxY = max (min (volumeShape[1] - 1, self._YBoundingBox[1]), 0)
            maxZ = max (min (volumeShape[2] - 1, self._ZBoundingBox[1]), 0)
            changed = False
            if (self._XBoundingBox[0] != minX) :
                self._XBoundingBox[0] = minX
                changed = True
            if (self._YBoundingBox[0] != minY) :
                self._YBoundingBox[0] = minY                
                changed = True
            if (self._ZBoundingBox[0] != minZ) :
                self._ZBoundingBox[0] = minZ
                changed = True
            if (self._XBoundingBox[1] != maxX) :
                self._XBoundingBox[1] = maxX
                changed = True
            if (self._YBoundingBox[1] != maxY) :
                self._YBoundingBox[1] = maxY
                changed = True
            if (self._ZBoundingBox[1] != maxZ) :
                self._ZBoundingBox[1] = maxZ
                changed = True
            if (changed) :
                self.setObjectChanged ()
   
   
   def adjustBoundingBoxToIncludeSliceList (self, sliceIndexList, volumeShape = None) :  
       
       def _getAxisMinMax (Axis):
           axis = Axis.nonzero()[0]  
           minV = np.min (axis) - 1
           maxV = np.max (axis) + 1           
           return minV, maxV
       
       maxZIndex = None       
       for sliceIndex in sliceIndexList :
           if sliceIndex in self._ROIAreaObjectSliceDictionary :                
                sliceData = self._ROIAreaObjectSliceDictionary[sliceIndex]
                tempXMask = sliceData.getXAxisMask ().getAxisMask (NoCopy = True) 
                if np.any (tempXMask) :
                    if maxZIndex is None :                                                            
                        xAxis =  np.copy (tempXMask)
                        yAxis = sliceData.getYAxisMask ().getAxisMask (NoCopy = False) 
                        maxZIndex = sliceIndex
                        minZIndex = sliceIndex                    
                    else:                                        
                        if sliceIndex > maxZIndex :
                            maxZIndex = sliceIndex
                        elif sliceIndex < minZIndex:
                            minZIndex = sliceIndex                        
                        np.logical_or (xAxis, tempXMask, out = xAxis)                        
                        np.logical_or (yAxis,  sliceData.getYAxisMask ().getAxisMask (NoCopy = True) , out = yAxis)
       if maxZIndex is not None :
           changed = False
           minZIndex -= 1
           maxZIndex += 1
           if (self._XBoundingBox[0] == -1 or self._YBoundingBox[0] == -1)  : # not yet initalized 
               self._ZBoundingBox[0] = minZIndex
               self._ZBoundingBox[1] = maxZIndex
               changed = True
           else:                              
               if (self._ZBoundingBox[0] > minZIndex) :
                   self._ZBoundingBox[0] = minZIndex
                   changed = True
               if (self._ZBoundingBox[1] < maxZIndex) :
                   self._ZBoundingBox[1] = maxZIndex
                   changed = True
                   
           minX, maxX = _getAxisMinMax (xAxis)
           del xAxis
           minY, maxY = _getAxisMinMax (yAxis)
           del yAxis
           
           if (self._XBoundingBox[0] == -1 or self._YBoundingBox[0] == -1)  : # not yet initalized 
               changed = True                   
               self._XBoundingBox[0] = minX
               self._XBoundingBox[1] = maxX
               self._YBoundingBox[0] = minY
               self._YBoundingBox[1] = maxY
           else:               
               if (minX < self._XBoundingBox[0]) :
                   self._XBoundingBox[0] = minX
                   changed = True                   
               if (minY < self._YBoundingBox[0]) :
                   self._YBoundingBox[0] = minY
                   changed = True                   
               if (maxX > self._XBoundingBox[1]) :
                   self._XBoundingBox[1] = maxX
                   changed = True                   
               if (maxY > self._YBoundingBox[1]) :
                   self._YBoundingBox[1] = maxY
                   changed = True     
           if volumeShape is not None :
                self.clipBoundBoxToVolume (volumeShape)   
           if (changed) :
               self.setObjectChanged ()
               
               
               

   def adjustBoundingBoxToFitContours (self, ExpandBoundingBox = False, volumeShape = None) :             
       self.setObjectChanged ()
       self._ZBoundingBox = np.array ([0, 0])   
       self._XBoundingBox = np.array ([0, 0])   
       self._YBoundingBox = np.array ([0, 0])   
       
       xAxis = self._XAxisMask.getAxisMask (NoCopy = True)
       if xAxis is not None : 
           axis = xAxis.nonzero ()[0]
           if len (axis) > 0 :
               xMin = np.min (axis)
               xMax = np.max (axis)
               yAxis = self._YAxisMask.getAxisMask (NoCopy = True)       
               axis = yAxis.nonzero ()[0]
               yMin = np.min (axis)
               yMax = np.max (axis)
               sliceIndexList = list (self._ROIAreaObjectSliceDictionary.keys ())
               self._XBoundingBox[0] = xMin
               self._XBoundingBox[1] = xMax
               self._YBoundingBox[0] = yMin
               self._YBoundingBox[1] = yMax
               self._ZBoundingBox[0] = min (sliceIndexList)
               self._ZBoundingBox[1] = max (sliceIndexList) 
     
               self._XBoundingBox[0] += -1
               self._XBoundingBox[1] +=  1
               self._YBoundingBox[0] += -1
               self._YBoundingBox[1] +=  1
               self._ZBoundingBox[0] += -1
               self._ZBoundingBox[1] +=  1
               if ExpandBoundingBox and volumeShape is not None :                        
                    ExpansionPrecent = 0.125
                    xP = int (ExpansionPrecent * float (self._XBoundingBox[1] - self._XBoundingBox[1])) + 1
                    yP = int (ExpansionPrecent * float (self._YBoundingBox[1] - self._YBoundingBox[1])) + 1
                    zP = int (ExpansionPrecent * float (self._ZBoundingBox[1] - self._ZBoundingBox[1])) + 1
                    self._XBoundingBox[0] -= xP
                    self._YBoundingBox[0] -= yP
                    self._ZBoundingBox[0] -= zP
                    self._XBoundingBox[1] += xP
                    self._YBoundingBox[1] += yP
                    self._ZBoundingBox[1] += zP
               if volumeShape is not None :
                    self.clipBoundBoxToVolume (volumeShape)
                    
            
   def mergeROI (self, areaObjectToMerge, VolumeShape, PerimeterAreaObjectMerge = True) : # return [ContourID], first slice, last slice       
        self.setObjectChanged ()
        allocatedContourIDs = areaObjectToMerge._contourIDManger.getAllocatedIDList ()
        if (len (allocatedContourIDs) == 0) :
            return ([], 0,0)
        if (PerimeterAreaObjectMerge) :
            newContourIDS = [] # create a place holder contour ids to correspond with the other list
            for count in range (len (allocatedContourIDs)) :
                newContourIDS.append (-1)   
        else:
            newContourIDS = None
        firstslice = None
        lastslice = None
        for slicenumber, slicedata in areaObjectToMerge._ROIAreaObjectSliceDictionary.items () :
            if (firstslice is None) :
                firstslice = slicenumber
                lastslice = firstslice
            else:
                firstslice = min (firstslice, slicenumber)
                lastslice = max (lastslice, slicenumber)            
            if (slicenumber not in self._ROIAreaObjectSliceDictionary) :
                self._ROIAreaObjectSliceDictionary[slicenumber] = SinglROIAreaObjDef (shape = slicedata.getShape (), MultiROIAreaObjectParent  = self)                            
                self._ROIAreaObjectSliceDictionary[slicenumber].setSliceNumber (slicenumber)
                self._clearSortedKeyList ()
            newContourIDS = self._getSliceCopy(slicenumber).mergeSliceROI (slicedata, allocatedContourIDs, newContourIDS)            
            
            self.adjustBoundBoxToFitSlice (slicenumber, VolumeShape)
        self._YAxisROIPixMap = None
        self._XAxisROIPixMap = None     
        
        if (not PerimeterAreaObjectMerge) :
            lst = self.getContourIDManger ().getAllocatedIDList ()  
            return (lst, firstslice,lastslice)
        else:
            return (newContourIDS, firstslice,lastslice)
   
    
    
   def getSliceVoxels (self, NiftiVolume, sliceIndex, ROIDefs, SliceView = None) :       
       NiftiVolumeData = NiftiVolume.getImageData ()       
       if (sliceIndex < 0) or (sliceIndex > NiftiVolume.getZDimSliceCount () - 1) :
           return (np.array ([], dtype=NiftiVolumeData.dtype), 0)
       minZ, maxZ = self._ZBoundingBox
       ROIContourCount = 0       
       if SliceView is not None and SliceView.getSliceAxis () == "Z" and SliceView.areAxisProjectionControlsEnabledForAxis () :
           sliceMask = self.getSliceViewObjectMask (SliceView, SliceNumber = sliceIndex, ClipOverlayingROI = True, DisableAxisProjection = False) 
           if np.any (sliceMask) :
               ProjectedVoxels = SliceView.getNIfTIVolumeSliceViewProjection (SliceIndex = sliceIndex)           
               voxels = ProjectedVoxels[sliceMask.astype (np.bool)]
               ROIContourCount = 1
           else:
               voxels = np.array ([])
       elif sliceIndex >= minZ and sliceIndex <= maxZ and sliceIndex in self._ROIAreaObjectSliceDictionary :                        
           if (ROIDefs.hasSingleROIPerVoxel ()) :
               clipMask = self._getObjClipMask ("Z", sliceIndex)
           else:
               clipMask = None
           
           voxels, sliceContourCount = self._ROIAreaObjectSliceDictionary[sliceIndex].getVoxels (NiftiVolumeData, self._XBoundingBox, self._YBoundingBox, SliceClipMask = clipMask)           
           voxelCount = voxels.shape[0]
           if (voxelCount > 0):
               ROIContourCount += sliceContourCount
       else:
           voxels = np.array ([])
       return (voxels, ROIContourCount)                      
                    
   """def getSliceVoxelMask (self, NiftiVolume, sliceIndex, ROIDefs = None) :             
       if (sliceIndex < 0) or (sliceIndex > NiftiVolume.getZDimSliceCount () - 1) :
           return None
       minZ, maxZ = self._ZBoundingBox       
       if sliceIndex >= minZ and sliceIndex <= maxZ and sliceIndex in self._ROIAreaObjectSliceDictionary :                    
           sliceData = np.copy (self._ROIAreaObjectSliceDictionary[sliceIndex].getSelectionMask (self._XBoundingBox, self._YBoundingBox))
           if (ROIDefs is not None and ROIDefs.hasSingleROIPerVoxel ()) :
               clipMask = self._getObjClipMask ("Z", sliceIndex)
               if (clipMask is not None):
                   sliceData[clipMask > 0] = 0 
           return sliceData
       return None           """
           
       
   def getVoxels (self, NiftiVolume, MaskedVolume = None, ROIDefs = None) :       
       NiftiVolumeData = NiftiVolume.getImageData ()       
       voxelList = []       
       minZ, maxZ = self._ZBoundingBox
       minZ = max (minZ,0)
       maxZ = min (maxZ,NiftiVolumeData.shape[2] -1)
           
       ROIContourCount = 0
       
       for sliceIndex in self.getSortedKeyList () :
           slicedata = self._ROIAreaObjectSliceDictionary[sliceIndex]
           if sliceIndex >= minZ and sliceIndex <= maxZ  :
               
               if (ROIDefs is not None and ROIDefs.hasSingleROIPerVoxel ()) :
                  clipMask = self._getObjClipMask ("Z", sliceIndex)
               else:
                   clipMask = None
                   
               voxels, sliceContourCount = slicedata.getVoxels (NiftiVolumeData, self._XBoundingBox, self._YBoundingBox, MaskedVolume = MaskedVolume, SliceClipMask=clipMask)               
               voxelCount = voxels.shape[0]
               if (voxelCount > 0):
                   ROIContourCount += sliceContourCount                   
                   voxelList.append (voxels)          
       if (len (voxelList) == 0) :
           return (np.array ([],dtype=NiftiVolumeData.dtype), 0)
       return (np.concatenate (voxelList, axis = 0), ROIContourCount)
              
   def getType (self) :
       return "Area"

   def isROIArea (self) :
       return self.getType () == "Area"   

   def setSelectedContours (self, contourID) :       
       self._selectedContourIDList = contourID       

   def getSelectedContours (self) :
       return self._selectedContourIDList

   def doesContourDefineNewPerimeter (self, sliceNumber, contourData):
       if sliceNumber in self._ROIAreaObjectSliceDictionary :
           sliceData = self._ROIAreaObjectSliceDictionary[sliceNumber]
           return (sliceData._doesContourDefineNewArea  (contourData))
       return True
       
   def addROIAreaContour (self, name) :
      self.setObjectChanged ()
      contourID = self._contourIDManger.allocateID () 
      self._contourIDManger.setContourName (contourID, name)
      return contourID
      
   def setContourName (self, contourID, name) :   
       if  self._contourIDManger.hasContourName (contourID) :
          self.setObjectChanged ()
          self._contourIDManger.setContourName (contourID, name)
       
   def setContourType (self, contourID, cType) :   
       self.setObjectChanged ()
       if  self._contourIDManger.hasContourName (contourID) :           
          self._contourIDManger.setContourType (contourID, cType)
          
          
   def getContourName (self, contourID) :   
       if  self._contourIDManger.hasContourName (contourID) :
           return self._contourIDManger.getContourName (contourID)
       else:
           return "Error unknown contour ID"
      
   def getContourType (self, contourID):
       if  self._contourIDManger.hasContourName (contourID) :
           return self._contourIDManger.getContourType (contourID)
       return None
   
   @staticmethod
   def _updateContourSlices (value) :
       value.setInitLineCross ()
       return
   
   def updateContourSlices (self,ContourID) : 
       contourSliceList = []
       for value in self._ROIAreaObjectSliceDictionary.values  ()  :
           if (value.hasContour (ContourID)) :
               contourSliceList.append (value)
       if len (contourSliceList) > 0 :
           self.setObjectChanged () 
           result = list (map (MultiROIAreaObject._updateContourSlices, contourSliceList))
           self._YAxisROIPixMap = None
           self._XAxisROIPixMap = None
           return result
       
   def getSlice (self, index) :
        return self._ROIAreaObjectSliceDictionary [index]

   def hasSlice (self, index) :
        return index in self._ROIAreaObjectSliceDictionary
   
   def getContourIDManger (self) :       
       return self._contourIDManger
        
   @staticmethod 
   def _copySlice (pair) :       
        key, value = pair
        return (key, value.copy (MultiROIAreaObjectParent = None))
   
   def swapSliceVolumeCache (self, newobj) :
       newobj._sliceMaskVolumeCache, self._sliceMaskVolumeCache = self._sliceMaskVolumeCache, newobj._sliceMaskVolumeCache
       
   def copy (self, MoveSliceMaskVolumeCache = False, DeepCopySlices = False) :     
        self.endCrossSliceChange () 
        if MoveSliceMaskVolumeCache and self.isSliceMaskVolumeCacheSet () :
            newobj = MultiROIAreaObject (SliceMaskVolumeCache = self._sliceMaskVolumeCache)
            newobj._singeSliceInitLineCross = self._singeSliceInitLineCross
        else:
            newobj = MultiROIAreaObject ()
        newobj._YAxisMask = self._YAxisMask.copy ()
        newobj._XAxisMask = self._XAxisMask.copy ()
        newobj._DeepGrowAnnotation = self._DeepGrowAnnotation.copy ()
        newobj._ROIUIDManager = self._ROIUIDManager.copy ()
        newobj._objectHasSlicesWithAllocatedContourMemoryThatCanOptionallyCleared = self._objectHasSlicesWithAllocatedContourMemoryThatCanOptionallyCleared
        newobj._compressionWaitCount = self._compressionWaitCount
        newobj._UndoChanged = self._UndoChanged           
        newobj._volumeShape = self.getSliceVolumeShape ()
        """if (MoveSliceMaskVolumeCache and self.isSliceMaskVolumeCacheSet ()) :
            newobj._sliceMaskVolumeCache = self._sliceMaskVolumeCache
            #self._sliceMaskVolumeCache = VolumeMaskCache (self._ROIDefIDNumber)
            newobj._singeSliceInitLineCross = self._singeSliceInitLineCross
            #self._singeSliceInitLineCross = False"""
        newobj._setFileObjectCache (self._getFileObjectCache (), SetWriteCache = False)
        newobj._objClipDictionary = self._objClipDictionary
        newobj._userChangeLog = copy.copy (self._userChangeLog)        
        newobj._RegistrationLog = self._RegistrationLog.copy (newobj)
        newobj._ROIDefIDNumber = self._ROIDefIDNumber
        newobj._contourIDManger = self._contourIDManger.copy (newobj)
        newobj._selectedContourIDList = copy.copy(self._selectedContourIDList)
        newobj._sliceNumber =  self._sliceNumber
        
        newobj._ROIAreaObjectSliceDictionary = {}        
        if (not DeepCopySlices) :            
            for sliceIndex in self._ROIAreaObjectSliceDictionary.keys () :
                sliceData = self._ROIAreaObjectSliceDictionary[sliceIndex]
                newobj._ROIAreaObjectSliceDictionary[sliceIndex] = sliceData
                sliceData.setSliceNotCopied ()           
                newobj._clearSortedKeyList ()
        else:
            for key in self.getSortedKeyList () :
                value = self._ROIAreaObjectSliceDictionary[key]
                newobj._ROIAreaObjectSliceDictionary[key] = value.copy (DeepCopyContours = True, MultiROIAreaObjectParent = newobj) 
                newobj._clearSortedKeyList ()
                    
        newobj._selected = self._selected  
        newobj._ZBoundingBox = np.copy  (self._ZBoundingBox)   
        newobj._XBoundingBox = np.copy (self._XBoundingBox)   
        newobj._YBoundingBox = np.copy (self._YBoundingBox)        
        newobj._SelectedBoundingBoxControlPoint = None
        newobj._SaveCurrentBoundingBox =  self._SaveCurrentBoundingBox                                
        return newobj
    
   """ @staticmethod 
   def _getSliceFileObject (pair) :       
        key, value, i = pair
        sliceFileObj = FileObject ("Slice" + str (i))        
        sliceFileObj.setParameter ("Key", key)            
        value.addToFileObj (sliceFileObj)
        return sliceFileObj"""
   
   def getDeepGrowAnnotation (self) :
       return self._DeepGrowAnnotation
   
   def getFileObject (self, ROIDefs, Name = None) :
       if (self.isChanged ()) :
           f = FileObject ("MultiROIAreaObject")       

           if (Name is None) :
               Name = self.getName (ROIDefs)
               
           roiColor = ROIDefs.getROIColor (Name)            
           f.setParameter ("Name", Name)     
           f.setParameter ("Color",(roiColor.red (), roiColor.green (), roiColor.blue (), roiColor.alpha ()))                 
           f.setParameter ("MaskValue",ROIDefs.getROIMaskValue (Name) )     
           f.setParameter ("RadlexID", ROIDefs.getROINameRadlexID (Name)) 
                                
           x1, x2, y1, y2, z1, z2 =  (self._XBoundingBox[0].item(), self._XBoundingBox[1].item(), self._YBoundingBox[0].item(), self._YBoundingBox[1].item(), self._ZBoundingBox[0].item(), self._ZBoundingBox[1].item())
           f.setParameter ("BoundingBox", (x1, x2, y1, y2, z1, z2))
           self._RegistrationLog.saveToFile (FileHeader = f)      
           f.setParameter ("SliceNumber", self._sliceNumber)
           count = len (self._ROIAreaObjectSliceDictionary)  
           f.setParameter ("SliceCount", count)                          
           f.setParameter ("UserChangeLog", self.getObjectChangeLog ())                          
           f.setParameter ("ROIDefUIDObj",  self._ROIUIDManager.getTuple ())   
           self._contourIDManger.save (f)
           i = 1       
          
           for key, item in self._ROIAreaObjectSliceDictionary.items():                                   
               sliceObj = item.getSinglROIAreaObjFileObj (i, key) #addToFileObj
               i += 1
               f.addInnerFileObject (sliceObj)                                                               
           f.addInnerFileObject (self._DeepGrowAnnotation.getFileObject ())
           self._setFileObjectCache (f)       
       return self._getFileObjectCache ()
              
   def setZBoundBox (self, sliceNumber) :       
       if (self._ZBoundingBox[0] != sliceNumber or self._ZBoundingBox[1] != sliceNumber) :
           self._ZBoundingBox[0] = sliceNumber
           self._ZBoundingBox[1] = sliceNumber
           self.setObjectChanged ()
       
   def adjustBoundBoxToFitSlice (self, index, volumeShape, NewBoxArea = (None, None,None,None)) :
       self.setObjectChanged ()       
       if len (NewBoxArea) == 4 and index not in self._ROIAreaObjectSliceDictionary :           
           return           
       if len (NewBoxArea) == 6 and NewBoxArea[0] is not None :
           minx, miny,minz, maxx, maxy,maxz = NewBoxArea
       elif (NewBoxArea[0] is None or self._XBoundingBox[0] == -1 or self._YBoundingBox[0] == -1) : # new box not defined or existing bounding box  not defined
           sliceList = self.getSliceNumberList ()
           firstsliceIndex = sliceList[0]
           sl = self._ROIAreaObjectSliceDictionary[firstsliceIndex]               
           #minx, miny, maxx, maxy = sl.getMinMaxXYBounding ()    
           
           xAxis = sl.getXAxisMask ()
           axis = xAxis.getAxisMask (NoCopy = True).nonzero()[0]
           if len (axis) > 0 :   
               minx = np.min (axis)
               maxx = np.max (axis)
              
               yAxis = sl.getYAxisMask ()
               axis = yAxis.getAxisMask (NoCopy = True).nonzero()[0]
               miny = np.min (axis)
               maxy = np.max (axis)
           else:
               minx, maxx, miny,maxy = 0,0,0,0
           minz = min (sliceList)
           maxz = max (sliceList)
       elif len (NewBoxArea) == 4 :
           minx, miny, maxx, maxy = NewBoxArea           
           minz, maxz = self._ZBoundingBox
           if (index <= minz):
               minz = int (index - 5)
           if (index >= maxz):
               maxz = int (index + 5)
       dy = max (int (0.25 * (maxy - miny)) + 1,5)
       dx = max(int (0.25 * (maxx - minx)) + 1,5)
       minx -= dx
       maxx += dx
       miny -= dy
       maxy += dy                       
       minz = int (minz - 5)
       maxz = int (maxz + 5)
           
       initbox= False
       if (self._XBoundingBox[0] == -1) :
           initbox= True
           self._XBoundingBox[0] = max(minx, 0)
       else:
           self._XBoundingBox[0] = max(min (self._XBoundingBox[0], minx), 0)
       self._XBoundingBox[1] = max (self._XBoundingBox[1], maxx)
       if (self._YBoundingBox[0] == -1) :
           initbox= True
           self._YBoundingBox[0] = max(miny,0)
       else:
           self._YBoundingBox[0] = max(min (self._YBoundingBox[0], miny),0)
       self._YBoundingBox[1] = max(max (self._YBoundingBox[1], maxy),0)
              
       if (initbox) :
           self._ZBoundingBox[0] = max(minz,0)
       else:
           self._ZBoundingBox[0] = max(min (self._ZBoundingBox[0], minz),0)
       self._ZBoundingBox[1] = max(max (self._ZBoundingBox[1], maxz),0)
           
           
       
               
       if (volumeShape is not None) :
           maxX, maxY, maxZ = volumeShape
           maxX -= 1
           maxY -= 1
           maxZ -= 1
           self._ZBoundingBox[0] = MultiROIAreaObject._cliptoVolume (self._ZBoundingBox[0], maxZ )
           self._ZBoundingBox[1] = MultiROIAreaObject._cliptoVolume (self._ZBoundingBox[1], maxZ )
           self._XBoundingBox[0] = MultiROIAreaObject._cliptoVolume (self._XBoundingBox[0], maxX )           
           self._XBoundingBox[1] = MultiROIAreaObject._cliptoVolume (self._XBoundingBox[1], maxX )     
           self._YBoundingBox[0] = MultiROIAreaObject._cliptoVolume (self._YBoundingBox[0], maxY )     
           self._YBoundingBox[1] = MultiROIAreaObject._cliptoVolume (self._YBoundingBox[1], maxY )     
   
   @staticmethod
   def _cliptoVolume (cord, maxValue):
       return max (0, min (cord, maxValue))       
     
     
   def getSliceContoursAtPoint (self,index, x, y, niftislicewidget):       
      if (x >= self._XBoundingBox[0] and x <= self._XBoundingBox[1] and y >= self._YBoundingBox[0] and y <= self._YBoundingBox[1] and index >= self._ZBoundingBox[0] and index <= self._ZBoundingBox[1]) :
          if index in self._ROIAreaObjectSliceDictionary :
              SharedROIMask = niftislicewidget.getSharedROIMask ()
              if self.hasAnnotationOnYAxis (y, SharedROIMask = SharedROIMask) and self.hasAnnotationOnXAxis (x, SharedROIMask = SharedROIMask) :
                  roiSlice = self._ROIAreaObjectSliceDictionary[index]
                  return roiSlice.getContoursAtPoint (x,y)
      return []

   def doesSliceContainHumanEditedContour (self, index) :
       if index in self._ROIAreaObjectSliceDictionary :
           roiSlice = self._ROIAreaObjectSliceDictionary[index]
           return roiSlice.doesSliceContainHumanEditedContour ()
       return None
       
   def isHumanEditedContour (self, contourID, index) :
       if index in self._ROIAreaObjectSliceDictionary :
           roiSlice = self._ROIAreaObjectSliceDictionary[index]
           if roiSlice.hasContour (contourID) :
               contour = roiSlice.getContour (contourID)                        
               return contour.isHumanEditedContour ()      
       return False
 
   def setHumanEditedContourSnapCount (self, contourID, index, snappedSliceCount) :
       if index in self._ROIAreaObjectSliceDictionary :
           roiSlice = self._ROIAreaObjectSliceDictionary[index]
           if roiSlice.hasContour (contourID) :
               self.setObjectChanged ()
               contour = roiSlice.getContour (contourID)                                       
               self._getSliceCopy (index).setHumanEditedContourSnapCount (contour, snappedSliceCount)   
           
   def setHumanEditedContour (self, contourID, index, val) :
       if index in self._ROIAreaObjectSliceDictionary :
           roiSlice = self._ROIAreaObjectSliceDictionary[index]
           if roiSlice.hasContour (contourID) :               
               self.setObjectChanged ()
               contour = roiSlice.getContour (contourID)                        
               self._getSliceCopy(index).setHumanEditedContour (contour, val)               
   
   def getHumanEditedContourSnapCount (self, contourID, index) :         
       if index in self._ROIAreaObjectSliceDictionary :
           roiSlice = self._ROIAreaObjectSliceDictionary[index]
           if roiSlice.hasContour (contourID) :               
               contour = roiSlice.getContour (contourID)                        
               return contour.getHumanEditedContourSnapCount ()      
       return 0
   
   def hasHumanEditedSlices (self) :       
       for index in self._ROIAreaObjectSliceDictionary.keys () :
           if self.doesSliceContainHumanEditedContour (index) :
               return True
       return False
   
   def setSelectedBoundingBoxControlPoint (self, pt):
       #self.setObjectChanged ()
       self._SelectedBoundingBoxControlPoint = pt

   def getSelectedBoundingBoxControlPoint (self):
       return self._SelectedBoundingBoxControlPoint
       
   def mergeLineSegments (self, contourID, index, shape, line):              
       segmenetsMerged = False
       if index in self._ROIAreaObjectSliceDictionary :
            contours = self._ROIAreaObjectSliceDictionary[index]
            if (contours.hasContour (contourID) and contours.isContourDefinedByPerimeterConID(contourID)) :                
                result, newArea = AreaMerge.mergeLineSegments (contours.getContourCoordinates (contourID).T, line)
                if (result) :
                    self.setObjectChanged ()
                    segmenetsMerged = True
                    self._getSliceCopy (index).setContourPerimeter (contourID, newArea)                     
                    self._sliceNumber = index    
                    self._YAxisROIPixMap = None
                    self._XAxisROIPixMap = None
       return segmenetsMerged
   
   def _setSliceROIContourData (self, sliceNumber, contour, shape, DelayInitLineCross = False):
       self.setObjectChanged ()
       self._sliceNumber = sliceNumber   
       self._YAxisROIPixMap = None
       self._XAxisROIPixMap = None
       #print ("self._XAxisROIPixMap set to None")
       #print ("self._YAxisROIPixMap set to None")
       if sliceNumber not in self._ROIAreaObjectSliceDictionary :
           self._ROIAreaObjectSliceDictionary[sliceNumber] = SinglROIAreaObjDef (sliceNumber, contour, shape, MultiROIAreaObjectParent = self, DelayInitLineCross = DelayInitLineCross)
           self._clearSortedKeyList ()
       else:      
           self._getSliceCopy(sliceNumber).addContour (contour, DelayInitLineCross = DelayInitLineCross)

   def setSliceROIPerimeter (self, contourID, sliceNumber, XYArea, shape, ContourType, SnappedHumanEditedSliceCount = 0, DelayInitLineCross = False):       
       #self.setObjectChanged () called in two callie functions
       contour = Contour ( contourID, XYArea, shape, ContourType, SnappedHumanEditedSliceCount= SnappedHumanEditedSliceCount)                                                    
       self._setSliceROIContourData (sliceNumber, contour, shape,DelayInitLineCross=DelayInitLineCross)                  


   
   def setSliceROIAreaMask (self, contourID, sliceNumber, AreaMask, shape, ContourType, SnappedHumanEditedSliceCount = 0,DelayInitLineCross = False, createSliceIfUndefined = False):
       #self.setObjectChanged ()
       if (createSliceIfUndefined or np.any (AreaMask)) :
           contour = Contour ( contourID, None, shape, ContourType, SnappedHumanEditedSliceCount= SnappedHumanEditedSliceCount, contourAreaMask = AreaMask)                                                    
           self._setSliceROIContourData (sliceNumber, contour, shape,DelayInitLineCross=DelayInitLineCross) 
       else:
           self.removeSliceContour (contourID, sliceNumber)
    
  
   def getSliceContourIDList (self, sliceIndex):
       try :
           return  self._ROIAreaObjectSliceDictionary[sliceIndex].getContourIDList ()
       except:
           return  None
       
   def setSliceDataForCrossSliceChange (self, shape, contourID, sliceNumber, Data, X = None, Y=None, ContourType = "Computer", createSliceIfUndefined = False):       
       self._sliceNumber = sliceNumber   
       self._YAxisROIPixMap = None
       self._XAxisROIPixMap = None
       if sliceNumber not in self._ROIAreaObjectSliceDictionary :
           if createSliceIfUndefined or np.any (Data) :
               self.setObjectChanged ()
               contour = Contour ( contourID, None, shape, ContourType, SnappedHumanEditedSliceCount= 0, contourAreaMask = np.zeros(shape, dtype=np.uint8))                                                    
               contour.setContourInitalizedForCrossSliceChange ()
               contour.setMaskContourCrossSlice (Data, X = X, Y=Y, ContourType = ContourType)
               self._ROIAreaObjectSliceDictionary[sliceNumber] = SinglROIAreaObjDef (sliceNumber, contour, shape, MultiROIAreaObjectParent = self, DelayInitLineCross = False)
               self._clearSortedKeyList ()           
       else:      
           #self.setObjectChanged () Called in child method       
           newSlice = self._getSliceCopy(sliceNumber)
           newSlice.setContourAreaMapForCrossSliceChange (contourID, Data, X=X, Y=Y, ContourType = ContourType)
       self._crossSliceChangeSliceNumberList.append ((sliceNumber,contourID))
   
   def clearSliceDataForCrossSliceChange (self, contourID, sliceNumber, Axis, SliceRange, ContourType = "Computer"):       
       if sliceNumber in self._ROIAreaObjectSliceDictionary :           
           self.setObjectChanged ()           
           self._YAxisROIPixMap = None
           self._XAxisROIPixMap = None
           newSlice = self._getSliceCopy(sliceNumber)
           newSlice.clearContourAreaMapForCrossSliceChange (contourID,  Axis , SliceRange, ContourType = ContourType)
           self._crossSliceChangeSliceNumberList.append ((sliceNumber,contourID))
           
   def endCrossSliceChange (self) :
       if len (self._crossSliceChangeSliceNumberList) > 0 :
           for sliceTpl in self._crossSliceChangeSliceNumberList :
               sliceNumber, contourID = sliceTpl 
               if sliceNumber in self._ROIAreaObjectSliceDictionary :
                   result = self._ROIAreaObjectSliceDictionary[sliceNumber].endCrossSliceChange (contourID)
                   if result is not None and not result :
                       self.setObjectChanged ()                  
                       self.removeSliceContour (contourID, sliceNumber)
           self._crossSliceChangeSliceNumberList = []
       return len (self._ROIAreaObjectSliceDictionary)
  
   def getSliceDataForCrossSliceChange (self, shape, contourID, sliceNumber, X = None, Y = None) :
       if sliceNumber in self._ROIAreaObjectSliceDictionary :
           sliceData = self._ROIAreaObjectSliceDictionary[sliceNumber].getMaskContourCrossSlice (contourID)
           if sliceData is not None :
               return sliceData
       if X is not None  :
           return np.zeros ((shape[1],), dtype = np.uint8)
       else:
           return np.zeros ((shape[0],), dtype = np.uint8)
          
    
    
   def isContourDefined (self, contourIDNumber) :
       for slicenumber, singleslice in self._ROIAreaObjectSliceDictionary.items () :
           if (singleslice.hasContour (contourIDNumber)):
               return (True, slicenumber)
       return (False, 0)
   
   def areSelectedContoursDeleteableInSlice (self, sliceNumber) :    
       if (sliceNumber in self._ROIAreaObjectSliceDictionary) :     
           for selectedid in self._selectedContourIDList :
               if (self._ROIAreaObjectSliceDictionary[sliceNumber].hasContour (selectedid)):
                   return True
        
       for selectedid in self._selectedContourIDList :
           ContoursFound, index = self.isContourDefined (selectedid)
           if (not ContoursFound) :
                   return True                   
       return False
       
   def _removeSliceAxisMasks (self, sliceNumber) :
       sliceData = self._ROIAreaObjectSliceDictionary[sliceNumber]
       xmask = sliceData.getXAxisMask ()
       msk = xmask.getAxisMask (NoCopy = True)
       voxelsCleared = 0
       if msk is not None :
           xAxisMaskIndexs = msk.nonzero ()[0]
           xmask.clear ()
           voxelsCleared += xAxisMaskIndexs.shape[0]
       else:
           xAxisMaskIndexs = None        
       ymask = sliceData.getYAxisMask ()
       msk = ymask.getAxisMask (NoCopy = True)
       if msk is not None :
           yAxisMaskIndexs = msk.nonzero ()[0]
           ymask.clear ()
           voxelsCleared += yAxisMaskIndexs.shape[0]
       else:
           yAxisMaskIndexs = None 
       if voxelsCleared > 0 :
           self.setVolumeAxisMask (sliceNumber, xmask, xAxisMaskIndexs, ymask, yAxisMaskIndexs)      
                      
   def removeSliceContour (self, ContourID, sliceNumber):              
       if (sliceNumber in self._ROIAreaObjectSliceDictionary) :         
           self.setObjectChanged ()
           remainingContours = self._getSliceCopy(sliceNumber).deleteContourID (ContourID)
           if (remainingContours == 0) :                                           
                  self._removeSliceAxisMasks (sliceNumber)               
                  del self._ROIAreaObjectSliceDictionary[sliceNumber] 
                  self._clearSortedKeyList ()
                  self._clearVolumeMaskSliceIfInitalized (sliceNumber)
           self._YAxisROIPixMap = None
           self._XAxisROIPixMap = None
       self._correctSliceNumber ()               
       return (len (self._ROIAreaObjectSliceDictionary))
      
   def _removeProjectedSlice (self, sliceList, SliceView,  SliceViewObjectsChanged) :
       sliceRemoved = False 
       if SliceView.getSliceAxis () == 'Z' :
           if  SliceView.areAxisProjectionControlsEnabledForAxis () :                
               isSliceRemoved = SliceView.getAxisProjectionControls ().clearROISliceChangeMasks (SliceView, self, "Human", SliceIndexList = sliceList)
               if  isSliceRemoved :
                   sliceRemoved = True
                   if SliceViewObjectsChanged is not None :
                       SliceViewObjectsChanged.add (self)
               for sliceNumber in sliceList :
                   if self._DeepGrowAnnotation.removeSlice (sliceNumber) :
                       sliceRemoved = True            
               if sliceRemoved :
                  self._correctSliceNumber () 
                  self._YAxisROIPixMap = None
                  self._XAxisROIPixMap = None
       return sliceRemoved
   
    
   def _getAxisProjection (self, ROIDictionary, SliceView = None, NIfTIVolume = None, Coordinate = None, axisProjection = None) :
       if SliceView is not None :
           NIfTIVolume = SliceView.getNIfTIVolume ()
           Coordinate = SliceView.getCoordinate ()
           axisProjection = SliceView.getAxisProjectionControls ()
       return axisProjection.getROIProjectionSliceAxis (NIfTIVolume, ROIDictionary,  Coordinate) 
        
   def _hasProjectedSliceAtIndex (self, projectionAxis, sliceIndex, roiID = None) :
       if roiID is None :
           roiID = self.getROIDefIDNumber()
       return (sliceIndex in projectionAxis and roiID in projectionAxis[sliceIndex])                   
               
   def hasDeleteableSliceInRange (self, firstSlice, lastSlice, RemoveProjectedSlices = False, SliceView = None, ROIDictionary = None) :
       if self.hasDeepGrowAnnotations () :
           deepGrow = self.getDeepGrowAnnotation ()
           for sliceIndex in range (firstSlice, lastSlice + 1) :
               if deepGrow.hasAnnotationsForSlice(sliceIndex) :
                   return True
       if RemoveProjectedSlices and SliceView is not None and SliceView.areAxisProjectionControlsEnabledForAxis () and ROIDictionary is not None :           
           NIfTIVolume = SliceView.getNIfTIVolume ()
           Coordinate = SliceView.getCoordinate ()
           axisProjection = SliceView.getAxisProjectionControls ()
           roiID = self.getROIDefIDNumber()
           projectionAxis = self._getAxisProjection (ROIDictionary, SliceView = SliceView, NIfTIVolume = NIfTIVolume, Coordinate = Coordinate, axisProjection = axisProjection)
           for sliceIndex in range (firstSlice, lastSlice + 1) :
               if self._hasProjectedSliceAtIndex (projectionAxis, sliceIndex, roiID) :
                     return True
       else:
           for sliceIndex in range (firstSlice, lastSlice + 1) :
               if self.hasSlice (sliceIndex) :
                   return True
       return False
                        
       
   def removeSlice (self, firstSliceNumber, RemoveAllSliceContours = False, Axis="Z", SharedROIMask = None, LastIndex = None, SliceViewObjectsChanged = None, RemoveProjectedSlices = False, SliceView = None):       
       sliceRemoved = False
       if LastIndex is None :
           LastIndex = firstSliceNumber
       if Axis == "Z" :           
           if RemoveProjectedSlices and SliceView is not None and SliceView.areAxisProjectionControlsEnabledForAxis () :
               sliceRemoved = self._removeProjectedSlice (range(firstSliceNumber, LastIndex + 1), SliceView, SliceViewObjectsChanged)
               if sliceRemoved :    
                   self.endCrossSliceChange ()
           else:    
               sliceRemoved = False
               for sliceNumber in range (firstSliceNumber, LastIndex + 1) :
                   if (sliceNumber in self._ROIAreaObjectSliceDictionary) :                    
                       sliceRemoved = True
                       if (RemoveAllSliceContours) :
                           removeContourIDList = self._ROIAreaObjectSliceDictionary[sliceNumber].getContourIDList ()
                       else:
                           removeContourIDList = self._selectedContourIDList                       
                       for contourID in removeContourIDList :
                          self.removeSliceContour (contourID, sliceNumber)           
                          RemainingContoursFound, index = self.isContourDefined (contourID)
                          if (not RemainingContoursFound):
                             self._contourIDManger.deleteID (contourID)                                               
                   if self._DeepGrowAnnotation.removeSlice (sliceNumber) :
                       sliceRemoved = True       
       elif Axis == "X" or Axis == "Y" :           
           if Axis == "X" : 
               AxisSliceList = self.getXAxisSliceNumberList (SharedROIMask = SharedROIMask)
           else:
               AxisSliceList = self.getYAxisSliceNumberList (SharedROIMask = SharedROIMask)            
           AxisMask = np.bitwise_and(AxisSliceList >= firstSliceNumber, AxisSliceList <= LastIndex)           
           if np.any (AxisMask) :
               firstSliceNumber = np.min (AxisSliceList[AxisMask])
               LastIndex = np.max (AxisSliceList[AxisMask])                   
               for ZSliceIndex in list (self._ROIAreaObjectSliceDictionary.keys ()) :                                               
                   if self._ROIAreaObjectSliceDictionary[ZSliceIndex].hasDataInAxisMaskRange (firstSliceNumber,LastIndex, Axis) :
                       sliceRemoved = True               
                       if (RemoveAllSliceContours) :
                           removeContourIDList = self._ROIAreaObjectSliceDictionary[ZSliceIndex].getContourIDList ()
                       else:
                           removeContourIDList = self._selectedContourIDList        
                   
                       if SliceViewObjectsChanged is not None : # deleting through sliceview interface                       
                           for contourID in removeContourIDList :                   
                               self.clearSliceDataForCrossSliceChange (contourID, ZSliceIndex, Axis = Axis, SliceRange = [firstSliceNumber, LastIndex+1], ContourType = "Human")                   
                           SliceViewObjectsChanged.add (self)
                           #if necessary remove contour handled in def endCrossSliceChange
                       else:
                           for contourID in removeContourIDList :                   
                               crossSliceMap =  self.getSliceMap  (ZSliceIndex, None, NonPerimeterMasksOnly = False, ClipOverlayedObjects = False)                   
                               if Axis == "X" : 
                                   crossSliceMap[firstSliceNumber:LastIndex+1, :] = 0
                               else:
                                   crossSliceMap[:,firstSliceNumber:LastIndex+1] = 0
                               if not np.any (crossSliceMap) :
                                   self.removeSliceContour (contourID, ZSliceIndex)           
                               else:
                                   self.setSliceROIAreaMask (contourID, ZSliceIndex , crossSliceMap, crossSliceMap.shape, "Human")  
       if sliceRemoved :
          self._correctSliceNumber () 
          self._YAxisROIPixMap = None
          self._XAxisROIPixMap = None
       return (len (self._ROIAreaObjectSliceDictionary) + self._DeepGrowAnnotation.getSliceCount ())
   
   
       
       
   def addLargestAreaAtSlicePos (self, contourID, shape, area, ZPos, ContourType) :   
      if (len (area) > 0):
         #self.setObjectChanged () called in two callie functions
         contour = Contour ( contourID, area[0], shape, ContourType)                                                             
         i = 1
         while (i < len (area)) :    
             test = Contour ( contourID, area[i], shape, ContourType)                                                                
             if (test.getArea() > contour.getArea()) :
                 contour = test
             i += 1                                       
         self._setSliceROIContourData (ZPos, contour, shape)                  
         
         
   def getSliceNumberList (self):       
       return copy.copy (self.getSortedKeyList ())
   
   
   def getCoordinate(self):  
       if (self._sliceNumber in self._ROIAreaObjectSliceDictionary) :
           return self._ROIAreaObjectSliceDictionary[self._sliceNumber].getCoordinate ()
       return None
  
   def hasSlices (self) : 
       if (len (self._ROIAreaObjectSliceDictionary) > 0 and self._sliceNumber != None) :
           for slicedata in self._ROIAreaObjectSliceDictionary.values ():
                if slicedata.isSliceAreaDefined ():
                    return True
       return False
       
   def setSliceNumber (self, num) :
       #self.setObjectChanged () Avoid Uncessary save if just slice number is modified
       self._sliceNumber = num
     
   def getContourMapVolume (self, ContourID, shape, mem = None) :              
       memSet = False
       if mem is None :
           mem = np.zeros ((shape), dtype=np.uint8)
       else:
           mem[...] = 0
       for sliceNumber in self._ROIAreaObjectSliceDictionary.keys () :
           singleSlice = self._ROIAreaObjectSliceDictionary[sliceNumber]
           if (singleSlice.hasContour (ContourID)) :
               contour = singleSlice.getContour (ContourID)
               area = contour.getContourArea ()                              
               mem[:,:,sliceNumber] = area > 0
               memSet = True
       return memSet, mem    
               
   def getContourMap (self, ContourID, SliceNumber, shape) :
       if (SliceNumber in  self._ROIAreaObjectSliceDictionary) :
           singleSlice = self._ROIAreaObjectSliceDictionary[SliceNumber]
           if (singleSlice.hasContour (ContourID)) :
               contour = singleSlice.getContour (ContourID)
               return contour.getContourArea ()               
       return np.zeros (shape) 
   
   def getSliceMap  (self, SliceNumber, shape, NonPerimeterMasksOnly = False, ClipOverlayedObjects = False) :
       if (SliceNumber in  self._ROIAreaObjectSliceDictionary) :
           singleSlice = self._ROIAreaObjectSliceDictionary[SliceNumber]
           sliceArea = singleSlice.getSliceArea (NonPerimeterMasksOnly = NonPerimeterMasksOnly)
           if (not ClipOverlayedObjects) :
               return sliceArea
           clipMask = self._getObjClipMask ("Z", SliceNumber)
           if (clipMask is not None) :
               sliceArea[clipMask > 0] = 0
           return sliceArea
       if shape is None :
           return None
       return np.zeros (shape, dtype=np.uint8) 
    
   def getSliceMaxHumanEditedContourSnapCount (self, SliceNumber) :
       if (SliceNumber in  self._ROIAreaObjectSliceDictionary) :
           singleSlice = self._ROIAreaObjectSliceDictionary[SliceNumber]
           return singleSlice.getMaxHumanEditedContourSnapCount ()
       return 0
   
   def hasContourWithPerimeterDefinitions (self) :
       for singleSlice in self._ROIAreaObjectSliceDictionary.values () :
           if singleSlice.sliceContainsPerimeterContours () :
               return True
       return False
   
   def hasContoursWithOnlyAreaDefinitions (self) :
       return not self.hasContourWithPerimeterDefinitions ()
   
   @staticmethod
   def _convertPerimeterToAreaMask (tpl) :
           newContourID, singleSlice,  QApp = tpl
           if (QApp is not None) :
               QApp.processEvents ()           
           singleSliceAreaMap = singleSlice.getSliceArea ()     
           if (singleSlice.doesSliceContainHumanEditedContour ()) :
               contourType = "Human"
           else:
               contourType = "Computer"
           HumanEditedContourSnapCount = singleSlice.getMaxHumanEditedContourSnapCount ()         
           singleSlice.deleteAllContourID ()
           singleSlice.addContour (Contour (contourID= newContourID, shape= singleSlice.getShape (), contourType = contourType, SnappedHumanEditedSliceCount= HumanEditedContourSnapCount,contourAreaMask =singleSliceAreaMap), DelayInitLineCross = False)
   
   def convertPerimeterToAreaMask (self,  QApp = None) :
       self.setObjectChanged ()
       previouslyAllocatedIDlist = copy.copy (self.getContourIDManger ().getAllocatedIDList ())
       newContourID = self.getContourIDManger ().allocateID ()
       sliceList = []
       for  singleSliceNumber in  self.getSortedKeyList () :
           sliceList.append ((newContourID, self._getSliceCopy (singleSliceNumber),  QApp))
       result = list (map (MultiROIAreaObject._convertPerimeterToAreaMask, sliceList))
       for cID in previouslyAllocatedIDlist :
            self.getContourIDManger ().deleteID (cID)
       return result
   
   def removeNonPerimeterMasks (self) :
       self.setObjectChanged ()
       contourIDRemoved = []
       for sliceIndex in self.getSortedKeyList () :
            slices = self._getSliceCopy (sliceIndex)
            contourIDRemoved += slices.removeNonPerimeterContours ()           
       contourIDManager = self.getContourIDManger ()
       for contourID in set(contourIDRemoved) :   
           contourIDManager.deleteID (contourID)
   
       
   def getNonPerimeterMaskVolume (self, shape, volumeMem = None, ClipOverlayedObjects = False) :
       if (volumeMem is None) :
           volume = np.zeros (shape, dtype=np.int) 
       else:
           volume = volumeMem
           volume[...] = 0
           
       sliceshape = (shape[0], shape[1])
       for SliceNumber in self.getSortedKeyList () :
           if (SliceNumber >= 0 and SliceNumber < shape[2]) :
               area = self.getSliceMap (SliceNumber, sliceshape, NonPerimeterMasksOnly = True, ClipOverlayedObjects = ClipOverlayedObjects)           
               volume[:,:, SliceNumber] = area                 
       return volume.astype (np.int)
   
   def getMaskedVolumeFromMemCache (self, SharedROIMask = None):
       vc = self._getSliceMaskVolumeCache (SharedROIMask = SharedROIMask)
       return vc.getROIVolume (SharedROIMask)     
       
   def getMaskedVolume (self, shape, volumeMem = None, ClipOverlayedObjects = False, ClipToBoundBox = False, UseVolumeCache = False, SharedROIMask = None) :       
       if (UseVolumeCache and self.isSliceMaskVolumeCacheSet () and self._volumeShape is not None and (SharedROIMask is not None or not ClipOverlayedObjects)) : 
           ROI3DVolume = self.getMaskedVolumeFromMemCache (SharedROIMask)                 
       else:
           ROI3DVolume = None 
           
       if (volumeMem is None or volumeMem.shape != shape) :
           volume = np.zeros (shape, dtype=np.int) 
       else:
           volume = volumeMem           
           volume[...] = 0
           
       sliceshape = (shape[0], shape[1])
       
       if (ClipToBoundBox) :
           zboxMin = min (max (self._ZBoundingBox[0], 0), shape[2]-1)
           zboxMax = min (max (self._ZBoundingBox[1], 0), shape[2]-1)
           xboxMin = min (max (self._XBoundingBox[0], 0), shape[0]-1)
           xboxMax = min (max (self._XBoundingBox[1], 0), shape[0]-1)
           yboxMin = min (max (self._YBoundingBox[0], 0), shape[1]-1)
           yboxMax = min (max (self._YBoundingBox[1], 0), shape[1]-1)
       
       for SliceNumber in self.getSortedKeyList () :
           if (SliceNumber >= 0 and SliceNumber < shape[2]) :
               if (ClipToBoundBox and (SliceNumber < zboxMin or SliceNumber > zboxMax)) :
                   continue
               if ROI3DVolume is None :
                   area = self.getSliceMap (SliceNumber, sliceshape, ClipOverlayedObjects = ClipOverlayedObjects)           
                   if (ClipToBoundBox) :
                       volume[xboxMin:xboxMax+1,yboxMin:yboxMax+1, SliceNumber] = area[xboxMin:xboxMax+1,yboxMin:yboxMax+1]
                   else:
                       volume[:,:, SliceNumber] = area[:,:]                            
               else:                   
                   if (ClipToBoundBox) :
                       volume[xboxMin:xboxMax+1,yboxMin:yboxMax+1, SliceNumber] = ROI3DVolume[xboxMin:xboxMax+1,yboxMin:yboxMax+1,SliceNumber]
                   else:
                       volume[:,:, SliceNumber] = ROI3DVolume[:,:,SliceNumber]                            
               
       return volume.astype (np.int)
       
   
   def isInAxisList (self, cord, axis, SharedROIMask = None):    
      if (axis == 'Z') :
          zC = cord.getZCoordinate ()
          return (zC in self._ROIAreaObjectSliceDictionary)  
      if (axis == 'X')  :
          xC = cord.getXCoordinate ()
          return self.hasAnnotationOnXAxis (xC, SharedROIMask = SharedROIMask)
      elif (axis == 'Y')  :
          yC = cord.getYCoordinate ()
          return self.hasAnnotationOnYAxis (yC, SharedROIMask = SharedROIMask)
   
   
   def _getAxisBoundingBox (self,  niftislicewidget = None, sliceAxis = None) :
       if niftislicewidget is not None :
           sliceAxis = niftislicewidget.getSliceAxis ()
       if sliceAxis == 'Z' :    
           if niftislicewidget is not None and niftislicewidget.areAxisProjectionControlsEnabledForAxis () :
               axisProjected = niftislicewidget.getAxisProjectionControls ()
               niftiVolume = niftislicewidget.getNIfTIVolume ()
               coord = Coordinate ()
               coord.setCoordinate ([self._XBoundingBox[0],self._YBoundingBox[0],0])
               x1,y1 = axisProjected.getProjectedPlaneXYCoordinateFromWC (coord, niftiVolume)
               coord.setCoordinate ([self._XBoundingBox[1],self._YBoundingBox[1],0])
               x2,y2 = axisProjected.getProjectedPlaneXYCoordinateFromWC (coord, niftiVolume)
               return ((np.array([x1,x2])).astype(np.int),(np.array([y1,y2])).astype (np.int)) 
           else:
               scaledXBox = self._XBoundingBox 
               scaledYBox = self._YBoundingBox  
       elif sliceAxis == 'X' :    
           scaledXBox = self._YBoundingBox
           scaledYBox = self._ZBoundingBox
       elif sliceAxis == 'Y' :    
           scaledXBox = self._XBoundingBox 
           scaledYBox = self._ZBoundingBox 
       return (np.copy (scaledXBox), np.copy (scaledYBox))

   def getShape (self) :        
       slices = list (self._ROIAreaObjectSliceDictionary.keys ())
       if (len (slices) <= 0) :           
           return ()
       first = slices[0]
       firstslice = self._ROIAreaObjectSliceDictionary[first]
       return firstslice.getShape ()       
       
   @staticmethod
   def _getBBoxMask (shape, dim1Box, dim2Box):
       mask = np.zeros (shape, dtype=np.uint8)
       x1, x2 = dim1Box[0], dim1Box[1]
       y1, y2 = dim2Box[0], dim2Box[1]
       maxXDim = shape[0]-1
       maxYDim = shape[1]-1
       x1 = int(min(max(x1,0), maxXDim))
       x2 = int(min(max(x2,0), maxXDim))
       y1 = int(min(max(y1,0),maxYDim))
       y2 = int(min(max(y2,0), maxYDim))
       x2 += 1
       y2 += 1          
       mask[x1:x2, y1:y2] = 1
       return mask
   
   def  getXYBoundingBoxMask (self, shape = None, ReturnAxisProjectedMask = False, niftislicewidget = None) :
       if shape is None :
           slices = list (self._ROIAreaObjectSliceDictionary.keys ())
           if (len (slices) <= 0) :           
               return None     
           first = slices[0]
           firstslice = self._ROIAreaObjectSliceDictionary[first]
           shape = firstslice.getShape ()      
       elif len (shape) == 3 :
           shape = (shape[0], shape[1])              
       if ReturnAxisProjectedMask and niftislicewidget is not None and niftislicewidget.getSliceAxis() == "Z" :
           xbox, ybox = self._getAxisBoundingBox (niftislicewidget)
           return MultiROIAreaObject._getBBoxMask (shape, xbox, ybox)
       else:
           return MultiROIAreaObject._getBBoxMask (shape, self._XBoundingBox, self._YBoundingBox)
   
   def getXZBoundingBoxMask (self, shape) :
       if len (shape) == 3 :
           shape = (shape[0], shape[2])                 
       return MultiROIAreaObject._getBBoxMask (shape, self._XBoundingBox, self._ZBoundingBox)
     
   def getYZBoundingBoxMask (self, shape) :    
       if len (shape) == 3 :
           shape = (shape[1], shape[2])              
       return MultiROIAreaObject._getBBoxMask (shape, self._YBoundingBox, self._ZBoundingBox)
       
   def _getScreenAxisBoundingBox (self, niftislicewidget) :
       scaledXBox, scaledYBox = self._getAxisBoundingBox (niftislicewidget)
       scaledXBox[0], scaledYBox[0] = niftislicewidget.transformPointFromImageToScreen (scaledXBox[0], scaledYBox[0])   
       scaledXBox[1], scaledYBox[1] = niftislicewidget.transformPointFromImageToScreen (scaledXBox[1], scaledYBox[1])   
       
       scaledXBox[0], scaledXBox[1] = (min (scaledXBox[0], scaledXBox[1]), max (scaledXBox[0], scaledXBox[1]))
       scaledYBox[0], scaledYBox[1] = (min (scaledYBox[0], scaledYBox[1]), max (scaledYBox[0], scaledYBox[1]))
       
       return (scaledXBox, scaledYBox)
     
   def _getControlPointList (self,scaledXBox , scaledYBox) :
       ControPointList= []      
       ControPointList.append (BoundingBoxControlPoint ( 0,0, scaledXBox, scaledYBox))       
       ControPointList.append (BoundingBoxControlPoint ( 1,0, scaledXBox, scaledYBox))       
       ControPointList.append (BoundingBoxControlPoint ( 0,1, scaledXBox, scaledYBox))       
       ControPointList.append (BoundingBoxControlPoint ( 1,1, scaledXBox, scaledYBox))       
       ControPointList.append (BoundingBoxControlPoint ( 0, 0.5, scaledXBox, scaledYBox))       
       ControPointList.append (BoundingBoxControlPoint ( 1, 0.5, scaledXBox, scaledYBox))        
       ControPointList.append (BoundingBoxControlPoint ( 0.5, 0, scaledXBox, scaledYBox))       
       ControPointList.append (BoundingBoxControlPoint ( 0.5, 1, scaledXBox, scaledYBox))    
       return ControPointList

   def _isSelected (self):
       return self._selected 
   
   def setSelected (self, selected):       
       #self.setObjectChanged ()   Avoid Unecessary Save if just selection is modified
       self._selected = selected   
       
   def isSliceContoured (self, sliceNumber, ContourIDLst = None) :
        if (sliceNumber in  self._ROIAreaObjectSliceDictionary) :
            if (ContourIDLst == None) :
                return True
            else:
                for cid in ContourIDLst :
                   if self._ROIAreaObjectSliceDictionary[sliceNumber].hasContour (cid) :
                       return True
        return False
   
   def isSliceRangeContoured (self, sliceRange, ContourIDLst = None) :       
       for sliceNumber in sliceRange : 
           if self.isSliceContoured (sliceNumber, ContourIDLst = ContourIDLst) :
               return True
       return False
      
   
   def _getSliceAxisMask (self, width, height, axis, coordinate, SharedROIMask = None):                         
       volumeMem = self._getSliceMaskVolumeCache (SharedROIMask = SharedROIMask)
       if (volumeMem.isInitalized () and not self.isRunningInLowMemoryMode ()):
           if (axis == 'Y') : 
               sliceBox = self._YBoundingBox
               if (coordinate < sliceBox[0] or coordinate > sliceBox[1]):
                   return np.zeros((volumeMem.shape[0],volumeMem.shape[2]), np.uint8)
               XBox = self._XBoundingBox
               YBox = self._ZBoundingBox
               sliceData = volumeMem.getYAxisSlice (coordinate)
           else:
               sliceBox = self._XBoundingBox
               if (coordinate < sliceBox[0] or coordinate > sliceBox[1]):
                   return np.zeros((volumeMem.shape[1],volumeMem.shape[2]), np.uint8)
               XBox = self._YBoundingBox
               YBox = self._ZBoundingBox    
               sliceData = volumeMem.getXAxisSlice (coordinate)
           mask = np.ones (sliceData.shape, np.uint8)           
           xmin = min (max (0, XBox[0]),sliceData.shape[0] -1)
           xmax = min (sliceData.shape[0]-1, XBox[1])
           ymin = min(max (0, YBox[0]),sliceData.shape[1] -1)
           ymax = min (sliceData.shape[1]-1, YBox[1])
           mask[xmin:xmax+1, ymin:ymax+1] = 0
           sliceData[mask > 0] = 0           
           return sliceData
       
       if (self._cachedAxisMask is None or self._cachedAxisMask.shape != (width, height)) :
           self._cachedAxisMask = np.zeros ((width, height), dtype=np.uint8)
           mask = self._cachedAxisMask
       else:
           mask = self._cachedAxisMask
           mask[...] = 0
       
       if (axis == 'Y') : 
           for sliceNum, slices in self._ROIAreaObjectSliceDictionary.items () :
               if (sliceNum >= 0 and sliceNum <= height):
                   mask[:, sliceNum] = slices.getYAxisPixMapROILineCross (coordinate) 
       else:
           for sliceNum, slices in self._ROIAreaObjectSliceDictionary.items () :
               if (sliceNum >= 0 and sliceNum <= height):
                   mask[:, sliceNum] = slices.getXAxisPixMapROILineCross (coordinate) 
       return mask
       
    
   def getQtMaskPixMap (self, color,niftislicewidget, SharedROIMask = None, DeepGrowAxisMask = None, ClipMask = None) :
       width, height = niftislicewidget.getSliceShape ()  
       mask = self._getSliceAxisMask (width, height, niftislicewidget.getSliceAxis (), niftislicewidget.getSliceNumber (), SharedROIMask = SharedROIMask)
       if ClipMask is not None :
           x1,y1,clipwidth,clipheight = ClipMask
           clipmask =np.zeros (mask.shape,dtype=np.uint8)
           clipmask[x1:x1+clipwidth,y1:y1+clipheight] = 1
           mask = np.bitwise_and (mask,clipmask)
       if DeepGrowAxisMask is not None :
           mask = np.bitwise_or (mask,DeepGrowAxisMask)           
       #mask = np.clip (mask,0, 1)               
       if (self._getQTMaskMem is None or self._getQTMaskMem.shape != (4, width, height)) :           
           self._getQTMaskMem = np.zeros ((4, width, height),  dtype = np.uint8)                                    
       maskRGBA = self._getQTMaskMem                
       """maskRGBA[0,:,:] = color.blue () * mask   #B
       maskRGBA[1,:,:] = color.green () * mask  #G
       maskRGBA[2,:,:] = color.red () * mask  #R
       maskRGBA[3,:,:] = 255 * mask    # Alpha  255 == on"""
       NativeSetMask._setMask (maskRGBA, color.blue (), color.green (), color.red (), mask)
       image =  QtGui.QImage (maskRGBA.tobytes(order="F"), width, height, width * 4, QtGui.QImage.Format_ARGB32)                        
       return QtGui.QPixmap.fromImage (image)                         
       
    
   def _isCoordinateInsideBB (self, niftislicewidget):
       if (niftislicewidget is None) :
           return False
       coord = niftislicewidget.getCoordinate ()
       if coord is None :
           return False
       Axis = niftislicewidget.getSliceAxis ()
       x,y,z = coord.getCoordinate ()  
       if (Axis == 'X') :           
           xBox = self._XBoundingBox 
           return (x >= xBox[0] and x <= xBox[1])
       elif (Axis == 'Y') :
           yBox = self._YBoundingBox  
           return (y >= yBox[0] and y <= yBox[1])
       elif (Axis == 'Z') :
           zBox = self._ZBoundingBox    
           if not niftislicewidget.areAxisProjectionControlsEnabledForAxis () :
                return (z >= zBox[0] and z <= zBox[1])    
           else:
                xBox = self._XBoundingBox 
                yBox = self._YBoundingBox  
                axisProjectionControls = niftislicewidget.getAxisProjectionControls ()
                cord = Coordinate ()
                minZ = None
                for xp in xBox :
                    for yp in yBox :
                         for zp in zBox :
                             cord.setCoordinate ([xp, yp, zp])      
                             slZ = axisProjectionControls.getProjectedSliceForWorldCoordinate (cord, niftislicewidget.getNIfTIVolume ())
                             if minZ is None :
                                 minZ = slZ
                                 maxZ = slZ
                             elif minZ > slZ :
                                 minZ = slZ
                             elif maxZ < slZ :
                                 maxZ = slZ
                return (z >= minZ and z <= maxZ) 
                
       #else:
       #    return ((x >= xBox[0] and x <= xBox[1]) and (y >= yBox[0] and y <= yBox[1]) and (z >= zBox[0] and z <= zBox[1]))
   

   def getObjClipMaskDictionaryHelper (self,niftishape, Axis, coordinate, SharedROIMask = None) :
       if coordinate < 0 :
           return None 
       if (len (self._ROIAreaObjectSliceDictionary) <= 0) :
           return None 
       xdim, ydim, zdim = niftishape 
       if (Axis == "Z") :
           if (coordinate >= zdim) :
               return None
           if coordinate not in self._ROIAreaObjectSliceDictionary :
               return None
           return  self._ROIAreaObjectSliceDictionary[coordinate].getSliceArea () 
       elif (Axis == "X") :
           if (coordinate >= xdim) :
               return None 
           return self._getSliceAxisMask (ydim, zdim, Axis, coordinate, SharedROIMask = SharedROIMask)
       elif (Axis == "Y") :
           if (coordinate >= ydim) :
               return None 
           return self._getSliceAxisMask (xdim, zdim, Axis, coordinate, SharedROIMask = SharedROIMask)
       else:
           print ("Invalid Axis: %s" % Axis)
           sys.exit ()
   
   def setObjectClipDictionary (self, clipDictionary) :
       #don't think these need to be called as this only called in core ROI loading  and file saving routines, calling set object changed here may actually break file saving mechanism
       #self.setObjectChanged ()  doesn't need to be called, 
       if (clipDictionary is not None) :
           self._objClipDictionary = weakref.ref(clipDictionary)()
       else:
           self._objClipDictionary =  None
           
   def _getObjClipMask (self, Axis, coordinate) :       
       if (self._objClipDictionary is None) :
           return None
       return self._objClipDictionary.getObjectClipMask (Axis, coordinate, self)
   
   def _clearObjClipMask (self,  Axis = None, SliceNumbers = None, SliceNumber = None) :       
       if (self._objClipDictionary is None) :
           return None
       self._objClipDictionary.clearClipMaskVolumeCache ( Axis = Axis, SliceNumbers = SliceNumbers, SliceNumber = SliceNumber)
      
   
   def getOptomizedDrawSingleROIColorizedSlice (self,niftislicewidget, ROIDefs, SelectedObjects, SliceNumber = None, ReturnVolume = False) :
       #blank, mask, ColorTable = obj.           
       memcache = self._getSliceMaskVolumeCache (niftislicewidget.getSharedROIMask ()).getSharedMemCache ()
       if memcache is None or not memcache.isInitalized () :
           self.setVolumeShape (niftislicewidget.getNIfTIVolume().getSliceDimensions(), niftislicewidget.getSharedROIMask ())
           memcache = self._getSliceMaskVolumeCache (niftislicewidget.getSharedROIMask ()).getSharedMemCache ()
       if memcache is None or not memcache.isInitalized () :
           return None, None, None     
       if ReturnVolume :
            return memcache.getRawROIMaskVolume ()     
       sliceAxis = niftislicewidget.getSliceAxis ()
       if SliceNumber is None :
           SliceNumber = niftislicewidget.getSliceNumber ()
       return memcache.getRawROIinAxis (SliceNumber, sliceAxis, ROIDefs, SelectedObjects)       

   
   
     
   def draw(self, qp, niftislicewidget, ROIDefs, SliceDrawn, drawResizeableBB, drawBB, visSettings, drawObj=True, overrideHideROI = False):
       ROIName = self.getName (ROIDefs)
       if ROIDefs.isROIHidden (ROIName) and not overrideHideROI :
           return
      
       imageXBox, imageYBox = self._getAxisBoundingBox (niftislicewidget)       
       scaledXBox, scaledYBox = self._getScreenAxisBoundingBox (niftislicewidget)       
       
       xoffset1, yoffset1 = niftislicewidget.transformPointFromImageToScreen (0, 0)
       xoffset2, yoffset2 = niftislicewidget.transformPointFromImageToScreen (1, 1)
       xoffset = xoffset2 - xoffset1
       yoffset = yoffset2 - yoffset1
       xoffset = max (0, xoffset - 1)
       yoffset = max (0, yoffset - 1)
       scaledXBox[1] += xoffset
       scaledYBox[1] += yoffset       
       
       selectedContours = []
       if (self._isSelected()) :
           selectedContours = self._selectedContourIDList

       sliceAxis = niftislicewidget.getSliceAxis ()

       DeepGrowPointObjectList = []
       if sliceAxis == 'Z' :     
           if self._DeepGrowAnnotation.hasAnnotationsForSlice (SliceDrawn) :     
               DeepGrowPointObjectList = self._DeepGrowAnnotation.getPointObjectInSlice (sliceAxis, SliceDrawn)
               if DeepGrowPointObjectList is None :
                   DeepGrowPointObjectList = []
       if (drawObj) :
           color = ROIDefs.getROIColor (ROIName)
           if sliceAxis == 'Z' :           
               tempPixMapMaskSlice = self.getQTSliceCache ().getTempQtMaskPixMap (niftislicewidget, color)
               if niftislicewidget.areAxisProjectionControlsEnabledForAxis () and niftislicewidget.isNiftiDataLoaded () :
                   imageXBox, imageYBox = None, None 
                   if tempPixMapMaskSlice is not None :     
                       SinglROIAreaObjDef.drawZSlice (tempPixMapMaskSlice, qp, niftislicewidget, imageXBox, imageYBox, self._ZBoundingBox, visSettings)    
                   else:
                       AreaQTPixmapMask = self.getQTSliceCache ().getQtMaskPixMap (niftislicewidget, color, None, self._ROIAreaObjectSliceDictionary, ROIObj = self)
                       SinglROIAreaObjDef.drawZSlice (AreaQTPixmapMask, qp, niftislicewidget, imageXBox, imageYBox, self._ZBoundingBox, visSettings)    
               else:
                   if SliceDrawn in self._ROIAreaObjectSliceDictionary  :
                       contours = self._ROIAreaObjectSliceDictionary[SliceDrawn]
                       
                       if tempPixMapMaskSlice is None :                       
                           AreaQTPixmapMask = self.getQTSliceCache ().getQtMaskPixMap (niftislicewidget, color, contours, self._ROIAreaObjectSliceDictionary,  ROIObj = self)                      
                       else:                       
                           AreaQTPixmapMask = None
                       contours.draw ( AreaQTPixmapMask, tempPixMapMaskSlice, ROIName , qp, niftislicewidget, ROIDefs, SliceDrawn, imageXBox, imageYBox, self._ZBoundingBox, selectedContours, visSettings = visSettings, drawObj = drawObj)
        
                   if len (DeepGrowPointObjectList) > 0 :                       
                       hasclipping = qp.hasClipping()
                       if not hasclipping :
                           qp.setClipping (True)
                       else:
                           currentboundingrect = qp.clipRegion()
                       x1, y1, width, height = niftislicewidget.getClippingView ()
                       qp.setClipRect (x1, y1, width, height)
                       deepGrowQtMaskAnnotation = self._DeepGrowAnnotation.getDeepGrowQTDrawMask (SliceDrawn, sliceAxis, color, niftislicewidget.getNIfTIVolume (), niftislicewidget.getProjectDataset (), ROIDefs.getROINameUID (ROIName))
                       if deepGrowQtMaskAnnotation is not None :
                           niftislicewidget.drawOverlayQTPixmap (qp, deepGrowQtMaskAnnotation)   
                       if not hasclipping :
                          qp.setClipping (False)
                       else:
                          qp.setClipRegion (currentboundingrect)   
           else:                      
                   x1, y1 = niftislicewidget.transformPointFromImageToScreen (imageXBox[0], imageYBox[0])   
                   x2, y2 = niftislicewidget.transformPointFromImageToScreen (imageXBox[1], imageYBox[1])    
                   x2 += xoffset
                   y2 += yoffset                  
                   testsettings = (color.rgba(), niftislicewidget.getSliceNumber ())
                   ClipMask = None
                   #Get Temporary Draw Map
                   drawPixmap = self.getQTSliceCache ().getTempQtMaskPixMap (niftislicewidget, color)               
                   if drawPixmap is None :
                       DeepGrowAxisMask = None
                       if self.hasDeepGrowAnnotations () and (self._DeepGrowAnnotation.hasPerpendicularAxisChanged (sliceAxis) or self._YAxisROIPixMap is None or testsettings != self._YAxisROIDrawSettings) :
                           NIfTIVolume = niftislicewidget.getNIfTIVolume ()          
                           ProjectDataset = niftislicewidget.getProjectDataset ()
                           ROIUID = ROIDefs.getROINameUID (ROIName)
                           DeepGrowAxisMask = self._DeepGrowAnnotation.getPerpendicularAxisMask (niftislicewidget.getSliceNumber (), sliceAxis, NIfTIVolume, ProjectDataset, ROIUID, niftislicewidget.getSliceShape ())
                           self._DeepGrowAnnotation.resetPerpendicularAxisChanged (sliceAxis)
                           ClipMask = (x1, y1, x2 - x1+1, y2 - y1+1)
                           testsettings = tuple (list (testsettings) + list (ClipMask))                       
                       if sliceAxis == 'Y' :               
                           if (self._YAxisROIPixMap is None or testsettings != self._YAxisROIDrawSettings or DeepGrowAxisMask is not None):
                               self._YAxisROIDrawSettings = testsettings                                 
                               self._YAxisROIPixMap = self.getQtMaskPixMap (color, niftislicewidget, SharedROIMask = niftislicewidget.getSharedROIMask (), DeepGrowAxisMask = DeepGrowAxisMask, ClipMask = ClipMask)
                           drawPixmap = self._YAxisROIPixMap
                       else:                       
                           if (self._XAxisROIPixMap is None or testsettings != self._XAxisROIDrawSettings or DeepGrowAxisMask is not None):
                               self._XAxisROIDrawSettings = testsettings                   
                               self._XAxisROIPixMap = self.getQtMaskPixMap (color, niftislicewidget, SharedROIMask = niftislicewidget.getSharedROIMask (), DeepGrowAxisMask = DeepGrowAxisMask, ClipMask = ClipMask)
                           drawPixmap = self._XAxisROIPixMap               
                    
                   
                   if ClipMask is None :
                       hasclipping = qp.hasClipping()
                       if not hasclipping :
                           qp.setClipping (True)
                       else:
                           currentboundingrect = qp.clipRegion()
                       qp.setClipRect (x1, y1, x2 - x1+1, y2 - y1+1)
                   qp.setOpacity(float (visSettings.getUITransparency ())/float(255))                                                               
                   niftislicewidget.drawOverlayQTPixmap (qp, drawPixmap)                  
                   if ClipMask is None :
                       if not hasclipping :
                           qp.setClipping (False)
                       else:
                           qp.setClipRegion (currentboundingrect)
                   
                   
       if drawBB and self._isSelected() :
            if sliceAxis == 'Z' :     
               if len (DeepGrowPointObjectList) > 0  :        
                  hasclipping = qp.hasClipping()
                  if not hasclipping :
                       qp.setClipping (True)
                  else:
                      currentboundingrect = qp.clipRegion()
                  x1, y1, width, height = niftislicewidget.getClippingView ()
                  qp.setClipRect (x1, y1, width, height)   
                  name = self.getName (ROIDefs)
                  color = ROIDefs.getROIColor (name)      
                  for pointObject in DeepGrowPointObjectList :
                      pointObject.draw (qp, SliceDrawn, color, self._DeepGrowAnnotation, niftislicewidget)
                  if not hasclipping :
                      qp.setClipping (False)
                  else:
                      qp.setClipRegion (currentboundingrect)
                          
       if ((drawBB and not drawResizeableBB) or (drawBB and drawResizeableBB and visSettings.getShowROIBoundingBoxInEditMode ())) :
           if (self._isSelected() and self._isCoordinateInsideBB (niftislicewidget)) or (self._isMoving()) :                                    
               qp.setPen ((QtGui.QColor (255,255,255)))
               qp.setBrush (QtGui.QColor (0,0,0,0))                 
               qp.drawRect (scaledXBox[0], scaledYBox[0], scaledXBox[1] - scaledXBox[0] + 1, scaledYBox[1] - scaledYBox[0] + 1)    
               if (drawResizeableBB and not ROIDefs.isROILocked (ROIName)) :
                   controPointList = self._getControlPointList (scaledXBox, scaledYBox)            
                   if (self._SelectedBoundingBoxControlPoint == None or self._SelectedBoundingBoxControlPoint[1].getSliceAxis () != niftislicewidget.getSliceAxis ()) :
                       for  point in  controPointList :
                           point.draw (qp)
                   else:
                       selectedControlPointPosition = (np.array (self._SelectedBoundingBoxControlPoint[2])).astype (np.int)
                       #print("selected Position %d, %d" % (selectedControlPointPosition[0],selectedControlPointPosition[1]))
                       
                       if len (controPointList) > 0 : 
                           selectionthreshold = controPointList[0].getPointSize () ** 2
                           for  i, point in enumerate (controPointList) :            
                             screenPos = point.getPosition ()
                             screenPos = np.array (screenPos).astype (np.int)
                             if np.sum ((selectedControlPointPosition - screenPos) ** 2) <= selectionthreshold :                     
                                 point.drawSelectd (qp)                 
                             else:                     
                                 #print ("Drawing ControlPoint at %d, %d " % (screenPos[0],screenPos[1] ))
                                 point.draw (qp)

   def _isMoving (self) :                     
       return self._isObjMoving 

   def setIsMoving (self, val) :
       self._isObjMoving = val
   

   
       
       
   def isControlPointSelected (self, niftslicewidget):
         #scaledXBox, scaledYBox = self._getAxisBoundingBox (niftslicewidget.getSliceAxis ())
         
         scaledXBox, scaledYBox = self._getScreenAxisBoundingBox (niftslicewidget)   
         xoffset1, yoffset1 = niftslicewidget.transformPointFromImageToScreen (0, 0)
         xoffset2, yoffset2 = niftslicewidget.transformPointFromImageToScreen (1, 1)
         xoffset = xoffset2 - xoffset1
         yoffset = yoffset2 - yoffset1
         xoffset = max (0, xoffset - 1)
         yoffset = max (0, yoffset - 1)
         scaledXBox[1] += xoffset
         scaledYBox[1] += yoffset
         
         controPointList = self._getControlPointList (scaledXBox, scaledYBox)                                          
         for index, point in enumerate(controPointList) :
             #print ("Point %d,%d  Mouse %d, %d" % (point._x, point._y, imageX, imageY))
             if (point.isSelected (niftslicewidget)):
                 #print ("Selected Control Point at %d " % (index))   
                 xImagePos, yImagePos = point.getPosition ()
                 #xControlPointScreenPos, yControlPointScreenPos = niftslicewidget.transformPointFromImageToScreen ( xImagePos, yImagePos)    
                 cpIndex = niftslicewidget.transformControlPointIndexToImage (index)
                 return (True, (cpIndex, niftslicewidget, [int (xImagePos), int (yImagePos)]))                 
         return (False, None)
   
   @staticmethod
   def _updateBoundingBox (boundingBox, pos, value):
        if (pos == 1 or pos == 0) :        
            boundingBox[pos] = value
            minV = min (boundingBox[0], boundingBox[1])
            maxV = max (boundingBox[0], boundingBox[1])
            boundingBox[0] = minV
            boundingBox[1] = maxV
            return value
        else:
            return (boundingBox[0] + boundingBox[1]) / 2

   def isDeepGrowPointSelected (self, niftslicewidget):
         #scaledXBox, scaledYBox = self._getAxisBoundingBox (niftslicewidget.getSliceAxis ())
         """scaledXBox, scaledYBox = self._getScreenAxisBoundingBox (niftslicewidget)   
         xoffset1, yoffset1 = niftslicewidget.transformPointFromImageToScreen (0, 0)
         xoffset2, yoffset2 = niftslicewidget.transformPointFromImageToScreen (1, 1)
         xoffset = xoffset2 - xoffset1
         yoffset = yoffset2 - yoffset1
         xoffset = max (0, xoffset - 1)
         yoffset = max (0, yoffset - 1)
         scaledXBox[1] += xoffset
         scaledYBox[1] += yoffset"""
         
         cord = niftslicewidget.getLastSystemMouseCoordinate ()
         return self._DeepGrowAnnotation.getPointObjectSelection ( (cord[0], cord[1]), niftslicewidget)

   def _DeepGrowObjectChanged (self, DeepGrowObject) :
       self.setObjectChanged ()
       
   def clearSelectedDeepGrowControlPoint (self) :
       self._DeepGrowAnnotation.clearSelectedDeepGrowControlPoint ()
          
   def getSelectedDeepGrowControlPoint (self) :
       return  self._DeepGrowAnnotation.getSelectedAnnotation ()
     
   def createDeepGrowControlPoint (self,cX, cY, niftslicewidget, niftiVolume, Positive):       
       xcord, ycord,zcord = niftslicewidget.convertMouseMoveImageCoordiantesToXYZImageCoordinates (cX, cY)   
   
       cord = Coordinate ()
       cord.setCoordinate ([xcord, ycord, zcord])
       xDim, yDim, zDim = niftiVolume.getSliceDimensions ()
       if (xcord >= 0 and xcord < xDim and ycord >= 0 and ycord < yDim and zcord >= 0 and zcord < zDim) :
           self._DeepGrowAnnotation.createDeepGrowPoint (cord, Radius = 20.0, Sigma = 1.0, Positive = Positive)              
           return True
       return False
   
   def isDeepGrowSlicePredictionClipSliceSet (self, sliceIndex) :
       return self._DeepGrowAnnotation.isDeepGrowSlicePredictionClipSliceSet (sliceIndex)
    
   def setDeepGrowSlicePredictionClipSlice (self, sliceIndex, sliceMask) :
        self._DeepGrowAnnotation.setDeepGrowSlicePredictionClipSlice (sliceIndex, sliceMask)
   
   def hasDeepGrowAnnotations (self) :
       return self._DeepGrowAnnotation.hasAnnotations ()
   
   def clearAllDeepGrowSlices (self) :
       self._DeepGrowAnnotation.clearAllSlices ()
       
       
   def removeDeepGrowPoint (self, point) :
       self._DeepGrowAnnotation.removeDeepGrowPoint (point)
       
   def setSelectedDeepGrowControlPoint (self, SelectedDeepGrowControlPoint, selection) :
       self._DeepGrowAnnotation.setSelectedDeepGrowControlPoint (SelectedDeepGrowControlPoint, selection)       
       
   def setControlPointPosition (self, imageX, imageY, SavePosition = True):        
        ControlPointID = self.getSelectedBoundingBoxControlPoint ()
        index =ControlPointID[0]  
        niftslicewidget = ControlPointID[1]  
        scaledXBox, scaledYBox = self._getAxisBoundingBox (niftslicewidget)
        controPointList =  self._getControlPointList (scaledXBox , scaledYBox)
        sliceAxis = ControlPointID[1].getSliceAxis ()
                
        if (sliceAxis == "Z"):
            if niftslicewidget is not None and niftslicewidget.areAxisProjectionControlsEnabledForAxis () :
                axisProjected = niftslicewidget.getAxisProjectionControls ()
                niftiVolume = niftslicewidget.getNIfTIVolume ()
                planeCord = axisProjected.getWorldCoordinateForPlaneCoordinate ( imageX, imageY, niftslicewidget.getAxisProjectionSliceNumber (), niftiVolume)
                imageX, imageY = planeCord[0], planeCord[1] 
                
            XPos, YPos = controPointList [index].getBoundingBoxIndex ()
            cX = MultiROIAreaObject._updateBoundingBox (self._XBoundingBox, XPos, imageX)
            cY = MultiROIAreaObject._updateBoundingBox (self._YBoundingBox, YPos, imageY)
        elif (sliceAxis == "X"):
            YPos, ZPos = controPointList [index].getBoundingBoxIndex ()
            cX = MultiROIAreaObject._updateBoundingBox (self._YBoundingBox, YPos, imageX)
            cY = MultiROIAreaObject._updateBoundingBox (self._ZBoundingBox, ZPos, imageY)
        elif (sliceAxis == "Y"):
            XPos, ZPos = controPointList [index].getBoundingBoxIndex ()
            cX = MultiROIAreaObject._updateBoundingBox (self._XBoundingBox, XPos, imageX)
            cY = MultiROIAreaObject._updateBoundingBox (self._ZBoundingBox, ZPos, imageY)
            
        screenx, screeny = niftslicewidget.transformPointFromImageToScreen (cX,cY)                        
        if (ControlPointID[2][0] != int (screenx)) :
            ControlPointID[2][0] = int (screenx)
        if (ControlPointID[2][1] != int (screeny)) :
            ControlPointID[2][1] =  int (screeny)
        if (SavePosition) :
            self.setObjectChanged ()
            
   def getBoundingBoxSliceRange (self):                #bugbug easy  neeeds to know slice Axis                 
       return self._ZBoundingBox 
   
   def hasControlPoints (self):
       return True   
   
   def getROIDefIDNumber (self) :
       return self._ROIDefIDNumber 
       
   def setROIIDNumber (self, idNumber):
       #self.setObjectChanged () Unnecessary save.  
       self._sliceMaskVolumeCache.changeROIOID (self._ROIDefIDNumber, idNumber)
       self._ROIDefIDNumber = idNumber
       
       
   def getName (self, RoiDefs) :
       return RoiDefs.getROIIDNumberName (self.getROIDefIDNumber ())
       
   def getROIUIDManager (self) :
       return self._ROIUIDManager
     
   def contourDefinedForSlice (self, contourID, sliceNumber) :
       if (not sliceNumber in self._ROIAreaObjectSliceDictionary) :     
           return False           
       return (self._ROIAreaObjectSliceDictionary[sliceNumber].hasContour (contourID))

   def checkSliceShape (self, volumeShape) :
       if (len (volumeShape) >= 2):
           xdim, ydim = volumeShape[0], volumeShape[1]
           for sliceNumber in list (self._ROIAreaObjectSliceDictionary.keys ()) :
               self._getSliceCopy(sliceNumber).checkSliceShape (xdim, ydim)

   @staticmethod 
   def _clip (pt, minV, maxV) :
        if pt < minV :
            return minV
        if pt > maxV :
            return maxV
        return pt
    
   @staticmethod
   def _moveSlicMem (sliceAreaMask, offset):
       movedData = np.zeros (sliceAreaMask.shape,dtype = sliceAreaMask.dtype)
       xdest = np.array ([0, sliceAreaMask.shape[0]])
       ydest = np.array ([0, sliceAreaMask.shape[1]])
       xdest += offset[0]
       ydest += offset[1]
       np.clip (xdest, 0, sliceAreaMask.shape[0], out = xdest)
       np.clip (ydest, 0, sliceAreaMask.shape[1], out = ydest)
       xsrc = xdest - offset[0]
       ysrc = ydest - offset[1]
       movedData[xdest[0]:xdest[1], ydest[0]:ydest[1]] = sliceAreaMask[xsrc[0]:xsrc[1], ysrc[0]:ysrc[1]]
       return movedData
       
   @staticmethod
   def _moveSliceXY (_slice, contourId, offset ):
       contour = _slice.getContour (contourId)
       sliceShape   = _slice.getShape ()     
       if contour.isHumanEditedContour () :
           contourType = "Human"
       else:
           contourType = "Computer"                       
       if (contour.isContourDefinedByPerimeter ()) :
           contour = Contour ( contourId, contour.getCoordinates ().T + offset, sliceShape, contourType)     
       else:
           movedData = MultiROIAreaObject._moveSlicMem (_slice.getSliceArea (), offset)
           contour = Contour ( contourId, None, sliceShape, contourType, contourAreaMask = movedData)                  
       _slice.addContour (contour)
         
   @staticmethod
   def _clipMove(dx, boundingBox, volumeShapeDim) :
       np.clip (boundingBox, 0,  volumeShapeDim - 1, out = boundingBox)
       #boundingBox[0] = MultiROIAreaObject._clip (boundingBox[0],0, volumeShapeDim - 1)
       #boundingBox[1] = MultiROIAreaObject._clip (boundingBox[1],0, volumeShapeDim - 1)
       if (dx < 0):
           pt = boundingBox[0]
           newPt = MultiROIAreaObject._clip (pt+dx, 0, volumeShapeDim-1)
           dx = newPt - pt       
       elif (dx > 0):
           pt = boundingBox[1]
           newPt = MultiROIAreaObject._clip (pt+dx, 0, volumeShapeDim-1)
           dx = newPt - pt 
       return dx
   
   def moveContours (self, contourIDLst, dx, dy, dz, volumeShape, moveMemory, SliceView = None, ROIDictionary = None, BaseCoordinate = None, MousePos = None) :                    
       self.setObjectChanged ()
       if moveMemory is None :
           moveMemory = {}           
           SliceData = []
           for sliceIndex in self.getSortedKeyList () :   
               sliceData = self._ROIAreaObjectSliceDictionary[sliceIndex].copy (None)  # make a true copy of the data detached from main volume
               sliceData.initalizeContourAreaFromContourIfNone ()
               SliceData.append (sliceData)
           moveMemory["SliceData"] = SliceData
           if SliceView is not None and SliceView.areAxisProjectionControlsEnabledForAxis () and ROIDictionary is not None :               
               sliceAxis = SliceView.getAxisProjectionControls().getROIProjectionSliceAxis (SliceView.getNIfTIVolume (), ROIDictionary, SliceView.getCoordinate ())               
               ROIIDNumber = self.getROIDefIDNumber ()               
               SliceDictionary = {}
               ProjectedAxisOffset = SliceView.getSliceNumber ()
               for sliceIndex, objectList in sliceAxis.items () :
                   if ROIIDNumber in objectList :
                       index = sliceIndex- ProjectedAxisOffset
                       SliceDictionary[index]=  self.getSliceViewObjectMask (SliceView, SliceNumber = sliceIndex, ClipOverlayingROI = False, DisableAxisProjection = False)                                           
               moveMemory["SliceProjection"] = SliceDictionary      
               moveMemory["Offset"]  = (0,0,0)
               
               moveMemory["BaseCord"] =  SliceView.getCoordinate ()
               moveMemory["BaseCordOffset"] = SliceView.getAxisProjectionControls().getProjectedPlaneXYCoordinateFromWC  (moveMemory["BaseCord"], SliceView.getNIfTIVolume ())
               moveMemory["BaseMouseOffset"] = MousePos
                          
       dx = MultiROIAreaObject._clipMove (dx, self._XBoundingBox, volumeShape[0])
       dy = MultiROIAreaObject._clipMove (dy, self._YBoundingBox, volumeShape[1])
       dz = MultiROIAreaObject._clipMove (dz, self._ZBoundingBox, volumeShape[2])
                    
       ROIIDNumber = self.getROIDefIDNumber ()
       if (dx != 0 or dy != 0 or dz != 0) :                          
           if SliceView is not None and SliceView.areAxisProjectionControlsEnabledForAxis () and ROIDictionary is not None :                                                                    
               slicePadding = []      
               mx, my, mz = moveMemory["Offset"] 
               mx += dx
               my += dy 
               mz += dz
               
               for sliceIndex in list (self._ROIAreaObjectSliceDictionary.keys ()) :
                   self.removeSlice (sliceIndex, RemoveAllSliceContours = True, Axis="Z", SharedROIMask = ROIDictionary.getSharedVolumeCacheMem (), SliceViewObjectsChanged = None, RemoveProjectedSlices = False, SliceView = None)
               SliceView.getAxisProjectionControls().invalidateObjectSliceCache (ROIIDNumber = ROIIDNumber)
               moveMemory["Offset"]  = (mx, my, mz)
              
               
               if moveMemory["BaseMouseOffset"] is not None and MousePos is not None :
                   x1, y1 = moveMemory["BaseMouseOffset"]
                   x2, y2 = MousePos
                   mx = x2 - x1
                   my = y2 - y1
                   dz = 0
               else: 
                   cord = moveMemory["BaseCord"].copy()
                   cord.setXCoordinate (cord.getXCoordinate() + mx)
                   cord.setYCoordinate (cord.getYCoordinate() + my)
                   #cord.setZCoordinate (cord.getZCoordinate() + mz)
                   x2,y2 = SliceView.getAxisProjectionControls().getProjectedPlaneXYCoordinateFromWC  (cord, SliceView.getNIfTIVolume ())
                   x1, y1 = moveMemory["BaseCordOffset"]
                   print (("Projected Cord ", x2-x1,y2-y1, mx, my, mz))
                   mx = x2 - x1
                   my = y2 - y1
               if mx < 0 :                            
                   slicePadding.append ([0,-mx])
               else:
                   slicePadding.append ([mx,0])
               if my < 0 :                            
                   slicePadding.append ([0,-my])
               else:
                   slicePadding.append ([my,0])               
               #AxisOffset = SliceView.getAxisProjectionControls().getProjectionAxisZeroOffset ()
               coord = SliceView.getCoordinate ().getCoordinate () 
               coordinate = Coordinate ()
               coordinate.setCoordinate ([coord[0] + dx, coord[1] + dy, coord[2] + dz])
               newProjectionOffset = SliceView.getSliceNumber () + dz
               IDList = self.getContourIDManger ().getAllocatedIDList ()
               if len (IDList) > 0 :
                   contourId = IDList[0]
               else:
                   contourId = self.getContourIDManger ().allocateID()
               for sliceIndex, Segmentation in moveMemory["SliceProjection"].items () :                                  
                    newSliceIndex = sliceIndex + newProjectionOffset                  
                    Segmentation = np.pad (Segmentation, slicePadding, mode="constant", constant_values= 0)
                    if mx < 0 :                            
                        if my < 0 :                            
                            Segmentation = Segmentation[-mx:,-my:]
                        else:
                            Segmentation = Segmentation[-mx:,:Segmentation.shape[1]-my]
                    else:                        
                        if my < 0 :                            
                            Segmentation = Segmentation[:Segmentation.shape[0]-mx,-my:]
                        else:
                            Segmentation = Segmentation[:Segmentation.shape[0]-mx,:Segmentation.shape[1]-my]                                          
                    self.setSliceViewObjectMask (SliceView,  contourId, Segmentation, newSliceIndex, Segmentation, set (), ContourType = "Human")# Coordinate = coordinate) 
               self.endCrossSliceChange () 
           else:                   
               for contourId in contourIDLst :                                                               
                   slicelist = []
                   nextSliceIndexList = []
                   for _slice in moveMemory["SliceData"] :                                                                                
                       if (_slice.hasContour (contourId)) :                                              
                           slicelist.append (_slice)                  
                           if (dz != 0) :
                               nextSliceIndexList.append (_slice.getSliceNumber () + dz)
                   
                   if (dz != 0) :
                       for _slice in slicelist :                                        
                           if (_slice.getSliceNumber () not in nextSliceIndexList) :
                               self.removeSliceContour (contourId, _slice.getSliceNumber())
                                                               
                   for _slice in slicelist :                   
                       contour = _slice.getContour (contourId)
                       sliceIndex = _slice.getSliceNumber()
                       sliceShape   = _slice.getShape ()                                
                       if contour.isHumanEditedContour () :
                           contourType = "Human"
                       else:
                           contourType = "Computer"
                       MultiROIAreaObject._moveSliceXY (_slice, contourId, [dx, dy])
                       if (contour.isContourDefinedByPerimeter ()) :
                           self.setSliceROIPerimeter (contourId, sliceIndex + dz, _slice.getContour (contourId).getCoordinates ().T, sliceShape, contourType) 
                       else:
                          self.setSliceROIAreaMask (contourId, sliceIndex + dz, _slice.getSliceArea (DoNotCopy = True), sliceShape, contourType)     
                       _slice.setSliceNumber (sliceIndex + dz)
                       if (sliceIndex + dz >= 0 and sliceIndex + dz < volumeShape[2]) :
                            self._sliceNumber = sliceIndex + dz                         
           
           self._correctSliceNumber () # if slice number no longer corresponds to valid slice 
       
           if (dz != 0) :
              self._ZBoundingBox[0] =  self._ZBoundingBox[0] + dz 
              self._ZBoundingBox[1] =  self._ZBoundingBox[1] + dz      
           if  (dx != 0) :
               self._XBoundingBox[0] =  self._XBoundingBox[0] + dx 
               self._XBoundingBox[1] =  self._XBoundingBox[1] + dx                               
           if (dy != 0) :
               self._YBoundingBox[0] =  self._YBoundingBox[0] + dy  
               self._YBoundingBox[1] =  self._YBoundingBox[1] + dy                        
       return (dx, dy, dz, moveMemory)     
    
   def reorientObjectToMatchVolume (self, niftiVolume, sourceSliceAxis, QApp = None, ClipSliceMask = True) :     
     shape = niftiVolume.getVolumeShapeinOrientation  (sourceSliceAxis)     
     self._clearVolumeSliceCache (ClipSliceMask = ClipSliceMask)     
     if len (self._ROIAreaObjectSliceDictionary) > 0 :   
         self.setObjectChanged ()
         self._YAxisROIPixMap = None
         self._XAxisROIPixMap = None

         sliceVolume = self.getMaskedVolume (shape, volumeMem = None)   
         sliceVolume = niftiVolume.reorientVolumeToMatchOrientation (sliceVolume, sourceSliceAxis)
         self.setVolumeShape (sliceVolume.shape, None, ClipSliceMask = ClipSliceMask)
        
         coordinate1 = (self._XBoundingBox[0], self._YBoundingBox[0], self._ZBoundingBox[0])
         coordinate2 = (self._XBoundingBox[1], self._YBoundingBox[1], self._ZBoundingBox[1])
         coordinate1 = niftiVolume.reorientPointToMatchVolumeOrientation (coordinate1, sourceSliceAxis)
         coordinate2 = niftiVolume.reorientPointToMatchVolumeOrientation (coordinate2, sourceSliceAxis)
         self._XBoundingBox[0], self._YBoundingBox[0], self._ZBoundingBox[0] = (min (coordinate1[0], coordinate2[0]), min (coordinate1[1], coordinate2[1]), min (coordinate1[2], coordinate2[2]))
         self._XBoundingBox[1], self._YBoundingBox[1], self._ZBoundingBox[1] = (max (coordinate1[0], coordinate2[0]), max (coordinate1[1], coordinate2[1]), max (coordinate1[2], coordinate2[2]))
        
         _ , _, newSliceNumer = niftiVolume.reorientPointToMatchVolumeOrientation ((0,0,self._sliceNumber), sourceSliceAxis)
        
         IDList = self.getContourIDManger ().getAllocatedIDList ()
         if len (IDList) > 0 :
            contourID = IDList[0]
         else:
            contourID = self.getContourIDManger ().allocateID()
                                   
         self._ROIAreaObjectSliceDictionary = {}
         self._clearSortedKeyList ()
         ContourType = "Human"
         for sliceNumber in range (sliceVolume.shape[2]):
            if (QApp is not None) :
                QApp.processEvents()
            AreaMask = sliceVolume[:,:,sliceNumber]
            if np.any (AreaMask) :
                sliceShape = (sliceVolume.shape[0], sliceVolume.shape[1])
                contour = Contour ( contourID, None, sliceShape, ContourType, SnappedHumanEditedSliceCount= 0, contourAreaMask = AreaMask)                          
                self._ROIAreaObjectSliceDictionary[sliceNumber] = SinglROIAreaObjDef (sliceNumber, contour, sliceShape, MultiROIAreaObjectParent = self, DelayInitLineCross = False)  
                self._clearSortedKeyList ()
         self._sliceNumber = newSliceNumer   
         self._volumeShape = copy.copy (sliceVolume.shape)
         self._sliceMaskVolumeCache.clearVolume ()         
   
   def _getFileObjectCache (self):
       return self._fobjCache 
   
   def _setFileObjectCache (self, fobj, SetWriteCache = True) :
       if (fobj is not None and SetWriteCache) :
           fulltext = fobj.getFileObjectTxt ()
           fobj.setWriteCache (fulltext)
       self._fobjCache = fobj
       
   def setObjectChanged (self) :
       self._setFileObjectCache (None)
       self._setObjectUndoChanged ()
       
   def isChanged (self) :       
       return self._getFileObjectCache () is None
   
   def _setObjectUndoChanged (self) :
       self._UndoChanged = True

   def resetObjectUndoChanged (self) :
       self._UndoChanged = False
       
   def isObjectUndoChanged (self) :
        return self._UndoChanged
      

   
class ROIPointObject :   
   def __init__ (self, cord = None, finterface = None):
      self.resetObjectUndoChanged ()
      self.setObjectChanged ()
      self._RegistrationLog = ANTSRegistrationLog (self)
                 
      self._userChangeLog = ()
      self._ROIUIDManager = ROIUIDManager ()
      
      self._ROIDefIDNumber = None
      self._pointDictionary = {}
      self._pointParameters = {}
      self._pointIDManger = ContourIDManger (self)
      self._selectedContourIDList = []
      self._LastModifiedPointID  = -1
      self._selected = False
      self._PointSize = 8      
      self._clearPointAxisCache ()
      
      if (cord != None) :
          PointId = self._pointIDManger.allocateID ()  
          self.setPoint (PointId, cord)          
          
      elif (finterface is not None) :                            
          self._RegistrationLog.loadFromFile (FileHeader = finterface)
          
          if (finterface.hasParameter ("ROIDefUIDObj")):
              self._ROIUIDManager.initFromTuple (finterface.getParameter ("ROIDefUIDObj"))

          
          if (finterface.hasParameter ("PointSize")):
              self._PointSize = finterface.getParameter ("PointSize")
              
          if (finterface.hasParameter ("UserChangeLog")):
              self._userChangeLog = finterface.getParameter ("UserChangeLog")
              
          if (not finterface.hasParameter ("PointCount")) :
              coordinate = Coordinate ()
              coordinate.setCoordinate (finterface.getParameter ("Coordinate"))          
              PointId = self._pointIDManger.allocateID ()          
              self.setPoint (PointId, coordinate)                    
              if (finterface.hasParameter ("Selected")):
                 self._selected = finterface.getParameter ("Selected")
          else:              
              self._selected = finterface.getParameter ("Selected")
              self._LastModifiedPointID = finterface.getParameter ("LastModifiedPointID")
              count = finterface.getParameter ("PointCount")
              self._pointIDManger.load (finterface)              
              for pctr in range (count) :
                  fobj = finterface.getFileObject ("PT" + str (pctr + 1))
                  pointID = fobj.getParameter ("PointID")                  
                  coordinate = Coordinate ()
                  coordinate.setCoordinate (fobj.getParameter ("Coordinate"))          
                  self._pointDictionary[pointID] = coordinate 
                  if fobj.hasParameter ("HumanEdited") :
                      self.setPointProperty (pointID, "HumanEdited", fobj.getParameter ("HumanEdited") )
                  else:
                      self.setPointProperty (pointID, "HumanEdited", True )
          self._setFileObjectCache (finterface, SetWriteCache = False)
   
   def setROIDefUIDManager (self, ROIUIDManager) :
       self._ROIUIDManager = ROIUIDManager

   def resetCompressionWaitIndicator (self):
       return
       
   def ifPossibleCompressROIMemory (self) :    
       return False
   
   def clear (self) :
      self.setObjectChanged ()
      self._ROIUIDManager = ROIUIDManager ()
      self._RegistrationLog = ANTSRegistrationLog (self)          
      self._userChangeLog = ()
      self._ROIDefIDNumber = None
      self._pointDictionary = {}
      self._pointParameters = {}
      self._pointIDManger = ContourIDManger (self)
      self._selectedContourIDList = []
      self._LastModifiedPointID  = -1
      self._selected = False
      self._PointSize = 8
      self._clearPointAxisCache ()
     
   def _clearPointAxisCache (self) :
       self._pointAxisCache = {}
       
   def setPointProperty  (self, pointID, Name, value) :
        self.setObjectChanged ()
        if pointID not in self._pointParameters :
            self._pointParameters[pointID] = {}
        self._pointParameters[pointID][Name] = value
   
   def deletePointProperty (self, PointID) :
       if (PointID in self._pointParameters) :
           self.setObjectChanged ()
           del self._pointParameters[PointID]           
       
   def getPointPropety  (self, pointID, Name) :
        if pointID not in self._pointParameters :
            return None
        pIDDictoinary = self._pointParameters[pointID]
        if Name not in pIDDictoinary :
            return None
        return pIDDictoinary[Name]
   
   def isPointPropetyDefined  (self, pointID, Name) : 
        if pointID not in self._pointParameters :
            return False
        pIDDictoinary = self._pointParameters[pointID]
        if Name not in pIDDictoinary :
            return False
        return True
   
   def isPointHumanEdited (self, pointID) :
       if (not self.isPointPropetyDefined (pointID, "HumanEdited")) :
           print ("Error Point human edited status not defined")
           return True
       return self.getPointPropety (pointID, "HumanEdited")
   
   def setPointHumanEdited (self, pointID, value) :
       self.setObjectChanged ()
       self.setPointProperty (pointID, "HumanEdited", value)
       
   def getANTSRegistrationLog (self) :
       return self._RegistrationLog
     
   @staticmethod
   def getTempFileName (tempfiledir = None, Name = ".nii.gz"):
        pointfileinputFile = tempfile.mkstemp (suffix= "_"+str (uuid.uuid1 ())+Name, dir = tempfiledir)                
        pointfileinputFileName = pointfileinputFile[1]
        os.close (pointfileinputFile[0])
        os.remove (pointfileinputFileName)  
        return pointfileinputFileName
    
   def applyAntsTransformToROI (self, sourceVolumeDescription, destVolumeDescription, ANTS_Transformations, tempfiledir = None, NumThreads = 10, ClipPointsToShape=False) :                
        """
            ANTS ApplyTransformsToPoints takes point coordinates in LPS coordinates and applies the ants transformation to them.  As a result of this
            given that points are stored interally in voxel coordinates.  voxel coordinates must be transformed to LPS and LPS must be transformed back
            to voxel coordinates.  This transformation is further complicated becouse the source and destination images are different.
            
            #the following code converts program voxel coordinates back to disk image voxel space
            #
            if (sourceVolumeDescription.getParameter ("FlipYAxis")) :            
                y = shape[1] - y 
            z = shape[2] - z 
            
            # The code below applys the required transform to move voxels -> RAS coordinates (nibabel coordinate system)
            pt = np.array ([x,y,z,1.0],dtype=float)            
            RAS =  np.matmul (affin, pt.T)   
            
            #Ras are then transfromed to LPS
            
            writeLPSX = float (-RAS[0])
            writeLPSY = float (-RAS[1])
            writeLPSZ = float (RAS[2]) 
            
            Following ANTS transformation everything is revered with the destination affin transformation and size set to the target image parameters.
            
        """
        self.setObjectChanged ()
        shape = sourceVolumeDescription.getParameter ("VolumeDimension")
        counter = 1            
        csvFileIn = ROIPointObject.getTempFileName (tempfiledir = tempfiledir, Name="In.csv")    
        csvOut = open (csvFileIn, "wt")
        
        csvFileOut = ROIPointObject.getTempFileName (tempfiledir = tempfiledir, Name="Out.csv")    
        affin = sourceVolumeDescription.getParameter ("Affine")        
        csvOut.write ("XPt, YPt, ZPt, PtIndex" + os.linesep)
        for coordinate in self._pointDictionary.values ():
            x = int(coordinate.getXCoordinate ())
            y = int (coordinate.getYCoordinate ())
            z = int (coordinate.getZCoordinate ())            
                   
            # convert program voxel coordinates to disk image coordinates
            nativeOrientation = sourceVolumeDescription.getParameter ("NativeNiftiFileOrientation")
            finalContouredOrientation = sourceVolumeDescription.getParameter ("FinalROINiftiOrientation")
            newpoint, newshape = NiftiVolumeData.transformPoint (finalContouredOrientation, nativeOrientation, (x,y,z), shape)                                        
            x,y,z = newpoint
            
            # transform voxel coordinates to RAS coordinates (Nibable base)
            pt = np.array ([x,y,z,1.0],dtype=float)            
            RAS =  np.matmul (affin, pt.T)   
            
            # Convert RAS to LPS ANTS coordinate system
            writeLPSX = float (-RAS[0])
            writeLPSY = float (-RAS[1])
            writeLPSZ = float (RAS[2])            
            
            # write the CSV
            csvOut.write ("%f, %f, %f, %d%s" % (writeLPSX, writeLPSY, writeLPSZ, int (counter), os.linesep))
            #print ("Writing Point %d at xyz(%d, %d, %d)" % (counter, x, y, z))
            counter += 1
        
        csvOut.close ()
                   
        at = ApplyTransformsToPoints ()
        at.inputs.dimension = 3
        at.inputs.input_file = csvFileIn
        at.inputs.output_file = csvFileOut
        at.inputs.transforms = ANTS_Transformations  #['trans.mat', 'ants_Warp.nii.gz']
        at.inputs.invert_transform_flags = [False] * len (ANTS_Transformations)
        at.inputs.num_threads = NumThreads
        #at.cmdline 
        at.run ()             
        
        csv = open (csvFileOut, "rt")
        
        #invert the destination matrix to transform RAS coordinates back to Voxel
        RASTOVoxels = np.linalg.inv (destVolumeDescription.getParameter ("Affine"))
        # get the destination shape
        destShape = destVolumeDescription.getParameter ("VolumeDimension")
        newpoint, newshape = NiftiVolumeData.transformPoint (finalContouredOrientation, nativeOrientation, (0,0,0), destShape)                                        
        nativeDestinationShape = newshape
        
        self._ROIDefIDNumber = None
        self._pointDictionary = {}
        self._pointIDManger = ContourIDManger (self)
        self._selectedContourIDList = []
        self._LastModifiedPointID  = -1
        self._selected = False
        self._PointSize = 8
        skipHeader = True
        for line in csv:
            if (skipHeader) :
                skipHeader = False
                continue
            #print (line)
            # read a input line from the CSV file
            num = line.split (',')
            num[0] = num[0].strip ()
            num[1] = num[1].strip ()
            num[2] = num[2].strip ()
            #count  =  int (float (num[3].strip ()))
            LPSx = float (num[0])
            LPSy = float (num[1])
            LPSz = float (num[2])
            
            # convert LPS coordinates to RAS            
            RASx = -LPSx
            RASy = -LPSy
            RASz = LPSz
            
            # convert RAS -> Voxels
            pt = np.array ([RASx,RASy,RASz, 1.0],dtype=float)
            voxel =  np.matmul (RASTOVoxels, pt.T)   
            x = int (voxel[0])
            y = int (voxel[1])
            z = int (voxel[2])
            
                        
            # flip the voxels back into program coordinates
            newpoint, newshape= NiftiVolumeData.transformPoint (nativeOrientation, finalContouredOrientation, (x,y,z), nativeDestinationShape)                              
            x,y,z = newpoint 
            destShape = newshape
            if (ClipPointsToShape) :
                if (x < 0):
                    x = 0
                if (y < 0) :
                    y = 0
                if (z < 0) :
                    z = 0
                if (x >= destShape[0]):
                    x = destShape[0] - 1
                if (y >= destShape[1]):
                    y = destShape[1] - 1
                if (z >= destShape[2]):
                    z = destShape[2] - 1
                                 
            if (x >= 0 and y >= 0 and z >= 0 and x < destShape[0] and y < destShape[1] and z < destShape[2]) :                        
                # allocate the point
                PointId = self._pointIDManger.allocateID ()          
                newC = Coordinate ()
                newC.setCoordinate ((x, y, z))
                #print ("adding point ID: %d, Num: %d, xyz: %d, %d, %d" % (PointId, index, x, y, z))
                self.setPoint (PointId, newC)                
            else:                 
                print ("Invalid point position. Could not transform point into voxel space.  skipping point. x,y,z (%d, %d, %d) shape (%d, %d, %d)" % (x,y,z, destShape[0], destShape[1], destShape[2]))
                print ("voxel[2]: %d" % (int (voxel[2])))
                print ("RASz: %f" % (RASz))
                print ("LPSz: %f" % (LPSz))        
                #self._pointDictionary = oldpointDictionary
                #self.applyAntsTransformToROI (sourceVolumeDescription, destVolumeDescription, ANTS_Transformations, tempfiledir = tempfiledir)             
        csv.close ()
        os.remove (csvFileIn)
        os.remove (csvFileOut)
        self._clearPointAxisCache ()
        return (len (self._pointDictionary) > 0)
   
   def _removePIDFromAxis (self, Axis, xc, pid) :
       if (Axis in self._pointAxisCache) :
           if xc in self._pointAxisCache[Axis] :
               if pid in self._pointAxisCache[Axis][xc] :
                   self._pointAxisCache[Axis][xc].remove(pid)
                   if len (self._pointAxisCache[Axis][xc]) == 0 :
                      del self._pointAxisCache[Axis][xc]
                      if len (self._pointAxisCache[Axis]) == 0 :
                           del self._pointAxisCache[Axis]
                           
   def _addPIDToAxis (self, Axis, xc, pid) :
       if Axis not in self._pointAxisCache :
           self._pointAxisCache[Axis] = {}           
           self._pointAxisCache[Axis][xc] = set ()
           self._pointAxisCache[Axis][xc].add (pid)
       elif xc not in self._pointAxisCache[Axis] :
           self._pointAxisCache[Axis][xc] = set ()
           self._pointAxisCache[Axis][xc].add (pid)
       else:
           self._pointAxisCache[Axis][xc].add (pid)
      
   def _removePointIDFromAxisCache (self, pid) :
       if pid in self._pointDictionary :
           coord = self._pointDictionary[pid]           
           self._removePIDFromAxis ("X", coord.getXCoordinate (), pid)
           self._removePIDFromAxis ("Y", coord.getYCoordinate (), pid)
           self._removePIDFromAxis ("Z", coord.getZCoordinate (), pid)
   
   def _addPointIDToAxisCache (self, pid) :
       if pid in self._pointDictionary :
           coord = self._pointDictionary[pid]           
           self._addPIDToAxis ("X", coord.getXCoordinate (), pid)
           self._addPIDToAxis ("Y", coord.getYCoordinate (), pid)
           self._addPIDToAxis ("Z", coord.getZCoordinate (), pid)
           
   def _getPointAxisCache (self):
       if len (self._pointAxisCache) == 0 :
           xAxis = {}
           yAxis = {}
           zAxis = {}           
           for pid, coordinate in  self._pointDictionary.items () :
               x = int(coordinate.getXCoordinate ())
               y = int (coordinate.getYCoordinate ())
               z = int (coordinate.getZCoordinate ()) 
               if x not in xAxis :
                   xAxis[x] = set ()
               if y not in yAxis :
                   yAxis[y] = set ()
               if z not in zAxis :
                   zAxis[z] = set ()
               xAxis[x].add (pid)
               yAxis[y].add (pid)
               zAxis[z].add (pid)
           self._pointAxisCache["X"] = xAxis
           self._pointAxisCache["Y"] = yAxis
           self._pointAxisCache["Z"] = zAxis
       return  self._pointAxisCache 
               
               
   def getPointVolumeBoundingBox (self) : 
       xBox = [0,0]
       yBox = [0,0]
       zBox = [0,0]
       axisCache = self._getPointAxisCache ()
       if len (axisCache) > 0 :
           xAxis = axisCache["X"]
           yAxis = axisCache["Y"]
           zAxis = axisCache["Z"]
           xBox = [min (xAxis),max (xAxis)]
           yBox = [min (yAxis),max (yAxis)]
           zBox = [min (zAxis),max (zAxis)]
       return (xBox, yBox,zBox)
       
   def hasSlices (self) : # returns true if slices are defined
       return (len (self._pointDictionary) > 0)
   
   def hasDeleteableSliceInRange (self, firstSlice, lastSlice, RemoveProjectedSlices = False, SliceView = None, ROIDictionary = None) :
       for sliceIndex in range (firstSlice, lastSlice + 1) :
            if self.hasSlice (sliceIndex) :
                return True
       return False
   
   def hasSlice (self, index) :
       axisCache = self._getPointAxisCache ()
       if "Z" not in axisCache :
           return False
       return index in axisCache["Z"]
    
   def getPointSize (self):
       return self._PointSize
    
    
   def getMaskedVolume (self, VolumeShape, inslicePointSize = None, crossSliceSize = None, UniqueLableForPoints = False):
       mask = np.zeros (VolumeShape, dtype=np.int)    
       if (crossSliceSize is None or crossSliceSize <= 1) :
          crossSliceSize = 1
          zUp = 0                 
       else:
          zUp = -int (crossSliceSize / 2)           
       
       if (inslicePointSize is None or inslicePointSize <= 1) :
           inslicePointSize = 1
           xUp = 0                 
       else:
           xUp = -int (inslicePointSize / 2)           
       yUp = xUp
       
       pointLabel = 1
       for pointID, coordinate in self._pointDictionary.items ():           
           zTop = coordinate.getZCoordinate () + zUp
           zBottom = zTop + crossSliceSize
           zTop = min (max (zTop, 0), VolumeShape[2])
           zBottom = min (max (zBottom, 0), VolumeShape[2])
           
           xTop = coordinate.getXCoordinate () + xUp
           xBottom = xTop + inslicePointSize
           xTop = min (max (xTop, 0), VolumeShape[0])
           xBottom = min (max (xBottom, 0), VolumeShape[0])
           
           yTop = coordinate.getYCoordinate () + yUp
           yBottom = yTop + inslicePointSize
           yTop = min (max (yTop, 0), VolumeShape[1])
           yBottom = min (max (yBottom, 0), VolumeShape[1])
           
           mask[xTop:xBottom, yTop:yBottom,zTop:zBottom] = pointLabel
           if (UniqueLableForPoints) :
               pointLabel += 1
               
       return mask
    
    
   def _samplePoint (self, NiftiVolume, VolumeShape, coordinate, inslicePointSize = None, crossSliceSize = None):
       if (inslicePointSize is None) :
           pointsize = self.getPointSize ()
       else:
           pointsize = inslicePointSize
       if (pointsize < 1) :
           pointsize = 1
           
       XMax, YMax, ZMax = VolumeShape
       z = coordinate.getZCoordinate ()
       if (crossSliceSize is None) :
           crossSliceSize = 1
       elif (crossSliceSize > 1):
           z -= int (crossSliceSize / 2)
           
       x = coordinate.getXCoordinate ()
       y = coordinate.getYCoordinate ()       
       if (pointsize > 1) :
           halfDim = int (pointsize / 2)              
           x -= halfDim
           y -= halfDim                     
       
       startZ = max (0, z)
       endZ   = min (z + crossSliceSize, ZMax)
       startY = max (0, y)
       endY   = min (y + pointsize, YMax)
       startX = max (0, x)
       endX   = min (x + pointsize, XMax)
       voxels = NiftiVolume[startX:endX,startY:endY, startZ:endZ]
       xdim, ydim, zdim = voxels.shape 
       return voxels.reshape ((xdim * ydim * zdim))
       
   def getSliceVoxels (self, NiftiVolume, sliceIndex, ROIDefs) :       
       return self.getVoxels (NiftiVolume, sliceIndex = sliceIndex)

   def getVoxels (self, NiftiVolume, sliceIndex = None, inslicePointSize = None, crossSliceSize = None, ROIDefs = None) :       
       voxelList = []
       NiftiVolumeData = NiftiVolume.getImageData ()
       volumeDim = (NiftiVolume.getXDimSliceCount (), NiftiVolume.getYDimSliceCount (), NiftiVolume.getZDimSliceCount ())              
       for pointID, coordinate in self._pointDictionary.items ():
           if (sliceIndex == None or sliceIndex == coordinate.getZCoordinate ()) :
               voxels = self._samplePoint (NiftiVolumeData, volumeDim, coordinate, inslicePointSize = inslicePointSize, crossSliceSize = crossSliceSize)
               if (len (voxels) > 0) :                                  
                   voxelList.append (voxels)                   
       if (len (voxelList) == 0) :
           return (np.array ([], dtype=NiftiVolumeData.dtype), 0)
       return (np.concatenate (voxelList, axis=0), len (voxelList))
               
               
   def setSelectedContours (self, contourID) :
       #self.setObjectChanged () contour Id list is not saved. call is not necessary
       self._selectedContourIDList = contourID       

   def getSelectedContours (self) :
       return self._selectedContourIDList   
       
   def getPointCount (self) :
       return len (self._pointDictionary)
    
   def getPointCoordinate (self, pointID) :
       return self._pointDictionary[pointID].copy ()
      
   def setPoint (self, pointID, cord, humanEditedPoint = True) :
       self.setObjectChanged ()
       self._LastModifiedPointID = pointID              
       self._removePointIDFromAxisCache (pointID)           
       self._pointDictionary[pointID] = cord.copy ()
       self.setPointProperty (pointID, "HumanEdited", humanEditedPoint) 
       self._addPointIDToAxisCache (pointID)    
       
       
   def setLastModifiedPointID (self, pointID) :
       self.setObjectChanged ()
       self._LastModifiedPointID = pointID   
   
   def getType (self) :
       return "Point" 
       
   def isROIArea (self) :
       return self.getType () == "Area"
       
   def copy (self) :
        newobj = ROIPointObject ()   
        newobj._ROIUIDManager = self._ROIUIDManager.copy ()
        newobj._UndoChanged = self._UndoChanged        
        newobj._setFileObjectCache (self._getFileObjectCache (), SetWriteCache = False)        
        newobj._userChangeLog = copy.copy (self._userChangeLog)        
        newobj._RegistrationLog = self._RegistrationLog.copy (newobj)
        newobj._ROIDefIDNumber = self._ROIDefIDNumber
        newobj._PointSize = self._PointSize
        newobj._selectedContourIDList = copy.copy (self._selectedContourIDList)        
        newobj._selected = self._selected
        newobj._LastModifiedPointID = self._LastModifiedPointID
        for key, value in self._pointDictionary.items () :
            newobj._pointDictionary[key] = value.copy ()
        newobj._pointIDManger = self._pointIDManger.copy (newobj)        
        newobj._pointParameters = copy.deepcopy (self._pointParameters)               
        newobj._pointAxisCache = copy.deepcopy (self._pointAxisCache)
        return newobj      
   
   def getContourName (self, PointId) :
       return self._pointIDManger.getContourName (PointId)
   
   def setContourName (self, PointId, name) :
       self._pointIDManger.setContourName (PointId, name)
       
   def addPoint (self, pointName, cord, humanEditedPoint = True):
       self.setObjectChanged ()
       PointId = self._pointIDManger.allocateID ()  
       self.setPoint (PointId, cord, humanEditedPoint = humanEditedPoint)          
       self._pointIDManger.setContourName (PointId, pointName)
       return PointId
       
   def isSliceContoured  (self, cord) :                     #bugbug easy  neeeds to know slice Axis                 
       return self.hasSlice (cord)       

   def isContourDefined (self, pointIDNumber) :
       if (pointIDNumber in self._pointDictionary) :
           return (True, self._pointDictionary[pointIDNumber].getZCoordinate ())            
       return (False, 0)
   
    
   def isPointPositionDefined (self, x, y, z) :
       axisCache = self._getPointAxisCache ()
       if "Z" not in axisCache :
           return False
       if z not in axisCache["Z"] :
           return False
       sliceList = axisCache["Z"][z] 
       for pointID in sliceList :
           cord = self.getPointCoordinate (pointID)
           if cord.getXCoordinate () == x and cord.getYCoordinate () == y :
               return True
       return False
           
               
   def getSlicePointIDList (self, sliceNumber) :
       pointIDList = []
       axisCache = self._getPointAxisCache ()
       if "Z" not in axisCache :
           return pointIDList
       if sliceNumber not in axisCache["Z"] :
           return pointIDList
       return list (axisCache["Z"][sliceNumber])       
    
   def contourDefinedForSlice (self, pointIDNumber, sliceindex) :        
        if (pointIDNumber in self._pointDictionary) :
            zCord = self._pointDictionary[pointIDNumber].getZCoordinate ()
            return (zCord == sliceindex or zCord == -1)
        return False
    
   def doesSliceContainHumanEditedContour (self, index) :             
       pointIDList = self.getSlicePointIDList (index)
       for pid in pointIDList :           
           if (self.isPointHumanEdited (pid)) :
               return True
       return False
   
   def areSelectedContoursDeleteableInSlice (self, index) :              
       for pointID in self._selectedContourIDList :
           if pointID in self._pointDictionary :
               coordinate =  self._pointDictionary[pointID]
               zCord = coordinate.getZCoordinate ()
               if (zCord == index or zCord == -1) :
                   return True
       return False       

   def getPointCoordinatesAsTxt (self, sourceVolumeDescription) :
        voxelCoordinates = [] 
        worldCoordinates = [] 
        
        shape = sourceVolumeDescription.getParameter ("VolumeDimension")        
        affin = sourceVolumeDescription.getParameter ("Affine")                
        for coordinate in list (self._pointDictionary.values ()):
            x = int(coordinate.getXCoordinate ())
            y = int (coordinate.getYCoordinate ())
            z = int (coordinate.getZCoordinate ())            
                   
            # convert program voxel coordinates to disk image coordinates
            nativeOrientation = sourceVolumeDescription.getParameter ("NativeNiftiFileOrientation")
            finalContouredOrientation = sourceVolumeDescription.getParameter ("FinalROINiftiOrientation")
            newpoint, newshape = NiftiVolumeData.transformPoint (finalContouredOrientation, nativeOrientation, (x,y,z), shape)                                        
            x,y,z = newpoint
            
            voxelCoordinates.append ([int (x), int (y), int (z)])
            
            # transform voxel coordinates to RAS coordinates (Nibable base)
            pt = np.array ([x,y,z,1.0],dtype=float)            
            RAS =  np.matmul (affin, pt.T)   
            RAS = RAS.tolist ()
            worldCoordinates.append ([float (RAS[0]), float (RAS[1]), float (RAS[2])])
        CoordinateDictionary = {}
        CoordinateDictionary["Nifti Coordinates (x,y,z in Voxels)"] = voxelCoordinates
        CoordinateDictionary["Spatial Coodinates Based on NIfTI Affine (mm in RAS based orientation)"] = worldCoordinates
        return json.dumps (CoordinateDictionary)                 
        
    
   def getFileObject (self, ROIDefs, Name = None) :
       if (self.isChanged ()) :
           f = FileObject ("ROIPointObject")       
           if (Name is None) :
                   Name = self.getName (ROIDefs)
                   
           roiColor = ROIDefs.getROIColor (Name)            
           f.setParameter ("Name", Name)     
           f.setParameter ("Color",(roiColor.red (), roiColor.green (), roiColor.blue (), roiColor.alpha ()))                 
           f.setParameter ("MaskValue",ROIDefs.getROIMaskValue (Name) )                
           f.setParameter ("RadlexID", ROIDefs.getROINameRadlexID (Name)) 
           
           f.setParameter ("Selected", self._isSelected ())
           f.setParameter ("LastModifiedPointID", self._LastModifiedPointID)
           f.setParameter ("PointSize", self._PointSize)
           f.setParameter ("PointCount", len (self._pointDictionary))  
           f.setParameter ("UserChangeLog", self.getObjectChangeLog ())                   
           f.setParameter ("ROIDefUIDObj", self._ROIUIDManager.getTuple ())
           
           self._RegistrationLog.saveToFile (FileHeader = f)       
           self._pointIDManger.save (f)
           i = 1
           for key, cord in self._pointDictionary.items():                        
               fobj = FileObject ("PT" + str (i))
               fobj.setParameter ("PointID", key)                    
               fobj.setParameter ("Coordinate", (cord.getXCoordinate (), cord.getYCoordinate (), cord.getZCoordinate ()))
               fobj.setParameter ("HumanEdited", self.isPointHumanEdited (key))
               i += 1
               f.addInnerFileObject (fobj)                             
           
           self._setFileObjectCache (f)
       return self._getFileObjectCache ()
       
    
    
    
   def isPointDefined (self, pointID) :
       if pointID in self._pointDictionary :
           return (self._pointDictionary[pointID].getZCoordinate () != -1)
       return (False)
       
   def getContourIDManger (self) :
       return self._pointIDManger
       
   def setSelected (self, selected):
       #self.setObjectChanged () Avoid Unecessary Save if just selection is modified
       self._selected = selected
   
   def _isSelected (self) :
       return self._selected
   
   def _deletePID (self, pid) :       
       self._removePointIDFromAxisCache (pid)
       del self._pointDictionary[pid]
       self.deletePointProperty (pid)
       
   def removeComputerEditedPointsInSliceRange (self, sliceRange = None) :
       self.setObjectChanged ()
       removeIDLst = []
       if (sliceRange is None) :           
           for pid, coord in self._pointDictionary.items () :
               if not self.isPointHumanEdited (pid) :
                   removeIDLst.append (pid)                                        
       else:
           axisCache = self._getPointAxisCache ()
           if "Z" in axisCache :           
               for zC in sliceRange :
                   if zC in axisCache["Z"] :
                       for pid in axisCache["Z"][zC] :
                           if not self.isPointHumanEdited (pid) :   
                               removeIDLst.append (pid)          
       for pid in removeIDLst :
            self._deletePID (pid)
        
       if (len (self._pointDictionary) > 0 and self._LastModifiedPointID not in self._pointDictionary) :
            self._LastModifiedPointID = list (self._pointDictionary.keys ())[0]
       else :
            self._LastModifiedPointID = -1              
       
   def removeSlice (self, firstIndex, RemoveAllSliceContours = False, Axis="Z", SharedROIMask = None, LastIndex = None, SliceViewObjectsChanged = None, RemoveProjectedSlices = False, SliceView = None):                        #bugbug easy  neeeds to know slice Axis                         
        if LastIndex is None :
            LastIndex = firstIndex 
        
        self.setObjectChanged ()
        removeIDLst = []
        if (RemoveAllSliceContours) :
            removeList = list (self._pointDictionary.keys ()) 
        else:
            removeList = self._selectedContourIDList
        for pointID in self._selectedContourIDList :
            if (pointID in removeList) :
                if Axis == "Z" :
                    cord = self._pointDictionary[pointID].getZCoordinate ()
                elif Axis == "X" :
                    cord = self._pointDictionary[pointID].getXCoordinate ()
                elif Axis == "Y" :
                    cord = self._pointDictionary[pointID].getYCoordinate ()
                if ((cord >= firstIndex and cord <= LastIndex) or cord == -1) :
                    self._pointIDManger.deleteID (pointID)
                    removeIDLst.append (pointID)                
        if (len (removeIDLst) == 0) :
            for pointID in self._pointDictionary.keys () :
                if Axis == "Z" :
                    cord = self._pointDictionary[pointID].getZCoordinate ()
                elif Axis == "X" :
                    cord = self._pointDictionary[pointID].getXCoordinate ()
                elif Axis == "Y" :
                    cord = self._pointDictionary[pointID].getYCoordinate ()
                if ((cord >= firstIndex and cord <= LastIndex) or cord == -1) :
                    self._pointIDManger.deleteID (pointID)
                    removeIDLst.append (pointID)                
                
        for pID in removeIDLst :
            self._deletePID (pID)
                    
        if (len (self._pointDictionary) > 0 and self._LastModifiedPointID not in self._pointDictionary) :
            self._LastModifiedPointID = list (self._pointDictionary.keys ())[0]
        else :
            self._LastModifiedPointID = -1        
        return (len (self._pointDictionary))        
                      
   def isHumanEditedContour (self, index):
       return (self.isPointHumanEdited (index))        
       
   def hasControlPoints (self):
       return False
             
   def getSlicePointIDClosestToXY (self, sliceIndex, XPt, YPt):
       pointIDLst = self.getSlicePointIDList (sliceIndex)       
       if (len (pointIDLst) == 0) :
           return None
       elif (len (pointIDLst) == 1) :
           return pointIDLst[0]
       #if multiple points, get closest pt.
       coord = self.getPointCoordinate (pointIDLst[0])
       xd = (coord.getXCoordinate () - XPt) 
       yd = (coord.getYCoordinate () - YPt) 
       distPointID = pointIDLst[0]
       distSQ = (xd * xd) + (yd * yd)
       for pointID in pointIDLst :
           coord = self.getPointCoordinate (pointID)
           xd = (coord.getXCoordinate () - XPt) 
           yd = (coord.getYCoordinate () - YPt)        
           testDistSQ = (xd * xd) + (yd * yd)
           if (testDistSQ < distSQ) :
               testDistSQ = distSQ 
               distPointID = pointID           
       return distPointID
    
   def getSliceNumberList (self):                           #bugbug easy  neeeds to know slice Axis       
       axisCache = self._getPointAxisCache ()
       if "Z" not in axisCache :
           return list ()
       return sorted(list (axisCache["Z"].keys ()))
   
   def hasAnnotationOnYAxis (self, cY, SharedROIMask = None) :       
       axisCache = self._getPointAxisCache ()
       if "Y" not in axisCache :
           return False
       return cY in axisCache["Y"]
    
   def hasAnnotationOnXAxis (self, cX, SharedROIMask = None) :
        axisCache = self._getPointAxisCache ()
        if "X" not in axisCache :
            return False
        return cX in axisCache["X"]
      
   def getYAxisSliceNumberList (self, SliceMem = None, SharedROIMask = None) :
       axisCache = self._getPointAxisCache ()         
       if SliceMem is None :
           if "Y" not in axisCache :
               return np.array([])           
           return np.array(list (axisCache["Y"].keys ()))
       else:
           if "Y" in axisCache :
               for yCord in axisCache["Y"].keys () :                    
                   if yCord >= 0 and yCord < SliceMem.shape[0] :
                       SliceMem[yCord] = True
                   
   def getXAxisSliceNumberList (self, SliceMem = None, SharedROIMask = None) :
       axisCache = self._getPointAxisCache ()         
       if SliceMem is None :
           if "X" not in axisCache :
               return np.array([])           
           return np.array(list (axisCache["X"].keys ()))
       else:
           if "X" in axisCache :
               for xCord in axisCache["X"].keys () :                    
                   if xCord >= 0 and xCord < SliceMem.shape[0] :
                       SliceMem[xCord] = True             
    
   def getChangedSlices (self, olderObject) :
       ChangedSlices = []
       for pointID in olderObject._pointDictionary.keys () :
           if pointID not in self._pointDictionary :
               ChangedSlices.append (olderObject._pointDictionary[pointID].getZCoordinate ()) # Point Removed
       for pointID in self._pointDictionary.keys () :
           if pointID not in olderObject._pointDictionary :
               ChangedSlices.append (self._pointDictionary[pointID].getZCoordinate ()) # Point Added
           else:
               oldPoint = olderObject._pointDictionary[pointID].getCoordinate ()
               newPoint = olderObject._pointDictionary[pointID].getCoordinate ()
               if tuple (oldPoint) != tuple (newPoint) :
                   ChangedSlices.append (newPoint.getZCoordinate ()) # Point Moved
       return sorted (set (ChangedSlices))
   
    
   def getLastModifiedPointID(self) :
       return self._LastModifiedPointID
       
   def getCoordinate(self):
       pointID = self.getLastModifiedPointID()
       if (pointID in self._pointDictionary) :
           return self._pointDictionary[pointID]
       else:
           c = Coordinate()
           c.setCoordinate ((0,0,0))
           return c
                 
 
   def isInAxisList (self, cord, axis, SharedROIMask = None):                     #bugbug easy  neeeds to know if axis is slice Axis       
       if axis == "Z" :
           return self.hasSlice (cord.getZCoordinate ())
       elif axis == "X" :
           return self.hasAnnotationOnXAxis (cord.getXCoordinate ())
       else:
           return self.hasAnnotationOnYAxis (cord.getYCoordinate ())
       
   
   def updateContourSlices (self,pointID) :           
       return 
       
   
   def getPointList (self):
       return list (self._pointDictionary.values ())
   
   def getROIDefIDNumber (self) :
       return self._ROIDefIDNumber 
       
   def setROIIDNumber (self, idNumber):
       #self.setObjectChanged () Unnessary save
       self._ROIDefIDNumber = idNumber
       
   def getName (self, RoiDefs) :
       return RoiDefs.getROIIDNumberName (self.getROIDefIDNumber ())    
   
   def getROIUIDManager (self) :
       return self._ROIUIDManager

   def getPointDrawnDim (self, niftislicewidget, coordinate):
       sliceAxis = niftislicewidget.getSliceAxis ()
       xC = coordinate.getXCoordinate ()
       yC = coordinate.getYCoordinate ()
       zC = coordinate.getZCoordinate () 
       if sliceAxis == 'Z' :   
           xC, yC = niftislicewidget.transformPointFromImageToScreen (xC, yC)
           return  (xC - 5, yC - 5, xC + 5, yC + 5)
       if sliceAxis == 'X' :    
           yC, zC = niftislicewidget.transformPointFromImageToScreen (yC, zC)
           return  (yC - 5, zC - 5, yC + 5, zC + 5)   
       if sliceAxis == 'Y' :    
           xC, zC = niftislicewidget.transformPointFromImageToScreen (xC, zC)
           return  (xC - 5, zC - 5, xC + 5, zC + 5)   
       
       pointsize = self.getPointSize ()
       halfpointsize = pointsize / 2 
       sliceAxis = niftislicewidget.getSliceAxis ()
       if sliceAxis == 'Z' :                 
             midX = coordinate.getXCoordinate ()
             midY = coordinate.getYCoordinate ()
             lY =   midY - halfpointsize
             rY =   midY + halfpointsize
       else:
           if sliceAxis == 'Y'  :
              midX = coordinate.getXCoordinate ()
              midY = coordinate.getZCoordinate ()
           elif sliceAxis == 'X'  :
              midX = coordinate.getYCoordinate ()
              midY = coordinate.getZCoordinate ()
           lY =  midY - 0.5
           rY =  midY + 0.5                   
       lX = int (midX - halfpointsize)
       rX = int (midX + halfpointsize)
       lX, lY = niftislicewidget.transformPointFromImageToScreen (lX, lY)
       rX, rY = niftislicewidget.transformPointFromImageToScreen (rX, rY)
       lX -= 1
       lY -= 1
       rX += 1
       rY += 1
       return (lX,lY,rX,rY)
   
   @staticmethod
   def _GetDistanceKey (key) :
       return key[1]
   
   def getSliceContoursAtPoint (self, cZ, cX, cY, niftislicewidget):
       pointIDLst = []
       #pointsize = self.getPointSize ()
       #pointdist = int (pointsize / 2) + 1
       #pointsize = self.getPointSize ()
       #halfpointsize = int (pointsize / 2)  + 1
       
       mX, mY = niftislicewidget.getLastSystemMouseCoordinate ()
       
       Axis = niftislicewidget.getSliceAxis ()      
       coord = Coordinate ()
       coord.setCoordinate ((cX, cY, cZ))
       pointIDList = self._getPointSliceAxisList (coord, Axis) 
           
       for pointID in pointIDList  :           
           coordinate = self._pointDictionary[pointID]
           lX,lY,rX,rY = self.getPointDrawnDim (niftislicewidget, coordinate)                                 
           if lX <= mX and mX <= rX and lY <= mY and mY <= rY :
              dist = math.sqrt ((cY-coordinate.getYCoordinate ())**2  + (cX-coordinate.getXCoordinate ())**2 + (cZ-coordinate.getZCoordinate ())**2)
              pointIDLst.append ((pointID, dist))           
               
       if (len (pointIDLst) == 0) :
           return []
       else:
           pointIDLst = sorted (pointIDLst, key = ROIPointObject._GetDistanceKey)           
           firstPoint = pointIDLst[0]       
           return [firstPoint[0]]
       
   
   
           
   
   def _getPointSliceAxisList (self, selCoordinate, sliceAxis) :
       axisCache = self._getPointAxisCache ()
       if sliceAxis not in axisCache :
           return []
       if sliceAxis == "Z"  :
           num = selCoordinate.getZCoordinate ()
       elif sliceAxis == "X"  :
           num = selCoordinate.getXCoordinate ()
       elif sliceAxis == "Y"  :
           num = selCoordinate.getYCoordinate ()
       if num not in axisCache[sliceAxis] :
           return []
       return axisCache[sliceAxis][num]
                 
       
   def draw(self, qp, niftislicewidget, ROIDefs, SliceDrawn, drawResizeableBB, drawBB, visSettings, drawObj=True, overrideHideROI = False):           #bugbug easy  neeeds to know if axis is slice Axis       
       if ROIDefs.isROIHidden (self.getName (ROIDefs)) :  #ignore override hide for point objects
           return    
       
       if (not drawObj):
           return
       
       selCoordinate = niftislicewidget.getCoordinate()                         
       sliceAxis = niftislicewidget.getSliceAxis ()       
       pidList = self._getPointSliceAxisList (selCoordinate, sliceAxis)
       if len (pidList) <= 0 :
           return    
       color = ROIDefs.getROIColor (self.getName (ROIDefs)) 
       defaultPen = QtGui.QPen(color, 0, QtCore.Qt.SolidLine )                  
       color.setAlpha (visSettings.getUITransparency ())
       defaultBrush = QtGui.QBrush(color)
       color.setAlpha (255)                    
       selectedPoints = self.getSelectedContours ()                    
       for pointID in pidList :            
           coordinate = self._pointDictionary[pointID]           
           lX,lY,rX,rY = self.getPointDrawnDim (niftislicewidget, coordinate)               
           if (not self._isSelected () or pointID not in selectedPoints) :
               qp.setPen (defaultPen)
               qp.setBrush (defaultBrush)
               qp.drawRect (lX,lY, rX - lX, rY - lY)            
           else:                                    
               qp.setBrush (QtGui.QColor (0, 0,0,0))
               qp.setPen (QtGui.QPen(QtGui.QColor (0, 0,0, 255), 0, QtCore.Qt.SolidLine ))
               qp.drawRect (lX,lY, rX - lX, rY - lY)                               
               qp.setPen (QtGui.QPen(QtGui.QColor (255,205,94), 0, QtCore.Qt.DotLine ))
               qp.setBrush (defaultBrush)
               qp.drawRect (lX,lY, rX - lX, rY - lY)            
   
    
   @staticmethod 
   def _clip (pt, minV, maxV) :
        if pt < minV :
            return minV
        if pt > maxV :
            return maxV
        return pt
          
   def clipROIToDimensions (self, origionalVoxelDim, DestinationVolumeSize, DestinationVolumeVoxelSize) :
           self.setObjectChanged ()
           xdim, ydim, zdim   = DestinationVolumeSize
           
           xScale = float (origionalVoxelDim[0]) / float (DestinationVolumeVoxelSize[0]) 
           yScale =  float (origionalVoxelDim[1]) / float (DestinationVolumeVoxelSize[1]) 
           zScale = float (origionalVoxelDim[2]) / float (DestinationVolumeVoxelSize[2]) 
           
           for pointID in self._pointDictionary.keys () :
              point = self._pointDictionary[pointID]
                                          
              midX = point.getXCoordinate ()
              midY = point.getYCoordinate ()
              midZ = point.getZCoordinate ()
              
              newX = ROIPointObject._clip (int (float (midX) * xScale), 0, xdim - 1)
              newY = ROIPointObject._clip (int (float (midY) * yScale), 0, ydim - 1)
              newZ = ROIPointObject._clip (int (float (midZ) * zScale), 0, zdim - 1)
              if (newX != midX or newY != midY or newZ != midZ)  :
                 self._removePointIDFromAxisCache (pointID)
                 point.setCoordinate ((newX, newY, newZ))
                 self._addPointIDToAxisCache (pointID)          
       
   def movePoint (self, pointIDLst, dx, dy, dz, volumeShape) :                                   
           self.setObjectChanged ()
           for pointID in pointIDLst :
               point = self._pointDictionary[pointID]
               
               midX = point.getXCoordinate ()
               midY = point.getYCoordinate ()
               midZ = point.getZCoordinate ()
               
               newX = ROIPointObject._clip (midX + dx, 0, volumeShape[0] - 1)
               newY = ROIPointObject._clip (midY + dy, 0, volumeShape[1] - 1)
               newZ = ROIPointObject._clip (midZ + dz, 0, volumeShape[2] - 1)
               
               if (newX != midX or newY != midY or newZ != midZ)  :
                   self._removePointIDFromAxisCache (pointID)
                   point.setCoordinate ((newX, newY, newZ))
                   self.setPointHumanEdited (pointID, True)          
                   self._addPointIDToAxisCache (pointID)                     
           return (newX - midX, newY - midY, newZ - midZ)          
   
   def _isMoving (self) :
       return False
   
   def _hasPoint (self, testpoint) :
       if not self.isInAxisList (testpoint, "X"):
           return False
       if not self.isInAxisList (testpoint, "Y"):
           return False
       if not self.isInAxisList (testpoint, "Z"):
           return False
       testC = testpoint.getCoordinate ()
       for point in self._pointDictionary.values () :
           if (testC == point.getCoordinate ()) :
               return True
       return False
           
   def mergeROI (self, pointObjectToMerge) :
       if (pointObjectToMerge.getType () == "Point"):
           self.setObjectChanged ()
           for PointID, newPointCord in pointObjectToMerge._pointDictionary.items () :               
               if not self._hasPoint (newPointCord) :
                   pointName = pointObjectToMerge.getContourName (PointID)
                   humanEditedPoint = pointObjectToMerge.isPointHumanEdited (PointID)
                   self.addPoint (pointName, newPointCord, humanEditedPoint = humanEditedPoint)           
   
   def getObjectChangeLog (self) :
       return self._userChangeLog
   
   def setObjectClipDictionary (self, clipDictionary) :
       return
   
   def getObjClipMaskDictionaryHelper (self,niftishape, Axis, coordinate, SharedROIMask = None) :
       return None
   
   def setObjectChangeLog (self, val) :
       self.setObjectChanged ()
       self._userChangeLog = val
   
   def _getFileObjectCache (self):
       return self._fobjCache 
   
   def _setFileObjectCache (self, fobj, SetWriteCache = True) :
       if (fobj is not None and SetWriteCache) :
           fulltext = fobj.getFileObjectTxt ()
           fobj.setWriteCache (fulltext)
       self._fobjCache = fobj
       
   def setObjectChanged (self) :
       self._setFileObjectCache (None)       
       self._setObjectUndoChanged ()
       
   def isChanged (self) :       
       return self._getFileObjectCache () is None
   
   def _setObjectUndoChanged (self) :
       self._UndoChanged = True

   def resetObjectUndoChanged (self) :
       self._UndoChanged = False
       
   def isObjectUndoChanged (self) :
        return self._UndoChanged
       
   def reorientObjectToMatchVolume (self, niftiVolume, sourceSliceAxis, QApp = None, ClipSliceMask = True) :
       self.setObjectChanged ()
       for pointId in self._pointDictionary.keys ():         
           if (QApp is not None) :
                QApp.processEvents()            
           coordinate = self._pointDictionary[pointId].getCoordinate ()
           coordinate = niftiVolume.reorientPointToMatchVolumeOrientation (coordinate, sourceSliceAxis)
           self._pointDictionary[pointId].setCoordinate (coordinate)
       self._clearPointAxisCache ()       
       #coordinate = niftiVolume.reorientPointToMatchVolumeOrientation (self.selCoordinate.getCoordinate (), sourceSliceAxis)
       #self.selCoordinate.setCoordinate (coordinate) 
   
   def clearTemporaryObjectSliceMask (self) :
       return
        
   def hasDeepGrowAnnotations (self) :
       return False
