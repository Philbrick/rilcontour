import os
import json
import sys
import platform
from six import *
import tempfile
import weakref
from rilcontourlib.util.FileUtil import FileUtil
from rilcontourlib.util.rcc_util import FileObject, DatasetTagManager, RelativePathUtil, PythonVersionTest, FileChangeMonitor, MessageBoxUtil, PlatformThreading
from rilcontourlib.pesscara.rcc_pesscarainterface import RCC_PesscaraInterface
from rilcontourlib.dataset.roi.rcc_roi import ROIDictionary
import pandas as pd
import time
from rilcontourlib.dataset.RCC_ROIFileLocks import PersistentFSLock, NonPersistentFSLock
from rilcontourlib.dataset.RCC_FileVersioning import FS_FileVersionManager
from rilcontourlib.ui.RCC_FileLockUI import LoadROIWarrningDlg
from uuid import uuid4
from rilcontourlib.util.FileUtil import ScanDir



"""import multiprocessing



#copyForChildProcessCopyDisableFinalizer
class BaseROIClass (object):
    def __init__ (self, EnableDestructor) :
        if EnableDestructor :
           self._FileHandleCount = multiprocessing.Value ("i", 1)
    
    def getHandleCount (self) :
         with self._FileHandleCount.get_lock():
              return self._FileHandleCount.value
              
    def incrementHandleCount (self) :
        with self._FileHandleCount.get_lock():
            oldhandleCount = self._FileHandleCount.value
            self._FileHandleCount.value += 1
            return oldhandleCount 
    
    def decrementHandleCount (self) :
        with self._FileHandleCount.get_lock():
            if self._FileHandleCount.value <= 1 :
                return True
            self._FileHandleCount.value -= 1
        return False
    
    def setHandleCount (self, oldhandleCount) :
        with self._FileHandleCount.get_lock():
            self._FileHandleCount.value = oldhandleCount
        
    def closeProcessHandleCleanup (self):
        with self._FileHandleCount.get_lock():
            if self._FileHandleCount.value <= 1 :
                self._closeHandleCallback ()
            else:
                self._FileHandleCount.value -= 1"""
        
        

class RCC_ROIDataset_FileSystemObject :
    def __init__ (self, path, NIfTIPath = None, ProjectDataset = None, LockFilePath = None, LockSignature = None, ProjectFileInterface = None, OpenFileAsReadOnly = "False", EnableDestructor = True, SuperUserFroceSilentGrabLock = False, SilentIgnoreOnLockFail = False, CreatingCopyForChildProcess = False, Verbose = True, TreePath = None):
        if len (path) > 255 and sys.platform.startswith ("win") and OpenFileAsReadOnly != "HardReadOnly":
            try :
                release, version, csd, ptype = platform.win32_ver ()
                if release <= 7 :                    
                    print ("---------------------------------------------------------------------------------------------")
                    print ("Windows file name length exceeded setting file open to read only to avoid possible data loss.")
                    print ("Filename: " + path)
                    print ("---------------------------------------------------------------------------------------------")
                    OpenFileAsReadOnly = "HardReadOnly"
                else:
                    dirname, fname = os.path.split (path)
                    try :
                       filehandle, filepath = tempfile.mkstemp (prefix=fname, suffix="_Extra_.tmp", dir = dirname)
                       os.close(filehandle)
                       os.remove (filepath)
                    except:
                        OpenFileAsReadOnly = "HardReadOnly"                        
                        print ("---------------------------------------------------------------------------------------------")
                        print ("Windows file name length exceeded setting file open to read only to avoid possible data loss.")
                        print ("Filename: " + path)
                        print ("---------------------------------------------------------------------------------------------")
            except:
                OpenFileAsReadOnly = "HardReadOnly"
                print ("")
                print ("A error occured while testing long window filename length.")
                print ("---------------------------------------------------------------------------------------------")
                print ("Windows file name length exceeded setting file open to read only to avoid possible data loss.")
                print ("Filename: " + path)
                print ("---------------------------------------------------------------------------------------------")
            
        try :
            NonPersistentFSLock.accessLock.acquire ()
            self._fileHandleUID = "FilePath_"+ path + "_" + str (uuid4()) + "_" + str (time.time ())
            self._fileTreePath = TreePath
            self._isClosed = False
            self._persistentLockFileName = None
            self._persistentLockData = None
            self._persistentLockFileChangeMonitor = None   
            self._ProjectFileInterface = ProjectFileInterface.copy ()
            
            if (os.path.exists (path) and not os.access (path, os.W_OK)) :
                OpenFileAsReadOnly = "HardReadOnly"
                                            
            if  LockFilePath is None and LockSignature is None and not CreatingCopyForChildProcess :
                if (OpenFileAsReadOnly ==  "HardReadOnly") :
                   LockFilePath = None
                   LockSignature = None
                else:
                   try: 
                       FSUID = ProjectFileInterface.getFileSystemUID ()
                       UserUID = ProjectFileInterface.getUserUID ()
                       LockFilePath = NonPersistentFSLock.getFileLockPathForROIFilePath (path)
                       self._LockFilePath = LockFilePath
                       
                       failCount = 0
                       while (True) :
                           CurrentHoldNonPersistentLock = False
                           try :
                               if os.path.exists (LockFilePath) :
                                   NonPersistentLockData = NonPersistentFSLock.getLockFile (LockFilePath, AccessLock = NonPersistentFSLock.accessLock)
                                   if ("UserUIDCreatedLock" in NonPersistentLockData and NonPersistentLockData["UserUIDCreatedLock"] == UserUID) :
                                       CurrentHoldNonPersistentLock = True
                           except:
                               CurrentHoldNonPersistentLock = False
                                                      
                           if  not CurrentHoldNonPersistentLock and not self.canTransferPersistentFileLock (IgnoreNonPersistantFileLock = True) and not ProjectFileInterface.isInSuperUserCommandLineMode (): # check if the if the file is assigned to some one else before trying to acquire the lock
                               LockFilePath = None
                               LockSignature = None
                               OpenFileAsReadOnly = "HardReadOnly"
                               break                       
                           
                           lockFileData =  NonPersistentFSLock.createLockFile (LockFilePath, ProjectFileInterface.getUserName (), FSUID, UserUID, WeakLocksEnabled = ProjectFileInterface.weakLocksCommandLineParameterEnabled (), ProjectDataset = ProjectDataset, FileHandleUID = self._fileHandleUID) 
                           if lockFileData is None :
                               failCount += 1
                               if failCount > 6 :
                                   LockFilePath = None
                                   LockSignature = None
                                   OpenFileAsReadOnly = "HardReadOnly"
                                   break
                           else:
                               if "LockSignature" in lockFileData and lockFileData["LockSignature"] == FSUID :
                                   LockSignature = FSUID
                                   break
                               else:
                                  if "LockSignature" in lockFileData :
                                      LockSignature = lockFileData["LockSignature"]  
                                  else:
                                      LockSignature = ""
                                  try :
                                      if (SuperUserFroceSilentGrabLock and ProjectFileInterface.isFileUnlockingCommandLineModeEnabled ()) :
                                           NonPersistentFSLock.unlock (LockFilePath, ProjectDataset = ProjectDataset)
                                      elif SilentIgnoreOnLockFail :
                                          LockFilePath = None
                                          LockSignature = None
                                          OpenFileAsReadOnly = "HardReadOnly"
                                          break
                                      elif ("UserUIDCreatedLock" in lockFileData and lockFileData["UserUIDCreatedLock"] == UserUID):                                  
                                          unlockDataset = None
                                          if ProjectDataset is not None :
                                              if ProjectDataset.isRunningInDemoMode () :
                                                  unlockDataset = "Readwrite"
                                          if unlockDataset is None :
                                              unlockDataset = LoadROIWarrningDlg.showLoadROIWarrningDlg ("Loading ROI Data Warrning", "RIL-Contour detects you are already editing this file. This can happen if you are editing on another computer. Or, it may indicate that RIL-Contour crashed in a previous session. Editing a file in two windows may result in data loss. How do you wish to open the file?", details = LockSignature, ReadWrite = True, ReadOnly = True)                                  
                                          if (unlockDataset =="Readwrite") :
                                              NonPersistentFSLock.unlock (LockFilePath, ProjectDataset = ProjectDataset)
                                              if (LockSignature != FSUID) :
                                                  pass
                                          elif (unlockDataset == "Readonly") :
                                              OpenFileAsReadOnly = "HardReadOnly"
                                              LockFilePath = None
                                              LockSignature = None
                                              break
                                          else:    
                                              self._isClosed = True
                                              OpenFileAsReadOnly = "HardReadOnly"
                                              LockFilePath = None
                                              LockSignature = None
                                              break
                                      elif ProjectFileInterface.isFileUnlockingCommandLineModeEnabled () or not ProjectFileInterface.isInMultiUserMode ():                                                                                          
                                          unlockDataset = LoadROIWarrningDlg.showLoadROIWarrningDlg ("Loading ROI Data Warrning",  "This file is assigned to someone else. Opening the file for readonly will allow you to view the file. Opening the file for 'read/write' will forcibly remove the current users file lock and allow you to make modifications to it." , details = LockSignature, ReadWrite = True, ReadOnly = True)                                  
                                          if (unlockDataset =="Readwrite") :
                                              NonPersistentFSLock.unlock (LockFilePath, ProjectDataset = ProjectDataset)
                                              #ForceCloseOpenPLocks = True
                                          elif (unlockDataset == "Readonly") :
                                              OpenFileAsReadOnly = "HardReadOnly"
                                              LockFilePath = None
                                              LockSignature = None
                                              break
                                          else:    
                                              self._isClosed = True
                                              OpenFileAsReadOnly = "HardReadOnly"
                                              LockFilePath = None
                                              LockSignature = None
                                              break
                                      else:
                                          unlockDataset = LoadROIWarrningDlg.showLoadROIWarrningDlg ("Loading ROI Data Warrning", "The selected ROI data file is opened by another user. The file cannot be edited until it is closed. Do you wish to open the file read only?", details = LockSignature, ReadWrite = False, ReadOnly = True)                                  
                                          if (unlockDataset == "Readonly") :
                                              OpenFileAsReadOnly = "HardReadOnly"
                                              LockFilePath = None
                                              LockSignature = None
                                              break
                                          else:    
                                              self._isClosed = True
                                              OpenFileAsReadOnly = "HardReadOnly"
                                              LockFilePath = None
                                              LockSignature = None
                                              break
                                  except:
                                      pass
                   except:
                       self._isClosed = True
                       OpenFileAsReadOnly = "HardReadOnly"
                       LockFilePath = None
                       LockSignature = None        
            ###
            self._EnableDestructor = EnableDestructor
            self._closeEnabled = True
            self._closeCalled = False
            self._closeUnsavedCalled = False
            self._OpenFileAsReadOnly = OpenFileAsReadOnly # Possible Values: "SoftReadOnly", "False", "HardReadOnly" 
            self._path = path
            self._fileVersionManager = FS_FileVersionManager (self._path, self._ProjectFileInterface)        
            self._NIfTIPath = NIfTIPath
            self._ProjectDataset = ProjectDataset
            self._DocumentUID = None
            self._LockFilePath = LockFilePath
            self._LockSignature = LockSignature
            if LockFilePath is not None :
                FileUtil.setFilePermissions (LockFilePath, self.getFilePermissions ())
            if not NonPersistentFSLock.isFileLocked (LockFilePath, LockSignature) :
                self._LockFilePath = ""
                self._LockSignature = None   
            else:
                if  not self.canTransferPersistentFileLock (IgnoreNonPersistantFileLock = True) and not ProjectFileInterface.isInSuperUserCommandLineMode () : #if after acquireing the file like the file was assigned to some one else.
                    self._unlockFile ()
                    self._OpenFileAsReadOnly = "HardReadOnly"
                    self._fileVersionManager = None
                    self._LockFilePath = ""
                    self._LockSignature = None                            
                
            self._saveOnCloseCacheFileName = None
            self._saveOnCloseCachePath = None
            self._finalizer = None
            if self._OpenFileAsReadOnly == "HardReadOnly" :
                self._EnableDestructor = False
            if Verbose : 
                if self._OpenFileAsReadOnly == "HardReadOnly" :
                    if (not self.isFileHandleLocked ()) :
                        print ("File opened unlocked as HardReadOnly")
                    else:
                        print ("File opened locked as HardReadOnly")
                else:
                    if (not self.isFileHandleLocked ()) :
                        print ("File opened unlocked as " + self._OpenFileAsReadOnly)
                    else:
                        print ("File opened locked as " + self._OpenFileAsReadOnly)
                        print ("   .lock path: " + self._LockFilePath)
                        print ("   .lock signature: " + self._LockSignature)
                
            if self._EnableDestructor :
                if PythonVersionTest.IsPython3 () :
                       self._finalizer = weakref.finalize (self, RCC_ROIDataset_FileSystemObject._finalizeObj, self._path, self._ProjectDataset, self._LockFilePath, self._LockSignature, self._saveOnCloseCacheFileName, self._saveOnCloseCachePath, self._ProjectFileInterface, NonPersistentFSLock.accessLock, self._fileHandleUID)    
        finally:
            NonPersistentFSLock.accessLock.release ()
    
    def _updateFileInDatasetUI (self, roiPath = None) :        
        path = self._NIfTIPath
        if roiPath is None :
            roiPath = self._path
        try :
            if self._ProjectDataset is not None and path is not None:
                niftiDataset = self._ProjectDataset.getNIfTIDatasetInterface ()
                if niftiDataset is not None :                                      
                    node = niftiDataset.findFileInDataset (path)
                    if node is not None :
                        niftiDataset.updateNodeStateFromPath (roiPath,  node)
        except:
            pass
                
    def getFileTreePath (self) :
        return self._fileTreePath   
        
    def setToDebug (self) :
        state = (self._finalizer, self._ProjectDataset)
        self._finalizer = None
        self._ProjectDataset = None
        return state
    
    def restoreFromDebug (self, state) :
        self._finalizer, self._ProjectDataset = state
        
    
    def getFilePermissions (self) :
        return self._ProjectFileInterface.getFileWritePermissions ()
       
    def getProjectDataset (self) :
        return self._ProjectDataset
        
    def _closeHandleCallback (self) :
        RCC_ROIDataset_FileSystemObject._finalizeObj (self._path, self._ProjectDataset, self._LockFilePath, self._LockSignature, self._saveOnCloseCacheFileName, self._saveOnCloseCachePath, self._ProjectFileInterface, NonPersistentFSLock.accessLock, self._fileHandleUID)
    
    def enableClose (self) :
        self._closeEnabled = True        
        if self._closeCalled :                        
            #roiPath = self._path
            self.close ()
            #self._updateFileInDatasetUI (roiPath)
        if self._closeUnsavedCalled :            
            #roiPath = self._path
            self.closeUnsaved ()
            #self._updateFileInDatasetUI (roiPath)
            
    def copyForChildProcessCopyDisableFinalizer (self) : #disable file unlocking in finalizer
        self._closeEnabled = False       
        fileCopy = RCC_ROIDataset_FileSystemObject (self._path, NIfTIPath = self._NIfTIPath, ProjectDataset = None, LockFilePath = self._LockFilePath, LockSignature = self._LockSignature, ProjectFileInterface = self._ProjectFileInterface, OpenFileAsReadOnly = self._OpenFileAsReadOnly, EnableDestructor = False, CreatingCopyForChildProcess = True, Verbose = False, TreePath= self._fileTreePath)
        fileCopy._DocumentUID = self._DocumentUID
        fileCopy._saveOnCloseCacheFileName = self._saveOnCloseCacheFileName
        fileCopy._saveOnCloseCachePath = self._saveOnCloseCachePath
        fileCopy._persistentLockFileName = self._persistentLockFileName
        fileCopy._persistentLockData = self._persistentLockData
        fileCopy._persistentLockFileChangeMonitor = self._persistentLockFileChangeMonitor  
        fileCopy._closeEnabled = False       
        return fileCopy
        
    def suppportsPersistentFileLocks (self) :
        return True
    
    def flushFileToDisk (self) :
         if (self._ProjectDataset is not None) :
             self._ProjectDataset.getROISaveThread ().getGlobalFileSaveThread ().blockingFileClose (self._path)         
         return True
    
    def getMultuserFileSharingAssignments (self,IncludeSelf = False) :
        return self._ProjectFileInterface.getMultuserFileSharingAssignments (IncludeSelf =IncludeSelf)
        
    def getProjectFileInterface (self):
        return self._ProjectFileInterface
    
    def getUserNickname (self, userName) :
        try:
            mgr = self._ProjectFileInterface.getMultiUserFileSharingManager()
            if mgr is not None :
                nickname = mgr.getUserNickname (userName)
                if nickname is not None :
                    return nickname
            return userName
        except:
            return userName
        
    def getMultiUserFileSharingManager (self) :
        return self._ProjectFileInterface.getMultiUserFileSharingManager()
    
    def getUserName (self) :
        return self._ProjectFileInterface.getUserName ()

    def getUserUID (self) :
        return self._ProjectFileInterface.getUserUID ()        
    
    def setDocumentUID (self, uid) :
        self._DocumentUID  =  uid
    
    def getDocumentUID (self) :
        if self._DocumentUID  is None :
            try :                             
                self._DocumentUID  = ROIDictionary.getDocumentUIDFromFile (self.getPath (), self._ProjectFileInterface)                
            except:
                pass
        return self._DocumentUID 
        
    def _removeFinalizer (self) :
        if self._finalizer is not None :            
            self._finalizer.detach()
            self._finalizer = None 
          
        
    def isReadOnly (self) :
         self._OpenFileAsReadOnly != "False"
         
    def setOpenFileReadOnlyStatus (self, val) :
        self._OpenFileAsReadOnly = val
        
    def getFileHandleReadOnlyStatus (self) :
        return self._OpenFileAsReadOnly    
       
    def setSaveCacheOnCloseCachePath (self, filename, cachePath) :
        self._saveOnCloseCacheFileName = filename
        self._saveOnCloseCachePath = cachePath  
        self._removeFinalizer ()         
        if self._EnableDestructor :
            if PythonVersionTest.IsPython3 () :                 
                     self._finalizer = weakref.finalize (self, RCC_ROIDataset_FileSystemObject._finalizeObj, self._path, self._ProjectDataset, self._LockFilePath, self._LockSignature, self._saveOnCloseCacheFileName, self._saveOnCloseCachePath, self._ProjectFileInterface, NonPersistentFSLock.accessLock, self._fileHandleUID)
        return
    
    if PythonVersionTest.IsPython2 () :
        def __del__ (self) :
            if self._EnableDestructor :
                if self._LockFilePath is not None and self._LockSignature is not None :
                    RCC_ROIDataset_FileSystemObject._finalizeObj (self._path, self._ProjectDataset, self._LockFilePath, self._LockSignature, self._saveOnCloseCacheFileName, self._saveOnCloseCachePath, self._ProjectFileInterface, NonPersistentFSLock.accessLock, self._fileHandleUID)
    
    @staticmethod
    def _finalizeObj (path, ProjectDataset, LockFilePath, LockSignature, cacheFileName, cachePath, projectFileInterface, NonPersistentFSLock_AccessLock, fileHandleUID):
            if LockSignature is not None and LockSignature != ""  :
                if cacheFileName is not None and cachePath is not None :
                    lockFileData = NonPersistentFSLock.getLockFile (LockFilePath, AccessLock = NonPersistentFSLock_AccessLock)
                    if lockFileData is not None and lockFileData["LockSignature"] == LockSignature and ("OpenFileHandles" not in lockFileData or len (lockFileData["OpenFileHandles"]) == 1) :
                        try :
                            try :
                                projectFileInterface.aquireFileLock ()
                                cacheData = projectFileInterface.getDatasetCompressedCache (cachePath)
                                projectFileInterface.saveDatasetCompressedCache (cacheFileName, cacheData, ProjectDataset = ProjectDataset)
                            finally:
                                projectFileInterface.releaseFileLock ()
                        except:
                            pass
            if (path is not None) :
                RCC_ROIDataset_FileSystemObject.static_save (path, ProjectDataset) 
            if (LockSignature is not None and os.path.exists (LockFilePath)) :
                RCC_ROIDataset_FileSystemObject.static_unlockFile (LockFilePath, LockSignature, Finalizer = True, ProjectDataset = ProjectDataset, NonPersistentFSLock_AccessLock = NonPersistentFSLock_AccessLock, FileHandleUID = fileHandleUID)
    
    def canVersionFile (self):
        return self._fileVersionManager is not None and self._fileVersionManager.isEnabled () 
    
    def getFileVersionHistory (self) :
        if not self.canVersionFile () :            
            return []       
        return self._fileVersionManager.getDocumentVersionHistory (self) 
    
   

    def getVersionNumberAndPathFromUID (self, versionUID) :
        if not self.canVersionFile () :
            return None, None
        return self._fileVersionManager.getVersionNumberAndPathFromUID (versionUID)
    
    
    def versionFile (self, Comment, ForceVersion = False) :
        if not self.canVersionFile () :            
            return None              
        self.flushFileToDisk ()
        versionList = self._fileVersionManager.versionDocument (self, Comment = Comment, ForceVersion = ForceVersion) 
        if versionList is None :
            return None
        if len (versionList) > 0 :
            return versionList[-1]
        return None
    
    def removeFileVersionsAfter (self, startVersion):
        if not self.canVersionFile () :            
            return False
        if startVersion is None :
            return False
        return self._fileVersionManager.removeFileVersionsAfter (startVersion)        
    
    def removeUncessaryFileVersionsBetween (self, startVersion, endVersion) :
        if not self.canVersionFile () :            
            return False
        if startVersion is None or endVersion is None :
            return False
        return self._fileVersionManager.removeUncessaryFileVersionsBetween (startVersion, endVersion)        
    
    def syncToPreviousFileVersion (self, version, Comment) :
        if not self.canVersionFile () :            
            return None        
        self.flushFileToDisk ()
        return self._fileVersionManager.syncToVersionedDocument (version, self, Comment = Comment)             
    
    def isFileHandleLocked (self) :
        if  (self._LockSignature is not None and  self._LockSignature != "") :
            lockFileData = NonPersistentFSLock.getLockFile (self._LockFilePath)
            return (lockFileData is not None and lockFileData["LockSignature"] == self._LockSignature)
        return False
    
    def _getPersistentLockData (self, IgnoreNonPersistantFileLock = False) :
        if not IgnoreNonPersistantFileLock and not self.isFileHandleLocked () :
            return None
        if self._persistentLockData is None :
            if self._persistentLockFileName is None :
                nplockFilePath = self._LockFilePath
                if nplockFilePath is None or nplockFilePath == "": 
                    nplockFilePath = NonPersistentFSLock.getFileLockPathForROIFilePath (self._path)
                self._persistentLockFileName = PersistentFSLock.getPersistentLockFileName (nplockFilePath)
            self._persistentLockData = None 
        if self._persistentLockData is None or (self._persistentLockFileChangeMonitor is not None and self._persistentLockFileChangeMonitor.hasFileChanged (self._persistentLockFileName)) :
            self._persistentLockFileChangeMonitor = FileChangeMonitor (self._persistentLockFileName)
            self._persistentLockData = PersistentFSLock.loadFileData (self._persistentLockFileName)
        return self._persistentLockData
    
    def getPersistentLockHistory (self, IgnoreNonPersistantFileLock = False) :
        pLockData = self._getPersistentLockData (IgnoreNonPersistantFileLock = IgnoreNonPersistantFileLock)
        return PersistentFSLock.getPLockFileHistory (self._persistentLockFileName, PLockData = pLockData)  
    
    def getPreviousPlockOwner (self) :
        pLockData = self._getPersistentLockData ()
        if pLockData is None :
            return None
        return PersistentFSLock.getPreviousPlockHolder (pLockData, self.getUserName (), self.getUserUID ())
    
    def getCurrentFilePLock (self) :
        pLockData = self._getPersistentLockData ()
        if pLockData is None :
            return None
        return PersistentFSLock.getCurrentPlock (pLockData)
    
    def ownsPersistentFileLock (self) :
        result = self.getPersistentFileLockStatus ()
        if result == "UserLocked":
            return True
        return False
    
    def canTransferPersistentFileLock (self, IgnoreNonPersistantFileLock = False) :
        result = self.getPersistentFileLockStatus (IgnoreNonPersistantFileLock = IgnoreNonPersistantFileLock)
        if result in ["UserLocked", "Unlocked", "UserGroupLocked"] :
            return True
        return False
    
    #return username, userid, groupID
    def getDefaultUserFileCheckin (self) :
        try :
            multiUserManager = self._ProjectFileInterface.getMultiUserFileSharingManager ()
            if multiUserManager is None or not multiUserManager.isInitalized ():
                return None, None, None
            userName = multiUserManager.getUserDefaultCheckin (self.getUserName ())
            if multiUserManager.isUserDefined (userName)  :
                userUID = multiUserManager.getUIDName (userName)
                return userName, userUID, None
            elif multiUserManager.isGroupDefined (userName) :
                groupName = userName
                return None, None, groupName
            return None, None, None
        except:
            return None, None, None
        
    def canTransferPersistentFileLockToOtherUserOrGroup (self) :
        multiUserManager = self._ProjectFileInterface.getMultiUserFileSharingManager ()
        if multiUserManager is None or not multiUserManager.isInitalized ():
            return False
        UserName = self.getUserName ()
        targets = multiUserManager.getUserAssignments (UserName)
        if targets is  None  :
            return False
        return len (targets) > 0 
    
    def canGetPersistentFileLock (self) :
        return self.canTransferPersistentFileLock ()
    
    def isPLockAvailable (self) :
        result = self.getPersistentFileLockStatus ()
        if result in ["Unlocked", "UserGroupLocked"] :
            return True
        return False
    
    def groupAssignedPersistentFileLock (self) :
        result = self.getPersistentFileLockStatus ()
        if result in ["UserGroupLocked"] :
            return True
        return False
    
    def otherUserHoldsPersistentFileLock (self) :
        result = self.getPersistentFileLockStatus ()
        if result in ["OtherUserLocked"] :
            return True
        return False
    
    def getPLockOwnerString (self) :
        data = self._getPersistentLockData ()
        if data is None :
            return "Unlocked"
        return PersistentFSLock.getCurrentLockOwnerString (data, self._ProjectFileInterface.getMultiUserFileSharingManager ())
     
    def hasPLockDataFile (self, IgnoreNonPersistantFileLock = False):
        nplockFilePath = self._LockFilePath
        if nplockFilePath is None or nplockFilePath == "" : 
            nplockFilePath = NonPersistentFSLock.getFileLockPathForROIFilePath (self._path)
        self._persistentLockFileName = PersistentFSLock.getPersistentLockFileName (nplockFilePath)
        if not os.path.exists (self._persistentLockFileName):
            return False
        return self._getPersistentLockData (IgnoreNonPersistantFileLock = IgnoreNonPersistantFileLock) is not None 
    
    def getPersistentFileLockStatus (self, IgnoreNonPersistantFileLock = False) :
        if not IgnoreNonPersistantFileLock and not self.isFileHandleLocked ():
            return "ErrorFileHandleisUnlocked"  # "Unlocked", "UserLocked" ,  "UserGroupLocked", "OtherUserLocked"  
        if not self.hasPLockDataFile (IgnoreNonPersistantFileLock = IgnoreNonPersistantFileLock) :
            mostRecentEntry = None
        else:
            pLockData = self._getPersistentLockData (IgnoreNonPersistantFileLock = IgnoreNonPersistantFileLock)
            if pLockData is None :
                mostRecentEntry = None
            elif len (pLockData) > 0 :
                mostRecentEntry = pLockData[-1]
            else:
                mostRecentEntry = None
        return PersistentFSLock.getLockStatusForCurrentUser (mostRecentEntry, self._ProjectFileInterface)
      
    
    def getPersistentFileLock (self, VersionComment = None, ForceVersion = False) :
        try :
            result = self.getPersistentFileLockStatus ()
            if result == "UserLocked" :
                return True
            elif result in ["Unlocked", "UserGroupLocked"] :
                UserName = self.getUserName ()
                FSUID = self._ProjectFileInterface.getFileSystemUID ()
                UserUID = self.getUserUID ()
                multiUserManager = self._ProjectFileInterface.getMultiUserFileSharingManager ()
                if multiUserManager is None or not multiUserManager.isInitalized () :
                    UserGroups = []
                else:
                    UserGroups = multiUserManager.getUserGroups (UserName)
                return PersistentFSLock.createPersistentFSLock (self._LockFilePath, UserName, FSUID, UserUID, UserGroups, self, VersionComment = VersionComment, ForceVersion = ForceVersion)
            else:
                return False
        finally:
            pass
            #self._updateFileInDatasetUI ()
    
    def revertPersistentFileLock (self, SuperUserOverride = False) :
        try :
            result = self.getPersistentFileLockStatus ()
            if result == "UserLocked" or (result == "OtherUserLocked" and SuperUserOverride):
                UserName = self.getUserName ()
                FSUID = self._ProjectFileInterface.getFileSystemUID ()
                UserUID = self.getUserUID ()
                self.flushFileToDisk ()
                return PersistentFSLock.revertPersistentFileLock (self._LockFilePath, UserName, FSUID, UserUID, self, SuperUserOverride = SuperUserOverride)
            return True
        finally:
            pass
            #self._updateFileInDatasetUI ()
    
    def forceCloseOpenPLocks (self) :
        try :
            result = self.getPersistentFileLockStatus ()
            if result != "Unlocked" :
                UserName = self.getUserName ()
                FSUID = self._ProjectFileInterface.getFileSystemUID ()
                UserUID = self.getUserUID ()
                return PersistentFSLock.forceClosePersistentFSLock (self._LockFilePath, UserName, FSUID, UserUID, self, "File was forcibly closed by " + UserName)
            return True    
        finally:
            pass
            #self._updateFileInDatasetUI ()
        
    def releasePersistentFileLock (self, Comment) :
        try :
            result = self.getPersistentFileLockStatus ()
            if result == "UserLocked" :
                UserName = self.getUserName ()
                FSUID = self._ProjectFileInterface.getFileSystemUID ()
                UserUID = self.getUserUID ()
                multiUserManager = self._ProjectFileInterface.getMultiUserFileSharingManager ()
                if multiUserManager is None or not multiUserManager.isInitalized () :
                    UserGroups = []
                else:
                    UserGroups = multiUserManager.getUserGroups (UserName)
                return PersistentFSLock.closePersistentFSLock (self._LockFilePath, UserName, FSUID, UserUID, self, Comment, UserGroups = UserGroups)
            return True
        finally:
            pass
            #self._updateFileInDatasetUI ()
        
    def transferPersistentFileLock (self, AssignToUserName, Comment = None) :
        try:
            if AssignToUserName is None  :
                return False
            result = self.getPersistentFileLockStatus ()
            if result in ["UserLocked", "Unlocked", "UserGroupLocked"] :
                UserName = self.getUserName ()
                FSUID = self._ProjectFileInterface.getFileSystemUID ()
                UserUID = self.getUserUID ()
                multiUserManager = self._ProjectFileInterface.getMultiUserFileSharingManager ()
                UserGroups = multiUserManager.getUserGroups (UserName)
                AssignToUserUID = multiUserManager.getNameUID (AssignToUserName)
                return PersistentFSLock.transferPersistentLockTo (self._LockFilePath, UserName, FSUID, UserUID, UserGroups, self,  AssignToUserName, AssignToUserUID, multiUserManager, Comment)
            else:
                return False
        finally:
            pass
            #self._updateFileInDatasetUI ()
        
    def queueFileSave (self, data) :
        FileSaveCallback = None
        if (self._ProjectDataset is not None) :  
            self._ProjectDataset.getROISaveThread ().getGlobalFileSaveThread ().queueFileSave (self._path, data, FileSaveCallback, LockFilePath = self._LockFilePath, LockSignature = self._LockSignature, FileHandle = self)     
        else:
            print ("Project Dataset is NONE saving to disk immediately.")
            with open (self._path, "wt") as outfile :
                outfile.write (data)
    
    @staticmethod
    def static_unlockFile (LockFilePath, LockSignature, Finalizer = False, ProjectDataset = None, NonPersistentFSLock_AccessLock = None, FileHandleUID = None) :
        if LockFilePath is not None and LockFilePath != "" and LockSignature is not None:
            try :
                print ("Static Unlocking " +  LockFilePath)
                if not os.path.exists (LockFilePath)  :
                    if not Finalizer :
                        print ("Lock file %s was unexpectedly deleted." % LockFilePath)
                    return True        
                else:
                    return NonPersistentFSLock.tryToUnlockFile (LockFilePath, LockSignature, ProjectDataset = ProjectDataset, AccessLock = NonPersistentFSLock_AccessLock, FileHandleUID = FileHandleUID)
            except:
                print ("A error occured unlocking %s" % LockFilePath)
        return False
    
    def _unlockFile (self) :
        if (RCC_ROIDataset_FileSystemObject.static_unlockFile (self._LockFilePath, self._LockSignature, ProjectDataset = self._ProjectDataset, NonPersistentFSLock_AccessLock = NonPersistentFSLock.accessLock, FileHandleUID = self._fileHandleUID)):
            print ("LockPath: " + self._LockFilePath)
            self._LockFilePath = None
            self._LockSignature = None  
            #print ("_LockSignuature Set To None")
    
    def getHandleType (self) :
        return "FileSystem"
    
    def fileSaveThreadCallback (self, path, ReturnRunningAsyncProcess = False) : # do nothing
        return
    
    def getPath (self) :
        return self._path
    
    def getNIfTIPath (self) :
        return self._NIfTIPath
    
    @staticmethod 
    def static_save (path, ProjectDataset) :
        if (path is not None) :
            try :                
                if (ProjectDataset is not None) :
                    ProjectDataset.getROISaveThread ().getGlobalFileSaveThread ().blockingFileClose (path)
            except:
                pass 
                #print ("Warning a error occured trying to close FS_ROIRef: %s" % self.getPath ())
        return
    
    def save (self, DeleteOnSave = False) :        
        if self.isFileHandleLocked () :
            print ("Calling File Handle is Locked Static Save")
            RCC_ROIDataset_FileSystemObject.static_save (self._path, self._ProjectDataset)
        else:
            print ("Calling File Handle is Not Locked Static Save")
    
    def isClosed (self) :
        return self._isClosed
    
    def close (self) :
            self._closeCalled = True
            if self._closeEnabled : 
                try :                
                    #roiPath = self._path
                    lockFileData = NonPersistentFSLock.getLockFile (self._LockFilePath)
                    if lockFileData is not None and lockFileData["LockSignature"] == self._LockSignature and ("OpenFileHandles" not in lockFileData or len (lockFileData["OpenFileHandles"]) == 1) :
                        #print ("File Is Locked")
                        if  self._saveOnCloseCacheFileName is not None and  self._saveOnCloseCachePath is not None :
                            #print ("Saving Compressed Cache")
                            try :
                                try :
                                    self._ProjectFileInterface.aquireFileLock ()
                                    cacheData = self._ProjectFileInterface.getDatasetCompressedCache (self._saveOnCloseCachePath)
                                    self._ProjectFileInterface.saveDatasetCompressedCache ( self._saveOnCloseCacheFileName, cacheData, ProjectDataset = self._ProjectDataset)
                                finally:
                                    self._ProjectFileInterface.releaseFileLock ()
                                self._saveOnCloseCacheFileName = None 
                                self._saveOnCloseCachePath = None
                            except:
                                pass
                        self.save (DeleteOnSave = True) 
                    if self.isFileHandleLocked () :
                        self._unlockFile ()
                    self._removeFinalizer ()
                    self._closeEnabled = False
                    self._isClosed = True
                finally:
                    pass
                    #self._updateFileInDatasetUI (roiPath)
                
    def closeUnsaved (self) :
        self._closeUnsavedCalled = True
        if self._closeEnabled :             
            try:
                #roiPath = self._path
                self._path = None
                if self.isFileHandleLocked () :
                    self._unlockFile ()
                self._removeFinalizer ()
                self._closeEnabled = False
                self._isClosed = True
            finally:
                pass
                #self._updateFileInDatasetUI (roiPath)
            return
    
    def getDescriptionFromDataSource (self) :
        return None
    
    def updateTagsFromDataSource (self, datasetTags, RemoveExistingInternalTags = False) : 
        if (self._NIfTIPath is not None) :
            try:
                    NewInternalTagList = []            
                    try:                            
                        origionalPath = self._NIfTIPath      
                        origionalPath,_ = FileUtil.removeExtension (origionalPath, [".nii",".nii.gz"])
                        datafilePath = origionalPath + ".DicomTags"                                                
                        if os.path.exists (datafilePath) :
                            try :
                                fileDicomDataTags = pd.read_csv (datafilePath, header = 0)                 
                                for row in range (len (fileDicomDataTags["TagName"].values)) :
                                    try :
                                        tagName  = fileDicomDataTags["TagName"].values[row]
                                        tagValue = fileDicomDataTags["TagValue"].values[row]
                                        NewInternalTagList.append ((tagName, tagValue ))                                        
                                    except :
                                        print ("Could not add Tag: " + str (tagName))
                            except:
                                print ("A error occured trying to read tags from dataset: " + str (datafilePath))
                            return
                                            
                        origionalPathParts = origionalPath.split ("_")
                        if len (origionalPathParts) > 1 :
                            endsInInt = False
                            try:
                                int(origionalPathParts[-1])
                                endsInInt = True
                            except:
                                endsInInt = False
                            if endsInInt :
                                del origionalPathParts[-1]
                                origionalPath = "_".join (origionalPathParts)
                        datafilePath = origionalPath + "_info.txt"
                        if os.path.exists (datafilePath) :
                            try :
                                with open (datafilePath, "rt") as fileDicomDataTags :
                                    for row in fileDicomDataTags :                                        
                                        items = row.split (":")
                                        if len (items) >= 2 :
                                            tagName = items[0]
                                            tagName = tagName.strip ()
                                            if len (tagName) > 0 :
                                                del items[0]
                                                tagValue = ":".join (items)                                             
                                                tagValue = tagValue.strip ()
                                                NewInternalTagList.append ((tagName, tagValue ))                                                                                        
                            except:
                                print ("A error occured trying to read tags from dataset:" + str (datafilePath))
                            return
                    finally:
                        if len (NewInternalTagList) > 0 :
                            if RemoveExistingInternalTags :                                                                                                 
                                datasetTags.removeAllInternalTags (SafeNameSet = ['File owner', 'File status', 'Date acquired', 'Time acquired', "Assign comment", 'PLock', "ML_DatasetDescription"])
                            for newTagTuple in NewInternalTagList :
                                try:
                                    tagName, TagValue = newTagTuple
                                    datasetTags.setInternalTag (tagName,TagValue)
                                except:
                                    print ("A error occured trying to add tag: " + str (tagName))
            except:
                print ("A error occured trying to read tags")



def _save (Path, fileLockedTxt, DeleteOnSave, projectName, wasfilelockset,  Series = None, _PesscaraInterface = None, exam_code = "", series_code = "", ServerName = "", ProjectName = "", UserName = "", Password = "", DescriptionTacticContext = "", FileChangeMonitor = None):    
        try :
            if DescriptionTacticContext is None or DescriptionTacticContext == "":
                DescriptionTacticContext = ProjectName
                
            taticServer = None
            if (_PesscaraInterface is None) :                                
                _PesscaraInterface = RCC_PesscaraInterface ()                        
                #print (("ConnectingtoPesscra", ServerName, ProjectName, UserName, Password, None, None))
                _PesscaraInterface.setTaticServer (ServerName, ProjectName, UserName, Password, None, None) 
                
            if (Series is None) :                
                if (exam_code == "" or series_code == "") :
                    print ("Exam and Series Code not initalized")
                    return
                taticServer = _PesscaraInterface._login ()
                #print (("GetSeries", taticServer, exam_code, series_code))            
                SeriesFromPesscara = _PesscaraInterface.getSeries ( exam_code, series_code, taticServer = taticServer)
                if (len (SeriesFromPesscara) == 0):
                    print (("\n\nPesscara Series", SeriesFromPesscara))
                    print ("Could not save ROI Data to pesscara")
                    return
                Series = SeriesFromPesscara[0]
            
            if  (Path is not None and os.path.isfile(Path)) :
                try:  
                    print ("saving data to pesscara")
                    if (not wasfilelockset) :
                        print ("\n\nError file lock was not set trying to save ROI file!!!\n\n")
                    
                    if (taticServer is None) :
                        taticServer = _PesscaraInterface._login ()
                    
                    SaveFile = True
                    while (_PesscaraInterface.hasFileLockChanged (Series, projectName, fileLockedTxt, taticServer = taticServer)) :
                        print ("fileLockedTxt: " + str(fileLockedTxt))
                        username,ProcessID,datetime, hostName, ipAddress = _PesscaraInterface.getROIDataFileLockDetails (Series, projectName, taticServer = taticServer)                                   
                        if (ipAddress == "") :
                              ipAddress = "Unknown IP Address"
                        if (ProcessID == "") :
                              ProcessID = "Unknown Process ID"
                        if (hostName == "") :
                              hostName = "Unknown Hostname"
                        if (datetime == "") :
                              datetime = "Unknown Date/time"
                        if (username == "") :
                              username = "Unknown User"
                        lockdetails = "Selected ROI data has been modified by: " + username +"\nProcess ID:" +ProcessID+"\nRunning on host: "+ hostName+"("+ipAddress+")\nOpened on: " + datetime + "\n\nClicking 'yes' will overwrite their changes." 
                        if (not MessageBoxUtil.showWarningMessageBox (WindowTitle = "Saving ROI data Warrning", WindowMessage = "The ROI data file has been modified by another user after the file was opened. Do you wish to over write their changes (Yes/No)? Click details for more info.", Details = lockdetails)) :                     
                            SaveFile = False
                            break
                        else:
                            _PesscaraInterface.unlockDataSeries (Series, projectName, taticServer = taticServer) 
                        succeed = _PesscaraInterface.tryLockROIFile (Series, projectName, fileLockedTxt, taticServer = taticServer)
                        if (succeed) :
                            break
                            
                    if SaveFile :    
                        contoursExistForFile, fileheader = ROIDictionary.contoursExistForDataSet_ReturnHeader ( Path)                   
                        tempTagManager = DatasetTagManager ()     #get tags from datafile to send back to pesscara                
                        tempTagManager.loadTagDatasetFromFile (None, FileHeader = fileheader, AddInternalTags = False, AddOwnershipAndLockingInternalTags = False) #load the 
                        scanphase = ROIDictionary.getScanPhaseFromHeader (fileheader)
                        if (scanphase != "Unknown") :
                            contoursExistForFile = True
                        #print (("Saving, ContoursExist:", contoursExistForFile))
                        DescriptionMap = {}
                        DescriptionMap["D"] = scanphase                             
                                
                        unlockROI = (DeleteOnSave and wasfilelockset)
                        #print ("")
                        #print ("------------------------------")
                        print ("Unlock ROI: " + str (unlockROI))
                        print ("start set seriesROIData File")            
                        if FileChangeMonitor.hasFileChanged (Path) :      
                            SavePath = Path
                        else:
                            SavePath = None # Don't upload Dataset
                        _PesscaraInterface.setSeriesROIDataFile (Series, projectName, SavePath, contoursExistForFile, json.dumps(DescriptionMap), tempTagManager, UnlockROI = unlockROI, taticServer = taticServer, DescriptionTacticContext =DescriptionTacticContext)    
                        print ("done set seriesROIData File")   
                        #print ("------------------------------")
                        #print ("")
                except:                    
                    import traceback
                    traceback.print_stack ()                    
                    print ("Could not save datafile '%s' to Passcara." % (Path))      
                    print (("series", Series.name, "project name", projectName))
                    if DeleteOnSave and wasfilelockset :
                        try :
                             _PesscaraInterface.unlockDataSeries (Series, projectName)
                        except:
                             print ("Could not unlock data series in tatic")
                if DeleteOnSave :
                    try:
                        os.remove (Path)                                             
                    except :
                        print ("Error: Could not delete file: " + Path)
            else:
                """if Path is None :
                    print ("--------")
                    print ("Path is None Not saving")
                    print ("--------")
                else:
                    print ("--------")
                    print ("Path Does not exist: %s" % Path)
                    print ("--------")"""
                if DeleteOnSave and wasfilelockset :                            
                    try :
                        _PesscaraInterface.unlockDataSeries (Series, projectName)
                    except:
                        print ("Could not unlock data series in tatic")
        finally :
            print ("done saving roi")
            
class RCC_ROIDataset_PASSCARAObject :
    def __init__ (self, passcaraInterface,  series, projectname,  path, filelocked, fileLockTxt, DataSetTreeNode, ProjectDataset, OpenFileAsReadOnly = "False",   EnableDestructor = True):        
        #super (RCC_ROIDataset_PASSCARAObject, self).__init__ (EnableDestructor)
        self._closeEnabled = True
        self._closeCalled = False
        self._closeUnsavedCalled = False
        self._fileChangeMonitor = FileChangeMonitor (path)
        self._EnableDestructor = EnableDestructor
        self._OpenFileAsReadOnly = OpenFileAsReadOnly # Possible Values: "SoftReadOnly", "False", "HardReadOnly" 
        self._passcaraInterface = passcaraInterface        
        self._series = series            
        self._DocumentUID = None
        self._projectname = projectname        
        self._path = path         
        self._wasfilelockset = filelocked
        if not filelocked : 
            self._OpenFileAsReadOnly   = "HardReadOnly"
        self._fileLockedTxt = fileLockTxt
        self._savingProcess = None
        self._dataset_treeNode = DataSetTreeNode
        self._ProjectDataset = ProjectDataset        
        self._FileHandleProjectDescriptionContext = self._ProjectDataset.getDescriptionContext () # save the project description at the time of handle creation so changes to description context do not cause tactic data to be improperly overwritten when old file handles are closed and reloaded to refect the handle change.
        self._finalizer = None   
        self._isClosed = False
        if self._EnableDestructor :
            if PythonVersionTest.IsPython3 () :
                self._finalizer = weakref.finalize (self, RCC_ROIDataset_PASSCARAObject._finalizeObj, self._path, self._ProjectDataset, self._fileLockedTxt, self._projectname, self._wasfilelockset, self._series, self._passcaraInterface, self._FileHandleProjectDescriptionContext, self._fileChangeMonitor)
     
    def getFileTreePath (self) :
        return self._dataset_treeNode.getTreePath ()   

    def flushFileToDisk (self) :
         if (self._ProjectDataset is not None) :
             self._ProjectDataset.getROISaveThread ().getGlobalFileSaveThread ().blockingFileClose (self._path)         
         return True
     
    def getFilePermissions (self) :        
        return self._ProjectDataset.getProjectFileInterface().getFileWritePermissions ()
    
    def isClosed (self) :
        return self._isClosed
    
    def getProjectDataset (self) :
        return self._ProjectDataset
    
    def _closeHandleCallback (self) :
        RCC_ROIDataset_PASSCARAObject._finalizeObj (self._path, self._ProjectDataset, self._fileLockedTxt, self._projectname, self._wasfilelockset, self._series, self._passcaraInterface, self._FileHandleProjectDescriptionContext, self._fileChangeMonitor)
        
    def copyForChildProcessCopyDisableFinalizer (self) : #disable file unlocking in finalizer
        fileCopy = RCC_ROIDataset_PASSCARAObject (self._passcaraInterface, self._series, self._projectname, self._path, self._wasfilelockset, self._fileLockedTxt, self._dataset_treeNode, self._ProjectDataset, self._OpenFileAsReadOnly,  EnableDestructor = False)
        fileCopy._DocumentUID =  self._DocumentUID
        fileCopy._FileHandleProjectDescriptionContext = self._FileHandleProjectDescriptionContext  
        self._closeEnabled = False  
        fileCopy._closeEnabled = False              
        return fileCopy
    
    
    
    def enableClose (self) :
        self._closeEnabled = True
        if self._closeCalled :
            self.close ()
        if self._closeUnsavedCalled :
            self.closeUnsaved ()
    
    def suppportsPersistentFileLocks (self) :
        return False
    
    def canVersionFile (self):
        return False
    
    def setDocumentUID (self, uid) :
        self._DocumentUID  =  uid
    
    def getDocumentUID (self) :
        return self._DocumentUID 
         
    def _removeFinalizer (self) :
        if self._finalizer is not None :            
            self._finalizer.detach()
            self._finalizer = None 
            
    def isReadOnly (self) :
         self._OpenFileAsReadOnly != "False" 
         
    if PythonVersionTest.IsPython2 () :
        def __del__ (self) :
            if self._EnableDestructor :
                RCC_ROIDataset_PASSCARAObject._finalizeObj (self._path, self._ProjectDataset, self._fileLockedTxt, self._projectname, self._wasfilelockset, self._series, self._passcaraInterface, self._FileHandleProjectDescriptionContext, self._fileChangeMonitor)
    
    def setOpenFileReadOnlyStatus (self, val) :
        self._OpenFileAsReadOnly = val
          
        
    def getFileHandleReadOnlyStatus (self) :
        return self._OpenFileAsReadOnly
    
    @staticmethod
    def _finalizeObj (path, ProjectDataset, fileLockedTxt, projectname, wasfilelockset, series, passcaraInterface, FileHandleProjectDescriptionContext, fileChangeMonitor):
        if (path is not None and os.path.isfile (path)) :
                RCC_ROIDataset_PASSCARAObject.static_save (path, ProjectDataset, fileLockedTxt, projectname, wasfilelockset, series, passcaraInterface, FileHandleProjectDescriptionContext, DeleteOnSave = True, FileChangeMonitor = fileChangeMonitor)            
            
    def _getFileHandleProjectDescriptionContext (self) :
        return self._FileHandleProjectDescriptionContext
    
    def setSaveCacheOnCloseCachePath (self, filename, cachePath) :
         return # do nothing
     
    def queueFileSave (self, data) :
        self._ProjectDataset.getROISaveThread ().getGlobalFileSaveThread ().queueFileSave (self._path, data, self.fileSaveThreadCallback, FileHandle = self)        
    
    def isFileHandleLocked (self) :    
        return True
    
    
    def getCurrentFilePLock (self) :
        return None 
    
    def getHandleType (self) :
        return "Tactic"
    
    def getDescriptionFromDataSource (self) :
        try:
            scanDescriptionTag = "RILC_"+ self._getFileHandleProjectDescriptionContext()  +"_Description"
            series = self.getSeries () 
            if scanDescriptionTag in series.tags  :
                value = series.tags[scanDescriptionTag]
                value = json.loads (value)
                if "D" in value :
                    return value["D"]
            return None
        except:
            return None
            
           
    def updateTagsFromDataSource (self, datasetTags, RemoveExistingInternalTags = False) :
       # Remove existing internal tags only necessary for data comming from the file system. Ignored for Pesscara
       series = self.getSeries ()       
       for  tag in datasetTags.getTags () :
           if (tag.storeTagInTatic ()) :
               tagname = tag.getName ()
               if (tag.shouldMangleTagNameWithTaticContext ()) :
                   context = "RILC_"+ tag.getTagTacticContext(self.getProjectName ())
                   tagname = context + "_" + tagname    
               if tagname in series.tags  :
                   tagvalue = series.tags[tagname]
                   tag.setValue (tagvalue)
                   customTagIndex = datasetTags.getCustomTagIndex (tag)                   
                   if (tag.getType() == "ItemList") :
                       pList = tag.getParameterList ()
                       if tagvalue not in pList :
                           pList.append (tagvalue)
                   datasetTags.setCustomTagByIndex (customTagIndex, tag)       
                   
    def getProjectName (self) :
        return self._projectname
    
    def getSeries (self) :
        return self._series
        
    def getPath (self) :
        return self._path
    
    def hasFileChangedOnServer (self) :
        return self._passcaraInterface.hasFileLockChanged (self._series, self._projectname, self._fileLockedTxt)
        
    _save = staticmethod (_save)
        
    def fileSaveThreadCallback (self, path, ReturnRunningAsyncProcess = False) :
        DeleteOnSave = False
        if (not ReturnRunningAsyncProcess) :            
            RCC_ROIDataset_PASSCARAObject._save (self._path, self._fileLockedTxt, DeleteOnSave, self._projectname, self._wasfilelockset,  Series = self._series, _PesscaraInterface= self._passcaraInterface, exam_code = "", series_code = "",  ServerName = "", ProjectName = "", UserName = "", Password = "", DescriptionTacticContext = self._getFileHandleProjectDescriptionContext(), FileChangeMonitor = self._fileChangeMonitor)
            #print ("Saving: %s to the database using blocking method" % (path))
            if self._EnableDestructor and PythonVersionTest.IsPython3 () and os.path.exists (self._path) :
                self._removeFinalizer ()
                self._finalizer = weakref.finalize (self, RCC_ROIDataset_PASSCARAObject._finalizeObj, self._path, self._ProjectDataset, self._fileLockedTxt, self._projectname, self._wasfilelockset, self._series, self._passcaraInterface, self._FileHandleProjectDescriptionContext, self._fileChangeMonitor)
            return None
        else:          
            # Async saving does not work on windows becouse windows fork crashes attempting to correclty fork the tactic library.
            # disable code is commented out below.  reverting to non-async implementation
            RCC_ROIDataset_PASSCARAObject._save (self._path, self._fileLockedTxt, DeleteOnSave, self._projectname, self._wasfilelockset,  Series = self._series, _PesscaraInterface= self._passcaraInterface, exam_code = "", series_code = "",  ServerName = "", ProjectName = "", UserName = "", Password = "", DescriptionTacticContext = self._getFileHandleProjectDescriptionContext(), FileChangeMonitor = self._fileChangeMonitor)
            #print ("Saving: %s to the database using blocking method" % (path))
            if self._EnableDestructor and PythonVersionTest.IsPython3 () and os.path.exists (self._path) :
                self._removeFinalizer ()
                self._finalizer = weakref.finalize (self, RCC_ROIDataset_PASSCARAObject._finalizeObj, self._path, self._ProjectDataset, self._fileLockedTxt, self._projectname, self._wasfilelockset, self._series, self._passcaraInterface, self._FileHandleProjectDescriptionContext, self._fileChangeMonitor)
            return None              
                        
            #savingProcess = multiprocessing.Process (target=RCC_ROIDataset_PASSCARAObject._save, args= (self._path, self._fileLockedTxt, DeleteOnSave, self._projectname, ForceSilentOverwrite, self._wasfilelockset,  None, None, self._series.exam_code, self._series.code, self._passcaraInterface.getServerName (), self._passcaraInterface.getProject (), self._passcaraInterface.getUser (), self._passcaraInterface.getPassword (), self._getFileHandleProjectDescriptionContext()))
            #savingProcess.start ()
            #print ("Saving: %s to the database using non-blocking async process" % (path))
            #return savingProcess      
            
    @staticmethod        
    def static_save (path, ProjectDataset, fileLockedTxt, projectname, wasfilelockset, series, passcaraInterface, FileHandleProjectDescriptionContext, DeleteOnSave = False, FileChangeMonitor = None) :            
        if (path is not None) :
            try :        
                #print ("Static Save Path: " + path)                   
                ProjectDataset.getROISaveThread ().getGlobalFileSaveThread ().blockingFileClose (path)                
            except: 
                pass 
                #print ("Warning a error occured trying to close Pescara_ROIRef: " % self.getPath ())
        print ("RCC_ROIDataset_PASSCARAObject.static_save")
        #self._dataset_treeNode 
        if (not DeleteOnSave) :            
            RCC_ROIDataset_PASSCARAObject._save (path, fileLockedTxt, DeleteOnSave, projectname, wasfilelockset,  Series = series, _PesscaraInterface= passcaraInterface, exam_code = "", series_code = "",  ServerName = "", ProjectName = "", UserName = "", Password = "", DescriptionTacticContext = FileHandleProjectDescriptionContext, FileChangeMonitor = FileChangeMonitor)
        else :                      
            RCC_ROIDataset_PASSCARAObject._save (path, fileLockedTxt, DeleteOnSave, projectname, wasfilelockset,  None, None, series.exam_code, series.code, passcaraInterface.getServerName (), passcaraInterface.getProject (), passcaraInterface.getUser (), passcaraInterface.getPassword (), DescriptionTacticContext = FileHandleProjectDescriptionContext, FileChangeMonitor = FileChangeMonitor)
            
        
    def save (self, DeleteOnSave = False) :            
        RCC_ROIDataset_PASSCARAObject.static_save (self._path, self._ProjectDataset, self._fileLockedTxt, self._projectname, self._wasfilelockset, self._series, self._passcaraInterface, self._FileHandleProjectDescriptionContext, DeleteOnSave = DeleteOnSave, FileChangeMonitor = self._fileChangeMonitor)            
        if self._EnableDestructor and PythonVersionTest.IsPython3 () and os.path.exists (self._path) :
            self._removeFinalizer ()
            self._finalizer = weakref.finalize (self, RCC_ROIDataset_PASSCARAObject._finalizeObj, self._path, self._ProjectDataset, self._fileLockedTxt, self._projectname, self._wasfilelockset, self._series, self._passcaraInterface, self._FileHandleProjectDescriptionContext, self._fileChangeMonitor)
     
        
    def isSavingProcessRunning (self) :
        if (self._savingProcess is not None) :
            return self._savingProcess.is_alive ()
        return False

    def close (self) :    
        self._closeCalled = True
        if self._closeEnabled :
            self.save (DeleteOnSave = True) 
            self._removeFinalizer ()
            self._closeEnabled = False
            self._isClosed = True
            
    def closeUnsaved (self) :
        self._closeUnsavedCalled = True
        if self._closeEnabled :
            try:
                if (self._path is not None and os.path.isfile(self._path)):
                    os.remove (self._path)                
                if (self._wasfilelockset):                
                    self._passcaraInterface.unlockDataSeries (self._series, self._projectname)
            except :
                print ("Error: Could not delete file: " + self._path)
            self._path = None
            self._removeFinalizer ()
            self._closeEnabled = False
            self._isClosed = True

class RCC_ROIDataset_FileSystemInterface :
   def __init__ (self, path = "ROIDatasets", FObject = None, ProjectName = "DefaultDataset", ProjectDataset = None, RILContourCommandLine = None) :                    
        self._ROIDatasetRootPath  = path
        self._DatasetFileMapping = {}  
        self._ProjectName = ProjectName
        self._ProjectDataset = ProjectDataset
        self._relativeROIDatasetRootPathList = None
        self._RILContourCommandLine = RILContourCommandLine
        
        if (FObject is not None) :
            if (FObject.hasParameter ("ROI_Dataset_Path")) :
                self._ROIDatasetRootPath = FObject.getParameter ("ROI_Dataset_Path") 
            else:
                MessageBoxUtil.showMessage ("Root ROI Search Path Not Found", "The loaded project file is misssing the ROI_Dataset_Path entry or has a malformed ROI_Dataset_Path entry.")
            if (FObject.hasParameter ("ROI_Dataset_Name")) :
                self._ProjectName = FObject.getParameter ("ROI_Dataset_Name") 
            if (FObject.hasParameter ("ROI_Relative_Dataset_Path_List")) :
                self._relativeROIDatasetRootPathList = RelativePathUtil.relativePathListCopy (FObject.getParameter ("ROI_Relative_Dataset_Path_List"))
         
        self._ROIDatasetRootPath = FileUtil.correctOSPath (self._ROIDatasetRootPath)
        
                     
   def setROIDatasetRootPath (self, path) :
       self._ROIDatasetRootPath = path
    
   def setTempDir (self, path) : # Do Nothing
        return
    
   def getTaticTagValue (self, selectedDataSet, tagName, oldValue) : 
       print ("FileSystem Interface Error: This should never be called")
       return oldValue
   
   def isPesscaraInterface (self) :
       return False
   
 
   def getFileObject (self) :
         fobj = FileObject ("RCC_ROIDataset_FileSystemInterface")      
         fobj.setParameter ("ROI_Dataset", "FILESYSTEM") 
         fobj.setParameter ("ROI_Dataset_Path", self._ROIDatasetRootPath)          
         fobj.setParameter ("ROI_Dataset_Name", self._ProjectName)
         fobj.setParameter ("ROI_Relative_Dataset_Path_List", RelativePathUtil.relativePathListCopy (self._relativeROIDatasetRootPathList ))      
         return fobj          
             
   def getROIDatasetRootPath (self) :
       if self._RILContourCommandLine is not None and "roirootdir" in self._RILContourCommandLine:
          return self._RILContourCommandLine["roirootdir"]
       return self._ROIDatasetRootPath
           
   def updateRelativePathIfNecessary (self, projectPath) :
       try :
           if (os.path.exists (self._ROIDatasetRootPath) and os.path.isdir (self._ROIDatasetRootPath)) :
               relativePathList = RelativePathUtil.getRelatePathListFromFullPath (self._ROIDatasetRootPath, projectPath)
               if not RelativePathUtil.doRelativePathListMatch (relativePathList, self._relativeROIDatasetRootPathList) :
                   self._relativeROIDatasetRootPathList = relativePathList
                   print ("Updating relative ROI dataset path")
                   return True
           else:
               if (self._relativeROIDatasetRootPathList is None) :
                   fullPath = projectPath
               else:
                   fullPath = RelativePathUtil.getFullPathFromRelativePathList (self._relativeROIDatasetRootPathList, projectPath)
               if (fullPath is not None and os.path.exists (fullPath) and os.path.isdir (fullPath)) :
                   self._ROIDatasetRootPath = fullPath
                   print ("Updating ROI dataset full path from relative path: " + fullPath)
                   return True
       except:
           pass
       return False
   
   def copy (self) :
       newDS = RCC_ROIDataset_FileSystemInterface ()
       newDS._ROIDatasetRootPath = self._ROIDatasetRootPath
       newDS._DatasetFileMapping = {}
       newDS._RILContourCommandLine  = self._RILContourCommandLine       
       newDS._ProjectDataset = self._ProjectDataset
       newDS._relativeROIDatasetRootPathList = RelativePathUtil.relativePathListCopy (self._relativeROIDatasetRootPathList)
       KeyValuePairs = self._DatasetFileMapping.items ()
       for key, value in KeyValuePairs :            
           newDS._DatasetFileMapping[key] = value
            
   @staticmethod 
   def MP_GetDirList (tpl) : 
       basePath,TreePath, directoryfiles = tpl
       if (TreePath != "") :
           filePath = os.path.join (basePath, TreePath)           
       else:
           filePath = basePath
       try:
           directoryfilesIterator = ScanDir.scandir(filePath)
           for dirFile in directoryfilesIterator :
               if dirFile.is_dir() :
                   if dirFile.name != "v" :
                       directoryfiles.append (dirFile)
               elif dirFile.path.endswith ("_ROI.txt") :                
                   directoryfiles.append (dirFile)
           return
       except:
           print ("Could not list files in ROI path: " + filePath)           
           if (not os.path.isdir (filePath)):
               try:
                   FileUtil.createPath (filePath) 
                   if (not os.path.isdir (filePath)):
                       print ("Could not make directory: " + filePath)                              
                   else:
                       print ("Creating directory: " + filePath)                              
               except:
                   print ("Could not make directory: " + filePath)                              
           return 
       finally:
           try:
               directoryfilesIterator.close ()
           except:
               pass
       return
            
   @staticmethod
   def sm_loadDataset (basePath,TreePath, App, ProgressDialog, directoryfileslist, DatasetFileMapping) :                
       dirList = []                                
       for dirFile in directoryfileslist :           
           if dirFile.is_dir() :               
                dirList.append ([basePath, os.path.join (TreePath, dirFile.name), []])
           else:                
               roiFilePath = os.path.join (TreePath, dirFile.name)               
               niftiDataset = FileUtil.changeExtention (roiFilePath, "_ROI.txt", "")                                                                                                                                                                 
               DatasetFileMapping[niftiDataset] = roiFilePath         
                     
       length = len (dirList)
       if length > 0 :        
           if (TreePath != "") :
               filePath = os.path.join (basePath, TreePath)           
           else:
               filePath = basePath                          
           if ProgressDialog is not None :
               ProgressDialog.setMessage ("Initializing: " + filePath)           
           if App is not None :
               App.processEvents ()
           PlatformThreading.ThreadMap (RCC_ROIDataset_FileSystemInterface.MP_GetDirList, dirList, App = App, SleepTime = 0.05, Debug = False, ThreadMax = 32)           
           for dirEntry in dirList :
               basePath,TreePath, directoryfiles = dirEntry
               if len  (directoryfiles) > 0 :
                   RCC_ROIDataset_FileSystemInterface.sm_loadDataset (basePath, TreePath, App, ProgressDialog, directoryfiles, DatasetFileMapping)

       
       
       
                                                              
   def loadDataset (self, path = None, App = None, progressDialog = None) :            
      if (path != None) :
           self._ROIDatasetRootPath = path            
      self._DatasetFileMapping = {}             
      if self._ROIDatasetRootPath is None :
          return
      startDir= [self._ROIDatasetRootPath, "", []]
      RCC_ROIDataset_FileSystemInterface.MP_GetDirList (startDir)      
      if self._ROIDatasetRootPath is None :
          progressDialog.setMessage ("Initializing: " + self._ROIDatasetRootPath)     
      if App is not None :
               App.processEvents ()      
      if len (startDir[2]) > 0 :
          RCC_ROIDataset_FileSystemInterface.sm_loadDataset (self._ROIDatasetRootPath, "", App, progressDialog, startDir[2], self._DatasetFileMapping)
      
   def _ifNecessaryCreateDirectorysForFilePath (self, filePath) :                                         
        FileUtil.createPath (filePath, True)
   
   def getROIDataFilePathForExistingFile (self, DataSetTreeNode, Exists = None) :
       filepath = DataSetTreeNode.getFilesystemROIDataPath ()   
       key_filepath,_ = FileUtil.removeExtension (filepath, [".nii",".nii.gz"])
       filepath =  key_filepath + "_ROI.txt"                  
       full_filepath = os.path.join (self.getROIDatasetRootPath () , filepath)     
       if Exists is None :
           Exists = os.path.isfile (full_filepath) 
       if Exists :
           self._DatasetFileMapping[key_filepath] = filepath 
           return full_filepath, True
       else:
           return full_filepath, False
       
       
   def getROIDataFilePathForDatasetPath (self, DataSetTreeNode):            
       #ForceCloseOpenPLocks = False  
       filepath, exists = self.getROIDataFilePathForExistingFile (DataSetTreeNode)  
       if not exists :
           self._ifNecessaryCreateDirectorysForFilePath (filepath) 
       return filepath
   
   def getROIDataForDatasetPath (self, DataSetTreeNode, UniqueName = "", IgnoreLock = False, UserNameStr ="", SilentIgnoreOnLockFail = False, SuperUserFroceSilentGrabLock = False, Verbose = True):            
       #ForceCloseOpenPLocks = False
       filepath = self.getROIDataFilePathForDatasetPath (DataSetTreeNode)
       
       if IgnoreLock :
           OpenFileAsReadOnly = "HardReadOnly"
       else:
           OpenFileAsReadOnly = "False"
       FileSystemHandle = RCC_ROIDataset_FileSystemObject (filepath, NIfTIPath = DataSetTreeNode.getPath(),  ProjectDataset = self._ProjectDataset, ProjectFileInterface = self._ProjectDataset.getProjectFileInterface (), OpenFileAsReadOnly = OpenFileAsReadOnly, EnableDestructor = True, SuperUserFroceSilentGrabLock = SuperUserFroceSilentGrabLock, SilentIgnoreOnLockFail = SilentIgnoreOnLockFail, CreatingCopyForChildProcess = False, Verbose = Verbose, TreePath = DataSetTreeNode.getTreePath())
       if  FileSystemHandle.isClosed ():
           return None
       return FileSystemHandle
   
                        
   
   def hasROIDatafileForNiftiDataset (self, DataSetTreeNode):       
       filePath = DataSetTreeNode.getFilesystemROIDataPath ()
       filePath,_ = FileUtil.removeExtension (filePath, [".nii",".nii.gz"])
       return filePath in self._DatasetFileMapping
   
   def getROIDataFilePathForNiftiDataset (self, DataSetTreeNode):       
       filePath = DataSetTreeNode.getFilesystemROIDataPath ()
       filePath,_ = FileUtil.removeExtension (filePath, [".nii",".nii.gz"])
       return os.path.join (self._ROIDatasetRootPath, filePath + "_ROI.txt")
   
   def getProjectName (self) :
       return self._ProjectName


      
#simple interface to save the dataset back to the database
class RCC_ROIDataset_PASSCARA :
   def __init__ (self, PesscaraInterface = None, ProjectName = "", FObject = None, ProjectDataset = None) :                                   
       self._ProjectDataset = ProjectDataset
       self._PesscaraInterface  = PesscaraInterface
       self._ProjectName        = ProjectName       
       self._OpenNodeSavingObjects = {}
       if (FObject is not None) :             
             self._PesscaraInterface = RCC_PesscaraInterface (FObject = FObject.getFileObject ("RCC_PesscaraInterface"))                                       

   def setTempDir (self, path) : 
        self._PesscaraInterface.setTempDir (path)
        
   def isPesscaraInterface (self) :
       return True
   
   def getProjectName (self) :
       return self._ProjectName
       
   def getFileObject (self):
       fobj = FileObject ("RCC_ROIDataset_PESSCARAInterface")
       fobj.addInnerFileObject (self._PesscaraInterface.getFileObject ())             
       return fobj
   
   def getTaticTagValue (self, selectedDataSet, tagName, oldValue) : 
       try :
           if (selectedDataSet is not None) :
               Series  = selectedDataSet.getSeries  ()  
               if tagName in Series.tags :
                   return Series.tags[tagName]               
           return oldValue
       except:           
           return oldValue             
                            
         
   def queueSavingPesscaraProcess (self, DataSetTreeNode, PASSCARAObject):
       if DataSetTreeNode in self._OpenNodeSavingObjects :
           existingObject = self._OpenNodeSavingObjects[DataSetTreeNode]    
           loopcount = 1
           while existingObject.isSavingProcessRunning () :
                   if (loopcount >= 10) :
                       print ("Waiting for ROI to Finish Saving")
                       loopcount = 1
                   time.sleep (1) # wait for saving to finish
                   loopcount += 1
       self._OpenNodeSavingObjects[DataSetTreeNode] = PASSCARAObject
       
   def getROIDataForDatasetPath (self, DataSetTreeNode, UniqueName = "", IgnoreLock = False, UserNameStr ="", SilentIgnoreOnLockFail = False, SuperUserFroceSilentGrabLock = False, Verbose = True): #UniqueName can be used to augment temporary file name generator       
       
       removeLst = []
       for key, value in self._OpenNodeSavingObjects.items () :
           if key == DataSetTreeNode :           
               loopcount = 1
               while value.isSavingProcessRunning () :
                   if (loopcount >= 10) :
                       print ("Waiting for ROI to Finish Saving")
                       loopcount = 1
                   time.sleep (1) # wait for saving to finish
                   loopcount += 1
           if not value.isSavingProcessRunning () :
               removeLst.append (key)               
       for key in removeLst :
           del self._OpenNodeSavingObjects[key]
           
       try:
           ProcessIDStr =  str(os.getpid())
       except :
           ProcessIDStr = "CouldNotGetProcessID"            
       Series  = DataSetTreeNode.getSeries  ()                     
       try:    
           taticServer = self._PesscaraInterface._login ()                                             
           while (True) :              
               ROITempPath, fileLocked, fileLockedHeader = self._PesscaraInterface.getSeriesROIDataFile (Series, self._ProjectName, UniqueName, UserNameStr = UserNameStr, ProcessIDStr = ProcessIDStr, taticServer = taticServer)          
               if (not fileLocked and not IgnoreLock and not SilentIgnoreOnLockFail) :               
                  username,ProcessID,datetime, hostName, ipAddress = self._PesscaraInterface.getROIDataFileLockDetails (Series, self._ProjectName, taticServer = taticServer)                
                  if (ipAddress == "") :
                      ipAddress = "Unknown IP Address"
                  if (ProcessID == "") :
                      ProcessID = "Unknown Process ID"
                  if (hostName == "") :
                      hostName = "Unknown Hostname"
                  if (datetime == "") :
                      datetime = "Unknown Date/time"
                  if (username == "") :
                      username = "Unknown User"
                  lockdetails = "Selected ROI data has been opened by: " + username +"\nRunning process ID:" +ProcessID+"\nOn host: "+ hostName+"("+ipAddress+")\nOpened on: " + datetime + "\n\nClicking 'Yes' will open the dataset and allow the ROI data file to be modified.\n\nHowever, if the data file is currently opend by " + username + " then modifcations to the ROI by either you or " + username + " will be lost. Proceed with caution. It is recommend that you contact " + username + " and check if they are running rilcontour and have the dataset open."                                                                                                                                                                                
                  unlockDataset = MessageBoxUtil.showWarningMessageBox (WindowTitle = "Loading ROI Data Warrning", WindowMessage = "The selected ROI data file has been opened by another user. Do you wish to open the file anyway (Yes/No)? Choosing 'Yes' may result in data loss. Click details for more info.", Details = lockdetails)                
                  if (unlockDataset) :
                      self._PesscaraInterface.unlockDataSeries (Series, self._ProjectName, taticServer = taticServer)
                  else:    
                      return None
               else:
                   break
               
           if (ROITempPath == None) :
               ROITempPath = self._PesscaraInterface.createSeriesROIDataFile (UniqueName)                     
       except :           
           ROITempPath = self._PesscaraInterface.createSeriesROIDataFile (UniqueName)                     
       return RCC_ROIDataset_PASSCARAObject (self._PesscaraInterface, Series, self._ProjectName, ROITempPath, fileLocked, fileLockedHeader, DataSetTreeNode, self._ProjectDataset)
                   
          
                                    
   def copy (self) :
       return RCC_ROIDataset_PASSCARA (self._PesscaraInterface, self._ProjectName, ProjectDataset = self._ProjectDataset)
              
   def getROIDatasetRootPath (self)  :
       return "Pesscara"
       
             
      
