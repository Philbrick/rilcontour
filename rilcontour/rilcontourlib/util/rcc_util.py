# -*- coding: utf-8 -*-
"""
Created on Thu Sep 15 14:24:12 2016

@author: Philbrick
"""
#import platform
import sys
import pandas as pd
import numpy as np
import collections
import json
import os
from uuid import uuid4
import shutil
import weakref
import math
import time
import scipy
import nibabel as nib
import filecmp
from six import *
import copy
import pickle
import binascii
import datetime
import skimage
import skimage.io
from datetime import date
from skimage import measure
from skimage.draw import polygon
from PyQt5 import QtCore, QtGui
from PyQt5.QtWidgets import  QDialog, QListWidgetItem, QFileDialog, QMessageBox
from rilcontourlib.ui.qt_ui_autogen.rcc_volumecsvexportAutoGen import Ui_rcc_ROIVolumeMaskExportDlg
from rilcontourlib.ui.qt_ui_autogen.RCC_NoCancelProgressDlgAutogen import Ui_RCC_NoCancelProgressDlg
from rilcontourlib.ui.qt_ui_autogen.rcc_multilinemessageboxautogen import Ui_MultiLineMessageBoxDlg
from rilcontourlib.dataset.RCC_ROIFileLocks import  NonPersistentFSLock
import pydicom
#from scipy.spatial.distance import pdist
from scipy import ndimage 
#from scipy.spatial.distance import squareform       
from threading import Thread, RLock, Condition, Lock
import multiprocessing
from rilcontourlib.util.PandasCompatibleTable import PandasCompatibleTable
from rilcontourlib.util.FileUtil import FileUtil
from rilcontourlib.util.NibabelDataAccessHelper import NibabelDataAccessHelper
from rilcontourlib.util.DateUtil import DateUtil, TimeUtil
import tempfile
from rilcontourlib.util.FileUtil import ScanDir
from rilcontourlib.util.LongFileNameAPI import LongFileNameAPI
from multiprocessing.pool import ThreadPool as MP_Pool    
import subprocess


try:    
    from StringIO import StringIO 
    import cStringIO    
    _usecStringIO = True
except ImportError:
    _usecStringIO = False
    from io import StringIO

"""# Method to show slices can take a tupil of slices.
import matplotlib.pyplot as plt
def show_slices (slices) :
    fig, axes = plt.subplots (1, len (slices))
        if len (slices) > 1 :
            for i, slice in enumerate (slices):
                axes[i].imshow (slice.T, cmap="gray", origin="lower")
        else:
            axes.imshow (slices[0].T, cmap="gray", origin="lower")
        plt.suptitle ("Slices")
        plt.tight_layout ()
    plt.show ()
"""

class FastUnique :    
    @staticmethod
    def unique (values, MaxInt = None, ZeroMem = None) :     
        try :
            if MaxInt is None :        
                if np.issubdtype(values.dtype, np.integer) :            
                    if not np.issubdtype(values.dtype, np.signedinteger) or np.min (values) >= 0 :                
                        maxV = np.max (values)
                        if maxV <= 10000 :
                            MaxInt = maxV + 1                            
            if MaxInt is not None :        
                if MaxInt == 1 :
                    if np.any (values) :
                        return np.arrary ([0,1],dtype = values.dtype)
                    else:
                        return np.arrary ([0],dtype = values.dtype)
                if ZeroMem is None or ZeroMem.shape[0] != MaxInt :
                    zeroVals = np.zeros ((MaxInt,), dtype=np.bool)
                else:
                    zeroVals = ZeroMem
                    zeroVals[...] = False
                zeroVals[values.flatten ()] = True
                return (zeroVals.nonzero ()[0]).astype (values.dtype)
        except:
            pass
        try:
            return np.sort(pd.unique (values.flatten ()))        
            origionalShape = values.shape
            shapeArray = (np.array(list (origionalShape))).astype (np.uint)
            elements = np.prod (shapeArray)
            return np.sort(pd.unique (values.reshape ((elements,))))
        except:
            return np.unique (values)


def isDICOM2NIfTIPackageInstalled () :
    return True
    """try :
        import dicom2nifti 
        return True
    except:
        return False"""


class PythonVersionTest :    
    @staticmethod    
    def IsPython2 () :
        return sys.version_info[0]  == 2
    
    @staticmethod    
    def IsPython3 () :
        return sys.version_info[0]  >= 3

class RCC_NoCancelProgressDlg (QDialog) :
    def __init__ (self, parent) :        
        QDialog.__init__ (self, None)
        self.ui = Ui_RCC_NoCancelProgressDlg ()        
        self.ui.setupUi (self)          
       
        self.setWindowFlags(QtCore.Qt.Window | QtCore.Qt.WindowStaysOnTopHint  | QtCore.Qt.WindowTitleHint| QtCore.Qt.CustomizeWindowHint)    
        
        self.setWindowModality(QtCore.Qt.ApplicationModal)        
        self.ui.ProgressBar.setRange (0, 100)
        
    def setValue (self, val) :
        self.ui.ProgressBar.setValue (val)
   
    def setMinimum(self, val) :
        self.ui.ProgressBar.setMinimum (val)
        
    def setMaximum (self, val) :
        self.ui.ProgressBar.setMaximum (val)
        
    def setRange (self, v1, v2):
       self.ui.ProgressBar.setRange (v1, v2)
       
    def setLabelText (self, txt):
        self.ui.Label.setText (txt)
        
    def closeWindow (self):
        self.close ()

class TimeLog :        
    def __init__ (self) :        
        self._timeLog = []
        self._currentUser = None
        self._activeLog = None
        
    def getTimeLog (self) :        
        logCopy = []
        for timeLogItem in self._timeLog :
            objDupe = {}
            for key, value in timeLogItem.items ():
                objDupe[key] = value
            logCopy.append (objDupe)
        if self._activeLog is not None :
            objDupe = {}
            for key, value in self._activeLog.items ():
                objDupe[key] = value
            logCopy.append (objDupe)            
        return logCopy
            
    def loadFromFileObject (self, fileObject) :
        try :            
            self._timeLog = fileObject.getParameter ("TimeLog")
        except :
            self._timeLog = []        
        if len (self._timeLog) == 0 :
            print ("Time log is empty.")
        else:
            TotalTime = 0
            for log in self._timeLog :
                TotalTime += log["ElapsedTime"]
            print ("File has been edited for: %f seconds" % TotalTime)
        
    def getFileObject (self, user, activityTimer) :
        if self._currentUser != user :        
            if (self._activeLog is not None) :
                self._timeLog.append (self._activeLog)
            if user is not None :
                self._activeLog = activityTimer.getNewTimeLog (user)
                self._currentUser = user
            else:
                self._activeLog = None
                self._currentUser = None
        elif (self._activeLog is not None) :
            activityTimer.updateTimeLog (self._activeLog)
        
        SaveLog = copy.copy (self._timeLog)
        if self._activeLog is not None :
            SaveLog.append (self._activeLog)
            
        fobj = FileObject ("FileChangeTimeLog")
        fobj.setParameter ("TimeLog", SaveLog)        
        return fobj
        
    
class ActivityTimer :
    def __init__ (self) :                
        self._StartSessionDate = ActivityTimer._getDateStruct (date.today ())
        self._StartSessionTime = time.time () #time in seconds
        self._minTimeUpdateInMin = 3        
        self._ElapsedTime = 0 
        currentTime = time.time ()        
        self._StartTimer = currentTime
        self._LastUpdate = currentTime  
        
        self._ElapsedPauseTimeInSec = 0
        self._PauseStart = None
        self._PauseTimerCounter = 0
    
    @staticmethod
    def _getDateStruct (tm):
        timeob = {}
        timeob["year"]  = tm.year
        timeob["month"] = tm.month
        timeob["day"]   = tm.day        
        return timeob
        
    def pauseTimer (self) :
        if not self.isTimerPaused ():
             self._ElapsedTime = self.getElapsedTime ()
             self._PauseStart = time.time ()
             self._PauseTimerCounter = 1
             #print ("Activity Timer Paused")
        else:
            self._PauseTimerCounter += 1
    
    def resumeTimer (self) :
        if self.isTimerPaused () :
            self._PauseTimerCounter -= 1
            if self._PauseTimerCounter == 0 :
                self._ElapsedPauseTimeInSec += max (time.time () - self._PauseStart, 0)
                self._PauseStart = None
                currentTime = time.time ()
                self._StartTimer  = currentTime
                self._LastUpdate = currentTime
                #print ("Activity Timer Resumed")
            
    def isTimerPaused (self) :
        return (self._PauseStart is not None)
    
    def getElapsedTimePaused (self) :
        return self._ElapsedPauseTimeInSec
    
    def getStartSessionDate (self) :
        return self._StartSessionDate
    
    def getStartSessionTime (self) :
        return self._StartSessionTime
    
    def setMinUpdateTime (self, updatetime)  :
        self._minTimeUpdateInMin = updatetime
    
    def getMinUpdateTime (self) :
        return self._minTimeUpdateInMin
                
    def keepAlive (self) :
        if (not self.isTimerPaused ()) :
            currentTime = time.time ()
            if currentTime - self._LastUpdate  <= (self.getMinUpdateTime () * 60) :  # to stay active Timer requires changes once every 3 min.
                self._LastUpdate = currentTime
                print ("Updateing Timer - ElapsedTime: %f seconds" % self.getElapsedTime ())
            else:
                self._StartTimer  = currentTime
                self._LastUpdate = currentTime
                print ("Restaring Timer.  Elapsed Time: %f seconds" % self.getElapsedTime ())
            
    def getElapsedTime (self) :
        if self.isTimerPaused () :
            return self._ElapsedTime 
        currentTime = time.time ()
        et = self._ElapsedTime + max (0, (currentTime - self._StartTimer))
        self._StartTimer = currentTime
        return et
    
    def getNewTimeLog (self, UserName) :
        LogEntry = {}                
        LogEntry["StartSessionDate"] = self.getStartSessionDate ()
        LogEntry["StartSessionTime"] = self.getStartSessionTime ()
        LogEntry["UserName"] = UserName
        self.updateTimeLog (LogEntry)
        return LogEntry
        
    def updateTimeLog (self, LogEntry) :
        LogEntry["ElapsedTime"] = self.getElapsedTime ()
        LogEntry["EndSessionDate"] = ActivityTimer._getDateStruct (date.today ())
        LogEntry["EndSessionTime"] = time.time ()        


class RelativePathUtil :
   @staticmethod     
   def _getPathStack (path) :
       pathStack = []
       if (os.path.isfile (path)) :
           path, fname = os.path.split (path)
       while True :
           dirName, fname = os.path.split (path)
           if fname != "" and dirName != path :
               pathStack.append (fname)
               path = dirName
           else:
               pathStack.append (dirName)
               break
       pathStack.reverse ()
       return pathStack
           
   @staticmethod 
   def relativePathListCopy (pathlist) :
       if pathlist is None :
           return None
       return copy.copy (pathlist)
   
   @staticmethod
   def getRelatePathListFromFullPath (absolutePath, projectPath):
       absolutePathStack = RelativePathUtil._getPathStack (FileUtil.correctOSPath(absolutePath))
       projectPathStack = RelativePathUtil._getPathStack (FileUtil.correctOSPath (projectPath))
       if len (projectPathStack) == 0 or len (absolutePathStack) == 0 or projectPathStack[0] != absolutePathStack[0] :
           return None #root does not match or paths have no lengt
       index = 0 
       while projectPathStack[index] == absolutePathStack[index] :
           index += 1
           if index >= len (projectPathStack) or index >= len (absolutePathStack) :
               break
       relativePathList = []
       for moveUp in range (index, len (projectPathStack)) :
           relativePathList.append ("..")
       for moveDown in range (index, len (absolutePathStack)) :
           relativePathList.append (absolutePathStack[moveDown])
       return relativePathList
   
   @staticmethod 
   def doRelativePathListMatch (path1, path2) :
       if path1 is None and path2 is not None :
           return False
       if path1 is not None and path2 is  None :
           return False
       if path1 is  None and path2 is  None :
           return True
       if len (path1) != len (path2) :
           return False
       for index in range (len (path1)) :
           if path1[index] != path2[index] :
               return False
       return True
   
   @staticmethod
   def getFullPathFromRelativePathList (relativePathList,projectPath):
        if (relativePathList is None) :
            return None
        projectPathStack = RelativePathUtil._getPathStack (projectPath)
        if len (projectPathStack) <= 0 :
            return None
        for relativeDir in relativePathList :
            if relativeDir == "." :
                continue
            if relativeDir == ".." :
                projectPathStack.pop ()
                continue
            projectPathStack.append (relativeDir)
        returnPath = projectPathStack[0]
        for index in range (1, len (projectPathStack)) :
            returnPath = os.path.join (returnPath, projectPathStack[index])
        return returnPath
    

class MessageBoxUtil :            
    @staticmethod
    def _coreMessage (WindowTitle = None, WindowMessage = None, Details = None, OkButton = False, Icon = None) :
        #from PyQt5 import QtCore
        #from PyQt5.QtWidgets import QMessageBox    
        if Icon is None :
            Icon = QMessageBox.Information

        msg = QMessageBox()
        msg.setIcon(QMessageBox.Warning)
        if (WindowMessage is not None and WindowMessage != ""):
            msg.setText(WindowMessage)            
        if (WindowTitle is not None and WindowTitle != ""):
            msg.setWindowTitle(WindowTitle)
        if (Details is not None and Details != ""):
            msg.setDetailedText(Details)
        msg.setWindowModality(QtCore.Qt.ApplicationModal)
        msg.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)    
        if OkButton :
            msg.setStandardButtons(QMessageBox.Ok)  
            return msg.exec_() 
        else:
            msg.setStandardButtons(QMessageBox.Yes | QMessageBox.No)     
            return QMessageBox.Yes == msg.exec_()             
    
    class RCC_MultiLineMsgDlg (QDialog) :
        def __init__ (self,  WindowTitle = "", WindowMessage ="", MultLineMessage = "", parent = None) :        
            QDialog.__init__ (self, None)      
            self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)         
            self.ui = Ui_MultiLineMessageBoxDlg ()        
            self.ui.setupUi (self)          
            self.setWindowModality(QtCore.Qt.ApplicationModal)        
            self._resizeWidgetHelper = ResizeWidgetHelper ()
            self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.MultiLineLbl)        
            self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.textEdit)
            self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.pushButton)
            self.ui.pushButton.setEnabled (True)
            self.ui.pushButton.clicked.connect (self.close)
            self.ui.MultiLineLbl.setText (WindowMessage)
            self.ui.textEdit.setPlainText (MultLineMessage)
            self.setWindowTitle (WindowTitle)
            
        def _internalWindowResize (self, newsize):                            
            try:
                if (self._resizeWidgetHelper is not None) :                                               
                    self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.MultiLineLbl, newsize)   
                    self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.textEdit, newsize)            
                    self._resizeWidgetHelper.setAbsoluteHeight  (self.ui.textEdit, newsize)                      
                    self._resizeWidgetHelper.setWidgetXPos  (self.ui.pushButton, int (self.width () /2) - 50)      
                    self._resizeWidgetHelper.setWidgetYPos  (self.ui.pushButton, int (self.height () - 40))      
            except:
               pass
        
        def resizeEvent (self, qresizeEvent) :                       
            QDialog.resizeEvent (self, qresizeEvent)                 
            self._internalWindowResize ( qresizeEvent.size () )   
            
    @staticmethod 
    def showMultiLineMessage (WindowsTitle="",MessageLabel="",MultiLineMessage= "", parent = None) :
        dlg = MessageBoxUtil.RCC_MultiLineMsgDlg (WindowsTitle, MessageLabel, MultiLineMessage, parent = parent)
        dlg.exec_ ()
        
    @staticmethod
    def showMessage (WindowTitle=None,WindowMessage = None, Details = None) :
        #from PyQt5.QtWidgets import QMessageBox    
        return MessageBoxUtil._coreMessage (WindowTitle=WindowTitle, WindowMessage=WindowMessage, Details = Details, OkButton = True, Icon = QMessageBox.Information)
        
    @staticmethod   
    def showWarningMessageBox (WindowTitle = None, WindowMessage = None, Details = None):
        #from PyQt5.QtWidgets import QMessageBox    
        return MessageBoxUtil._coreMessage (WindowTitle=WindowTitle, WindowMessage=WindowMessage, Details = Details, OkButton = False, Icon = QMessageBox.Warning)        

    @staticmethod   
    def showQuestionMessageBox (WindowTitle = None, WindowMessage = None, Details = None, Parent = None):
        #from PyQt5.QtWidgets import QMessageBox    
        return MessageBoxUtil._coreMessage (WindowTitle=WindowTitle, WindowMessage=WindowMessage, Details = Details, OkButton = False, Icon = QMessageBox.Question)        


class ShortcutKeyList :    
    @staticmethod
    def getNonNumericPassThroughKeyStrokesIgnorePlusMinus () :
        return [QtCore.Qt.Key_O, QtCore.Qt.Key_M, QtCore.Qt.Key_T,QtCore.Qt.Key_L, QtCore.Qt.Key_P, QtCore.Qt.Key_I, QtCore.Qt.Key_H, QtCore.Qt.Key_N, QtCore.Qt.Key_A, QtCore.Qt.Key_D, QtCore.Qt.Key_Z, QtCore.Qt.Key_X, QtCore.Qt.Key_C, QtCore.Qt.Key_V, QtCore.Qt.Key_B, QtCore.Qt.Key_R, QtCore.Qt.Key_U, QtCore.Qt.Key_Z, QtCore.Qt.Key_Y, QtCore.Qt.Key_E, QtCore.Qt.Key_F, QtCore.Qt.Key_S, QtCore.Qt.Key_Backslash, QtCore.Qt.Key_Escape,   QtCore.Qt.Key_BracketLeft, QtCore.Qt.Key_BracketRight]
    
    @staticmethod
    def getNonNumericPassThroughKeyStrokesWholeNumbersOnly () :
        return [QtCore.Qt.Key_Period] + ShortcutKeyList.getNonNumericPassThroughKeyStrokesIgnorePlusMinus ()
    
    @staticmethod
    def getNonNumericPassThroughKeyStrokes () :
        return [QtCore.Qt.Key_Minus, QtCore.Qt.Key_Plus] + ShortcutKeyList.getNonNumericPassThroughKeyStrokesIgnorePlusMinus ()
        
    @staticmethod
    def getPassThroughKeyStrokes () :
        return ShortcutKeyList.getNonNumericPassThroughKeyStrokes () +  [ QtCore.Qt.Key_1, QtCore.Qt.Key_2, QtCore.Qt.Key_3, QtCore.Qt.Key_4, QtCore.Qt.Key_5, QtCore.Qt.Key_6, QtCore.Qt.Key_7, QtCore.Qt.Key_8, QtCore.Qt.Key_9, QtCore.Qt.Key_0]
 

class PesscaraLoaded :
    
    @staticmethod
    def arePesscaraLibrarysLoaded () :
        try:            
            import tiPY as qin  # qin (image database) library (requires the sys.path.append(...) line above)    
            from tactic_client_lib import TacticServerStub  #
            
            return True
        except :
            return False


class FindContours:
    
    @staticmethod
    def getCenter (area) :                
        # find deep center        
        testarea = scipy.ndimage.morphology.binary_erosion (area)
        cordList = np.nonzero (testarea)        
        while (len (cordList[0]) > 0 ):            
            area = testarea
            testarea = scipy.ndimage.morphology.binary_erosion (area)
            cordList = np.nonzero (testarea)            
        
        rowLst = (np.nonzero (area[:,:]))[1]                
        middleRow = sorted (rowLst)[int (len (rowLst) / 2)]                                                 
        columnLst = (np.nonzero (area[:,middleRow]))[0]        
        middleColumn = sorted (columnLst)[int (len (columnLst) / 2)]                                         
        return float (middleColumn), float (middleRow)
    
    
    
    @staticmethod  
    def correctContourWinding (sliceShape, perimeter) :    
        if (len (perimeter) < 2) :            
            return (np.array ([],dtype=np.int))
        
        ary = np.array (perimeter, dtype = np.int)
        #remove duplicate indexs 
        index = (ary[:-1] - ary[1:]).nonzero ()[0]
        index = index + 1
        first = np.zeros ((1,),dtype=index.dtype)
        index = np.concatenate ((first, index))
        ary = ary[index]
        del index
        if (ary.shape[0] <= 1) :
            return (np.array ([],dtype=np.int))
        
       
        
        inputCordx = ary[:,0]
        inputCordy = ary[:,1]
        rr, cc = polygon (inputCordy, inputCordx)        
        if (len (rr) == 0 or len (cc) == 0):
            return (np.array ([],dtype=np.int))
        
        area = np.zeros(sliceShape, dtype=np.uint8)       
        area[cc, rr] = 1     
                            
        x, y =  FindContours.getCenter (area)
        
        x += 0.5
        y += 0.5
        
        radians = 0.0
        rowcount = ary.shape[0]
        px, py = ary[0, 0], ary[0, 1]
        dx = float (px) - x
        dy = float (py) - y
        mag = math.sqrt (dx * dx + dy * dy) 

        if (mag == 0) :
            return (np.array ([],dtype=np.int))    
        
        dy1 = float(dy) / float (mag)

        Angle1 = math.acos (dy1)
        if (dx > 0.0) :
            Angle1 = (2.0 * math.pi) - Angle1  
        halfPi = float (math.pi / 2.0)
        
        index = np.array (list (range (1, rowcount)), dtype=np.int)
        dx = ary[index, 0].astype (np.float) - x
        dy = ary[index, 1].astype (np.float) - y
        mag = np.sqrt (dx * dx + dy * dy) 
        if np.min (mag) == 0 :
            return (np.array ([],dtype=np.int)) 
        dy2 = dy / mag
        Angle2Ary = np.arccos (dy2)
        #indx = dx > 0.0
        #Angle2Ary[indx] = (2.0 * math.pi) - Angle2Ary[indx]
        #Angle2 = dy2[index]
        
        
        for index in range(len(Angle2Ary)) :                                                          
           #Angle2 = math.acos (dy2[index])
           Angle2 = Angle2Ary[index]
           if (dx[index] > 0.0) :
              Angle2 = (2.0 * math.pi) - Angle2
          
           if (Angle2 != Angle1) :
               if (Angle1 <= halfPi and Angle2 >= (math.pi + halfPi)):
                   dAngle = -(Angle1 + (math.pi + math.pi - Angle2))
               elif (Angle1 >= (math.pi + halfPi) and Angle2 <= halfPi):
                   dAngle = math.pi + math.pi - Angle1 + Angle2
               else :
                   dAngle = Angle2 - Angle1                   
               
               radians += dAngle               
           Angle1 = Angle2
           
        #windingNumber = radians / (2.0 * math.pi)   
        #print ("Winding: " + str (windingNumber))
        if (radians == 0.0) :
            return (np.array ([],dtype=np.int))
        
        if (radians < 0.0) :
            #index = range (len(ary) - 1,-1, -1)
            return ary[::-1]    
        
        #ary = FindContours.removetails (ary)
        return ary
        
    
    @staticmethod   
    def findContours (area)  :
       contourList = measure.find_contours (area, 0.5, positive_orientation = "high")                                 
       # remove duplicates and convert cordinates to ints
       newlst = []
       for Clst in contourList :
           lst = Clst.astype (np.int)
           if (len (lst) >= 1) :
               cIndex = 1           
               for index in range(1, len (lst)) :
                  if (lst[index,0] != lst[cIndex-1,0] or lst[index,1] != lst[cIndex-1,1]) :
                      lst[cIndex,0] = lst[index,0]
                      lst[cIndex,1] = lst[index,1]
                      cIndex += 1                  
               lst[0:cIndex,0] += 1
               lst[0:cIndex,1] += 1
               newlst.append (lst[0:cIndex,:])
       return newlst       

class ANTSRegistrationLog :   

    def __init__ (self, parentobj) :
        self._registrationLog = []
        self._fileName  = None
        self._parentObj = parentobj

    def copy (self, newParentObj) :
        newLog = ANTSRegistrationLog  (newParentObj)
        newLog._registrationLog = copy.copy (self._registrationLog)
        newLog._fileName = self._fileName        
        return newLog
        
    def logANTSRegistration (self, ROIName, ROIType, SourceDictionary, RegistrationParams, Log) :
        if (self._parentObj is not None) : # if roi object otherwise if none is whole dictionary
            self._parentObj.setObjectChanged ()
        today = date.today ()
        tdate = (today.year, today.month, today.day)
        timeNow  = time.time() 
        self._registrationLog.append ([tdate, timeNow, ROIName, ROIType, SourceDictionary, RegistrationParams, Log])

    def _getLastROIRegistration (self, ROIName) :
        count = len (self._registrationLog) 
        for index in range (count, 0, -1) :
            tdate, timeNow, testROIName, roiType, SourceDictionary, RegistrationParams, Log = self._registrationLog[index]
            if ROIName == testROIName : 
                return self._registrationLog[index]
        return None
    
    def getLastRegisterDate (self) :
        index = len (self._registrationLog) -1
        if (index < 0) :
            return None
        tdate, timeNow, testROIName, roiType, SourceDictionary, RegistrationParams, Log = self._registrationLog[index]
        return (tdate)
        
    def hasRegisteredROI (self, ROIName = None) :
        if (ROIName == None) :
            return (len (self._registrationLog) > 0) 
        else:
            return (None != self._getLastROIRegistration (ROIName))
    
    def getANTSRegistrationLog (self) :
        msgLst = []
        for lines in  self._registrationLog :
            tdate, timeNow, testROIName, roiType, SourceDictionary, RegistrationParams, Log = lines
            msg = "Register: %s(%s) from %s at %s\n" % (testROIName, roiType, SourceDictionary, time.asctime(time.localtime (timeNow)) )
            if (RegistrationParams != ""):
                msg += "Parameters: " + RegistrationParams
            if (Log != ""):
                msg += "Log:\n" 
                msg += Log            
            msgLst.append (msg)
        return "\n-------------------\n".join (msgLst)
    
    def saveToFile (self,  FileHeader = None) :
        if (FileHeader is not None) :
            FileHeader.setParameter ("ANTSRegistrationLog", self._registrationLog)                                    
        else:
            if (self._fileName is not None) :                
                f = FileInterface ()      
                try :
                    f.readDataFile (self._fileName)                                        
                except :
                    f = FileInterface ()      
                header = f.getFileHeader()
                header.setParameter ("ANTSRegistrationLog", self._registrationLog)
                f.writeDataFile (self._fileName)
    
    def loadFromFile (self, Path = None, FileHeader = None) :
        self._registrationLog = []
        self._fileName  = None
        if (FileHeader == None) :
            if (Path != self._fileName) :
                self._fileName = Path
            if (os.path.isfile (self._fileName)) :            
                f = FileInterface ()
                f.readDataFile (self._fileName)
                FileHeader = f.getFileHeader()                
        if (FileHeader is not None and FileHeader.hasParameter ("ANTSRegistrationLog")) :
            self._registrationLog = FileHeader.getParameter ("ANTSRegistrationLog")     
            
                
                
class InitalWidgetRelativeState :
        def __init__ (self, parent, widget) :
            w = float (parent.width ())
            h = float (parent.height ())
            self._xPrecent = float (widget.pos().x ()) / w
            self._yPrecent = float (widget.pos().y ()) / h
            self._widthPrecent = float (widget.width()) / w
            self._heightPrecent = float (widget.height()) / h
            self._initalWidthGap = parent.width () - widget.width() 
            self._initalHeightGap = parent.height () - widget.height()     
            
        def getHeightPrecentWindowHeight (self, newsize) :
            return self._heightPrecent * newsize.height ()
            
        def getWidthPrecentWindowWidth (self, newsize) :
            return self._widthPrecent * newsize.width ()
        
        def getYPrecentWindowHeight (self, newsize) :
            return self._yPrecent * newsize.height ()
            
        def getXPrecentWindowWidth (self, newsize) :
            return self._xPrecent * newsize.width ()
            
        def getAbsoluteWidth (self, newsize):
            return max (0, newsize.width () - self._initalWidthGap)    
            
        def getAbsoluteHeight (self, newsize):
            return max (0, newsize.height () - self._initalHeightGap)    

class ResizeWidgetHelper :
    def __init__ (self) :
         self._WidgetUIMem = {}      

    def saveWidgetUIPos (self, parent, widget):
        self._WidgetUIMem[widget] = InitalWidgetRelativeState (parent, widget)

    def setProportionalHeight (self, widget, newsize) :
        widgetUI = self._WidgetUIMem[widget]      
        w = widget.width ()
        widget.resize (w, widgetUI.getHeightPrecentWindowHeight(newsize))
   
    def setProportionalWidth (self, widget, newsize) :
        widgetUI = self._WidgetUIMem[widget]
        h = widget.height ()
        widget.resize (widgetUI.getWidthPrecentWindowWidth(newsize), h)    
    
    def setProportionalXPos (self, widget, newsize) :    
        widgetUI = self._WidgetUIMem[widget]
        y = widget.pos().y ()    
        newX =  widgetUI.getXPrecentWindowWidth(newsize)
        widget.move (newX , y)    
        
    def setProportionalYPos (self, widget, newsize) :    
        widgetUI = self._WidgetUIMem[widget]
        x = widget.pos().x ()    
        newY = widgetUI.getYPrecentWindowHeight(newsize)
        widget.move (x , newY) 
   
    def setWidgetYPos (self, widget, newY) :            
        x = widget.pos().x ()            
        widget.move (x , newY) 
    
    def setWidgetXPos (self, widget, newX) :            
        y = widget.pos().y ()            
        widget.move (newX , y) 
        
    def setAbsoluteWidth  (self, widget, newsize) :       
        widgetUI = self._WidgetUIMem[widget]      
        h = widget.height ()
        widget.resize (widgetUI.getAbsoluteWidth(newsize), h)
        
    def setAbsoluteHeight  (self, widget, newsize) :       
        widgetUI = self._WidgetUIMem[widget]      
        w = widget.width ()
        widget.resize (w, widgetUI.getAbsoluteHeight(newsize))

    def setHeight  (self, widget, height) :       
        w = widget.width ()
        widget.resize (w, height)

    def setWidth  (self, widget, width) :       
        h = widget.height ()
        widget.resize (width, h)




class VisSettings :
    def __init__ (self, TransparencyValue = 100, selectionBoxIndicator = True, selectionDashIndicator = False, smoothVis = True) :
        self._fileName = None
        self._transparencyValue = TransparencyValue
        self._selectionBoxIndicator = selectionBoxIndicator
        self._selectionDashIndicator = selectionDashIndicator
        self._changeListeners = []
        self._isInitalized  = False 
        self._smoothViszualizaton = smoothVis
        self._showROIBoundingBoxInEditMode = False        
        self._toggleUITransparancy = False
        self._showSliceOrientation = False
        self._invertMouseWheel = False
        self._disableObjectSelectionMove = True
        self._ROISelectionMoveToLastEditedSlice = False
        self._undoredoStackSize = 10
        self._autoLockROIWhenHidden = True        
        self._movetoCoordinateAtEndOfPaint = False
        self._movetoCoordinateDuringPaint = False
        self._setRemovedIslandsTo = "None"
        self._showFileNameTitle = True
        self._automatically_perform_N4BiasCorrection = False
        self._N4BiasMaskMode = "automatic"
        self._N4BiasMaskThreshold = 100
        self._DisableImagingOrientationChange = False
        self._performIslandRemovalIn3D = False
        self._disableMouseWheelSliceScrolling = False
        self._mouseWheelSensitivity = 50
        
    def copy (self) :
        newSettings = VisSettings ()
        newSettings._toggleUITransparancy = self._toggleUITransparancy
        newSettings._fileName = None
        newSettings._transparencyValue = self._transparencyValue
        newSettings._selectionBoxIndicator = self._selectionBoxIndicator
        newSettings._selectionDashIndicator = self._selectionDashIndicator        
        newSettings._changeListeners = []
        newSettings._isInitalized  = self._isInitalized
        newSettings._smoothViszualizaton = self._smoothViszualizaton
        newSettings._showROIBoundingBoxInEditMode = self._showROIBoundingBoxInEditMode 
        newSettings._showSliceOrientation = self._showSliceOrientation
        newSettings._invertMouseWheel = self._invertMouseWheel
        newSettings._disableObjectSelectionMove = self._disableObjectSelectionMove
        newSettings._ROISelectionMoveToLastEditedSlice = self._ROISelectionMoveToLastEditedSlice
        newSettings._undoredoStackSize = self._undoredoStackSize
        newSettings._autoLockROIWhenHidden = self._autoLockROIWhenHidden
        newSettings._movetoCoordinateAtEndOfPaint = self._movetoCoordinateAtEndOfPaint
        newSettings._movetoCoordinateDuringPaint = self._movetoCoordinateDuringPaint
        newSettings._setRemovedIslandsTo = self._setRemovedIslandsTo
        newSettings._showFileNameTitle = self._showFileNameTitle
        newSettings._automatically_perform_N4BiasCorrection = self._automatically_perform_N4BiasCorrection        
        newSettings._N4BiasMaskMode = self._N4BiasMaskMode
        newSettings._N4BiasMaskThreshold = self._N4BiasMaskThreshold
        newSettings._DisableImagingOrientationChange = self._DisableImagingOrientationChange
        newSettings._performIslandRemovalIn3D = self._performIslandRemovalIn3D
        newSettings._disableMouseWheelSliceScrolling = self._disableMouseWheelSliceScrolling
        newSettings._mouseWheelSensitivity = self._mouseWheelSensitivity
        return newSettings
    
    def isMouseWheelSliceSelectionDisabled (self) :
        return self._disableMouseWheelSliceScrolling
    
    def getMouseWheelSliceSelectionSensitivity (self) :
        return self._mouseWheelSensitivity
    
    def setMouseWheelSliceSelectionEnabled (self, val) :
        if (val != self._disableMouseWheelSliceScrolling) :
            self._disableMouseWheelSliceScrolling = val 
            self._isInitalized = True
            self._callChangeListener ()
            
    def setMouseWheelSliceSelectionSensitivity (self, val) :
        if (val != self._mouseWheelSensitivity) :
            self._mouseWheelSensitivity = val 
            self._isInitalized = True
            self._callChangeListener ()
            
    def shouldPerformIslandRemovalIn3D (self) :
        return self._performIslandRemovalIn3D
    
    def setPerform3DIslandRemoval (self, val) :
        if val != self._performIslandRemovalIn3D :
            self._performIslandRemovalIn3D = val 
            self._isInitalized = True
            self._callChangeListener ()
            
    def isN4BiasMaskDisabled (self) :
        return self._N4BiasMaskMode  == "disabled"
        
    def isAutomaticN4BiasMask (self) :
        return self._N4BiasMaskMode  == "automatic"
        
    def isN4BiasMaskThreshold (self) :
        return self._N4BiasMaskMode  == "custom"
    
    def getAutomaticN4BiasMaskMode (self) :
        return self._N4BiasMaskMode
    
    def isImageOrientationChangeDisabled (self) :
        return self._DisableImagingOrientationChange
    
    def setDisableImagingOrientationChange (self, val) :
        if (val != self._DisableImagingOrientationChange) :
            self._DisableImagingOrientationChange = val 
            self._isInitalized = True
            self._callChangeListener ()
            
    def setN4BiasMaskModeAutomatic (self) :
        self._setN4BiasMaskMode ("automatic")
        
    def setN4BiasMaskModeCustomThreshold (self) :
        self._setN4BiasMaskMode ("custom")
        
    def setN4BiasMaskModeDisabled (self) :
        self._setN4BiasMaskMode ("disabled")
        
    def _setN4BiasMaskMode (self, val) :
        if (val != self._N4BiasMaskMode) :
            self._N4BiasMaskMode = val 
            self._isInitalized = True
            self._callChangeListener ()
        
    def getN4BiasCustomMaskThreshold (self) :
        return self._N4BiasMaskThreshold
    
    def setN4BiasCustomMaskThreshold (self, val) :
        val = int (val)
        if (val != self._N4BiasMaskThreshold) :
            self._N4BiasMaskThreshold = val 
            self._isInitalized = True
            self._callChangeListener ()
            
    def setPerformAutomaticN4BiasCorrection (self, val) :
        if (val != self._automatically_perform_N4BiasCorrection) :
            self._automatically_perform_N4BiasCorrection = val 
            self._isInitalized = True
            self._callChangeListener ()
            
    def getPerformAutomaticN4BiasCorrection (self) :
        return self._automatically_perform_N4BiasCorrection
            
        
    def setShowFileNameTitle (self, val) :
        if (val != self._showFileNameTitle) :
            self._showFileNameTitle = val 
            self._isInitalized = True
            self._callChangeListener ()
            
    def showFileNameTitle (self) :
        return self._showFileNameTitle
        
    def moveToCoordinateAtPaintEnd (self) :
        return self._movetoCoordinateAtEndOfPaint
    
    def moveToCoordinatDuringPaint (self) :
        return self._movetoCoordinateDuringPaint
    
    def getSetRemovedIslandToROI (self) :
        return self._setRemovedIslandsTo
        
    def setMoveToCoordinateAtPaintEnd (self, val):
        if (val != self._movetoCoordinateAtEndOfPaint) :
            self._movetoCoordinateAtEndOfPaint = val 
            self._isInitalized = True
            self._callChangeListener ()
            
    def setMoveToCoordinateDuringPaint (self,val):
        if (val != self._movetoCoordinateDuringPaint) :
            self._movetoCoordinateDuringPaint = val 
            self._isInitalized = True
            self._callChangeListener ()
            
    def setRemovedIslandToROI (self, val) :
        if (val != self._setRemovedIslandsTo) :
            self._setRemovedIslandsTo = val 
            self._isInitalized = True
            self._callChangeListener ()
            
    def setUndoRedoStackSize (self, val) :
        if (val != self._undoredoStackSize ) :
            self._undoredoStackSize = val 
            self._isInitalized = True
            self._callChangeListener ()
            
    def getUndoRedoStackSize (self) :
        return self._undoredoStackSize
    
    def incrementROITransparency (self, inc) :        
        self.setUITranparency (self._transparencyValue + inc)
        
    def isObjectSelectionMoveDisabled (self) :
        return self._disableObjectSelectionMove
    
    def setObjectSelectionMove (self,val) :
        if (self._disableObjectSelectionMove != val) :
            self._disableObjectSelectionMove = val
            self._isInitalized = True
            self._callChangeListener ()
    
    def shouldMoveToLastEditedSliceOnROISelection (self) :
        return self._ROISelectionMoveToLastEditedSlice
    
    def setMoveToLastEditedSliceOnROISelection (self,val) :
        if (self._ROISelectionMoveToLastEditedSlice != val) :
            self._ROISelectionMoveToLastEditedSlice = val
            self._isInitalized = True
            self._callChangeListener ()
            
        
    def setInvertMouseWheel (self, val) :
            self._invertMouseWheel = val
            self._isInitalized = True
            self._callChangeListener ()    
    
    def isMouseWheelInverted (self) :
        return self._invertMouseWheel
        
    def showSliceOrientation (self) :
        return self._showSliceOrientation
    
    def setShowSliceOrientation (self, val) :
        if (val != self._showSliceOrientation) :
            self._showSliceOrientation = val
            self._isInitalized = True
            self._callChangeListener ()    
    
    def toggleUITranparency (self) :
        self._toggleUITransparancy = not self._toggleUITransparancy  
        self._callChangeListener ()
        
    def setSaveFileName (self, name) :
        self._fileName = name
    
          
    def getParameterList (self) :
        return (["MouseWheelSliceScrollingSensitvity","DisableMouseWheelSliceScrolling","PerformIslandRemovalIn3D", "DisableOrientationChange","N4BiasCorrectionMaskThreshold","N4BiasCorrectionMaskMode","PerfromAutomaticN4BiasCorrection","UIshowFileNameTitle","UITransp", "UITranspBox", "UITranspDash", "UISmoothVis", "UIShowROIBoundingBox", "UIShowSliceOrientation", "UIInvertMouseWheel", "UndoRedoStackSize", "ObjectSelectionMove", "ROISelectionMoveToLastEditedSlice", "AutoLockROIWhenHidden", "SetRemovedIslandsToROI","MovetoCoordinateAtEndOfPaint", "MovetoCoordinateDuringPaint"])
    
    def _saveSettingsToFile (self,  FileHeader = None) :
        fInterface = None
        if (FileHeader is None) :
            if (self._fileName is not None) :
                fInterface = FileInterface ()      
                try :
                    fInterface.readDataFile (self._fileName)                    
                except :
                    fInterface = FileInterface ()      
                FileHeader = fInterface.getFileHeader()                
        if (FileHeader is not None) :
            FileHeader.setParameter ("UITransp", self._transparencyValue)                        
            FileHeader.setParameter ("UITranspBox", self._selectionBoxIndicator)            
            FileHeader.setParameter ("UITranspDash", self._selectionDashIndicator)            
            FileHeader.setParameter ("UISmoothVis", self._smoothViszualizaton)            
            FileHeader.setParameter ("UIShowROIBoundingBox", self._showROIBoundingBoxInEditMode)            
            FileHeader.setParameter ("UIShowSliceOrientation", self._showSliceOrientation)            
            FileHeader.setParameter ("UIInvertMouseWheel", self._invertMouseWheel)            
            FileHeader.setParameter ("ObjectSelectionMove", self._disableObjectSelectionMove)    
            FileHeader.setParameter ("ROISelectionMoveToLastEditedSlice", self._ROISelectionMoveToLastEditedSlice)    
            FileHeader.setParameter ("UndoRedoStackSize", self._undoredoStackSize)    
            FileHeader.setParameter ("AutoLockROIWhenHidden", self._autoLockROIWhenHidden)                
            FileHeader.setParameter ("SetRemovedIslandsToROI", self._setRemovedIslandsTo)    
            FileHeader.setParameter ("MovetoCoordinateAtEndOfPaint", self._movetoCoordinateAtEndOfPaint)    
            FileHeader.setParameter ("MovetoCoordinateDuringPaint", self._movetoCoordinateDuringPaint)                
            FileHeader.setParameter ("UIshowFileNameTitle", self._showFileNameTitle)     
            FileHeader.setParameter ("PerfromAutomaticN4BiasCorrection", self._automatically_perform_N4BiasCorrection)     
            
            FileHeader.setParameter ("N4BiasCorrectionMaskMode", self._N4BiasMaskMode)     
            FileHeader.setParameter ("N4BiasCorrectionMaskThreshold", self._N4BiasMaskThreshold)     
            FileHeader.setParameter ("DisableOrientationChange", self._DisableImagingOrientationChange)     
            FileHeader.setParameter ("PerformIslandRemovalIn3D", self._performIslandRemovalIn3D)         
            
            FileHeader.setParameter ("DisableMouseWheelSliceScrolling", self._disableMouseWheelSliceScrolling) 
            FileHeader.setParameter ("MouseWheelSliceScrollingSensitvity", self._mouseWheelSensitivity) 
         
        
            if fInterface is not None :
                fInterface.writeDataFile (self._fileName)

        

    def isInitalized (self)  :
        return (self._isInitalized)
    
    def loadSettingsFromFile (self, Path = None, FileHeader = None) :
        self._toggleUITransparancy = False
        if (FileHeader == None) :
            if (Path != self._fileName) :
                self._fileName = Path
            if (os.path.isfile (self._fileName)) :            
                f = FileInterface ()
                f.readDataFile (self._fileName)
                FileHeader = f.getFileHeader()        
        if (FileHeader is not None) :
            changed = False
            if FileHeader.hasParameter ("UITransp"):
                self._transparencyValue = max (FileHeader.getParameter ("UITransp"), 1)          
                changed = True
            if FileHeader.hasParameter ("UITranspBox"):
                self._selectionBoxIndicator = FileHeader.getParameter ("UITranspBox")            
                changed = True
            if FileHeader.hasParameter ("UITranspDash"):
                self._selectionDashIndicator = FileHeader.getParameter ("UITranspDash")            
                changed = True
            if FileHeader.hasParameter ("UISmoothVis"):
                self._smoothViszualizaton = FileHeader.getParameter ("UISmoothVis")            
                changed = True
            if FileHeader.hasParameter ("UIShowROIBoundingBox"):
                self._showROIBoundingBoxInEditMode = FileHeader.getParameter ("UIShowROIBoundingBox")            
                changed = True
            if FileHeader.hasParameter ("UIShowSliceOrientation"):
                self._showSliceOrientation = FileHeader.getParameter ("UIShowSliceOrientation")            
                changed = True
            if FileHeader.hasParameter ("UIInvertMouseWheel"):
                self._invertMouseWheel = FileHeader.getParameter ("UIInvertMouseWheel")            
                changed = True
            #if FileHeader.hasParameter ("ObjectSelectionMove"):
            #    self._disableObjectSelectionMove = FileHeader.getParameter ("ObjectSelectionMove")                            
            #    changed = True
            if FileHeader.hasParameter ("ROISelectionMoveToLastEditedSlice"):
                self._ROISelectionMoveToLastEditedSlice = FileHeader.getParameter ("ROISelectionMoveToLastEditedSlice")            
                changed = True                            
            if FileHeader.hasParameter ("UndoRedoStackSize"):
                self._undoredoStackSize = FileHeader.getParameter ("UndoRedoStackSize")            
                changed = True         
            if FileHeader.hasParameter ("AutoLockROIWhenHidden"):
                self._autoLockROIWhenHidden = FileHeader.getParameter ("AutoLockROIWhenHidden")            
                changed = True       
            
            if FileHeader.hasParameter ("SetRemovedIslandsToROI"):
                self._setRemovedIslandsTo = FileHeader.getParameter ("SetRemovedIslandsToROI")            
                changed = True       

            if FileHeader.hasParameter ("MovetoCoordinateAtEndOfPaint"):
                self._movetoCoordinateAtEndOfPaint = FileHeader.getParameter ("MovetoCoordinateAtEndOfPaint")            
                changed = True       
            
            if FileHeader.hasParameter ("MovetoCoordinateDuringPaint"):
                self._movetoCoordinateDuringPaint = FileHeader.getParameter ("MovetoCoordinateDuringPaint")            
                changed = True       
                                     
            if FileHeader.hasParameter ("UIshowFileNameTitle"):
                self._showFileNameTitle = FileHeader.getParameter ("UIshowFileNameTitle")            
                changed = True       
            
            if FileHeader.hasParameter ("PerfromAutomaticN4BiasCorrection"):
                self._automatically_perform_N4BiasCorrection = FileHeader.getParameter ("PerfromAutomaticN4BiasCorrection")            
                changed = True                   
                
            if FileHeader.hasParameter ("N4BiasCorrectionMaskMode"):
                self._N4BiasMaskMode = FileHeader.getParameter ("N4BiasCorrectionMaskMode")            
                changed = True                   
            
            if FileHeader.hasParameter ("N4BiasCorrectionMaskThreshold"):
                self._N4BiasMaskThreshold = FileHeader.getParameter ("N4BiasCorrectionMaskThreshold")            
                changed = True                   
                
            if FileHeader.hasParameter ("DisableOrientationChange"):
                self._DisableImagingOrientationChange = FileHeader.getParameter ("DisableOrientationChange")            
                changed = True                                     

            if FileHeader.hasParameter ("PerformIslandRemovalIn3D"):
                self._performIslandRemovalIn3D = FileHeader.getParameter ("PerformIslandRemovalIn3D")            
                changed = True      
            
            if FileHeader.hasParameter ("DisableMouseWheelSliceScrolling"):
                self._disableMouseWheelSliceScrolling = FileHeader.getParameter ("DisableMouseWheelSliceScrolling")            
                changed = True      
            
            if FileHeader.hasParameter ("MouseWheelSliceScrollingSensitvity"):
                self._mouseWheelSensitivity = FileHeader.getParameter ("MouseWheelSliceScrollingSensitvity")            
                changed = True      
                
            
            self._disableObjectSelectionMove = True  # Always default to disabling object selection moving.
            self._isInitalized = True
            if (changed) :
                self._callChangeListener ()            

    def shouldAutoLockROIWhenHidden (self) :
        return self._autoLockROIWhenHidden

    def setShouldAutoLockROIWhenHidden (self, val) :
        if (val != self._autoLockROIWhenHidden) :     
            self._autoLockROIWhenHidden = val
            self._isInitalized = True
            self._callChangeListener ()
        
    def getShowROIBoundingBoxInEditMode (self) :
        return self._showROIBoundingBoxInEditMode
    
    def setShowROIBoundingBoxInEditMode (self, val):
        if (val != self._showROIBoundingBoxInEditMode) :            
            self._showROIBoundingBoxInEditMode = val
            self._isInitalized = True
            self._callChangeListener ()
            
    def setUISmoothVisualization (self, val) :
        if (val != self._smoothViszualizaton) :
            self._smoothViszualizaton = val
            self._isInitalized = True
            self._callChangeListener ()
            
    def getSmoothVisualization (self) :
        return (self._smoothViszualizaton)
    
    def setUIDashIndicator (self, val) :
        if (val != self._selectionDashIndicator) :
            self._selectionDashIndicator = val
            self._isInitalized = True
            self._callChangeListener ()
            
    def setUIBoxIndicator (self, val) :
        if (val != self._selectionBoxIndicator) :
            self._selectionBoxIndicator = val
            self._isInitalized = True
            self._callChangeListener ()
            
    def getUIDashIndicator (self) :        
        return (self._selectionDashIndicator)        
    
    def getUIBoxIndicator (self) :
        return (self._selectionBoxIndicator)        
   
    def getDrawROI (self) :
        return not self._toggleUITransparancy
    
    def getUITransparency (self, IgnoreUITransparancyToggle = False) :
        if (IgnoreUITransparancyToggle) :
           return (max (self._transparencyValue, 0))
        if (self._toggleUITransparancy) :
            return 0        
        return (max (self._transparencyValue, 0))
  
    def setUITranparency (self, value):
        value = min (max (value, 0), 255)
        if (value != self._transparencyValue) :
            self._toggleUITransparancy = False
            self._transparencyValue = value
            self._isInitalized = True
            self._callChangeListener ()
        
    def _callChangeListener (self) :      
        self._saveSettingsToFile ()
        for i in self._changeListeners :
            i ()            
    
    def addListener (self, callback):
        if (callback not in self._changeListeners) :
            self._changeListeners.append (callback)
    
    def removeListener (self, callback):        
        if (callback in self._changeListeners) :
            self._changeListeners.remove (callback)        
            
class HounsfieldVisSettings :
    def __init__ (self) :        
        self._HounsfieldVisSetting = "MaximizeWithinSlice"
        self._HounsfieldLowValue = -160
        self._HounsfieldHighValue = 240
        self._changeListeners = []
        self.datasetMinValue = None
        self.datasetMaxValue = None
        self._isInitalized  = False 
        self._customLevelPreset = 40
        self._customWindowPreset = 400
        self._windowBWComponentOfColorData = False
        
    def copy (self) :
        newSettings = HounsfieldVisSettings ()
        newSettings._HounsfieldVisSetting = self._HounsfieldVisSetting
        newSettings._HounsfieldLowValue = self._HounsfieldLowValue
        newSettings._HounsfieldHighValue = self._HounsfieldHighValue
        newSettings._changeListeners = []
        newSettings.datasetMinValue = self.datasetMinValue
        newSettings.datasetMaxValue = self.datasetMaxValue
        newSettings._isInitalized  = self._isInitalized
        newSettings._customLevelPreset = self._customLevelPreset
        newSettings._customWindowPreset = self._customWindowPreset
        newSettings._windowBWComponentOfColorData = self._windowBWComponentOfColorData
        return newSettings
    
    def removeChangeListener (self, callback):
        if (callback in self._changeListeners) :
            self._changeListeners.remove (callback)      
    
    def getParameterList (self) :
        return (["HounsfieldVisulizationSettings", "HounsfieldVisulizationSettingsDataSetMinMaxThreshold", "HounsfieldVisulizationSettingsCustomLevelWindowPresets", "HounsfieldVisulizationSettingsWindowBWComponentOfColorData"])
    
    def _saveSettingsToFile (self, FileHeader) :
         FileHeader.setParameter ("HounsfieldVisulizationSettings", (self._HounsfieldVisSetting, self._HounsfieldLowValue, self._HounsfieldHighValue))                     
         FileHeader.setParameter ("HounsfieldVisulizationSettingsDataSetMinMaxThreshold", (self.datasetMinValue, self.datasetMaxValue))                     
         FileHeader.setParameter ("HounsfieldVisulizationSettingsCustomLevelWindowPresets", (self._customLevelPreset, self._customWindowPreset))
         FileHeader.setParameter ("HounsfieldVisulizationSettingsWindowBWComponentOfColorData", self._windowBWComponentOfColorData)
         
    def loadSettingsFromFile (self, FileHeader) :
        self._HounsfieldVisSetting = "MaximizeWithinSlice"
        self._HounsfieldLowValue = -160
        self._HounsfieldHighValue = 240       
        self.datasetMinValue = None
        self.datasetMaxValue = None
        if FileHeader.hasParameter ("HounsfieldVisulizationSettings"):
             self._HounsfieldVisSetting, self._HounsfieldLowValue, self._HounsfieldHighValue = FileHeader.getParameter ("HounsfieldVisulizationSettings")                                             
        if FileHeader.hasParameter ("HounsfieldVisulizationSettingsDataSetMinMaxThreshold"):
             self.datasetMinValue, self.datasetMaxValue = FileHeader.getParameter ("HounsfieldVisulizationSettingsDataSetMinMaxThreshold")
                
        if FileHeader.hasParameter ("HounsfieldVisulizationSettingsCustomLevelWindowPresets"):
             self._customLevelPreset, self._customWindowPreset = FileHeader.getParameter ("HounsfieldVisulizationSettingsCustomLevelWindowPresets")        
        else:
            self._customLevelPreset, self._customWindowPreset  = 40, 400
        
        if FileHeader.hasParameter ("HounsfieldVisulizationSettingsWindowBWComponentOfColorData"):
            self._windowBWComponentOfColorData = FileHeader.getParameter ("HounsfieldVisulizationSettingsWindowBWComponentOfColorData") 
        else:
            self._windowBWComponentOfColorData = False
        self._callChangeListener ()            
        
    def isInitalized (self)  :
        return (self._isInitalized)
    
    def getCustomLevelWindowPreset (self) :        
        return (self._customLevelPreset, self._customWindowPreset)
    
    def setCustomLevelWindowPreset (self, level, window) :
        if (level != self._customLevelPreset or window != self._customWindowPreset) :
            self._customLevelPreset = level
            self._customWindowPreset = window
            self._isInitalized = True
            self._callChangeListener ()
    
    def setWindowBWComponentOfColorData (self, val) :
        if val != self._windowBWComponentOfColorData :
            self._windowBWComponentOfColorData = val
            self._isInitalized = True
            self._callChangeListener ()
    
    def windowBWComponentOfColorData (self) :
        return self._windowBWComponentOfColorData
        
    def getCustomHounsfieldRange (self, ClipToDataset = True) :
        lowValue = self._HounsfieldLowValue
        highValue = self._HounsfieldHighValue
        if not ClipToDataset :
           return (lowValue, highValue)         
        if (self.datasetMinValue is not None) :
            lowValue = max (lowValue, self.datasetMinValue)
        if (self.datasetMaxValue is not None) :
            highValue = min (highValue, self.datasetMaxValue)
        return (lowValue, highValue)        
    
    def setCustomHounsfieldRange (self, low, high):
        if (self._HounsfieldVisSetting != "CustomRange" or self._HounsfieldLowValue != low or self._HounsfieldHighValue != high) :
            self._HounsfieldVisSetting = "CustomRange" 
            self._HounsfieldLowValue = low
            self._HounsfieldHighValue = high
            self._isInitalized = True
            self._callChangeListener ()
    
    def setMaximizeWithinSliceContrast (self):
        if (self._HounsfieldVisSetting != "MaximizeWithinSlice") :
            self._HounsfieldVisSetting = "MaximizeWithinSlice"
            self._isInitalized = True
            self._callChangeListener ()
        
        
    def setMaximizeContrastAcross5To95PrecentInVolume (self):
        if (self._HounsfieldVisSetting != "5To95PrecentInVolume") :
            self._HounsfieldVisSetting = "5To95PrecentInVolume"
            self._isInitalized = True
            self._callChangeListener ()
            
    def setMaximizeContrastAcross5To95PrecentInSlice (self):
        if (self._HounsfieldVisSetting != "5To95PrecentInSlice") :
            self._HounsfieldVisSetting = "5To95PrecentInSlice"
            self._isInitalized = True
            self._callChangeListener ()
            
    def maximizeContrastAcross5To95PrecentInVolume (self):
        return  self._HounsfieldVisSetting == "5To95PrecentInVolume" 
    
    def maximizeContrastAcross5To95PrecentInSlice (self):
        return  self._HounsfieldVisSetting == "5To95PrecentInSlice" 
    
    def maximizeWithinSliceContrast (self):
        return  self._HounsfieldVisSetting == "MaximizeWithinSlice" 
        
    def customHoundsfieldRange (self):
        return  self._HounsfieldVisSetting == "CustomRange" 

    def addChangeListener (self, callback) :        
        if (callback not in self._changeListeners) :
            self._changeListeners.append (callback)
    
    def _callChangeListener (self) :          
        for i in self._changeListeners :
            i ()            
    
    def setDataSetMaxRange (self, values) :
        if ((self.datasetMinValue, self.datasetMaxValue) != values) :
            self.datasetMinValue, self.datasetMaxValue = values
            self._callChangeListener ()
    
    def getDatasetMinMaxValue (self) :
        return self.datasetMinValue, self.datasetMaxValue
        

        
class Coordinate :
    def __init__ (self) :
        self._CoordinateSelected  = None
        self._OldCoordinate  = None
        self._CoordinateChangedCallBack = []
        
    def copy (self) :
        c = Coordinate ()
        if (self._CoordinateSelected is not None) :    
            c._CoordinateSelected = copy.copy (self._CoordinateSelected)
        if (self._OldCoordinate is not None) :       
            c._OldCoordinate = copy.copy (self._OldCoordinate)
        return c    
    
    def isXCoordinateChanged (self):
        if self._OldCoordinate == None :
            return True;
        return (self._OldCoordinate[0] !=  self._CoordinateSelected[0])
    
    def isYCoordinateChanged (self):
        if self._OldCoordinate == None :
            return True;
        return (self._OldCoordinate[1] !=  self._CoordinateSelected[1])
        
    def isZCoordinateChanged (self):
        if self._OldCoordinate == None :
            return True;
        return (self._OldCoordinate[2] !=  self._CoordinateSelected[2])
    
    def clipToShape (self, shape) :
        for index in range (3) :
            if (self._CoordinateSelected[index] < 0) :
                self._CoordinateSelected[index] = 0
            elif (self._CoordinateSelected[index] >= shape[index]) :
                self._CoordinateSelected[index] = shape[index] - 1
        
    def getCoordinate (self) :
        return  self._CoordinateSelected
        
    def setCoordinate  (self, Cordinate, DisableCallback = False, Event = None):   
        try :
            CoordinateChanged = False
            if (self._CoordinateSelected is None or tuple (self._CoordinateSelected) != tuple (Cordinate)):
                self._OldCoordinate = self._CoordinateSelected           
                self._CoordinateSelected = [int (Cordinate[0]),int (Cordinate[1]), int (Cordinate[2])] 
                CoordinateChanged = True
                if (not DisableCallback) :
                    if (self._CoordinateChangedCallBack is not None) :
                        for i in self._CoordinateChangedCallBack :
                            try :
                                i (self, Event=Event)
                            except:
                                i (self)
        finally:
            return CoordinateChanged
                
    def setXCoordinate  (self, xVal) :
        self.setCoordinate ([int (xVal), self._CoordinateSelected[1], self._CoordinateSelected[2]])        
        return self.isXCoordinateChanged ()
        
    def setYCoordinate  (self, yVal) :  
        self.setCoordinate ([self._CoordinateSelected[0], int (yVal), self._CoordinateSelected[2]])  
        return self.isYCoordinateChanged ()
          
    def setZCoordinate  (self, zVal) :     
        self.setCoordinate ([self._CoordinateSelected[0], self._CoordinateSelected[1], int (zVal)])  
        return self.isZCoordinateChanged ()         
         
    def getXCoordinate  (self) :
        return int (self._CoordinateSelected[0])
        
    def getYCoordinate  (self) :
            return int (self._CoordinateSelected[1])    
    
    def getZCoordinate  (self) :
        return int (self._CoordinateSelected[2])
        
    def addChangeListenerCallback (self, callback):
        if (callback not in self._CoordinateChangedCallBack) :
            self._CoordinateChangedCallBack.append (callback)


        
       
class DatasetUIHeader :
    def __init__ (self, FObject= None) :                
        self._visibleHeaderCache = None
        self.clearHeaderObjects ()
        if (FObject is None or not FObject.hasFileObject ("RCC_Dataset_UIHeader")) :
            self.addHeaderObject (HeaderObject ("Dataset","Dataset"))            
        elif FObject.hasFileObject ("RCC_Dataset_UIHeader") :            
            innerFileObject = FObject.getFileObject ("RCC_Dataset_UIHeader")
            tagCount = innerFileObject.getParameter ("TagsCount")
            for tagindex in range (tagCount) :
                headerDefObject = innerFileObject.getFileObject ("Header_" + str (tagindex))
                temp = HeaderObject ("Dataset")
                temp.initFromFileObject (headerDefObject)
                self._headerObjList.append (temp)
                
    def getFileObject (self) :
        fobj = FileObject ("RCC_Dataset_UIHeader")
        fobj.setParameter ("TagsCount", len (self._headerObjList))
        for headerIndex, headerDefObject in enumerate(self._headerObjList) :
            tag_fobj = FileObject ( "Header_" + str(headerIndex))
            headerDefObject.writeToFileObject (tag_fobj)
            fobj.addInnerFileObject (tag_fobj)             
        return fobj
    
    def removeDatasetHeader (self) :
        self._visibleHeaderCache = None
        del self._headerObjList[0]
        
    def insertDatasetHeader (self) :
        self._visibleHeaderCache = None
        self._headerObjList = [HeaderObject ("Dataset","Dataset")] + self._headerObjList
        
    def copy (self) :
        newobj = DatasetUIHeader ()
        newobj.clearHeaderObjects ()
        for obj in self._headerObjList :
            newobj.addHeaderObject    (obj.copy ())
        return newobj
            
    def removeHeaderObjectAtIndex (self, index) :        
        if (index >= 0 and index <= len (self._headerObjList)) :
            self._visibleHeaderCache = None
            del self._headerObjList[index]
            
    def getHeaderObjectAtIndex (self, index):
        if (index >= 0 and index <= len (self._headerObjList)) :
            return self._headerObjList[index]
        return None
    
    def clearHeaderObjects (self) :
        self._visibleHeaderCache = None
        self._headerObjList = []
        
    def addHeaderObject (self, obj) :
        self._visibleHeaderCache = None
        self._headerObjList.append (obj)
    
        
    def headerObjectCount (self) :
        return len (self._headerObjList)
    
    def headerVisibleObjectCount (self) :
        temp = self._getVisibleHeader ()
        if (temp is None) :
            return 0
        return len (temp)        
    
    def _getVisibleHeader (self) :
        if (self._visibleHeaderCache is None) :
            ret = []
            for item in self._headerObjList :
                if item.getVisible () :                  
                    ret.append (item)
            self._visibleHeaderCache = ret
        return self._visibleHeaderCache
    
    def getVisibleHeaderByIndex (self, index):
        lst = self._getVisibleHeader ()
        if (index >= 0 and index <= len (lst)):
            return lst[index]
        return None
    
    def getVisibleHeaderName (self, index):
        lst = self._getVisibleHeader ()
        if (index >= 0 and index <= len (lst)):
            return lst[index].getName ()
        return ""
    
    def getVisibleHeaderType (self, index):
        lst = self._getVisibleHeader ()
        if (index >= 0 and index <= len (lst)):
            return lst[index].getType ()
        return None
    
    def setHeaderByIndex (self, index, header) :
        self._visibleHeaderCache = None
        if (index >= 0 and index <= len (self._headerObjList)):
            self._headerObjList[index] = header
        
    def getVisibleHeaderIdentifierByIndex (self, index):        
        lst = self._getVisibleHeader ()
        if (index >= 0 and index <= len (lst)):
            return lst[index].getIdentifer ()
        return None
    
    
    def getHeaderName (self, index):
        if (index >= 0 and index <= len (self._headerObjList)):
            return self._headerObjList[index].getName ()
        return ""
    
    def getHeaderType (self, index):
        if (index >= 0 and index <= len (self._headerObjList)):
            return self._headerObjList[index].getType ()
        return None
        
    def getHeaderIdentifier (self, index):
        if (index >= 0 and index <= len (self._headerObjList)):
            return self._headerObjList[index].getIdentifer ()
        return None

class HeaderObject :    
    def __init__ (self, Type, Name = "", TagIdentifier = None, visible = True) :        
        self._Type  = Type
        if self._Type not in ["Dataset", "Description", "Registration", "Tag"]:
            print ("Unrecognized header tag type: (Name = %s, Type = %s)" % (Name, Type))
        self._Name  = Name
        if (TagIdentifier is None) :
            TagIdentifier = (Type, Name)
        self._TagIdentifer = TagIdentifier
        self._visible = visible        
        self._headerWidth = 35 # initalize to  value > 0
        self._headerWidthInitlized = False
        
    def writeToFileObject (self, fobj) :
        fobj.setParameter ("Type", self._Type)
        fobj.setParameter ("Name", self._Name)
        fobj.setParameter ("TagIdentifier", self._TagIdentifer)
        fobj.setParameter ("Visible", self._visible)
        fobj.setParameter ("HeaderWidth", self._headerWidth)
        
    def initFromFileObject (self, fobj) :
        self._Type = fobj.getParameter ("Type")
        self._Name = fobj.getParameter ("Name")
        self._TagIdentifer = tuple (fobj.getParameter ("TagIdentifier"))
        self._visible = fobj.getParameter ("Visible")
        if (fobj.hasParameter ("HeaderWidth")) :
            self._headerWidth = fobj.getParameter ("HeaderWidth")
            self._headerWidthInitlized = True
    
    def isHeaderSectionSizeInitlized (self):
        return self._headerWidthInitlized
    
    def setHeaderSectionSize (self, width) :
        self._headerWidth = width
        self._headerWidthInitlized = True
        
    def getHeaderWidth (self) :
        return self._headerWidth
    
    def copy (self) :
        return HeaderObject (self._Type, self._Name, self._TagIdentifer, self._visible)
        
    def getName (self) :
        return self._Name
    
    def getType (self) :
        return self._Type
    
    def getIdentifer (self) :
        return self._TagIdentifer
    
    def getVisible (self) :
       return self._visible
    
    def setVisible (self, val) :
       self._visible = val
  

class DatasetTagManager :
        
    def __init__ (self) :
        self._clearTagCache ()
        self._ParamaterChangeListenerList = []        
        self._unusedIDList = []
        self._idCounter = 0
        self._dataSetTags = []         
        self._disableCallParameterChangeListener = False    
        
        
    def getParameterChangeListeners (self) :
        return self._ParamaterChangeListenerList
    
    def setParameterChangeListeners (self, lst) :
        self._ParamaterChangeListenerList = lst
            
    def areAnyTagsDefined (self) :
        return len (self._dataSetTags) > 0
    
    def _clearTagCache (self) :
        self._customTagCache = None
        self._internalTagCache = None
        
    def _hasTag (self, tag):
        tagName = tag.getName ()
        isInternalTag = tag.isInternalTag ()
        
        for tag in self._dataSetTags :
            if tag.getName () == tagName and isInternalTag == tag.isInternalTag ():
                return tag
        return None
    
    def _RenameTag (self, tagToRename, AppendName):
        self._clearTagCache ()
        tagNameList = []
        newName = tagToRename.getName () + "_" + AppendName
        for tag in self._dataSetTags :
            name = tag.getName ()
            if (name.startswith (newName)) :
                tagNameList.append (name)
        
        fcount = len (tagNameList)
        if (fcount == 0)  : # no collsions found on file rename
            tagToRename.setName (newName)
            return
        uID = 2
        while True :
            testName = newName + "_" + str (uID)
            if (testName not in tagNameList):
                tagToRename.setName (testName)
                return
            uID += 1
    
    def insertTagAtFront (self, tag) :
        self._clearTagCache ()
        self._dataSetTags = [tag] + self._dataSetTags
        
    def popFrontTag (self) :
        self._clearTagCache ()
        front = self._dataSetTags[0]
        del self._dataSetTags[0]
        return front
    
    def removeMissingTags (self, globalTagList) :        
        removeTagList = []
        globalInternalTagDict = set ()
        for gTag in globalTagList :
            globalInternalTagDict.add (gTag.getName ())                
        
        for tag in self._dataSetTags :
            tagName = tag.getName ()            
            if tag.isInternalTag () :
                continue
            if tagName in globalInternalTagDict  :            
                continue                
            removeTagList.append (tag) 
        for tag in removeTagList :
            self.removeTag (tag)
        
    def mergeDatsetFileTagsWithProjectTags (self, projectDatasetTagManager, CallParamaterChangeListener = True) :        
        self._disableCallParameterChangeListener = True
        self._clearTagCache ()
        self.removeMissingTags (projectDatasetTagManager._dataSetTags)         
        TagAdded = False
        for tagindex, tag in enumerate(projectDatasetTagManager._dataSetTags) :
            foundTag = self._hasTag (tag)
            if (foundTag is None) : # If project doesn't have a tag add it
                newtag = tag.copy ()
                newtag.setID (None)
                self.addTag (newtag, index = tagindex)
                TagAdded = True
            else: # project has a tag with matching name
                if (foundTag.getType () != tag.getType ()) :  # if type does not match then remove the one in the DS and take the one in the project             
                    self.removeTag (foundTag)
                    newtag = tag.copy ()                   
                    newtag.setID (None)
                    self.addTag (newtag)
                    TagAdded = True
                else: 
                    #remove tag from ds Re-add                     
                    foundID = foundTag.getID ()
                    for index in range (len (self._dataSetTags)):                        
                        if self._dataSetTags[index].getID () ==  foundID:                                                        
                            del self._dataSetTags[index]        
                            break
                    foundTag.setDefaultValue (tag.getDefaultValue ())
                    if (tag.getType () == "ItemList"):                    
                        newPList = copy.copy (tag.getParameterList ())                        
                        for param in foundTag.getParameterList() :
                            if (param not in newPList) :
                                newPList.append (param)
                        foundTag.setParameterList (newPList)                                            
                    self.addTag (foundTag, index = tagindex) 
                    TagAdded = True
        self._disableCallParameterChangeListener = False   
        if (CallParamaterChangeListener) :                     
            self.callParameterChangeListener ()
        return TagAdded
    
    def addMissingFileTagsToGlobalDataset (self, datasetTagManger, CallParamaterChangeListener = True) :
        self._disableCallParameterChangeListener = True
        self._clearTagCache ()
        TagAdded = False
        for tagindex, tag in enumerate(datasetTagManger._dataSetTags) :
            if tag.isInternalTag () :
                continue
            foundTag = self._hasTag (tag)
            if (foundTag is None) : # If project doesn't have a tag add it
                newtag = tag.copy ()
                newtag.setID (None)
                self.addTag (newtag)
                TagAdded = True
            else: # project has a tag with matching name
                if (foundTag.getType () != tag.getType ()) : 
                    newtag = tag.copy ()
                    self._RenameTag (newtag, newtag.getType ())  
                    newtag.setID (None)
                    self.addTag (newtag)
                    TagAdded = True
        self._disableCallParameterChangeListener = False   
        if (CallParamaterChangeListener) :                     
            self.callParameterChangeListener ()
        return TagAdded
    
    def clear (self) :
        self._clearTagCache ()
        self._unusedIDList = []
        self._idCounter = 0
        self._dataSetTags = []    
        self.callParameterChangeListener ()
    
    def equals (self, otherManager) :
        if len (self._dataSetTags) != len (otherManager._dataSetTags) :
            return False
        mem = {}
        for tag in self._dataSetTags :
            mem[tag.getName ()] = tag.getValue ()
        for tag in otherManager._dataSetTags :
            name = tag.getName ()
            if name not in mem :
                return False
            if mem[name] != tag.getValue () :
                return False
        return True
    
    def copy (self) :
        #print ("Copy Tag Manager")
        newTagManager = DatasetTagManager ()                
        for tag in self._dataSetTags :
           newTag = tag.copy ()                    
           newTag.setID (newTagManager._getID ())        
           newTagManager._dataSetTags.append (newTag)
        return newTagManager

    def _getID (self):
        if len (self._unusedIDList) == 0 :
            self._idCounter = self._idCounter + 1
            return self._idCounter
        else:
            return self._unusedIDList.pop ()
    
    def _recycleID (self, idnumber):
        self._unusedIDList.append (idnumber)
        
    def replaceTagOrAddByMatchingIdentifier (self, tag) :
        self._clearTagCache ()  
        identifer = tag.getIdentifer()
        tag.setID (self._getID ())  
        for index in range (len (self._dataSetTags)):
            if self._dataSetTags[index].getIdentifer() == identifer :
                self._dataSetTags[index] = tag
                self.callParameterChangeListener ()
                return 
        self._dataSetTags.append (tag)
        self.callParameterChangeListener ()
        
        
    def getTagByIdentifer (self, identifer) :
        for index in range (len (self._dataSetTags)):
            if self._dataSetTags[index].getIdentifer() == identifer :
                return self._dataSetTags[index]
        return None        
                
    def removeTag (self, tag) :        
        self._clearTagCache ()
        for index in range (len (self._dataSetTags)):
            if self._dataSetTags[index].getID () == tag.getID () :
                del self._dataSetTags[index]        
                self._recycleID (tag.getID ())
                self.callParameterChangeListener ()
                return
        
    def removeAllInternalTags (self, SafeNameSet = None ) :        
        if SafeNameSet is not None :
            if not isinstance (SafeNameSet, set) :                
                SafeNameSet = set (SafeNameSet)
        else:
            SafeNameSet = set ()                        
        self._clearTagCache ()        
        datasetLength = len (self._dataSetTags)
        for index in range (datasetLength-1, -1, -1):
            tag = self._dataSetTags[index]
            if tag.isInternalTag () :    
                if tag.getName () not in SafeNameSet :
                    self._recycleID (tag.getID ())
                    del self._dataSetTags[index]           
                    del tag
        if datasetLength != len (self._dataSetTags) :
            self.callParameterChangeListener ()
                
    def getTags (self) :
        return self._dataSetTags
            
            
    def getTagByIndex (self, index):
        if (len (self._dataSetTags) > index and index >= 0) :            
            return self._dataSetTags[index]
        return None
            
    def addTag (self, tag, index = None) : 
        self._clearTagCache ()           
        if (tag.getID () is None) : 
            tag.setID (self._getID ())
        if (self._hasTag (tag) is None) :
            if index is None :
                self._dataSetTags.append (tag)
            else:
                self._dataSetTags.insert (index, tag)
            self.callParameterChangeListener ()
        else:
           MessageBoxUtil.showMessage ("Duplicate file tags", "Cannot add tags with duplicate names.")
      
                
    def saveToFile (self,  FileHeader) :        
        fobj = FileObject ("TagManager")
        fobj.setParameter ("TagsCount", len (self._dataSetTags))
        for tagIndex, tag in enumerate(self._dataSetTags) :
            tag_fobj = FileObject ( "Tag_" + str(tagIndex))
            tag.writeToFileObject (tag_fobj)
            fobj.addInnerFileObject (tag_fobj)
        FileHeader.addInnerFileObject (fobj)
            
    def loadTagDatasetFromFile (self, filehandle, FileHeader = None, AddInternalTags = True, AddOwnershipAndLockingInternalTags = True) :        
        self._clearTagCache ()
        if (FileHeader is None and filehandle is not None) :            
            try :
                f = FileInterface ()
                f.readDataFile (filehandle.getPath ())
                FileHeader = f.getFileHeader()       
            except:
                FileHeader = None
                print ("Error loading file header, trying to retrive file tags none loaded.")
                
        self.clear ()        
        #support old file tag format for reading internal flags
        if (FileHeader is not None) :
            if FileHeader.hasParameter ("ROIFileTags") :
                it = FileHeader.getParameter ("ROIFileTags")     
                for key, value in it.items () :
                    tag = Tag (None, key, "Text", value, internalTag=True, storeinTatic=False,mangleTagNameWithTaticContext=True,ParamaterList = [])
                    self.addTag (tag)        
            if (FileHeader.hasFileObject ("TagManager")) :
                fobject = FileHeader.getFileObject ("TagManager")     
                tagCount = fobject.getParameter ("TagsCount")                     
                for tagIndex in range (tagCount) :
                    tagfileobjectName = "Tag_" + str(tagIndex)
                    try :
                        tag = Tag (None, "", "Text", "", internalTag=True, storeinTatic=False,mangleTagNameWithTaticContext=True,ParamaterList = [])
                        tag_fileobject = fobject.getFileObject (tagfileobjectName)  
                        tag.initalizeFromFileObject (tag_fileobject)
                        if not tag.isInternalTag () or AddInternalTags :
                            self.addTag (tag)
                    except:
                        print ("Error Loading Tag: " + tagfileobjectName)            
        if AddOwnershipAndLockingInternalTags  :
            self.setInternalTag ("File owner", "")
            self.setInternalTag ("File status", "")
            self.setInternalTag ("Date acquired", "")
            self.setInternalTag ("Time acquired", "")
            self.setInternalTag ("Assign comment", "")
            self.setInternalTag ("PLock", "")
        self.callParameterChangeListener ()
        
    def _getCustomTagList (self) :
        if (self._customTagCache is None) :
            customLst = []
            for index, tag in enumerate (self._dataSetTags) :
                if (not tag.isInternalTag ()) :
                    customLst.append ((tag, index))
            self._customTagCache = customLst
        return self._customTagCache
               
    def getCustomTagCount (self) :
        return len (self._getCustomTagList ())
        
    def getCustomTagIndex (self, tag) :
        taglist = self._getCustomTagList ()
        identifer = tag.getIdentifer ()
        for index, tags in enumerate(taglist) :
            if tags[0].getIdentifer () == identifer :
                return index
        return None
        
    def getCustomTagByIndex (self, customIndex) :
        taglist = self._getCustomTagList ()
        tag, index = taglist[customIndex]
        return tag
    
    def setCustomTagByIndex (self, customIndex, tag) :
        taglist = self._getCustomTagList ()
        taglist[customIndex] = (tag, taglist[customIndex][1])  #update cache
        _, masterIndex = taglist[customIndex]
        self._dataSetTags[masterIndex] = tag
        self.callParameterChangeListener ()
                         
    ## old API 
    def _getInternalTagList (self) :
        if (self._internalTagCache is None) :
            internalLst = []
            for index, tag in enumerate (self._dataSetTags) :
                if (tag.isInternalTag ()) :
                    internalLst.append ((tag, index))
            self._internalTagCache = internalLst
        return self._internalTagCache
               
    def getInternalTagCount (self) :
        return len (self._getInternalTagList ())
        
    def getInternalTagByIndex (self, internalIndex) :
        taglist = self._getInternalTagList ()
        tag, index = taglist[internalIndex]
        return tag
    
    def setInternalTagByIndex (self, internalIndex, tag) :
        taglist = self._getInternalTagList ()
        taglist[internalIndex] = (tag,taglist[internalIndex][1]) #update cache
        _, masterIndex = taglist[internalIndex]
        self._dataSetTags[masterIndex] = tag
    
    def setInternalTag (self, name, value) :
        self._clearTagCache ()
        for tag in self._dataSetTags :
            if (tag.isInternalTag () and tag.getName () == name) :
                tag.setValue (value)
                return
        tag = Tag (None, name, "Text", value, internalTag=True, storeinTatic=False,mangleTagNameWithTaticContext=True,ParamaterList = [])
        tag.setValue (value)
        self.addTag (tag)
        self.callParameterChangeListener ()

    def hasInternalTag (self, name) :
        for tag in self._dataSetTags :
            if (tag.isInternalTag () and tag.getName () == name) :
                return True
        return False
        
    
    def getInternalTag (self, name) :
        for tag in self._dataSetTags :
            if (tag.isInternalTag () and tag.getName () == name) :
                return tag.getValue ()
        return None
    
   
    
    def callParameterChangeListener (self):    
        if (self._disableCallParameterChangeListener) :
            return 
        """if len (self._ParamaterChangeListenerList) == 0 :
            print ("No Listener: TagManager Change Listener No Op")
        else:
            print ("Calling TagManager Change Listeners: " + str (len (self._ParamaterChangeListenerList)) + " listeners")"""        
        for item in self._ParamaterChangeListenerList :            
            item ()

    def clearChangeListeners (self) :
        self._ParamaterChangeListenerList = []
        
    def addParameterChangeListener (self, listener):
        if (listener not in self._ParamaterChangeListenerList) :
            self._ParamaterChangeListenerList.append (listener)
    
    def removeParameterChangeListener (self, listener):
        if (listener  in self._ParamaterChangeListenerList) :
            del self._ParamaterChangeListenerList[self._ParamaterChangeListenerList.index (listener)]
    
    def createTagFromTacticValues (self, Name, TagValue, internalTag, storeInTactic, mangleTagNameWithTaticContext, Override_Tactic_Context = None) :
        self._clearTagCache ()
        tag = Tag (None, Name, "Text", TagValue, internalTag = internalTag, storeinTatic= storeInTactic,mangleTagNameWithTaticContext=mangleTagNameWithTaticContext,ParamaterList=[], Override_Tactic_Context = Override_Tactic_Context)    
        self.addTag (tag)
        
class Tag :
    def __init__ (self, idnum, name, paramType, defaultValue, internalTag = False, storeinTatic = True, mangleTagNameWithTaticContext = True, ParamaterList = [],Override_Tactic_Context = None) :
        self._ID   = idnum
        self._Name = name
        if (paramType not in ["ItemList", "Checked", "Text", "Description", "None"]):
            print ("Invalid Tag Type")            
            
        self._Type = paramType
        self._DefaultValue = defaultValue        
        self._ParamaterList = []
        for item in ParamaterList :
            self._ParamaterList.append (item)
        self.setValue (defaultValue)
        self._Override_Tactic_Context = Override_Tactic_Context
        
        # flags 
        self._internalTag = internalTag  # flag indicates if Tag is shown to user or hidden from UI
        self._storeInTactic = storeinTatic # flag indicates if Tag is shown to user
        self._mangleTagNameWithTaticContext = mangleTagNameWithTaticContext # flag indicates if Tag Name should be mangled with tatic context when storing in Tatic DB. Normally this should be True, Set to False To View/Edit Tatic tags created outside of the software 
    
    @staticmethod
    def getStaticInternalTagIndentifer (name) :
        tag = Tag (None, name, "Text", None, internalTag=True, storeinTatic=False,mangleTagNameWithTaticContext=True,ParamaterList = [])
        return tag.getIdentifer ()
    
    @staticmethod 
    def createFromDef (tupl) :
        ID, Name, Type, DefaultValue, ParamaterList, Override_Tactic_Context, internalTag, storeinTatic, mangleTagNameWithTaticContext = tupl
        return Tag (ID,Name,Type, DefaultValue, internalTag = internalTag,  storeinTatic =storeinTatic, mangleTagNameWithTaticContext = mangleTagNameWithTaticContext, ParamaterList = ParamaterList  , Override_Tactic_Context= Override_Tactic_Context)
    
    def getTagDefTuple (self) :   
        return (self._ID, self._Name, self._Type, self._DefaultValue, tuple (self._ParamaterList), self._Override_Tactic_Context, self._internalTag, self._storeInTactic, self._mangleTagNameWithTaticContext)
    
    def getTagTacticContext (self, DefaultTacticContext):
        if (self._Override_Tactic_Context is not None and self._Override_Tactic_Context != "") :
            return self._Override_Tactic_Context
        return DefaultTacticContext
    
    def getIdentifer (self) :
        #tag identifers are designed to identify tags between tag managers where tagID will not be the same
        return ("Tag", self._Name, self._internalTag, self._storeInTactic, self._mangleTagNameWithTaticContext)
        
    def initalizeFromFileObject (self, tag_fileobject) :
        self._ID = None
        self._Name = tag_fileobject.getParameter ("Name")
        self._Type = tag_fileobject.getParameter ("ParameterType")
        try:
            self.setDefaultValue (tag_fileobject.getParameter ("DefaultValue"))
        except:
            self.setDefaultValue ("")
            
        self._ParamaterList = tag_fileobject.getParameter ("ParamaterList")        
        try:
            self.setValue (tag_fileobject.getParameter ("Value"))
        except:
            self.setValue ("")
        self._internalTag = tag_fileobject.getParameter ("IsInternalTag")        
        self._storeInTactic = tag_fileobject.getParameter ("StoreTagInTactic")        
        self._mangleTagNameWithTaticContext = tag_fileobject.getParameter ("ShouldMangleTagNameWithTaticContect")        
        try:
            self._Override_Tactic_Context = tag_fileobject.getParameter ("Override_Tactic_Context")        
        except:
            self._Override_Tactic_Context = None
    
    def writeToFileObject (self, tag_fileobject) :        
        tag_fileobject.setParameter ("ID", self._ID)        
        tag_fileobject.setParameter ("Name", self._Name)        
        tag_fileobject.setParameter ("ParameterType", self._Type)
        tag_fileobject.setParameter ("DefaultValue", self._DefaultValue )
        tag_fileobject.setParameter ("ParamaterList", self._ParamaterList )
        tag_fileobject.setParameter ("Value", self.getValue ())
        tag_fileobject.setParameter ("IsInternalTag", self._internalTag)
        tag_fileobject.setParameter ("StoreTagInTactic", self._storeInTactic)
        tag_fileobject.setParameter ("ShouldMangleTagNameWithTaticContect", self._mangleTagNameWithTaticContext)        
        tag_fileobject.setParameter ("Override_Tactic_Context", self._Override_Tactic_Context)        
        
    def copy (self) :
        newtag = Tag (self.getID (), self.getName (), self.getType (), self.getDefaultValue (), self.isInternalTag (), self.storeTagInTatic (), self.shouldMangleTagNameWithTaticContext(),  self.getParameterList (), self._Override_Tactic_Context)
        newtag.setValue (self.getValue ())
        return newtag
      
    def isInternalTag (self) :
        return self._internalTag
    
    def storeTagInTatic (self) :
        return self._storeInTactic
        
    def shouldMangleTagNameWithTaticContext (self) :
        return self._mangleTagNameWithTaticContext
    
    def getID (self) :
        return self._ID
    
    def setID (self, newID) :        
        self._ID = newID
    
    def setName (self, value) :
        self._Name = value
    
    def getName (self) :
        return self._Name
    
    def getType (self) :
        return self._Type
    
    def setDefaultValue (self, value) :
        self._DefaultValue = value
        
    def getDefaultValue (self) :
        return self._DefaultValue
    
    def setParameterList (self, newPList) :
        self._ParamaterList  = newPList
        
    def getParameterList (self) :
        return self._ParamaterList 
    
    def setValue (self, value) :
        self._value = value
    
    def getValue (self) :
        return self._value
    
    def getTextValue (self) :
        value =self.getValue ()
        if self.isInternalTag () :
            from rilcontourlib.util.DateUtil import TimeUtil, DateUtil
            name = self.getName ()
            if name == "Date acquired"  :
                if isinstance (value, str) :
                    return value
                try :       
                    return DateUtil.dateToString (value)
                except:
                    return ""
            elif name == "Time acquired"  :
                if isinstance (value, str) :
                    return value
                try :
                    return TimeUtil.timeToString (value)
                except:
                    return ""
        return value
        
    def getUIString (self) :
        txt = self.getName () + " : " + self.getType () + " <default value: " + self.getDefaultValue ()
        pList = self.getParameterList ()
        if (len (pList) > 0) :
            txt += " Options ["
            first = True
            for item in pList : 
                if not first :
                    txt += ", "
                txt += str (item)
                first = False
            txt += "]"                
        return txt + ">"
    
#For each series generate a nifti file
def _ConvertRGBImageToNifti (value) :
    dicomDirectory, ImageName, PreferredFileFormat = value
    if PreferredFileFormat not in [".nii",".nii.gz"] :
        PreferredFileFormat = ".nii.gz"
        
    try :
        ArrayDicom = None
        affine = None
        imagefilepath = os.path.join (dicomDirectory, ImageName)
        imageData = skimage.io.imread (imagefilepath)
        if len (imageData.shape) > 2 and imageData.shape[2] >= 3:
            red = imageData[:,:,0].astype (np.int)
            green = imageData[:,:,1].astype (np.int)
            blue = imageData[:,:,2].astype (np.int)
            isColor = np.any (red - green) or np.any (green - blue) or np.any (blue - red)
        else:
            isColor = False
            
        if isColor :
            imageShape = (imageData.shape[0],imageData.shape[1],1,3)
            ArrayDicom = np.zeros(imageShape, dtype=imageData.dtype)
            for channel in range (3) :
                ArrayDicom[:, :, 0, channel] = imageData[:,:, channel]
        else:
            imageShape = (imageData.shape[0],imageData.shape[1],1)
            if len (imageData.shape) > 2 :
                ArrayDicom[:, :, 0] = imageData[:,:, 0]
            elif len (imageData.shape) == 2 :
                ArrayDicom[:, :, 0] = imageData[:,:]
    
        ArrayDicom = ArrayDicom[::,::-1,...]
        ArrayDicom = np.rot90(ArrayDicom,-1)
        if isColor :
            shape_3d = ArrayDicom.shape[0:3]
            rgb_dtype = np.dtype([('R', ArrayDicom.dtype), ('G', ArrayDicom.dtype), ('B', ArrayDicom.dtype)])
            ArrayDicom = ArrayDicom.copy().view(dtype=rgb_dtype).reshape(shape_3d)  
               
        #Create the affine
        affine = np.zeros(shape=(4,4))
        affine[0,0] = 1
        affine[1,1] = 1
        affine[2,2] = 1
        affine[0,3] = 0 # 0
        affine[1,3] = 0 # 1
        affine[2,3] = 0 # 2
        affine[3,3] = 1
                    
        NIfTIfilepath = imagefilepath + PreferredFileFormat
        if (not os.path.isfile (NIfTIfilepath)) :
            try :
                #save out as nifti
                nifti = nib.Nifti1Image(ArrayDicom, affine)
                nifti.to_filename(NIfTIfilepath)
                print ("Saving: " + NIfTIfilepath)                                                   
            except :
                try :
                    print ("Could not convert image file to Nifti: " + NIfTIfilepath) 
                except:
                    print ("Could not convert image file to Nifti")
                NIfTIfilepath = None
        else:
            print ("Unexpected Nifti file found.")    
            NIfTIfilepath = None
        #GenerateDicomTag File
        if NIfTIfilepath is not None :
            try :
                dicomtags = {}
                dicomtags["OrigionalImageFileName"] = imagefilepath
                sortedDicomNames = sorted (list(dicomtags.keys()))
                datafile = {"TagName" : [], "TagValue" : []}                                          
                for tagname in sortedDicomNames :
                    value = dicomtags[tagname]
                    datafile["TagName"].append (tagname)
                    datafile["TagValue"].append (value)     
                df = pd.DataFrame(datafile)        
                dicomTagFilename = NIfTIfilepath[:-len (PreferredFileFormat)] + ".DicomTags"
                df.to_csv (dicomTagFilename)                                                  
            except:
                print ("A error occured exporting the dicom tags.")
        
        return NIfTIfilepath, ImageName
    except:
        #print ("A error occured trying to convert image data to NIfTI")
        return None, None
         
def _ConvertDicomToNifti (value) :
    
    def _ConvertDicomUsingDCM2Niix (sliceSet, App = None, PreferedFileFormat = ".nii.gz")  :
    
        def _GetCopyFileFileName (Imaging, SavingNIfTIfilepath, TimeSeriesString = None, NIfTI = None, PreferedFileFormat = ".nii.gz") :                        
            _, sourceFname = os.path.split (Imaging)                                        
            destDir, _ = os.path.split (SavingNIfTIfilepath)
            writeDescriptionName = os.path.join (destDir,sourceFname)    
            writeDescriptionName, extension = FileUtil.removeExtension (writeDescriptionName, [".nii",".nii.gz"])        
            if NIfTI is not None and extension != PreferedFileFormat :
                 extension = PreferedFileFormat
            writeDescriptionName += extension        
            if isinstance (TimeSeriesString, str) :                
                writeDescriptionName, extension = FileUtil.removeExtension (writeDescriptionName, [".nii",".nii.gz"])        
                writeDescriptionName += "_" + TimeSeriesString + extension
                SavingNIfTIfilepath, extension = FileUtil.removeExtension (SavingNIfTIfilepath, [".nii",".nii.gz"])  
                if NIfTI is not None and extension != PreferedFileFormat :
                    extension = PreferedFileFormat
                SavingNIfTIfilepath += "_" + TimeSeriesString + extension
            return [writeDescriptionName, SavingNIfTIfilepath]
          
        def _trySave (Imaging, SavingNIfTIfilepath, jsonDicomTags, NIfTI = None, TimeSeriesString = None, PreferedFileFormat = ".nii.gz", ForceNibabelSave = False) :
            for trySavePath in _GetCopyFileFileName (Imaging, SavingNIfTIfilepath, TimeSeriesString = TimeSeriesString, NIfTI = NIfTI, PreferedFileFormat = PreferedFileFormat) :
                if not os.path.exists (trySavePath) :
                    try :
                        if NIfTI is not None and (not Imaging.endswith (PreferedFileFormat) or ForceNibabelSave):
                            NIfTI.to_filename(trySavePath)
                        else:
                            shutil.copyfile (Imaging, trySavePath)                                                                                   
                        NIfTIfilepath.append ((trySavePath, jsonDicomTags))
                        break
                    except:                        
                        pass            
                    
                    
                                    
        def _loadJsonDicomTags (path) :
            try :
                with open (path,"rt") as infile:
                    return json.load (infile)
            except:
                return {}
        
        tempdir = None         
        NIfTIfilepath = []
        try :
            tempdir = tempfile.mkdtemp ()        
            
            for file in sliceSet :
                _, fname = os.path.split (file)
                shutil.copyfile (file, os.path.join (tempdir, fname))                    
            dicomfilepath = os.path.join (dicomDirectory, firstDicomFileName)                            
            dicomfilepath, _ = FileUtil.removeExtension (dicomfilepath, [".dcm"])            
            SavingNIfTIfilepath = dicomfilepath + "_dcm" + PreferedFileFormat
            print ("Coverting Dicoms to Nifti: " + SavingNIfTIfilepath)
            proc = subprocess.Popen (['dcm2niix', '-w','0','-d','0','-f','%d_%j', tempdir])
            if App is  None :
                proc.wait ()
            else:
                while (True):
                    App.processEvents ()  
                    try:
                        proc.wait (0.05)
                        break
                    except:
                        pass
                        print ("Waiting")
                
            print ("Done Waiting")    
            if SavingNIfTIfilepath == "/Users/KennethPhilbrick/Desktop/New folder/Prostate-Fetch-2020-02-12/TODD_ANTHONY_DION/MR_PROSTATE_WITHOUT_AND_WITH_IV_CONTRAST-2018-06-12/DCE__DOTRM_-D_Q9C360040_TTC_15_2s/1.3.12.2.1107.5.2.19.45327.2018061214050087201554525_dcm.nii" :
                print ("Stop")
            fileList = os.listdir (tempdir)
                        
            FileMatch = {}
            for fname in fileList :                        
                fileMinusExtension, extension = FileUtil.removeExtension (fname, [".nii", ".nii.gz", ".json"])
                if extension is not None :                    
                    extension = extension.lower()
                    if extension in [".nii", ".nii.gz", ".json"] :
                        if extension == ".nii.gz" :
                            extension = ".nii"
                        if fileMinusExtension not in FileMatch :
                            FileMatch[fileMinusExtension] = {}                    
                        FileMatch[fileMinusExtension][extension] = os.path.join(tempdir,fname)
                        
            for filePairs in FileMatch.values () :                        
                Imaging = None
                JasonDicomDescription = None
                if ".nii" in filePairs :
                    if ".json" in filePairs :
                        Imaging, JasonDicomDescription = filePairs[".nii"], filePairs[".json"]
                    else:
                        Imaging, JasonDicomDescription =  filePairs[".nii"], None
                    if Imaging is not None :
                        test =  nib.load (Imaging)                        
                        testShapeLen = len(test.shape)
                        if testShapeLen > 0 and testShapeLen < 3 :
                            data = NibabelDataAccessHelper.getNibabelData (test)
                            while (len (data.shape) < 3) :
                                data = np.expand_dims (data, axis=-1)
                            nifti = nib.Nifti1Image(data, test.affine)                                                                                                                   
                            _trySave (Imaging, SavingNIfTIfilepath,  _loadJsonDicomTags (JasonDicomDescription), NIfTI = nifti, PreferedFileFormat = PreferedFileFormat, ForceNibabelSave = True) 
                        elif testShapeLen == 3 :  
                            if not Imaging.endswith (PreferedFileFormat) :
                                data = NibabelDataAccessHelper.getNibabelData (test)
                                nifti = nib.Nifti1Image(data, test.affine)        
                            else:
                                nifti = None             
                            _trySave (Imaging, SavingNIfTIfilepath, _loadJsonDicomTags (JasonDicomDescription), NIfTI = nifti, PreferedFileFormat = PreferedFileFormat)                            
                        elif testShapeLen == 4 :       
                            exportedBValList = None
                            baseName, extension = FileUtil.removeExtension (Imaging,[".nii",".nii.gz"])
                            if extension is None :
                                print ("Error Unrecogonized imaging format")
                            else:
                                TimeSeriesList = []
                                if os.path.isfile (baseName + ".bval") :
                                    with open (baseName + ".bval", "rt") as infile :
                                        exportedBValList = infile.readline ()
                                    exportedBValList = exportedBValList.strip ()
                                    exportedBValList = exportedBValList.split (" ")
                                    if len (exportedBValList) != test.shape[3] :
                                        exportedBValList = None
                                    else:
                                        TimeSeriesList = exportedBValList
                                if len (TimeSeriesList) != test.shape[3] :
                                    TimeSeriesList = list(range(test.shape[3]))
                                                                    
                                SaveTimeSeriesData = True
                                if 1 in test.shape and exportedBValList is None :                                
                                    data = NibabelDataAccessHelper.getNibabelData (test)
                                    try:
                                        for index in range (3, -1, -1) :                                    
                                            if data.shape[index] == 1 :
                                                data = np.squeeze(data, axis=(index,))
                                                nifti = nib.Nifti1Image(data, test.affine)                                                                                                                   
                                                _trySave (Imaging, SavingNIfTIfilepath,  _loadJsonDicomTags (JasonDicomDescription), NIfTI = nifti, PreferedFileFormat = PreferedFileFormat, ForceNibabelSave = True)                                             
                                                SaveTimeSeriesData = False
                                                break
                                    except:                                    
                                        print ("A error occured trying save dicom frame")
                                if SaveTimeSeriesData :
                                    baseJsonDicomTags = _loadJsonDicomTags (JasonDicomDescription)
                                    data4D = NibabelDataAccessHelper.getNibabelData (test)
                                    for timeIndex in range (test.shape[3]) :
                                        print ("Exporting 4D Slice (%d/%d)" % (timeIndex, test.shape[3]) )
                                        if App is not None :
                                            App.processEvents ()
                                        try :
                                            jsonDicomTags = copy.deepcopy (baseJsonDicomTags)
                                            if exportedBValList is not None and timeIndex < len (exportedBValList) :                                        
                                                jsonDicomTags["BValue"] = exportedBValList[timeIndex]                                            
                                            data = data4D[:,:,:, timeIndex]                 
                                            nifti = nib.Nifti1Image(data, test.affine)                                       
                                            _trySave (Imaging, SavingNIfTIfilepath, jsonDicomTags, NIfTI = nifti, TimeSeriesString = str (TimeSeriesList[timeIndex]), PreferedFileFormat = PreferedFileFormat, ForceNibabelSave = True)                                                                                                                                                                              
                                        except:                                    
                                            print ("A error occured trying save dicom frame")     
                                    try :
                                        jsonDicomTags = copy.deepcopy (baseJsonDicomTags)    
                                        if not Imaging.endswith (PreferedFileFormat) :
                                           nifti = nib.Nifti1Image(data4D, test.affine)        
                                        else:
                                           nifti = None
                                        _trySave (Imaging, SavingNIfTIfilepath, jsonDicomTags, NIfTI = nifti, TimeSeriesString = "4D", PreferedFileFormat = PreferedFileFormat)                                                                                                                                                                              
                                    except:                                    
                                            print ("A error occured trying save 4D dicom")                
                                try :
                                    os.remove (baseName + extension)                    
                                except:
                                    print ("Could not remove: " + baseName + extension)
                                    pass                        
                        else:
                            print ("Error unrecognized dicom  shape after DCM2NII conversion.")
                
        except :
            NIfTIfilepath = None
            exportedBValList = None            
        
        finally :
            if tempdir is not None :
                try:
                    fileObjIterator = ScanDir.scandir(tempdir)       
                    for fileObj in fileObjIterator :                            
                        try :
                            os.remove (fileObj.path)
                        except:
                            pass
                finally:
                    try :
                        fileObjIterator.close ()
                    except:
                        pass
                try:
                    os.rmdir (tempdir)
                except:
                    pass        
            return NIfTIfilepath
        
    def _ConvertImageToNifti (ConstPixelDims, RefDs, HasImageFrames, sliceSet, PreferedFileFormat) :
        # The array is sized based on 'ConstPixelDims'
        ArrayDicom = np.zeros(ConstPixelDims, dtype=RefDs.pixel_array.dtype)
        sliceCount = len (sliceSet)             
        if HasImageFrames :
            for index in range (sliceCount):                              
                ArrayDicom[:, :, sliceCount - index -1] = RefDs.pixel_array[index,...]
        else:    
            for index, sliceData in enumerate (sliceSet):                              
                dicomData = pydicom.read_file(sliceData)
                ArrayDicom[:, :, sliceCount - index -1] = dicomData.pixel_array
        
        ArrayDicom = ArrayDicom[::,::-1,...]
        ArrayDicom = np.rot90(ArrayDicom,-1)
        if RGB :            
           shape_3d = ArrayDicom.shape[0:3]
           rgb_dtype = np.dtype([('R', ArrayDicom.dtype), ('G', ArrayDicom.dtype), ('B', ArrayDicom.dtype)])
           ArrayDicom = ArrayDicom.copy().view(dtype=rgb_dtype).reshape(shape_3d)  
           try :
               imageColorspace = RefDs.PhotometricInterpretation
           except:
               imageColorspace = "RGB"
           
           if "YUV" in imageColorspace :
               YColorChannel = np.copy (ArrayDicom[...]['R'].astype (np.float) - 16.0)
               UColorChannel = np.copy (ArrayDicom[...]['G'].astype (np.float) - 128.0)
               VColorChannel = np.copy (ArrayDicom[...]['B'].astype (np.float)- 128.0)                   
               constantFirstPart = 1.164 * YColorChannel 
               ArrayDicom[...]['B'] = np.clip (constantFirstPart + 2.018*UColorChannel, 0, 255).astype(np.uint8)
               ArrayDicom[...]['G'] = np.clip (constantFirstPart - 0.813*VColorChannel - 0.391*UColorChannel, 0, 255).astype(np.uint8)
               ArrayDicom[...]['R'] = np.clip (constantFirstPart +1.596*VColorChannel, 0, 255).astype(np.uint8)                   
           elif "YBR" in imageColorspace : 
               YColorChannel = np.copy (ArrayDicom[...]['R'].astype (np.float))
               BColorChannel = np.copy (ArrayDicom[...]['G'].astype (np.float) - 128.0)
               RColorChannel = np.copy (ArrayDicom[...]['B'].astype (np.float) - 128.0)                   

               
               ArrayDicom[...]['R'] = np.clip (YColorChannel + 1.402*RColorChannel, 0, 255).astype(np.uint8)                   
               ArrayDicom[...]['G'] = np.clip (YColorChannel - 0.34414*BColorChannel - 0.71414*RColorChannel, 0, 255).astype(np.uint8)
               ArrayDicom[...]['B'] = np.clip (YColorChannel + 1.772*BColorChannel, 0, 255).astype(np.uint8)
        #Create the affine            
        affine = np.zeros(shape=(4,4), dtype=np.float)
        try :
            affine[0,0] = float(RefDs.PixelSpacing[0])
            if affine[0,0] <= 0.0 :
                affine[0,0] = 1.0
            affine[1,1] = float(RefDs.PixelSpacing[1])
            if affine[1,1] <= 0.0 :
                affine[1,1] = 1.0
            affine[2,2] = float(RefDs.SliceThickness)
            if affine[2,2] <= 0.0 :
                affine[2,2] = 1.0
            affine[0,3] = float(RefDs.ImagePositionPatient[0]) # 0
            affine[1,3] = float(RefDs.ImagePositionPatient[1]) # 1
            affine[2,3] = float(RefDs.ImagePositionPatient[2]) # 2
            affine[3,3] = 1.0
        except:   
            dx, dy = DicomVolume._getUltraSoundDims (RefDs)
            affine[0,0] = 1.0
            affine[1,1] = 1.0
            affine[2,2] = 2.0
            affine[0,3] = 0.0 # 0
            affine[1,3] = 0.0 # 1
            affine[2,3] = 0.0 # 2
            affine[3,3] = 1.0
        dicomfilepath = os.path.join (dicomDirectory, firstDicomFileName)            
        if (dicomfilepath.lower().endswith(".dcm")) :
            dicomfilepath = dicomfilepath[:-4]
        NIfTIfilepath = dicomfilepath + "_dcm" + PreferedFileFormat
        if (not os.path.isfile (NIfTIfilepath)) :
            try :
                #save out as nifti
                nifti = nib.Nifti1Image(ArrayDicom, affine)
                nifti.to_filename(NIfTIfilepath)
                print ("Saving: " + NIfTIfilepath)                                                   
            except :
                print ("Could not convert DICOM file to Nifti: " + NIfTIfilepath)
                NIfTIfilepath = None
        else:
            print ("Unexpected Nifti file found.")    
            NIfTIfilepath = None
        if NIfTIfilepath is None :
            return None
        return [(NIfTIfilepath, None)]
    
    dicomDirectory, firstDicomFileName, sliceSet, App, PreferedFileFormat = value
    ImageHasPixelData = False
    HasImageFrames = False    
    try :           
        RefDs = pydicom.read_file(firstDicomFileName)
        sliceCount = len (sliceSet)
        if sliceCount == 1 :        
            try :
                sliceCount = int (RefDs.NumberOfFrames)
                HasImageFrames = True
            except:
                pass        
        RGB = False
        # Load dimensions based on the number of rows, columns, and slices (along the Z axis)
        if (not HasImageFrames and 3 == RefDs.pixel_array.shape[-1] and int (RefDs.Rows) == RefDs.pixel_array.shape[0] and int (RefDs.Columns) == RefDs.pixel_array.shape[1]) :
            ConstPixelDims = (int(RefDs.Rows), int(RefDs.Columns), sliceCount, 3)
            RGB = True
        elif (HasImageFrames and 3 == RefDs.pixel_array.shape[-1] and int (RefDs.Rows) == RefDs.pixel_array.shape[1] and int (RefDs.Columns) == RefDs.pixel_array.shape[2]) :
            ConstPixelDims = (int(RefDs.Rows), int(RefDs.Columns), sliceCount, 3)
            RGB = True
        else:
            ConstPixelDims = (int(RefDs.Rows), int(RefDs.Columns), sliceCount)
        
        ImageHasPixelData = True            
        
        if sliceCount > 1 and not RGB :                        
            if isDICOM2NIfTIPackageInstalled () :
                NIfTIfilepath = _ConvertDicomUsingDCM2Niix (sliceSet, App = App, PreferedFileFormat = PreferedFileFormat)
        else:
           NIfTIfilepath =  _ConvertImageToNifti (ConstPixelDims, RefDs, HasImageFrames, sliceSet, PreferedFileFormat)
           if App is not None :
               App.processEvents ()   
               
        if NIfTIfilepath is not None and len (NIfTIfilepath) == 0 :
            NIfTIfilepath = None
        #GenerateDicomTag File
        processedImaging = []
        if NIfTIfilepath is not None :        
            for subpath in NIfTIfilepath :
                nifti, jsondicom = subpath                
                if isinstance (jsondicom, dict) :
                    dicomtags = jsondicom 
                else:
                    dicomtags = {}                                        
                for tagname in RefDs.dir () :                        
                    try:
                        if (RefDs.data_element (tagname) is not None) :                                
                            if (tagname != "PixelData") :
                                if tagname not in dicomtags :                                    
                                    dicomtags[tagname] = RefDs.data_element (tagname).value
                    except:
                        print ("Error getting docp, tag (name/value) pair")
                
                try :                                            
                    sortedDicomNames = sorted (list(dicomtags.keys()))
                    datafile = {"TagName" : [], "TagValue" : []}                                          
                    for tagname in sortedDicomNames :
                        value = dicomtags[tagname]
                        datafile["TagName"].append (tagname)
                        datafile["TagValue"].append (value)     
                        
                    df = pd.DataFrame(datafile)   
                    dicomTagFilename, _ = FileUtil.removeExtension (nifti, [".nii",".nii.gz"])
                    dicomTagFilename += ".DicomTags"                                                
                    df.to_csv (dicomTagFilename)                                                                  
                except:
                    print ("A error occured exporting the dicom tags.")        
                processedImaging.append ((nifti, sliceSet))
        return (processedImaging, sliceSet)
    except Exception as exp:
        if (ImageHasPixelData) :
            print ("A error occured trying to convert dicom data to NIfTI")
            print ()
            print ("Exception:" + str (exp))
        return (None, None)
    


    
class DicomVolume :          
    
    class DicomSubjectAccessionDescriptionCatalog :
        class objectDictionary :
            def __init__ (self) :
                self._itemList = []   
                self._catalog = {}
                
            def addItem (self, catalogList, item) :
               
                if len (catalogList) > 0 :
                    cat = catalogList.pop ()
                else:
                    cat = None
                if cat is None :
                   self._itemList.append (item) 
                else:
                    if cat not in self._catalog :
                        self._catalog[cat] = DicomVolume.DicomSubjectAccessionDescriptionCatalog.objectDictionary ()
                    self._catalog[cat].addItem (catalogList, item)    
            
            def getOrganizedDatasetList (self, unmappedFileList, resultList, CatalogTreePath, RootDirPath = None) :
                if len (self._itemList) > 0 :
                    if len (self._catalog) <= 0 and len (CatalogTreePath) == 3:
                        appendList = resultList
                    else:
                        appendList = unmappedFileList
                    if  RootDirPath is not None :
                        appendList.append ((RootDirPath, self._itemList))
                    else:
                        appendList.append ((CatalogTreePath, self._itemList))
                for key, value in self._catalog.items () :
                    value.getOrganizedDatasetList (unmappedFileList, resultList, CatalogTreePath + [key], None)
                    
                    
        def __init__ (self, ReadDirPath) :
            self._objDict = DicomVolume.DicomSubjectAccessionDescriptionCatalog.objectDictionary () 
            self._RootList = []
            self._RootDirPath = ReadDirPath
            
        @staticmethod
        def _testSetNoneIfLenZero (test) :
            try:
                if test is None:
                    return None
                if not isinstance (test, str) :
                    test = str (test)
                if len (test) == 0 :
                    return None
                return test 
            except:
                return None
            
        def add (self, subject, accession, description, dataFile) :
            subject = DicomVolume.DicomSubjectAccessionDescriptionCatalog._testSetNoneIfLenZero (subject)
            accession = DicomVolume.DicomSubjectAccessionDescriptionCatalog._testSetNoneIfLenZero (accession)
            description = DicomVolume.DicomSubjectAccessionDescriptionCatalog._testSetNoneIfLenZero (description)
            self._objDict.addItem ([description, accession, subject], dataFile)
        
        def getOrganizedDatasetList (self) :
            resultList = []
            unmappedFileList = []
            self._objDict.getOrganizedDatasetList (unmappedFileList, resultList, [], self._RootDirPath)
            return resultList, unmappedFileList
            
    @staticmethod 
    def formatSubjectID (subjectID, formatStr) :
        formatStr = formatStr.strip ()
        subjectID = subjectID.strip ()
        try:
            if len (formatStr) > 0 and ('0' in formatStr or '#' in formatStr) :
                sID = subjectID.replace ("-","")
                sID = sID.replace ("-","")
                sID = sID.replace (" ","")      
                sID = sID.replace ("\\","")     
                sID = sID.replace ("/","")     
                IDCount = formatStr.count ('0') + formatStr.count ('#')  
                rightPad = max (len (sID) - IDCount, 0) 
         
                RetList = []
                sIndex = len (sID) - 1
                for index in range (len (formatStr)-1,-1,-1) :
                    if formatStr[index] == "0" :
                        for _ in range (1 + rightPad) :
                            if sIndex >= 0 :
                                RetList.append (sID[sIndex])
                                sIndex -= 1
                            else:
                                RetList.append ("0")
                        rightPad = 0
                    elif formatStr[index] == "#" :
                        for _ in range (1 + rightPad) :
                            if sIndex >= 0 :
                                RetList.append (sID[sIndex])
                                sIndex -= 1
                            else:
                                RetList.append ("\\")
                        rightPad = 0
                    else:
                        RetList.append (formatStr[index])
                RetList.reverse ()
                returnstring = "".join (RetList)
                returnStringLst = returnstring.split ("\\")
                if len (returnStringLst) < 1 :
                    return subjectID
                elif len (returnStringLst) == 1 :
                    return returnStringLst[0]
                else:
                    return returnStringLst[0] + returnStringLst[-1]
            else:
                return formatStr + subjectID
        except:
            return subjectID
    
    @staticmethod 
    def getNIfTIDicomTagSubjectAccessionExamTags (path) :
        SubjectID = None
        AccessionNumber = None
        Description = None
        
        if not path.endswith (".DicomTags") :
            path, extension = FileUtil.removeExtension (path, [".nii",".nii.gz"])
            if extension is None or extension not in [".nii",".nii.gz"] :
                return SubjectID, AccessionNumber, Description
            path += ".DicomTags"
        try:
            if os.path.isfile (path) :
                fileDicomDataTags = pd.read_csv (path, header = 0,index_col=0)
                patientIDField = fileDicomDataTags['TagName']== "PatientID"
                if not np.any (patientIDField) :
                    patientIDField = fileDicomDataTags['TagName']== "OrigionalPatientID"
                    if not np.any (patientIDField) :
                        return SubjectID, AccessionNumber, Description
                foundIndex = fileDicomDataTags.loc[patientIDField]
                SubjectID = str (foundIndex["TagValue"].values[0])
                SubjectID = SubjectID.strip ()
                
                accessionNumberField = fileDicomDataTags['TagName']== "AccessionNumber"
                if not np.any (accessionNumberField) :
                    return SubjectID, AccessionNumber, Description
                
                foundIndex = fileDicomDataTags.loc[accessionNumberField]
                AccessionNumber = str (foundIndex["TagValue"].values[0])
                AccessionNumber = AccessionNumber.strip ()
                
                descriptionField = fileDicomDataTags['TagName']== "SeriesDescription"
                if np.any (descriptionField) :
                    foundIndex = fileDicomDataTags.loc[descriptionField]
                    Description = str (foundIndex["TagValue"].values[0])
                Description = Description.strip ()
            
            return SubjectID, AccessionNumber, Description
        except:
            return SubjectID, AccessionNumber, Description
        
   
    
    @staticmethod
    def isDicomFile (fileobj) :
        if not fileobj.is_file() :
            return (False, None)
        try :
            data = pydicom.read_file(fileobj.path)
            return (True, data)
        except :
            return (False, None)

    @staticmethod
    def isImageFile (fileobj) :
        if not fileobj.is_file() :
            return (False, None)
        try :
            data = skimage.io.imread(fileobj.path)
            if len (data.shape) >= 2 :
                return (True, data)
            return (False, None)
        except :
            return (False, None)
        
    @staticmethod
    def _getFileName (fileobj) :
        return fileobj.name
    
    @staticmethod
    def _getDirectoryFileList (dirname, imageformat, IgnoreProcessedDicoms = False, App = None) :
        DicomFileNameList = []
        DicomDataList = []
        DicomFilePathList = []     
        LoadedDicoms = {}
        SkipDicoms = {}
        if os.path.isdir (dirname) :
            if IgnoreProcessedDicoms :
                try :
                    datafile = os.path.join (dirname,"ConvDicoms.dat")
                    if os.path.exists (datafile) :
                        with open (datafile,"rt") as infile:
                            LoadedDicoms = json.load (infile)
                except:
                    pass
            try :
                datafile = os.path.join (dirname,"SkipDicoms.dat")
                if os.path.exists (datafile) :
                    with open (datafile,"rt") as infile:
                        SkipDicoms = json.load (infile)
            except:
                pass

            AddedSkipDicomFile = False         
            try:
                fileIterator =  ScanDir.scandir (dirname)
                filelObjList = sorted (list (fileIterator), key=DicomVolume._getFileName)                
                for fileObj in filelObjList :
                    if App is not None :
                        App.processEvents ()
                    lfname = fileObj.name.lower ()    
                    if lfname.endswith (".nii") or lfname.endswith (".nii.gz") or lfname in ["skipdicoms.dat","convdicoms.dat"]:
                        pass
                    elif fileObj.name not in LoadedDicoms and fileObj.name not in SkipDicoms :                        
                        if imageformat == "DICOM" :
                           isDicom, data = DicomVolume.isDicomFile (fileObj)
                        else:
                            isDicom, data = DicomVolume.isImageFile (fileObj)
                        if (isDicom) :                    
                            DicomFileNameList.append (fileObj.name)
                            if imageformat == "DICOM" :
                                DicomDataList.append (data)
                            DicomFilePathList.append (fileObj.path)    
                        else:
                            SkipDicoms[fileObj.name] = True
                            AddedSkipDicomFile = True
            finally:
                try:
                   fileIterator.close ()
                except:
                    pass
                
            if AddedSkipDicomFile :
                 try :
                    datafile = os.path.join (dirname,"SkipDicoms.dat")
                    if os.path.exists (datafile) :
                        with open (datafile,"wt") as infile:
                            json.dump (SkipDicoms, infile)
                        FileUtil.setFilePermissions (datafile, 0o775)
                 except:
                     pass
        return DicomFileNameList, DicomDataList, DicomFilePathList
    
    @staticmethod
    def getDirectoryDicomFilesList (dirname, IgnoreProcessedDicoms = False, App = None) :
        return DicomVolume._getDirectoryFileList (dirname, "DICOM", IgnoreProcessedDicoms = IgnoreProcessedDicoms, App = App)
    
    @staticmethod
    def getDirectoryImageFilesList (dirname, IgnoreProcessedDicoms = False, App = None) :
        return DicomVolume._getDirectoryFileList (dirname, "IMAGE", IgnoreProcessedDicoms = IgnoreProcessedDicoms, App = App)
    
    @staticmethod
    def _doesDirectoryContainConvertableFiles (dirname, imageformat, IgnoreProcessedDicoms = False, App = None) :
        if os.path.isdir (dirname) :     
            LoadedDicoms = {}
            SkipDicoms = {}
            if IgnoreProcessedDicoms :
                try :
                    datafile = os.path.join (dirname,"ConvDicoms.dat")
                    if os.path.exists (datafile) :
                        with open (datafile,"rt") as infile:
                            LoadedDicoms = json.load (infile)
                except:
                    pass
            try :
                datafile = os.path.join (dirname,"SkipDicoms.dat")
                if os.path.exists (datafile) :
                    with open (datafile,"rt") as infile:
                        SkipDicoms = json.load (infile)
            except:
                pass
                
            AddedSkipDicomFile = False      
            try:
                fileIterator = ScanDir.scandir (dirname)
                for file in fileIterator :      
                    if App is not None :
                        App.processEvents ()
                    if file.name not in LoadedDicoms and  file.name not in SkipDicoms:    
                        if imageformat == "DICOM"  :
                            isDicom = DicomVolume.isDicomFile (file)[0]
                            if isDicom :
                                return True
                            else:
                                isImage = DicomVolume.isImageFile (file)[0]
                                if not isImage :
                                    SkipDicoms[file.name] = True
                                    AddedSkipDicomFile = True   
                        elif imageformat == "IMAGE" :
                            isImage = DicomVolume.isImageFile (file)[0]
                            if isImage :
                                return True
                            else: 
                                isDicom = DicomVolume.isDicomFile (file)[0]
                                if not isDicom :
                                    SkipDicoms[file.name] = True
                                    AddedSkipDicomFile = True   
            finally:
                try:
                   fileIterator.close ()
                except:
                    pass
                if AddedSkipDicomFile :
                     try :
                        datafile = os.path.join (dirname,"SkipDicoms.dat")
                        if os.path.exists (datafile) :
                            with open (datafile,"wt") as infile:
                                json.dump (SkipDicoms, infile)
                            FileUtil.setFilePermissions (datafile, 0o775)
                     except:
                         pass
        return False
    
    @staticmethod
    def doesDirectoryContainDicomFiles (dirname, IgnoreProcessedDicoms = False, App = None) :
        return DicomVolume._doesDirectoryContainConvertableFiles (dirname, "DICOM", IgnoreProcessedDicoms = IgnoreProcessedDicoms, App = App)

    @staticmethod
    def doesDirectoryContainImageFiles (dirname, IgnoreProcessedDicoms = False, App = None) :
        return DicomVolume._doesDirectoryContainConvertableFiles (dirname, "IMAGE", IgnoreProcessedDicoms = IgnoreProcessedDicoms, App = App)

    @staticmethod
    def _getUltraSoundDims (RefDs) :
        try:
            UltraSoundRegions = RefDs.SequenceOfUltrasoundRegions
            if len (UltraSoundRegions) > 0 :
                Delta = UltraSoundRegions[0]
                return Delta.PhysicalDeltaX, Delta.PhysicalDeltaY
        except:
            pass
        return (1, 1)
            
    _ConvertDicomToNifti = staticmethod (_ConvertDicomToNifti)
    _ConvertRGBImageToNifti = staticmethod (_ConvertRGBImageToNifti)
    
    @staticmethod
    def convertRGBFilesToNIfTI(dicomDirectory, ReturnListofNiftiAndDicomConverted = False, App = None, PreferredFileFormat = ".nii.gz") :
        ReturnFileNameList = []
        #Dicom File Loading Code based on code contributed by Tim Kline
        
        #Get a list of Dicome files in the directory
        fileNameList, _, _ = DicomVolume.getDirectoryImageFilesList (dicomDirectory, IgnoreProcessedDicoms = True, App = App)        
        if (len (fileNameList) == 0) :
            return ReturnFileNameList
        
        ProcessedDicoms = {}
        try :
            datafile = os.path.join (dicomDirectory,"ConvDicoms.dat")
            if os.path.exists (datafile) :
                with open (datafile,"rt") as infile:
                    ProcessedDicoms = json.load (infile)
        except:
             ProcessedDicoms = {}
       
        SkipDicoms = {}
        try :
            datafile = os.path.join (dicomDirectory,"SkipDicoms.dat")
            if os.path.exists (datafile) :
                with open (datafile,"rt") as infile:
                    SkipDicoms = json.load (infile)
        except:
            SkipDicoms = {}
            
        mp_Params = []
        for filename in fileNameList :
            if filename not in ProcessedDicoms and filename not in SkipDicoms :
                ProcessedDicoms[filename] = True          
                mp_Params.append ((dicomDirectory, filename, PreferredFileFormat))
           
        results = PlatformMP.processMap (DicomVolume._ConvertRGBImageToNifti, mp_Params, None, PoolSize = None, UseProcessBasedMap = False, QApp = None, Debug = False, ProgressDialog = None, ProcessDialogTextList = None, ProjectFileInterface = None, ParametersContainFileHandles = False)         
                
        returnResults = []
        dicomFilesProcessed = False
        for val in results :
            NIfTIFilePath, dicomFilePath = val
            if NIfTIFilePath is not None :
                if not ReturnListofNiftiAndDicomConverted :
                    returnResults.append (NIfTIFilePath)
                else:
                    returnResults.append ((NIfTIFilePath, dicomFilePath))
                if dicomFilePath is not None :
                    ProcessedDicoms[dicomFilePath] = True
                    dicomFilesProcessed  = True
                        
        if dicomFilesProcessed :  
            try :
                datafile = os.path.join (dicomDirectory,"ConvDicoms.dat")                
                with open (datafile,"wt") as outfile:
                    json.dump (ProcessedDicoms, outfile)
            except:
                pass             

        return returnResults
       
    @staticmethod
    def convertDicomFilesToNIfTI(dicomDirectory, ShowDICOM2NiftiErrorMsg = None, App = None, PreferredFileFormat = ".nii.gz") :
        if PreferredFileFormat not in [".nii", ".nii.gz"] :
            PreferredFileFormat = ".nii.gz"
            
        ReturnFileNameList = []
        #Dicom File Loading Code based on code contributed by Tim Kline
        
        #Get a list of Dicome files in the directory
        fileNameList, lstFilesDCM, filePathList = DicomVolume.getDirectoryDicomFilesList (dicomDirectory, IgnoreProcessedDicoms = True, App = App)        
        if (len (fileNameList) == 0) :
            return ReturnFileNameList

        # loop through all the DICOM files (first determine dicom series,
        # for each series store, a file name and list of files
        AcquisitionDictionary = {}    
        firstFileLocation = None
        
        ProcessedDicoms = {}
        try :
            datafile = os.path.join (dicomDirectory,"ConvDicoms.dat")
            if os.path.exists (datafile) :
                with open (datafile,"rt") as infile:
                    ProcessedDicoms = json.load (infile)
        except:
             ProcessedDicoms = {}
             
        for fileCounter, ds in enumerate (lstFilesDCM):  
            _, fname = os.path.split (filePathList[fileCounter])
            ProcessedDicoms[fname] = True
            InvalidID = False
            try :                                                              
                try: 
                    studyUID = str (ds.StudyInstanceUID)
                except:
                    studyUID = ""
                try: 
                    seriesUID = str (ds.SeriesInstanceUID)
                except:
                    seriesUID = ""
                try: 
                    frameOfReferenceUID = str (ds.FrameOfReferenceUID)
                except:
                    frameOfReferenceUID = ""
                try:
                    pixelSpacing = str(ds.PixelSpacing)
                except:
                    pixelSpacing = ""
                try:
                    imageOrientation = str(ds.ImageOrientationPatient)
                except:
                    imageOrientation = ""
                try:
                    slicethickness = str(ds.SliceThickness)
                except:
                    slicethickness = ""
                try:
                    spacingBetweenSlices = str(ds.SpacingBetweenSlices)
                except:
                    spacingBetweenSlices = ""
                try:
                    rows = str(ds.Rows)
                except:
                    rows = ""
                try:
                    columns = str(ds.Columns)
                except:
                    columns = ""
                if len (seriesUID) > 0 and slicethickness != "" :
                    acquisionID = studyUID + seriesUID + frameOfReferenceUID + pixelSpacing + imageOrientation + slicethickness + rows + columns + spacingBetweenSlices
                    try :
                        imageLocation = int (ds.ImagePositionPatient[2])
                    except:
                        imageLocation = 0
                else:                    
                    acquisionID = "2DImage%d_" % (fileCounter)
                    imageLocation = 0
            except:
                InvalidID = True
            if (not InvalidID) :                
                if acquisionID not in AcquisitionDictionary:                    
                    try :                      
                        filename = filePathList[fileCounter]                        
                        AcquisitionDictionary[acquisionID] = [filename, set ()]
                        AcquisitionDictionary[acquisionID][1].add (filename)
                        firstFileLocation = imageLocation
                    except:
                        print ("Error") # do nothing
                else:                    
                    try :                        
                        filename = filePathList[fileCounter]                        
                        AcquisitionDictionary[acquisionID][1].add (filename)
                        if firstFileLocation is None or imageLocation < firstFileLocation :
                            AcquisitionDictionary[acquisionID][0] = filename
                            firstFileLocation = imageLocation
                    except:
                        print ("Error")# do nothing
        mp_Params = []
        for value in AcquisitionDictionary.values() :
            if len (value[1]) == 1 :
                mp_Params.append ((dicomDirectory, value[0], value[1], App, PreferredFileFormat))
            elif len (value[1]) > 1 and isDICOM2NIfTIPackageInstalled () :
                mp_Params.append ((dicomDirectory, value[0], value[1], App, PreferredFileFormat))
            elif len (value[1]) > 1 and not isDICOM2NIfTIPackageInstalled () and ShowDICOM2NiftiErrorMsg == True :
                MessageBoxUtil.showMessage ("Python Package Required for DICOM to NIfTI Conversion","pip install dicom2nifti to add support for multislice DICOM to NIfTI file conversion. Dicom2nifti is a pypi package and not part of the Conda distribution.")
                ShowDICOM2NiftiErrorMsg = False
        
        SkipDicomLoaded = None 
        results = []
        for param in mp_Params :
            NIfTIFilePath, dicomSlices = DicomVolume._ConvertDicomToNifti (param)
            if NIfTIFilePath is None :
                if SkipDicomLoaded is None :
                    try :
                       datafile = os.path.join (dicomDirectory,"SkipDicoms.dat")                
                       with open (datafile,"rt") as infile:
                           SkipDicomLoaded = json.load (infile) 
                    except:
                        SkipDicomLoaded = {}
                for dcmFile in param[2] :
                    try :
                        _, fname = os.path.split (dcmFile)
                        SkipDicomLoaded[fname] = True
                    except:
                        pass
            else:
                results.append ((NIfTIFilePath, dicomSlices))
            
        if SkipDicomLoaded is not None :
            try :
                datafile = os.path.join (dicomDirectory,"SkipDicoms.dat")                
                with open (datafile,"wt") as outfile:
                    SkipDicomLoaded = json.dump (SkipDicomLoaded, outfile) 
                FileUtil.setFilePermissions (datafile, 0o775)
            except:
                pass
            SkipDicomLoaded = None
        
        returnResults = []
        dicomFilesProcessed = False
        for val in results :
            NIfTIFilePath, dicomSlices = val
            if NIfTIFilePath is not None :
                if isinstance (NIfTIFilePath, list) :
                    for fp in NIfTIFilePath :
                        returnResults.append (fp)
                else:
                    returnResults.append (NIfTIFilePath)
                if dicomSlices is not None :
                    for dicomFilePath in dicomSlices :
                        _ ,filename = os.path.split (dicomFilePath)
                        ProcessedDicoms[filename] = True
                        dicomFilesProcessed  = True
                        
        if dicomFilesProcessed :  
            try :
                datafile = os.path.join (dicomDirectory,"ConvDicoms.dat")                
                with open (datafile,"wt") as outfile:
                    ProcessedDicoms = json.dump (ProcessedDicoms, outfile)
                FileUtil.setFilePermissions (datafile, 0o775)
            except:
                pass             

        if ShowDICOM2NiftiErrorMsg is None :
            return returnResults
        return returnResults, ShowDICOM2NiftiErrorMsg

class NiftiVolumeData :
  
    def __init__ (self, nifti = None, MatchOrientation = None, SliceAxis = "None", Verbose = True, dtype = None, TimeDimension = 0, OpenAndApplyN4BiasCorrection = False, N4BiasMaskMode = "disabled", N4BiasMaskThreshold=100, N4BProgressQueue = None) :    
        if not  NiftiVolumeData.hasSimpleITKInstalled () :
            OpenAndApplyN4BiasCorrection = False
            
        self._didLoadN4BiasCorrectedImage = False
        self._LoadN4BiasN4BiasMaskMode = N4BiasMaskMode
        self._LoadN4BiasN4BiasMaskThreshold = N4BiasMaskThreshold
        self._ImagingShown = "Origional"
        self._ChangeCache = {}        
        self._ImageData = None
        self._rawColorData = None
        self._normalizedOrientation_imageData = None
        if nifti is None :
            return
        self._MinMaxVolumeVoxelValue = None
        self._ChangeListener = []        
        self._FileName = nifti.get_filename()        
        n4bNiftiFilePath = None
        ReNamedN4BFilePath = None
        try :
            if OpenAndApplyN4BiasCorrection :
                try :
                    if NiftiVolumeData.hasN4BiasCache (self._FileName, N4BiasMaskMode, N4BiasMaskThreshold) :
                        n4bNiftiFilePath = self._FileName  + ".n4b"                       
                        ReNamedN4BFilePath = n4bNiftiFilePath + ".nii.gz"
                        try :
                            os.rename (n4bNiftiFilePath, ReNamedN4BFilePath)
                            newNifti = nib.load (ReNamedN4BFilePath)
                            if newNifti is not None :
                                print ("Loading cached N4Bias corrected NIfTI: "+ n4bNiftiFilePath)
                                nifti = newNifti
                                OpenAndApplyN4BiasCorrection = False     
                                self._didLoadN4BiasCorrectedImage = True                    
                        except:
                            ReNamedN4BFilePath = None
                            print ("A error occured tryingto load the saved n4bias nifti")
                except:
                    pass                        
            self._Dimensions  = NiftiVolumeData._init3(nifti.shape, default = 1)            
            self._header = nifti.header      
            self._nifti_affine = np.copy (nifti.affine)          
            self._VoxelSize   = NiftiVolumeData._init3 (nifti.header.get_zooms (), default = min (nifti.header.get_zooms ()))        
            minVoxelSize = float (min (self._VoxelSize))        
            self._NormalizedVoxels = (self._VoxelSize[0] / minVoxelSize, self._VoxelSize[1] / minVoxelSize, self._VoxelSize[2] / minVoxelSize)
            self._affine = nifti.get_qform(False)            #affine = np.eye(4)                     
            if (len (nifti.shape) == 4) :                        
                self._TimeDimensionCount = nifti.shape[3]
                #self._TimeDimensionLoaded = TimeDimension At Present time dimension feature is not implemented
                self._TimeDimensionLoaded = 0            
                self._ImageData = NibabelDataAccessHelper.getNibabelData (nifti)[:,:,:, self._TimeDimensionLoaded]                 
                if (Verbose) :
                    print ("")
                    print ("Warrning multivolume NIfTI file loaded. First volume loaded.")
                    print ("")
            elif (len (nifti.shape) == 3) :                        
                self._TimeDimensionCount = 1
                self._TimeDimensionLoaded = 0            
                self._ImageData = NibabelDataAccessHelper.getNibabelData (nifti)[:,:,:]        
            elif (len (nifti.shape) == 2) :                        
                self._TimeDimensionCount = 1
                self._TimeDimensionLoaded = 0            
                aryData = NibabelDataAccessHelper.getNibabelData (nifti)[:,:]
                self._ImageData = np.zeros ((self._Dimensions[0],self._Dimensions[1], 1), dtype = aryData.dtype)
                self._ImageData[:,:,0] = aryData 
            else:
                self._TimeDimensionCount = 1
                self._TimeDimensionLoaded = 0
                raise ValueError('NiFti file had unexpected dimensions')
            
            if OpenAndApplyN4BiasCorrection :                
                if self._applyN4BiasCorrection (N4BiasMaskMode = N4BiasMaskMode, N4BiasMaskThreshold=N4BiasMaskThreshold, N4BProgressQueue = N4BProgressQueue) :
                    NiftiVolumeData.saveN4BiasCorrectionImage (self,self._ImageData)
                    try:
                        n4FileStateData = {}
                        n4FileStateData["filename"] = self._FileName
                        n4FileStateData["mode"] = N4BiasMaskMode
                        n4FileStateData["CustomThreshold"] = N4BiasMaskThreshold
                        n4FileState = open (self._FileName + ".ndt","wt")
                        json.dump (n4FileStateData, n4FileState)
                        n4FileState.close ()
                        FileUtil.setFilePermissions (self._FileName + ".ndt", 0o775)
                    except:
                        try:
                            os.remove (self._FileName + ".ndt") 
                        except:
                            pass
                            
                    self._didLoadN4BiasCorrectedImage = True
            
            self._rawColorData = None
            if (len (nifti.get_data_dtype()) == 1) :
                if (dtype is not None) :
                    self._ImageData = self._ImageData.astype (dtype)
                else :
                    self._ImageData = self._ImageData.astype (nifti.get_data_dtype())              
            elif (len (nifti.get_data_dtype()) > 1) : # Assume RGB take first component           
               Rimgdata =  (self._ImageData[:,:,:]['R']).astype (np.float)
               Gimgdata =  (self._ImageData[:,:,:]['G']).astype (np.float)
               Bimgdata =  (self._ImageData[:,:,:]['B']).astype (np.float)         
               self._rawColorData = np.zeros ( list (self._ImageData.shape) +[3], dtype =  self._ImageData.dtype[0])
               self._rawColorData[:,:,:,0] = self._ImageData[:,:,:]['R']
               self._rawColorData[:,:,:,1] = self._ImageData[:,:,:]['G']
               self._rawColorData[:,:,:,2] = self._ImageData[:,:,:]['B']
               self._ImageData = (0.2126 * Rimgdata + 0.7152 * Gimgdata +  0.0722 * Bimgdata).astype (np.int)           
               if (dtype is not None) :
                   self._ImageData = self._ImageData.astype (dtype)
               else:  
                   minV = np.min (self._ImageData) 
                   maxV = np.max (self._ImageData)
                   if (np.iinfo(np.uint8).min <= minV and maxV <= np.iinfo(np.uint8).max):                         
                       self._ImageData = self._ImageData.astype (np.uint8) 
                   elif (np.iinfo(np.uint16).min <= minV and maxV <= np.iinfo(np.uint16).max):                         
                       self._ImageData = self._ImageData.astype (np.uint16) 
                
            
            if (Verbose) :
                print ("Scan Orientation")        
            orientation = nib.aff2axcodes(nifti.affine)
            self._nativeNiftiFileOrientation = tuple (orientation)
            self._adjustedAffine = np.eye(4, dtype=float)  
            
            if (MatchOrientation is not None) :
                SliceAxis = MatchOrientation.getSliceAxis ()            
           
            #start determine and save Default Slice Axis
            vlist = list (self._VoxelSize)
            istropicVoxels = True
            firstDimension = vlist[0]
            for dimension in vlist :
                if (firstDimension != dimension) :
                    istropicVoxels = False
                    break
            
            if (not istropicVoxels) :
                highestResolutionOrientation = vlist.index (max (vlist))            
                self._DefaultSliceAxis = NiftiVolumeData.getOrientationString (orientation[highestResolutionOrientation])                                
            else:
                XDim, YDim, ZDim = self._ImageData.shape
                if (XDim == YDim and XDim != ZDim) :
                    Orientation = 2
                elif (XDim == ZDim and XDim != YDim) :
                    Orientation = 1
                elif (ZDim == YDim and XDim != ZDim) :
                    Orientation = 0
                else:
                    shapelst = list(self._ImageData.shape)
                    Orientation = shapelst.index (min (self._ImageData.shape))                  
                self._DefaultSliceAxis = NiftiVolumeData.getOrientationString (orientation[Orientation])                                
            # end determine default slice axis
            
            if (SliceAxis is None or SliceAxis == "None"or SliceAxis == "Default") : 
                SliceAxis = self._DefaultSliceAxis
             
            orientation, self._adjustedAffine, self._ImageData  = self.normalizeOrientation (orientation, self._adjustedAffine)
            self._contourROINiftiOrientation = orientation        
            
            self._normalizedOrientation_adjustedAffine = np.copy (self._adjustedAffine)
            #self._normalizedOrientation_imageData = np.copy (self._ImageData)          
            self._normalizedVoxelSize = np.copy (self._VoxelSize)
            self._normalizedDimensions = np.copy (self._Dimensions)
            self._normalizedNormalizedVoxels =  np.copy (self._NormalizedVoxels)
            
                
            self.rotateLoadedVolumeToNewOrientation (SliceAxis)
            if self._rawColorData is not None :
                junkAffine = np.copy (self._adjustedAffine)
                newColorDataHolder = np.zeros (list (self._ImageData.shape) + [3], dtype = self._rawColorData.dtype)
                for channel in range (3) :
                    _, _, newColorDataHolder[:,:,:,channel] = NiftiVolumeData.transformNiftDataOrientation (self._nativeNiftiFileOrientation,  self._finalROINiftiOrientation, junkAffine, self._rawColorData[:,:,:,channel])        
                self._rawColorData = newColorDataHolder        
        finally:
             if ReNamedN4BFilePath is not None and n4bNiftiFilePath is not None :
                 os.rename (ReNamedN4BFilePath, n4bNiftiFilePath)
        
    @staticmethod 
    def hasN4BiasCache (path, N4BiasMaskMode, N4BiasMaskThreshold) :
         n4bNiftiFilePath = path + ".n4b"
         if os.path.exists (n4bNiftiFilePath) :
             n4bNiftiFileState = path + ".ndt"
             if not os.path.exists (n4bNiftiFileState) :
                 try :
                     os.remove (n4bNiftiFilePath)                     
                 except:
                    pass
                 return False            
             try:
                n4FileState = open (n4bNiftiFileState,"rt")
                n4FileStateData = json.load (n4FileState)
                n4FileState.close ()
                if n4FileStateData["filename"] != path or n4FileStateData["mode"] != N4BiasMaskMode or n4FileStateData["CustomThreshold"] != N4BiasMaskThreshold :
                    try :
                        os.remove (n4bNiftiFilePath)                        
                    except:
                        pass
                    try :
                        os.remove (n4bNiftiFileState)                        
                    except:
                        pass
                    return False
                return True
             except:
                try :
                    os.remove (n4bNiftiFilePath)
                except:
                    pass
                try :
                    os.remove (n4bNiftiFileState)                        
                except:
                    pass
         return False   
                                
    @staticmethod             
    def hasSimpleITKInstalled () :
        #return False
        try :
            import SimpleITK as sitk
            return True
        except :    
            return False
    
    def setImageDataFromNiftiVolumeData (self, volumeData) :        
        if self._normalizedOrientation_imageData is not None :
            self._normalizedOrientation_imageData[...] = volumeData._normalizedOrientation_imageData[...]
        if self._ImageData is not None :
            self._ImageData[...] = volumeData._ImageData[...]
            self._ImagingShown = volumeData._ImagingShown
            self._ChangeCache = copy.deepcopy (volumeData._ChangeCache)
        if self._rawColorData is not None :
            self._rawColorData[...] = volumeData._rawColorData[...]
        self._didLoadN4BiasCorrectedImage = volumeData._didLoadN4BiasCorrectedImage
        
    def didN4BiasCorrectionSucceed (self) :
        return self._didLoadN4BiasCorrectedImage 
    
    def getLoadedN4BiasMaskMethodAndThreshold (self) :
        return self._LoadN4BiasN4BiasMaskMode, self._LoadN4BiasN4BiasMaskThreshold         
    
    @staticmethod 
    def saveN4BiasCorrectionImage (volumedata, ImageData = None) :
         try :            
             n4btestPath = volumedata._FileName + ".n4.nii.gz"
             if not os.path.exists (n4btestPath) :
                 if ImageData is None :
                     ImageData = volumedata._ImageData
                 img = nib.Nifti1Image(ImageData, volumedata._nifti_affine, header = volumedata._header)
                 nib.save (img, n4btestPath)
                 os.rename (n4btestPath, volumedata._FileName + ".n4b")
                 FileUtil.setFilePermissions (n4btestPath, 0o775)
                 del img
                 print ("Saved N4BiasCorrection")                                                
         except:
            pass             
        
    @staticmethod 
    def N4BCorrection (image, maskMode = "disabled", threshold = 100, N4BProgressQueue = None) :    
        if maskMode == "custom" :
            try:
                threshold = int (threshold)
            except:
                print ("Invalid N4B mask threshold, N4B masking disabled.")
                maskMode = "disabled"
            
        try :
            import SimpleITK as sitk
        except :
            print ("Simple ITK not installed. N4Bias correction not supported.")
            return None
        try :
            print ("Starting N4BiasCorrection")                   
            if N4BProgressQueue is not None :
                N4BProgressQueue.put (("range", 0,image.shape[2], 0))
            for sliceIndex in range (image.shape[2]) :
                if N4BProgressQueue is not None :
                    N4BProgressQueue.put (("value", sliceIndex))
                imageSlice = image[:,:,sliceIndex]
                volumeMask = None
                if maskMode in ["automatic", "custom"] :
                    try :
                        if maskMode == "automatic" :
                            minImage = np.min(imageSlice)
                            maxImage = np.max(imageSlice)
                            steps = maxImage - minImage + 1
                            steps /= 100.0         
                            hist, edges = np.histogram (imageSlice, max (int (steps),1), range = (minImage, maxImage))
                            index = np.argmax (hist)                
                            if index / hist.shape[0] <= 0.1 :
                                hindex = index
                                while hindex < hist.shape[0] - 2 and hist[hindex] > hist[hindex + 1] :
                                    hindex += 1
                                if hindex / hist.shape[0] <= 0.2 :
                                    if hindex == index:
                                        upperedge = edges[hindex+1]
                                    else:
                                        upperedge = edges[hindex]                                                
                                    bodymask = imageSlice >= upperedge                    
                                    bodymask = ndimage.morphology.binary_fill_holes (bodymask)
                                    volumeMask = (bodymask).astype (np.uint8)                                                    
                                    print ("Automatic mask (background threshold < %f)" % upperedge)
                        elif maskMode == "custom" :
                            bodymask = imageSlice >= threshold                    
                            bodymask = ndimage.morphology.binary_fill_holes (bodymask)
                            volumeMask = (bodymask).astype (np.uint8)                                                    
                            print ("Custom mask (background threshold < %d)" % threshold)
                    except:
                        volumeMask = None
                        print ("A Error occured determining the N4Bias mask")                
                    
                inputImage = sitk.GetImageFromArray(imageSlice.astype (np.float32))
                if volumeMask is None :
                    output = sitk.N4BiasFieldCorrection (inputImage)
                else:
                    inputMask = sitk.GetImageFromArray(volumeMask)
                    output = sitk.N4BiasFieldCorrection (inputImage,inputMask)                    
                outputImage =  sitk.GetArrayFromImage(output)
                image[:,:,sliceIndex] = outputImage
            if N4BProgressQueue is not None :
                N4BProgressQueue.put (("value", image.shape[2]))
            print ("N4BiasCorrection perfromed")            
            return image
        except:
            print ("A error occured performing N4Bias correction.  Correction not performed.")
            return None
        
    def _applyN4BiasCorrection (self, N4BiasMaskMode = "disabled", N4BiasMaskThreshold=100, N4BProgressQueue = None) :
        result = NiftiVolumeData.N4BCorrection (self._ImageData, N4BiasMaskMode,N4BiasMaskThreshold, N4BProgressQueue = N4BProgressQueue)        
        if result is None :
            print ("N4B Result is None")
            return False
        result = result.astype (dtype = self._ImageData.dtype)
        self._ImageData[...] =  result[...]
        print ("N4B Result is True")
        return True        
            
            
    def getVolumeMinMax (self) :
        try:
            if self._MinMaxVolumeVoxelValue is None :            
                if self.hasColorImageData () :
                    bwindex = self._rawColorData[...,0] == self._rawColorData[...,1]
                    bwindex[self._rawColorData[...,0] != self._rawColorData[...,2]] = False
                    voxels = self._ImageData[bwindex]
                    if len (voxels) <= 0:
                        self._MinMaxVolumeVoxelValue = (None, None)
                    else:
                        self._MinMaxVolumeVoxelValue = (np.min (voxels), np.max (voxels))
                else:
                    self._MinMaxVolumeVoxelValue = (np.min (self._ImageData), np.max (self._ImageData))            
        except:
            self._MinMaxVolumeVoxelValue = (None, None)
        return self._MinMaxVolumeVoxelValue
        
    def hasColorImageData (self) :
        return self._rawColorData is not None
    
    def getColorImageData (self):
        return self._rawColorData
        
    def _cleanupListenerIndexs (self, IndexList) :
        if len (IndexList) > 0 :
            IndexList = sorted (IndexList)
            IndexList.reverse ()
            for index in IndexList :
                try :
                    del self._ChangeListener[index]
                except:
                    pass
    
    def getVoxelUnits (self) :
        try:
            return self._header.get_xyzt_units ()[0].lower ()
        except:
            return ""     
        
    def removeChangeListener (self, listener) :
        foundIndex = -1
        removeIndexList = []
        for index, weakListener in enumerate (self._ChangeListener):            
            try:
                if weakListener () is listener :
                    foundIndex = index
                    break
            except:
                removeIndexList.append (index)                            
        if foundIndex != -1 :
            del self._ChangeListener[foundIndex]
        self._cleanupListenerIndexs (removeIndexList)
        
    def addChangeListener (self, listener) :
        removeIndexList = []
        for index,weakListener in enumerate(self._ChangeListener):
            if PythonVersionTest.IsPython2 () :
                if weakListener == listener :
                    return
            else:
                try :
                    if weakListener () is listener :
                        return        
                except:
                    removeIndexList.append (index)        
        
        if PythonVersionTest.IsPython2 () :
            self._ChangeListener.append (listener)
        else:
            self._ChangeListener.append (weakref.WeakMethod (listener))
        self._cleanupListenerIndexs (removeIndexList)
    
    def callChangeListener (self) :
        removeIndexList = []
        for index, callback in enumerate (self._ChangeListener) :
            try :
                callback ()()
            except:
                removeIndexList.append (index)                                
        self._cleanupListenerIndexs (removeIndexList)
        
        
    def getValueCountPerVoxel (self) :
        return 1
        
    def getFileName (self) :
        if self._FileName is None :
            return ""
        return str (self._FileName)
    
    @staticmethod        
    def InitFromNumpyArray (npData, ParentNiftiVolumeData, DataAxis = "Z", SwapXYDim = False) :    
        newNiftVolumeData = NiftiVolumeData ()
        newNiftVolumeData._FileName = None
        newNiftVolumeData._ChangeListener = [] 
        newNiftVolumeData._Dimensions  = npData.shape
        newNiftVolumeData._header = ParentNiftiVolumeData._header                
        newNiftVolumeData._VoxelSize   = np.copy (ParentNiftiVolumeData._VoxelSize)
        newNiftVolumeData._NormalizedVoxels = np.copy (ParentNiftiVolumeData._NormalizedVoxels)
        newNiftVolumeData._affine  = np.copy (ParentNiftiVolumeData._affine)
        newNiftVolumeData._TimeDimensionCount = 1
        newNiftVolumeData._TimeDimensionLoaded = 0
        newNiftVolumeData._ImageData = npData        
        newNiftVolumeData._nativeNiftiFileOrientation = copy.copy (ParentNiftiVolumeData._nativeNiftiFileOrientation)
        newNiftVolumeData._adjustedAffine = np.copy (ParentNiftiVolumeData._adjustedAffine)
        newNiftVolumeData._DefaultSliceAxis = ParentNiftiVolumeData._DefaultSliceAxis
        newNiftVolumeData._contourROINiftiOrientation = copy.copy (ParentNiftiVolumeData._contourROINiftiOrientation)
        newNiftVolumeData._nifti_affine = np.copy (ParentNiftiVolumeData._nifti_affine)
        newNiftVolumeData._normalizedOrientation_adjustedAffine = np.copy (ParentNiftiVolumeData._normalizedOrientation_adjustedAffine)
        newNiftVolumeData._normalizedOrientation_imageData = np.copy (newNiftVolumeData._ImageData)
        newNiftVolumeData._normalizedVoxelSize = np.copy (ParentNiftiVolumeData._normalizedVoxelSize)
        newNiftVolumeData._normalizedDimensions = npData.shape
        newNiftVolumeData._normalizedNormalizedVoxels =  np.copy (ParentNiftiVolumeData._normalizedNormalizedVoxels)
        if DataAxis != "Z" or SwapXYDim :
             newNiftVolumeData._header = None      
             newNiftVolumeData._affine = None
             newNiftVolumeData._adjustedAffine = None 
             newNiftVolumeData._nativeNiftiFileOrientation = None
             newNiftVolumeData._contourROINiftiOrientation  = None
             newNiftVolumeData._nifti_affine = None
             newNiftVolumeData._normalizedOrientation_adjustedAffine = None
             newNiftVolumeData._normalizedOrientation_imageData = None
             #newNiftVolumeData._normalizedVoxelSize = None
             #newNiftVolumeData._normalizedDimensions = None
             #newNiftVolumeData._normalizedNormalizedVoxels = None
             if DataAxis == "X" :
                 dz = 0
                 dx = 1
                 dy = 2
             elif DataAxis == "Y" :
                 dz = 1
                 dx = 0
                 dy = 2
             elif DataAxis == "Z" :
                 dz = 2
                 dx = 0
                 dy = 1
             if SwapXYDim :
                 dx, dy = dy, dx
             newNiftVolumeData._VoxelSize  =  (newNiftVolumeData._VoxelSize[dx],newNiftVolumeData._VoxelSize[dy],newNiftVolumeData._VoxelSize[dz])
             newNiftVolumeData._NormalizedVoxels =  (newNiftVolumeData._NormalizedVoxels[dx],newNiftVolumeData._NormalizedVoxels[dy],newNiftVolumeData._NormalizedVoxels[dz])
             newNiftVolumeData._normalizedVoxelSize  =  (newNiftVolumeData._normalizedVoxelSize[dx],newNiftVolumeData._normalizedVoxelSize[dy],newNiftVolumeData._normalizedVoxelSize[dz])
             newNiftVolumeData._normalizedNormalizedVoxels  =  (newNiftVolumeData._normalizedNormalizedVoxels[dx],newNiftVolumeData._normalizedNormalizedVoxels[dy],newNiftVolumeData._normalizedNormalizedVoxels[dz])
             #print (("Voxel Size: ", newNiftVolumeData._VoxelSize ))
             #print (("Normalized Voxels: ", newNiftVolumeData._NormalizedVoxels ))
             #print (("Normalized Voxel Size: ", newNiftVolumeData._normalizedVoxelSize ))
             #print (("normalized Normalized Voxel Size: ", newNiftVolumeData._normalizedNormalizedVoxels ))
        return newNiftVolumeData
        
        
    def getSeedPointMask (self, ptX, ptY, ptZ, LowerThreshold = None, UpperThreshold = None, Clip = None) :
        if (LowerThreshold is None) :
            LowerThreshold = np.min (self._ImageData)
        if (UpperThreshold is None) :
            UpperThreshold = np.max (self._ImageData)                 
        selectedIndexs = self._ImageData >= LowerThreshold
        selectedIndexs[self._ImageData > UpperThreshold] = False
        if (Clip is not None)  :
            xBox,yBox,zBox = Clip            
            if (xBox[0] > 0) :
                selectedIndexs[0: xBox[0], :,:] = False
            if (xBox[1] + 1 <= selectedIndexs.shape[0] - 1) :
                selectedIndexs[xBox[1] + 1 :, :,:] = False            
            if (yBox[0] > 0) :
                selectedIndexs[xBox[0]:xBox[1] + 1, 0:yBox[0],:] = False
            if (yBox[1] + 1 <= selectedIndexs.shape[1] - 1) :
                selectedIndexs[xBox[0]:xBox[1] + 1, yBox[1] + 1 :,:] = False            
            if (zBox[0] > 0) :
                selectedIndexs[xBox[0]:xBox[1] + 1, yBox[0]:yBox[1] + 1, 0: zBox[0]] = False
            if (zBox[1] + 1 <= selectedIndexs.shape[2] - 1) :
                selectedIndexs[xBox[0]:xBox[1] + 1, yBox[0]:yBox[1] + 1,zBox[1] + 1 :] = False
            
        structure = structure  = np.zeros ((3,3,3),dtype = np.uint8)            
        structure[0,1,1] = 1
        structure[1,0,1] = 1
        structure[1,1,0] = 1
        structure[1,1,1] = 1
        structure[1,1,2] = 1
        structure[1,2,1] = 1
        structure[2,1,1] = 1         
        labeledObjects, num_features  = ndimage.measurements.label (selectedIndexs.astype (np.uint8), structure)            
        labelIndexValue = labeledObjects[ptX, ptY, ptZ]
        if (labelIndexValue == 0) :
            return None    
        testObj = labeledObjects == labelIndexValue            
        testObj = ndimage.morphology.binary_erosion (testObj)
        testObj = ndimage.morphology.binary_erosion (testObj)
        if not np.any (testObj) :
            return None
        testObj = ndimage.morphology.binary_dilation (testObj)
        testObj = ndimage.morphology.binary_dilation (testObj)
        
        return testObj.astype(np.uint8)                        
        
    
    def rotateLoadedVolumeToNewOrientation (self, newOrientation, Verbose = False) :
        if (newOrientation == None or newOrientation == "None"or newOrientation == "Default") : 
            SliceAxis = self._DefaultSliceAxis
        else:
            SliceAxis = newOrientation
        orientation = self._contourROINiftiOrientation
        self._adjustedAffine =  np.copy (self._normalizedOrientation_adjustedAffine)
        if self._normalizedOrientation_imageData is not None :
            self._ImageData  = np.copy (self._normalizedOrientation_imageData)  
        self._VoxelSize = np.copy (self._normalizedVoxelSize)
        self._Dimensions = np.copy (self._normalizedDimensions)
        self._NormalizedVoxels = np.copy (self._normalizedNormalizedVoxels)
        
        
        xAxisView = NiftiVolumeData.getOrientationString (self._contourROINiftiOrientation[0])
        yAxisView = NiftiVolumeData.getOrientationString (self._contourROINiftiOrientation[1])
        zAxisView = NiftiVolumeData.getOrientationString (self._contourROINiftiOrientation[2])                
        if (Verbose) :
            print ("XAxis: " + xAxisView)        
            print ("YAxis: " + yAxisView)                
            print ("ZAxis: " + zAxisView)                
        
        #self._adjustedAffine = np.matmul( self._adjustedAffine, nifti.affine ) 
        
        if (SliceAxis == xAxisView) :    
            if self._normalizedOrientation_imageData is None :
                self._normalizedOrientation_imageData = np.copy (self._ImageData)  
            moveAxisStartTuple = (0,1,2)
            moveAxisDestLst = [0,1,2]
            orientation, self._adjustedAffine,  moveAxisDestLst  = NiftiVolumeData._swapAxis (0,2, orientation, self._adjustedAffine, self._ImageData, self, MoveAxisTuple = moveAxisDestLst)
            orientation, self._adjustedAffine,  moveAxisDestLst  = NiftiVolumeData._swapAxis (0,1, orientation, self._adjustedAffine, self._ImageData, self, MoveAxisTuple = moveAxisDestLst)                        
            moveAxisDestLst = tuple (moveAxisDestLst)
            if moveAxisDestLst != moveAxisStartTuple :
                moveAxisDestLst = (moveAxisDestLst.index(0), moveAxisDestLst.index(1), moveAxisDestLst.index(2))
                self._ImageData = np.moveaxis (self._ImageData, moveAxisStartTuple, moveAxisDestLst)
            #print ("Flip")
            orientation, self._adjustedAffine, self._ImageData = NiftiVolumeData._flipAxis (2, orientation, self._adjustedAffine, self._ImageData)   
            if (Verbose) :
                print ("SwapX")         
        elif (SliceAxis == yAxisView) :
            if self._normalizedOrientation_imageData is None :
                self._normalizedOrientation_imageData = np.copy (self._ImageData)  
            orientation, self._adjustedAffine, self._ImageData  = NiftiVolumeData._swapAxis (1,2, orientation, self._adjustedAffine, self._ImageData, self)                                    
            if (Verbose) :
                print ("SwapY")         
        
        self._finalROINiftiOrientation = orientation                 
        self._SliceAxis = SliceAxis
        #self._contourROINiftiOrientation = orientation
        self._adjustedAffine = np.matmul( self._nifti_affine, self._adjustedAffine ) 
    
    def isNiftiRotatedFromDefaultOrientatation (self) :
        return np.any (self._nifti_affine - self._adjustedAffine)
    
    def getNativeFileOrientationAxis (self) :
        return self.getOrientationString (self._nativeNiftiFileOrientation[2])
    
    def getDisplayOrieintationAxis (self) :
        return self.getDisplaySliceOrientation ()
        
    @staticmethod
    def _translatePt ( X,Y,Z, xdim,ydim,zdim, sourceSliceAxis, destinationSliceAxis) :
        if (sourceSliceAxis == "Axial") :
           if (destinationSliceAxis == "Sagittal") :                   
               X, Z, Y = Y, zdim-X, Z                   
           elif (destinationSliceAxis == "Coronal") :
               Y,Z = Z,Y # Good
        elif (sourceSliceAxis == "Coronal") :
           if (destinationSliceAxis == "Sagittal") :
              X,Z = Z,zdim-X
           elif (destinationSliceAxis == "Axial") :
               Y,Z = Z,Y  #good
        elif (sourceSliceAxis == "Sagittal") :
           if (destinationSliceAxis == "Coronal") :
              Z, X = X,xdim-Z 
           elif (destinationSliceAxis == "Axial") :
               Y, X, Z   = X, xdim-Z, Y  
        return (X,Y,Z)
   
    def getVolumeShapeinOrientation (self, destinationSliceAxis) :
           X, Y, Z = self.getSliceDimensions ()
           sourceSliceAxis = self.getSliceAxis ()
           if (sourceSliceAxis == "Axial") :
               if (destinationSliceAxis == "Sagittal") :                   
                   X, Z, Y = Y, X, Z                   
               elif (destinationSliceAxis == "Coronal") :
                   Y,Z = Z,Y # Good
           elif (sourceSliceAxis == "Coronal") :
               if (destinationSliceAxis == "Sagittal") :
                  X,Z = Z,X
               elif (destinationSliceAxis == "Axial") :
                   Y,Z = Z,Y  #good
           elif (sourceSliceAxis == "Sagittal") :
               if (destinationSliceAxis == "Coronal") :
                  Z, X = X,Z 
               elif (destinationSliceAxis == "Axial") :
                   Y, X, Z   = X, Z, Y  
           return (X,Y,Z)
       
    def reorientPointToMatchVolumeOrientation (self, coordinate, sourceSliceAxis) :
           X, Y, Z = coordinate
           xdim, ydim, zdim = self.getSliceDimensions ()
           destinationSliceAxis = self.getSliceAxis ()
           return NiftiVolumeData._translatePt ( X,Y,Z, xdim-1,ydim-1,zdim-1, sourceSliceAxis, destinationSliceAxis)
    
    def reorientVolumeToMatchOrientation (self, volume, sourceSliceAxis) :
           X, Y, Z = volume.nonzero ()
           xdim, ydim, zdim = self.getSliceDimensions ()
           destinationSliceAxis = self.getSliceAxis ()
           X,Y,Z = NiftiVolumeData._translatePt ( X,Y,Z, xdim-1,ydim-1,zdim-1, sourceSliceAxis, destinationSliceAxis)  
           volume = np.zeros ((xdim, ydim, zdim), dtype = np.uint8)
           volume[X,Y,Z] = 1
           return volume
       
        
    #used in stats dlg
    def getDisplayAxis (self, orientation) :
        if (orientation == NiftiVolumeData.getOrientationString (self._finalROINiftiOrientation[0])) :
            return "X-axis"
        if (orientation == NiftiVolumeData.getOrientationString (self._finalROINiftiOrientation[1])) :
            return "Y-axis"
        if (orientation == NiftiVolumeData.getOrientationString (self._finalROINiftiOrientation[2])) :
            return "Z-axis"
        return "Unknown"        
    
    
        
    def getDisplaySliceOrientation  (self):
        return NiftiVolumeData.getOrientationString (self._finalROINiftiOrientation[2])
        
    def getTimeDimensionCount (self) :
        return self._TimeDimensionCount
    
    def getTimeDimensionLoaded (self) :
        return self._TimeDimensionLoaded
    
    def getSliceAxis (self) :
        return self._SliceAxis
    
    @staticmethod 
    def transformData (initalOrientation, outputOrientation, Data) : 
        orinentation, affine, Data = NiftiVolumeData.transformNiftDataOrientation (initalOrientation, outputOrientation,  np.eye(4, dtype=float) ,Data, None)
        return Data
    
    @staticmethod 
    def transformPoint (initalOrientation, outputOrientation, point, shape) :
        orientation = initalOrientation
        zAxisOutputOrientiation = NiftiVolumeData.getOrientationString (outputOrientation[2])                
        yAxisOutputOrientiation = NiftiVolumeData.getOrientationString (outputOrientation[1])                        
        
        if (NiftiVolumeData.getOrientationString (orientation[0]) == zAxisOutputOrientiation)  :
            point, shape = NiftiVolumeData._swapPointAxis  (0, 2, point, shape)            
        elif (NiftiVolumeData.getOrientationString (orientation[1]) == zAxisOutputOrientiation)  :
            point, shape = NiftiVolumeData._swapPointAxis  (1, 2, point, shape)
        if (NiftiVolumeData.getOrientationString (orientation[0]) == yAxisOutputOrientiation)  :
            point, shape = NiftiVolumeData._swapPointAxis  (0, 1, point, shape)        
        for index in range (3) :
            if (orientation[index] != outputOrientation[index]):
                point = NiftiVolumeData._flipPointAxis (index, point, shape)            
        return point, shape
    
    @staticmethod  
    def transformNiftDataOrientation (initalOrientation, outputOrientation, adjustedAffine, Data, imageData = None) :        
        orientation = initalOrientation
        zAxisOutputOrientiation = NiftiVolumeData.getOrientationString (outputOrientation[2])                
        yAxisOutputOrientiation = NiftiVolumeData.getOrientationString (outputOrientation[1])                        
        
        moveAxisStartTuple = (0,1,2)
        moveAxisDestLst = [0,1,2]
        if (NiftiVolumeData.getOrientationString (orientation[0]) == zAxisOutputOrientiation)  :
            orientation, adjustedAffine, moveAxisDestLst = NiftiVolumeData._swapAxis  (0, 2, orientation, adjustedAffine, Data, imageData, MoveAxisTuple = moveAxisDestLst)            
        elif (NiftiVolumeData.getOrientationString (orientation[1]) == zAxisOutputOrientiation)  :
            orientation, adjustedAffine, moveAxisDestLst = NiftiVolumeData._swapAxis  (1, 2, orientation, adjustedAffine, Data, imageData, MoveAxisTuple = moveAxisDestLst)
        if (NiftiVolumeData.getOrientationString (orientation[0]) == yAxisOutputOrientiation)  :
            orientation, adjustedAffine, moveAxisDestLst = NiftiVolumeData._swapAxis  (0, 1, orientation, adjustedAffine, Data, imageData, MoveAxisTuple = moveAxisDestLst)        
        moveAxisDestLst = tuple (moveAxisDestLst)
        if moveAxisDestLst != moveAxisStartTuple :
            moveAxisDestLst = (moveAxisDestLst.index(0), moveAxisDestLst.index(1), moveAxisDestLst.index(2))
            Data = np.moveaxis (Data, moveAxisStartTuple, moveAxisDestLst)
        
        #print ("orientation == " + orientation[0]+ orientation[1] + orientation[2])
        flipAxis = []
        for index in range (3) :
            if (orientation[index] != outputOrientation[index]):
                orientation, adjustedAffine, _ = NiftiVolumeData._flipAxis (index, orientation, adjustedAffine, Data, FlipData = False)            
                flipAxis.append (index)        
        if len (flipAxis) > 0 :            
            try :
                Data = np.flip (Data, axis=tuple (flipAxis))                
            except:            
                for index in flipAxis :
                    Data = np.flip (Data, axis=index)                
        return (orientation, adjustedAffine, Data)
    
    def correctViewPortTransformation (self, XZSliceViewPortTransformation, YZSliceViewPortTransformation):
        xAxisView = NiftiVolumeData.getOrientationString (self._contourROINiftiOrientation[0])
        yAxisView = NiftiVolumeData.getOrientationString (self._contourROINiftiOrientation[1])                
        if (self.getSliceAxis () == xAxisView) :
            XZSliceViewPortTransformation.setRotation (90)  
            YZSliceViewPortTransformation.setRotation (90)  
        elif (self.getSliceAxis () == yAxisView) :
            YZSliceViewPortTransformation.setRotation (90)  
            YZSliceViewPortTransformation.setFipVertical (True)            
                            
    def normalizeOrientation (self, orientation, adjustedAffine):
        return self.transformNiftDataOrientation (orientation, ("L", "P", "I"), adjustedAffine, self._ImageData, self)
        
    @staticmethod       
    def _swapAxis (a1, a2, orientation, adjustedAffine, Data, imageData = None, MoveAxisTuple = None) :       
            axis1 = min (a1, a2)
            axis2 = max (a1, a2)    
            transform = np.zeros((4,4), dtype=float)            
            transform [3,3] = 1                         
            if (axis1 == 0 and axis2 == 1) :
                a1, a2, a3 = 1, 0, 2   
                transform [1,0] = 1
                transform [0,1] = 1
                transform [2,2] = 1                
            elif (axis1 == 0 and axis2 == 2):
                a1, a2, a3 = 2, 1, 0     
                transform [2,0] = 1
                transform [1,1] = 1
                transform [0,2] = 1
            elif (axis1 == 1 and axis2 == 2):
                a1, a2, a3 = 0, 2, 1     
                transform [0,0] = 1
                transform [2,1] = 1
                transform [1,2] = 1
            else:
                print ("unknown axis combo")                                        
            
            orientation = (orientation[a1], orientation[a2], orientation[a3])
            if (imageData is not None) :
                imageData._VoxelSize = (imageData._VoxelSize[a1],imageData._VoxelSize[a2],imageData._VoxelSize[a3])
                imageData._Dimensions= (imageData._Dimensions[a1],imageData._Dimensions[a2],imageData._Dimensions[a3])
                imageData._NormalizedVoxels= (imageData._NormalizedVoxels[a1],imageData._NormalizedVoxels[a2],imageData._NormalizedVoxels[a3])                                                                        
            transform = np.matmul( transform, adjustedAffine)   
            if MoveAxisTuple is None :
                Data = np.swapaxes (Data,axis1,axis2)
                return (orientation, transform, Data)
            else:
                MoveAxisTuple[axis1],  MoveAxisTuple[axis2] = MoveAxisTuple[axis2],  MoveAxisTuple[axis1]
                return (orientation, transform, MoveAxisTuple)
        
            
        
    @staticmethod         
    def _swapPointAxis (a1, a2, point, shape) :    
            axis1 = min (a1, a2)
            axis2 = max (a1, a2)            
            if (axis1 == 0 and axis2 == 1) :
                a1, a2, a3 = 1, 0, 2                   
            elif (axis1 == 0 and axis2 == 2):
                a1, a2, a3 = 2, 1, 0     
            elif (axis1 == 1 and axis2 == 2):
                a1, a2, a3 = 0, 2, 1     
            returnPoint = (point[a1], point[a2], point[a3])
            returnShape = (shape[a1], shape[a2], shape[a3])
            return (returnPoint, returnShape)
        
    @staticmethod         
    def _flipPointAxis (axis, point, shape) :    
        if (axis == 0):
            return (shape[0] - 1 -point[0], point[1],point[2])
        elif (axis == 1):
            return (point[0], shape[1] - 1 -point[1] ,point[2])
        elif (axis == 2):
            return (point[0], point[1], shape[2] - 1 -point[2])
        
    @staticmethod         
    def _flipAxis (axis, orientation, adjustedAffine, Data, FlipData = True) :                                
        direction = orientation[axis]        
        if (direction == "S") :
            newDirection = "I"
        elif (direction == "I") :
            newDirection = "S"
        elif (direction == "P") :
            newDirection = "A"
        elif (direction == "A") :
            newDirection = "P"
        elif (direction == "L") :
            newDirection = "R"
        elif (direction == "R") :
            newDirection = "L"
        else:
            print ("Error")
                
        transform = np.eye(4, dtype=float)            
        xdim, ydim, zdim = Data.shape
        if (axis == 0) :                    
            orientation = (newDirection, orientation[1], orientation[2])      
            if FlipData :                  
                Data = np.flip (Data, 0)
            transform[0,0] = -1.0
            transform[0,3] = xdim -1                             
        elif (axis == 1) :            
            orientation = (orientation[0], newDirection, orientation[2])   
            if FlipData :          
                Data = np.flip (Data, 1)
            transform[1,1] = -1.0
            transform[1,3] = ydim -1                             
        elif (axis == 2) :                        
            orientation = (orientation[0], orientation[1], newDirection)     
            if FlipData :          
                Data = np.flip (Data, 2)
            transform[2,2] = -1.0
            transform[2,3] = zdim -1                             
        else:
            print ("Error")                 
        transform = np.matmul( transform, adjustedAffine)     
        return (orientation, transform, Data)
    
    @staticmethod     
    def getOrientationString (orientationCode):
        if (orientationCode ==  'S' or orientationCode ==  'I' ) :
            return "Axial"
        elif (orientationCode ==  'R' or orientationCode ==  'L' ) :
            return "Sagittal"
        elif (orientationCode ==  'A' or orientationCode ==  'P' ) :
            return  "Coronal"
        else:
            return "Unknown"

        
    def get_data_dtype (self)  :
        return self._ImageData.dtype
    
    @staticmethod 
    def _init3 (tpl, default=1):
        if (len (tpl) >= 3) :
            return ((tpl[0], tpl[1], tpl[2]))
        if (len (tpl) == 2):
            return ((tpl[0], tpl[1], default))
        if (len (tpl) == 1):
            return ((tpl[0], default, default))
    
    def getAdjustedAffine (self) :
        return self._adjustedAffine 
    
    def getXDimSliceCount (self):
        return self._Dimensions[0]
        
    def getYDimSliceCount (self):
        return self._Dimensions[1]

    def getZDimSliceCount (self):
        return self._Dimensions[2]
    
    def getSliceDimensions (self):
        return self._Dimensions
        
    def getVoxelSize (self):
        return self._VoxelSize
    
    def getNormalizedVoxelSize (self):
        return self._NormalizedVoxels   
    
    def setImageData (self, data) :
        if (self._ImageData.shape == data.shape) :
            self._ImageData = data
        else:
            print ("Cannot set image, data objects do not have equal shapes")
            
    def hasTransformedImaging (self) :
        return "Transformed" in self._ChangeCache 
    
    
    def getImagingShown (self) :
        return self._ImagingShown
    
    def showTransformedImaging (self) :
        if "Transformed" in self._ChangeCache : 
            if self._ImagingShown != "Transformed" :
                self._ImageData[...] = self._ChangeCache["Transformed"][...]
                self._ImagingShown = "Transformed"
                self.callChangeListener ()
            
    def showOrigionalImaging (self) :
        if "Original" in self._ChangeCache : 
            if self._ImagingShown != "Origional" :
                self._ImageData[...] = self._ChangeCache["Original"][...]
                self._ImagingShown = "Origional"                
                self.callChangeListener ()
            
    def setSliceData (self, sliceIndex, predictionMaskData, SetMask = None, Axis="Z") :                                
        self.showTransformedImaging ()
        if (Axis == "Z") :            
            if SetMask is None :
                if (np.any (self._ImageData[:, :, sliceIndex] - predictionMaskData != 0)) :
                    if "Original" not in self._ChangeCache :
                        self._ChangeCache["Original"] = np.copy (self._ImageData) 
                        self._ChangeCache["Transformed"] = np.copy (self._ImageData)
                    self._ImageData[:, :, sliceIndex] = predictionMaskData                                        
                    self._ChangeCache["Transformed"][:, :, sliceIndex] = self._ImageData[:, :, sliceIndex]
                    self._ImagingShown = "Transformed"
                    self.callChangeListener ()
            else:
                if (np.any (self._ImageData[:, :, sliceIndex][SetMask] - predictionMaskData[SetMask] != 0)) :
                    if "Original" not in self._ChangeCache :
                        self._ChangeCache["Original"] = np.copy (self._ImageData) 
                        self._ChangeCache["Transformed"] = np.copy (self._ImageData)
                    self._ImageData[:, :, sliceIndex][SetMask] = predictionMaskData[SetMask]                    
                    self._ChangeCache["Transformed"][:, :, sliceIndex] = self._ImageData[:, :, sliceIndex]
                    self._ImagingShown = "Transformed"
                    self.callChangeListener ()
        elif (Axis == "X") :
            if SetMask is None :
                if (np.any (self._ImageData[sliceIndex, :, :] - predictionMaskData != 0)) :
                    if "Original" not in self._ChangeCache :
                        self._ChangeCache["Original"] = np.copy (self._ImageData) 
                        self._ChangeCache["Transformed"] = np.copy (self._ImageData)
                    self._ImageData[sliceIndex, :, :] = predictionMaskData                    
                    self._ChangeCache["Transformed"][sliceIndex, :, :] = self._ImageData[sliceIndex, :, :]
                    self._ImagingShown = "Transformed"
                    self.callChangeListener ()
            else:
                if (np.any (self._ImageData[sliceIndex, :, :][SetMask] - predictionMaskData[SetMask] != 0)) :
                    if "Original" not in self._ChangeCache :
                        self._ChangeCache["Original"] = np.copy (self._ImageData) 
                        self._ChangeCache["Transformed"] = np.copy (self._ImageData)
                    self._ImageData[sliceIndex,:, :][SetMask] = predictionMaskData[SetMask]                    
                    self._ChangeCache["Transformed"][sliceIndex, :, :] = self._ImageData[sliceIndex, :, :]
                    self._ImagingShown = "Transformed"
                    self.callChangeListener ()
        elif (Axis == "Y") :
            if SetMask is None :
                if (np.any (self._ImageData[:, sliceIndex, :] - predictionMaskData != 0)) :
                    if "Original" not in self._ChangeCache :
                        self._ChangeCache["Original"] = np.copy (self._ImageData) 
                        self._ChangeCache["Transformed"] = np.copy (self._ImageData)
                    self._ImageData[:, sliceIndex, :] = predictionMaskData
                    self._ChangeCache["Transformed"][:, sliceIndex, :] = self._ImageData[:, sliceIndex, :]
                    self._ImagingShown = "Transformed"
                    self.callChangeListener ()
            else:
                if (np.any (self._ImageData[:, sliceIndex,:][SetMask] - predictionMaskData[SetMask] != 0)) :
                    if "Original" not in self._ChangeCache :
                        self._ChangeCache["Original"] = np.copy (self._ImageData) 
                        self._ChangeCache["Transformed"] = np.copy (self._ImageData)
                    self._ImageData[:, sliceIndex,:][SetMask] = predictionMaskData[SetMask]
                    self._ChangeCache["Transformed"][:, sliceIndex, :] = self._ImageData[:, sliceIndex, :]
                    self._ImagingShown = "Transformed"
                    self.callChangeListener ()
            
            
    def getImageData (self, X=None,Y=None,Z=None, Axis= None, Coordinate = None, AxisProjectionControls = None, ReturnClipMask = False) :        
        
        def _ReturnClipMask (val, ReturnClipMask):
            if ReturnClipMask :
                return val, None
            return val
            
        if (X is not None and Axis is not None) :
            if Axis == "X" :
                return _ReturnClipMask(self._ImageData[X, :, :], ReturnClipMask) 
            elif Axis == "Y" :
                return _ReturnClipMask(self._ImageData[:, X, :], ReturnClipMask) 
            else:
                if AxisProjectionControls is None or Coordinate is None or not AxisProjectionControls.isAxisRotated () :
                    return _ReturnClipMask(self._ImageData[:, :, X], ReturnClipMask) 
                else:
                    result = AxisProjectionControls.getNIfTIProjection (Coordinate, self, Mask = None, UseCache = True, SliceIndex = X, ArrayMem = None) 
                    if ReturnClipMask :
                        return result
                    else:
                        return result[0]
        if (X is not None) :
            return _ReturnClipMask(self._ImageData[X, :, :], ReturnClipMask)
        if (Y is not None) :
            return _ReturnClipMask(self._ImageData[:, Y, :], ReturnClipMask)
        if (Z is not None) :
            if AxisProjectionControls is None or Coordinate is None or not AxisProjectionControls.isAxisRotated () :                
                return _ReturnClipMask(self._ImageData[:, :, Z], ReturnClipMask)
            else:
                result = AxisProjectionControls.getNIfTIProjection (Coordinate, self, Mask = None, UseCache = True, SliceIndex = Z, ArrayMem = None) 
                if ReturnClipMask :
                    return result
                else:
                    return result[0]
        #print ("No parameter to get image returning whole dataset")
        return _ReturnClipMask (self._ImageData, ReturnClipMask)
        

    def getMinMaxValues (self) :
        return (np.min (self._ImageData), np.max (self._ImageData))

    def getNIfTIVolumeOrientationDescription (self) :         
        obj = FileObject ("NIfTIVolumeOrientationDescription")
        obj.setParameter ("VolumeDimension", self._Dimensions  )            
        obj.setParameter ("TimeDimensionCount", self.getTimeDimensionCount()  )  
        obj.setParameter ("Affine", self._affine )    
        obj.setParameter ("NIfTIDataFileHeader", self._header)    
                
        
        obj.setParameter ("NativeNiftiFileOrientation", self._nativeNiftiFileOrientation)
        obj.setParameter ("ContourROINiftiOrientation", self._contourROINiftiOrientation)
        obj.setParameter ("FinalROINiftiOrientation", self._finalROINiftiOrientation)        
        obj.setParameter ("SliceAxis", self.getSliceAxis ())      
        
        
        
        # Hack make sure that it will be possbile to seralize object through Json
        try :
            outputString = StringIO()              
            obj.writeFileObject (outputString, "")
            inputString = StringIO (outputString.getvalue ())
            outputString.close ()
            
            tstObj = FileObject ("NIfTIVolumeOrientationDescription")            
            tstObj.readFileObject (inputString)            
            inputString.close ()
            return obj
        except :
            return None
    
    def getNativeVolumeOrientation (self) :
        return self._nativeNiftiFileOrientation
    
    def getContourOrientation (self) :
        return self._finalROINiftiOrientation
    
    @staticmethod        
    def _writeMaskVolume (volumeDescription, maskfilepath, objectLst,  Background = 0, MaskValue = 1, InSlicePointSize = 9, CrossSlicePointSize = 1, RoiDefs = None, BinaryOrMask = False, ExportOriginalNiftiFile = False, ExportOrigionalNiftiFilePath = "", ExportNiftiMaskfileDescription = False, roiDictionary = None, ExportAreaROIUsingBoundingVolumeDefinition = False, OverwriteFiles = True, LongNiftiFileName = None, ExportDestNiftiFileName = None) :
        try :
            ContouredSliceAxis = volumeDescription.getParameter ("SliceAxis")  
        except:
            ContouredSliceAxis = "Default"
            
        expNiftiFileName = ""
        expROIFileName = ""        
        exportTagFileName = ""
        if (ExportOriginalNiftiFile) :
            exportdir, _ = os.path.split (maskfilepath)            
            if ExportDestNiftiFileName is None :                                
                nifitDir, niftiFileName = os.path.split (ExportOrigionalNiftiFilePath)
                exportNiftiPath = os.path.join (exportdir, niftiFileName)
            else:
                nifitDir, _ = os.path.split (ExportOrigionalNiftiFilePath)
                exportNiftiPath = os.path.join (exportdir, ExportDestNiftiFileName)            
                
            if (exportNiftiPath == maskfilepath) :
                if exportNiftiPath.lower().endswith (".nii") :
                   exportNiftiPath = os.path.join (exportdir, "OrigonalNiftiFile.nii")     
                else:
                   exportNiftiPath = os.path.join (exportdir, "OrigonalNiftiFile.nii.gz")     
            if (exportNiftiPath != "" and os.path.isfile(ExportOrigionalNiftiFilePath)):                
                _, expNiftiFileName = os.path.split (ExportOrigionalNiftiFilePath)                       
                if (ExportOrigionalNiftiFilePath != exportNiftiPath) :                        
                    if (OverwriteFiles or not os.path.isfile (exportNiftiPath) ) :
                        try:                
                            shutil.copyfile (ExportOrigionalNiftiFilePath, exportNiftiPath)     
                            _, expNiftiFileName = os.path.split (exportNiftiPath)                       
                        except :
                            print ("A error occured copying the origional NIfTI data file from %s to %s." %  (ExportOrigionalNiftiFilePath, exportNiftiPath) )
                            expNiftiFileName = ""                                                        
            
                    OrigionalTagFilePath, extention = FileUtil.removeExtension  (ExportOrigionalNiftiFilePath, [".nii.gz", ".nii"])                  
                    if (extention is not None) :
                        OrigionalTagFilePath += ".DicomTags"                
                        if (os.path.isfile (OrigionalTagFilePath)):                            
                            exportTagFilePath, _  = FileUtil.removeExtension  (exportNiftiPath, [".nii.gz", ".nii"])
                            exportTagFilePath  += ".DicomTags"                                                                        
                            if (OverwriteFiles or not os.path.isfile (exportTagFilePath) ) :
                                try:                
                                    shutil.copyfile (OrigionalTagFilePath, exportTagFilePath)                                     
                                except :
                                    print ("A error occured copying the Dicom Tag file associated with the NIfTI data file from %s to %s." %  (OrigionalTagFilePath, exportTagFilePath) )
                                    exportTagFileName = ""                                                        
            else:                
                print ("A error occured finding the origional NIfTI data file. Path = %s" % (ExportOrigionalNiftiFilePath))
                print (exportNiftiPath)                        
            try :
                ROIDatasourcePath= ""
                if (roiDictionary is not None) :
                    ROIDatasourcePath = roiDictionary.getLoadedROIFilename ()
                if (ROIDatasourcePath != "" and os.path.isfile(ROIDatasourcePath)) :                                        
                    exportROIPath, _  = FileUtil.removeExtension  (exportNiftiPath, [".nii.gz", ".nii"])
                    exportROIPath += "_ROI.txt"                                        
                    if (exportROIPath != ROIDatasourcePath) :                        
                        if (OverwriteFiles or not os.path.isfile (exportROIPath) ) :
                            try:                
                                shutil.copyfile (ROIDatasourcePath, exportROIPath)      
                                _ ,expROIFileName = os.path.split (exportROIPath)
                            except :
                                print ("A error occured copying the origional roi data file from %s to %s." % (ROIDatasourcePath, exportROIPath))
                                expROIFileName = ""                                                        
                else:
                    print ("A error occured finding the origional roi data file. Path = %s" % (ROIDatasourcePath))
            except:
                expROIFileName = ""
        
        if (not OverwriteFiles and os.path.isfile (maskfilepath)) :
            #print ("nifti mask allready found skipping")
            return maskfilepath        
        
        if (ExportNiftiMaskfileDescription) :
            try :
                exportdir, _ = os.path.split (maskfilepath)
                exportfiledescription = os.path.join (exportdir, "NIfTIMaskFileDescription.csv")                    
                maskDictionary = collections.OrderedDict ()                
                if (os.path.isfile(exportfiledescription)) :                                                                                       
                    firstline = True
                    fd = open (exportfiledescription, "rt")                          
                    for line in fd :
                        if (not firstline) :
                            params = json.loads (line)
                            maskfilename = params["NIfTI_Mask_File"]
                            maskDictionary[maskfilename] = params
                        else:
                            firstline = False
                    fd.close ()
                ROIDatasourcePath= ""
                if (roiDictionary is not None) :
                    ROIDatasourcePath = roiDictionary.getLoadedROIFilename ()
                    
                maskLst = []                
                if (MaskValue is None and RoiDefs is not None) :                                                
                    for obj in objectLst :
                        name = obj.getName (RoiDefs)               
                        writeMaskValue = RoiDefs.getROIMaskValue (name)
                        RadlexID = RoiDefs.getROINameRadlexID (name)
                        roiMaskentry = collections.OrderedDict ()
                        roiMaskentry ["RadlexID"] = RadlexID
                        roiMaskentry ["ROI_Name"] = name
                        roiMaskentry ["ROI_Type"] = obj.getType()
                        roiMaskentry ["NIfTI_Mask_Value"] = writeMaskValue
                        maskLst.append (roiMaskentry)
                _, fname = os.path.split (maskfilepath)    
                
                dt = datetime.datetime.now ()
                time = (dt.hour, dt.minute, dt.second, dt.microsecond)
                date = (dt.month, dt.day, dt.year)
                
                try :
                    nativeOrientation = volumeDescription.getParameter ("NativeNiftiFileOrientation")
                    finalContouredOrientation = volumeDescription.getParameter ("FinalROINiftiOrientation")                    
                    point, shape = NiftiVolumeData.transformPoint (finalContouredOrientation, nativeOrientation, (0,0,1), (2,2,2))                                        
                    ContourAxisTuple = [ContouredSliceAxis, point]
                except:                    
                    ContourAxisTuple = [ContouredSliceAxis]
                
                DataFileEntry = collections.OrderedDict ()
                if LongNiftiFileName is None :
                    LongNiftiFileName = ExportOrigionalNiftiFilePath
                DataFileEntry ["Source_nifti_long_file_name"] = LongNiftiFileName
                DataFileEntry ["NIfTI_Mask_File"] = fname
                DataFileEntry ["Contour_Slice_Axis"] = ContourAxisTuple
                DataFileEntry ["ROI_Mask_List"] = maskLst
                DataFileEntry ["Background_Voxel_Value"] = Background                
                DataFileEntry ["Default_Mask_Value"] = MaskValue                
                DataFileEntry ["In_slice_point_size"] = InSlicePointSize
                DataFileEntry ["Cross_slice_point_size"] = CrossSlicePointSize
                DataFileEntry ["Binary_OR_mask_value"] = BinaryOrMask
                DataFileEntry ["exported_nifti_file_name"] = expNiftiFileName                
                DataFileEntry ["source_nifti_file"] = ExportOrigionalNiftiFilePath
                DataFileEntry ["ROIFileName"] = expROIFileName                
                DataFileEntry ["ROI_data_file_path"] = ROIDatasourcePath                
                DataFileEntry ["Dicom_TagFileName"] = exportTagFileName
                DataFileEntry ["log_time(h:m:s:ms)"] = time
                DataFileEntry ["log_date(m:d:y)"] = date                
                maskDictionary[fname] = DataFileEntry                
                
                fd = open (exportfiledescription, "wt")    #write header
                header = json.dumps (["Source_nifti_long_file_name", 'NIfTI_Mask_File', "Contour_Slice_Axis", { "ROI_Mask_List" : ["RadlexID","ROI_Name", "ROI_Type", "NIfTI_Mask_Value"]} , "Background_Voxel_Value", "Default_Mask_Value", "In_slice_point_size", "Cross_slice_point_size", "Binary_OR_mask_value", "exported_nifti_file_name","source_nifti_file",  "ROIFileName", "ROI_data_file_path", "Dicom_TagFileName","log_time(h:m:s:ms)", "log_date(m:d:y)"])
                fd.write (header + "\n")                        
                for value in maskDictionary.values () :                    
                    fd.write  (json.dumps(value) + "\n")                      
                fd.close ()
               
            except :
                print ("A error occured saving the nifti file description CSV file.")
                    
        shape = volumeDescription.getParameter ("VolumeDimension")
        data = np.zeros (shape, dtype=np.uint16)                      
        Shape2d = (shape[0], shape[1])         
        if (Background != 0) :            
            Background = np.array ([Background], dtype=np.uint16)[0]
            data[:] = Background
        
        
        if (RoiDefs is not None and RoiDefs.hasSingleROIPerVoxel ()) :
            objectLst = RoiDefs.getObjectLstInROIOrder (objectLst)
            objectLst.reverse ()
        
        for obj in objectLst :            
            if (MaskValue is None and RoiDefs is not None) :
               name = obj.getName (RoiDefs)                              
               writeMaskValue = np.array ([RoiDefs.getROIMaskValue (name)], dtype=np.uint16)[0]               
            else:
                writeMaskValue = np.array ([MaskValue], dtype=np.uint16)[0]                                       
            
            if obj.getType() == "Area" :
                objXRange, objYRange = obj._getAxisBoundingBox (sliceAxis = 'Z')            
                _, objZRange = obj._getAxisBoundingBox (sliceAxis = 'Y')
                if (ExportAreaROIUsingBoundingVolumeDefinition) :
                    
                   startX, endX =  objXRange 
                   startY, endY =  objYRange 
                   startZ, endZ =  objZRange 
                   try :
                       if (BinaryOrMask) :
                            data[max (0, int (startX)) : min(shape[0], int (endX)+1), max (0, int (startY)) : min(shape[1], int (endY)+1), max (0, int (startZ)) : min(shape[2], int (endZ)+1)] = data[max (0, int (startX)) : min(shape[0], int (endX)+1), max (0, int (startY)) : min(shape[1], int (endY)+1), max (0, int (startZ)) : min(shape[2], int (endZ)+1)] | writeMaskValue
                       else:
                            data[max (0, int (startX)) : min(shape[0], int (endX)+1), max (0, int (startY)) : min(shape[1], int (endY)+1), max (0, int (startZ)) : min(shape[2], int (endZ)+1)] = writeMaskValue      
                   except:
                        print ("A error occured trying to set bounding box mask definition.")
                else:    
                    firstSlice = int (max (0, int (objZRange[0])))
                    lastSlice = int (min (shape[2], int (objZRange[1] + 1)))
                    ClipOverlayedObjects = False
                    if (RoiDefs is not None and RoiDefs.hasSingleROIPerVoxel ()) :
                        ClipOverlayedObjects = True
                    for z in range (firstSlice, lastSlice, 1) :            
                        try :
                            if (obj.hasSlice(z)) :             
                                
                                slicemap = obj.getSliceMap (z, Shape2d, ClipOverlayedObjects = ClipOverlayedObjects)         
                                minV = np.min (slicemap)
                                maxV = np.max (slicemap)
                                if (minV != 0 and minV != 1) :
                                    print ("Warrning: Slicemap not zeroed as expected, correcting.")
                                    slicemap -= minV
                                    maxV -= minV
                                if (maxV != 1 and maxV != 0) :
                                    print ("Warrning: slicemap data max not 1 as expected, correcting.")
                                    slicemap = (slicemap / maxV).astype (np.int)
                                    maxV = 1
                                                        
                                sliceData = data[:,:,z]                            
                                if (BinaryOrMask) :                            
                                    mergeData = slicemap * writeMaskValue
                                    sliceData = np.bitwise_or (sliceData, mergeData.astype (np.uint16))
                                else:                                
                                    sliceData[slicemap > 0] = writeMaskValue                                     
                                data[:,:,z] = sliceData
                        except:
                            print ("A error occured trying to export slice: %d" % (z))
                        
            elif obj.getType() == "Point" :
                pointlst = obj.getPointList ()
                for point in pointlst :
                    centerx = int (point.getXCoordinate ())
                    centery = int (point.getYCoordinate ())
                    centerz = int (point.getZCoordinate ())
                    
                    startX = centerx - int ((InSlicePointSize -1) / 2)
                    endX = startX + InSlicePointSize 
                    startY = centery - int ((InSlicePointSize - 1) / 2)
                    endY = startY + InSlicePointSize 
                    startZ = centerz - int ((CrossSlicePointSize - 1) / 2)
                    endZ = startZ + CrossSlicePointSize
                    
                    #z = list (range (max (0, int (startZ)), min(shape[2] - 1, int (endZ)), 1))
                    #y = list (range (max (0, int (startY)), min(shape[1] - 1, int (endY)), 1))
                    #x = list (range (max (0, int (startX)), min(shape[0] - 1, int (endX)), 1))
                    try :
                        if (BinaryOrMask) :
                            data[max (0, int (startX)) : min(shape[0], int (endX)), max (0, int (startY)) : min(shape[1], int (endY)), max (0, int (startZ)) : min(shape[2], int (endZ))] = data[max (0, int (startX)) : min(shape[0], int (endX)), max (0, int (startY)) : min(shape[1], int (endY)), max (0, int (startZ)) : min(shape[2], int (endZ))] | writeMaskValue
                        else:
                            data[max (0, int (startX)) : min(shape[0], int (endX)), max (0, int (startY)) : min(shape[1], int (endY)), max (0, int (startZ)) : min(shape[2], int (endZ))] = writeMaskValue
                    except:
                        print ("A error occured trying to set the point mask")
            else:   
                print ("Unknown Object Type")                
        
        nativeOrientation = volumeDescription.getParameter ("NativeNiftiFileOrientation")
        finalContouredOrientation = volumeDescription.getParameter ("FinalROINiftiOrientation")
        exportData = NiftiVolumeData.transformData (finalContouredOrientation, nativeOrientation, data)    
        
        try :
            img = nib.Nifti1Image(exportData, volumeDescription.getParameter ("Affine") , header = volumeDescription.getParameter ("NIfTIDataFileHeader"))
            nib.save (img, maskfilepath)
        except:
            img = nib.Nifti1Image(exportData, volumeDescription.getParameter ("Affine") )
            nib.save (img, maskfilepath)
            
        if roiDictionary is not None :
            try :
                tags = roiDictionary.getDataFileTagManager ()
                if tags is not None :
                    if tags.hasInternalTag ("OrigionalImageFileName") :
                        originalImageFileName = tags.getInternalTag ("OrigionalImageFileName") 
                        if ExportOriginalNiftiFile :
                            try :
                                _, fname = os.path.split (originalImageFileName)
                                nifitDir, _ = os.path.split (ExportOrigionalNiftiFilePath)
                                sourceImage = os.path.join (nifitDir, fname)
                                
                                destImage, _  = FileUtil.removeExtension  (exportNiftiPath, [".nii.gz", ".nii"])
                                extension = destImage.split (".")
                                if len (extension) > 1 :
                                    extension = extension[-1]
                                    extension = extension.strip ()
                                    if len (extension) > 0 :
                                        destImage += "." + extension
                                    
                                if os.path.isfile (sourceImage) and os.path.exists (sourceImage) :                                    
                                    if (OverwriteFiles or not os.path.isfile (destImage) ) :                            
                                        shutil.copyfile (sourceImage, destImage)         
                            except:
                                print ("A error occured exporting the origional 2D imaging")
                        try :
                            extension="png"
                            lmaskpath = maskfilepath.lower ()
                            if lmaskpath.endswith (".nii") :
                                imageMaskPath = maskfilepath[:-len ("nii")] + extension
                            elif lmaskpath.endswith (".nii.gz") :
                                imageMaskPath = maskfilepath[:-len ("nii.gz")] + extension
                            else :
                                imageMaskPath = maskfilepath + "." + extension
                            
                            exportImageData = np.copy (exportData)
                            if np.any (exportImageData < 0) or  np.any (exportImageData > 65535) :
                                print ("Warrning exported 2d image is being clipped to 0-65535 export range. Values outside of this range are not exported in the 2d mask!")
                                exportImageData[exportImageData<0] = 0
                                exportImageData[exportImageData>65535] = 0
                            exportImageData = np.clip (exportImageData,0,65535)
                            exportImageData = exportImageData.astype (np.uint16)
                            imageData = np.zeros ((exportImageData.shape[0], exportImageData.shape[1]),dtype = np.uint16)
                            imageData[:,:] = exportImageData[:,:,0]
                            imageData = np.rot90 (imageData, 1)
                            imageData = imageData[::,::-1,...]
        
                            skimage.io.imsave (imageMaskPath, imageData) 
                        except:
                            print ("A error occured exporting the 2D imaging mask")
            except:
                 print ("A error occured trying to export2D imaging")
        print ("Done exporting: " + maskfilepath)
        return maskfilepath
    
    def writeMaskVolume (self, maskfilepath, objectLst,  Background = 0, MaskValue = 1, InSlicePointSize = 9, CrossSlicePointSize = 1, RoiDefs = None, BinaryOrMask = False, ExportNiftiFile = False, ExportNiftiMaskfileDescription = False, roiDictionary = None, ExportAreaROIUsingBoundingVolumeDefinition = False, OverwriteFiles = True, ExportDestNiftiFileName = None) :                
        longfilenamePath = LongFileNameAPI.getLongFileNamePath (self._FileName)
        return self._writeMaskVolume (self.getNIfTIVolumeOrientationDescription (),  maskfilepath, objectLst,  Background, MaskValue , InSlicePointSize, CrossSlicePointSize, RoiDefs, BinaryOrMask, ExportNiftiFile, self._FileName, ExportNiftiMaskfileDescription, roiDictionary, ExportAreaROIUsingBoundingVolumeDefinition, OverwriteFiles, LongNiftiFileName = longfilenamePath, ExportDestNiftiFileName = ExportDestNiftiFileName)
        
   
               
    
       
class AreaMerge :
    
    @staticmethod
    def _intersectLines( pt1, pt2, ptA, ptB ): 
        a = int (pt1[0])
        b = int (pt2[0]) - int (pt1[0])
        e = int (pt1[1])
        f = int (pt2[1]) - int (pt1[1])
        
        c = int (ptA[0])
        d = int (ptB[0]) - int (ptA[0])
        g = int (ptA[1])
        h = int (ptB[1]) - int (ptA[1])

        den = int ((h*b) - (f*d))
        if (den == 0) : #lines are parallel
           #print ("testing parallel lines for ovralling")     
           intersect1X = c - a   
           intersect1Y = g - e   
           
           if (intersect1X * b + f * intersect1Y == 0):  #dot product of the vector described by segment 1 and a line from segment1 to segment 2 is 0, if its 0 the segments are on the same line
               #test if segments ovrlap.
               segment1minX = int (min ( pt2[0], pt1[0])) 
               segment1maxX = int (max ( pt2[0], pt1[0]))
            
               segment2minX = int (min ( ptA[0], ptB[0]))
               segment2maxX = int (max ( ptA[0], ptB[0]))
               
               segment1minY = int (min ( pt2[1], pt1[1]))
               segment1maxY = int (max ( pt2[1], pt1[1]))
            
               segment2minY = int (min ( ptA[1], ptB[1]))
               segment2maxY = int (max ( ptA[1], ptB[1]))
               
               #test if segments overlap
               if (segment2maxX < segment1minX or segment2minX > segment1maxX) :  
                   #print ("lines fall on one side of each other")  
                   return ((0,0),0)
               if (segment2maxY < segment1minY or segment2minY > segment1maxY) :
                   #print ("lines fall on one side of each other")  
                   return ((0,0),0)
               #line segmnts at least partially overlap find the point of ovralping 
               vector2Len = int (math.sqrt (h*h + d*d))
               if (vector2Len > 0) :
                   for i in range (vector2Len + 1) :
                       px = c + ((i * d) / vector2Len)
                       py = g + ((i * h) / vector2Len)
                       if (px >= segment1minX and px <= segment1maxX and py >= segment1minY and py <= segment1maxY):
                           #print ("segments overlap")                        
                           return ((int (px), int (py)), 1)
           #print ("segments not on the same Line")
           return ((0,0),0)         
        
        if (d == 0) : # Lines are not parallel but second line has no x slope, 
            #print ("line segment has no slope")
            intersect = c - a            
            pt = float (intersect) / float (b)            
            if (pt >= 0 and pt <= 1.0):
                testy = int (pt* float (f)) + e
                
                if (testy >= min (ptA[1], ptB[1]) and testy <= max (ptA[1], ptB[1])) :               
                    #print ("d=0 intersection hit")                    
                    return ((int (c),int (testy)),1)
            #print ("d=0 intersection fail")  
            return ((0,0),0)           
        #if den == 0 then parrallel test for overlaping
        
        #if d = 0 then second line is flat.. 
        # test if line crosses.
            
        p1 = float (e*d - h*a -g*d + c*h) / float (den)
        p2 = float (float (a - c) + (float (b) * p1)) / float (d)
        if (p1 >= 0 and p1 <= 1.0 and p2 >= 0 and p2 <= 1.0) :
            x1 = a + int (float (b) * p1)
            y1 = e + int (float (f) * p1)
            return ((int (x1), int (y1)),1)
        return ((0,0),0) 
     
    @staticmethod
    def _LineCrossSegment ( cord1, cord2, oldLineList, i, starti): 
        if i == len(oldLineList):
            i = 0
        previ = i - 1
        if previ < 0 :
           previ = len(oldLineList) - 1 
        while True: 
            result = AreaMerge._intersectLines (cord1, cord2, oldLineList[previ], oldLineList[i])
            if (result[1] != 0):
                return (True, i, result[0])
            previ = i     
            i = i + 1
            if i == len(oldLineList):
                i = 0
            if i == starti :        
                return (False, 0, (0, 0))
    
    @staticmethod
    def _MergeLists ( newLineList, firstCrossX, secondCrossX, oldLineList, firstCross, secondCross) :
        start = firstCross[1]
        end   = secondCross[1]        
        retList = []       
        retList.append (firstCross[2])                
        for x in range (firstCrossX, secondCrossX) :        
            retList.append (newLineList[x]) 
        retList.append (secondCross[2])        
        length = len (oldLineList)
        i = end
        while i != start :     
            retList.append (oldLineList[i].tolist ())
            i = i + 1
            if i == length:
                i = 0 
        retList.append ( firstCross[2])    
        return retList
        
    @staticmethod
    def mergeLineSegments ( oldLineList, newLineList):                  
        i = 1        
        firsti = i
        newLineListLen =  len (newLineList) -1        
        for x in range (1, newLineListLen) :        
            firstCross = AreaMerge._LineCrossSegment (newLineList[x-1], newLineList [x], oldLineList, i, firsti)            
            if firstCross[0] == True :  
                firsti = firstCross[1]
                i = firsti  
                firstCrossX = x
                       
                for x in range (newLineListLen, firstCrossX, -1) :                           
                    secondCross = AreaMerge._LineCrossSegment (newLineList[x-1], newLineList [x], oldLineList, i, firsti)
                    if secondCross[0] == True and firstCross[2] != secondCross[2]   :                
                        #print ("Merging Lists")                        
                        return True, AreaMerge._MergeLists (newLineList, firstCrossX, x, oldLineList, firstCross, secondCross)                                   
                 
                #print ("New Lists Hit 1")
                return False, copy.copy (newLineList)

        #print ("New Lists No hit")
        return False, copy.copy (newLineList)

class FileChangeMonitor :    
    def __init__ (self, path = None):
        self.monitorFile (path)
    
    def copy (self) :
        fcm = FileChangeMonitor ()
        fcm._filePath = self._filePath 
        fcm._fileSize = self._fileSize 
        fcm._fileLastWritten = self._fileLastWritten 
        return fcm
    
    def getMonitorDescription (self) :
        description = {}
        description["Path"] = self._filePath 
        description["Size"] = self._fileSize 
        description["LastWrite"] = self._fileLastWritten 
        return description 
    
    def initFromDescription (self, description, CorrectFilePath = None) :        
        self._filePath  = description["Path"]
        if CorrectFilePath is not None :
            self._filePath  = CorrectFilePath
        self._fileSize  = description["Size"]
        self._fileLastWritten  = description["LastWrite"]        
    
    def clear (self) :
        self._filePath = None
        self._fileSize = None
        self._fileLastWritten = None
        
    def monitorFile (self, path = None):
        self._filePath = path
        self._fileSize = None
        self._fileLastWritten = None
        if path is None :
            return 
        if os.path.exists (path) :
            try :
                fileResults = os.stat (self._filePath)
                self._fileSize = fileResults.st_size 
                self._fileLastWritten  = fileResults.st_mtime
            except:
                self._fileSize = None
                self._fileLastWritten = None
                
    def getPath (self) :
        return self._filePath
        
    def hasFileChanged (self, filePath, ByteCompare = False, IgnoreFileName = False) :
        if (not IgnoreFileName):
            if (self._filePath != filePath) :
                return True
            if (self._filePath is None) :
                return False            
        if (self._filePath is None and filePath is not None) :
            return True
        if (self._filePath is not None and filePath is None) :
            return True        
        if not os.path.exists (self._filePath) :
            if self._fileSize is not None or self._fileLastWritten is not None :
                self._fileSize = None
                self._fileLastWritten = None
                return True
            return False
        elif ByteCompare :
            try :
                if not filecmp.cmp(self._filePath, filePath, shallow=False) :
                    fileResults = os.stat (self._filePath)
                    fileSize = fileResults.st_size 
                    fileLastWritten  = fileResults.st_mtime
                    self._fileSize = fileSize 
                    self._fileLastWritten = fileLastWritten
                    return True
                else:
                    return False
            except:
                self._fileSize = None
                self._fileLastWritten = None
                return False
        else:
            try :
                fileResults = os.stat (filePath)
                fileSize = fileResults.st_size 
                fileLastWritten  = fileResults.st_mtime
                if (fileSize != self._fileSize or self._fileLastWritten != fileLastWritten) :
                    self._fileSize = fileSize 
                    self._fileLastWritten = fileLastWritten
                    return True
                else:
                    return False
            except:
                self._fileSize = None
                self._fileLastWritten = None
                return False



class FileObject :
    def __init__ (self, name) :
         self._name = name
         self._parameters = collections.OrderedDict ()         
         self._fileObjects = {}
         self._writeCache = None                
    
    @staticmethod
    def processEvent (QApp = None) :
        try :
            if (QApp is not None) :
                QApp.processEvents()
        except:
            pass
            
        
    def getFileObjectTxt (self, indent = "\t", skipobjectlst = [], partialOutputSkipFileHeader = False, QApp = None) :
        if (_usecStringIO) :
            output = cStringIO.StringIO()
        else:           
            output = StringIO()  
        
        if (len (skipobjectlst) > 0) :
            if (_usecStringIO) :
                skipOutput = cStringIO.StringIO()
            else:           
                skipOutput = StringIO()  
                
            self.writeFileObject (output, indent, skipobjectlst = skipobjectlst, skipOutput = skipOutput, partialOutputSkipFileHeader = partialOutputSkipFileHeader)                                
            fullTxt = output.getvalue ()
            skipTxt = skipOutput.getvalue ()
            output.close () 
            skipOutput.close ()
            return fullTxt, skipTxt
        else:
            self.writeFileObject (output, indent)                                
            fullTxt = output.getvalue ()            
            output.close ()
            return fullTxt
        
    def clearWriteCache (self, ignoreobjects = []) :
        self._writeCache = None
        for key, value in self._fileObjects.items () :
            if (key not in ignoreobjects) :
                value.clearWriteCache (ignoreobjects = ignoreobjects)
        
    def setWriteCache (self, writecache) :
        self._writeCache = writecache
    
    def setFileObjectName (self, name) :
        self._name = name
        self._writeCache = None
        
    def getFileObjectName (self):
        return self._name
        
    def setParameter (self, key, obj) :        
        self._parameters[key] = obj

    def getParameter (self, key) :    
        return self._parameters[key]

    def hasParameter (self, key) :
        return key in self._parameters

    def removeParameter (self, key):
        del self._parameters[key]
    
    def getFileObjects (self):
        lst = []
        for _, fobj in self._fileObjects.items () :
            lst.append (fobj)
        return lst
    
    def getFileObjectKeyList (self):
        return list (self._fileObjects.keys ())
    
    def getFileObject (self, key):
        return self._fileObjects[key]

    def hasFileObject (self, key):
        return key in self._fileObjects

    def removeFileObject (self, key) :
        del  self._fileObjects[key]
        
    def addInnerFileObject (self, fobj) :
        self._fileObjects[fobj.getFileObjectName ()] = fobj
        
    def writeFileObject (self, f, indentStr, skipobjectlst = [], skipOutput = None, partialOutputSkipFileHeader = False):
           if (self._writeCache is not None and len (skipobjectlst) == 0 and skipOutput is None) :                                                     
               f.write(self._writeCache)
               return
                      
           objName = ""
           if (self.hasParameter ("Name")) :
               objName = self.getParameter ("Name")           
               #print ("Returning Uncached " + objName) 
           
           fileobjectCountParamStr = objName + self.getFileObjectName ()+"FILEOBJECTCOUNT"
           self.setParameter (fileobjectCountParamStr, len (self._fileObjects))           
           
           linelist = []
           linelist.append   (indentStr)
           linelist.append   (json.dumps (("Start FileObject", self._name, len (self._parameters))))
           linelist.append   ("\n")           
           linetxt = "".join (linelist)
           f.write (linetxt)
           if (not partialOutputSkipFileHeader and skipOutput is not None) :
               skipOutput.write   (linetxt)               
           
           fobj_indent = indentStr + "\t"
           sortedKeys = sorted (list(self._parameters.keys()))
           for key in sortedKeys :               
               linetxt=    FileObject._writeParam (key, self._parameters[key], fobj_indent)
               f.write (linetxt)
               if (skipOutput is not None and key not in skipobjectlst):
                  skipOutput.write (linetxt)
           
           sortedKeys = sorted (list(self._fileObjects.keys ()))           
           for key in sortedKeys :
               fobj = self._fileObjects[key]               
               linetxt = FileObject._writeFileObject (fobj, fobj_indent)
               f.write (linetxt)
               if (skipOutput is not None and key not in skipobjectlst) :
                   skipOutput.write (linetxt)
           
           linelist = []
           linelist.append  (indentStr)
           linelist.append  (json.dumps (("End FileObject", self._name)))
           linelist.append  ("\n")
           linetxt = "".join (linelist)
           f.write (linetxt)
           if (not partialOutputSkipFileHeader and skipOutput is not None) :
               skipOutput.write   (linetxt)      
    
    @staticmethod 
    def _writeParam (key, parameter, indentStr ) :
        linelist = []
        linelist.append (indentStr)               
        try :
           linelist.append  (json.dumps ((key, parameter)))
        except :
           #could not json serialze.  Try pickeling value before saveing.
           pickleResult = pickle.dumps (parameter, protocol=2)
           hexStr = binascii.hexlify (pickleResult)
           hexStr = hexStr.decode('utf-8')                       
           linelist.append  (json.dumps ((key, hexStr, "pickeled value")))                       
        linelist.append  ("\n")                                            
        linetxt = "".join (linelist)
        return linetxt
    
    @staticmethod       
    def _writeFileObject ( fobj, fobj_indent) :
        if (_usecStringIO) :
            output = cStringIO.StringIO()
        else:           
            output = StringIO()  
        fobj.writeFileObject (output, fobj_indent)
        linetxt = output.getvalue ()
        fobj.setWriteCache (linetxt)               
        output.close ()  
        return linetxt
           
    
    def readFileObject (self, f, QApp = None) :
        FileObject.processEvent (QApp)
        line = f.readline ()
        _, self._name, count = json.loads (line)
        self._parameters = collections.OrderedDict ()
        self._fileObjects = {}
        for x in range (count) :            
            rline = f.readline ()
            try :
                tupl = json.loads (rline)
                if (len (tupl) == 2) :
                    key, val = tupl 
                    if (key == "End FileObject") and (val == self._name) :
                         return               
                else:
                    key, val, pickedValue = tupl 
                    val =  binascii.unhexlify(val)
                    val = pickle.loads (val)
                if (key not in self._parameters) :
                    self._parameters [key] = val                    
                else:
                    print ("File Read Error Duplicate Keys Found")
            except:
                pass            
            FileObject.processEvent (QApp)
            
            
        fileobjectCountParamStr = self.getFileObjectName ()+"FILEOBJECTCOUNT"
        if (not self.hasParameter (fileobjectCountParamStr) and self.hasParameter ("Name")) :
            fileobjectCountParamStr = self.getParameter ("Name") + self.getFileObjectName ()+"FILEOBJECTCOUNT"            
        if (self.hasParameter (fileobjectCountParamStr)) :
           count = self.getParameter(fileobjectCountParamStr)
           for x in range (count) :
               obj = FileObject ("Unknown")
               obj.readFileObject (f, QApp = QApp)
               self._fileObjects[obj.getFileObjectName ()] = obj               
        _, _ = json.loads (f.readline ())        



class FileSaveThreadManager :    

        
    def getGlobalFileSaveThread (self) :        
        try:                        
            return self._GlobalFileSaveThread
        except:
            print ("CREATING GLOBAL FILE THREAD")
            self._GlobalFileSaveThread = FileSaveThread ()
            return self._GlobalFileSaveThread
    
    def pauseThread (self) :
        try:
            if (self._GlobalFileSaveThread is not None) : # allocated
                 self._GlobalFileSaveThread.pauseThread ()
        except :            
            pass  
        
    def restartThread (self) :
        try:
            if (self._GlobalFileSaveThread is not None) : # allocated
                 self._GlobalFileSaveThread.restartThread ()
        except :            
            pass  
        
    def closeAndDeleteThread (self) :        
        try:
            if (self._GlobalFileSaveThread is not None) : # allocated
                try :
                    self._GlobalFileSaveThread.close ()
                    print ("CLOSING GLOBAL SAVE Thread")                    
                finally:
                    del self._GlobalFileSaveThread
        except :            
            pass
                    

def _processMap_ProcessBased (function, arguments, returnPipe) :
   #pid = os.getpid()
   #print ("Starting: ", pid)       
   result = None
   try:
       #fileHandleList = PlatformMP.getHandlesInParameters (arguments)
       result = function (arguments)
       #for filehandle in fileHandleList :
       #    filehandle.closeProcessHandleCleanup ()
   except:
       result = None
   finally :
       if returnPipe is not None :    
           try :
               returnPipe.send (result)
               returnPipe.close ()
           except:
               print ("Error returning process result")
       else:   
           return result
       
     
class PlatformThreading :
    
    @staticmethod
    def _threadFunc (function, param, threadResults, threadwait) :    
        try:
            threadResults.append (function (param))
        except:
            threadResults.append (None)
        finally :
            while (True) :
                try :
                    with threadwait :
                        threadwait.notify ()
                        break
                except:
                    print ("Thread notification failed")
                    time.sleep (5)                    
    
    @staticmethod 
    def _poolFunc (threadParam) :
        func, lock, threadCounter, param = threadParam
        try :
            lock.acquire ()
            threadCounter[0] += 1
        finally:
            lock.release ()
        try :
            return func(param)
        except:
            return None
        
    @staticmethod 
    def _threadPoolRunner (ThreadMax, result, ParamList) :
        try :
            pool = MP_Pool(ThreadMax) 
            result += list (pool.map (PlatformThreading._poolFunc, ParamList))
        finally:
            pool.close ()
            

    
    @staticmethod 
    def ThreadMap (function, ParamList, App = None, SleepTime = 0.05, Debug = False, ThreadMax = 10, ProgressDialog = None, ProjectFileInterface = None, BaseProgressBarIndex = 0, ProgressDialogTxtLst = None):
        ParamCount = len (ParamList)
        if ParamCount == 0 :
            return []
        elif ParamCount == 1 :
            return [function (ParamList[0])]
        elif Debug or ThreadMax == 1:
            result = []
            if App is None :                
                for param in ParamList :
                    result.append (function(param))                
            else:
                lastTime = time.time ()
                for param in ParamList :
                    result.append (function(param))                
                    currentTime = time.time () 
                    if currentTime - lastTime > SleepTime :
                        lastTime = currentTime
                        App.processEvents ()
            return result
        else:
            try :
                if ProjectFileInterface is not None :
                   ProjectFileInterface.setMPEnabled (True)
                if App is None :
                    try :   
                       try :                           
                           pool = MP_Pool(ThreadMax) 
                           result = list (pool.map (function, ParamList))
                           return result
                       finally:
                           try:
                              pool.close ()
                           except:
                              pass
                    except:
                        pass
                else:
                    lock = Lock ()
                    threadCounter = [0]     
                    threadParam = []
                    for param in ParamList:
                        threadParam.append ((function, lock, threadCounter, param))
                    #threadParam = PlatformThreading.ListIterator ((function, lock, threadCounter), ParamList)
                    results = []
                    th = Thread (target = PlatformThreading._threadPoolRunner,  args=(ThreadMax, results, threadParam)) 
                    th.start ()
                    while th.is_alive ():
                        time.sleep (SleepTime)
                        if ProgressDialog is not None : 
                            try:
                                lock.acquire ()
                                ParamIndex = threadCounter[0]
                            finally:
                                lock.release ()
                            ProgressDialog.setValue (ParamIndex + BaseProgressBarIndex)
                            if ProgressDialogTxtLst is not None :
                                if ParamIndex < len (ProgressDialogTxtLst) :
                                    ProgressDialog.setLabelText (ProgressDialogTxtLst [ParamIndex])
                                else:
                                    ProgressDialog.setLabelText (ProgressDialogTxtLst [-1])
                            App.processEvents()
                    th.join ()
                    del threadCounter
                    del lock
                    del th
                    del threadParam
                    return results
                    
                """    
                
                RunningThreadList = []
                threadPoolResults = collections.OrderedDict ()
                ParamCounter = 0
                try:
                    threadwait = Condition (Lock ())
                    with (threadwait) :      
                        while ParamCounter < len (ParamList)  :
                           if ProgressDialog is not None and ProgressDialog.wasCanceled () :
                                break
                           if len (RunningThreadList) > 0 :                                
                               for index in range (len (RunningThreadList)-1,-1,-1) :
                                   if not RunningThreadList[index].is_alive() :
                                      del RunningThreadList[index]      
                                                                                       
                           while len (RunningThreadList) < ThreadMax and  ParamCounter < len (ParamList) :
                                   param = ParamList[ParamCounter]
                                   if ProgressDialog is not None :
                                       if ProgressDialog.wasCanceled () :
                                           break                                   
                                   ParamCounter += 1                            
                                   threadResults = []
                                   th = Thread (target=PlatformThreading._threadFunc, args=(function, param,threadResults, threadwait))          
                                   th.daemon = True
                                   RunningThreadList.append (th)
                                   threadPoolResults[th] = threadResults
                                   while True :
                                       try :
                                           th.start ()                                   
                                           break
                                       except :
                                           print ("Error occured Starting Thread")
                                           time.sleep (4)                                              
                           if ProgressDialog is not None :                       
                               ParamIndex = max (0, ParamCounter -1)
                               ProgressDialog.setValue (ParamIndex + BaseProgressBarIndex)
                               if ProgressDialogTxtLst is not None :
                                   ProgressDialog.setLabelText (ProgressDialogTxtLst [ParamIndex])
                           if len (RunningThreadList) == ThreadMax :
                                while (True) :
                                   App.processEvents ()           
                                   #print ("Waiting for threads to finish running")
                                   if threadwait.wait (SleepTime) :
                                       break
                                   
                        while len (RunningThreadList) > 0:
                           for index in range (len (RunningThreadList)-1,-1,-1) :
                               if not RunningThreadList[index].is_alive() :
                                   del RunningThreadList[index]      
                           if len (RunningThreadList) > 0 :                              
                                 #print ("Thread Queue finished.  Waiting for threads to finish running ")                                   
                                 App.processEvents ()           
                                 threadwait.wait (SleepTime) 
                                   
                    result = []         
                    for value in threadPoolResults.values () :
                        if len (value) == 0 :
                            result.append (None)
                        else:
                            result += value
                    return result
                finally:
                    del threadPoolResults"""
            finally:
                if ProjectFileInterface is not None :
                   ProjectFileInterface.setMPEnabled (False)
    
    

class PlatformMP :            
    
    # file handles must implement 
    #------------------------------
    #setManualHandleCloseDisabled
    #setManualHandleCloseEnabled
    #copyForChildProcessCopyDisableFinalizer
    
    
    @staticmethod 
    def closeParameterHandles (listOfHandles) :
        for handle in listOfHandles :
            handle.setManualHandleCloseEnabled ()         
            
    @staticmethod
    def getHandlesAndParamsInParameters (parameters) :        
        handleList = []
        ProcessParametersWithHandleCloseDisabled = []        
        if isinstance (parameters, str) :
            return handleList, parameters
        from rilcontourlib.dataset.rcc_DatasetROIInterface import RCC_ROIDataset_FileSystemObject, RCC_ROIDataset_PASSCARAObject
        from rilcontourlib.util.rcc_NIfTIDataFileHandlers import  FileSystemDF, PesscaraDF
        handleList = []
        ProcessParametersWithHandleCloseDisabled = []        
        for item in parameters :
            if isinstance (item, RCC_ROIDataset_FileSystemObject) or isinstance (item, RCC_ROIDataset_PASSCARAObject) or isinstance (item, FileSystemDF) or isinstance (item, PesscaraDF) :
                ProcessParametersWithHandleCloseDisabled.append (item.copyForChildProcessCopyDisableFinalizer ())   
                handleList.append (item)
            else:
                if (isinstance (item, list)) or (isinstance (item, set)) or (isinstance (item, tuple)) :
                    hL, newLst = PlatformMP.getHandlesAndParamsInParameters (item)   
                    if (isinstance (item, set)) :
                        item = set (newLst)
                    elif (isinstance (item, tuple)) :
                        item = tuple (newLst)
                    elif (isinstance (item, list)) :
                        item = list (newLst)   
                    handleList += hL
                ProcessParametersWithHandleCloseDisabled.append (item)        
        return handleList, tuple (ProcessParametersWithHandleCloseDisabled)
    
    @staticmethod
    def getHandlesInParameters (parameters) :
        from rilcontourlib.dataset.rcc_DatasetROIInterface import RCC_ROIDataset_FileSystemObject, RCC_ROIDataset_PASSCARAObject
        from rilcontourlib.util.rcc_NIfTIDataFileHandlers import  FileSystemDF, PesscaraDF
        handleList = []
        for item in parameters :
            if isinstance (item, RCC_ROIDataset_FileSystemObject) or isinstance (item, RCC_ROIDataset_PASSCARAObject) or isinstance (item, FileSystemDF) or isinstance (item, PesscaraDF) :
                handleList.append (item)
            else:
                if (isinstance (item, list)) or (isinstance (item, set)) or (isinstance (item, tuple)) :
                    hL = PlatformMP.getHandlesInParameters (item)
                    handleList += hL
        return handleList


    @staticmethod 
    def isMPDisabled (EnableMultiProcessingOnAllOpperatingSystems = False) :        
        if EnableMultiProcessingOnAllOpperatingSystems :
            return False
        import platform
        return platform.system() == "Windows"
    
    @staticmethod
    def getAvailableCPU (NoUpperCPULimit = False) :                
        try :
            if NoUpperCPULimit :
                processCount = max (1, multiprocessing.cpu_count() - 1)
            else:
                MaxProcessCount = 10
                processCount = min (max (1, multiprocessing.cpu_count() - 1), MaxProcessCount)
        except :
            processCount = 1        
        return processCount
    
    _processMap_ProcessBased = staticmethod (_processMap_ProcessBased)
    
    
    @staticmethod 
    def _slowActiveSpinProcessRunningTimer (function, block, PoolSize, TimeProcess, ProcessTime, SingleThreadTime, QApp, ProgressDialog, ProgressDialogOffset, ProcessDialogTextList):
        if not TimeProcess :
            return PlatformMP._slowProcessBasedMap (function, block, PoolSize), ProcessTime, SingleThreadTime
        else:
            if (SingleThreadTime <= ProcessTime)  :
                SingleThreadTime = time.time ()
                results = PlatformMP._SeriesMap (function, block, QApp = QApp, ProgressDialog = ProgressDialog, ProgressDialogOffset = ProgressDialogOffset, ProcessDialogTextList=ProcessDialogTextList)
                SingleThreadTime = time.time () - SingleThreadTime
                return results, ProcessTime, SingleThreadTime
            else:
                ProcessTime = time.time ()
                results = PlatformMP._slowProcessBasedMap (function, block, PoolSize, QApp = QApp, ProgressDialog = ProgressDialog, ProgressDialogOffset = ProgressDialogOffset, ProcessDialogTextList=ProcessDialogTextList)
                ProcessTime = time.time () - ProcessTime
                return results, ProcessTime, SingleThreadTime
            
        
    @staticmethod 
    def _slowActiveSpinProcessBasedMap (function, argumentList, PoolSize, QApp = None, ProgressDialog = None, ProcessDialogTextList = None, ProcessName = None, ProcessTimingHintPath = None):
        block = []
        resultList = []
        TimeProcess = (len (argumentList) > 2 * PoolSize)
        if (ProcessTimingHintPath is None or ProcessName is None) :
            ProcessTime, SingleThreadTime = 0, 0
        else:
            try:
                timingHintFile = open (ProcessTimingHintPath,"rt")
                timeingHintDictionary = json.loads (timingHintFile.read ())
                timingHintFile.close ()
                if ProcessName in timeingHintDictionary :
                   ProcessTime, SingleThreadTime = timeingHintDictionary[ProcessName]
                else:
                   ProcessTime, SingleThreadTime = 0, 0
            except:
                ProcessTime, SingleThreadTime = 0, 0
            
        if (ProgressDialog is not None) :
            ProgressDialog.setValue (0)
        for item in argumentList :
            if (QApp is not None) :
                 QApp.processEvents() 
            if (ProgressDialog is not None and ProgressDialog.wasCanceled ()) :
                 break
            block.append (item) 
            if len (block) >= PoolSize :
                result, ProcessTime, SingleThreadTime = PlatformMP._slowActiveSpinProcessRunningTimer (function, block, PoolSize, TimeProcess, ProcessTime, SingleThreadTime, QApp, ProgressDialog, len (resultList), ProcessDialogTextList)
                resultList += result
                if (ProgressDialog is not None) :
                    ProgressDialog.setValue (len (resultList))
                block = []
                
        if (ProgressDialog is not None and ProgressDialog.wasCanceled ()) :
            return resultList
        
        if len (block) > 0 : 
            if (QApp is not None) :
                 QApp.processEvents() 
            result, _, _ = PlatformMP._slowActiveSpinProcessRunningTimer (function, block, PoolSize, TimeProcess, ProcessTime, SingleThreadTime,  QApp, ProgressDialog, len (resultList), ProcessDialogTextList)
            resultList += result
            if (ProgressDialog is not None) :
                 ProgressDialog.setValue (len (resultList))
        
        if (ProcessTimingHintPath is not None or ProcessName is not None) :
            try:
                try:
                    timingHintFile = open (ProcessTimingHintPath,"rt")
                    timeingHintDictionary = json.loads (timingHintFile.read ())
                    timingHintFile.close ()
                except:
                    timeingHintDictionary = {}
                timeingHintDictionary[ProcessName] = (ProcessTime, SingleThreadTime)
                with open (ProcessTimingHintPath,"wt") as timingHintFile :
                    timingHintFile.write (json.dumps (timeingHintDictionary))
            except:
                pass
        return resultList
        
    #written for simplicity, runs process in batches of PoolSize it would be faster to run continously as processes finish but adds complexity, and 
    #can be changed later.  
    @staticmethod 
    def _slowProcessBasedMap (function, argumentList, PoolSize, QApp = None, ProgressDialog = None, ProgressDialogOffset = 0, ProcessDialogTextList = None) :        
        resultIndex = 0
        argumentIndex = 0
        runningProcessList = []
        returnLst = []
        for index in range (len (argumentList)) :
            returnLst.append ("placeholder")
            
        while (argumentIndex < len (argumentList)) :
            if (QApp is not None) :
               QApp.processEvents() 
            if (ProgressDialog is not None) :                    
                if ProgressDialog.wasCanceled () :
                    break
            process_parent_conn, process_child_conn = multiprocessing.Pipe(False)
            proc = multiprocessing.Process (target = PlatformMP._processMap_ProcessBased, args = (function, argumentList[argumentIndex], process_child_conn))
            proc.start ()
            runningProcessList.append ((proc, process_parent_conn, process_child_conn, argumentIndex))
            argumentIndex += 1
            if (len (runningProcessList) >= PoolSize) :
                for proc_tpl in runningProcessList :
                    if (QApp is not None) :
                        QApp.processEvents() 
                    proc, parent_conn, child_conn, argumentIndex = proc_tpl
                    if parent_conn.poll () :
                        try :
                            proc_results = parent_conn.recv()
                            proc.join ()
                        except:
                            proc_results = None
                        del parent_conn
                        del child_conn
                        del proc
                        returnLst[argumentIndex] = proc_results
                        if (ProgressDialog is not None) :
                            ProgressDialog.setValue (resultIndex + ProgressDialogOffset)
                            resultIndex+=1
                            if ProcessDialogTextList is not None :
                                ProgressDialog.setLabelText (ProcessDialogTextList[argumentIndex])
                runningProcessList = [] 
        for proc_tpl in runningProcessList :
            if (QApp is not None) :
               QApp.processEvents() 
            proc, parent_conn, child_conn, argumentIndex = proc_tpl
            try :
                proc_results = parent_conn.recv()
                proc.join ()
            except:
                proc_results = None
            del parent_conn
            del child_conn
            del proc
            returnLst[argumentIndex] = proc_results
            if (ProgressDialog is not None) :
                ProgressDialog.setValue (resultIndex + ProgressDialogOffset)
                if ProcessDialogTextList is not None :
                    ProgressDialog.setLabelText (ProcessDialogTextList[argumentIndex])
                resultIndex+=1
        del runningProcessList        
        return returnLst
        
    
    @staticmethod
    def _SeriesMap (function, itemList, QApp = None, ProgressDialog = None, ProgressDialogOffset = 0, ProcessDialogTextList = None) :
        if (QApp is None and ProgressDialog is None) :
            return list (map (function, itemList))
        else:
            returnList = []
            for index, item in enumerate (itemList) :
                returnList.append (function (item))
                if (ProgressDialog is not None) :
                    ProgressDialog.setValue (index + ProgressDialogOffset)
                    if ProcessDialogTextList is not None :
                        ProgressDialog.setLabelText (ProcessDialogTextList[index + ProgressDialogOffset])
                    if ProgressDialog.wasCanceled () :
                        break
                if (QApp is not None) :
                   QApp.processEvents() 
            return returnList
        
    @staticmethod 
    #Set UseProcessBasedMap if Process Map may call futher process instances.
    def processMap (function, itemList, FileSaveThreadManager, PoolSize = None, UseProcessBasedMap = False, QApp = None, Debug = False, ProgressDialog = None, ProcessDialogTextList = None, ProcessName = None, ProcessTimingHintPath = None, ProjectFileInterface = None,EnableMultiProcessingOnAllOpperatingSystems = False, ParametersContainFileHandles = True, NoUpperCPULimit = False) :        
        if (ProcessDialogTextList is not None) :
            if len (ProcessDialogTextList) != len (itemList) :
                ProcessDialogTextList = None
        if len (itemList) < 1 :
            return []
        elif (len (itemList) == 1 or PlatformMP.isMPDisabled (EnableMultiProcessingOnAllOpperatingSystems = EnableMultiProcessingOnAllOpperatingSystems)) :
            return PlatformMP._SeriesMap (function, itemList, QApp = QApp, ProgressDialog = ProgressDialog, ProcessDialogTextList = ProcessDialogTextList)
        else:
            if (Debug):
                return PlatformMP._SeriesMap (function, itemList, QApp = QApp, ProgressDialog = ProgressDialog, ProcessDialogTextList = ProcessDialogTextList)
            
            processCount = PlatformMP.getAvailableCPU (NoUpperCPULimit = NoUpperCPULimit)
            if (PoolSize is None) :
                PoolSize = processCount
            else:
                PoolSize = min (max (PoolSize, 1), processCount)            
            PoolSize = min (PoolSize, len (itemList))   
            if (FileSaveThreadManager is not None) :
                FileSaveThreadManager.closeAndDeleteThread ()  
            try :
                if ProjectFileInterface is not None :
                   ProjectFileInterface.setMPEnabled (True)
                
                listOfProcessParams = []
                handleList = []
                for paramTupl in itemList :                    
                        if not ParametersContainFileHandles or isinstance (paramTupl, str):
                            listOfProcessParams.append (paramTupl)
                        else:
                            hl, processParameterTuple = PlatformMP.getHandlesAndParamsInParameters (paramTupl)
                            listOfProcessParams.append (processParameterTuple)
                            handleList += hl
                #oldhandleCount = []
                #for handle in handleList :
                #    oldhandleCount.append (handle.incrementHandleCount ())
                try :
                    if (QApp is not None or ProgressDialog is not None) :                
                        resultlist = PlatformMP._slowActiveSpinProcessBasedMap (function, listOfProcessParams, PoolSize, QApp = QApp, ProgressDialog = ProgressDialog, ProcessDialogTextList = ProcessDialogTextList, ProcessName = ProcessName, ProcessTimingHintPath = ProcessTimingHintPath)
                    elif (UseProcessBasedMap) :
                        resultlist = PlatformMP._slowProcessBasedMap (function, listOfProcessParams, PoolSize)
                    else:                                      
                        pool = multiprocessing.Pool (PoolSize)                
                        try :
                            resultlist = pool.map (function, listOfProcessParams)
                        finally :
                            pool.close ()
                finally:  
                    for handle in handleList :
                        handle.enableClose ()                      
                #    for handleindex, handle in handleList :
                #        handle.setHandleCount (oldhandleCount[handleindex])
                        
                del listOfProcessParams
                return resultlist
            finally:
                if ProjectFileInterface is not None :
                   ProjectFileInterface.setMPEnabled (False)
    
    class ProcessProgressDialog :
        def __init__ (self, manager) :
            self._state = manager.dict ()
            self._lock = manager.Lock ()
            self._state["wasCanceled"] = False
            self._state["labelText"] = ""
            self._state["value"] = 0
            self._state["maximum"] = 100
            self._state["minimum"] = 0
            self._state["StateChangeIndex"] = 0
            self._state["close"] = False
            self._state["ReadChangeIndex"]  = -1
        
        def _setState (self, key, value) :
            try:
                self._lock.acquire ()
                self._state[key] = value
                self._state["StateChangeIndex"] += 1
            finally:
                self._lock.release ()
            
        def _getState (self, key) :
            try:
                self._lock.acquire ()
                return self._state[key]
            finally:
                self._lock.release ()
                
        def isChanged (self, ParentWasCanceled = None) :
            try:
                self._lock.acquire ()
                isChanged = self._state["StateChangeIndex"] != self._state["ReadChangeIndex"] 
                if isChanged :
                    self._state["ReadChangeIndex"]  = self._state["StateChangeIndex"]
                elif ParentWasCanceled is not None and ParentWasCanceled and not self._state["wasCanceled"] :                    
                    isChanged =  True                    
                return isChanged
            finally:
                self._lock.release ()
                
        def getStateChangeIndex (self) :
            return self._getState ("StateChangeIndex")                               
                
        def labelText (self) :  
            return self._getState ("labelText")            
                
        def value (self):
            return self._getState ("value")                       
        
        def getPrecent (self) :
            try:
                self._lock.acquire ()
                val = self._state["value"]
                minV = self._state["minimum"]
                maxV = self._state["maximum"]
                minV, maxV = min (minV, maxV), max (minV, maxV)
                if minV == maxV :
                    return 0.0
                return (100.0 * float(val - minV)) / float (maxV - minV)
            finally:
                self._lock.release ()
                
        def maximum(self) :
            return self._getState ("maximum")                                         
                
        def miniumum (self) :
            return self._getState ("minimum")                                         
                 
        def wasCanceled (self) :
            return self._getState ("wasCanceled")                                                  
        
        def setWasCanceled (self) :
            self._setState ("wasCanceled", True)                                
                
        def setLabelText(self, label) :            
            self._setState ("labelText", label)                    
                
        def setMaximum (self, val) :
            self._setState ("maximum", val)                      
                
        def setMinimum (self, val) :
            self._setState ("minimum", val)          
                
        def setRange (self, minV, maxV) :
            try:
                self._lock.acquire ()
                self._state["minimum"] = min (minV, maxV)
                self._state["maximum"] = max (minV, maxV)
                self._state["StateChangeIndex"] += 1
            finally:
                self._lock.release ()
                
        def setValue (self, val) :            
            self._setState ("value", val)
                            
        def close (self) :
            self._setState ("close", True)
            
            
            
    class processQueue  :
        def __init__ (self, processFunction, mainThreadProcessCleanupFunction, PoolSize = None, SleepTime = 5, Debug = False, ProjectDataset = None, ProgressDialog = None, NoUpperCPULimit = False) :                        
            self._ParentProgressDialog = ProgressDialog
            self._ProjectDataset = ProjectDataset
           
            if self._ProjectDataset is not None :
                self._QApp = self._ProjectDataset.getApp ()
                if not Debug :
                    self._ProjectDataset.getProjectFileInterface ().setMPEnabled (True)
            else:
                self._QApp = None
            self._Debug = Debug
            self._SleepTime = max (SleepTime, 1)
            self._ProcessFunction = processFunction
            self._CleanupFunction = mainThreadProcessCleanupFunction
            processCount = PlatformMP.getAvailableCPU (NoUpperCPULimit = NoUpperCPULimit)
            if (PoolSize is None) :
                self._PoolSize = processCount
            else:                
                self._PoolSize = min (max (PoolSize, 1), processCount)     
            self._RunningProcs = []
            self._Results = []
            if self.isSerial () :
                self._progressDialogManager = None                
            elif ProjectDataset is None :
                self._progressDialogManager = multiprocessing.Manager ()
            else:
                self._progressDialogManager = ProjectDataset.getMultiprocessingManager ()
                
        def getMPProgressDialogInterface (self) :
            return PlatformMP.ProcessProgressDialog (self._progressDialogManager)
            
        def isSerial (self) :
            try:
                if PlatformMP.isMPDisabled () :
                    return True
                if self._Debug :
                    return True
                if self._PoolSize == 1 :
                    return True
                return False
            except:
                return False
            
        def _handleMPProgressDialog (self, mpProcessDialogInterface) :
            if self._ParentProgressDialog is not None and mpProcessDialogInterface is not None :
                if mpProcessDialogInterface.isChanged (self._ParentProgressDialog.wasCanceled ())  :
                    if self._ParentProgressDialog.wasCanceled () :
                        mpProcessDialogInterface.setWasCanceled ()
                    else:
                        self._ParentProgressDialog.setLabelText (mpProcessDialogInterface.labelText ())
                        parentValue = self._ParentProgressDialog.value () 
                        mpProcValue = mpProcessDialogInterface.value ()
                        if (mpProcValue > parentValue) :
                            self._ParentProgressDialog.setValue (mpProcValue)                 
                    if self._QApp is not None :
                        self._QApp.processEvents ()
                    
        def _waitForQueue (self, minsize) :
            cleanupIndexs = []
            while len (self._RunningProcs) >  minsize :
                if (self._QApp is not None) :
                    self._QApp.processEvents() 
                if self._SleepTime > 0 :
                    time.sleep (self._SleepTime)
                for procIndex in range (len (self._RunningProcs)) :
                    runningProc, cleanUpParams, resultIndex, parent_conn, child_conn, _, _, _, mpProcessDialogInterface = self._RunningProcs[procIndex]
                    self._handleMPProgressDialog (mpProcessDialogInterface)     
                    if parent_conn.poll () :
                        cleanupIndexs.append (procIndex)
                if len (cleanupIndexs) > 0 :
                    cleanupIndexs = sorted (cleanupIndexs)
                    cleanupIndexs.reverse ()
                    for index in cleanupIndexs :
                        runningProc, cleanUpParams, resultIndex, parent_conn, child_conn, _, _, handleList, _ = self._RunningProcs[index]            
                        try :
                            proc_results = parent_conn.recv()
                            runningProc.join ()
                        except Exception as exp:
                            print ("")
                            print ("Thread RecieveFailed")
                            print ("")
                            print (str (exp))
                            print ("")
                            proc_results = None
                        del parent_conn
                        del runningProc
                        self._Results[resultIndex] = proc_results
                        if self._CleanupFunction is not None :
                            self._CleanupFunction (cleanUpParams)
                        del cleanUpParams
                        del self._RunningProcs[index]
                        for handle in handleList :
                            handle.enableClose ()
                    cleanupIndexs = []
                
        def queueJob (self, FileSaveThreadManager, processParameters, cleanupParameters, Debug = None, MPProgressDialogInterface = None) :                        
            minsize = self._PoolSize - 1
            if Debug is None or self._Debug == True:
                Debug = self._Debug            
            if len (self._RunningProcs) >  minsize :
                self._waitForQueue (minsize)
            if PlatformMP.isMPDisabled () or Debug or  self._PoolSize == 1:
                self._Results.append (self._ProcessFunction (processParameters))
                if self._CleanupFunction is not None :
                    self._CleanupFunction (cleanupParameters)
            else:
                if MPProgressDialogInterface is not None and MPProgressDialogInterface.wasCanceled () :
                    return                
                handleList, processParameterTuple = PlatformMP.getHandlesAndParamsInParameters (processParameters)
                #for handle in handleList :
                #    handle.incrementHandleCount ()
                if (FileSaveThreadManager is not None) :
                    FileSaveThreadManager.closeAndDeleteThread ()        
                
                parent_conn, child_conn = multiprocessing.Pipe(False)
                 
                proc = multiprocessing.Process (target = PlatformMP._processMap_ProcessBased, args = (self._ProcessFunction, processParameterTuple, child_conn))
                proc.start ()
                self._Results.append (None)
                self._RunningProcs.append ((proc, cleanupParameters, len (self._Results) - 1, parent_conn, child_conn, processParameterTuple, processParameters, handleList, MPProgressDialogInterface))
                #self._Results.append (PlatformMP._processMap_ProcessBased (self._ProcessFunction, processParameterTuple, None))
                
               
                
        def join (self) :
            self._waitForQueue (0)
            if self._ProjectDataset is not None :
                self._QApp = None
                if not self._Debug :
                    self._ProjectDataset.getProjectFileInterface ().setMPEnabled (False)
            del self._ProjectDataset
            return self._Results 
        
        def close (self) :
            return self.join ()
                 
                
                
        
class FileSaveThread :
    def __init__ (self) :
        #print ("File Save Thread Initalized")
        self._fileSaveQueue = {}
        self._fileSaveAsycProcessCallbackQueue = {}
        self._fileQueueLock = Condition(RLock ())
        self._fileSaveLock =  RLock()
        self._StopThread = False 
        self._startWait = time.time()
        self._ThreadPaused = False
        self._RunningSavingThread = Thread(target=self.saveThread)
        self._RunningSavingThread.start ()
        
            
    
    def close (self) :
        self._blockingWaitForClose ()
        
    def getPendingFileSaveQueueLength (self) :
        self._fileQueueLock.acquire()
        fileSaveQueueLength = len (self._fileSaveQueue)
        self._fileQueueLock.release()      
        return fileSaveQueueLength
    
    def _waitForAllFileSaveAsycProcessCallbackToEnd (self) :
        for proc in self._fileSaveAsycProcessCallbackQueue.values () :
            try :
                if proc is not None :
                    proc.join ()    
            except:
                print ("A error occured while waiting for file async callback process to end.")
        self._fileSaveAsycProcessCallbackQueue = {}
    
   
            
            
    def _blockingWaitForClose (self) :
        #print ("")
        #print ("Trying to stop Thread")
        self.setStopThread ()
        self._RunningSavingThread.join ()
        self._waitForAllFileSaveAsycProcessCallbackToEnd ()   #un-nessary but wait..          
        #print ("File Saving Thread Closed") 
        
    def _isThreadStopping (self) :
        return self._StopThread
    
    def setStopThread (self) :
        self._fileQueueLock.acquire()
        self._StopThread = True
        self._ThreadPaused = False
        self._fileQueueLock.notify()
        self._fileQueueLock.release()     
        
    def blockingFileClose (self, path) :
        if path is None :
            return
        self._fileQueueLock.acquire()
        self._fileSaveLock.acquire()
        
        try :
            if (path in self._fileSaveAsycProcessCallbackQueue):  # if file async process is running wait for it to finish            
                if (self._fileSaveAsycProcessCallbackQueue[path] is not None) :
                    self._fileSaveAsycProcessCallbackQueue[path].join ()
                del self._fileSaveAsycProcessCallbackQueue[path]
        except :
            print ("blockingFileClose Error: A error occured waiting for previously running file asyc prcess callback to end")
            
        if path in self._fileSaveQueue :
            processID = os.getpid()
            saveText, creatingProcess, file_save_callback, LockFilePath, LockSignature, fileHandle = self._fileSaveQueue[path]
            del self._fileSaveQueue[path]
            if (creatingProcess == processID) :
                try:
                    if  NonPersistentFSLock.isFileLocked (LockFilePath, LockSignature) or fileHandle.getHandleType () == "Tactic" :
                        FileInterface.safeFileWrite (path, saveText, FileSavePermissions = fileHandle.getFilePermissions())                                                  
                        if (file_save_callback is not None) :
                            file_save_callback (path, ReturnRunningAsyncProcess = False)
                        print ("File Saving Done")
                except:
                   print ("")
                   print ("Error failed to save: %s" % path)
                   print ("") 
            del fileHandle
            del saveText
            del creatingProcess                
                        
        self._fileSaveLock.release()
        self._fileQueueLock.release()           
                       
    def queueFileSave (self, path, text, fileSaveCallback = None, LockFilePath = None, LockSignature = None, FileHandle=None) :                
        self._fileQueueLock.acquire()
        processID = os.getpid()
        if (self._isThreadStopping ()) :
            print ("Error Dictionary File Save Thread Shutting Down.  Cannot queue file save for: %s." % path)
        else:
            self._fileSaveQueue[path] = (text, processID, fileSaveCallback, LockFilePath, LockSignature, FileHandle)
        self._startWait = time.time ()        
        self._fileQueueLock.notify()
        self._fileQueueLock.release()        

    def pauseThread (self) :
        self._fileQueueLock.acquire()
        self._ThreadPaused = True
        print ("File Save Thread Paused")
        self._fileQueueLock.notify()
        self._fileQueueLock.release()
        
    def restartThread (self) :
        self._fileQueueLock.acquire()
        self._ThreadPaused = False
        self._startWait = time.time ()
        print ("File Save Thread Restarted")
        self._fileQueueLock.notify()
        self._fileQueueLock.release()
        
    def saveThread (self):
        #print ("File Saving Thread Started")
        Running = True
        try:
            while (Running) :
                self._fileQueueLock.acquire()   
                if (self._ThreadPaused) :
                    try:
                        self._fileQueueLock.wait (5) 
                    except:
                        pass
                    continue
                if len (self._fileSaveQueue) <= 0 :
                    if self._isThreadStopping ()  :
                        self._fileQueueLock.release() 
                        Running = False
                        continue
                    else:                           
                        try:
                            self._fileQueueLock.wait ()             
                        except:
                            pass
                        while (not self._isThreadStopping () and not self._ThreadPaused) :
                                deltaTime = int (time.time() - self._startWait)
                                sleepTime = max (5 - deltaTime, 0)
                                if (sleepTime > 0 and not self._isThreadStopping () and not self._ThreadPaused) :
                                    try:
                                        self._fileQueueLock.wait (sleepTime) 
                                    except:
                                        pass
                                else:
                                    break
                if (self._ThreadPaused) :
                    continue            
                
                foundPath = False 
                for path, saveTupl in self._fileSaveQueue.items () :
                       foundPath = True
                       break
                if (foundPath) :  
                    del self._fileSaveQueue[path]
                    self._fileSaveLock.acquire()
                self._fileQueueLock.release()   
                if (foundPath) :                      
                    try :
                       if (path in self._fileSaveAsycProcessCallbackQueue):  # if file async process is running wait for it to finish            
                          if (self._fileSaveAsycProcessCallbackQueue[path] is not None) :
                              self._fileSaveAsycProcessCallbackQueue[path].join ()                          
                    except :
                       print ("saveThread Error: A error occured waiting for previously running file asyc prcess callback to end")
        
                    saveText, creatingProcess, file_save_callback, LockFilePath, LockSignature, fileHandle = saveTupl
                    processID = os.getpid()
                    if (creatingProcess == processID) :                        
                        try :                          
                            if (fileHandle.getHandleType () == "Tactic") or NonPersistentFSLock.isFileLocked (LockFilePath, LockSignature) :
                                FileInterface.safeFileWrite (path, saveText, FileSavePermissions = fileHandle.getFilePermissions())
                                if (file_save_callback is not None) :
                                    proc = file_save_callback (path, ReturnRunningAsyncProcess = True)
                                    if proc is not None :
                                        self._fileSaveAsycProcessCallbackQueue[path] = proc                                
                                print ("File Saving Done")
                            else:
                                print ("")
                                print ("Cannot save file.  File lock Path and signature do not match")
                                print (LockFilePath)
                                print (LockSignature)
                                if not os.path.exists (LockFilePath) :
                                    print ("Error Lock file path does not exist")
                                else:
                                    print ("Lock file path exists")
                                    print ("")                                    
                                    print ("Lock file contains")
                                    print ("")
                                    linelist = NonPersistentFSLock.getLockFile (LockFilePath, ReturnLines = True)
                                    if linelist is not None :
                                        for index, line in enumerate (linelist) :
                                            print ("Line %d: %s" % (index, str (line)))
                                    print ("")
                                print ("")
                        except:
                            print ("")
                            print ("Error failed to save: %s" % path)
                            print ("")
                    del fileHandle
                    del saveText
                    del creatingProcess
                    del saveTupl
                    self._fileSaveLock.release()        
            self._waitForAllFileSaveAsycProcessCallbackToEnd ()             
        finally:
            print ("File Saving Thread Done!!!!")          


    
        
class FileInterface:   
   def __init__ (self) :       
       self._FileObjectHeader = FileObject ("FileHeader")       
       self._FileObjects = []                     
           
   def getFileHeader (self):
       return self._FileObjectHeader

   def addFileObject (self, obj):
       return self._FileObjects.append (obj)
   
   def addFileObjects (self, obj):
       return self._FileObjects.append (obj)
   
   def hasFileObject (self, name)  :
       for fobj in self._FileObjects :
           if fobj.getFileObjectName () == name :
               return True
       return False            
            
   def getFileObject (self, name) :
       for fobj in self._FileObjects :
           if fobj.getFileObjectName () == name :
               return fobj
       return None        
    
   def getFileObjects (self) :
       return self._FileObjects
      
   @staticmethod
   def safeFileWrite (path, writeString, FileSavePermissions = None) :       
       """dirname, fname = os.path.split (path)
       fnamelst = fname.split(".")
       suffix = "." +  fnamelst[len (fnamelst) -1]
       del fnamelst[len (fnamelst) -1]
       prefix = ".".join (fnamelst)
       prefix = "TMP_" + prefix
       try :
           handle, temp_path = tempfile.mkstemp (dir = dirname, prefix= prefix, suffix =suffix,  text = True)                  
           tempfilecreated = True
       except:
           print ("")
           print ("Warrning: Failure to create temporary file in directory: " + dirname)
           print ("          Verrify that you have file permissions to create files within the directory.")
           print ("          Reverting old file saving mechanism.")
           print ("")
           tempfilecreated = False
       if tempfilecreated :           
           os.close(handle)                                    
           file = open (temp_path, "wt")
           file.write (writeString)
           file.close ()
           shutil.copyfile (temp_path, path)           
           os.remove (temp_path)    
       else:"""
       datafile = None
       try :
           datafile = open (path, "wt")
           datafile.write (writeString)
           datafile.flush ()
           os.fsync(datafile.fileno())
       finally :
           if datafile is not None :
               datafile.close ()
       FileUtil.setFilePermissions (path, FileSavePermissions)        
       
   @staticmethod 
   def doesFileHaveROIorScanPhase_ReturnHeader (path, QApp = None):              
       _FileObjectHeader = FileObject ("FileHeader")      
       if ( not os.path.isfile(path)):
           return False, None
       try :
           with open (path, "rt") as f :
               _FileObjectHeader.readFileObject (f, QApp = QApp)       
               _, count = json.loads (f.readline ())  
           return count > 0, _FileObjectHeader    
       except :
           return False, None
         
  
   def toString (self, QApp = None) :
       if (_usecStringIO) :
           output = cStringIO.StringIO()
       else:           
           output = StringIO()  
       
       try :            
           self._FileObjectHeader.writeFileObject (output, "")
           output.write (json.dumps (("FileObjectCount", len (self._FileObjects))))
           output.write ("\n")
           for obj in self._FileObjects :
               obj.writeFileObject (output, "\t")                
       except :
           print ("Error occured trying to seralize objects to a string")
           return None
       writeString = output.getvalue ()
       output.close ()
       return writeString
   
   def writeDataFile (self, path, OldText = None, QApp = None, FileSavePermissions = None):
       if FileSavePermissions is None :
           FileSavePermissions = FileUtil.getFilePermissions (path)
           
       dataWritten = False
       writeString = self.toString (QApp = QApp)
       if writeString is None :
           return (False, None)
        
       if path is None :
           return True, writeString, dataWritten
       
       if (OldText is None or len (writeString) != len (OldText) or (writeString != OldText)) :                     
           FileInterface.safeFileWrite (path, writeString, FileSavePermissions = FileSavePermissions)           
           dataWritten = True
       return True, writeString, dataWritten
     
   def readStringFile (self, buffer, QApp = None, RaiseError = False):
       if (_usecStringIO) :
           f = cStringIO.StringIO(buffer)
       else:           
           f = StringIO(buffer)  
       
       self._FileObjectHeader.readFileObject (f, QApp = QApp)
       firstline = f.readline ()
       _, count = json.loads (firstline)
       self._FileObjects = []
       for i in range (count) :
           obj = FileObject ("Unknown")
           try:
               obj.readFileObject (f, QApp = QApp)
               self._FileObjects.append (obj)
           except:
               print ("A error occured reading from string buffer")
               if RaiseError :
                   raise
       f.close ()
       
       
   def readDataFile (self, path, QApp = None, FileContent = None):
       #start = time.time ()
       try:
           if FileContent is None :
               with open (path, "rt") as f :
                   FileContent = f.read ()
           if FileContent is not None :
               self.readStringFile (FileContent, QApp = QApp, RaiseError = True)
       except:
           print ("A error occured reading file: %s" % (path))
       #print ("File Read Time: " + str (time.time () - start))




class RCC_ROIVolumeCSVExportDlg (QDialog) :
    
    def __init__ (self, parent, ROIDefs) :
        self._result = "Cancel"
        self._txtChanging = False
        QDialog.__init__ (self, None)
        self._roiDef = ROIDefs
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)         
        self.ui = Ui_rcc_ROIVolumeMaskExportDlg ()        
        self.ui.setupUi (self)    
       
        self._resizeWidgetHelper = ResizeWidgetHelper ()        
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.line_2)
           
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.PointROILbl)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.InSliceLbl)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.CrossSliceLbl)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.InSliceTxt)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.CrossSliceTxt)
        
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.InSliceSlider)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.CrossSliceSlider)
        
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.ROIObjectExportLbl)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.ROIObjectList)
        
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.CancelBtn)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.ExportBtn)
        self._resizeWidgetHelper.saveWidgetUIPos (self, self.ui.combineNonTouchingROI)        

        self.ui.ExportBtn.setEnabled (False)
        self.ui.InSliceTxt.setText ('9')        
        self.ui.CrossSliceTxt.setText ('1')
        self.ui.InSliceSlider.setValue (9)
        self.ui.CrossSliceSlider.setValue (1)
        
        self.ui.InSliceTxt.editingFinished.connect    (self.InSliceTxtChange)
        self.ui.CrossSliceTxt.editingFinished.connect (self.CrossSliceTxtChange)             
        self.ui.InSliceSlider.valueChanged.connect (self.InSliceSliderChange)        
        self.ui.CrossSliceSlider.valueChanged.connect (self.CrossSliceSliderChange)                          
        self.setPointSettingsEnabled (False)                
        RCC_ROIVolumeCSVExportDlg._setListUI (self.ui.ROIObjectList, self._roiDef , True) 
        self.ui.ROIObjectList.selectionModel().selectionChanged.connect(self.ListSelectionChanged)        
                
        self.ui.CancelBtn.clicked.connect (self.Cancel)
        self.ui.ExportBtn.clicked.connect (self.Export)

        self.canExport ()  
        self._SelectedROILst = []
        self._pointInSliceSize = int (self.ui.InSliceSlider.value ())
        self._pointCrossSliceSize = int (self.ui.CrossSliceSlider.value ())
        self.ui.PointROILbl.setText ("Point ROI object size (%d voxels): " % (self._pointInSliceSize * self._pointCrossSliceSize))
    
    @staticmethod
    def _setListUI (lst, roiDefs, hideundefined):
        lst.clear ()
        if (not hideundefined) :
                item = QListWidgetItem()     
                item.setText ("None")                
                item.setForeground (QtGui.QColor (0,0,0))
                lst.addItem (item)    
        namelist =  roiDefs.getNameLst ()        
        for name in namelist: 
                item = QListWidgetItem() 
                item.setText (name) 
                color  = roiDefs.getROIColor (name)
                item.setForeground (color)
                lst.addItem (item)      
                
    def canExport (self):    
        exportTest = (len (self.ui.ROIObjectList.selectedItems ()) > 0)
        self.ui.ExportBtn.setEnabled (exportTest)
        return (exportTest)                
    
        
    def InSliceSliderChange (self):
        self._pointInSliceSize = int (self.ui.InSliceSlider.value ())
        self._pointCrossSliceSize = int (self.ui.CrossSliceSlider.value ())
        self.ui.PointROILbl.setText ("Point ROI object size (%d voxels): " % (self._pointInSliceSize * self._pointCrossSliceSize))
        if (not self._txtChanging) :
            self.ui.InSliceTxt.setText ( str (self.ui.InSliceSlider.value ()))
            
    def CrossSliceSliderChange (self):
        self._pointInSliceSize = int (self.ui.InSliceSlider.value ())
        self._pointCrossSliceSize = int (self.ui.CrossSliceSlider.value ())
        self.ui.PointROILbl.setText ("Point ROI object size (%d voxels): " % (self._pointInSliceSize * self._pointCrossSliceSize))
        if (not self._txtChanging) :
            self.ui.CrossSliceTxt.setText ( str (self.ui.CrossSliceSlider.value ()))
        
    def InSliceTxtChange (self) :
        if (self._txtChanging) :
            return
            
        self._txtChanging = True
        try :
            num = int (self.ui.InSliceTxt.text ())
            if num < 1 :     
                num = 1                
            if num > 99 :
                num = 99
            self.ui.InSliceTxt.setText (str (num))
            self.ui.InSliceSlider.setValue (num)
        except:
            val =  self.ui.InSliceSlider.value ()
            self.ui.InSliceTxt.setText (str (val))
        self._txtChanging = False
        
    def CrossSliceTxtChange (self) :        
        if (self._txtChanging) :
            return            
        self._txtChanging = True
        try :
            num = int (self.ui.CrossSliceTxt.text ())
            if num < 1 :     
                num = 1                
            if num > 99 :
                num = 99
            self.ui.CrossSliceTxt.setText (str (num))
            self.ui.CrossSliceSlider.setValue (num)
        except:
            val =  self.ui.CrossSliceSlider.value ()
            self.ui.CrossSliceTxt.setText (str (val))
        self._txtChanging = False
            
    def setPointSettingsEnabled (self, enable):
        self.ui.PointROILbl.setEnabled (enable)
        self.ui.InSliceLbl.setEnabled (enable)
        self.ui.CrossSliceLbl.setEnabled (enable)
        self.ui.InSliceTxt.setEnabled (enable)
        self.ui.CrossSliceTxt.setEnabled (enable)
        self.ui.InSliceSlider.setEnabled (enable)
        self.ui.CrossSliceSlider.setEnabled (enable)
        

    
    def _internalWindowResize (self, newsize):                            
        if (self._resizeWidgetHelper is not None) :                                                           
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.line_2, newsize)                    
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.InSliceSlider, newsize)            
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.CrossSliceSlider, newsize)                    
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.ROIObjectList, newsize)            
            self._resizeWidgetHelper.setAbsoluteHeight  (self.ui.ROIObjectList, newsize)                    
            self._resizeWidgetHelper.setWidgetYPos     (self.ui.CancelBtn, self.height () - 30)            
            self._resizeWidgetHelper.setWidgetYPos     (self.ui.ExportBtn, self.height () - 30)            
            self._resizeWidgetHelper.setWidgetXPos     (self.ui.CancelBtn, self.width () - 189)            
            self._resizeWidgetHelper.setWidgetXPos     (self.ui.ExportBtn, self.width () - 99)            
            
            self._resizeWidgetHelper.setAbsoluteWidth  (self.ui.combineNonTouchingROI, newsize)            
            self._resizeWidgetHelper.setWidgetYPos     (self.ui.combineNonTouchingROI, self.height () - 60)                        
                        
    def resizeEvent (self, qresizeEvent) :                       
        QDialog.resizeEvent (self, qresizeEvent)         
        self._internalWindowResize ( qresizeEvent.size () ) 
        
    def ListSelectionChanged (self) : 
        items = self.ui.ROIObjectList.selectedItems ()        
        pointSelected = False
        self._SelectedROILst = []
        for item in items :
            if (self._roiDef.isROIPoint (item.text ())):
                pointSelected = True        
            self._SelectedROILst.append (item.text ())
        self.setPointSettingsEnabled (pointSelected)                
        
        self.canExport ()
        
    def Cancel (self) :
        self.close ()
        
    def Export (self) :
        self._result = "Export"
        self._combineNonTochingROIwithTheSameName = self.ui.combineNonTouchingROI.isChecked ()
        self.canExport () # Ensure Export variables 
        self.close ()
        
        
class AreaObjStatisticsHelper :
    
    @staticmethod    
    def getDerivedDiameter (Area):
        return 2.0 * float (math.sqrt (Area / math.pi))
    
    @staticmethod    
    def _getLongestDist (xVec, yVec):        
        maxDist = 0
        saveDistance = None
        saveIndex = 0
        for index in range (len (xVec)):
            dX = xVec - xVec[index]
            dY = yVec - yVec[index]
            distanceVec = dX * dX + dY * dY
            tempMaxDist = np.max (distanceVec)
            if (tempMaxDist >= maxDist) :
                maxDist = tempMaxDist
                saveDistance = distanceVec
                saveIndex = index
        
        indexOfOtherPt = (saveDistance == maxDist).nonzero ()[0][0]
        return (xVec[saveIndex], yVec[saveIndex]), (xVec[indexOfOtherPt], yVec[indexOfOtherPt])
     
    
    @staticmethod 
    def _getAreaObjStatsHelper (NiftiVolume, maskedVolume, sliceList) :
        largestSliceNumber = -1
        largestSliceVoxelCount = -1
        maxObjectInSliceVoxelDistance = -1
        #get largest slice voxel count.  Voxels may not be touching. so raw voxel count is not 
        #accurate by itself.  To accunt for this objects need to be labeled and counted. 
        #also voxel distance or length count requires accounting for varriation in voxel dimensions 
        connectivity = np.ones ((3,3), dtype = np.uint8)         
        for slicenumber in sliceList :                                 
            sliceVoxelCount = maskedVolume[:,:,slicenumber]
            if (sliceVoxelCount is not None) :
                labeledObjects, numObjects = ndimage.measurements.label (sliceVoxelCount, connectivity)                
                for objectIndex in range (1, numObjects+1) :
                    labeledVoxels = (labeledObjects == objectIndex).astype (np.uint8)
                    if np.any (labeledVoxels) :                         
                         sliceVoxelCount = np.sum (labeledVoxels)
                         if (sliceVoxelCount > 0) :                             
                             if largestSliceVoxelCount < sliceVoxelCount :
                                 largestSliceVoxelCount = sliceVoxelCount
                                 largestSliceNumber = slicenumber        

                                 insideVoxels = ndimage.binary_erosion(labeledVoxels).astype(labeledVoxels.dtype)
                                 labeledVoxels = labeledVoxels - insideVoxels
                                 xVox, yVox = labeledVoxels.nonzero () # only perimeter voxels left.
                                 
                                 x, y, _ = NiftiVolume.getVoxelSize ()                                                                                                                                   
                                 #pairVPair = np.dstack ((xVox, yVox))[0]                                 
                                 #distanceAry = pdist(pairVPair, 'euclidean')                                                     
                                 if (len (xVox) == 1) :
                                     maxObjectInSliceVoxelDistance = max (x,y)                                                         
                                 else:                                                         
                                     xVox = xVox.astype (np.float) * float(x)
                                     yVox = yVox.astype (np.float) * float(y)    
                                     P1, P2 = AreaObjStatisticsHelper._getLongestDist (xVox, yVox)
                                     
                                     #distanceAry = squareform(distanceAry)                                                     
                                     #xPt, yPt = (distanceAry == np.max (distanceAry)).nonzero ()
                                     #P1 = pairVPair[xPt[0]]
                                     #P2 = pairVPair[yPt[0]]
                                     dx = math.fabs (P1[0] - P2[0])
                                     dy = math.fabs (P1[1] - P2[1])  
                                     if (dx > 0) :
                                         dx += x
                                     if (dy > 0) :
                                         dy += y                                                                                                                                                                           
                                     maxObjectInSliceVoxelDistance = math.sqrt (dx*dx + dy *dy )
                                     maxObjectInSliceVoxelDistance = max (maxObjectInSliceVoxelDistance,max (x,y))                
        connectivity = np.ones ((3,3,3), dtype = np.uint8) 
        _, numNonTouchingObjects = ndimage.measurements.label (maskedVolume, connectivity)                        
        return (largestSliceNumber, largestSliceVoxelCount, maxObjectInSliceVoxelDistance, numNonTouchingObjects)          
    
    @staticmethod        
    def getAreaObjStatsHelperFromMask (NiftiVolume, exportMask) :
        sliceList = []
        for sI in range (exportMask.shape[2]) :
            if np.any (exportMask[:,:,sI]) :
                sliceList.append (sI)
        return AreaObjStatisticsHelper._getAreaObjStatsHelper (NiftiVolume, exportMask, sliceList)
        
    @staticmethod    
    def getAreaObjStatsHelper (NiftiVolume, obj, sliceList = None, ROIDefs = None):
        if (sliceList is None):
            sliceList =  obj.getSliceNumberList ()
        exportMask = obj.getMaskedVolume (NiftiVolume.getSliceDimensions (),  ClipOverlayedObjects = True)         
        return AreaObjStatisticsHelper._getAreaObjStatsHelper (NiftiVolume, exportMask, sliceList)
                                        
                        
    
class ExportVolumeStatsCSV :    
             
    @staticmethod
    def _getROIStats (NiftiVolume, exportMask):    
        voxels = NiftiVolume.getImageData()[exportMask > 0]         
        voxelCount = 0
        median = ""
        mean = ""
        std = ""
        if (len (voxels) > 0):                 
            voxelCount = voxels.shape[0]                  
            median = str (int (np.median  (voxels)))
            mean = str (float (np.mean (voxels)))
        if (len (voxels) > 1):                 
            std = str (float (math.sqrt(np.var  (voxels,ddof = 1))))
        return (voxelCount, median, mean, std)    
    
    @staticmethod    
    def _getSliceCount (mem) :
        count = 0
        for si in range(mem.shape[2]) :
            if np.any(mem[:,:,si]) :
                count += 1
        return count
    
    @staticmethod    
    def _getUniqueName (namelist, name) :
        testname = name 
        counter = 1
        while True :            
            if testname not in namelist  :
                return testname 
            counter += 1
            testname = name + "_%d" % counter
    
    @staticmethod    #ROIPointCountLst, SlicesContouredLst, exportMask, maskIndexNameLst
    def _getROIMask (NiftiVolume, roiDictionary, ROIName, SamplePointsInSlice, SamplePointsCrossSlice, CombineNonTouchingROI) :
        obj = roiDictionary.getROIObject (ROIName)
        if obj.isROIArea () :            
            if (CombineNonTouchingROI) :
                exportMask = obj.getMaskedVolume (NiftiVolume.getSliceDimensions (),  ClipOverlayedObjects = True)                        
                sliceCount =  len (obj.getSliceNumberList ())
                return ([], [sliceCount], exportMask, "CombinedVolume")
            else:
                wholeObjectExportMask = obj.getMaskedVolume (NiftiVolume.getSliceDimensions (),  ClipOverlayedObjects = True, ClipToBoundBox = True)                                        
                wholeObjectExportMask = wholeObjectExportMask.astype (np.int)
                contourIdmanager = obj.getContourIDManger ()
                cIDList = contourIdmanager.getAllocatedIDList ()
                connectivity = np.ones ((3,3,3), dtype = np.uint8) 
                shape = NiftiVolume.getSliceDimensions ()
                vMem = None
                objectCounter = 0
                contourNameDictionary = {}
                extractedVolumeMem = np.zeros ((shape), dtype=np.uint8)
                firstPass = True
                for cID in cIDList  :                    
                    memset, vMem = obj.getContourMapVolume (cID, shape, vMem)
                    if memset :                        
                        if not firstPass :
                            extractedVolumeMem[...] = 0
                        firstPass = False
                        vMemIndex = vMem > 0
                        extractedVolumeMem[vMemIndex] = wholeObjectExportMask[vMemIndex]                
                        mem, numNonTouchingObjects = ndimage.measurements.label (extractedVolumeMem, connectivity)                        
                        for mI in range (1, numNonTouchingObjects + 1) :
                           objectCounter += 1
                           wholeObjectExportMask [mem == mI] = objectCounter
                           contourNameDictionary[objectCounter] = contourIdmanager.getContourName (cID)
                sliceList = []
                sliceNameList = []
                for objectCounter in contourNameDictionary.keys () :
                    sliceCount =  ExportVolumeStatsCSV._getSliceCount (wholeObjectExportMask == objectCounter)
                    if (sliceCount > 0) :
                        sliceList.append (sliceCount)
                        sliceNameList.append (ExportVolumeStatsCSV._getUniqueName (sliceNameList, contourNameDictionary[objectCounter]))                
                return ([], sliceList, wholeObjectExportMask, sliceNameList)                    
        else:
            if (CombineNonTouchingROI) :               
               exportMask = obj.getMaskedVolume (NiftiVolume.getSliceDimensions (), SamplePointsInSlice, SamplePointsCrossSlice)               
               pointcount = obj.getPointCount ()                
               return ([pointcount], [], exportMask, "CombinedVolume")
            else:
               exportMask = obj.getMaskedVolume (NiftiVolume.getSliceDimensions (), SamplePointsInSlice, SamplePointsCrossSlice, UniqueLableForPoints = True)               
               pointcount = obj.getPointCount ()                
               contourIdmanager = obj.getContourIDManger ()
               cIDList = contourIdmanager.getAllocatedIDList ()                              
               pointList = []
               pointNameList = []
               for index in range (1, pointcount + 1) :
                   mask = exportMask == index
                   if np.any (mask):
                       pointList.append (1)                                   
                       pointName = contourIdmanager.getContourName (cIDList[index-1])
                       pointNameList.append (ExportVolumeStatsCSV._getUniqueName (pointNameList, pointName))                                       
               return (pointList, [], exportMask, pointNameList)
            
    
    @staticmethod
    def _getROIExportData (NiftiVolume, roiDictionary, ROIName, SamplePointsInSlice, SamplePointsCrossSlice, CombineNonTouchingROI, forceROIDefs = None) :
        ROIDataExportData = {}        
        roiDefs = roiDictionary.getROIDefs ()
        ROIType = roiDefs.getROIType (ROIName)        
        RadlexID = roiDefs.getROINameRadlexID (ROIName)        
        ROITypeStr =  ROIType                
                            
        ROIDataExportData["CombineNonTouchingROI_Flag"] = CombineNonTouchingROI        
        ROIDataExportData["RadlexID"] = RadlexID
        ROIDataExportData["ROIName"] = ROIName
        ROIDataExportData["ROI Type (Point/Area)"] = ROITypeStr                
        ROIDataExportData["CombineNonTouchingROI"] = None
        if (CombineNonTouchingROI):
            ROIDataExportData["ContourLst"] = None
        else:
            ROIDataExportData["ContourLst"] = []    
            
        if (not roiDictionary.isROIDefined (ROIName)):
            return ROIDataExportData                
        obj = roiDictionary.getROIObject (ROIName)        
        if (forceROIDefs is not None) : # if poject and dataset  types don't match don't export data.
            projectROIType = forceROIDefs.getROIType (ROIName)
            if (projectROIType != ROIType) :
                ROIDataExportData["ROIType"] = "Error Project (%s) and Dataset (%s) ROI Types do not match." % (projectROIType, ROIType)
                return ROIDataExportData
                        
        ROIPointCountLst, SlicesContouredLst, exportMask, maskIndexNameLst = ExportVolumeStatsCSV._getROIMask (NiftiVolume, roiDictionary, ROIName, SamplePointsInSlice, SamplePointsCrossSlice, CombineNonTouchingROI)        
        maxValue = np.max (exportMask)         
                        
        for exportMaskIndex in range (1, maxValue + 1) :      
            exportMaskSelection = exportMask == exportMaskIndex
            voxelCount, median, mean, std = ExportVolumeStatsCSV._getROIStats (NiftiVolume, exportMaskSelection)
            if (voxelCount <= 0) :
                continue
            
            xDim, yDim, zDim = NiftiVolume.getVoxelSize ()
            xDim = float(xDim)
            yDim = float(yDim)
            zDim = float(zDim)
            voxelUnits = NiftiVolume.getVoxelUnits ()
            if voxelUnits == "cm" :
                xDim *= 10.0
                yDim *= 10.0
                zDim *= 10.0
            elif voxelUnits != "mm" :
                print ("")
                print ("Unexpected voxel units: " + str (voxelUnits))
                print ("")
            volume = float (voxelCount) * float (xDim * yDim *zDim) 
            
            if (ROIType == "Area") :                                                       
                #get largest slice index                                               
                largestSliceNumber, largestSliceVoxelCount, maxObjectInSliceVoxelDistance, NumberofNonTouchingObj = AreaObjStatisticsHelper.getAreaObjStatsHelperFromMask (NiftiVolume, exportMaskSelection)                                
                if (largestSliceVoxelCount <= 0) :
                    largestSliceArea = 0
                    largestSliceVoxelCount = 0
                    largestSliceDiameter = 0
                    largestSliceNumber = -1
                    maxObjectInSliceVoxelDistance = 0
                else:
                    try :
                        largestSliceArea = float (xDim * yDim) * float (largestSliceVoxelCount)
                        largestSliceDiameter = AreaObjStatisticsHelper.getDerivedDiameter (largestSliceArea) 
                    except:
                        print ("A error occured computing the slice diameter")
                        print (("Inputs", xDim, yDim, largestSliceVoxelCount))
                        largestSliceArea = 0
                        largestSliceVoxelCount = 0
                        largestSliceDiameter = 0
                        largestSliceNumber = -1
                        maxObjectInSliceVoxelDistance = 0
                                                
                ContourData = {}
                ContourData["Slices Contoured (#)"] = int (SlicesContouredLst[exportMaskIndex-1])
                ContourData["Voxel Count (#)"] = int (voxelCount)
                ContourData["ROI Contour Object (#)"] = int (NumberofNonTouchingObj)
                ContourData["Median Voxel Value"] = median
                ContourData["Mean Voxel Value"] = mean
                ContourData["Std Voxel Value"] = std
                ContourData["Volume Contoured (mm^3)"] = float (volume)
                ContourData["Largest Slice Index (#)"] = largestSliceNumber
                ContourData["Largest Slice Voxel Count (#)"] = largestSliceVoxelCount
                ContourData["Largest Slice Area (mm^2)"] = "%0.4f" % largestSliceArea
                ContourData["Largest Slice Derived Diameter (mm)"] = "%0.4f" % largestSliceDiameter
                ContourData["Greatest Distance in Object (mm)"] = "%0.4f" % maxObjectInSliceVoxelDistance
                
                if (CombineNonTouchingROI) :                
                    ROIDataExportData["CombineNonTouchingROI"] = ContourData                    
                else:                                                                                        
                    ContourData["Contour Name"] = maskIndexNameLst[exportMaskIndex-1]                    
                    ROIDataExportData["ContourLst"].append (ContourData)               
            elif (ROIType == "Point") :        
                PointData = {}
                PointData["Points Contoured (#)"] = int (ROIPointCountLst[exportMaskIndex-1])
                PointData["Point Size (Voxels)"] = int (SamplePointsInSlice * SamplePointsCrossSlice)
                PointData["InSlice Point Size (Voxels)"] = int (SamplePointsInSlice)
                PointData["Cross Slice Point Size (Voxels)"] = int (SamplePointsCrossSlice)
                PointData["Voxel Count (#)"] =  int (voxelCount)
                PointData["Median Voxel Value"] =  median
                PointData["Mean Voxel Value"] =  mean
                PointData["Std Voxel Value"] =  std
                PointData["Volume Contoured (mm^3)"] =  float (volume)
                PointData["Coordinates of Points Exported (Voxel)"] = obj.getPointCoordinatesAsTxt (roiDictionary.getNIfTIVolumeOrientationDescription ())
                        
                #csvFile.write ("ROIName, ROI Type (Point/Area), Points Contoured (#), In Point Size (Voxels), Cross Plane Point Size (Voxels), Voxel Count (#), Median Voxel Value, Mean Voxel Value, Std Voxel Value, Volume Contoured (mm^3), Point coordinates,")                                    
                if (CombineNonTouchingROI) :         
                    ROIDataExportData["CombineNonTouchingROI"] = PointData                    
                else:                    
                    PointData["Point Name"] = maskIndexNameLst[exportMaskIndex-1]
                    ROIDataExportData["ContourLst"].append (PointData)               
                    
            if (not CombineNonTouchingROI) :         
                if (ROIType == "Area") :                                                       
                    ROIDataExportData["Non-Touching Contours (#)"] = len (ROIDataExportData["ContourLst"])
                else:
                    ROIDataExportData["Total Points (#)"] = len (ROIDataExportData["ContourLst"])
                        
        return ROIDataExportData   
        
    @staticmethod
    #writeExportVolumeCSVData
    def getDatasetExportData (DatasetDescriptionList, NiftiVolume, roiDictionary, exportROIList, pointInSlice, pointCrossSlice, CombineNonTouchingROI, forceROIDefs = None) :                                                    
        DatasetExportData = {}        
        volumeVoxelSize = NiftiVolume.getVoxelSize ()        # transformed cordinates
        volumeVoxelDim  = NiftiVolume.getSliceDimensions ()                
        _, volumeVoxelSize  = NiftiVolume.transformPoint ( NiftiVolume.getContourOrientation () , NiftiVolume.getNativeVolumeOrientation (), (0,0,0), volumeVoxelSize)
        _, volumeVoxelDim   = NiftiVolume.transformPoint ( NiftiVolume.getContourOrientation () , NiftiVolume.getNativeVolumeOrientation (), (0,0,0), volumeVoxelDim)
        xDim, yDim, zDim = volumeVoxelSize
        xVoxels, yVoxels, zVoxels = volumeVoxelDim    
        ROIdescription = roiDictionary.getScanPhaseTxt ()
        ROIDesciptionIndex = -1
        if (ROIdescription in DatasetDescriptionList) :
            ROIDesciptionIndex = DatasetDescriptionList.index(ROIdescription)                
        PatientName = "Unknown"
        ExamName = "Unknown"
        SeriesName = "Unknown"
        TreePath ="Unknown"
        DataSourceDescription = roiDictionary.getNiftiDataSourceDescription ()
        if (DataSourceDescription is not None) :
            if DataSourceDescription.hasParameter ("roitree_path") :        
                roiPath = DataSourceDescription.getParameter ("roitree_path")  
                TreePath = roiPath
                if (roiPath.find ("\\") != -1)  :
                    parts = roiPath.split ("\\")
                elif (roiPath.find ("/") != -1)  :
                    parts = roiPath.split ("/")
                else:
                    parts = [roiPath]
                    
                if (len (parts) == 1):
                    PatientName = parts[0]            
                if (len (parts) == 2):
                    PatientName = parts[1]            
                elif (len (parts) == 3):
                    PatientName = parts[1]
                    ExamName    = parts[2]
                elif (len (parts) == 4):
                    PatientName = parts[1]
                    ExamName    = parts[2]
                    SeriesName  = parts[3]
                elif (len (parts) > 4):
                    length = len (parts)
                    PatientName = parts[length - 4]
                    ExamName    = parts[length - 3]
                    SeriesName  = parts[length - 2]  
            else:
                if DataSourceDescription.hasParameter ("PESSCARA_ExamCode") :
                    ExamName = DataSourceDescription.getParameter ("PESSCARA_ExamCode")
                if DataSourceDescription.hasParameter ("PESSCARA_SeriesCode") :
                    SeriesName = DataSourceDescription.getParameter ("PESSCARA_SeriesCode")
                if DataSourceDescription.hasParameter ("PESSCARA_SubhectCode") :
                    PatientName = DataSourceDescription.getParameter ("PESSCARA_SubjectCode")
            
        nativeOrientation = NiftiVolume.getNativeVolumeOrientation ()        
        contourOrientation = NiftiVolume.getContourOrientation ()        
        roiPath = roiDictionary.getLoadedROIFilename ()
        if (roiPath == None) :
            roiPath = "Undefined"        
        DataSourceDescription = roiDictionary.getNiftiDataSourceDescription ()
        if (DataSourceDescription is not None and DataSourceDescription.hasParameter ("full_path")) :
             niftifilename = DataSourceDescription.getParameter ("full_path")
        else:
             niftifilename = NiftiVolume.getFileName ()
             if niftifilename is None or niftifilename == "" :
                 niftifilename = "Undefined"  
        if niftifilename != "Undefined" :
             LongNiftiFilenamePath = LongFileNameAPI.getLongFileNamePath (niftifilename)
        else:
            LongNiftiFilenamePath = "Undefined"
        dataset = {}
        dataset["Long Nifti Filename Path"] = str (LongNiftiFilenamePath)
        dataset["pointInSlice"] = pointInSlice
        dataset["pointCrossSlice"] = pointCrossSlice
        dataset["Dataset Path"] = str (TreePath)
        dataset["Patient"] = str (PatientName)
        dataset["Exam"] = str (ExamName)
        dataset["Series"] = str (SeriesName)
        dataset["Description"] = str (ROIdescription)
        dataset["Description Index (#)"] = "%d" % (ROIDesciptionIndex)
        dataset["Nifti.XDim (Voxel)"] = "%d" % (int ( xVoxels))
        dataset["Nifti.YDim (Voxel)"] = "%d" % (int ( yVoxels))
        dataset["Nifti.ZDim (Stack Voxel)"] = "%d" % (int ( zVoxels))
        dataset["Slice Voxel (#)"] = "%d" % (int ( xVoxels* yVoxels))
        dataset["Volume Voxel (#)"] = "%d" % (int (xVoxels* yVoxels* zVoxels))
        dataset["X Voxel Dim (mm)"] = "%f" % (float (xDim))
        dataset["Y Voxel Dim (mm)"] = "%f" % (float (yDim))
        dataset["Z (Stack) Voxel Dim (mm)"] = "%f" % (float (zDim))        
        dataset["Slice Volume (mm^3)"] = "%f" % (float (xVoxels * yVoxels) * float (yDim * xDim * zDim))
        dataset["Scan Volume (mm^3)"] = "%f" % (float (xVoxels * yVoxels * zVoxels) * float (yDim * xDim * zDim))
        dataset["Nifti X Orientation"] = "%s" % NiftiVolumeData.getOrientationString (nativeOrientation[0])
        dataset["Nifti Y Orientation"] = "%s" % NiftiVolumeData.getOrientationString (nativeOrientation[1])
        dataset["Nifti Z (Stack) Orientation"] = "%s" % NiftiVolumeData.getOrientationString (nativeOrientation[2])    
        dataset["Nifti Orientation Code"] = "%s%s%s" % (nativeOrientation[0],nativeOrientation[1],nativeOrientation[2])
        dataset["Contour X Orientation"] = "%s" % NiftiVolumeData.getOrientationString (contourOrientation[0])        
        dataset["Contour Y Orientation"] = "%s" % NiftiVolumeData.getOrientationString (contourOrientation[1])
        dataset["Contour Z (Stack) Orientation"] = "%s" % NiftiVolumeData.getOrientationString (contourOrientation[2])
        dataset["Contour Orientation Code"] = "%s%s%s" % (contourOrientation[0],contourOrientation[1],contourOrientation[2])
        dataset["ROI File Path"] = str (roiPath)
        dataset["NiftiFile"] = str (niftifilename)        
        DatasetExportData["ROIDatasetDescription"] = dataset        
        DatasetExportData["ROIDict"] = collections.OrderedDict ()
        for roiName in exportROIList :
            DatasetExportData["ROIDict"][roiName] = ExportVolumeStatsCSV._getROIExportData (NiftiVolume, roiDictionary, roiName, pointInSlice, pointCrossSlice, CombineNonTouchingROI = CombineNonTouchingROI, forceROIDefs = forceROIDefs)                                                
        return DatasetExportData
    
    
    @staticmethod
    #writeCSVDataFileHeader
    def _addROIDataFileHeader (table, ROIType, CombineNonTouchingROI, additionalTableHeader, headerTxt) :              
        initalCount = table.getHeaderColumnCount ()
        if (ROIType == "Area") :            
            baseHeader = ["Slices Contoured (#)", "Voxel Count (#)", "ROI Contour Object (#)", "Median Voxel Value", "Mean Voxel Value", "Std Voxel Value", "Volume Contoured (mm^3)", "Largest Slice Index (#)", "Largest Slice Voxel Count (#)", "Largest Slice Area (mm^2)",  "Largest Slice Derived Diameter (mm)",  "Greatest Distance in Object (mm)"]
            if (CombineNonTouchingROI) :
                table.insertHeaderColumnsRight (baseHeader)    
            elif (table.getHeaderColumnCount () == 3) :            
                table.insertHeaderColumnsRight (["Non-Touching Contours (#)", "Contour Name"] + baseHeader)
            else:
                table.insertHeaderColumnsRight (["Contour Name"] + baseHeader)            
        elif (ROIType == "Point") :
            baseHeader = ["Points Contoured (#)", "Point Size (Voxels)", "InSlice Point Size (Voxels)", "Cross Slice Point Size (Voxels)", "Voxel Count (#)", "Median Voxel Value", "Mean Voxel Value", "Std Voxel Value", "Volume Contoured (mm^3)", "Coordinates of Points Exported (Voxel)"]
            if (CombineNonTouchingROI) :
                table.insertHeaderColumnsRight (baseHeader)    
            elif (table.getHeaderColumnCount () == 3)  :              
                table.insertHeaderColumnsRight (["Total Points (#)","Point Name"] + baseHeader)
            else:
                table.insertHeaderColumnsRight (["Point Name"] + baseHeader)
        finalCount = table.getHeaderColumnCount ()
        
        cc = additionalTableHeader.getColumnCount ()
        for x in range (finalCount - initalCount) :
            additionalTableHeader.insertData (0,cc+x,headerTxt)
            
        return table
   
    @staticmethod 
    def _getAreaName  (tpl) :
        return tpl["Contour Name"]
    
    @staticmethod 
    def _getPointName  (tpl) :
        return tpl["Point Name"]
        
    @staticmethod
    #writeCSVData
    def _appendROIDataToTable (table, additionalTableHeader, ROIDataExportData) :        
        row = table.getRowCount ()                
        headerType = ROIDataExportData["ROI Type (Point/Area)"]
        CombineNonTouchingROI = ROIDataExportData["CombineNonTouchingROI_Flag"]        
        headerRow = table.getColumnHeader ()
        dataVector = []                
        AdditonalROIHeaderName = ROIDataExportData["ROIName"]
        if (table.getHeaderColumnCount () == 3) :
            additionalTableHeader.insertDataVector (0, 0, [AdditonalROIHeaderName,AdditonalROIHeaderName,AdditonalROIHeaderName])                      
            ExportVolumeStatsCSV._addROIDataFileHeader (table, headerType, CombineNonTouchingROI, additionalTableHeader,AdditonalROIHeaderName)   
            headerRow = table.getColumnHeader ()                                                   
        for index in range (3) :
            columnName = headerRow[index]
            dataVector.append (ROIDataExportData[columnName])        
        
        if (CombineNonTouchingROI) :
            combinedROI = ROIDataExportData["CombineNonTouchingROI"]
            if (combinedROI is not None) :
                for index in range (3, len (headerRow)) :                                
                    columnName = headerRow[index]
                    if columnName in combinedROI :
                        dataVector.append (combinedROI[columnName])                               
                    else:
                        dataVector.append ("")                               
            table.insertDataVector (row, 0, dataVector)                            
        elif ROIDataExportData["ContourLst"] is  None or len (ROIDataExportData["ContourLst"]) <= 0:
            table.insertDataVector (row, 0, dataVector)                            
        else:              
            if (headerType == "Area") : #Area Contour                    
                dataVector.append (ROIDataExportData["Non-Touching Contours (#)"])                    
                table.insertDataVector (row, 0, dataVector)                                            
                headerIndexLabel= "Contour Name"       
                contourLst = sorted (ROIDataExportData["ContourLst"], key=ExportVolumeStatsCSV._getAreaName)                    
            else:
                dataVector.append (ROIDataExportData["Total Points (#)"])        
                table.insertDataVector (row, 0, dataVector)                                                
                headerIndexLabel= "Point Name"                                
                contourLst = sorted (ROIDataExportData["ContourLst"], key=ExportVolumeStatsCSV._getPointName)
            for contourData in contourLst :                
                ContourName = contourData[headerIndexLabel]            
                columnNameIndexList = table.getHeaderIndexList (headerIndexLabel)
                insertIndex = -1
                for cI in columnNameIndexList :
                    if (table.hasDataInColumn (cI,ContourName,OrEmpty = True, IgnoreCase = True)):
                        insertIndex = cI
                        break
                if (insertIndex == -1):
                    insertIndex = table.getHeaderColumnCount ()                                        
                    appendName = AdditonalROIHeaderName + ":"+ ContourName
                    ExportVolumeStatsCSV._addROIDataFileHeader (table, headerType, CombineNonTouchingROI, additionalTableHeader, appendName)                        
                    headerRow = table.getColumnHeader ()                
                dataVector = []                                
                for index in range(len (contourData)) :
                    columnName = headerRow[insertIndex+index]
                    dataVector.append (contourData[columnName])                                
                table.insertDataVector (row, insertIndex, dataVector)                           
                    
                    
                                                                             

    # writeExportVolumeCSVHeader    
    @staticmethod
    def _getDatasetTable (DatasetExportData, maintable, ROIExportTableDictionary, ROIHeaderTableDictionary) :                                                                
        row = maintable.getRowCount ()
        if (row == 0) :            
            maintable.insertHeaderColumnsRight (["Long Nifti Filename Path","Dataset Path","Patient", "Exam", "Series", "Description", "Description Index (#)", "Nifti.XDim (Voxel)", "Nifti.YDim (Voxel)", "Nifti.ZDim (Stack Voxel)","Slice Voxel (#)", "Volume Voxel (#)", "X Voxel Dim (mm)", "Y Voxel Dim (mm)", "Z (Stack) Voxel Dim (mm)", "Slice Volume (mm^3)","Scan Volume (mm^3)","Nifti X Orientation", "Nifti Y Orientation", "Nifti Z (Stack) Orientation","Nifti Orientation Code","Contour X Orientation","Contour Y Orientation", "Contour Z (Stack) Orientation", "Contour Orientation Code","ROI File Path","NiftiFile"])                                    
        headerColumns =  maintable.getColumnHeader ()
        dataVector = []
        mainDescription = DatasetExportData['ROIDatasetDescription']
        for headtext in headerColumns :
            dataVector.append (mainDescription[headtext])        
        maintable.insertDataVector (row, 0, dataVector)            
                        
        exportROIList = sorted (list (DatasetExportData["ROIDict"].keys ()))            
        for roiName in exportROIList :
            #print ("Start + " + roiName )
            ROIDataset = DatasetExportData["ROIDict"][roiName]            
            
            if roiName not in ROIExportTableDictionary :            
                table = PandasCompatibleTable ()            
                table.insertHeaderColumnsRight (["RadlexID", "ROIName", "ROI Type (Point/Area)"])                
                ROIExportTableDictionary[roiName] = table 
                
                headerTable = PandasCompatibleTable ()    
                ROIHeaderTableDictionary[roiName] = headerTable                
            else:
                table = ROIExportTableDictionary[roiName]                
                headerTable = ROIHeaderTableDictionary[roiName]
            ExportVolumeStatsCSV._appendROIDataToTable (table, headerTable, ROIDataset)                            
        
    
    @staticmethod    
    def getDatasetTable (DatasetExportList) :                                                    
        if len (DatasetExportList) <= 0 :
            return None        
        pointInSlice = None
        for DatasetExportData in DatasetExportList :
            if DatasetExportData is not None :
                mainDescription = DatasetExportData['ROIDatasetDescription']
                pointInSlice = mainDescription["pointInSlice"]
                pointCrossSlice = mainDescription["pointCrossSlice"]        
                break
        if (pointInSlice is None) : 
             return None        
        exportTable = PandasCompatibleTable ()        
        exportTable.insertDataVector (0,0, ["Generated", DateUtil.dateToString (DateUtil.today()), TimeUtil.timeToString (TimeUtil.getTimeNow ()) ])
        exportTable.insertDataVector (1,0, ["Point (Voxel) in slice", str (pointInSlice) ])
        exportTable.insertDataVector (2,0, ["Point (Voxel) cross slice", str (pointCrossSlice) ])
        exportTable.appendBlankRow ()        
        roiExportDictionary = collections.OrderedDict ()        
        roiExportTableAdditionalHeader = collections.OrderedDict ()        
        maintable = PandasCompatibleTable ()
        for dataset in DatasetExportList :        
            if (dataset is not None) :
                ExportVolumeStatsCSV._getDatasetTable (dataset, maintable, roiExportDictionary,roiExportTableAdditionalHeader)                        
        
        headertable = PandasCompatibleTable ()
        datatable = PandasCompatibleTable ()
        for roiName in roiExportDictionary.keys () :     
            headertable.insertTableRight (roiExportTableAdditionalHeader[roiName])
            datatable.insertTableRight (roiExportDictionary[roiName])
        headertable.appendTable (datatable)            
        finaltable = PandasCompatibleTable ()
        finaltable.appendBlankRow ()
        finaltable.appendTable (maintable)
        finaltable.insertTableRight (headertable)
        exportTable.appendTable (finaltable)
        return  exportTable      
        
        
    @staticmethod
    def exportVolumeCSVUI (parent, ROIDefs) :        
        dlg = RCC_ROIVolumeCSVExportDlg (parent, ROIDefs)
        dlg.exec_ ()
        if (dlg._result != "Export"):
            return ("", 0,0, [], True)
        pointInSlice = dlg._pointInSliceSize
        pointCrossSlice = dlg._pointCrossSliceSize
        exportROIList = dlg._SelectedROILst
        combineNonTochingROIwithTheSameName = dlg._combineNonTochingROIwithTheSameName
         
        dlg= QFileDialog( None )
        dlg.setWindowTitle( 'Select file to save ROI description data to:' )
        dlg.setViewMode( QFileDialog.Detail )    
        dlg.setFileMode (QFileDialog.AnyFile)        
        dlg.setNameFilters( ['csv file (*.csv)', 'excel file (*.xlsx)', 'any file (*)'] )        
        dlg.setAcceptMode (QFileDialog.AcceptSave)
        if dlg.exec_() and (len (dlg.selectedFiles())== 1):
            exportPath = dlg.selectedFiles()[0]                                        
            if ("xlsx" in dlg.selectedNameFilter ()) :
                if not exportPath.lower ().endswith (".xlsx") :
                    exportPath += ".xlsx"
            elif not exportPath.lower ().endswith (".csv") :
                exportPath += ".csv"
                
            return (exportPath, pointInSlice, pointCrossSlice, exportROIList, combineNonTochingROIwithTheSameName)
        return ("", 0,0, [], True)
            
    
class PenThresholdPreset :    
    def __init__ (self, PresetName = "Untitled", MinThreshold = 0, MaxThreshold = 1024, EraseOutofRange = True) :
        self._PresetUID = None
        self._PresetName = PresetName        
        self._MinThreshold = MinThreshold
        self._MaxThreshold = MaxThreshold
        self._EraseOutofRange = EraseOutofRange
   
    def copy (self) :
        newObj = PenThresholdPreset ()
        newObj._PresetUID = self._PresetUID
        newObj._PresetName = self._PresetName
        newObj._MinThreshold = self._MinThreshold
        newObj._MaxThreshold = self._MaxThreshold
        newObj._EraseOutofRange = self._EraseOutofRange
        return newObj
        
    @staticmethod
    def initFromFileObj (fobj) :
        preset = PenThresholdPreset ()
        preset._PresetUID = fobj.getParameter ("PresetUID")
        preset._PresetName = fobj.getParameter ("PresetName")
        preset._MinThreshold = fobj.getParameter ("PresetMinThreshold")
        preset._MaxThreshold = fobj.getParameter ("PresetMaxThreshold")
        preset._EraseOutofRange = fobj.getParameter ("EraseOutofRange")
        return preset
    
    def getFileObject (self,number) :
        fobj = FileObject ("ThresholdPreset_" + str (number))
        fobj.setParameter ("PresetUID",self._PresetUID)
        fobj.setParameter ("PresetName",self._PresetName)
        fobj.setParameter ("PresetMinThreshold",self._MinThreshold)
        fobj.setParameter ("PresetMaxThreshold",self._MaxThreshold)     
        fobj.setParameter ("EraseOutofRange",self._EraseOutofRange)     
        return fobj
           
    def setName (self, name) :
        if (self._PresetName != name) :
            self._PresetName = name
            return True
        return False
    
    def setMinThreshold (self, minThreshold) :
        if (self._MinThreshold != minThreshold) :
            self._MinThreshold = minThreshold
            return True
        return False
        
    def setMaxThreshold (self, maxThreshold) :
        if (self._MaxThreshold != maxThreshold) :
            self._MaxThreshold = maxThreshold
            return True
        return False
    
    def getMinThreshold (self) :
        return self._MinThreshold
    
    def getMaxThreshold (self) :
        return self._MaxThreshold

    def setEraseOutofRange (self, erase) :
        if self._EraseOutofRange != erase :
            self._EraseOutofRange = erase
            return True
        return False
        
    def getEraseOutofRange (self) :
        return self._EraseOutofRange
    
    def getPresetName (self) :
        return self._PresetName
    
    def getPresetUID (self) :
        if self._PresetUID  is None :
            uid =  str (uuid4())
            self._PresetUID = "Threshold_Preset_" + self._PresetName + "_" + uid + "_" + str (time.time ())
        return self._PresetUID 
    
class PenSettings :
        def __init__ (self, fobj = None) :
            self._ActiveROIUID = None
            self._ROIPaintSettings = {}
            self._RememberActiveROIPaintSettings = False
            self._pixmapSourceCacheShape = (-1,-1)
            self._ThresholdPresets = {}
            self._SelectedThresholdPreset = None
            self._penPixmap = None 
            self._pixmapSourceCache = None 
            self._ListenerList = []
            self._PenOpperation = 1
            self._penmaskcache = None            
            self._ThresholdSet = False
            self._minThreashold = 0
            self._maxThreashold = 1024
            self._penShape = "Circle" # "Square"
            self._penSize = 10
            self._penCacheMem = None
            self._autoEraseOutofRangeThresholdDraw = True
            self._penCacheState = ()
            self._penSliceThickness = 1            
            self._penTool = "Draw"
            self._AutoAcceptOneClickSegmentation = False
            self._NormalizePenDimensionsToVoxelDimensions = True
            if fobj is not None :
               self._ThresholdSet = fobj.getParameter ("ThresholdSet")
               self._ThresholdSet = False
               self._minThreashold = fobj.getParameter ("MinThreshold")
               self._maxThreashold = fobj.getParameter ("MaxThreshold")
               self._penShape = fobj.getParameter ("PenShape")
               self._penSize = fobj.getParameter ("PenSize")
               self._PenOpperation = fobj.getParameter ("PenOpperation")
               self._PenOpperation = 1
               if (self._PenOpperation == 0) :
                   self._PenOpperation = -1
               try :
                   self._autoEraseOutofRangeThresholdDraw = fobj.getParameter ("AutoEraseOutofRangeThresholdDraw")
               except :
                   self._autoEraseOutofRangeThresholdDraw = True
               try :
                   self._penSliceThickness = fobj.getParameter ("PenSliceThickness")
                   self._penSliceThickness = 1
               except :
                   self._penSliceThickness = 1
               try :
                   self._penTool = fobj.getParameter ("PenTool")
               except :
                   self._penTool = "Draw"
               try :
                   self._RememberActiveROIPaintSettings = fobj.getParameter ("RememberActiveROIPaintSettings")                   
               except:
                   self._RememberActiveROIPaintSettings = True
               try : 
                   self._AutoAcceptOneClickSegmentation = fobj.getParameter ("AutoAcceptOneClickSegmentation")         
               except:
                   self._AutoAcceptOneClickSegmentation = False
               try :
                   self._ROIPaintSettings = fobj.getParameter ("ROIPaintSettings")
                   #all ROI initaled in edit mode with thresholding off drawing on a single slice
                   for settings in self._RememberActiveROIPaintSettings.values () :
                       settings["PenOpperation"]  = None
                       settings["ThresholdEnabled"] = None
                       settings["PenThickness"] = None
                       settings["AutoAcceptOneClickSegmentation"] = None
               except:
                   self._ROIPaintSettings = {}
               try :
                   self._SelectedThresholdPreset = fobj.getParameter ("SelectedThresholdPreset")
                   thresholdPresetCount = fobj.getParameter ("ThresholdPresetcount")
                   self._ThresholdPresets = {}
                   for presetCount in range (thresholdPresetCount) :
                       try :
                           thresholdFileObject = fobj.getFileObject ("ThresholdPreset_" + str (presetCount))
                           setting = PenThresholdPreset.initFromFileObj (thresholdFileObject)
                           self._ThresholdPresets[setting.getPresetUID ()] = setting
                       except:
                           pass
                   if self._SelectedThresholdPreset not in self._ThresholdPresets :
                       self._SelectedThresholdPreset = None
               except:
                   self._ThresholdPresets = {}
                   self._SelectedThresholdPreset = None
                   
               try : 
                   self._NormalizePenDimensionsToVoxelDimensions = fobj.getParameter ("NormalizePenDimensionsToVoxelDimensions")         
               except:
                   self._NormalizePenDimensionsToVoxelDimensions = True
        
        def setNormalizePenDimensionsToVoxelDimensions (self, val, UpdateSettings = True) :
            if (val != self._NormalizePenDimensionsToVoxelDimensions):
                self._NormalizePenDimensionsToVoxelDimensions = val
                if (UpdateSettings):
                    self._PenSettingsChanged ()
                            
        def getNormalizePenDimensionsToVoxelDimensions (self) :
            return self._NormalizePenDimensionsToVoxelDimensions
        
        def setThresholdSettingsFromSelectedThreshold (self) :
            selected = self.getSelectedThresholdPreset ()
            if (selected is not None) :
                minTh = selected.getMinThreshold ()
                maxTh = selected.getMaxThreshold ()
                eraseoutofrange = selected.getEraseOutofRange ()
                if (self._minThreashold, self._maxThreashold, self._autoEraseOutofRangeThresholdDraw) != (minTh, maxTh, eraseoutofrange) :
                    self._minThreashold = minTh
                    self._maxThreashold = maxTh
                    self._autoEraseOutofRangeThresholdDraw = eraseoutofrange
                    self._PenSettingsChanged ()       
               
        def setSelectedThresholdPresetValues (self, Name, minV, maxV, EraseOutofRange) :
            preset = self.getSelectedThresholdPreset ()
            if preset  is not None :
                changed = False
                if preset.setName (Name) :
                    changed = True
                if preset.setMinThreshold (minV) :
                    changed = True
                if preset.setMaxThreshold (maxV) :
                    changed = True
                if preset.setEraseOutofRange (EraseOutofRange) :
                    changed = True
                if changed :
                    self._PenSettingsChanged ()       
            
        def getSelectedThresholdPreset (self) :
            if self._SelectedThresholdPreset is None :
                return None
            if self._SelectedThresholdPreset in self._ThresholdPresets :
                return self._ThresholdPresets[self._SelectedThresholdPreset] 
            return None
        
        def updateSelectedThresholdPresetToCurrentSettings (self) :            
            if self._SelectedThresholdPreset is None :
                return 
            if self._SelectedThresholdPreset in self._ThresholdPresets :
                changed = False
                if (self._ThresholdPresets[self._SelectedThresholdPreset].setMinThreshold (self._minThreashold)) :
                    changed = True
                if (self._ThresholdPresets[self._SelectedThresholdPreset].setMaxThreshold (self._maxThreashold)) :
                    changed = True
                if (self._ThresholdPresets[self._SelectedThresholdPreset].setEraseOutofRange (self._autoEraseOutofRangeThresholdDraw)) :
                    changed = True
                if changed :
                    self._PenSettingsChanged ()  
                    
        def deleteThreshold (self, uid) :
            if uid is None :
                return 
            if uid in self._ThresholdPresets :
                del self._ThresholdPresets[uid]
                if uid == self._SelectedThresholdPreset :
                    self._SelectedThresholdPreset = None
                self._PenSettingsChanged ()  
        
        def createThresholdPreset (self, name, minV, maxV, autoErase) :
            preset = PenThresholdPreset (name, minV, maxV, autoErase)
            uid = preset.getPresetUID ()
            self._ThresholdPresets[uid] = preset
            self._SelectedThresholdPreset = uid
            self._PenSettingsChanged ()
            return uid
            
        
        def setSelectedThresholdPresetUID (self, uid, FirePenSettingsChanged = True) :
            if uid is None :
                if self._SelectedThresholdPreset != uid :
                    self._SelectedThresholdPreset = uid
                    if (FirePenSettingsChanged) :
                        self._PenSettingsChanged ()  
                return True
            if uid in self._ThresholdPresets :
                if self._SelectedThresholdPreset != uid :
                    self._SelectedThresholdPreset = uid
                    if (FirePenSettingsChanged) :
                        self._PenSettingsChanged ()  
                return True
            return False
                
        def _getName (self, tpl) :
            return tpl[1]
        
        def getPresetList (self) :
            lst = []
            for key, value in self._ThresholdPresets.items () :
                lst.append ((key, value.getPresetName ()))            
            return sorted (lst, key= self._getName)
            
        def setSelectedThresholdName (self, name) :            
            if self._SelectedThresholdPreset is None :
                return 
            if self._SelectedThresholdPreset in self._ThresholdPresets :
                if (self._ThresholdPresets[self._SelectedThresholdPreset].setName (name)) :
                    self._PenSettingsChanged ()  
        
        def setActiveROIID (self, uid) :
            if not self._RememberActiveROIPaintSettings :
                self._ActiveROIUID = None
            elif self._ActiveROIUID != uid :                
                self._ActiveROIUID = uid 
                if uid is not None :                    
                    if uid in self._ROIPaintSettings :
                        UpdateROISettingToReflectCurrentState = False                    
                        settings = self._ROIPaintSettings[uid]
                        SettingsChanged = False
                        try :
                            if "ThresholdEnabled" in settings :
                                if settings["ThresholdEnabled"] is None :        
                                    UpdateROISettingToReflectCurrentState = True                                                                
                                elif self._ThresholdSet != settings["ThresholdEnabled"] :
                                    self._ThresholdSet =  settings["ThresholdEnabled"]
                                    SettingsChanged = True
                            if "EraseOutOfRange" in settings :
                                if  self._autoEraseOutofRangeThresholdDraw !=  settings["EraseOutOfRange"] :
                                    self._autoEraseOutofRangeThresholdDraw =  settings["EraseOutOfRange"]
                                    SettingsChanged = True
                            if "PenShape" in settings :
                               if self._penShape !=  settings["PenShape"] :
                                   self._penShape =  settings["PenShape"]
                                   SettingsChanged = True
                            if "PenSize" in settings :
                               if self._penSize !=  settings["PenSize"] :
                                   self._penSize =  settings["PenSize"]
                                   SettingsChanged = True
                            if "PenThickness" in settings :
                               if settings["PenThickness"] is None :        
                                   UpdateROISettingToReflectCurrentState = True                                                                
                               if self._penSliceThickness != settings["PenThickness"] :
                                   self._penSliceThickness =  settings["PenThickness"]
                                   SettingsChanged = True
                            if "UpperThreshold" in settings :
                                if self._maxThreashold !=  settings["UpperThreshold"] :
                                   self._maxThreashold =  settings["UpperThreshold"]
                                   SettingsChanged = True
                            if "LowerThreshold" in settings :
                                if self._minThreashold !=  settings["LowerThreshold"] :
                                    self._minThreashold =  settings["LowerThreshold"]
                                    SettingsChanged = True
                            if "PenOpperation" in settings:
                                if settings["PenOpperation"] is None :                                                                        
                                    UpdateROISettingToReflectCurrentState = True
                                elif self._PenOpperation != settings["PenOpperation"]  :
                                    self._PenOpperation = settings["PenOpperation"] 
                                    SettingsChanged = True
                            if "AutoAcceptOneClickSegmentation" in settings:
                                if settings["AutoAcceptOneClickSegmentation"] is None :                                                                        
                                    UpdateROISettingToReflectCurrentState = True
                                elif self._AutoAcceptOneClickSegmentation != settings["AutoAcceptOneClickSegmentation"]  :
                                    self._AutoAcceptOneClickSegmentation = settings["AutoAcceptOneClickSegmentation"] 
                                    SettingsChanged = True
                                    
                            if "SelectedThresholdPreset" in settings:
                                if self._SelectedThresholdPreset != settings["SelectedThresholdPreset"]  :
                                    self.setSelectedThresholdPresetUID (settings["SelectedThresholdPreset"], FirePenSettingsChanged = False)
                                    SettingsChanged = True
                            
                            if "NormalizePenDimensionsToVoxelDimensions" in settings :
                                  if settings["NormalizePenDimensionsToVoxelDimensions"] is None :                                                                        
                                      UpdateROISettingToReflectCurrentState = True
                                  elif self._NormalizePenDimensionsToVoxelDimensions != settings["NormalizePenDimensionsToVoxelDimensions"]  :
                                      self._NormalizePenDimensionsToVoxelDimensions = settings["NormalizePenDimensionsToVoxelDimensions"] 
                                      SettingsChanged = True
                        except:
                            pass
                        if UpdateROISettingToReflectCurrentState :
                            self.UpdateSelectedROIPaintSettings ()
                        elif SettingsChanged :
                            self._PenSettingsChanged ()                          
        
        def getActiveROIID (self) :                        
            return self._ActiveROIUID
        
        def getROIPaintSettings (self) :
            if self._ActiveROIUID in self._ROIPaintSettings :
                return self._ROIPaintSettings[self._ActiveROIUID]
            return self._getCurrentPaintSettings ()
        
        def _getCurrentPaintSettings (self) :
            obj = {}
            obj["ThresholdEnabled"] = self._ThresholdSet
            obj["EraseOutOfRange"] = self._autoEraseOutofRangeThresholdDraw
            obj["PenOpperation"] =self._PenOpperation
            obj["PenShape"] = self._penShape 
            obj["PenSize"] = self._penSize
            obj["PenThickness"] = self._penSliceThickness
            obj["UpperThreshold"] = self._maxThreashold
            obj["LowerThreshold"] = self._minThreashold
            obj["SelectedThresholdPreset"] = self._SelectedThresholdPreset
            obj["AutoAcceptOneClickSegmentation"] = self._AutoAcceptOneClickSegmentation
            obj["NormalizePenDimensionsToVoxelDimensions"] = self._NormalizePenDimensionsToVoxelDimensions
            return obj
        
        def arePenSettingsChanged (self, PenSettings) :
            if PenSettings is None :
                return True
            currentSettings = self._getCurrentPaintSettings ()
            otherSettings = PenSettings._getCurrentPaintSettings () 
            for key, value in currentSettings.items () :
                if otherSettings[key] != value :
                    return True
            return False
        
        def setRememberActiveROIPaintSettings (self, val) :
            if self._RememberActiveROIPaintSettings != val :
               self._RememberActiveROIPaintSettings = val 
               self._PenSettingsChanged ()
               return True
            return False
        
        def getRememberActiveROIPaintSettings (self) :
            return self._RememberActiveROIPaintSettings
        
        def UpdateSelectedROIPaintSettings (self) :
            if self._RememberActiveROIPaintSettings and self._ActiveROIUID is not None :
                self._ROIPaintSettings[self._ActiveROIUID] = self._getCurrentPaintSettings ()
                self._PenSettingsChanged ()
            
        def getPenSliceThickness (self):
            return self._penSliceThickness
        
        def setPenSliceThickness (self, val, UpdateSettings = True):
            if (self._penSliceThickness != val) :
                self._penSliceThickness = val
                if (UpdateSettings):
                    self._PenSettingsChanged ()
                    
        def getAutoEraseOutofRangeinThresholdDraw (self) :
            return self._autoEraseOutofRangeThresholdDraw
        
        def copyThresholdPresetsAndROIPresetsFromProject (self, projectSettings) :
            self._ROIPaintSettings = {}
            for ROIDUID, pSettings in projectSettings._ROIPaintSettings.items ():
                dictCopy = {}
                for key, value in pSettings.items () :
                    dictCopy[key] = value
                self._ROIPaintSettings[ROIDUID] = dictCopy
            
            self._SelectedThresholdPreset = self._SelectedThresholdPreset
            self._ThresholdPresets = {}
            for key, value in projectSettings._ThresholdPresets.items () :
                self._ThresholdPresets[key] = value.copy ()
            self._PenSettingsChanged ()
            
        def copy (self) :
            newSettings = PenSettings ()
            newSettings._NormalizePenDimensionsToVoxelDimensions = self._NormalizePenDimensionsToVoxelDimensions
            newSettings._PenOpperation = self._PenOpperation
            newSettings._ThresholdSet = self._ThresholdSet
            newSettings._minThreashold = self._minThreashold
            newSettings._maxThreashold = self._maxThreashold
            newSettings._penShape = self._penShape
            newSettings._penSize = self._penSize            
            newSettings._penSliceThickness = self._penSliceThickness            
            newSettings._autoEraseOutofRangeThresholdDraw = self._autoEraseOutofRangeThresholdDraw
            newSettings._penTool = self._penTool
            newSettings._AutoAcceptOneClickSegmentation = self._AutoAcceptOneClickSegmentation
            newSettings._ActiveROIUID = self._ActiveROIUID 
            newSettings._RememberActiveROIPaintSettings =  self._RememberActiveROIPaintSettings
            newSettings._ROIPaintSettings = {}
            for ROIDUID, pSettings in self._ROIPaintSettings.items ():
                dictCopy = {}
                for key, value in pSettings.items () :
                    dictCopy[key] = value
                newSettings._ROIPaintSettings[ROIDUID] = dictCopy
            
            newSettings._SelectedThresholdPreset = self._SelectedThresholdPreset
            newSettings._ThresholdPresets = {}
            for key, value in self._ThresholdPresets.items () :
                newSettings._ThresholdPresets[key] = value.copy ()
            return newSettings
        
        def getPenTool (self) :
            return self._penTool
        
        def setPenTool (self, tool) :
            if (tool != self._penTool) :
                self._penTool = tool
                self._PenSettingsChanged ()                 
        
        def setAutoEraseOutofRangeThresholdDraw (self, val):
            if val != self._autoEraseOutofRangeThresholdDraw :
                self._autoEraseOutofRangeThresholdDraw = val
                self._PenSettingsChanged ()                 
                
        def getPenSize (self) :
            return self._penSize
        
        def getAutoAcceptOneClickSegmentation (self, ROIID = None) :
            if ROIID is None or ROIID not in self._ROIPaintSettings :
                return self._AutoAcceptOneClickSegmentation
            if "AutoAcceptOneClickSegmentation" not in  self._ROIPaintSettings[ROIID] or self._ROIPaintSettings[ROIID]["AutoAcceptOneClickSegmentation"] is None :
                return self._AutoAcceptOneClickSegmentation
            return bool (self._ROIPaintSettings[ROIID]["AutoAcceptOneClickSegmentation"])
        
        def setAutoAcceptOneClickSegmentation (self, val, ROIDefs) :
            if self._AutoAcceptOneClickSegmentation != val :
                self._AutoAcceptOneClickSegmentation = val
                for roiName in ROIDefs.getSelectedROI () :
                    if roiName != "None" :
                        ROIID = ROIDefs.getROIIDNumber (roiName)
                        if ROIID not in self._ROIPaintSettings :
                            self._ROIPaintSettings[ROIID] = self._getCurrentPaintSettings ()
                        self._ROIPaintSettings[ROIID]["AutoAcceptOneClickSegmentation"] = val
                self._PenSettingsChanged ()
                
        def setCirclePen (self) :
            if (self._penShape != "Circle") :
                self._penShape = "Circle"
                self._PenSettingsChanged ()
        
        
        def getPenPixmap (self, NiftiVolume, SliceView, width, height) :
            if NiftiVolume is None :
                return None 
            mask = self.getPenMask (NiftiVolume, SliceView)
            if self._pixmapSourceCache is not mask or self._pixmapSourceCacheShape != (width, height):
                self._pixmapSourceCache = mask
                self._pixmapSourceCacheShape = (width, height)
                mask = skimage.transform.resize (mask, (width, height), order = 0, preserve_range = True)
                mask = mask == 1.0
                mask = np.pad (mask, (1,1), mode="constant")
                base = skimage.morphology.dilation (mask)
                base[mask] = False
                outerEdge = skimage.morphology.dilation (base)
                if width < 4 or height < 4 :
                    outerEdge[mask] = False
                imageshape = [4] + list(base.shape)
                base = base.astype (np.uint8)
                base *= 255             
                width, height = base.shape 
                base = np.expand_dims (base, 0)
                data = np.zeros (imageshape, dtype=np.uint8)
                data[...] = base[...]
                x, y = outerEdge.nonzero ()
                data[3,x, y] = 255
                #data[1,...] = base[...]
                #data[2,...] = base[...]
                #data[3,...] = base[...] 
                
                self._penimage =  QtGui.QImage (data.tobytes('F'), width, height, width*4, QtGui.QImage.Format_ARGB32)
                self._penPixmap = QtGui.QPixmap.fromImage (self._penimage)
            return self._penPixmap
            
        def isCircle (self) :
            return self._penShape == "Circle"
        
        def isSquare (self) :
            return self._penShape == "Square"
        
        def setSquarePen (self) :
            if (self._penShape != "Square") :
                self._penShape = "Square"
                self._PenSettingsChanged ()                
        
        def setPenSize (self, pensize) :
            if (self._penSize != pensize):
                self._penSize = pensize
                self._PenSettingsChanged ()
        
        def setAddPen (self, UpdateSettings=True) :
            if (self._PenOpperation != 1) :
                self._PenOpperation = 1
                if (UpdateSettings):
                    self._PenSettingsChanged ()
        
        def setDeepGrowPen (self, UpdateSettings=True) :
            if (self._PenOpperation != 2) :
                self._PenOpperation = 2
                if (UpdateSettings):
                    self._PenSettingsChanged ()
        
        def isDeepGrowPen (self) :
            return self._PenOpperation == 2
        
        def isAddPen (self):
            return self._PenOpperation == 1
        
        def isErasePen (self):
            return self._PenOpperation == -1
        
        def setErasePen (self, UpdateSettings=True) :
            if (self._PenOpperation != -1) :
                self._PenOpperation = -1
                if (UpdateSettings):
                    self._PenSettingsChanged ()
        
        def addChangeListener (self, func) :
            if (func not in self._ListenerList) :
               self._ListenerList.append (func)
               
        def _PenSettingsChanged (self) :
            for listener in self._ListenerList :
                listener ()
                
        def getFileObject (self) :
            fobj = FileObject ("RCC_PenSettings")
            fobj.setParameter ("ThresholdSet",self._ThresholdSet)
            fobj.setParameter ("MinThreshold",self._minThreashold)
            fobj.setParameter ("MaxThreshold",self._maxThreashold)
            fobj.setParameter ("PenShape",self._penShape)
            fobj.setParameter ("PenSize",self._penSize)
            fobj.setParameter ("PenOpperation",self._PenOpperation)            
            fobj.setParameter ("AutoEraseOutofRangeThresholdDraw", self._autoEraseOutofRangeThresholdDraw)
            fobj.setParameter ("PenSliceThickness",self._penSliceThickness)     
            fobj.setParameter ("PenTool", self._penTool)
            
            fobj.setParameter ("RememberActiveROIPaintSettings", self._RememberActiveROIPaintSettings)
            fobj.setParameter ("ROIPaintSettings", self._ROIPaintSettings)
            
            fobj.setParameter ("AutoAcceptOneClickSegmentation", self._AutoAcceptOneClickSegmentation)
            fobj.setParameter ("NormalizePenDimensionsToVoxelDimensions", self._NormalizePenDimensionsToVoxelDimensions)
            fobj.setParameter ("SelectedThresholdPreset", self._SelectedThresholdPreset )
            fobj.setParameter ("ThresholdPresetcount", len (self._ThresholdPresets) )            
            for count, value in enumerate (self._ThresholdPresets.values ()) :
                fobj.addInnerFileObject (value.getFileObject (count))            
            return fobj
        
        @staticmethod
        def _drawCircle (_penCacheMem, penSize):            
            radiusSQ = (penSize -1) **2
            sqList = np.arange (-penSize,penSize + 1)
            sqList *= sqList                    
            for ix, sqX in enumerate(sqList) :
                if sqX > radiusSQ :
                    _penCacheMem[:,ix] = False
                else:
                    zY = (sqX + sqList > radiusSQ).nonzero ()
                    _penCacheMem[zY,ix] = False                           
            return _penCacheMem        
                    
        
        @staticmethod
        def _drawElipse (_penCacheMem, penXDim, penYDim):            
            pxS = int ((penXDim-1)/2)
            pyS = int ((penYDim-1)/2)
            xVec = np.arange (-pxS,pxS+1,dtype=np.float)
            yVec = np.arange (-pyS,pyS+1,dtype=np.float)                                                        
            xVec *= xVec 
            xVec /= float ((pxS + 1)**2)
            yVec *= yVec
            yVec /= float ((pyS + 1)**2)                                                            
            for yInd in range (penYDim) : 
                _penCacheMem[:,yInd] = xVec
            for xInd in range (penXDim) : 
                _penCacheMem[xInd,:] += yVec                                                        
            return _penCacheMem <= 1.0
        
        def getPenMask (self, NiftiVolume, SliceView):
            axis = SliceView.getSliceAxis ()
            penShape, penSize = self._penShape,  self._penSize
            cacheState = (penShape, penSize, axis, self._NormalizePenDimensionsToVoxelDimensions)
            if (self._penCacheState == cacheState) :
                return self._penCacheMem 
            else:    
                self._penCacheMem = None
                self._penCacheState = cacheState
                if self._NormalizePenDimensionsToVoxelDimensions :
                    xDim, yDim, zDim = NiftiVolume.getNormalizedVoxelSize ()                
                    if axis == "Z" :
                        pX = xDim
                        pY = yDim
                    elif axis == "Y" :
                        pX = xDim
                        pY = zDim
                    elif axis == "X" :
                        pX = yDim
                        pY = zDim                    
                    if pX != pY : 
                        size  = float (penSize + penSize + 1)
                        if pX < pY :                            
                            penXDim = max (int (size),1)                                                        
                            penYDim = max (1, int (size / float (pY)))
                            if penYDim % 2 == 0 :
                                penYDim += 1
                        else:                            
                            penYDim = max (1, int (size))                            
                            penXDim = max (1, int (size / float (pX)))                        
                            if penXDim % 2 == 0 :
                                penXDim += 1
                        if (penShape == "Circle") and penXDim > 1 and penYDim > 1 :
                            if penYDim == penXDim :
                                self._penCacheMem =  np.ones ((penXDim, penYDim), dtype=np.bool)
                                self._penCacheMem = PenSettings._drawCircle (self._penCacheMem, penSize)     
                            else:                                                 
                                self._penCacheMem =  np.ones ((penXDim, penYDim), dtype=np.float)                                   
                                self._penCacheMem = PenSettings._drawElipse (self._penCacheMem, penXDim, penYDim)                                     
                        else:
                            self._penCacheMem =  np.ones ((penXDim, penYDim), dtype=np.bool)
                        return self._penCacheMem     
                if self._penCacheMem is None :
                    dim = 1 + penSize + penSize
                    self._penCacheMem =  np.ones ((dim, dim), dtype=np.bool)
                    if (dim > 1) and (penShape == "Circle") :
                        self._penCacheMem = PenSettings._drawCircle (self._penCacheMem, penSize)                            
            return self._penCacheMem                
        
        def setThresholdRange (self, minval, maxVal) :                           
                if (self._minThreashold, self._maxThreashold) != (minval, maxVal) :
                    self._minThreashold = minval
                    self._maxThreashold = maxVal
                    self._PenSettingsChanged ()       
        
        def getThresholdRange (self) :
            return (self._minThreashold, self._maxThreashold)
        
        def getPenOpperation (self) :
            return self._PenOpperation
        
        def setThresholdSet (self, val) :
            if (val != self._ThresholdSet) :
                self._ThresholdSet = val
                self._PenSettingsChanged ()
            
        def isThresholded (self) :
            return  (self._ThresholdSet) 
        
        def getThresholdMask (self, NiftiSliceValues):
            if (self._penmaskcache is None or self._penmaskcache.shape != NiftiSliceValues.shape) :
                self._penmaskcache = np.ones ((NiftiSliceValues.shape),dtype = np.uint8)
            else:
                self._penmaskcache[:] = 1
            penMask = self._penmaskcache
            penMask[NiftiSliceValues < self._minThreashold] = 0
            penMask[NiftiSliceValues > self._maxThreashold] = 0
            return penMask == 1
