#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 11 15:14:45 2019

@author: m160897
"""
import nibabel as nib
from rilcontour.rilcontourlib.util.rcc_util import NiftiVolumeData
import numpy as np
import os

class RILContourFileAccessHelper :    
    
    @staticmethod    
    def loadAlignedNiftiVolume ( pathToNifti, contour_axis, ReturnNibabelObj = False, ReturnRILContourVolumeObject = False, Verbose = False) :                    
        if not os.path.exists (pathToNifti):            
            print ("File does not exist: %s" % pathToNifti)
            if not ReturnNibabelObj :
                return None
            return None, None
        nifti = nib.load (pathToNifti)         
        volumeData = NiftiVolumeData (nifti, SliceAxis = contour_axis, Verbose=Verbose)
        if ReturnRILContourVolumeObject :
            data = volumeData 
        else:
            data = volumeData.getImageData ()                                                  
        if not ReturnNibabelObj :
            return data
        return data, nifti
    