#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 11 15:31:37 2019

@author: m160897
"""

import numpy as np

class NibabelDataAccessHelper  :
    
    @staticmethod 
    def getNibabelData (nibabelObject) :
        try :
            return np.asanyarray(nibabelObject.dataobj)
        except:
            return nibabelObject.get_data ()
        