#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  9 13:37:32 2016

@author: m160897
"""
import sys
import os
import tempfile
import uuid
import datetime
import socket
import weakref
from PyQt5 import QtCore
from PyQt5.QtWidgets import  QDialog
from PyQt5.QtCore import QThread, pyqtSignal 
from rilcontourlib.util.rcc_util import ResizeWidgetHelper, FileObject, PythonVersionTest, MessageBoxUtil
from rilcontourlib.ui.qt_ui_autogen.rcc_pesscaralogin import Ui_LoginDlg
from rilcontourlib.ui.qt_ui_autogen.rcc_pesscarasearch import Ui_SearchPesscaraDlg


try:
    try :
        import wget
        print('wget loaded')
        import tiPY as qin  # qin (image database) library (requires the sys.path.append(...) line above)
        print ("Qin library loaded.")
        from tactic_client_lib import TacticServerStub 
        print ("TaticServerStub library loaded.")
    except:
        import wget
        print('wget loaded')
        sys.path.append ("tiPY")
        sys.path.append ("tactic_client_lib")
        import tiPY as qin  # qin (image database) library (requires the sys.path.append(...) line above)
        print ("Qin library loaded.")
        from tactic_client_lib import TacticServerStub  #
        print ("TaticServerStub library loaded.")
    
except:
    print ('Error loading wget, qin or tatic server stub libraries')

class RCC_PesscaraLoginDlg (QDialog) :   
    
    def __init__ (self, parent, server, project, user, password) :                                       
        QDialog.__init__ (self, parent)
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)         
        self.ui = Ui_LoginDlg ()       
        self._okClicked = False
        self._server = ""
        self._project = ""
        self._username = ""
        self._password = ""
        self.ui.setupUi (self)               
        self.ui.server.setText (server)
        self.ui.project.setText (project)
        self.ui.username.setText (user)
        self.ui.password.setText (password)
        self.ui.CancelBtn.clicked.connect (self.CancelClk)
        self.ui.OkBtn.clicked.connect (self.OKClk)
        
    def OKClk (self):
        self._okClicked = True        
        self._server = self.ui.server.text ()
        self._project = self.ui.project.text ()
        self._username = self.ui.username.text ()
        self._password = self.ui.password.text ()
        self.accept ()
    
    def getServer (self) :
        return self._server
        
    def getProject (self) :
        return self._project
        
    def getUsername (self) :
        return self._username
        
    def getPassword (self) :
        return self._password
            
    def CancelClk (self) :
        self.accept ()
                    

        
class MyTableModel(QtCore.QAbstractTableModel):
    
    def __init__(self, datain, parent = None, *args):        
        QtCore.QAbstractTableModel.__init__(self, parent, *args)
        self.subjectList = datain
        #self._CashedKeywords = {}    
    
    def getKey0 (self, subject):
        return subject.name.strip ()
        
    def getKey1 (self, subject):
        return subject.code.strip ()    
        
    def getKey2 (self, subject):
        return subject.description.strip ()            
        
    def sort(self, column, sortorder):
        self.layoutAboutToBeChanged.emit()
        reversesortorder = QtCore.Qt.AscendingOrder != sortorder
        if (column == 0):
            self.subjectList = sorted (self.subjectList, key=self.getKey0, reverse=reversesortorder)
        elif (column == 1):
            self.subjectList = sorted (self.subjectList, key=self.getKey1, reverse=reversesortorder)
        elif (column == 2): 
            self.subjectList = sorted (self.subjectList, key=self.getKey2, reverse=reversesortorder)
        self.changePersistentIndex(self.createIndex (0,0), self.createIndex(len (self.subjectList) -1,2))
        self.layoutChanged.emit ()
        
    def rowCount(self, parent):
        return len(self.subjectList)

    def flags (self, index) :
        return QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled
        
    def columnCount(self, parent):
        return 3
    
    def headerData (self, section, orientation, role):
        if role != QtCore.Qt.DisplayRole  :
            return(None)
        if (orientation == QtCore.Qt.Horizontal):
            if section == 0 :
                 return "Subject Name"
            elif (section == 1) :
                 return "Code"
            elif (section == 2) :
                 return "Description"
        else:
            return str (section)
            
    def data(self, index, role):
        if role != QtCore.Qt.DisplayRole or not index.isValid () :
            return(None)
        
        row = index.row()
        subject = self.subjectList[row]
        column = index.column()
        if column == 0 :
            return subject.name.strip ()
        elif column == 1 :
            return subject.code.strip ()
        elif column == 2 :
            return subject.description.strip ()        
                         
    
        def flags (self, index) :
            return  QtCore.Qt.ItemIsEnabled
        
class RCC_SearchPesscaraDlg (QDialog) :   
    
    def __init__ (self, parent, pesscaraInterface) :                                               
        self._selectedSubjectList = []        
        self._SubjectTxtThread = self.SubjectInfoTextThread () 
        self._SubjectTxtThread.examQuery = pesscaraInterface.examQuery
        
        self.resizeWidgetHelper = None
        QDialog.__init__ (self, parent)
        self._subjectList = None 
        self.setAttribute (QtCore.Qt.WA_DeleteOnClose, True)         
        self.ui = Ui_SearchPesscaraDlg ()               
        self.ui.setupUi (self)               
        self._pesscaraInterface = pesscaraInterface
        
        self.ui.ServerLbl.setText ("Pesscara Server: " + pesscaraInterface.getServerName ())        
        self.refreshPatientName ()
        #self.refreshExams ()
        self.ui.patientTable.setSortingEnabled(True)
        #self.ui.patientTable.horizontalHeader().sectionClicked.connect (self.tableheaderclicked)
        self.ui.RefreshPatientNameBtn.clicked.connect (self.refreshPatientName)        
                
        self.ui.CancelBtn.clicked.connect (self.CancelClk)
        self.ui.OkBtn.clicked.connect (self.OKClk)
                
        self.ui.ClearSelectionBtn.clicked.connect (self.ClearSelectionClk)
        
        self.resizeWidgetHelper = ResizeWidgetHelper ()
        self.resizeWidgetHelper.saveWidgetUIPos (self, self.ui.ServerLbl)
        self.resizeWidgetHelper.saveWidgetUIPos (self, self.ui.PatientName)
        self.resizeWidgetHelper.saveWidgetUIPos (self, self.ui.Code)
        self.resizeWidgetHelper.saveWidgetUIPos (self, self.ui.Description)
        self.resizeWidgetHelper.saveWidgetUIPos (self, self.ui.Keywords)
        self.resizeWidgetHelper.saveWidgetUIPos (self, self.ui.RefreshPatientNameBtn)
        self.resizeWidgetHelper.saveWidgetUIPos (self, self.ui.Subjectlbl)
        self.resizeWidgetHelper.saveWidgetUIPos (self, self.ui.patientTable)
        self.resizeWidgetHelper.saveWidgetUIPos (self, self.ui.SelectedPatientInfo)
        self.resizeWidgetHelper.saveWidgetUIPos (self, self.ui.OkBtn)
        self.resizeWidgetHelper.saveWidgetUIPos (self, self.ui.CancelBtn)        
        self.resizeWidgetHelper.saveWidgetUIPos (self, self.ui.ClearSelectionBtn)
        self._SubjectTxtThread.update.connect (self.updateSubjectHTML)
        self._finalizer = None
        if PythonVersionTest.IsPython3 () :
            self._finalizer = weakref.finalize (self, RCC_SearchPesscaraDlg._finalizeObj, self._SubjectTxtThread)
    
    if PythonVersionTest.IsPython2 () :
            def __del__ (self) :
                RCC_SearchPesscaraDlg._finalizeObj (self._SubjectTxtThread)
    #def tableheaderclicked (self, header) :
    #    print ("header clicked")
    #    self.ui.patientTable.repaint ()
    
    @staticmethod
    def _finalizeObj (SubjectTxtThread) : 
        SubjectTxtThread.exiting = True
        SubjectTxtThread.wait () 
        
    
    def ClearSelectionClk (self):
        model = self.ui.patientTable.selectionModel()
        model.clearSelection ()
        #self.patientTableSelectionChanged ()
        
    def _internalWindowResize (self, newsize):                            
        if (self.resizeWidgetHelper != None) :                                               
            self.resizeWidgetHelper.setAbsoluteWidth  (self.ui.ServerLbl, newsize)        
            self.resizeWidgetHelper.setAbsoluteWidth  (self.ui.PatientName, newsize)
            self.resizeWidgetHelper.setAbsoluteWidth  (self.ui.Code, newsize)
            self.resizeWidgetHelper.setAbsoluteWidth  (self.ui.Description, newsize)
            self.resizeWidgetHelper.setAbsoluteWidth  (self.ui.Keywords, newsize)
            xPos = self.width () - 89
            self.resizeWidgetHelper.setWidgetXPos (self.ui.RefreshPatientNameBtn, xPos)            
            
            self.resizeWidgetHelper.setProportionalWidth  (self.ui.Subjectlbl, newsize)
            self.resizeWidgetHelper.setProportionalWidth  (self.ui.patientTable, newsize)
            self.resizeWidgetHelper.setAbsoluteHeight  (self.ui.patientTable, newsize)            
            self.resizeWidgetHelper.setProportionalXPos  (self.ui.SelectedPatientInfo, newsize)            
            self.resizeWidgetHelper.setProportionalWidth  (self.ui.SelectedPatientInfo, newsize)
            self.resizeWidgetHelper.setAbsoluteHeight  (self.ui.SelectedPatientInfo, newsize)                        
            self.resizeWidgetHelper.setProportionalXPos  (self.ui.OkBtn, newsize)            
            self.resizeWidgetHelper.setProportionalXPos  (self.ui.CancelBtn, newsize)
            
            yPos = self.height () - 26
            self.resizeWidgetHelper.setWidgetYPos (self.ui.OkBtn, yPos)            
            self.resizeWidgetHelper.setWidgetYPos (self.ui.CancelBtn, yPos)
            self.resizeWidgetHelper.setWidgetYPos (self.ui.ClearSelectionBtn, yPos)
     
    def resizeEvent (self, qresizeEvent) :                       
        self._internalWindowResize ( qresizeEvent.size () )               
        QDialog.resizeEvent (self, qresizeEvent) 
    


    class SubjectInfoTextThread (QThread):
        update = pyqtSignal(['QString'])        
        
        def __init__(self,parent = None):            
            QThread.__init__(self, parent)            
            self.exiting = False                        
                                  
            
        def run(self):
            subject = self._subject
            mainTxt  = "<html>"
            mainTxt += "<h3>Subject: " + subject.name + "</h3>"
            mainTxt += "<b>Sex: </b>" + subject.subjectsex + "<br>"
            mainTxt += "<b>Birth date</b>" + subject.subjectbirthdate + "<br>"
            mainTxt += "<b>Code: </b>" + subject.code + "<br>"
            mainTxt += "<b>Description: </b>" + subject.description + "<br>"
            mainTxt += "</p>"            
            self.update.emit(mainTxt + "</html>")
            
            KeywordList = []
            for key, value in subject.tags.items ():
                KeywordList.append (str(key) + "=" + str (value))
                if (self.exiting):
                    return
            keywordstring = "; ".join (KeywordList)
            mainTxt += "<b>Keywords: </b>" + keywordstring + "<br>"                
            self.update.emit(mainTxt + "</html>")            
            mainTxt += "</p>"                
            
            examList = []
            
            exams = subject.getExams ()                
            if (exams != None and len (exams) > 0) :                                 
                for index, exam in enumerate (exams) :
                    examList.append ("< a href='#Exam" + str (index)+ "'>"+ exam.name + "</a>")
                    if (self.exiting):
                        return
                examNames = ", ".join(examList)
                mainTxt += "<b>Exams (N = "+str (len (exams))+"): </b>" + examNames + "<br>"
                self.update.emit(mainTxt + "</html>")                    
                examList = []                
                for index, exam in enumerate (exams) :                                            
                    examTxt = "<p>"                             
                    examTxt += "<a name='Exam" + str (index)+ "'><b>Exams: </b>"+ exam.name + "</a><br>"                    
                    examTxt += "<b>Code: </b>" + exam.code + "<br>"                             
                    examTxt += "<b>Modality: </b>" + exam.modality + "<br>"                             
                    examTxt += "<b>Study Date: </b>" + exam.studydate + "<br>"                             
                    examTxt += "<b>Study Description: </b>" + exam.studydescription + "<br>"                             
                    examList.append (examTxt)
                    if (self.exiting):
                        return
                self.update.emit(mainTxt + "".join(examList) + "</html>")                    
                
                for index, exam in enumerate (exams) :                                            
                    KeywordList = []
                    for key, value in exam.tags.items ():
                        KeywordList.append (str(key) + "=" + str (value))
                        if (self.exiting):
                            return
                    keywordstring = "; ".join (KeywordList)                    
                    examList[index] += "<b>Keywords: </b>" + keywordstring + "<br>"                           
                self.update.emit(mainTxt + "".join(examList) + "</html>")                    
                
                for index, exam in enumerate (exams) :                                            
                    series = self.examQuery (exam.code)                                            
                    examseriestxt  = "<b>Exam Series (N = %d): </b><br>"% (len (series))
                    for ser in series:
                        examseriestxt += "Name: " + ser.name + "<br>"                                                                               
                        if (self.exiting):
                            return
                    examList[index] += examseriestxt                                
            self.update.emit(mainTxt + "".join(examList) + "</html>")        
    
    def updateSubjectHTML (self, html) :
        self.ui.SelectedPatientInfo.setText (html)
        
    def patientTableSelectionChanged (self) :        
        model = self.ui.patientTable.selectionModel()
        self.ui.SelectedPatientInfo.clear ()
        if (model.hasSelection () and self._subjectList != None) :
            rows = model.selectedRows()             
            self.ui.SelectedPatientInfo.setTextInteractionFlags (QtCore.Qt.LinksAccessibleByMouse)            
            if (len (rows) == 1) :                
                rNumber = rows[0].row ()
                subject = self._subjectList[rNumber]                
                
                self._SubjectTxtThread.exiting = True
                self._SubjectTxtThread.wait ()                       
                self._SubjectTxtThread.exiting = False                
                self._SubjectTxtThread._subject = subject            
                self._SubjectTxtThread.start ()                          
            else:            
                self._SubjectTxtThread.exiting = True
                self._SubjectTxtThread.wait ()                       
                txt  = "<html>"
                txt += "<h3>Subjects selected (N = %d):</h3>" % len (rows)
                for r in rows :
                    rNumber = r.row ()
                    subject = self._subjectList[rNumber]
                    txt += "<b>Subject:</b> " + subject.name + "<br>"
                txt += "</html>"                
                self.ui.SelectedPatientInfo.setText (txt)
                
        
    def getServer (self) :
        return self._pesscaraInterface.getServer ()
            
    def getKeyword (self, text, AddWildCards = True) :        
        text = text.strip ()
        if (len (text) == 0) :
            return text
        if (AddWildCards and text[0] != "%" and text[len (text) -1] != "%"):
            return "%" + text + "%"
        return text
        
    def getKeywordDictionary (self, textbox, AddWildCards = True) :        
        Keywords = textbox.text ().split (";")                
        KeyDictionary = {}
        for pairs in Keywords :    
            KeyValue = pairs.split ("=")                                        
            Value = ""
            if (len (KeyValue) <= 2) :                
                Key  = KeyValue[0]
                if (len (KeyValue) == 2) :                
                    Value  = KeyValue[1]
                Key = self.getKeyword (Key, False)                
                Value = self.getKeyword (Value, AddWildCards)    
                if (len (Key) > 0) :
                    KeyDictionary[Key] = Value
        return KeyDictionary
                     
          
    def refreshPatientName (self) :               
        try :
            self._subjectList = self._pesscaraInterface.patientQuery ( self.getKeyword (self.ui.PatientName.text ()) , self.getKeyword (self.ui.Description.text ()),  self.getKeyword (self.ui.Code.text ()) ,self.getKeywordDictionary (self.ui.Keywords, True)                        )                
        except:
            print ("Query Error")
            self._subjectList = []

        subjectCount = len (self._subjectList)
        if (subjectCount > 0) :        
            self.ui.Subjectlbl.setText ("Subjects (N = " + str (subjectCount) +"):")
        else:
            self.ui.Subjectlbl.setText ("Subjects:")
        
        self.ui.patientTable.setModel(MyTableModel (self._subjectList))
        selection = self.ui.patientTable.selectionModel()
        selection.selectionChanged.connect(self.patientTableSelectionChanged)
            
    def OKClk (self):                
        self._selectedSubjectList = []
        model = self.ui.patientTable.selectionModel()
        if (model.hasSelection () and self._subjectList != None) :
            for row in model.selectedRows() :
                rNumber = row.row ()
                subject = self._subjectList[rNumber]
                self._selectedSubjectList.append (subject)        
        if (self._SubjectTxtThread != None) :                                
            self._SubjectTxtThread.exiting = True
            self._SubjectTxtThread.wait ()            
        self.accept ()        
            
            
    def CancelClk (self) :
        if (self._SubjectTxtThread != None) :                                
            self._SubjectTxtThread.exiting = True
            self._SubjectTxtThread.wait ()            
        self.accept ()
        
        
        


class CachedDatafile :
    def __init__ (self, filename, taticIDTuple) :
        self._age = 0
        self._filename = filename
        self._taticIDTuple = taticIDTuple
        self._finalizer = None
        if PythonVersionTest.IsPython3 () :
            self._finalizer = weakref.finalize (self, CachedDatafile._finalizeObj, self._filename)
            
    if PythonVersionTest.IsPython2 () :
        def __del__ (self) :
            CachedDatafile._finalizeObj (self._filename)
        
    def getAge (self) :
        return self._age 
        
    def setAge (self, age) : 
        self._age = age
        
    def incAge (self) :
        self._age += 1
        return self._age
    
    def getTaticIDTuple (self) :
        return self._taticIDTuple
        
    def getFilename (self) :
        return self._filename
    
    @staticmethod 
    def _finalizeObj (filename):
        if filename is not None :
            try :
                os.remove ( filename)                
                print ("")
                print ("Del Cached file: removed file" +  filename)
                print ("")
            except:
                print ("Could not remove file")
                
    def closeFile (self) :
        if self._filename is not None :
            try :
                os.remove ( self._filename)
                self._filename = None
                self._taticIDTuple = None
                self._age = -1 
                print ("")
                print ("Del Cached file: removed file" +  self._filename)
                print ("")
            except:
                print ("Could not remove file")
              
    def exists (self) :
        if self._filename is  None :
            return False
        if os.path.isfile (self._filename) :
            return True
        return False
        
        
            
class RCC_PesscaraInterface:    
    
    def __init__ (self, FObject = None):    
        self._TempDirPath  = None
        #self._PesscaraDFCache = None
        if (FObject != None) :            
            try:
                server = FObject.getParameter ("server")
                project = FObject.getParameter ("project")
                user = FObject.getParameter ("user")
                password = FObject.getParameter ("password")            
                taticServer = TacticServerStub(setup=False)
                taticServer.set_server(server)
                taticServer.set_project(project)
                ticket = taticServer.get_ticket (user, password)
                taticServer.set_login_ticket(ticket)            
                self.setTaticServer (server, project, user, password, taticServer, ticket)       
            except Exception as exp:
                RCC_PesscaraInterface.taciticLoginError (server, project, user, password, exp)
                raise 
        return
    
    @staticmethod
    def taciticLoginError (serverName, project, user, password, exp) :
         try:
             serverName = str (serverName)
         except:
             serverName = "Exception"
         try:
             project = str (project)
         except:
             project = "Exception"
         try:
             user = str (user)
         except:
             user = "Exception"
         try:
             password = str (password)
         except:
             password = "Exception"
         try:
             exp = str (exp)
         except:
             exp = "Exception"
         details = ["Tactic server login connection settings.","","Server: " + serverName,"","Project: " + project, "User: " +user, "Password: " + password,"","Exception:",exp]
         MessageBoxUtil.showMessage ("Tactic Connection Failure", "Could not connect to tactic server" + serverName + ". Please check that the sever is currently accessible.", "\n".join (details))

           
    def getTempDir (self) :
        return self._TempDirPath 
    
    def setTempDir (self, path) :
        self._TempDirPath = path
    
    def getFileObject (self):
        fobj = FileObject ("RCC_PesscaraInterface")
        fobj.setParameter ("server", self._ActiveTaticServerName)
        fobj.setParameter ("project", self._ActiveProject)
        fobj.setParameter ("user", self._ActiveUser)
        fobj.setParameter ("password", self._ActivePassword)        
        return fobj
        
    def setTaticServer (self, server, project, user, password, taticServer, ticket) :
            self._ActiveTaticServerName = server
            self._ActiveProject = project
            self._ActiveUser = user
            self._ActivePassword = password
            self._ActiveTaticServer = taticServer
            self._ActiveTaticServerTicket = ticket            

    def _login (self):
        try:
            taticServer = TacticServerStub(setup=False)
            taticServer.set_server(self._ActiveTaticServerName)
            taticServer.set_project(self._ActiveProject)
            ticket = taticServer.get_ticket (self._ActiveUser, self._ActivePassword)
            taticServer.set_login_ticket(ticket)            
            return taticServer
        except Exception as exp:
            RCC_PesscaraInterface.taciticLoginError (self._ActiveTaticServerName, self._ActiveProject, self._ActiveUser, self._ActivePassword, exp)
            raise 
     
    def getPassword (self) :
        return self._ActivePassword
        
    def getProject (self) :
        return self._ActiveProject
        
    def getUser (self) :
        return self._ActiveUser
        
    def getServerName (self) :
        return self._ActiveTaticServerName
        
    def getServer (self) :
        return self._ActiveTaticServer
        
    def isConnected (self) :
        return self.getServer () != None        

    def login (self, parent, server, project, user, password) :
        dlg = RCC_PesscaraLoginDlg (parent, server, project, user, password)
        dlg.exec_()
        if (dlg._okClicked) :           
            try:                
                taticServer = TacticServerStub(setup=False)
                taticServer.set_server(dlg.getServer())
                taticServer.set_project(dlg.getProject ())
                ticket = taticServer.get_ticket (dlg.getUsername (), dlg.getPassword ())
                taticServer.set_login_ticket(ticket)            
                self.setTaticServer (dlg.getServer(), dlg.getProject (), dlg.getUsername (), dlg.getPassword (), taticServer, ticket)                
                return True
            except Exception as exp:
                RCC_PesscaraInterface.taciticLoginError (dlg.getServer(), dlg.getProject (), dlg.getUsername (), dlg.getPassword (), exp)
                self.setTaticServer (None, None, None, None, None, None)                
        else:
            self.setTaticServer (None, None, None, None, None, None)                
        return False    
     
    def selectSubjectDataset (self, parent) :
        dlg = RCC_SearchPesscaraDlg (parent, self)
        dlg.exec_ ()        
        return dlg._selectedSubjectList
    
    def examQuery (self, examcode, newTaticSession = False) :                    
        if (newTaticSession) :
            taticServer = self._login ()                    
            expression = "@SOBJECT(qin/exam['code','%s'].qin/series)" %examcode
            #print (("series Query", expression))
            series = qin.Series.search (expression = expression, server=taticServer)
            return series
        else:
            expression = "@SOBJECT(qin/exam['code','%s'].qin/series)" %examcode
            #print (("series Query", expression))
            series = qin.Series.search (expression = expression, server=self.getServer())
            return series
             
    def getAllSubjectsExamsSeriesQuery (self, subjectCode) :                    
            taticServer = self._login ()                      
            expression = "@SOBJECT(qin/exam['subject_code','%s'].qin/series)" %subjectCode
            #print (("series Query", expression))
            return qin.Series.search (expression = expression, server=taticServer)
        
        
    def getSubjectFromSubjectCode (self, code):        
        subjectQuery = ""
        if (code != "") : 
            subjectQuery += "['code','=','" + code + "']"                                    
        expression = "@SOBJECT(qin/subject" + subjectQuery + ")"
        subjectList = qin.Subject.search(expression=expression, server=self.getServer())
        if (len (subjectList) == 1) :
            return subjectList[0]
        return None
        
            
    def patientQuery (self, subjectName = "", discriptiontext = "", code = "", keywords = {}) :                    
        if (subjectName == "" and discriptiontext == "" and code == "") :
            subjectName = "%"
        
        subjectQuery = ""
        if (subjectName != "") :
            subjectQuery += "['subjectname','like','" + subjectName + "']"                  
        if (discriptiontext != "") : 
            subjectQuery += "['description','like','" + discriptiontext + "']"                              
        if (code != "") : 
            subjectQuery += "['code','like','" + code + "']"                  
           
        keywordQueryList = []
        if (len (keywords) > 0) :            
            for key, value in keywords.items () :                                                        
                keywordQuery = "['key','=','" + key + "']"  
                if (value != "") :
                    keywordQuery += "['text_value','like','" + value + "']"
                keywordQueryList.append (keywordQuery)
        if (len (keywordQueryList) == 0) :
            expression = "@SOBJECT(qin/subject" + subjectQuery + ")"
            return qin.Subject.search(expression=expression, server=self.getServer())
        else:
            subjectResultList = []
            for keywordQuery in keywordQueryList :
                expression = "@SOBJECT(qin/subject_info" + keywordQuery + ".qin/subject" + subjectQuery + ")"                            
                #print (("search",  expression))
                subjectResultList.append (qin.Subject.search(expression=expression, server=self.getServer()))
            if (len (subjectResultList) == 1):
                return subjectResultList[0]
            else:
                testlist = subjectResultList[0]
                for subjectlist in subjectResultList :
                    if (len (subjectlist) < len (testlist)) :
                        testlist = subjectlist
                
                testSubjectDictionaryList = []
                for index, subjectlist in enumerate (subjectResultList) : 
                    if (subjectlist != testlist) :
                        subjectdictionary = {}
                        for subject in subjectlist :
                            subjectdictionary[subject.code] = True
                        testSubjectDictionaryList.append (subjectdictionary)
                        subjectResultList[index] = 0
                        
                joinSubjectList = []
                for subject in testlist :                    
                    found = True
                    for subjectDictionary in testSubjectDictionaryList :
                       if (subject.code not in subjectDictionary) :
                           found = False
                           break
                    if (found) :
                        joinSubjectList.append (subject)
                return joinSubjectList

    
    def getSeriesNIfTIDataFileBySearchKey (self, SeriesSearchKey, PesscaraContext, taticServer = None):       
        if (taticServer != None) :
            taticServer = self._login ()                           
        try :
            pathsa = taticServer.get_paths (SeriesSearchKey, context=PesscaraContext, version=-1, file_type='*', versionless=True)
            file_loca= pathsa['web_paths']
        except:
            print ("Could not find image associated with dataset in pesscara")
            return None            
        if file_loca[0].lower().endswith (".nii") :
            extention = ".nii"
        elif file_loca[0].lower().endswith (".nii.gz") :
            extention = ".nii.gz"
        else:
            return None
        tempNiftiFileName = self._makeTempFile  (suffix= "_"+str (uuid.uuid1 ())+extention)                        
        try:            
            return wget.download(file_loca[0], out=tempNiftiFileName)
        except:
            try :
                os.remove (tempNiftiFileName)
                return None
            except :
                return None            
        
    def getSeriesNIfTIDataFile (self, Series, PesscaraContext):        
        taticServer = self._login ()                   
        return self.getSeriesNIfTIDataFileBySearchKey (Series.__search_key__, PesscaraContext, taticServer = taticServer)

    def _makeTempFile (self, suffix) :            
            try:            
                tempNiftiFile = tempfile.mkstemp (suffix= suffix, dir = self.getTempDir ())        
                tempNiftiFileName = tempNiftiFile[1]
                os.close (tempNiftiFile[0])
                os.remove (tempNiftiFileName)                                
                return tempNiftiFileName
            except:
                self.setTempDir (None)
                tempNiftiFile = tempfile.mkstemp (suffix= suffix)        
                tempNiftiFileName = tempNiftiFile[1]
                os.close (tempNiftiFile[0])
                os.remove (tempNiftiFileName)                                
                return tempNiftiFileName
      
       
    def _createLockFile (self, UserName, ProcessID) :        
        tempNiftiFileName = self._makeTempFile (str (uuid.uuid1 ())+"_NewLock.txt")        
        filelines = []
        timestr = str (datetime.datetime.now ())
        try :
            hostName = socket.getfqdn()
        except :
            hostName = ""
        try :
            address = socket.gethostbyname (hostName)    
        except: 
            address = ""
        filelines.append (UserName + "_" + str(ProcessID) + "_" + str (uuid.uuid1 ()) + "_" + timestr+ "_" + hostName + "@" + address)
        filelines.append (UserName)        
        filelines.append (ProcessID)        
        filelines.append (timestr)
        filelines.append (hostName)
        filelines.append (address)        
        txtfile = open (tempNiftiFileName,"wt")
        for lines in filelines :
            txtfile.writelines (lines + "\n")        
        txtfile.close ()
        return (tempNiftiFileName, filelines)
    
    def _getROIDataFileLockTxt (self, taticServer, Series, ProjectName) :
        try :
            file_type = "_UserLock.txt"                   
            pathsa = taticServer.get_paths (Series.__search_key__, context=ProjectName+"_UsrLock", version=-1, file_type=file_type, versionless=True)
            file_loca= pathsa['web_paths']
            
            tempNiftiFileName = self._makeTempFile  (suffix= str (uuid.uuid1 ())+"_UserLockData.txt")                    
                    
            try:
                wget.download(file_loca[0], out=tempNiftiFileName)            
                infile = open (tempNiftiFileName,"rt")
                lines = []                
                for line in infile:
                    lines.append (line.strip ())
                infile.close ()                
                return lines                
            finally:
                try :
                    os.remove (tempNiftiFileName)
                except:
                    if os.path.isfile(tempNiftiFileName) :
                        print ("Warrning a error occured could not remove file: " + tempNiftiFileName)    
        except:
            print ("Error could not get lock details from tactic")
        return None
        
    def _getFileLockHeader (self, taticServer, Series, ProjectName) :        
        header = self._getROIDataFileLockTxt  (taticServer, Series, ProjectName)
        if header == None :
            return header
        if len (header) == 0 :
            return None
        if len (header) > 0 :
            return header[0]
        
          
    def _tryLockROIFile (self, taticServer, Series, ProjectName, UserNameStr, ProcessIDStr) :
        file_type = "_UserLock.txt"       
        LockFileLines = []
        try :                         
            DatafilePath, LockFileLines = self._createLockFile (UserNameStr, ProcessIDStr)            
            CheckinSucceed = False
            LockFileSucceed = False
            try :                
                taticServer.simple_checkin(Series.__search_key__, ProjectName+"_UsrLock", DatafilePath, mode='upload', create_icon=False, checkin_type='auto', file_type=file_type)                    
                CheckinSucceed = True
                taticServer.lock_sobject (Series.__search_key__, ProjectName+"_UsrLock")
                LockFileSucceed = True
            finally :
                os.remove (DatafilePath)     
                           
        except:
            print (("Lock Results", CheckinSucceed, LockFileSucceed))            
            return False, LockFileLines        
            
        if (len (LockFileLines) == 0) :
            return False, LockFileLines        
        
        FirstLineRead = self._getFileLockHeader (taticServer, Series, ProjectName)                    
        return (LockFileLines[0] == FirstLineRead), LockFileLines
    
    def tryLockROIFile (self, Series, ProjectName, fileLockedTxt, taticServer = None) :
        if (taticServer == None) :
            taticServer = self._login ()                    
        succeed ,_ = self._tryLockROIFile (taticServer, Series, ProjectName, fileLockedTxt[1], fileLockedTxt[2])
        return succeed
                
    def hasFileLockChanged (self, Series, ProjectName, fileLockedTxt, taticServer = None) :            
        if (taticServer == None) :
            taticServer = self._login ()            
        header = self._getFileLockHeader (taticServer, Series, ProjectName)         
        if (len (fileLockedTxt) == 0):
            return True
        return (fileLockedTxt[0] != header)    
    
    def unlockDataSeries (self, Series, ProjectName, taticServer = None) :
        if (taticServer == None) :
            taticServer = self._login ()                      
        taticServer.unlock_sobject (Series.__search_key__, ProjectName+"_UsrLock")                             
        
    def getROIDataFileLockDetails (self, Series, ProjectName, taticServer = None) :
        if (taticServer == None) :
            taticServer = self._login ()                      
        header = self._getROIDataFileLockTxt  (taticServer, Series, ProjectName)                
        if (header is not None and len (header) >= 2) :
            UserName = header[1]
        else:
            UserName = ""
        if (header is not None and len (header) >= 3) :
            ProcessID = header[2]
        else:
            ProcessID = ""
        if (header is not None and len (header) >= 4) :
            timestr = header[3]
        else:
            timestr = ""
            
        if (header is not None and len (header) >= 5) :
            hostName = header[4]
        else:
            hostName = ""
        if (header is not None and len (header) >= 6) :
            ipAddress = header[5]
        else:
            ipAddress = ""
        return (UserName,ProcessID,timestr, hostName, ipAddress)
        
    def getSeriesROIDataFile (self, Series, ProjectName, UniqueName = "", UserNameStr ="", ProcessIDStr = "", IgnoreLock = False, taticServer = None):
        if (taticServer == None) :
            taticServer = self._login ()   
        
        fileLockTxt = []
        fileLocked = False
        #pathsa = taticServer.get_paths (Series.__search_key__, context=ProjectName, version=-1, file_type='_ROI.txt', versionless=True)
        
        if (not IgnoreLock):
            fileLocked, fileLockTxt = self._tryLockROIFile (taticServer, Series, ProjectName, UserNameStr, ProcessIDStr)            
            if (not fileLocked) :
                print ("Could not lock series in tatic")                
                return (None, fileLocked, fileLockTxt)
                
        pathsa = taticServer.get_paths (Series.__search_key__, context=ProjectName, version=-1, file_type='_ROI.txt', versionless=True)
        if ('web_paths' not in pathsa) :
            return (None, fileLocked, fileLockTxt)
        
        file_loca= pathsa['web_paths']
        tempNiftiFileName = self.createSeriesROIDataFile (UniqueName)
        try:            
            return (wget.download(file_loca[0], out=tempNiftiFileName), fileLocked, fileLockTxt)
        except:
            try :
                os.remove (tempNiftiFileName)
                return (None, fileLocked, fileLockTxt)
            except :
                return (None, fileLocked, fileLockTxt)
                        
    def createSeriesROIDataFile (self, UniqueName = ""):
        tempNiftiFileName = self._makeTempFile  (suffix= UniqueName +"_"+str (uuid.uuid1 ())+"_ROI.txt")        
        return tempNiftiFileName
                
                   
        
    def setSeriesROIDataFile (self, Series, ProjectName, DatafilePath, hasContours, SeriesDescription, datasetTags = None, UnlockROI = False, taticServer = None, DescriptionTacticContext = ""):                
        if (taticServer == None) :
            taticServer = self._login ()  
            
        search_key = Series.__search_key__               
        if DatafilePath is not None :
            try :
                taticServer.simple_checkin(search_key, ProjectName, DatafilePath, mode='upload', create_icon=False, checkin_type='auto', file_type='_ROI.txt')                    
            except Exception as exception:
                print ("An error occured saving the file to tactic.")
                print (exception)
            
        tag = "RILC_" + ProjectName
        inc = 0
        try :     
            SaveSeries = False
            try :
                DescriptionTag = "RILC_" + DescriptionTacticContext + "_Description"
            except:
                DescriptionTag = tag + "_Description"
            if (hasContours and (tag not in Series.tags or Series.tags[tag] != '1')) :
               Series.tags[tag] = '1'
               Series.tags[DescriptionTag] = SeriesDescription
               inc = 1
               SaveSeries = True
            elif (not hasContours and (tag not in Series.tags or Series.tags[tag] != '0')) :
               Series.tags[tag] = '0'
               Series.tags[DescriptionTag] = SeriesDescription
               inc = -1
               SaveSeries = True
            elif (DescriptionTag not in Series.tags or Series.tags[DescriptionTag] != SeriesDescription) :
                Series.tags[DescriptionTag] = SeriesDescription                
                SaveSeries = True
            if (datasetTags is not None) :
                for index, ds_tag in enumerate (datasetTags.getTags ()) :              
                    if (ds_tag.storeTagInTatic ()) :
                        tagname = ds_tag.getName ()
                        if (ds_tag.shouldMangleTagNameWithTaticContext ()) :
                            tagname = "RILC_" + ds_tag.getTagTacticContext (ProjectName) + "_" + tagname    
                        tagValue = ds_tag.getValue ()
                        if (tagname not in Series.tags or Series.tags[tagname] != tagValue) :
                            Series.tags[tagname] = tagValue
                            SaveSeries = True   
            if (SaveSeries):
                Series.save ()
        except Exception as exception:
            print ("Error occured setting Series")
            print (exception)
        if (inc != 0) :            
            ExamAddedorRemoved = False
            try:   
                Exam = Series.getExam ()                                          
                examCount = str (self.getExamSeriesCount (tag, Exam.code))                     
                if (tag not in Exam.tags) :
                    Exam.tags[tag] = examCount
                    Exam.save ()                    
                    ExamAddedorRemoved = True
                else:
                    oldExamCount = Exam.tags[tag]
                    if (oldExamCount != examCount) :
                        Exam.tags[tag] = examCount
                        Exam.save ()                    
                        ExamAddedorRemoved = (examCount == "0" or oldExamCount == "0")                              
            except Exception as exception:
                print ("Error occured updating the exam count")
                print (exception)
            
            if (ExamAddedorRemoved) :
                try :            
                    Subject = Exam.getSubject ()                           
                    subjectCount = str (self.getSubjectExamCount (tag, Subject.code))
                    if (tag not in Subject.tags or Subject.tags[tag] != subjectCount) :
                        Subject.tags[tag] = str(subjectCount)
                        Subject.save ()
                except Exception as exception:
                    print ("Error occured updating the subject count")
                    print (exception)
        if (UnlockROI) :
            self.unlockDataSeries (Series, ProjectName, taticServer = taticServer)                                            
            
        
        
    def doSubjectsHaveProjectDataset (self, ProjectName, pesscaraNiftiSubjectList) :                    
        testtag = "RILC_" + ProjectName        
        taticServer = self._login ()                      
        expression = "@SOBJECT(qin/subject_info['key', '=', '%s']['text_value','!=','0'].qin/subject)" %(testtag)
        result = qin.Subject.search (expression = expression, server=taticServer)
        if len (result) < len (pesscaraNiftiSubjectList) :
            testTree = {}
            for subject in result :
                testTree[subject.code] = 1
            for subject in pesscaraNiftiSubjectList :
                if subject.code in testTree :
                    return True
        else:
            testTree = {}
            for subject in pesscaraNiftiSubjectList :
                testTree[subject.code] = 1
            for subject in result :
                if subject.code in testTree :
                    return True        
        return False
    
    def getSubjectsWithTag (self, testtag) :
        taticServer = self._login ()                      
        expression = "@SOBJECT(qin/subject_info['key', '=', '%s']['text_value','!=','0'].qin/subject)" %(testtag)
        #print (("Subject Query", expression))
        result = qin.Subject.search (expression = expression, server=taticServer)
        subjectcodelist = []
        for subject in result :
            subjectcodelist.append(subject.code)
        return subjectcodelist
    
    def _getExamsWithTag (self, testTag, subject) :
        taticServer = self._login ()                      
        expression = "@SOBJECT(qin/exam_info['key', '=', '%s']['text_value','!=','0'].qin/exam['subject_code','%s'])" %(testTag, subject)
        #print (("Exam Query", expression))
        return qin.Exam.search (expression = expression, server=taticServer)
    
    def getSubjectExamCount (self, testtag, subject) :
        result = self._getExamsWithTag (testtag, subject)
        return len (result)    
        
    def getExamsWithTag (self, testTag, subject) :
        result = self._getExamsWithTag (testTag, subject)
        examcodelist = []
        for exam in result :
            examcodelist.append (exam.code)
            #print (("Subject code",subject,"Found Exams", exam.name, exam.code))
        return examcodelist
                
    
    def getAllSeriesForSubject (self, subjectcode) :
        taticServer = self._login ()           
        expression = "@SOBJECT(qin/exam['subject_code', '=', '%s'].qin/series)" %(subjectcode)
        #print (("Series Query", expression))
        return qin.Series.search (expression = expression, server=taticServer)
                
    def getAllSeriesWithTag (self, tag) :
        taticServer = self._login ()           
        expression = "@SOBJECT(qin/series_info['key', '=', '%s']['text_value','!=','0'].qin/series)" %(tag)
        return qin.Series.search (expression = expression, server=taticServer)        
            
    def _getSeriesWithTag (self, testTag, exam) :                    
        taticServer = self._login ()           
        expression = "@SOBJECT(qin/series_info['key', '=', '%s']['text_value','!=','0'].qin/series['exam_code','%s'])" %(testTag, exam)
        #print (("Series Query", expression))
        return qin.Series.search (expression = expression, server=taticServer)
    
    def getSeries (self,  exam, seriescode, taticServer = None) :                    
        if taticServer is None :
            taticServer = self._login ()           
        expression = "@SOBJECT(qin/series['exam_code','%s']['code','%s'])" %( exam, seriescode)
        #print (("Series Query", expression))
        return qin.Series.search (expression = expression, server=taticServer)
    
    def getSeriesWithTag (self, testTag, exam) :
        result = self._getSeriesWithTag (testTag, exam)
        seriescodelist = []
        for series in result :
            seriescodelist.append (series.code)
        return seriescodelist  
                    
    def getExamSeriesCount (self, testtag, exam) :
        result = self._getSeriesWithTag (testtag, exam)
        return len (result)
        
    def setSeriesROIMask (self, localfileName, series, PesscaraContext) :
        if localfileName.lower().endswith (".nii.gz") :
            file_type='.nii.gz'
        elif localfileName.lower().endswith (".nii") :
            file_type='.nii'
        else:
            print ("Cannot checkFile in unrecognized file type")
            file_type = None 
        if file_type is not None :
            taticServer = self._login ()                             
            taticServer.simple_checkin(series.__search_key__, PesscaraContext, localfileName, mode='upload', create_icon=False, checkin_type='auto', file_type=file_type)        
            print ("Exporting mask dataset: " + localfileName + " as context: " + PesscaraContext)            
