#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec  6 11:39:49 2018

@author: m160897
"""
import os
from rilcontour.rilcontourlib.dataset.roi.rcc_roi import ROIDictionary,ROIDefinitions
from rilcontour.rilcontourlib.dataset.rcc_DatasetROIInterface import RCC_ROIDataset_FileSystemObject


   
def getfilelist (path) :
    returnFileList  = []
    pathQueue = [path]
    while len (pathQueue) > 0 :
        path = pathQueue.pop ()
        filelist = os.listdir (path)
        for filename in filelist :
            fP = os.path.join (path, filename)
            if os.path.isdir (fP) :
                pathQueue.append (fP)
            elif fP.endswith ("_ROI.txt") :                                
                    returnFileList.append (fP)
    return returnFileList  

if __name__ == "__main__":    
    fileList = {}
    basepath = "/home/m160897/Desktop/mitchData/Rilcontour/ROIData"
    fileDirList = getfilelist (basepath)    
    for filePath in fileDirList:        
        roidictionary = ROIDictionary (ROIDefinitions (), None)         
        fileHandle = RCC_ROIDataset_FileSystemObject (filePath)             
        log = None
        try :
            roidictionary.loadFromFile (fileHandle)
            timeLog = roidictionary.getFileChangeTimeLog ()
            log = timeLog.getTimeLog ()
            roidictionary.delete ()
        except:
            log = None
        if (log is not None) :           
            if filePath not in fileList :
                fileList[filePath] = {}
            for session in log :
                username = session["UserName"]
                if username not in fileList[filePath] :
                    fileList[filePath][username] = []                
                dT = session["ElapsedTime"]
                fileList[filePath][username].append (dT)
    print ("")
    print ("")    
    FilterPerson = "MP"    
    pathList = sorted (list (fileList.keys ()))
    for path in pathList :        
        DirPath , fname = os.path.split (path)
        _ , DirName = os.path.split (DirPath)        
        personList = sorted (list (fileList[path].keys ()))
        if FilterPerson in personList :
            person = FilterPerson
            timelist = fileList[path][person]            
            print ("%s : %s, %f" % (DirName, fname, sum (timelist)))
    
                
    