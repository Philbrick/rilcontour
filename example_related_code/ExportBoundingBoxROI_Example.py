#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 16 11:33:03 2017

@author: m160897
"""
import os
from rilcontour.rilcontourlib.dataset.rcc_DatasetROIInterface import RCC_ROIDataset_FileSystemObject
from rilcontour.rilcontourlib.dataset.roi.rcc_roi import ROIDefinitions, ROIDictionary
from rilcontour.rilcontourlib.util.rcc_util import NiftiVolumeData

"""
Code reads in ROI file object and exports nifti mask file masking the ROI using the bounding box dimensions

    Mask background = 0
    ROI mask value = 1    
"""

def WriteROIBoundBoxFileMask (roifilepath, maskfilepath) :
    ROIDataHandle = RCC_ROIDataset_FileSystemObject (roifilepath)
    tempDictionary = ROIDictionary (ROIDefinitions(), None, None)   
    tempDictionary.loadFromFile (ROIDataHandle)                           
    volumeDescription = tempDictionary.getNIfTIVolumeOrientationDescription ()
    NameLst = tempDictionary.getDefinedROINames ()
    objLst = []
    for name in NameLst :
        objLst.append (tempDictionary.getROIObject (name))
    NiftiVolumeData._writeMaskVolume (volumeDescription, maskfilepath, objLst,  Background = 0, MaskValue = 1, InSlicePointSize = 9, CrossSlicePointSize = 1, RoiDefs = tempDictionary.getROIDefs(), BinaryOrMask = False, ExportOriginalNiftiFile = False, ExportOrigionalNiftiFilePath = "", ExportNiftiMaskfileDescription = False, roiDictionary = tempDictionary, ExportAreaROIUsingBoundingVolumeDefinition = True, OverwriteFiles = False) 
    

if __name__ == "__main__":
    dirlist = []
    dirlist.append ("/research/projects/DATA_LOGLIO/DATA_LOGLIO")
    CrawlDir = False
    while (len (dirlist) > 0) :
        path = dirlist.pop ()
        for filename in os.listdir (path) :
            filepath = os.path.join (path, filename)
            if (os.path.isfile (filepath) and filename.endswith ("_ROI.txt")) :
                maskfilename = "BoundingBoxMask" + filename.replace ("_ROI.txt",".nii.gz")
                maskfilename = os.path.join (path, "BoundingBoxMask/"+ maskfilename)
                try :        
                    WriteROIBoundBoxFileMask (filepath, maskfilename)
                    print ("Writing mask: %s" % (maskfilename))
                except:
                    print ("Error: could not export mask for ROI file: %s" % (filepath))
            elif CrawlDir and os.path.isdir (filepath)  :
                dirlist.append (filepath)     
    print ("Done!")
    exit ()