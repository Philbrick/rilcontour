RIL-Contour
==========================

The "rilcontour" package consists of analysis tools to facilitate medical imaging research. The toolkit consists of an imaging viewer with a wide range of capabilities for annotating medical images. These include region of interest and patch selection. Measurement of regions, including volume and voxel value statistics. Semi and fully automated segmentation approaches are also implemented. 

Citation
-----------
[Philbrick KA, Weston AD, Akkus Z, Kline TL, Korfiatis P, Sakinis T, Kostandy P, Boonrod A, Zeinoddini A, Takahashi N, Erickson BJ. RIL-Contour: a Medical Imaging Dataset Annotation Tool for and with Deep Learning. J Digit Imaging. May, 2019](https://link.springer.com/epdf/10.1007/s10278-019-00232-0?author_access_token=pMSIWMs1IQ7m4VfvGB7Tqfe4RwlQNchNByi7wbcMAY5rB2rQkgTu1c6YBqHA4CHTE4wLeclO8z5j-sY16MhbMNqFxBWcU9V2grj04SDhUJLVDV6y3L49RSK8v6_RwOZH2dy__z6ukec5uBGIc97jSA%3D%3D)

References
-----------
[Philbrick KA, Weston AD, Akkus Z, Kline TL, Korfiatis P, Sakinis T, Kostandy P, Boonrod A, Zeinoddini A, Takahashi N, Erickson BJ. RIL-Contour: a Medical Imaging Dataset Annotation Tool for and with Deep Learning. J Digit Imaging. May, 2019](https://link.springer.com/epdf/10.1007/s10278-019-00232-0?author_access_token=pMSIWMs1IQ7m4VfvGB7Tqfe4RwlQNchNByi7wbcMAY5rB2rQkgTu1c6YBqHA4CHTE4wLeclO8z5j-sY16MhbMNqFxBWcU9V2grj04SDhUJLVDV6y3L49RSK8v6_RwOZH2dy__z6ukec5uBGIc97jSA%3D%3D)

[Sakinis T, Milletari F, Roth H, Korfiatis P, Philbrick KA, Akkus Z, Xu Z, Xu D, Erickson BJ. Interactive segmentation of medical images through fully convolutional neural networks. arXiv preprint arXiv: 1903.08205, 2019.](https://arxiv.org/abs/1903.08205)

[Philbrick KA, Yoshida K, Inoue D, Akkus Z, Kline TL, Weston AD, Korfiatis P, Takahashi N, Erickson BJ. What Does Deep Learning See? Insights From a Classifier Trained to Predict Contrast Enhancement Phase From CT Images. Am J Roentgen. 211:1184-1193, 2018](https://www.ajronline.org/doi/full/10.2214/AJR.18.20331)

[Kline TL, Edwards ME, Korfiatis P, Akkus Z, Torres VE, Erickson BJ. Semiautomated segmentation of polycystic kidneys in T2-weighted MR images. Am J Roentgen. 207:1-9, 2016.](https://www.ncbi.nlm.nih.gov/pubmed/27341140)

[Kline TL, Korfiatis P, Edwards ME, Warner JD, Irazabal MV, King BF, Torres VE, Erickson BJ. Automatic total kidney volume measurement on follow-up magnetic resonance image to facilitate monitoring of ADPKD progression. Nephrol Dial Transplant. 31(2):241-248, 2016.](Automatic total kidney volume measurement on follow-up magnetic resonance image to facilitate monitoring of ADPKD )

Developers
-----------

    - Kenneth A Philbrick, PhD
    - Timothy L Kline, PhD
    
Installation
------------

 Install anaconda (python 3 version available at https://www.continuum.io/downloads).

 At an anaconda command prompt run:

    conda config --add channels conda-forge   

    conda config  --add channels philbrik 

    conda install rilcontour

After that, you can run the gui from the command line by just typing rilcontour

Funding
-----------
This work was supported by the the National Cancer Institute (NCI) under grant/award CA160045, the PKD Foundation under grant 206g16a, and the National Institute of Diabetes and Digestive and Kidney Diseases under NIH Grant/Award Number P30 DK090728 “Mayo Translational PKD Center (MTPC). 

